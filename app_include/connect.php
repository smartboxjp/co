<?php 
session_start();
$global_path = $_SERVER['DOCUMENT_ROOT']."/";
require_once "/home/cossot-com/cossot.com/app_cossot/connect_db.php";

require_once $global_path."/app_include/CommonConnect.php";
require_once $global_path."/app_include/CommonDao.php";

if($_SERVER['HTTP_HOST']=="test.cossot.com")
{
	//テストサーバー
	$testserver = true;
	define('global_service_name', "cossot");
	define('global_no_ssl', "http://test.cossot.com");//www.
	define('global_ssl', "http://test.cossot.com");//www.
	define('global_site', "test.cossot.com");
}
else
{
	//本番
	define('global_service_name', "cossot");
	define('global_no_ssl', "http://www.cossot.com");//www.
	define('global_ssl', "http://www.cossot.com");//www.
	define('global_site', "www.cossot.com");
}

define('global_copyright', "Copyright (C) cossot.com. All Rights Reserved.");
define('global_shop_dir', "app_photo/shop/");
define('global_apply_dir', "app_photo/apply/");
define('global_member_dir', "app_photo/member/");
define('global_voice_img_dir', "app_photo/voice_img/");
define('global_matome_dir', "app_photo/matome/");
define('global_pr_dir', "app_photo/pr/");
define('global_pr_fix_dir', "app_photo/pr_fix/");
define('global_admin_temp_dir', "app_photo_temp/admin/");
define('global_shop_temp_dir', "app_photo_temp/shop/");
define('global_shop_blog_dir', "app_photo/shop_blog/");
define('global_topics_dir', "app_photo/topics/");
define('global_shop_movie_dir', "app_photo/movie/");




//facebook
define('MY_APP_ID', '588305291330356');
define('MY_APP_SECRET', '9e6b3c715274a9571ffa0e6e4782e505');

//twitter
define('Consumer_Key', 'Jps1EPSUMqR6ZHUcaKWLsNme6');
define('Consumer_Secret', 'nlDKdMz4fjg5w2TMAzDvqhps5qM93jNVLVb84rlxwA2HH5Limr');


$arr_global_area = preg_split("/\//", $_SERVER['PHP_SELF']);
$global_area = $arr_global_area[1];


$global_send_mail = "sendonly@cossot.com";//送信先
$global_mail_from = "cossot";
$global_bcc_mail = "system@cossot.com";//受信
$global_support_mail = "support@cossot.com";


$global_email_footer = <<<EOF
*******************************************************************
  ○○○株式会社
 
  〒○○○-○○○
  東京都新宿○○○
  TEL：03-○○○-○○○
  Fax：03-○○○-○○○
  E-mail: support@cossot.com
*******************************************************************
※このメールにお心当たりのない場合は、上記メールアドレスまでご連絡ください。
※このメールは送信専用メールアドレスから配信されています。
EOF;
?>