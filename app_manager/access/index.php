<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/config.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_start.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_head.php';
?>


<!-- Page content -->
<div id="page-content">
	
	
	<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_headnavi.php'; ?>

	<!-- パンクズリスト -->
	<ul class="breadcrumb breadcrumb-top">
		<li><a href="">管理画面</a>
		</li>
		<li>アクセス</li>
	</ul>
	<!-- パンクズリスト -->
	
	<div class="row">

	<div class="col-md-3">
		<div class="block">
			<div class="block-title">
				<h2>アクセス数</h2>
			</div>
			<p>自分のお店ページのアクセス数を確認出来ます。
			確認出来る時期は本日も含めて過去一週間分となります。それ以降は削除されますので、ご注意下さい。
			また表示されてる数字は<span class="text-danger">ページビュー数</span>です。<span class="text-danger">アクセスランキング</span>などはユーザー数で計算されてますので
			ご注意下さい。</p>

<?
	$yyyymmdd_0 = date("Y/m/d");
	$yyyymmdd_7 = date("Y/m/d", strtotime("-7 day"));

	$yyyymm_from01 = date("Ym01", strtotime("-7 day"));
	$yyyymm_to01 = date("Ym01");

	$arr_pv = array();
	for ($i=$yyyymm_from01; $i <= $yyyymm_to01; $i=date("Ym01", strtotime($i." +1 month"))) { 
		$sql = "select ";
		for ($loop=1; $loop <=31 ; $loop++) { 
			$sql .= " sum(p_".substr(($loop+100), 1).") as s_".substr(($loop+100), 1).", ";
		}
		$sql .= " 1 from shop_pv where shop_id='".$shop_id."' and yyyymm='".date("Ym", strtotime($i))."' group by yyyymm, shop_id ";
		$db_result = $common_dao->db_query_bind($sql);
		if($db_result)
		{
	        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
	        {
				for ($loop=1; $loop <=31 ; $loop++) { 
					$arr_pv[date("Ym", strtotime($i)).substr(($loop+100), 1)] = $db_result[$db_loop]["s_".substr(($loop+100), 1)];
				}
			}
		}
	}

	for ($i=$yyyymmdd_7; $i <= $yyyymmdd_0; $i=date("Y/m/d", strtotime($i." +1 day"))) { 
		$arr_day[(strtotime($yyyymmdd_0)-strtotime($i))/(3600*24)] = $arr_pv[date("Ymd", strtotime($i))];
	}
?>

			<div class="table-responsive">
				<table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
					<thead>
						<tr>
							<td>日時</td>
							<td>アクセス数</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>本日</td>
							<td><? $loop=0; if($arr_day[$loop]=="") { echo "-";} else {echo $arr_day[$loop];} $loop++;?></td>
						</tr>
						<tr>
							<td>昨日</td>
							<td><? if($arr_day[$loop]=="") { echo "-";} else {echo $arr_day[$loop];} $loop++;?></td>
						</tr>
						<tr>
							<td>一昨日</td>
							<td><? if($arr_day[$loop]=="") { echo "-";} else {echo $arr_day[$loop];} $loop++;?></td>
						</tr>
						<tr>
							<td><? echo $loop;?>日前</td>
							<td><? if($arr_day[$loop]=="") { echo "-";} else {echo $arr_day[$loop];} $loop++;?></td>
						</tr>
						<tr>
							<td><? echo $loop;?>日前</td>
							<td><? if($arr_day[$loop]=="") { echo "-";} else {echo $arr_day[$loop];} $loop++;?></td>
						</tr>
						<tr>
							<td><? echo $loop;?>日前</td>
							<td><? if($arr_day[$loop]=="") { echo "-";} else {echo $arr_day[$loop];} $loop++;?></td>
						</tr>
						<tr>
							<td><? echo $loop;?>日前</td>
							<td><? if($arr_day[$loop]=="") { echo "-";} else {echo $arr_day[$loop];} $loop++;?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col-md-9">
		<div class="block">

			<div class="block-title">
				<h2>グラフ表示</h2>
			</div>

			<p>グラフ化され表示されます。</p>
			<div class="">
				<canvas id="myChart"></canvas>
			</div>

		</div>
	</div>
	
	</div>

</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_footer.php'; ?>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_scripts.php'; ?>

<!-- チャート -->
<script src="/app_manager/js/tool/chart/chart.js"></script>
<script>
var ctx = document.getElementById("myChart");
var myBarChart = new Chart(ctx, {
  //グラフの種類
  type: 'bar',
  //データの設定
  data: {
      //データ項目のラベル
      labels: ['今日', '昨日', '一昨日', '3日前', '4日前', '5日前', '6日前'],
      //データセット
      datasets: [{
          //凡例
          label: "アクセス",
          //背景色
          backgroundColor: "rgba(75,192,192,0.4)",
          //枠線の色
          borderColor: "rgba(75,192,192,1)",
          //グラフのデータ
          data: [<? $loop=0; if($arr_day[$loop]=="") { echo "0";} else {echo $arr_day[$loop];} $loop++;?>, <? if($arr_day[$loop]=="") { echo "0";} else {echo $arr_day[$loop];} $loop++;?>, <? if($arr_day[$loop]=="") { echo "0";} else {echo $arr_day[$loop];} $loop++;?>, <? if($arr_day[$loop]=="") { echo "0";} else {echo $arr_day[$loop];} $loop++;?>, <? if($arr_day[$loop]=="") { echo "0";} else {echo $arr_day[$loop];} $loop++;?>, <? if($arr_day[$loop]=="") { echo "0";} else {echo $arr_day[$loop];} $loop++;?>]
      }]
  },

});
</script>


<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_end.php'; ?>