<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
    $common_shop = new CommonShop();

	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/config.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_start.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_head.php';


    foreach ( $_GET as $key => $value ) {
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_shop_check();
    $shop_id = $_SESSION['shop_id'];



    if($shop_blog_id!="")
    {
        $arr_db_field = array("shop_blog_id", "shop_blog_title", "shop_blog_comment", "img_1");
        $arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open", "regi_date", "up_date"));
            
        $sql = "SELECT shop_blog_id, ";
        foreach($arr_db_field as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM shop_blog where shop_id='".$shop_id."' and shop_blog_id='".$shop_blog_id."'";
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            foreach($arr_db_field as $val)
            {
                $$val = $db_result[0][$val];
            }
        }

        $shop_blog_comment = htmlspecialchars_decode($shop_blog_comment);
    }

    //投稿可能数
    $shop_blog_possible = 1;
    $sql = "SELECT c.blog_count FROM shop s inner join cate_shop c on s.shop_view_level=c.cate_shop_id where s.shop_id='".$shop_id."' ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $shop_blog_possible = $db_result[0]["blog_count"];
    }

    //登録可能件数
    $blog_count = 0;
    $sql = "SELECT blog_count FROM shop_blog_count where shop_id='".$shop_id."' and regi_date='".date("Y-m-d")."' ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $blog_count = $db_result["0"]["blog_count"];
    }
?>


<script type="text/javascript">
    $( function () {

        //イメージクリック
        $( '#insert_img_1' ).click( function () {
            $( ".note-editable" ).append( "<img src='" + $( this ).attr( "src" ) + "'>" );
        } );

        $( '#form_confirm' ).click( function () {
            $( "#shop_blog_comment" ).val( $( ".note-editable" ).html() );
            err_default = "";
            err_check_count = 0;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";

            err_check_count += check_input( "shop_blog_title" );

            if ( err_check_count != 0 ) {
                alert( "入力に不備があります" );
                return false;
            } else {
                $( '#form_confirm' ).submit();
                //$( '#form_confirm', "body" ).submit();
                return true;
            }
        } );

        function check_input( $str ) {
            $( "#err_" + $str ).html( err_default );
            $( "#" + $str ).css( background, bgcolor_default );

            if ( $( '#' + $str ).val() == "" ) {
                err = "<span class='errorForm'>正しく入力してください。</span>";
                $( "#err_" + $str ).html( err );
                $( "#" + $str ).css( background, bgcolor_err );

                return 1;
            }
            return 0;
        }

        function check_input_id( $str ) {
            $( "#err_" + $str ).html( err_default );
            $( "#" + $str ).css( background, bgcolor_default );

            if ( $( '#' + $str ).val() == "" ) {
                err = "<span class='errorForm'>正しく入力してください。</span>";
                $( "#err_" + $str ).html( err );
                $( "#" + $str ).css( background, bgcolor_err );

                return 1;
            } else if ( checkID( $( '#' + $str ).val() ) == false ) {
                err = "<span class='errorForm'>英数半角で入力してください。</span>";
                $( "#err_" + $str ).html( err );
                $( "#" + $str ).css( background, bgcolor_err );

                return 1;
            } else if ( $( '#' + $str ).val().length < 3 ) {
                err = "<span class='errorForm'>3文字以上で入力してください。</span>";
                $( "#err_" + $str ).html( err );
                $( "#" + $str ).css( background, bgcolor_err );

                return 1;
            }
            return 0;
        }

    } );

    $(function() {
        //チェックリストを一括削除
      $('#check_delete').click(function() {
        if(confirm('削除しますか？'))
        {
            check_del_list = $('[class="check_del"]:checked').map(function(){
                //$(this)でjQueryオブジェクトが取得できる。val()で値をvalue値を取得。
                return $(this).val();
            }).get().join('&matome_id[]=');
            document.location.href = './matome_del.php?matome_id[]='+check_del_list;
        }
      });
            
    });
    //-->
</script>

<script language="javascript"> 
    function fnChangeSel(j) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './blog_del.php?shop_blog_id='+j;
        } 
    }

    function fnCopy(j) { 
        var result = confirm('コピーしますか？'); 
        if(result){ 
            document.location.href = './blog_copy.php?shop_blog_id='+j;
        } 
    }

</script>

<style type="text/css">
    .panel-body p {line-height:1;}

    /* ファイルアップロード削除 */
    .note-group-select-from-files {
        display:none;
    }
</style>

<!-- Page content -->
<div id="page-content">

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_headnavi.php'; ?>

   
    <!-- パンクズリスト -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="">管理画面</a></li>
        <li>ブログ投稿</li>
    </ul>
     <!-- パンクズリスト -->
   
    <!-- Dashboard 2 Content -->
    <div class="row">
    
    
    
    <div class="col-md-12">
     <!-- CKEditor Block -->
    <div class="block">
        <!-- CKEditor Title -->
        <div class="block-title">
            <h2><strong><i class="fa fa-pencil"></i>ブログを投稿してお店をPRしてみましょう</strong></h2>
        </div>
        <!-- END CKEditor Title -->

        <!-- CKEditor Content -->
        <form action="./blog_save.php" method="POST" name="form_write" id="form_regist" enctype="multipart/form-data" class="form-horizontal form-bordered">
            <? $var = "shop_blog_id"; ?>
            <input type="hidden" name="<? echo $var;?>" value='<? echo $$var;?>'>
            <? $var = "shop_blog_comment"; ?>
            <input type="hidden" name="<? echo $var;?>" id="<? echo $var;?>" value='<? echo $$var;?>'>

            <fieldset>
                <legend>日付の変わる0時で投稿回数がリセットされます</legend>
                	<div class="form-group">
                    	<label class="col-md-2 control-label" for="example-text-input">投稿回数</label>
                    	<div class="col-md-6">
                       	
                        	<div class="progress progress-striped active bertopspace">
                    			<div id="progress-bar-wizard" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" class="progress-first" style="width:<? echo (($blog_count/$shop_blog_possible)*100)+1;?>%;"><? echo $blog_count;?>/<? echo $shop_blog_possible;?>回投稿</div>
                    			<!--プログレスバー長さ設定/app_manager/js/pages/formsWizard.js　-->
                			</div>
              
                  		
                   		</div>
                    </div>
            </fieldset>
                    
            <fieldset>                     

            	<div class="form-group">
                	<label class="col-md-2 control-label" for="example-text-input">ブログタイトル</label>
                        		
                	<div class="col-md-6">
                        <? $var = "shop_blog_title";?>
                    	<input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" class="form-control down" placeholder="タイトル" maxlength="30">
                       	<span class="help-block">タイトルの文字数は最大30文字までです。</span>
                    </div>
                </div>
                    		
                <div class="form-group">
                	<label class="col-md-2 control-label" for="example-text-input">サムネイル画像</label>
                        		
                	<div class="col-md-5">
                	   <div class="btn btn-default fileinput-button">
							<div class="firefox_hack">
							<span>画像をアップ</span>
							<!-- The file input field used as target for the file upload widget -->
                            <? $var = "img_1";?>
                            <input type="file" id="upload1" name="<? echo $var;?>" accept="image/*">
                            <input type="hidden" name="text_<?=$var;?>" value="<?=$$var;?>">
                            </div>
						</div>
                    	<span class="help-block">画像は3：2の比率でサムネイル化されます。<br>
            			<a href="">→画像をカット3：2にカットしたいなら</a></span>
            			<br>
						<br>
                			<!-- アップロードされたファイル -->
						<p id="preview1" class="preview-img"></p>
                        <? if($$var!="") { ?>
                        <?
                            echo "<img src='/".global_shop_blog_dir.$shop_blog_id."/".$$var."?d=".date(his)."' id='insert_".$var."' width='90'>";
                        ?>
                        <? } ?>
			

                    </div>
                </div>
                
                
             
                
                
                
                <div class="form-group">
                    <label class="col-md-2 control-label" for="example-text-input">ブログ本文</label>
    
    				<div class="col-md-5">
						<div class="summernote">
                            <? echo $shop_blog_comment;?>
						</div>
                            <? $var = "form_confirm";?>
							<button type="submit" id="<? echo $var;?>" class="btn btn-sm btn-primary"><i class="fa fa-ok"></i>投稿する</button>
							
							<script type="text/javascript">
								$( function () {
									$( '.summernote' ).summernote( {
										width: 755,
										height: 300,
										disableDragAndDrop: true,
										toolbar: [
											['style', ['style']],
											['font', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
											['fontsize', ['fontsize']],
											['color', ['color']],
											['para', ['ul', 'ol', 'paragraph']],
											['height', ['height']],
											['table', ['table']],
											['insert', ['link', 'picture', 'video', 'hr']],
											['view', ['codeview']],
											['help', ['help']]
												],
										defaultFontName: 'Meiryo',
										fontNames: ["YuGothic","Yu Gothic","Hiragino Kaku Gothic Pro","sans-serif", "Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana"],
										fontSizes: ['11', '12', '13', '16', '18', '20', '24', '28'],
										callbacks: {
											onImageUpload: function ( files, editor, welEditable ) {
												console.log( 'image upload:', files );
												$.each( files, function ( i, val ) {
													sendFile( files[ i ], editor, welEditable );
												} );
											},
										}
									} );

									function sendFile( file, editor, welEditable ) {
										data = new FormData();
										data.append( "file", file );
										$.ajax( {
											url: "saveimage.php", // image 저장 소스
											data: data,
											cache: false,
											contentType: false,
											processData: false,
											type: 'POST',
											success: function ( data ) {
												//       alert(data);
												var image = $( '<img style="max-width:100%;">' ).attr( 'src', '' + data ); // 에디터에 img 태그로 저장을 하기 위함
												$( '.summernote' ).summernote( "insertNode", image[ 0 ] ); // summernote 에디터에 img 태그를 보여줌
												//       editor.insertImage(welEditable, data);
											},
											error: function ( jqXHR, textStatus, errorThrown ) {
												console.log( textStatus + " " + errorThrown );
											}
										} );
									}
									/*
$('form').on('submit', function (e) {
e.preventDefault();
//     alert($('.summernote').summernote('code'));
//     alert($('.summernote').val());
});
*/
								} );

							</script>

					</div>
					
				</div>
	<!----->
                


            </fieldset>
            

        </form>
        <!-- END CKEditor Content -->
    </div>
    <!-- END CKEditor Block -->
    </div>
    
    

    
    <div class="col-md-12">
    
    <!-- Responsive Full Block -->
    <div class="block">
        <!-- Responsive Full Title -->
        <div class="block-title">
            <h2><strong>ブログ投稿一覧</strong></h2>
        </div>
        <!-- END Responsive Full Title -->

        
        <p>過去に投稿したブログは最大で10件まで履歴として残ります。<br>
           ※操作にある【再度投稿】は同じ内容のブログを再びアップすることが出来ます。<br>
           その場合、その日の投稿数は減ってしまうのでご注意ください。</p>
<?php
$view_count=10;   // List count
$offset=0;

if(!$page)
{
    $page=1;
}
Else
{
    $offset=$view_count*($page-1);
}

$where = " and shop_id = '".$shop_id."' ";
if($s_keyword != "")
{
    $where .= " and (shop_blog_title like '%".$s_keyword."%') ";
}
if($s_flag_open != "")
{
    $where .= " and flag_open='".$s_flag_open."' ";
}

//合計
$sql_count = "SELECT count(shop_blog_id) as all_count FROM shop_blog where 1 ".$where ;

$db_result_count = $common_dao->db_query_bind($sql_count);
if($db_result_count)
{
    $all_count = $db_result_count[0]["all_count"];
}

//リスト表示
$arr_db_field = array("shop_blog_id", "shop_blog_title", "img_1", "count_comment");
$arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open", "regi_date", "up_date"));

$sql = "SELECT ";
foreach($arr_db_field as $val)
{
    $sql .= $val.", ";
}
$sql .= " 1 FROM shop_blog where 1 ".$where ;
if($order_name != "")
{
    $sql .= " order by ".$order_name." ".$order;
}
else
{
    $sql .= " order by view_level, up_date desc";
}
$sql .= " limit $offset,$view_count";

$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
    $inner_count = count($db_result);
?>
        <div class="table-responsive">
            <table class="table table-vcenter table-striped">
                <thead>
                    <tr>
                        <th style="width: 100px;" class="text-center"><i class="fa fa-picture-o" aria-hidden="true"></i></th>
                        <th class="text-center">投稿日</th>
                        <th class="text-center">時間</th>
                        <th class="text-center">タイトル</th>
                        <th class="text-center">回数/日</th>
                        <th style="width: 150px;" class="text-center">操作</th>
                    </tr>
                </thead>
                <tbody>

<?php
    for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[$db_loop][$val];
        }
?>
                    <tr>
                        <td class="text-center">
                        <div class="blog_blok">
                        <? if($img_1!="") { ?>
                        <img src="<? echo "/".global_shop_blog_dir.$shop_blog_id."/".$img_1;?>" width=90>
                        <? } ?>
                        </td>
                        </div>
                        <td class="text-center"><? echo date('Y'.'/'.'m'.'/'.'d', strtotime($regi_date));?></td>
                        <td class="text-center"><? echo date('H'.'時'.'i分投稿', strtotime($regi_date));?></td>
                        <td class="text-center"><? echo $shop_blog_title?></td>
                        <td class="text-center"><p class="label label-warning"><? echo $count_comment;?></p></td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                <a href="./blog.php?shop_blog_id=<?php echo $shop_blog_id;?>" data-toggle="tooltip" title="編集" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                                <a href="#" onClick='fnChangeSel("<?php echo $shop_blog_id;?>");' data-toggle="tooltip" title="削除" class="btn btn-danger"><i class="fa fa-times"></i></a>
                                <a href="#" onClick='fnCopy("<?php echo $shop_blog_id;?>");' data-toggle="tooltip" title="再度投稿" class="btn btn-default"><i class="fa fa-reply-all"></i></a>
                            </div>
                        </td>
                    </tr>

<?php
        }
    }
?>

                </tbody>
            </table>


    
        </div>
        <!-- END Responsive Full Content -->
    </div>
    <!-- END Responsive Full Block -->

</div>


    </div>
    <!-- END Dashboard 2 Content -->
    
    
    
            	<div class="text-right">
                    <ul class="pagination">
                        <li class="disabled"><a href="">&laquo;</a></li>
                        <li class="active"><a href="">1</a></li>
                        <li><a href="">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href="">4</a></li>
                        <li><a href="">&raquo;</a></li>
                    </ul>
                </div>
    
</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_footer.php'; ?>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="/app_manager/js/helpers/excanvas.min.js"></script><![endif]-->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_scripts.php'; ?>

<!-- プログレスバー-->
<script src="/app_manager/js/pages/formsWizard.js"></script>
<script>$(function(){ FormsWizard.init(); });</script>

<!-- 文字カウント-->
<script src="/app_manager/js/pages/bootstrap-maxlength.js"></script>
<script>
$(document).ready(function () {
  $('.down').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>

<!--アップロードサムネイル関係プラグイン-->
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<script src="/app_manager/js/tool/fileupload/jquery.iframe-transport.js"></script>
<!-- メインファイル -->
<script src="/app_manager/js/tool/fileupload/jquery.fileupload.js"></script>
<!-- プログレスバー-->
<script src="/app_manager/js/tool/fileupload/jquery.fileupload-process.js"></script>
<!-- アップロードサムネイル-->
<script src="/app_manager/js/tool/thumbs/thumbs.js"></script>
<script>
$(function() {
    // jQuery Upload Thumbs 
    $('#upload1').uploadThumbs({
        position : '#preview1',   // any: arbitrarily jquery selector
    });

});
</script>

<script type="text/javascript" src="/app_manager/js/tool/resize/imgLiquid-min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(".blog_blok").imgLiquid({
    });
});
</script>


<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_end.php'; ?>




