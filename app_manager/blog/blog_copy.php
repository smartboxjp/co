<?php
    //error_reporting(0);
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    $common_image = new CommonImage(); //画像
    

?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php   

    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
    
    $common_connect -> Fn_shop_check();
    $shop_id = $_SESSION['shop_id'];
    $datetime = date("Y/m/d H:i:s");

    if($shop_blog_id == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力してください。");
    }

    //投稿可能数
    $shop_blog_possible = 1;
    $sql = "SELECT c.blog_count FROM shop s inner join cate_shop c on s.shop_view_level=c.cate_shop_id where s.shop_id='".$shop_id."' ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $shop_blog_possible = $db_result[0]["blog_count"];
    }

    //登録可能件数
    $blog_count = 0;
    $sql = "SELECT blog_count FROM shop_blog_count where shop_id='".$shop_id."' and regi_date='".date("Y-m-d")."' ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $blog_count = $db_result["0"]["blog_count"];
    }

    if($blog_count==0)
    {
        $db_insert = "insert into shop_blog_count ( shop_id, regi_date, blog_count ) values ";
        $db_insert .= " ( '".$shop_id."', '".date("Y-m-d")."', '0') ";
        $db_result = $common_dao->db_update($db_insert);
    }
    if($blog_count>=$shop_blog_possible)
    { 
        $common_connect -> Fn_javascript_back($shop_blog_possible."個まで登録可能です。。");
    }
    else
    {
        $db_up = "update shop_blog_count set blog_count=blog_count+1 where shop_id='".$shop_id."' and regi_date='".date("Y-m-d")."' ";
        $db_result = $common_dao->db_update($db_up);
    }

    $arr_db_field = array("shop_blog_id", "shop_blog_title", "shop_blog_comment", "img_1");
    $arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open"));
        
    $sql = "SELECT shop_blog_id, ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM shop_blog where shop_id='".$shop_id."' and shop_blog_id='".$shop_blog_id."'";
    
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[0][$val];
        }
    }
    

    $count_comment = ($blog_count+1)."/".$shop_blog_possible;
    $arr_db_field = array("shop_blog_title", "shop_blog_comment", "img_1", "count_comment");
    $arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open"));
    $db_insert = "insert into shop_blog ( shop_blog_id, ";
    foreach($arr_db_field as $key=>$value)
    {
        $db_insert .= $value.", ";
    }
    $db_insert .= " shop_id, regi_date, up_date ) values ( '', ";
    foreach($arr_db_field as $key=>$value)
    {
        $db_insert .= "'".$$value."', ";
    }
    $db_insert .= " '".$shop_id."', '".$datetime."', '".$datetime."')  ";
    $db_result = $common_dao->db_update($db_insert);

    $sql = "select last_insert_id() as last_id " ;
    $db_result = $common_dao->db_query_bind($sql);
    $shop_blog_id_new = $db_result[0]["last_id"];

    
    //Folder生成
    $save_dir = $global_path.global_shop_blog_dir.$shop_blog_id_new."/";
    $common_image -> create_folder ($save_dir);

    
    if(file_exists($_SERVER['DOCUMENT_ROOT']."/".global_shop_blog_dir.$shop_blog_id."/".$img_1))
    {
        copy($_SERVER['DOCUMENT_ROOT']."/".global_shop_blog_dir.$shop_blog_id."/".$img_1, $save_dir."/".$img_1);

    }

    
    $common_connect-> Fn_javascript_move("登録・修正しました", "blog.php?shop_blog_id=".$shop_blog_id);
?>
</body>
</html>