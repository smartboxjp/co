        <!-- お店メイン画像 -->

		<div class="block full">
			<div class="block-title">
				<div class="block-options pull-right">
					<a href="/app_manager/shop/shop_mainimg.php" class="btn btn-sm btn-alt btn-default" data-toggle="tooltip" title="画像変更"><i class="fa fa-picture-o"></i></a>
				</div>
				<h2>現在登録されてるメイン画像</h2>
			</div>

			<div class="widget">
				<div class="widget-advanced widget-advanced-alt">
					 <div class="widget-header text-center">
					 	<div class="mainimgbox"><div class="animation-container"><img src="/app_manager/img/test/shop1.jpg"></div></div>
					</div>
				</div>
			</div>
			
			<hr>
            <p class="text-muted text-center">3/5枚登録済み</p>

		</div>
            <!-- お店メイン画像-->



 <!-- 口コミボックス -->
            <div class="block full">
                <!-- Twitter Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="" class="btn btn-sm btn-alt btn-default" data-toggle="tooltip" title="口コミ一覧"><i class="fa fa-share"></i></a>
                    </div>
                    <h2>お店の最新口コミ </h2>
                </div>
                <!-- END Twitter Title -->


				<ul class="media-list">
					<li class="media">
						<a href="page_ready_user_profile.php" class="pull-left">
                            <img src="/app_manager/img/test/123.gif" alt="Avatar" class="img-circle" width="60px">
                        </a>
					

						<div class="media-body">
							<span class="text-muted pull-right"><small><em>2017/08/25投稿</em></small></span>
							<a href="/app_manager/voise/details.php"><strong>りずりさ</strong></a>
							                      
                                <div class="text-warning">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                    <span class="text-danger point">【3.17点】</span>
								</div>
                            
							<p class="voisetitle"><a href="http://www.cossot.com/app_manager/voise/details.php">私は好き！</a>
							
							</p>
						</div>
						<hr>
					</li>
                
                	<li class="media">
						<a href="page_ready_user_profile.php" class="pull-left">
                            <img src="/app_manager/img/test/123.gif" alt="Avatar" class="img-circle" width="60px">
                        </a>
					

						<div class="media-body">
							<span class="text-muted pull-right"><small><em>2017/08/25投稿</em></small></span>
							<a href="/app_manager/voise/details.php"><strong>りずりさ</strong></a>
							                      
                                <div class="text-warning">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                    <span class="text-danger point">【3.17点】</span>
								</div>
                            
							<p class="voisetitle"><a href="http://www.cossot.com/app_manager/voise/details.php">私にとってはこのお店は最悪でしたしもう二度と働きたいなんて思えな</a>
							</p>
						</div>
						<hr>
					</li>
                
                	<li class="media">
						<a href="page_ready_user_profile.php" class="pull-left">
                            <img src="/app_manager/img/test/123.gif" alt="Avatar" class="img-circle" width="60px">
                        </a>
					

						<div class="media-body">
							<span class="text-muted pull-right"><small><em>2017/08/25投稿</em></small></span>
							<a href="/app_manager/voise/details.php"><strong>りずりさ</strong></a>
							                      
                                <div class="text-warning">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                    <span class="text-danger point">【3.17点】</span>
								</div>
                            
							<p class="voisetitle"><a href="http://www.cossot.com/app_manager/voise/details.php">私にとってはこのお店は最悪でした、、。</a>
							</p>
						</div>
						<hr>
					</li>

               		<p class="text-muted text-center">全17口コミ</p>
                </ul>
                <!-- END Twitter Content -->
                
                
            </div>
   <!-- 口コミボックス -->

  <!-- 関連画像ここまで -->
            <div class="block">
                <!-- Photos Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="" class="btn btn-sm btn-alt btn-default" data-toggle="tooltip" title="画像一覧"><i class="fa fa-share"></i></a>
                    </div>
                    <h2>投稿された関連画像</h2>
                </div>
                <!-- END Photos Title -->

                <!-- Photos Content -->
                <div class="gallery" data-toggle="lightbox-gallery">
                    <div class="row">
                        <div class="col-xs-6 col-sm-4">
                            <a href="/kanto/user/img/test/imgtest1.jpg" class="gallery-link" title="Image Info">
                                <div class="samimg-dash"><img src="/kanto/user/img/test/imgtest1.jpg"></div>
                            </a>
                            <div class="imguser"><span class="text-muted">お店から投稿</span></div>
                        </div>
                        <div class="col-xs-6 col-sm-4">
                            <a href="/kanto/user/img/test/imgtest1.jpg" class="gallery-link" title="Image Info">
                                <img src="/kanto/user/img/test/imgtest3.jpg">
                            </a>
                            <div class="imguser"><a href="" target="_blank">ユーザー名</a></div>
                        </div>
                        <div class="col-xs-6 col-sm-4">
                            <a href="/kanto/user/img/test/imgtest1.jpg" class="gallery-link" title="Image Info">
                                <div class="samimg-dash"><img src="/kanto/user/img/test/imgtest2.jpeg"></div>
                            </a>
                        </div>
                        <div class="imguser"><a href="" target="_blank">ユーザー名</a></div>
                    </div>
                </div>
                
                <hr>
                <p class="text-muted text-center">全17画像</p>
            </div>
            <!-- 関連画像ここまで -->


<!-- Info  -->
            <div class="block">
                <!-- Info Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="" class="btn btn-sm btn-alt btn-default" data-toggle="tooltip" title="お知らせ一覧"><i class="fa fa-share"></i></a>
                    </div>
                    <h2>コソットからのお知らせ</h2>
                </div>
                <!-- END Info Title -->

                <!-- Info Content -->
                <table class="table table-borderless table-striped">
                    <tbody>
                        <tr>
                            <td style="width: 20%;">2016/08/25</td>
                            <td><a href="/app_manager/dashboard/info.php">関西版がオープンしました！</a></td>
                        </tr>
                        <tr>
                            <td>2016/08/25</td>
                            <td>
								<a href="/app_manager/dashboard/info.php">今なら有料切り替えが10%OFF</a>
                            </td>
                        </tr>
                        <tr>
                            <td>2016/08/25</td>
                            <td>
				<a href="/app_manager/dashboard/info.php">今なら有料切り替えが10%OFF</a>
                            </td>
                        </tr>
                        <tr>
                            <td>2016/08/25</td>
                            <td>
				<a href="/app_manager/dashboard/info.php">今なら有料切り替えが10%OFF</a>
                            </td>
                        </tr>
                        <tr>
                            <td>2016/08/25</td>
                            <td>
				<a href="/app_manager/dashboard/info.php">今なら有料切り替えが10%OFF</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- END Info Content -->
                
                <hr>
                <p class="text-muted text-center">全14件</p>
            </div>
            <!-- END Info  -->



	<!-- tool Block -->
            <div class="block">
                <!-- Share Title -->
                <div class="block-title">
                    <h2>ツール</h2>
                </div>
                <!-- END Share Title -->

                <!-- Share Content -->
                <div class="block-section text-center">
                    <div class="btn-group">
                        <a href="" class="btn btn-default" data-toggle="tooltip" title="画像カット"><i class="fa fa-scissors"></i></a>
                        <a href="" class="btn btn-default" data-toggle="tooltip" title="Twitter"><i class="si si-twitter"></i></a>
                        <a href="" class="btn btn-default" data-toggle="tooltip" title="Google Plus"><i class="si si-google_plus"></i></a>
                        <a href="" class="btn btn-default" data-toggle="tooltip" title="Pinterest"><i class="si si-pinterest"></i></a>
                    </div>
                </div>
                <!-- END Share Content -->
                
                
            </div>
            <!-- END Share Block -->