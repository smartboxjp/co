<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/config.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_start.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_head.php';
?>


<!-- Page content -->
<div id="page-content">

	<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_headnavi.php'; ?>

	<!-- Dashboard 2 Content -->
	<div class="row">


		<!--左-->
		<div class="col-md-2">

			<?	
				require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/dashboard/common/left_deta.php";
			?>

		</div>



		<!--真ん中　タイムライン---->
		<div class="col-md-6">



			<div class="block">
				<div class="block-title">
					<div class="block-options pull-right">
						<a href="/app_manager/tutorial/index.php" class="btn btn-sm btn-alt btn-default" data-toggle="tooltip" title="MYニュースとは"><i class="fa fa-rss"></i></a>
					</div>
					<h2>MYニュース</h2><span class="text-muted">最新更新時間　8時25分</span>
				</div>
				<div class="fast-content">
					お店に関係する情報が自動的に更新され表示されます。
					<div class="refresh-icon">
						<a href="" class="text-muted"><i class="fa fa-angle-double-down animation-pulse"></i></a>
					</div>


				</div>
			</div>

		

					<div class="block">
						<!-- 自店舗の点数変化 -->
						<div class="block-title backcro">
							<div class="block-options pull-right">
								<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-hide"><i class="fa fa-times"></i></a>
							</div>

						</div>

						<div class="block-content">
							<div class="timeline-avatar-shop">
								<img src="/app_manager/img/icon/mychart.png" alt="Avatar" class="pull-left" width="50px">
								<h4>お店の平均点が変化</h4>
								<span class="text-muted menutime"><i class="fa fa-clock-o"></i>今のニュース</span>
							</div>
							<p class="tilinesabtitle"><i class="fa fa-exclamation-circle"></i>あなたのお店の平均点数が、新しい口コミ投稿により変更されました。</p>

							<div class="timelinecontent">
								お店の平均点数が<span class="text-danger point">【3.15点】→【3.20点】</span>と変更されました。
							</div>

						</div>
						<!-- 自店舗の点数変化 -->
					</div>

					<div class="block">
						<!-- 口コミの投稿 -->
						<div class="block-title backcro">
							<div class="block-options pull-right">
								<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-hide"><i class="fa fa-times"></i></a>
							</div>

						</div>

						<div class="block-content">
							<div class="timeline-avatar-shop">
								<img src="/app_manager/img/icon/comment.png" alt="Avatar" class="pull-left" width="50px">
								<h4>お店に口コミが投稿されました。</h4>
								<span class="text-muted menutime"><i class="fa fa-clock-o"></i>30分以内のニュース</span>
							</div>
							<p class="tilinesabtitle"><i class="fa fa-exclamation-circle"></i>あなたのお店に口コミが投稿されました。</p>

							<div class="timelinecontent">
								<a href="page_ready_user_profile.php" class="pull-left timelinenewuser-avatar">
                        	<img src="/app_manager/img/test/123.gif" class="img-circle" alt="Avatar" width="60px">
                       	</a>
							

								<div class="pull-left">
									<a href="/app_manager/voise/details.php"><strong>ユーザーの名前</strong></a>
									<div class="text-warning">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star-half-o"></i>
										<span class="text-danger point">【3.17点】</span>
									</div>

									<div class="timelinecontent_text"><a href="/app_manager/voise/details.php">私にはこのお店は合わないと思いました。
							</a>
									</div>

								</div>
								<div class="clear"></div>
							</div>
						</div>
						<!-- 口コミの投稿 -->
					</div>
					
					<div class="block">
						<!-- 画像の投稿 -->
						<div class="block-title backcro">
							<div class="block-options pull-right">
								<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-hide"><i class="fa fa-times"></i></a>
							</div>

						</div>

						<div class="block-content">
							<div class="timeline-avatar-shop">
								<img src="/app_manager/img/icon/img.png" alt="Avatar" class="pull-left" width="50px">
								<h4>お店に関連画像が投稿されました。</h4>
								<span class="text-muted menutime"><i class="fa fa-clock-o"></i>1時間以内のニュース</span>
							</div>
							<p class="tilinesabtitle"><i class="fa fa-exclamation-circle"></i>あなたのお店に関連画像が投稿されました。</p>

							<div class="timelinecontent" data-toggle="lightbox-gallery">
								<div class="timeline_imgbox">
									<a href="/kanto/user/img/test/imgtest4.jpeg" class="gallery-link" title="コメントが最大で10文字">
                                					<img src="/kanto/user/img/test/imgtest4.jpeg">
                            					</a>
								

									<div class="imguser"><a href="" target="_blank"><strong>ユーザー名表示</strong></a>
									</div>
								</div>
							</div>
						</div>
						<!-- 画像の投稿 -->
					</div>


					<div class="block">
						<!-- 新規登録店 -->
						<div class="block-title backcro">
							<div class="block-options pull-right">
								<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-hide"><i class="fa fa-times"></i></a>
							</div>

						</div>

						<div class="block-content">
							<div class="timeline-avatar-shop">
								<img src="/app_manager/img/icon/shop.png" alt="Avatar" class="pull-left" width="50px">
								<h4>新しいお店</h4>
								<span class="text-muted menutime"><i class="fa fa-clock-o"></i>2時間以内のニュース</span>
							</div>
							<p class="tilinesabtitle"><i class="fa fa-exclamation-circle"></i>同じ地域に新しいお店が掲載されました。</p>

							<div class="timelinecontent">
								<a href="page_ready_user_profile.php" class="pull-left timelinenewshop-avatar">
                            <img src="/app_manager/img/test/shop.jpg" alt="Avatar" class="img-circle" width="60px">
                        </a>
							

								<div class="pull-left">
									<a href="/app_manager/voise/details.php"><strong>お店の名前が表示されます</strong></a>
									<div class="text-warning">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star-half-o"></i>
										<span class="text-danger point">【-.--点】</span>
									</div>

									<div class="text-muted"><small>(デリバリーヘルス)</small>
									</div>
								</div>
								<div class="clear"></div>
							</div>

						</div>
						<!-- 新規登録店 -->
					</div>

					<div class="block">
						<!-- 他のお店の点数が変化 -->
						<div class="block-title backcro">
							<div class="block-options pull-right">
								<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-hide"><i class="fa fa-times"></i></a>
							</div>

						</div>

						<div class="block-content">
							<div class="timeline-avatar-shop">
								<img src="/app_manager/img/icon/shopchart.png" alt="Avatar" class="pull-left" width="50px">
								<h4>他のお店の点数が変化</h4>
								<span class="text-muted menutime"><i class="fa fa-clock-o"></i>3時間以内のニュース</span>
							</div>
							<p class="tilinesabtitle"><i class="fa fa-exclamation-circle"></i>同じ地域のお店の評価に変動がありました。</p>

							<div class="timelinecontent">
								<a href="page_ready_user_profile.php" class="pull-left timelinenewshop-avatar">
                        		<img src="/app_manager/img/test/shop.jpg" alt="Avatar" class="img-circle" width="60px">
                        	</a>
							

								<a href="/app_manager/voise/details.php"><strong>お店の名前が表示されます</strong></a>
								<div class="timelinecontent_text">
									同じエリアの店名が表示..の平均点数が<span class="text-danger point">【3.15】→【3.20】</span>と変更されました。
								</div>

								<div class="clear"></div>
							</div>

						</div>
						<!-- 他のお店の点数が変化 -->
					</div>

					<div class="block">
						<!-- メールがありました -->
						<div class="block-title backcro">
							<div class="block-options pull-right">
								<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-hide"><i class="fa fa-times"></i></a>
							</div>

						</div>

						<div class="block-content">
							<div class="timeline-avatar-shop">
								<img src="/app_manager/img/icon/mail.png" alt="Avatar" class="pull-left" width="50px">
								<h4>メールが来てます</h4>
								<span class="text-muted menutime"><i class="fa fa-clock-o"></i>半日以内のニュース</span>
							</div>
							<p class="tilinesabtitle"><i class="fa fa-exclamation-circle"></i>会員ユーザーからメールが届いてます。</p>

							<div class="timelinecontent">
								<a href="page_ready_user_profile.php" class="pull-left timelinenewuser-avatar">
                        		<img src="/app_manager/img/test/123.gif" alt="Avatar" class="img-circle" width="50px">
                        	</a>
							
								<div class="mailtext-rink">
									<a href="/app_manager/mail/detail-user.php"><strong>ユーザーネーム</strong>さんからメールが届いてます。</a>
									<p>
										※早めの返信をお願いします。
									</p>
								</div>

								<div class="clear"></div>
							</div>

						</div>
						<!-- メールがありました -->
					</div>

					<div class="block">
						<!-- メールがありました コソットから -->
						<div class="block-title backcro">
							<div class="block-options pull-right">
								<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-hide"><i class="fa fa-times"></i></a>
							</div>

						</div>

						<div class="block-content">
							<div class="timeline-avatar-shop">
								<img src="/app_manager/img/icon/mail.png" alt="Avatar" class="pull-left" width="50px">
								<h4>メールが来てます</h4>
								<span class="text-muted menutime"><i class="fa fa-clock-o"></i>本日中のニュース</span>
							</div>
							<p class="tilinesabtitle"><i class="fa fa-exclamation-circle"></i>コソットからメールが届いてます。</p>

							<div class="timelinecontent">
								<a href="page_ready_user_profile.php" class="pull-left timelinenewuser-avatar">
                        		<img src="/app_manager/img/cossoticon.png" alt="Avatar" class="img-circle" width="50px">
                        	</a>
							
								<div class="mailtext-rink">
									<a href="/app_manager/mail/detail.php"><strong>コソット</strong>からメールが届いてます。</a>
									<p>
										※コソットへは何度も返信可能です。
									</p>
								</div>

								<div class="clear"></div>
							</div>

						</div>
						<!-- メールがありました コソットから -->
					</div>

					<div class="block">
						<!-- インフォメーション -->
						<div class="block-title backcro">
							<div class="block-options pull-right">
								<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-hide"><i class="fa fa-times"></i></a>
							</div>

						</div>

						<div class="block-content">
							<div class="timeline-avatar">
								<img src="/app_manager/img/icon/info.png" alt="Avatar" class="pull-left" width="50px">
								<h4>お知らせ</h4>
								<span class="text-muted menutime"><i class="fa fa-clock-o"></i>昨日のニュース</span>
							</div>
							<p class="tilinesabtitle"><i class="fa fa-exclamation-circle"></i>コソットからお知らせです。</p>

							<div class="timelinecontent">
								<a href="/app_manager/dashboard/info.php">新しく関西版がオープンしました！</a>
							</div>

						</div>
						<!-- インフォメーション -->
					</div>


					<div class="block">
						<!-- PR※画像　マスター管理で追加 -->
						<div class="block-content">
							<p class="tilinesabtitle"><i class="fa fa-exclamation-circle"></i>PR</p>

							<div class="prcoment">
								コメントが入力されます。コメントが入力されます。
							</div>
							<div class="prcontent">
								<a href="" target="_blank"><img src="/app_manager/img/test/prtest2.jpg"></a>
							</div>


						</div>

					</div>

					<div class="block">
						<!-- 新規登録ユーザー -->
						<div class="block-title backcro">
							<div class="block-options pull-right">
								<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-hide"><i class="fa fa-times"></i></a>
							</div>

						</div>

						<div class="block-content">
							<div class="timeline-avatar-shop">
								<img src="/app_manager/img/icon/user.png" alt="Avatar" class="pull-left" width="50px">
								<h4>新しいユーザー</h4>
								<span class="text-muted menutime"><i class="fa fa-clock-o"></i>一昨日のニュース</span>
							</div>
							<p class="tilinesabtitle"><i class="fa fa-exclamation-circle"></i>同じ地域に新しくユーザーが登録されました。</p>

							<div class="timelinecontent">

								<a href="page_ready_user_profile.php" class="pull-left timelinenewuser-avatar">
                        		<img src="/app_manager/img/test/123.gif" alt="Avatar" class="img-circle" width="50px">
                        	</a>
							
								<a href="/app_manager/voise/details.php"><strong>ユーザーの名前</strong></a>
								<div class="text-muted">
									エリア:【東京】
								</div>

								<div class="text-muted">
									登録日:【2015/08/25】
								</div>

								<div class="clear"></div>
							</div>


						</div>

						<!-- 新規登録ユーザー -->
					</div>

					<div class="block">
						<!-- ブログ投稿-->
						<div class="block-title backcro">
							<div class="block-options pull-right">
								<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-hide"><i class="fa fa-times"></i></a>
							</div>

						</div>

						<div class="block-content">
							<div class="timeline-avatar-shop">
								<img src="/app_manager/img/icon/blog.png" alt="Avatar" class="pull-left" width="50px">
								<h4>ブログの投稿</h4>
								<span class="text-muted menutime"><i class="fa fa-clock-o"></i>昨日のニュース</span>
							</div>
							<p class="tilinesabtitle"><i class="fa fa-exclamation-circle"></i>新しくブログを投稿しました。</p>

							<div class="timelinecontent">
								<a href="/app_manager/blog">今なら100%バック還元キャンペーン実施中！</a>
							</div>

						</div>
						<!-- ブログ投稿-->
					</div>

					<div class="block">
						<!-- 更新ボタン-->
						<div class="block-title backcro">
							<div class="block-options pull-right">
								<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-hide"><i class="fa fa-times"></i></a>
							</div>

						</div>

						<div class="block-content">
							<div class="timeline-avatar-shop">
								<img src="/app_manager/img/icon/updeta.png" alt="Avatar" class="pull-left" width="50px">
								<h4>更新ボタン</h4>
								<span class="text-muted menutime"><i class="fa fa-clock-o"></i>昨日のニュース</span>
							</div>
							<p class="tilinesabtitle"><i class="fa fa-exclamation-circle"></i>更新ボタンが押されました。</p>

							<div class="timelinecontent">
								検索結果が上位に表示されました。更新ボタンはあと2回まで押せます。
							</div>

						</div>
						<!-- 更新ボタン-->
					</div>

					<div class="block">
						<!-- ランキングが更新されました -->
						<div class="block-title backcro">
							<div class="block-options pull-right">
								<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-hide"><i class="fa fa-times"></i></a>
							</div>

						</div>

						<div class="block-content">
							<div class="timeline-avatar-shop">
								<img src="/app_manager/img/icon/ranking.png" alt="Avatar" class="pull-left" width="50px">
								<h4>ランキング</h4>
								<span class="text-muted menutime"><i class="fa fa-clock-o"></i>昨日のニュース</span>
							</div>
							<p class="tilinesabtitle"><i class="fa fa-exclamation-circle"></i>新しくランキングが更新されました。</p>

							<div class="timelinecontent">
								<table class="table table-condensed">
									<caption>平均点ランキング<span class="text-muted"><small>2017/08/25更新</small></span>
									</caption>

									<tr>
										<td style="vertical-align:top;">1位</td>
										<td>
											<a href="">お店の名前が最大で二十文字まで記載されます</a>
										</td>

										<td><span class="text-danger point">3.15点</span>
										</td>
										<td><i class="fa fa-arrow-down"></i>
										</td>
									</tr>
									<tr>
										<td>2位</td>
										<td>
											<a href="">お店の名前が最大で20文字</a>
										</td>

										<td><span class="text-danger point">3.15点</span>
										</td>
										<td><i class="fa fa-arrow-right"></i>
										</td>
									</tr>
									<tr>
										<td>3位</td>
										<td>
											<a href="">お</a>
										</td>

										<td><span class="text-danger point">3.15点</span>
										</td>
										<td><i class="fa fa-arrow-up"></i>
										</td>
									</tr>
									<tr>
										<td colspan="4">
											<p class="text-right"><a href="">全てのランキングを見る</a>
											</p>
											</p>
										</td>
									</tr>
								</table>


							</div>

						</div>
						<!-- ランキングが更新されました -->
					</div>

					<div class="block">
						<!-- PR※画像　マスター管理で追加 -->
						<div class="block-content">
							<p class="tilinesabtitle"><i class="fa fa-exclamation-circle"></i>PR</p>

							<div class="prcoment">
								コメントが入力されます。コメントが入力されます。
							</div>
							<div class="prcontent_movie">
								<iframe width="560" height="315" src="https://www.youtube.com/embed/bg1sT4ILG0w" frameborder="0" allowfullscreen></iframe>
							</div>


						</div>

					</div>

				<div class="block">
					<div class="fast-content">
						※お店に関するニュースはここまでです。<br>ニュースは最大三日前まで表示され、それより以前の情報は削除されていきます。
					</div>
				</div>
	

		</div>



		<!--右---->



		<div class="col-md-4">

			<?	
				require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/dashboard/common/right_deta.php";
			?>

		</div>



		<!--左右のコンテンツ終了6:6-->


	</div>
	<!-- END Dashboard 2 Content -->


</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_footer.php'; ?>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="/app_manager/js/helpers/excanvas.min.js"></script><![endif]-->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_scripts.php'; ?>

<!-- 自動更新-->
<script>
	$( function () {
		setTimeout( function () {
			location.reload();
		}, 100000 );
	} );
</script>
<!-- 自動更新-->

<!-- プログレスバー-->
<script src="/app_manager/js/pages/formsWizard.js"></script>
<script>
	$( function () {
		FormsWizard.init();
	} );
</script>

<!-- Load and execute javascript code used only in this page -->
<script src="/app_manager/js/pages/index2.js"></script>
<script>
	$( function () {
		Index2.init();
	} );
</script>



<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_end.php'; ?>