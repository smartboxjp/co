<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/config.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_start.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_head.php';
?>



<!-- Page content -->
<div id="page-content">

	<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_headnavi.php'; ?>

	<!-- Dashboard 2 Content -->
	<div class="row">


		<!--左-->
		<div class="col-md-2">

			<?	
								require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/dashboard/common/left_deta.php";
							?>

		</div>


		<!--左-->

		<!--真ん中　インフォメーション---->
		<div class="col-md-6">
			<!-- View Message -->

			<div class="block full">
				<!-- View Message Title -->
				<div class="block-title">

					<h2><strong>インフォメーションタイトル</strong> </h2>
				</div>
				<!-- END View Message Title -->
				
				
				<table class="table table-borderless table-vcenter remove-margin">
                    <tbody>
                        <tr>
                            <td class="text-right"><strong>2017/08/25 - 09:10 </strong></td>
                        </tr>
                    </tbody>
                </table>


				<!-- Message Body -->
				<p>コソットからのお知らせの本文が表示されます。されます。されます。
				



			</div>
			<!-- END View Message Block -->


			<div class="text-right">
                    <ul class="pagination">
                        <li class="disabled"><a href="javascript:void(0)">&laquo;</a></li>
                        <li class="active"><a href="javascript:void(0)">1</a></li>
                        <li><a href="javascript:void(0)">2</a></li>
                        <li><a href="javascript:void(0)">3</a></li>
                        <li><a href="javascript:void(0)">4</a></li>
                        <li><a href="javascript:void(0)">&raquo;</a></li>
                    </ul>
           </div>

		</div>



		<!--右---->



		<div class="col-md-4">

			<?	
								require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/dashboard/common/right_deta.php";
							?>

		</div>








		<!--左右のコンテンツ終了6:6-->


	</div>
	<!-- END Dashboard 2 Content -->


</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_footer.php'; ?>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="/app_manager/js/helpers/excanvas.min.js"></script><![endif]-->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_scripts.php'; ?>

<!-- 自動更新-->
<script>
	$( function () {
		setTimeout( function () {
			location.reload();
		}, 10000000 );
	} );
</script>
<!-- 自動更新-->

<!-- プログレスバー-->
<script src="/app_manager/js/pages/formsWizard.js"></script>
<script>
	$( function () {
		FormsWizard.init();
	} );
</script>

<!-- Load and execute javascript code used only in this page -->
<script src="/app_manager/js/pages/index2.js"></script>
<script>
	$( function () {
		Index2.init();
	} );
</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_end.php'; ?>