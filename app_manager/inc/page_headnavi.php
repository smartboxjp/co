<!-- Dashboard 2 Header -->
    <div class="content-header">
        <ul class="nav-horizontal text-center">
            <li>
                <a href="/app_manager/dashboard/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li>
               <a id="modal-open" class="button-link" data-toggle="modal" data-target="#myModal"><i class="fa fa-level-up"></i> 上位表示</a>
            </li>
            <li>
                <a href="/app_manager/blog/blog.php"><i class="fa fa-pencil"></i> ブログ</a>
            </li>
            <li>
                <a href="/app_manager/voise/voise.php"><i class="fa fa-commenting-o"></i> 口コミ</a>
            </li>
            <li>
                <a href="/app_manager/shop-img/shop-img.php"><i class="fa fa-picture-o"></i> 画像</a>
            </li>
            <li>
                <a href="/app_manager/mail/"><i class="fa fa-envelope-o"></i> メール</a>
            </li>
        </ul>
    </div>
    <!-- END Dashboard 2 Header -->
    
   	<!-- モーダルウィンドウの中身 -->
	<div class="modal fade" id="myModal">
  		<div class="modal-dialog">
    		<div class="modal-content">
      			<div class="modal-header">
  
        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        			<h4 class="modal-title"><i class="fa fa-level-up"></i>検索結果の表示順番を上位に出来ます。</h4>
      			</div>
      			
      			<div class="modal-body">
      				<P>一日に有料店は3回、無料店は1回、検索結果の表示順番を上に表示させることが出来ます。<br>
      				回数は夜中の0時でリセットされ、繰り越しは出来ませんのでなるべく使い切って下さい。</p>
                		
                		<div class="progress progress-striped active">
                    		<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 66%">2/3回</div>
                		</div>
      				
      			</div>
      			
      			
      			<div class="modal-footer">
       			       					<div class="block-options pull-right">
							<a href="/app_manager/tutorial/index.php">
							<div class="btn btn-sm btn-alt btn-default" data-toggle="tooltip" title="チュートリアル">
							<i class="fa fa-exclamation"></i></div>
							</a>
						</div>
        			<button type="button" class="btn btn-primary"><i class="fa fa-arrow-up"></i>上位表示をする</button>
        			
       			</div>
    		</div>
  		</div>
	</div>
<!-- モーダルウィンドウの中身 -->