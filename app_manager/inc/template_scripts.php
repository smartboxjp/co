<?php
/**
 * template_scripts.php
 *
 * Author: pixelcave
 *
 * All vital JS scripts are included here
 *
 */
?>


<!--<script src="/app_manager/js/vendor/jquery-1.12.0.min.js"></script>-->
<script src="/app_manager/js/vendor/bootstrap.min.js"></script>
<script src="/app_manager/js/plugins.js"></script>
<script src="/app_manager/js/app.js"></script>
<!-- Image cropper -->
<script src="/app_manager/js/tool/cropper/cropper.js"></script>
<script src="/app_manager/js/tool/cropper/croppermain.js"></script>
<!-- Vegas 2 -->
<script src="/app_manager/js/tool/anime/vegas.min.js"></script>
<!-- 文字数カウント -->
<script src="/app_manager/js/tool/count/bootstrap-maxlength.js"></script>
<!-- summernote テキストエディタ -->
<script src="/app_manager/js/tool/summernote/summernote.js"></script>

