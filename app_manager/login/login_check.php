<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ログインチェック</title>
</head>

<body>

<?
	if ($shop_id == "" or $shop_pw == "") 
	{
	    $common_connect->Fn_javascript_back("必須項目を正しく入力してください。");
	}
	else
	{
		//ショップの基本情報
		$arr_where = array();
		$var = "s_flag_open";
		$arr_where[$var] = "1"; //公開のみ
		$var = "shop_pw";
		$arr_where[$var] = $$var;
		$result_login = $common_shop -> Fn_db_shop($shop_id, $arr_where);
		
		if($result_login)
		{
			$db_shop_id = $result_login["0"]["shop_id"];
			$db_shop_name = $result_login["0"]["shop_name"];
			$db_flag_open = $result_login["0"]["flag_open"];
		}
		else
		{
			$common_connect->Fn_javascript_back("IDとパスワードを確認してください。");
		}

		
		if($db_flag_open!="1")
		{
			$common_connect->Fn_javascript_back("ログイン権限がありません。");
		}
		
		if ($db_shop_id == $shop_id)
		{
			//管理者の場合
			session_start();
			$_SESSION['shop_id']=$db_shop_id;
			$_SESSION['shop_name']=$db_shop_name;

			if($db_shop_id!="" and $shop_remember=="on")
			{
				$arr_where = array();
				$arr_where["shop_id"] = $db_shop_id;
				
				$login_cookie = sha1($db_shop_id."_".$shop_pw);
				$arr_data = array();
				$arr_data["login_cookie"] = $login_cookie;
				$common_shop->Fn_shop_update ($arr_data, $arr_where);
				
		
				setcookie("shop", $login_cookie, time()+3600*24*31);//暗号化してクッキーに保存, 31日間
			}
			
			$common_connect->Fn_redirect(global_ssl."/app_manager/dashboard/");

		}
	}
?>
</body>
</html>