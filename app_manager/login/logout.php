<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ログアウト</title>
</head>

<body>
<?
	session_start();
	//session_destroy();
	$shop_id = $_SESSION['shop_id'];
	$_SESSION['shop_id']="";
	$_SESSION['shop_name']="";
	
	setcookie("shop", "");
	
	if($shop_id!="")
	{
		$arr_data = array();
		$arr_data["login_cookie"] = "";
		
		$arr_where = array();
		$arr_where["shop_id"] = $shop_id;
		
		$common_shop->Fn_shop_update ($arr_data, $arr_where) ;
		
	}
	
	echo ("<meta http-equiv='Refresh' content='0; URL=/app_manager/login/'>");
?>
</body>
</html>