<div class="block full">
	<div class="block-title clearfix">
	
		<h2>メールフォルダ</h2>
		<div class="block-options pull-right">
			<a href="/app_manager/set/setting.php" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="アドレス設定"><i class="fa fa-cog" aria-hidden="true"></i></a>
		</div>
		<div class="block-options pull-right">
			<a href="/app_manager/mail/mail.php" target="_blank" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="メール作成"><i class="fa fa-pencil" aria-hidden="true"></i></a>
		</div>
	</div>

	<!-- Menu Content -->
	<ul class="nav nav-pills nav-stacked">
		<li class="active">
			<a href="">
                 <span class="badge pull-right">17</span>
                       <i class="fa fa-envelope-o fa-fw text-muted"></i> <strong>全ての受信メール</strong>
              </a>
		

		</li>
		<li>
			<a href="">
                 <span class="badge pull-right">5</span>
                       <i class="fa fa-tag fa-fw text-info"></i> <strong>コソットからのメール</strong>
              </a>
		

		</li>
		<li>
			<a href="">
                            <i class="fa fa-share fa-fw text-muted"></i> <strong>送信したメール</strong>
                        </a>
		

		</li>
		<li>
			<a href="">
                            <span class="badge pull-right">2</span>
                            <i class="fa fa-tag fa-fw text-danger"></i> <strong>未読メール</strong>
                        </a>
		

		</li>
		
		<li>
			<a href="">
                            <span class="badge pull-right">2</span>
                            <i class="fa fa-tag fa-fw text-warning"></i> <strong>未返信メール</strong>
                        </a>
		

		</li>
		<li>
			<a href="">
                            <i class="fa fa-star fa-fw text-warning"></i> <strong>重要なメール</strong>
                        </a>
		

		</li>

	</ul>
	<!-- END Menu Content -->
</div>