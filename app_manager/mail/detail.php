
<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/config.php';
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/template_start.php';
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/page_head.php';
?>


<!-- Page content -->
<div id="page-content">

	<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_headnavi.php'; ?>



	<!-- パンクズリスト -->
	<ul class="breadcrumb breadcrumb-top">
		<li><a href="">管理画面</a>
		</li>
		<li><a href="">メール一覧</a></li>
		<li>コソットからのメール</li>
	</ul>
	<!-- パンクズリスト -->

	<!-- Dashboard 2 Content -->
	<div class="row">


		<div class="col-md-3">
			
				<?	
				require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/mail/common/meilmenu.php"; 
				?>
			
		</div>

 <!-- View Message -->
        <div class="col-sm-8 col-lg-9">
            <!-- View Message Block -->
            <div class="block full">
                <!-- View Message Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="お気に入り"><i class="fa fa-star-o"></i></a>
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="削除する"><i class="fa fa-trash-o"></i></a>
                    </div>
                    <h2><strong>メールタイトルが表示されます。</strong> <small><span class="label label-info">コソットから</span></small></h2>
                </div>
                <!-- END View Message Title -->

                <!-- Message Meta -->
                <table class="table table-borderless table-vcenter remove-margin">
                    <tbody>
                        <tr>
                            <td class="text-center" style="width: 80px;">
                                <a href="page_ready_user_profile.php" class="pull-left">
                                    <img src="/app_manager/img/test/123.gif" alt="Avatar" class="img-circle" width="60px">
                                </a>
                            </td>
                            <td class="hidden-xs">
                                <a href="page_ready_user_profile.php"><strong>コソット</strong></a>
                            </td>
                            <td class="text-right"><strong>2017/08/25 - 09:10 </strong></td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                
                <!-- END Message Meta -->

                <!-- Message Body -->
                <p>本文が表示されます</p>
                <p>本文が表示されます</p>
                <p>本文が表示されます</p>
                <p>本文が表示されます</p>
                <p>本文が表示されます</p>
                <p>本文が表示されます</p>
                <p>本文が表示されます</p>
                <hr>
                <!-- END Message Body -->
				
               
               ※コソットとのやり取りは何度も返信可能です。
                <!-- Quick Reply Form -->
                <form action="" method="post" onsubmit="return false;">
                    <div class="mailtitleblok">
                    	<input type="text" id="example-text-input" name="example-text-input" class="form-control" placeholder="返信用タイトル">
					</div>
                    <textarea id="message-quick-reply" name="message-quick-reply" rows="5" class="form-control push-bit" placeholder="返信用本文"></textarea>
                    <button class="btn btn-sm btn-primary"><i class="fa fa-share"></i> 返信する</button>
                </form>
                <!-- END Quick Reply Form -->
            </div>
            <!-- END View Message Block -->
        </div>
        <!-- END View Message -->
        
        


	</div>
	<!-- low -->

</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_footer.php'; ?>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="/app_manager/js/helpers/excanvas.min.js"></script><![endif]-->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_scripts.php'; ?>

<!-- プログレスバー-->
<script src="/app_manager/js/pages/formsWizard.js"></script>
<script>
	$( function () {
		FormsWizard.init();
	} );
</script>

<!-- Load and execute javascript code used only in this page -->
<script src="/app_manager/js/pages/readyInboxCompose.js"></script>
<script>$(function(){ ReadyInboxCompose.init(); });</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_end.php'; ?>