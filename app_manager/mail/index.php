
<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/config.php';
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/template_start.php';
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/page_head.php';
?>


<!-- Page content -->
<div id="page-content">

	<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_headnavi.php'; ?>



	<!-- パンクズリスト -->
	<ul class="breadcrumb breadcrumb-top">
		<li><a href="">管理画面</a>
		</li>
		<li>メール一覧</li>
	</ul>
	<!-- パンクズリスト -->

	<!-- Dashboard 2 Content -->
	<div class="row">


		<div class="col-md-3">
			
				<?	
				require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/mail/common/meilmenu.php"; 
				?>
			
		</div>


<!-- Compose Message List -->
        <div class="col-sm-8 col-lg-9">
            <!-- Compose Message Block -->
            <div class="block">
                <!-- Compose Message Title -->
                <div class="block-title">
                    <h2>メール一覧</h2>
				</div>
                <!-- END Compose Message Title -->


           
           <!-- Messages List Content -->
                <div class="table-responsive">
                    <table class="table table-hover table-vcenter">
                        <thead>
                            <tr>
                                <td class="text-center">
                                    <input type="checkbox" id="checkbox-all" name="checkbox-all">
                                </td>
                                <td colspan="2">
                                    <div class="btn-group btn-group-sm">
                                        <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="チェックしたのをまとめてお気に入り"><i class="fa fa-star"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="チェックしたのをまとめて削除"><i class="fa fa-times"></i></a>
                                    </div>
                                </td>
                                <td class="text-right" colspan="3">
                                    <strong>1-10</strong> from <strong>250</strong>
                                    <div class="btn-group btn-group-sm">
                                        <a href="javascript:void(0)" class="btn btn-sm btn-default"><i class="fa fa-angle-left"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-sm btn-default"><i class="fa fa-angle-right"></i></a>
                                    </div>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Use the first row as a prototype for your column widths -->
                            
                            <tr>
                            	<td class="text-left" colspan="6">
                            	※ユーザーからのメールは一通に対して<span class="text-danger">一回しか</span>返信出来ません。<span class="label label-info">コソットから</span>
                            	のラベルの付いたメールは何度も返信可能。
								</td>
                            </tr>
                            
                            <tr>
                                <td class="text-center" style="width: 30px;">
                                    <input type="checkbox" id="checkbox1" name="checkbox1">
                                </td>
                                <td class="text-center" style="width: 30px;">
                                    <span class="text-muted msg-fav-btn"><i class="fa fa-star-o"></i></span>
                                </td>

								<td style="width: 20%;">ユーザー名がはち</td>
                                <td>
                                    <a href="/app_manager/mail/detail-user.php">
                                    	<span class="text-muted">メールのタイトルが表示されます。</span>
                                    </a>
                                </td>
                                <td class="text-center" style="width: 30px;">
                                    <i class="fa fa-paperclip"></i>
                                </td>
                                <td class="text-right" style="width: 90px;"><em>08：27</em></td>
                            </tr>
                            
                            <tr>
                                <td class="text-center">
                                    <input type="checkbox" id="checkbox2" name="checkbox2">
                                </td>
                                <td class="text-center" style="width: 30px;">
                                    <span class="text-muted msg-fav-btn"><i class="fa fa-star-o"></i></span>
                                </td>

                                <td style="width: 20%;">りずりさ</td>
                                <td>
                                   <span class="label label-info">コソットから</span>
                                    <a href="/app_manager/mail/detail.php">
                                    	<span class="text-muted">メールのタイトルが表示されます。</span>
                                    </a>
                                </td>
                                <td class="text-center" style="width: 30px;">
                                    <i class="fa fa-paperclip"></i>
                                </td>
                                <td class="text-right" style="width: 90px;"><em>12：17</em></td>
                            </tr>
                            
                            <tr>
                                <td class="text-center">
                                    <input type="checkbox" id="checkbox3" name="checkbox3">
                                </td>
                                <td class="text-center">
                                    <a href="javascript:void(0)" class="text-warning msg-fav-btn"><i class="fa fa-star"></i></a>
                                </td>

                                <td style="width: 20%;">りずりさ</td>
                                <td>
                                    <a href="/app_manager/mail/detail-user.php">
                                    	<span class="text-muted">メールのタイトルが表示されます。</span>
                                    </a>
                                </td>
                                <td class="text-center" style="width: 30px;">
                                
                                </td>
                                <td class="text-right" style="width: 90px;"><em>12：17</em></td>
                            </tr>
                            
                            <tr>
                                <td class="text-center">
                                    <input type="checkbox" id="checkbox4" name="checkbox4">
                                </td>
                                <td class="text-center">
                                    <a href="javascript:void(0)" class="text-warning msg-fav-btn"><i class="fa fa-star"></i></a>
                                </td>

                                <td style="width: 20%;">りずりさ</td>
                                <td>
                                   <span class="label label-danger">未読</span>
                                    <a href="/app_manager/mail/detail-user.php">
                                    	<span class="text-muted">メールのタイトルが表示されます。</span>
                                    </a>
                                </td>
                                <td class="text-center" style="width: 30px;">
                                
                                </td>
                                <td class="text-right" style="width: 90px;"><em>12：17</em></td>
                            </tr>
                            
                            <tr>
                                <td class="text-center">
                                    <input type="checkbox" id="checkbox5" name="checkbox5">
                                </td>
                                <td class="text-center">
                                    <a href="javascript:void(0)" class="text-warning msg-fav-btn"><i class="fa fa-star"></i></a>
                                </td>

                                <td style="width: 20%;">りずりさ</td>
                                <td>
                                  <span class="label label-info">コソットから</span>
                                   <span class="label label-danger">未読</span>
                                    <a href="/app_manager/mail/detail.php">
                                    	<span class="text-muted">メールのタイトルが表示されます。</span>
                                    </a>
                                </td>
                                <td class="text-center" style="width: 30px;">
                                
                                </td>
                                <td class="text-right" style="width: 90px;"><em>12：17</em></td>
                            </tr>
                            
                            <tr>
                                <td class="text-center">
                                    <input type="checkbox" id="checkbox6" name="checkbox6">
                                </td>
                                <td class="text-center">
                                    <a href="javascript:void(0)" class="text-warning msg-fav-btn"><i class="fa fa-star"></i></a>
                                </td>

                                <td style="width: 20%;">りずりさ</td>
                                <td>
                                   <span class="label label-warning">未返信</span>
                                   	<a href="/app_manager/mail/detail-user.php">
                                    	<span class="text-muted">メールのタイトルが表示されます。</span>
                                    </a>
                                </td>
                                <td class="text-center" style="width: 30px;">
                                
                                </td>
                                <td class="text-right" style="width: 90px;"><em>12：17</em></td>
                            </tr>
                            
                            <tr>
                                <td class="text-center">
                                    <input type="checkbox" id="checkbox7" name="checkbox7">
                                </td>
                                <td class="text-center">
                                    <a href="javascript:void(0)" class="text-warning msg-fav-btn"><i class="fa fa-star"></i></a>
                                </td>

                                <td style="width: 20%;">りずりさ</td>
                                <td>
                                  <span class="label label-danger">未読</span>
                                   <span class="label label-warning">未返信</span>
                                    <a href="/app_manager/mail/detail-user.php">
                                    	<span class="text-muted">メールのタイトルが表示されます。</span>
                                    </a>
                                </td>
                                <td class="text-center" style="width: 30px;">
                                
                                </td>
                                <td class="text-right" style="width: 90px;"><em>12：17</em></td>
                            </tr>
                            
                             <tr>
                                <td class="text-center">
                                    <input type="checkbox" id="checkbox8" name="checkbox8">
                                </td>
                                <td class="text-center" style="width: 30px;">
                                    <span class="text-muted msg-fav-btn"><i class="fa fa-star-o"></i></span>
                                </td>

                                <td style="width: 20%;">りずりさ</td>
                                <td>
                                   
                                    	<span class="text-muted">メールのタイトルが表示されます。</span>
                                    </a>
                                </td>
                                <td class="text-center" style="width: 30px;">
                                    <i class="fa fa-paperclip"></i>
                                </td>
                                <td class="text-right" style="width: 90px;"><em>12：17</em></td>
                            </tr>
                            
                                                        <tr>
                                <td class="text-center">
                                    <input type="checkbox" id="checkbox9" name="checkbox9">
                                </td>
                                <td class="text-center" style="width: 30px;">
                                    <span class="text-muted msg-fav-btn"><i class="fa fa-star-o"></i></span>
                                </td>

                                <td style="width: 20%;">りずりさ</td>
                                <td>
                                    <a href="/app_manager/mail/detail-user.php">
                                    	<span class="text-muted">メールのタイトルが表示されます。</span>
                                    </a>
                                </td>
                                <td class="text-center" style="width: 30px;">
                                    <i class="fa fa-paperclip"></i>
                                </td>
                                <td class="text-right" style="width: 90px;"><em>12：17</em></td>
                            </tr>
                            
                                                        <tr>
                                <td class="text-center">
                                    <input type="checkbox" id="checkbox10" name="checkbox10">
                                </td>
                                <td class="text-center" style="width: 30px;">
                                    <span class="text-muted msg-fav-btn"><i class="fa fa-star-o"></i></span>
                                </td>

                                <td style="width: 20%;">りずりさ</td>
                                <td>
                                   <a href="/app_manager/mail/detail-user.php">
                                    	<span class="text-muted">メールのタイトルが表示されます。</span>
                                    </a>
                                </td>
                                <td class="text-center" style="width: 30px;">
                                    <i class="fa fa-paperclip"></i>
                                </td>
                                <td class="text-right" style="width: 90px;"><em>12：17</em></td>
                            </tr>
                            
                            
                          
                        </tbody>
                    </table>
                </div>
                <!-- END Messages List Content -->
           
           
           
           
           
            </div>
            <!-- END Compose Message Block -->
        </div>
        <!-- END Compose Message -->
        
        


	</div>
	<!-- low -->

</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_footer.php'; ?>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="/app_manager/js/helpers/excanvas.min.js"></script><![endif]-->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_scripts.php'; ?>

<!-- プログレスバー-->
<script src="/app_manager/js/pages/formsWizard.js"></script>
<script>
	$( function () {
		FormsWizard.init();
	} );
</script>

<!-- Load and execute javascript code used only in this page -->
<script src="/app_manager/js/pages/readyInboxCompose.js"></script>
<script>$(function(){ ReadyInboxCompose.init(); });</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_end.php'; ?>