
<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/config.php';
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/template_start.php';
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/page_head.php';
?>


<!-- Page content -->
<div id="page-content">

	<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_headnavi.php'; ?>



	<!-- パンクズリスト -->
	<ul class="breadcrumb breadcrumb-top">
		<li><a href="">管理画面</a>
		</li>
		<li><a href="">メール一覧</a></li>
		<li>メール作成</li>
	</ul>
	<!-- パンクズリスト -->

	<!-- Dashboard 2 Content -->
	<div class="row">


		<div class="col-md-3">
			
				<?	
				require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/mail/common/meilmenu.php"; 
				?>
			
		</div>


<!-- Compose Message List -->
        <div class="col-sm-8 col-lg-9">
            <!-- Compose Message Block -->
            <div class="block">
                <!-- Compose Message Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" id="cc-input-btn" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Show Cc field"><i class="fa fa-plus"></i> Cc</a>
                        <a href="javascript:void(0)" id="bcc-input-btn"  class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Show Bcc field"><i class="fa fa-plus"></i> Bcc</a>
                    </div>
                    <h2>メール作成</h2>
				</div>
                <!-- END Compose Message Title -->

                <!-- Compose Message Content -->
                <form action="page_ready_inbox_compose.php" method="post" class="form-horizontal form-bordered" onsubmit="return false;">
                    <div class="form-group">
                        <label class="col-md-3 col-lg-2 control-label" for="compose-to">宛先</label>
                        <div class="col-md-9 col-lg-10">
                            <p class="form-control-static">コソット管理へ</p>
                        </div>
                    </div>
                    <div id="cc-input" class="form-group display-none">
                        <label class="col-md-3 col-lg-2 control-label" for="compose-to-cc">Cc</label>
                        <div class="col-md-9 col-lg-10">
                            <input type="email" id="compose-to-cc" name="compose-to-cc" class="form-control form-control-borderless" placeholder="※自分のメールアドレスを入力してください">
                        </div>
                    </div>
                    <div id="bcc-input" class="form-group display-none">
                        <label class="col-md-3 col-lg-2 control-label" for="compose-to-bcc">Bcc</label>
                        <div class="col-md-9 col-lg-10">
                            <input type="email" id="compose-to-bcc" name="compose-to-bcc" class="form-control form-control-borderless" placeholder="※自分のメールアドレスを入力してください">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 col-lg-2 control-label" for="compose-to-bcc">タイトル</label>
                        <div class="col-md-9 col-lg-10">
                            <select id="example-select" name="example-select" class="form-control" size="1">
                                <option value="0">口コミについて</option>
                                <option value="1">投稿画像</option>
                                <option value="2">点数について</option>
                                <option value="3">Blogについて</option>
                                <option value="4">動画について</option>
                                <option value="5">管理画面の操作</option>
                                <option value="6">広告掲載希望</option>
                                <option value="7">その他</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 col-lg-2 control-label" for="compose-message">本文</label>
                        <div class="col-md-9 col-lg-10">
                            <textarea id="compose-message" name="compose-message" rows="20" class="form-control" placeholder="Your message.."></textarea>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-share"></i> メールを送る</button>
                            <button type="button" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i> 保存</button>
                        </div>
                    </div>
                </form>
                <!-- END Compose Message Content -->
            </div>
            <!-- END Compose Message Block -->
        </div>
        <!-- END Compose Message -->
        
        


	</div>
	<!-- low -->

</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_footer.php'; ?>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="/app_manager/js/helpers/excanvas.min.js"></script><![endif]-->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_scripts.php'; ?>

<!-- プログレスバー-->
<script src="/app_manager/js/pages/formsWizard.js"></script>
<script>
	$( function () {
		FormsWizard.init();
	} );
</script>

<!-- Load and execute javascript code used only in this page -->
<script src="/app_manager/js/pages/readyInboxCompose.js"></script>
<script>$(function(){ ReadyInboxCompose.init(); });</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_end.php'; ?>