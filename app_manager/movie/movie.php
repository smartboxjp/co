<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/config.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_start.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_head.php';
    
    $common_connect -> Fn_shop_check();
    $shop_id = $_SESSION['shop_id'];
?>

<script language="javascript"> 
    function fnChangeDel(i) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './movie_del.php?shop_movie_id='+i;
        } 
    }
</script>

<!-- Page content -->
<div id="page-content">

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_headnavi.php'; ?>
<?

    $sql = "SELECT shop_movie_title, movie_1 FROM shop_movie where shop_id='".$shop_id."' ";

    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $shop_movie_title = $db_result[0]["shop_movie_title"];
        $movie_1 = $db_result[0]["movie_1"];
    }
?>
   
   
    <!-- パンクズリスト -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="">管理画面</a></li>
        <li>動画投稿</li>
    </ul>
     <!-- パンクズリスト -->
   
    <!-- Dashboard 2 Content -->
    <div class="row">
    
    
    
    <div class="col-md-12">
     <!-- CKEditor Block -->
    <div class="block">
        <!-- CKEditor Title -->
        <div class="block-title">
            <h2><strong><i class="fa fa-play"></i>動画でお店をもっと知ってもらいましょう</strong></h2>
        </div>
        <!-- END CKEditor Title -->

        <!-- CKEditor Content -->
            <fieldset>
                <legend>動画は一回のみです。新しい動画をUPすれば古いのは消えてしまいます</legend>

            </fieldset>
                    
            <fieldset>                     

            <form action="movie_save.php" method="post" class="form-horizontal form-bordered">
            	<div class="form-group">
                	<label class="col-md-2 control-label" for="example-text-input">動画コメント</label>
                        		
                    	<div class="col-md-6">
          					<textarea id="example-textarea-input" name="shop_movie_title" rows="2" class="form-control" placeholder="コメント" maxlength="92"><? echo $shop_movie_title;?></textarea>
                       		<span class="help-block">コメントの文字数は最大92文字までです。</span>
                       			<button type="submit" id="form_confirm" class="btn btn-sm btn-primary"><i class="fa fa-ok"></i>コメントを編集する</button>
                    	
                        </div>
                       			
                </div>
            </form>

                    		
                <div class="form-group">
                	<label class="col-md-2 control-label" for="example-text-input">動画のUP</label>
                        		
                	<div class="col-md-5">
                	   <div class="btn btn-default fileinput-button">
							<div class="firefox_hack">
							<span>動画をアップ</span>
							<!-- The file input field used as target for the file upload widget -->
							<input id="fileupload" type="file" name="files[]" >
							</div>
						</div>
                    	<span class="help-block">著作権に反するもの、公序良俗に反する恐れのあるものはお控えください<br>
                    	動画ファイルはMP4にのみ対応しております。また容量は最大20MBまでです。<br>
                    	<a href="https://www.onlinevideoconverter.com/ja/cloud-converter" target="_blank">→動画ファイルを変換したいなら</a><br>
                    	<a href="">→動画の容量を落としたいなら</a><br>
                    	
                    	</span>
                    	<br>
						<br>
						
						<!-- アップロードされたファイル -->
						<div id="files" class="files"></div>
                   		<!-- プログレスバー -->
						<div id="progress" class="progress progress-striped active">
							<div class="progress-bar progress-bar-info"></div>
						</div>
                    </div>
                </div>
                
            </fieldset>
            

        <!-- END CKEditor Content -->
    </div>
    <!-- END CKEditor Block -->
    </div>
    
    

    
    <div class="col-md-12">
    
    <!-- Responsive Full Block -->
    <div class="block">
        <!-- Responsive Full Title -->
        <div class="block-title">
            <h2><strong>投稿された動画</strong></h2>
        </div>
        <!-- END Responsive Full Title -->

        
        <p>動画に日の投稿数ありません。どのお店も一回だけ投稿する事が出来ますが、
        <br>新しく動画を投稿すると古い動画は上書きされ、削除されますので注意してください。
        </p>
        <div class="table-responsive">
            <table class="table table-vcenter table-striped">
                <thead>
                    <tr>
                        <th style="width: 100px;" class="text-center">動画サムネイル</th>
                        <th class="text-center">投稿日</th>
                        <th class="text-center">時間</th>
                        <th class="text-center">コメント</th>
                        <th style="width: 150px;" class="text-center">操作</th>
                    </tr>
                </thead>
<?php
    
    $view_count=10;   // List count
    $offset=0;

    if(!$page)
    {
        $page=1;
    }
    Else
    {
        $offset=$view_count*($page-1);
    }
    
    $where = "";
    $where .= " and i.shop_id='".$shop_id."' ";

    //合計
    $sql_count = "SELECT count(i.shop_id) as all_count FROM shop_movie i inner join shop s on i.shop_id=s.shop_id where s.flag_open=1 ".$where ;
    
    $db_result_count = $common_dao->db_query_bind($sql_count);
    if($db_result_count)
    {
        $all_count = $db_result_count[0]["all_count"];
    }
    
    //リスト表示
    $arr_db_field = array("shop_movie_id");
    
    $sql = "SELECT i.shop_id, i.regi_date, i.up_date, i.shop_movie_title, i.movie_1, i.up_date, ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM shop_movie i inner join shop s on i.shop_id=s.shop_id where s.flag_open=1 ".$where ;
    if($order_name != "")
    {
        $sql .= " order by ".$order_name." ".$order;
    }
    else
    {
        $sql .= " order by i.regi_date desc";
    }
    $sql .= " limit $offset,$view_count";
    
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $inner_count = count($db_result);
        for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
        {
            foreach($arr_db_field as $val)
            {
                $$val = $db_result[$db_loop][$val];
            }
            $shop_movie_id = $db_result[$db_loop]["shop_movie_id"];
            $shop_id = $db_result[$db_loop]["shop_id"];
            $regi_date = $db_result[$db_loop]["regi_date"];
            $up_date = $db_result[$db_loop]["up_date"];
            $shop_movie_title = $db_result[$db_loop]["shop_movie_title"];
            $movie_1 = $db_result[$db_loop]["movie_1"];
?>
                <tbody>
                    <tr>
                        <td class="text-center">
                        <? if($movie_1!="") { ?>
                        <video src="/<? echo global_shop_movie_dir.$shop_id."/".$movie_1;?>" widht="200" height="113" preload="metadata" onclick="this.play()"  controls></video>
                        <? } ?>
                        </td>
                        <td class="text-center"><? echo date("Y/m/d", strtotime($regi_date));?></td>
                        <td class="text-center"><? echo date("H時i分", strtotime($regi_date));?>投稿</td>
                        <td class="text-center"><? echo $common_connect->Fn_shot_string($shop_movie_title, 10, "…");?></td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                <a href="#" onClick='fnChangeDel("<?php echo $shop_movie_id;?>");' data-toggle="tooltip" title="削除" class="btn btn-danger"><i class="fa fa-times"></i></a>
                            </div>
                        </td>
                    </tr>
                </tbody>
<?
        }
    }
?>
            </table>
        </div>
        <!-- END Responsive Full Content -->
    </div>
    <!-- END Responsive Full Block -->

</div>


    </div>
    <!-- END Dashboard 2 Content -->
</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_footer.php'; ?>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="/app_manager/js/helpers/excanvas.min.js"></script><![endif]-->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_scripts.php'; ?>

<!-- プログレスバー-->
<script src="/app_manager/js/pages/formsWizard.js"></script>
<script>$(function(){ FormsWizard.init(); });</script>

<!-- 文字カウント-->
<script src="/app_manager/js/pages/bootstrap-maxlength.js"></script>
<script>
$(document).ready(function () {
  $('.form-control').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>

<!--アップロードサムネイル関係プラグイン-->
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<script src="/app_manager/js/tool/fileupload/jquery.iframe-transport.js"></script>
<!-- メインファイル -->
<script src="/app_manager/js/tool/fileupload/jquery.fileupload.js"></script>
<!-- プログレスバー-->
<script src="/app_manager/js/tool/fileupload/jquery.fileupload-process.js"></script>
<!-- アップロードするファイルの種類 -->
<script src="/app_manager/js/tool/fileupload/jquery.fileupload-video.js"></script>
<script src="/app_manager/js/tool/fileupload/jquery.fileupload-validate.js"></script>
<script>
$(function () {
    'use strict';
    var file_count=0;
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'server/php/',
        uploadButton = $('<button/>')
            .addClass('btn btn-primary')
            .prop('disabled', true)
            .text('Processing...')
            .on('click', function () {
                var $this = $(this),
                    data = $this.data();
                $this
                    .off('click')
                    .text('Abort')
                    .on('click', function () {
                        $this.remove();
                        data.abort();
                    });
                data.submit().always(function () {
                    $this.remove();
                });
            });
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(mp4)$/i,
        maxFileSize: 20000000,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 150,
        previewMaxHeight: 100,
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
        file_count++;
        if(file_count>1)
        {
            alert("ファイルは１個登録できます。");
            return false; 
        }

        data.context = $('<div/>').appendTo('#files');
        $.each(data.files, function (index, file) {
            var node = $('<p/>')
                    .append($('<span/>').text(file.name));
            if (!index) {
                node
                    .append('<br>')
                    .append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('この動画で決定')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {

                $.ajax({
                    type: "POST",
                    url: "filename_up.php",
                    data: "movie_1="+file.name,
                    success: function(data) {
                        //alert("登録");
                    },
                    error: function(msg) {
                        alert('DB登録中エラー: '+msg);
                    }
                });
                /*
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
                */
                $(data.context.children()[index]).append("<a href='"+file.url+"' target='blank'>ダウンロード</a><input type='hidden' name='img_1' value='"+file.name+"'>");
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('エラー');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>


<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_end.php'; ?>




