<?php
error_reporting(E_ALL | E_STRICT);
require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
$common_connect = new CommonConnect();
$common_dao = new CommonDao(); //DB関連

require('UploadHandler.php');
/*
 * jQuery File Upload Plugin PHP Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */



$common_connect -> Fn_shop_check();
$shop_id = $_SESSION['shop_id'];



class myUploadHandler extends UploadHandler{


    protected function get_unique_filename($file_path, $name, $size, $type, $error, $index, $content_range) {
        $ext = "";
        if(preg_match('/^(.+)\.(.+)$/',$name,$matchs)){
          $ext = ".".$matchs[2];
        }
        //$name = uniqid().$ext;
        
        $name = "movie".$ext;
        

        return $name;
    }
}


$options=array(
    'upload_url' => '/app_photo/movie/'.$shop_id.'/',
    'upload_dir' => $_SERVER['DOCUMENT_ROOT'].'/app_photo/movie/'.$shop_id.'/',
    'accept_file_types' => '/\.(mp4)$/i'

);
$upload_handler = new myUploadHandler($options);

