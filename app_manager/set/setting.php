
<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/config.php';
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/template_start.php';
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/page_head.php';
?>


<!-- Page content -->
<div id="page-content">

	<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_headnavi.php'; ?>



	<!-- パンクズリスト -->
	<ul class="breadcrumb breadcrumb-top">
		<li><a href="">管理画面</a>
		</li>
		<li>設定</li>
	</ul>
	<!-- パンクズリスト -->

	<!-- Dashboard 2 Content -->
	<div class="row">

           
        <!--右ナビゲーションimg-->
	<div class="col-sm-3">
            <div class="block">
		<!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2>設定</h2>
                </div>
                <!-- END Form Elements Title -->
		<div class="block"><p><img src="/app_manager/img/navimg/set.png" class="img-responsive center-block"></p></div>
	    </div>
	</div>

<!--右ナビゲーションimg-->
		


<!-- Compose Message List -->
        <div class="col-sm-8 col-lg-9">
            <!-- Compose Message Block -->
            <div class="block">
                <!-- Compose Message Title -->
                <div class="block-title">
                    <h2>申請する変更項目</h2>
				</div>
                <!-- END Compose Message Title -->

                <!-- Compose Message Content -->
                <form action="" method="post" class="form-horizontal form-bordered" onsubmit="return false;">
                   
                    <div class="form-group">
                        <label class="col-md-3 col-lg-2 control-label" for="compose-to-bcc">お店のID</label>
                        <div class="col-md-9 col-lg-10">
                            <input type="text" id="example-disabled-input" name="example-disabled-input" class="form-control" placeholder="akdfawer22" disabled>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 col-lg-2 control-label" for="compose-to-bcc">店名</label>
                        <div class="col-md-9 col-lg-10">
                            <input type="text" id="example-disabled-input" name="example-disabled-input" class="form-control" placeholder="お店の名前が20文字まで表示" disabled>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 col-lg-2 control-label" for="compose-to-bcc">登録エリア</label>
                        <div class="col-md-9 col-lg-10">
                            <input type="text" id="example-disabled-input" name="example-disabled-input" class="form-control" placeholder="東京/新宿,新大久保,エリア" disabled>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 col-lg-2 control-label" for="compose-to-bcc">お店の業種</label>
                        <div class="col-md-9 col-lg-10">
                            <input type="text" id="example-disabled-input" name="example-disabled-input" class="form-control" placeholder="デリバリーヘルス" disabled>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-share"></i> コソットに修正依頼</button>
                        </div>
                    </div>
				</form>
              </div>
                    
              <div class="block">
              
                <div class="block-title">
                    <h2>自分で出来る変更項目</h2>
				</div>
              	<form action="" method="post" class="form-horizontal form-bordered" onsubmit="return false;">
                    <div class="form-group">
                        <label class="col-md-3 col-lg-2 control-label" for="compose-to-bcc">登録アドレス</label>
                        <div class="col-md-9 col-lg-10">
                            <input type="email" id="example-email-input" name="example-email-input" class="form-control" placeholder="メールアドレス">
                            <span class="help-block">届いたメールのURLから認証をしてください</span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 col-lg-2 control-label" for="compose-to-bcc">もう一度</label>
                        <div class="col-md-9 col-lg-10">
                            <input type="email" id="example-email-input" name="example-email-input" class="form-control" placeholder="メールアドレス">
                        </div>
                    </div>
                    
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-share"></i> 設定確認メールを送る</button>
                        </div>
                    </div>
                </form>
                <!-- END Compose Message Content -->
            </div>
            <!-- END Compose Message Block -->
        </div>
        <!-- END Compose Message -->
        
        


	</div>
	<!-- low -->

</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_footer.php'; ?>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="/app_manager/js/helpers/excanvas.min.js"></script><![endif]-->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_scripts.php'; ?>

<!-- プログレスバー-->
<script src="/app_manager/js/pages/formsWizard.js"></script>
<script>
	$( function () {
		FormsWizard.init();
	} );
</script>

<!-- Load and execute javascript code used only in this page -->
<script src="/app_manager/js/pages/readyInboxCompose.js"></script>
<script>$(function(){ ReadyInboxCompose.init(); });</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_end.php'; ?>