<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/config.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_start.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_head.php';

    foreach ( $_GET as $key => $value ) {
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_shop_check();
    $shop_id = $_SESSION['shop_id'];



    if($voice_img_id!="")
    {
        $arr_db_field = array("shop_id", "member_id", "img_1", "voice_img_title");
        $arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
            
        $sql = "SELECT voice_img_id, ";
        foreach($arr_db_field as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM voice_img where shop_id='".$shop_id."' and voice_img_id='".$voice_img_id."'";

        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            foreach($arr_db_field as $val)
            {
                $$val = $db_result[0][$val];
            }
        }
    }
?>
<script src="/app_manager/js/jquery.min.js"></script>
<script type="text/javascript">
    $(function() {
            
        $('#form_confirm').click(function() {
            err_default = "";
            err_check_count = 0;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";
            
            err_check_count += check_input("voice_img_title");
            err_check_count += check_input("img");
            
            if(err_check_count!=0)
            {
                alert("入力に不備があります");
                return false;
            }
            else
            {
                //$('#form_confirm').submit();
                $('#form_confirm', "body").submit();
                return true;
            }
            
            
        });
        
        function check_input($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);

            if($('#'+$str).val()=="")
            {
                err ="<span class='errorForm'>※未選択</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }
        

    });
    
    
    
//-->
</script>
<script language="javascript"> 
    function fnChangeDel(i) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './post-img_del.php?voice_img_id='+i;
        } 
    }
</script>


<!-- Page content -->
<div id="page-content">

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_headnavi.php'; ?>

   
   
    <!-- パンクズリスト -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="">管理画面</a></li>
        <li>ブログ投稿</li>
    </ul>
     <!-- パンクズリスト -->
   
    <!-- Dashboard 2 Content -->
    <div class="row">
    
    
    
    <div class="col-md-12">
     <!-- CKEditor Block -->
    <div class="block">
        <!-- CKEditor Title -->
        <div class="block-title">
            <h2><strong><i class="fa fa-camera"></i>関連画像に投稿してみましょう</strong></h2>
        </div>
        <!-- END CKEditor Title -->

        <!-- CKEditor Content -->
        <form action="./post-img_save.php" method="POST" name="form_write" id="form_regist" class="form-horizontal form-bordered" enctype="multipart/form-data">
            <fieldset>
                <legend>関連画像はユーザーだけでなくお店側もアピールとして画像を投稿できます。</legend>
            </fieldset>
                    
            <fieldset>                     

            	<div class="form-group">
                	<label class="col-md-2 control-label" for="voice_img_title">画像コメント</label>
                        		
                    	<div class="col-md-6">
                            <? $var = "voice_img_title"; ?>
                        	<input type="text" id="<? echo $var;?>" name="<? echo $var;?>" value="<? echo $$var;?>" class="form-control down" placeholder="タイトル" maxlength="10">
                           	<span class="help-block">画像コメントは最大10文字までです。</span>
                            <span id="err_<?php echo $var;?>" class="errtext"></span>
                        </div>
                        	

                </div>
                    		
                <div class="form-group">
                	<label class="col-md-2 control-label" for="example-text-input">関連画像</label>
                        		
                    	<div class="col-md-5">
                   				
                    			<div class="btn btn-default fileinput-button">
        								<div class="firefox_hack">
                                   			画像をアップ
                                    	<? $var = "img_1";?>
                                    	<input type="file" id="<? echo $var;?>" name="<? echo $var;?>" />
                                    	</div>
    							</div>

                                <!-- アップロードされたファイル -->
								<p id="preview2" class="preview-img"></p>
                                <? if($$var!="") { ?>
                                <?
                                    echo "<img src='/".global_voice_img_dir.$voice_img_id."/".$$var."?d=".date(his)."' id='insert_".$var."' width='90'>";
                                ?>
                                <? } ?>
                        </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label" for="example-text-input"></label>
    
                    <div class="col-md-5">
                        <? $var = "form_confirm";?>
                        <button type="submit" id="<? echo $var;?>" class="btn btn-sm btn-primary"><i class="fa fa-ok"></i>投稿する</button>
                    </div>
                </div>


                
                


            </fieldset>
            

        </form>
        <!-- END CKEditor Content -->
    </div>
    <!-- END CKEditor Block -->
    </div>
    
    

    
    <div class="col-md-12">
    
    <!-- Responsive Full Block -->
    <div class="block">
        <!-- Responsive Full Title -->
        <div class="block-title">
            <h2><strong>投稿された関連画像一覧</strong></h2>
        </div>
        <!-- END Responsive Full Title -->

        
        <p>関連画像は制限なく投稿が可能です。お店の雰囲気や待機室の写真、<br>
          スタッフの写真など一日に何度も投稿が可能です。但し、法律に明らかに<br>
          反してしてる画像、著作権問題になるような画像の投稿はお控えください。<br>
          またコソットが問題と判断した画像は予告なく削除する場合がありますのでご注意下さい。</p>
        <div class="table-responsive">
            <table class="table table-vcenter table-striped">
                <thead>
                    <tr>
                        <th style="width: 100px;" class="text-center"><i class="fa fa-picture-o" aria-hidden="true"></i></th>
                        <th class="text-center">投稿日</th>
                        <th class="text-center">時間</th>
                        <td class="text-center">10文字以内でコメント</td>
                        <th style="width: 150px;" class="text-center">操作</th>
                    </tr>
                </thead>
                <tbody>

<?php
    
    $view_count=10;   // List count
    $offset=0;

    if(!$page)
    {
        $page=1;
    }
    Else
    {
        $offset=$view_count*($page-1);
    }
    
    $where = "";
    $where .= " and i.shop_id='".$shop_id."' ";
    $where .= " and i.member_id='0' "; //お店登録のみ
    if($s_keyword != "")
    {
        $where .= " and (i.voice_img_title like '%".$s_keyword."%') ";
    }

    //合計
    $sql_count = "SELECT count(i.shop_id) as all_count FROM voice_img i inner join shop s on i.shop_id=s.shop_id where s.flag_open=1 ".$where ;
    
    $db_result_count = $common_dao->db_query_bind($sql_count);
    if($db_result_count)
    {
        $all_count = $db_result_count[0]["all_count"];
    }
    
    //リスト表示
    $arr_db_field = array("voice_img_id");
    
    $sql = "SELECT i.shop_id, i.regi_date, i.up_date, i.voice_img_title, i.img_1, i.up_date, ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM voice_img i inner join shop s on i.shop_id=s.shop_id where s.flag_open=1 ".$where ;
    if($order_name != "")
    {
        $sql .= " order by ".$order_name." ".$order;
    }
    else
    {
        $sql .= " order by i.regi_date desc";
    }
    $sql .= " limit $offset,$view_count";
    
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $inner_count = count($db_result);
        for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
        {
            foreach($arr_db_field as $val)
            {
                $$val = $db_result[$db_loop][$val];
            }
            $shop_id = $db_result[$db_loop]["shop_id"];
            $regi_date = $db_result[$db_loop]["regi_date"];
            $up_date = $db_result[$db_loop]["up_date"];
            $voice_img_title = $db_result[$db_loop]["voice_img_title"];
            $img_1 = $db_result[$db_loop]["img_1"];
?>
                    <tr>
                        <td class="text-center"><div class="post-imgsam">
                        <? if($img_1!="") { ?>
                        <img src="/<? echo global_voice_img_dir.$voice_img_id."/".$img_1."?d=".date(his);?>">
                        <? } ?>
                        </div></td>
                        <td class="text-center"><? echo date("Y/m/d", strtotime($regi_date));?></td>
                        <td class="text-center"><? echo date("H時i分", strtotime($regi_date));?>投稿</td>
                        <td class="text-center"><? echo $common_connect->Fn_shot_string($voice_img_title, 10, "…");?></td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                <a href="<? echo $_SERVER["PHP_SELF"]."?voice_img_id=".$voice_img_id;?>" data-toggle="tooltip" title="編集" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                                <a href="#" onClick='fnChangeDel("<?php echo $voice_img_id;?>");' data-toggle="tooltip" title="削除" class="btn btn-danger"><i class="fa fa-times"></i></a>
                            </div>
                        </td>
                    </tr>
<?
        }
    }
?>

                    
                </tbody>
            </table>

        </div>
        <!-- END Responsive Full Content -->
        
  
        
    </div>
    <!-- END Responsive Full Block -->

</div>


    </div>
    <!-- END Dashboard 2 Content -->
    
    
            <div class="text-right">
                <?php $common_connect -> Fn_paging_10_list($view_count, $all_count); ?>
            </div>
    
    
    
</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_footer.php'; ?>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="/app_manager/js/helpers/excanvas.min.js"></script><![endif]-->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_scripts.php'; ?>

<!-- プログレスバー-->
<script src="/app_manager/js/pages/formsWizard.js"></script>
<script>$(function(){ FormsWizard.init(); });</script>

<!-- 文字カウント-->
<script src="/app_manager/js/pages/bootstrap-maxlength.js"></script>
<script>
$(document).ready(function () {
  $('.down').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>

<!--アップロードサムネイル関係プラグイン-->
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<script src="/app_manager/js/tool/fileupload/jquery.iframe-transport.js"></script>
<!-- メインファイル -->
<script src="/app_manager/js/tool/fileupload/jquery.fileupload.js"></script>
<!-- プログレスバー-->
<script src="/app_manager/js/tool/fileupload/jquery.fileupload-process.js"></script>
<!-- アップロードサムネイル-->
<script src="/app_manager/js/tool/thumbs/thumbs.js"></script>
<script>
$(function() {
    // jQuery Upload Thumbs 
    $('#img_1').uploadThumbs({
        position : '#preview2',   // any: arbitrarily jquery selector
    });
});
</script>


<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_end.php'; ?>




