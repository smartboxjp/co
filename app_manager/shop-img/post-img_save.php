<?php
    //error_reporting(0);
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    $common_image = new CommonImage(); //画像
    

?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php   

    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
    
    $common_connect -> Fn_shop_check();
    $shop_id = $_SESSION['shop_id'];
    $datetime = date("Y/m/d H:i:s");

    if($voice_img_title == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力してください。");
    }

    $flag_open = 1;

    $arr_db_field = array("shop_id", "voice_img_title");
    $arr_db_field = array_merge($arr_db_field, array("flag_open"));

    //基本情報
    if($voice_img_id=="")
    {
        $db_insert = "insert into voice_img ( voice_img_id, ";
        foreach($arr_db_field as $key=>$value)
        {
            $db_insert .= $value.", ";
        }
        $db_insert .= " regi_date, up_date ) values ( '', ";
        foreach($arr_db_field as $key=>$value)
        {
            $db_insert .= "'".$$value."', ";
        }
        $db_insert .= " '".$datetime."', '".$datetime."')  ";
        $db_result = $common_dao->db_update($db_insert);


        $sql = "select last_insert_id() as last_id " ;
        $db_result = $common_dao->db_query_bind($sql);
        $voice_img_id = $db_result[0]["last_id"];

    //3回まで登録出来る可能
    }
    else
    {
        $db_up = "update voice_img set ";
        foreach($arr_db_field as $key=>$value)
        {
            $db_up .= $value."= '".$$value."', ";
        }
        $db_up .= " up_date= '".$datetime."' ";
        $db_up .= " where shop_id='".$shop_id."' and voice_img_id='".$voice_img_id."' ";
        $db_result = $common_dao->db_update($db_up);
    }
    
    //Folder生成
    $save_dir = $global_path.global_voice_img_dir.$voice_img_id."/";
    $common_image -> create_folder ($save_dir);

    
    $new_end_name="_1";
    $fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $voice_img_id, $text_img_1, "");
    
    $db_up = "update voice_img set ";
    $db_up .= " img_1= '".$fname_new_name[1]."', ";
    $db_up .= " up_date= '".$datetime."' ";
    $db_up .= " where voice_img_id='".$voice_img_id."' ";
    $db_result = $common_dao->db_update($db_up);

    
    $common_connect-> Fn_javascript_move("登録・修正しました", "post-img.php?voice_img_id=".$voice_img_id);
?>
</body>
</html>