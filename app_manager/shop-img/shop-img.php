
<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/config.php';
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/template_start.php';
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/page_head.php';

    foreach ( $_GET as $key => $value ) {
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_shop_check();
    $shop_id = $_SESSION['shop_id'];
?>


<!-- Page content -->
<div id="page-content">

	<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_headnavi.php'; ?>



	<!-- パンクズリスト -->
	<ul class="breadcrumb breadcrumb-top">
		<li><a href="">管理画面</a>
		</li>
		<li>投稿された画像一覧</li>
	</ul>
	<!-- パンクズリスト -->

	<!-- Dashboard 2 Content -->
	<div class="row">


		<div class="col-md-3">
			<div class="block">

				<div class="widget">

					<!-- 口コミデータ -->
					<div class="block-title">
						<div class="block-options pull-right">
							<a href="/app_manager/tutorial/index.php">
								<div class="btn btn-sm btn-alt btn-default" data-toggle="tooltip" title="チュートリアル">
									<i class="fa fa-exclamation"></i>
								</div>
							</a>
						</div>
						<h2>評価一覧</h2>
					</div>


<?
    $all_count = 0;
    $where = "";
    $where .= " and shop_id='".$shop_id."' ";

    //合計
    $sql_count = "SELECT count(voice_img_id) as all_count FROM voice_img where flag_open=1 ".$where ;
    
    $db_result_count = $common_dao->db_query_bind($sql_count);
    if($db_result_count)
    {
        $all_count = $db_result_count[0]["all_count"];
    }
?>
					<!-- Widget Main -->

					<div class="widget-main">


						<div class="list-group remove-margin">

							<div class="list-group-item bottomspc">
								<span class="pull-right"><strong><? echo $all_count;?></strong></span>
								<p class="list-group-item-heading remove-margin"><i class="fa fa-picture-o"></i>お店の関連画像</p>
							</div>

<?
    $all_count = 0;
    $where = " and member_id=0 ";
    $where .= " and shop_id='".$shop_id."' ";

    //合計
    $sql_count = "SELECT count(voice_img_id) as all_count FROM voice_img where flag_open=1 ".$where ;
    
    $db_result_count = $common_dao->db_query_bind($sql_count);
    if($db_result_count)
    {
        $all_count = $db_result_count[0]["all_count"];
    }
?>
							<div class="list-group-item">
								<span class="pull-right"><strong><? echo $all_count;?></strong></span>
								<p class="list-group-item-heading remove-margin">お店からの投稿</p>
							</div>

<?
    $all_count = 0;
    $where = " and member_id!=0 ";
    $where .= " and shop_id='".$shop_id."' ";

    //合計
    $sql_count = "SELECT count(voice_img_id) as all_count FROM voice_img where flag_open=1 ".$where ;
    
    $db_result_count = $common_dao->db_query_bind($sql_count);
    if($db_result_count)
    {
        $all_count = $db_result_count[0]["all_count"];
    }
?>
							<div class="list-group-item">
								<span class="pull-right"><strong><? echo $all_count;?></strong></span>
								<p class="list-group-item-heading remove-margin">投稿したユーザー</p>
							</div>

						</div>
					</div>
					<!-- END Widget Main -->

				</div>
			</div>
		</div>


		<div class="col-md-9">
			<!-- CKEditor Block -->
			<div class="block">



				<div class="block-title">
					<div class="block-options pull-right">
						<a href="/app_manager/mail/mail.php" target="_blank">
							<div class="btn btn-sm btn-alt btn-default" data-toggle="tooltip" title="コソットにメールする">
								<i class="fa fa-envelope-o"></i>
							</div>
						</a>
					</div>
					<h2>お店の関連画像一覧</h2>
				</div>


				<div class="widget">
					<div class="widget-advanced widget-advanced-alt">


						<!-- Widget Main -->
						<div class="widget-main">
							<p>関連画像はユーザーだけでなく、お店からも投稿や削除が可能です。枚数制限もありませんので
							お店の雰囲気やスタッフの写真など、ユーザーへの情報発信として投稿してください。不適切な画像やあきらかに著作権や法律に触れるもの、
							コソットが問題と判断したものに関しては予告なく削除する場合がありますのでご注意下さい。<a href="/app_manager/shop-img/post-img.php">→関連画像の投稿はこちら</a></p>

							<!-- コンテンツ -->
							<div data-toggle="lightbox-gallery">

								<div class="row">

<?
    $view_count=12;   // List count
    $offset=0;

    if(!$page)
    {
        $page=1;
    }
    Else
    {
        $offset=$view_count*($page-1);
    }
    
    $where = "";
    $where .= " and shop_id='".$shop_id."' ";


    //合計
    $sql_count = "SELECT count(shop_id) as all_count FROM voice_img where flag_open=1 ".$where ;
    
    $db_result_count = $common_dao->db_query_bind($sql_count);
    if($db_result_count)
    {
        $all_count = $db_result_count[0]["all_count"];
    }
    
    //リスト表示
    $arr_db_field = array("voice_img_id", "member_id");
    
    $sql = "SELECT shop_id, regi_date, up_date, voice_img_title, img_1, up_date, ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM voice_img where flag_open=1 ".$where ;
    if($order_name != "")
    {
        $sql .= " order by ".$order_name." ".$order;
    }
    else
    {
        $sql .= " order by regi_date desc";
    }
    $sql .= " limit $offset,$view_count";
    
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $inner_count = count($db_result);
        for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
        {
            foreach($arr_db_field as $val)
            {
                $$val = $db_result[$db_loop][$val];
            }
            $shop_id = $db_result[$db_loop]["shop_id"];
            $regi_date = $db_result[$db_loop]["regi_date"];
            $up_date = $db_result[$db_loop]["up_date"];
            $voice_img_title = $db_result[$db_loop]["voice_img_title"];
            $img_1 = $db_result[$db_loop]["img_1"];
?>
									<div class="col-sm-3">
										<div class="galleryboxnaka">
                                            <? if($img_1!="") { ?>
                                            <a href="/<? echo global_voice_img_dir.$voice_img_id."/".$img_1."?d=".date(his);?>" class="gallery-link" title="<? echo $voice_img_title;?>">
                                                <div class="samimg">
                                                    <img src="/<? echo global_voice_img_dir.$voice_img_id."/".$img_1."?d=".date(his);?>" alt="image">
                                                </div>
                                            </a>
                                            <? } ?>
                            				<div class="caption"><? echo $voice_img_title;?></div>
											<div class="imguser">
                                            <?
                                            if($member_id==0)
                                            {
                                            ?>
                                            <span class="text-muted">お店から投稿</span>
                                            <?
                                            }
                                            else
                                            {

                                                $sql_member = " select nickname from member where member_id='".$member_id."' ";
                                                $db_result_member = $common_dao->db_query_bind($sql_member);
                                                if($db_result_member)
                                                {
                                            ?>
                                                <a href="<? echo "/masterdashboard/member/entry.php?member_id=".$member_id;?>."><? echo $db_result_member[0]["nickname"];?></a>
                                            <?
                                                }
                                            ?>
                                            <?
                                            }
                                            ?>
                                            </div>
                            			</div>
									</div>
<?
        }
    }
?>

									
								</div>
								
								
							</div>
							<!-- END Lightbox Gallery Content -->


						</div>
						<!-- END Widget Main -->

					</div>
				</div>



			</div>
			<!-- END CKEditor Block -->
		</div>


	</div>
	<!-- low -->
    <div class="text-right">
        <?php $common_connect -> Fn_paging_10_list($view_count, $all_count); ?>
    </div>
    
</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_footer.php'; ?>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="/app_manager/js/helpers/excanvas.min.js"></script><![endif]-->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_scripts.php'; ?>

<!-- プログレスバー-->
<script src="/app_manager/js/pages/formsWizard.js"></script>
<script>
	$( function () {
		FormsWizard.init();
	} );
</script>



<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_end.php'; ?>