<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/config.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/template_start.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/page_head.php";
	
	$common_connect -> Fn_shop_check();
	$shop_id = $_SESSION['shop_id'];
	
	
	//ショップの基本情報
	$arr_where = array();
	$result_login = $common_shop -> Fn_db_shop($shop_id, $arr_where);
	
	if($result_login)
	{
		foreach($result_login[0] as $key=>$value)
		{
			$$key = $value;
		}
	}
	else
	{
		$common_connect->Fn_javascript_back("IDとパスワードを確認してください。");
	}

	?>


<script src="/app_manager/js/vendor/jquery-1.12.0.min.js"></script>
<script type="text/javascript">
	$(function() {
		
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";
			
			err_check_count += check_input("open_time_to");
			err_check_count += check_input("job_comment");
			
			if(err_check_count!=0)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
				
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}

		function check_input_id($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkID($('#'+$str).val())==false)
			{
				err ="<span class='errorForm'>英数半角で入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if($('#'+$str).val().length<3)
			{
				err ="<span class='errorForm'>3文字以上で入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		//英数半角
		function checkID(value){
			if(value.match(/[^0-9a-z]+/) == null){
				return true;
			}else{
				return false;
			}
		} 

		//メールチェック
		function check_input_email($str_1) 
		{
			$("#err_"+$str_1).html(err_default);
			$("#"+$str_1).css(background,bgcolor_default);
			if($('#'+$str_1).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str_1).val()) == false)
			{
				err ="<span class='errorForm'>メールアドレスは半角英数字でご入力ください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}

		//メールチェック
		
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}
		
		function check_input_pw($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if($('#'+$str).val().length<6)
			{
				err ="<span class='errorForm'>６文字以上入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
						

	});
	
//-->
</script>

<!-- Page content -->
<div id="page-content">

<?php require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/page_headnavi.php"; ?>

    <!-- パンクズリスト -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="">管理画面</a></li>
        <li><a href="">各種情報更新</a></li>
        <li>基本情報</li>
    </ul>
    <!-- END Forms General Header -->






    <div class="row">



<!--残り文字数表示-->
    <script type="text/javascript">
    <!--
    
    $(function () {
        $('textarea.limited').maxlength({
            'feedback' : '.charsLeft' // note: looks within the current form
        });
        
        $('textarea.limited1').maxlength({
            'feedback' : '.charsLeft' // note: looks within the current form
        });
        
        $('textarea.limited2').maxlength({
            'feedback' : '.charsLeft' // note: looks within the current form
        });
        
        $('input.limited').maxlength({
            'feedback' : '.charsLeft' // note: looks within the current form
        });

        $('input.limited1').maxlength({
            'feedback' : '.charsLeft' // note: looks within the current form
        });

        $('input.limited2').maxlength({
            'feedback' : '.charsLeft' // note: looks within the current form
        });
        
        $('textarea.wordLimited').maxlength({
            'words': true,
            'feedback': '.wordsLeft'
        });
    });
    
    //-->
    </script>
<!--残り文字数表示-->






<!--右ナビゲーションimg-->
	<div class="col-sm-3">
            <div class="block">
		<!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2>橙色の部分に反映されます</h2>
                </div>
                <!-- END Form Elements Title -->
		<div class="block"><p><img src="/app_manager/img/navimg/maindeta.png" class="img-responsive center-block"></p></div>
	    </div>
	</div>

<!--右ナビゲーションimg-->





<!--右店舗入力フォーム-->


        <div class="col-md-9">

            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2>お店の基本情報入力</h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form action="shop_save.php" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                <? $var = "shop_id";?>
                <input name="session_shop_id" value="<? echo $$var;?>" type="hidden" >


<!-- キャッチッコピー -->

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-text-input">キャッチコピー</label>
                        <div class="col-md-9">
                        		<? $var = "catchcopy";?>
                            <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" cols="26" maxlength="26" class="form-control demo" placeholder="例)女性スタッフ多数でアットホームな職場">
                            <span class="help-block">検索結果のお店一覧などで表示されるキャッチコピーを26文字以内で入力して下さい。</span>
                        </div>
                    </div>


<!-- 営業時間 -->
                    <div class="form-group">
                      <label class="col-md-3 control-label" for="example-daterange1">お店の営業時間</label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <? $var = "open_time_from";?>
                          <?
                            $arr_open_time_to = array('朝6時から', '朝7時から', '朝8時から', '朝9時から', '10時から', '11時から', '12時から', '13時から', '14時から', '15時から', '16時から', '17時から', '18時から', '19時から', '20時から', '21時から', '22時から', '23時から', '24時から', '深夜から', '早朝から', 'お昼から', '不定期', '24時間営業');
                          ?>
                          <select name="<? echo $var;?>" id="<? echo $var;?>" class="form-control">
                              <option value="">開始時間</option>
                              <? foreach($arr_open_time_to as $key=>$value) { ?>
                              <option value="<? echo $value?>" <? if($value==$$var) { echo " selected ";}?>><? echo $value?></option>
                              <? } ?>
                              </select>

                            <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>

                          <? $var = "open_time_to";?>
                          <?
                            $arr_open_time_to = array('朝6時まで', '朝7時まで', '朝8時まで', '朝9時まで', '10時まで', '11時まで', '12時まで', '13時まで', '14時まで', '15時まで', '16時まで', '17時まで', '18時まで', '19時まで', '20時まで', '21時まで', '22時まで', '23時まで', '24時まで', '深夜まで', '早朝まで', 'お昼まで', '不定期', '24時間営業');
                          ?>
                          <select name="<? echo $var;?>" id="<? echo $var;?>" class="form-control">
                              <option value="">終了時間</option>
                              <? foreach($arr_open_time_to as $key=>$value) { ?>
                              <option value="<? echo $value?>" <? if($value==$$var) { echo " selected ";}?>><? echo $value?></option>
                              <? } ?>
                            </select>
                        </div>
                        <span class="help-block">お店の営業時間を選択して下さい。(24時間)の場合は開始だけ選択して下さい</span>
                        <label id="err_<?php echo $var;?>"></label>
                      </div>
                     </div>

<!-- 住所 -->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-text-input">住所</label>
                        <div class="col-md-9">
                        		<? $var = "shop_address";?>
                            <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" cols="36" maxlength="36" class="form-control demo1" placeholder="例)東京都渋谷区道玄坂付近(渋谷駅南口徒歩5分)">
                            <span class="help-block">お店や事務所の住所をある程度まで記載。または最寄り駅名。36文字まで。</span>
                        </div>
                    </div>

<!-- 電話番号 -->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-email-input">電話番号</label>
                        <div class="col-md-9">
                        		<? $var = "shop_tel";?>
                            <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" maxlength="13" class="form-control demo2" placeholder="例)03-000-0000">
                            <span class="help-block">直接お問合せする場合の電話番号。</span>
                        </div>
                    </div>

<!-- メールアドレス -->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-email-input">メールアドレス</label>
                        <div class="col-md-9">
                        		<? $var = "email_user";?>
                            <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="email" maxlength="36" class="form-control demo3" placeholder="例)cossot@gmail.com">
                            <span class="help-block">直接お問合せする場合のメールアドレス。</span>
                        </div>
                    </div>
<!-- お仕事内容 -->

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-textarea-input">お仕事の内容</label>
                        <div class="col-md-9">
                        		<? $var = "job_comment";?>
                            <textarea name="<? echo $var;?>" id="<? echo $var;?>" rows="2" cols="72" rap="hard" maxlength="72" class="form-control demo4" placeholder="例)当店のお仕事内容はソープランドという業種になりますので..."><? echo $$var;?></textarea>
                            <span class="help-block">お店の仕事内容を最大72文字以内で記載して下さい。改行も1文字に含まれます。</span>
                            <label id="err_<?php echo $var;?>"></label>
                        </div>
                    </div>


<!-- お給料体形 -->
                    <div class="form-group">
                            <label class="col-md-3 control-label" for="example-daterange1">お給料体系</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                	<? $var = "job_salary_pre";?>
                                	<?
																	$arr_job_salary_pre = array("単価制", "時給制", "日給制", "月給制", "その他");
                                  ?>
                                  <select name="<? echo $var;?>" id="<? echo $var;?>" class="form-control">
                                    	<option value="">計算方法</option>
                                      <? foreach($arr_job_salary_pre as $key=>$value) { ?>
                                    	<option value="<? echo $value?>" <? if($value==$$var) { echo " selected ";}?>><? echo $value?></option>
                                      <? } ?>
                                    </select>

                                    <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>
                                	<? $var = "job_salary";?>
                                    <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" cols="36" maxlength="36" class="form-control demo5" placeholder="例)60分8500円～">
                                </div>
				<span class="help-block">お給料の計算方法と代表的な料金の例を入力してください。</span>
                            </div>
                     </div>
<!-- 業務形態 -->
			<div class="form-group">
                            <label class="col-md-3 control-label" for="val_skill">業務形態</label>
                            <div class="col-md-9">
                                	<? $var = "job_style";?>
                                	<?
																	$arr_job_style = array("店舗型", "デリバリー型", "その他");
                                  ?>
                                  <select name="<? echo $var;?>" id="<? echo $var;?>" class="form-control">
                                    <option value="">集客スタイル</option>
                                      <? foreach($arr_job_style as $key=>$value) { ?>
                                    	<option value="<? echo $value?>" <? if($value==$$var) { echo " selected ";}?>><? echo $value?></option>
                                      <? } ?>
                                </select>
                            </div>
                        </div>

<!-- 募集年齢 -->
                    <div class="form-group">
                            <label class="col-md-3 control-label" for="example-daterange1">募集資格年齢</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                	<? $var = "job_years_from";?>
                                  <select name="<? echo $var;?>" id="<? echo $var;?>" class="form-control">
                                    	<option value="">何歳から</option>
                                      <? for($loop=18 ; $loop<=44 ; $loop++) { ?>
                                    	<option value="<? echo $loop;?>歳" <? if($loop."歳"==$$var) { echo " selected ";}?>><? echo $loop;?>歳</option>
                                      <? } ?>
                                    	<option value="45歳以上" <? if("45歳以上"==$$var) { echo " selected ";}?>>45歳以上</option>
                                    	<option value="不問" <? if("不問"==$$var) { echo " selected ";}?>>不問</option>
                                    </select>

                                    <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>

                                	<? $var = "job_years_to";?>
                                  <select name="<? echo $var;?>" id="<? echo $var;?>" class="form-control">
                                    	<option value="">何歳位まで</option>
                                      <? for($loop=19 ; $loop<=44 ; $loop++) { ?>
                                    	<option value="<? echo $loop;?>歳位まで" <? if($loop."歳位まで"==$$var) { echo " selected ";}?>><? echo $loop;?>歳位まで</option>
                                      <? } ?>
                                    	<option value="45歳以上" <? if("45歳以上"==$$var) { echo " selected ";}?>>45歳以上</option>
                                    	<option value="不問" <? if("不問"==$$var) { echo " selected ";}?>>不問</option>
                                    </select>
                                </div>
                                <span class="help-block">(不問)と(45歳以上)の場合は開始だけ選択してください。</span>
                            </div>
                     </div>

<!-- 募集時間 -->
                    <div class="form-group">
                            <label class="col-md-3 control-label" for="example-daterange1">特に募集したい時間帯</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                	<? $var = "job_time_from";?>
                                	<?
																	$arr_job_time_from = array("朝6時から", "朝7時から", "朝8時から", "朝9時から", "10時から", "11時から", "12時から", "13時から", "14時から", "15時から", "16時から", "17時から", "18時から", "19時から", "20時から", "21時から", "22時から", "23時から", "24時から", "深夜から", "早朝から", "お昼から", "営業時間帯全て", "24時間全て");
                                  ?>
                                  <select name="<? echo $var;?>" id="<? echo $var;?>" class="form-control">
                                    	<option value="">開始時間</option>
                                      <? foreach($arr_job_time_from as $key=>$value) { ?>
                                    	<option value="<? echo $value?>" <? if($value==$$var) { echo " selected ";}?>><? echo $value?></option>
                                      <? } ?>
                                    	</select>

                                    <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>

                                	<? $var = "job_time_to";?>
                                	<?
										$arr_job_time_to = array("朝6時まで", "朝7時まで", "朝8時まで", "朝9時まで", "10時まで", "11時まで", "12時まで", "13時まで", "14時まで", "15時まで", "16時まで", "17時まで", "18時まで", "19時まで", "20時まで", "21時まで", "22時まで", "23時まで", "24時まで", "深夜まで", "早朝まで", "お昼まで", "営業時間帯全て", "24時間全て");
                                  ?>
                                  <select name="<? echo $var;?>" id="<? echo $var;?>" class="form-control">
                                    	<option value="">終了時間</option>
                                      <? foreach($arr_job_time_to as $key=>$value) { ?>
                                    	<option value="<? echo $value?>" <? if($value==$$var) { echo " selected ";}?>><? echo $value?></option>
                                      <? } ?>
                                    </select>
                                </div>
                                <span class="help-block">お店の営業時間内で特に女の子を募集してる時間帯を選んで下さい。(営業時間帯全て)(24時間全て)の場合は開始だけ選択してください。</span>
                            </div>
                     </div>
<!-- 待遇 -->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-text-input">待遇</label>
                        <div class="col-md-9">
                        	<? $var = "job_special";?>
                          <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" cols="36" maxlength="36" class="form-control demo6" placeholder="例)送迎有り、託児所完備、など">
                          <span class="help-block">お店の待遇面を記入してください。36文字まで。</span>
                        </div>
                    </div>

<!-- 営業用オフィシャルページ -->
                    <div class="form-group">
                     <label class="col-md-3 control-label" for="example-password-input">営業オフィシャルページ</label>
                     <div class="col-md-9">
                        <div class="input-group">
                        	<? $var = "hp_offical";?>
                          <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" maxlength="70" class="form-control demo7" placeholder="http://　から入力してください。">
                          <span class="help-block">お店の営業ページのURLを記載して下さい。</span>
                        </div>
                      </div>
                    </div>
<!-- 求人用オフィシャルページ -->
                    <div class="form-group">
                     <div class="form-group">
                     <label class="col-md-3 control-label" for="example-email-input">求人オフィシャルページ</label>
                     <div class="col-md-9">
                        <div class="input-group">
                        	<? $var = "hp_job";?>
                          <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" maxlength="70" class="form-control demo8" placeholder="http://　から入力してください。">
                          <span class="help-block">お店の求人サイトのURLを記載して下さい。</span>
                        </div>
                      </div>
                    </div>

<!-- 罰金ノルマ -->

                    <div class="form-group">
                      <label class="col-md-3 control-label" for="example-text-input">罰金ノルマ等</label>
                      <div class="col-md-9">
                        	<? $var = "penalty";?>
                        <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" cols="36" maxlength="36" class="form-control demo9" placeholder="例)無断欠勤の場合は罰金が発生する場合があります">
                        <span class="help-block">お店の罰金やノルマ、ルールなどがあれば記載して下さい。36文字まで。</span>
                      </div>
                    </div>

<!-- お店からのPRコメント -->

                    <div class="form-group">
                      <label class="col-md-3 control-label" for="example-textarea-input">お店からのPRコメント</label>
                      <div class="col-md-9">
                        	<? $var = "shop_pr";?>
                        <textarea name="<? echo $var;?>" id="<? echo $var;?>" rows="3" cols="36" rap="hard" maxlength="108" class="form-control demo10" placeholder="例)まだオープンして間もないですが、スタッフ全員が仲が良く..."><? echo $$var;?></textarea>
                        <span class="help-block">お店のPRコメントを最大108文字以内で記載して下さい。改行も1文字に含まれます。</span>
                      </div>
                    </div>



                    <div class="form-group form-actions">
                      <div class="col-md-9 col-md-offset-3">
                        <button type="submit" class="btn btn-sm btn-primary" id="form_confirm"><i class="fa fa-angle-right"></i> 登録</button>
                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> 修正</button>
                      </div>
                    </div>

                </form>
                <!-- END Basic Form Elements Content -->

            </div>
            <!-- END Block -->

        </div>
	<!--col-md-8-->
<!--右店舗入力フォーム-->




    </div>
       <!--low-->

</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/page_footer.php"; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/template_scripts.php"; ?>
<!--文字カウントダウン-->
<script src="/app_manager/js/tool/count/bootstrap-maxlength.js"></script>
<script>
$(document).ready(function () {
  $('.demo,.demo1,.demo2,.demo3,.demo4,.demo5,.demo6,.demo7,.demo8,.demo9,.demo10').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/formsGeneral.js"></script>
<script>$(function(){ FormsGeneral.init(); });</script>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/template_end.php"; ?>