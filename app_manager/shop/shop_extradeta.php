<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/config.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/template_start.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/page_head.php";
	 
	 
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}
	
	$common_connect -> Fn_shop_check();
	$shop_id = $_SESSION['shop_id'];
	

	$where = " and shop_id='".$shop_id."' ";
	$sql = "SELECT pr_comment, line_id FROM shop where 1 ".$where ;
	
	$db_result = $common_dao->db_query_bind($sql);
	if($db_result)
	{
		$pr_comment = $db_result[0]["pr_comment"];
		$line_id = $db_result[0]["line_id"];
	}
?>

<!-- Page content -->
<div id="page-content">

<?php require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/page_headnavi.php"; ?>

    <!-- パンクズリスト -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="">管理画面</a></li>
        <li><a href="">各種情報更新</a></li>
        <li>基本情報</li>
    </ul>
    <!-- END Forms General Header -->






    <div class="row">


<!--右ナビゲーションimg-->
	<div class="col-sm-3">
            <div class="block">
		<!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2>橙色の部分に反映されます</h2>
                </div>
                <!-- END Form Elements Title -->
		<div class="block"><p><img src="/app_manager/img/navimg/exdeta.png" class="img-responsive center-block"></p></div>
	    </div>
	</div>

<!--右ナビゲーションimg-->





<!--右店舗入力フォーム-->


        <div class="col-md-9">

            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2>有料店用の情報入力</h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form action="shop_extradeta_save.php" method="post" class="form-horizontal form-bordered" >
                <? $var = "shop_id";?>
                <input type="hidden" id="<? echo $var;?>" name="<? echo $var;?>" value="<? echo $shop_id;?>">



<!--折り畳みPRコメント -->

                  <div class="form-group">
                  	<? $var = "pr_comment";?>
                    <label class="col-md-3 control-label" for="example-textarea-input">PRコメント</label>
                    <div class="col-md-9">
                      <textarea id="<? echo $var;?>" name="<? echo $var;?>"  rows="10" cols="48" maxlength="480" rap="hard" class="form-control demo" placeholder="例)どこよりも高待遇で、働きやすい職場を..."><? echo $$var;?></textarea>
                      <span class="help-block">お店のPRコメントを入力出来ます。48文字で改行されスペースや改行も一文字と計算されます。</span>
                    </div>
                  </div>




<!-- ラインID -->
                    <div class="form-group">
                    	<? $var = "line_id";?>
                      <label class="col-md-3 control-label" for="example-text-input">ラインID</label>
                      <div class="col-md-9">
                          <input type="text" id="<? echo $var;?>" name="<? echo $var;?>" value="<? echo $$var;?>" cols="20" maxlength="20" class="form-control demo2" placeholder="cossot">
                          <span class="help-block">女の子とやり取り出来るラインのIDを入力出来ます。半角英数20文字まで</span>
                      </div>
                    </div>



                    <div class="form-group form-actions">
                      <div class="col-md-9 col-md-offset-3">
                        <button type="submit" class="btn btn-sm btn-primary" id="form_confirm"><i class="fa fa-angle-right"></i> 登録</button>
                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> 修正</button>
                      </div>
                    </div>



                </form>
                <!-- END Basic Form Elements Content -->

            </div>
            <!-- END Block -->

        </div>
	<!--col-md-8-->
<!--右店舗入力フォーム-->




    </div>
       <!--low-->

</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/page_footer.php"; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/template_scripts.php"; ?>
<!--文字カウントダウン-->
<script src="/app_manager/js/tool/count/bootstrap-maxlength.js"></script>
<script>
$(document).ready(function () {
  $('.demo,.demo2').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/formsGeneral.js"></script>
<script>$(function(){ FormsGeneral.init(); });</script>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/template_end.php"; ?>