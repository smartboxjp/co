<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>メイン画像登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	$common_connect -> Fn_shop_check();
	$session_shop_id = $_SESSION['shop_id'];
	
	foreach($_POST as $key => $value)
	{ 
		//$$key = trim($common_dao->db_string_escape($value));
		$$key = $value;
	}
	
	if($session_shop_id!=$shop_id)
	{
		$common_connect-> Fn_javascript_back("変更するログイン情報が違います。再度ログインしてください。");
	}
	
	//リスト表示
	$arr_db_field = array("pr_comment", "line_id");
	
	$datetime = date("Y/m/d H:i:s");
	$dbup = "update shop set ";
	foreach($arr_db_field as $val)
	{
		$dbup .= $val."='".trim($common_dao->db_string_escape($$val))."', ";
	}
	$dbup .= " up_date='$datetime' where shop_id='".$shop_id."'";
	
	$db_result = $common_dao->db_update($dbup);

	
	$common_connect-> Fn_javascript_move("情報を更新しました", "shop_extradeta.php");
?>
</body>
</html>