<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/config.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/template_start.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/page_head.php";
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShopImg.php";
	$common_shop_img = new CommonShopImg();
	
	foreach($_GET as $key => $value)
	{ 
		//$$key = trim($common_dao->db_string_escape($value));
		$$key = $value;
	}
	
	$common_connect -> Fn_shop_check();
	$shop_id = $_SESSION['shop_id'];
	
	if($s_shop_img_id!="")
	{
		//ショップの基本情報
		$arr_where = array();
		$arr_where["shop_img_id"] = $s_shop_img_id;
		$result_db = $common_shop_img -> Fn_db_shop($shop_id, $arr_where);
		
		if($result_db)
		{
			foreach($result_db as $arr_shop_img)
			{
				foreach($arr_shop_img as $key=>$value)
				{
					$$key = $value;
				}
			}
		}
		
	}
?>
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
<script type="text/javascript">
	$(function() {
			
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";
			
			err_check_count += check_input("view_level");
			
			if(err_check_count!=0)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<span class='errorForm'>※未選択</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		

	});
	
	
	
//-->
</script>
<script language="javascript"> 
	function fnChangeDel(i, j) { 
		var result = confirm('削除しますか？'); 
		if(result){ 
			document.location.href = './shop_extramainimg_del.php?s_shop_img_id='+i+'&img='+j;
		} 
	}
</script>
<!-- Page content -->
<div id="page-content">

<?php require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/page_headnavi.php"; ?>

    <!-- パンクズリスト -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="">管理画面</a></li>
        <li><a href="">各種情報更新</a></li>
        <li>お店のサムネイル画像登録</li>
    </ul>
    <!-- END Forms General Header -->






    <div class="row">




<!--右ナビゲーションimg-->
	<div class="col-sm-3">
            <div class="block">
		<!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2>橙色の部分に反映されます</h2>
                </div>
                <!-- END Form Elements Title -->
		<div class="block"><p><img src="/app_manager/img/navimg/mainimg.png" class="img-responsive center-block"></p></div>
	    </div>
	</div>

<!--右ナビゲーションimg-->




<!---->


        <div class="col-md-9">




<!--ブロック-->
            <div class="block">
                
<form action="./shop_extramainimg_save.php" method="POST" name="form_write" id="form_regist" enctype="multipart/form-data">
	<? $var = "s_shop_img_id"; ?>
  <input type="hidden" id="<? echo $var;?>" name="<? echo $var;?>" value="<? echo $$var;?>">
<!--イメージクリッパー-->
  <div class="block-title">
    <h2>お店のメイン画像(670px×377px)を登録する事が出来ます。</h2>
  </div>
  
  <div class="ibox-content">
  
    <div class="row">
      <div class="col-md-8">
        <div class="image-crop">
          <img src="/app_manager/img/default/picture.jpg">
        </div>

      </div>
      
      <div class="col-md-4">
        <h4>Preview image</h4>
        <div class="img-preview img-preview-main"></div>
        <p>
          画像の好きな位置に枠を合わせて下さい。<br>
          枠は好きな大きさに変えられます。
        </p>
      
        <div class="btn-group">
          <button class="btn btn-primary" id="zoomIn" type="button"><i class="fa fa-search-plus"></i></button>
          <button class="btn btn-primary" id="zoomOut" type="button"><i class="fa fa-search-minus"></i></button>
          <button class="btn btn-primary" id="rotateLeft" type="button"><i class="fa fa-rotate-left"></i></button>
          <button class="btn btn-primary" id="rotateRight" type="button"><i class="fa fa-rotate-right"></i></button>
          <label title="画像をアップロードする" for="inputImage" class="btn btn-primary">
            <input type="file" accept="image/*" name="file" id="inputImage" class="hide">
            <i class="fa fa-upload"></i>
          </label>
        </div>
	<br>
	<br>
        <div class="onoffswitch">
        	<? $var = "check_img";?>
		<label class="switch switch-primary">
			<input type="checkbox" name="<? echo $var;?>" value="1" class="onoffswitch-checkbox" id="<? echo $var;?>">
			<span class="onoffswitch-inner"></span>
		</label>
	サムネイルを反映する
        </div>
    

    </div>
  </div>
  <input type="hidden" name="cropped" class="cropped">

    <table class="table table-vcenter table-striped">
    <tbody>
      <tr>
        <td>
        <?
        if($img!="")
        {
            echo "<img src='/".global_shop_dir.$shop_id."/".$img."?".date("his")."' width=\"150\">";
        }
        ?>
        </td>

        <td>
          <? $var = "comment"; ?>
          <span id="err_<?php echo $var;?>" class="errtext"></span>
          <input type="text" id="<? echo $var;?>" name="<? echo $var;?>" value="<? echo $$var;?>" cols="30" maxlength="30" class="form-control demo" placeholder="各画像にコメントを添えられます">
        </td>

        <td>
					<? $var = "view_level"; ?>
          <span id="err_<?php echo $var;?>" class="errtext"></span><!--エラーメッセージ-->
          <select id="<? echo $var;?>" name="<? echo $var;?>"  class="form-control">
            <option value="">表示順</option>
            <? for($loop=1 ; $loop<=5 ;  $loop++) { ?>
            <option value="<? echo $loop;?>" <? if($$var == $loop) { echo " selected ";}?>><? echo $loop;?>番目</option>
            <? } ?>
          </select>
        </td>
        <td class="text-center">
        	<button class="btn btn-warning" id="form_confirm" type="submit">これで決定</button>
        </td>
      </tr>




     </tbody>
    </table>

<!--イメージクリッパー-->
</form>


            </div>
            <!--ブロック -->





      <!--登録画像-->
      <div class="block">
        <!-- Basic Form Elements Title -->
        <div class="block-title">
          <h2>アップされた画像の並び替え、削除が出来きます。</h2>
        </div>
        <!-- END Form Elements Title -->
      
      
        <!-- 画像テーブル -->
        
        
        <!-- Responsive Full Content -->
        <div class="table-responsive">
          <table class="table table-vcenter table-striped">
            <tbody>
            <?
			$where = " and shop_id='".$shop_id."' ";
			$sql = "SELECT shop_img_id, img, comment, up_date, view_level FROM shop_img where 1 ".$where ;
			$sql .= " order by view_level ";
			
			$db_result = $common_dao->db_query_bind($sql);
			if($db_result)
			{
				for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
				{
					$shop_img_id = $db_result[$db_loop]["shop_img_id"];
					$img = $db_result[$db_loop]["img"];
					$comment = $db_result[$db_loop]["comment"];
					$view_level = $db_result[$db_loop]["view_level"];
			?>
              <tr>
                <td><div class="imgno"><? echo $db_loop+1?></div></td>
                <td>
                <?
                if($img!="")
                {
									echo "<img src='/".global_shop_dir.$shop_id."/".$img."?".date("his")."' width=\"150\">";
                }
                ?>
                </td>
                <td>
                <? echo $comment;?>
                </td>
                <td>
                <? echo $view_level;?>
                </td>
                <td class="text-center">
                  <div class="btn-group btn-group-xs">
                    <a href="	<?=$_SERVER["PHP_SELF"];?>?s_shop_img_id=<? echo $shop_img_id;?>" data-toggle="tooltip" title="修正" class="btn btn-default"><i class="fa fa-upload"></i></a>
                    <a href="#" onClick='fnChangeDel("<?php echo $shop_img_id;?>", "<? echo $img;?>");' data-toggle="tooltip" title="削除" class="btn btn-danger"><i class="fa fa-times"></i></a>
                  </div>
                </td>
              </tr>
              <?
							}
						}
              ?>
              
            </tbody>
          </table>
        </div>
        <!-- 画像テーブル -->
      
      </div>
      <!--登録画像-->








        </div>
	<!--col-md-8-->


    </div>
       <!--low-->

</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/page_footer.php"; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/template_scripts.php"; ?>
<!--文字カウントダウン-->
<script src="/app_manager/js/tool/count/bootstrap-maxlength.js"></script>
<script>
$(document).ready(function () {
  $('.demo,.demo2').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>

<!-- Load and execute javascript code used only in this page -->
<script src="/app_manager/js/pages/formsGeneral.js"></script>
<script>$(function(){ FormsGeneral.init(); });</script>

<script src="/app_manager/js/tool/cropper/cropper.min.js"></script>
    <script>//トリミング
        $(document).ready(function(){

            var $image = $(".image-crop > img")
            $($image).cropper({
                aspectRatio: 1.7777777777777777,
                preview: ".img-preview",
                done: function(data) {
                    // Output the result data for cropping image.
										$('.cropped').val($image.cropper("getDataURL"));
                }
            });

            var $inputImage = $("#inputImage");
            if (window.FileReader) {
                $inputImage.change(function() {
                    var fileReader = new FileReader(),
                            files = this.files,
                            file;

                    if (!files.length) {
                        return;
                    }

                    file = files[0];

                    if (/^image\/\w+$/.test(file.type)) {
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function () {
													$('#check_img').prop("checked",true); 
                            $inputImage.val("");
                            $image.cropper("reset", true).cropper("replace", this.result);
                        };
                    } else {
                        showMessage("Please choose an image file.");
                    }
                });
            } else {
                $inputImage.addClass("hide");
            }

            $("#download").click(function() {
                window.open($image.cropper("getDataURL"));
            });

            $("#zoomIn").click(function() {
                $image.cropper("zoom", 0.1);
            });

            $("#zoomOut").click(function() {
                $image.cropper("zoom", -0.1);
            });

            $("#rotateLeft").click(function() {
                $image.cropper("rotate", 45);
            });

            $("#rotateRight").click(function() {
                $image.cropper("rotate", -45);
            });

            $("#setDrag").click(function() {
                $image.cropper("setDragMode", "crop");
            });

            $('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true
            });

            $('#data_2 .input-group.date').datepicker({
                startView: 1,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                format: "dd/mm/yyyy"
            });

            $('#data_3 .input-group.date').datepicker({
                startView: 2,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });

            $('#data_4 .input-group.date').datepicker({
                minViewMode: 1,
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                todayHighlight: true
            });

            $('#data_5 .input-daterange').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });

            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, { color: '#1AB394' });

            var elem_2 = document.querySelector('.js-switch_2');
            var switchery_2 = new Switchery(elem_2, { color: '#ED5565' });

            var elem_3 = document.querySelector('.js-switch_3');
            var switchery_3 = new Switchery(elem_3, { color: '#1AB394' });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green'
            });

            $('.demo1').colorpicker();

            var divStyle = $('.back-change')[0].style;
            $('#demo_apidemo').colorpicker({
                color: divStyle.backgroundColor
            }).on('changeColor', function(ev) {
                        divStyle.backgroundColor = ev.color.toHex();
                    });

            $('.clockpicker').clockpicker();

            $('input[name="daterange"]').daterangepicker();

            $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

            $('#reportrange').daterangepicker({
                format: 'MM/DD/YYYY',
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2015',
                dateLimit: { days: 60 },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'right',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-primary',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Cancel',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            });

            $(".select2_demo_1").select2();
            $(".select2_demo_2").select2();
            $(".select2_demo_3").select2({
                placeholder: "Select a state",
                allowClear: true
            });


            $(".touchspin1").TouchSpin({
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

            $(".touchspin2").TouchSpin({
                min: 0,
                max: 100,
                step: 0.1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                postfix: '%',
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

            $(".touchspin3").TouchSpin({
                verticalbuttons: true,
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });


        });
        var config = {
                '.chosen-select'           : {},
                '.chosen-select-deselect'  : {allow_single_deselect:true},
                '.chosen-select-no-single' : {disable_search_threshold:10},
                '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                '.chosen-select-width'     : {width:"95%"}
                }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }

        $("#ionrange_1").ionRangeSlider({
            min: 0,
            max: 5000,
            type: 'double',
            prefix: "$",
            maxPostfix: "+",
            prettify: false,
            hasGrid: true
        });

        $("#ionrange_2").ionRangeSlider({
            min: 0,
            max: 10,
            type: 'single',
            step: 0.1,
            postfix: " carats",
            prettify: false,
            hasGrid: true
        });

        $("#ionrange_3").ionRangeSlider({
            min: -50,
            max: 50,
            from: 0,
            postfix: "°",
            prettify: false,
            hasGrid: true
        });

        $("#ionrange_4").ionRangeSlider({
            values: [
                "January", "February", "March",
                "April", "May", "June",
                "July", "August", "September",
                "October", "November", "December"
            ],
            type: 'single',
            hasGrid: true
        });

        $("#ionrange_5").ionRangeSlider({
            min: 10000,
            max: 100000,
            step: 100,
            postfix: " km",
            from: 55000,
            hideMinMax: true,
            hideFromTo: false
        });

        $(".dial").knob();

        $("#basic_slider").noUiSlider({
            start: 40,
            behaviour: 'tap',
            connect: 'upper',
            range: {
                'min':  20,
                'max':  80
            }
        });

        $("#range_slider").noUiSlider({
            start: [ 40, 60 ],
            behaviour: 'drag',
            connect: true,
            range: {
                'min':  20,
                'max':  80
            }
        });

        $("#drag-fixed").noUiSlider({
            start: [ 40, 60 ],
            behaviour: 'drag-fixed',
            connect: true,
            range: {
                'min':  20,
                'max':  80
            }
        });


    </script>



<?php require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/inc/template_end.php"; ?>