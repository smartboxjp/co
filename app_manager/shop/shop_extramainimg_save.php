<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>サブイメージ登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	$common_connect -> Fn_shop_check();
	$shop_id = $_SESSION['shop_id'];
	
	foreach($_POST as $key => $value)
	{ 
		//$$key = trim($common_dao->db_string_escape($value));
		$$key = $value;
	}
	
	if($comment == "")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
	
	$datetime = date("Y/m/d H:i:s");
	//array
	$arr_db_field = array( "comment", "view_level");

	//基本情報
	if($s_shop_img_id=="")
	{
		$db_insert = "insert into shop_img ( ";
		$db_insert .= " shop_id, ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val.", ";
		}
		$db_insert .= " regi_date, up_date ";
		$db_insert .= " ) values ( ";
		$db_insert .= "'".$shop_id."' , ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= " '".trim($common_dao->db_string_escape($$val))."', ";
		}
		$db_insert .= " '$datetime', '$datetime')";
	}
	else
	{
		$db_insert = "update shop_img set ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val."='".trim($common_dao->db_string_escape($$val))."', ";
		}
		$db_insert .= " up_date='".$datetime."' ";
		$db_insert .= " where shop_id='".$shop_id."' and shop_img_id='".$s_shop_img_id."'";
	}
	$db_result = $common_dao->db_update($db_insert);


	if ($s_shop_img_id == "")
	{
		//自動生成されてるID出力
		$sql = "select last_insert_id() as last_id";  
		$db_result = $common_dao->db_query_bind($sql);
		if($db_result)
		{
			$s_shop_img_id = $common_connect -> str_htmlspecialchars($db_result[0]["last_id"]);
		}
	}
	
	if($check_img=="1")
	{
		//Folder生成
		$up_file_name = "shop_img_".$s_shop_img_id.".png";
		$save_dir = $global_path.global_shop_dir.$shop_id."/";
		$fname_new_name[1] = $up_file_name;
		
		//Folder生成
		$common_image -> create_folder ($save_dir);
		//ヘッダに「data:image/png;base64,」が付いているので、それは外す
		$cropped = preg_replace("/data:[^,]+,/i","",$cropped);
		
		//残りのデータはbase64エンコードされているので、デコードする
		$cropped = base64_decode($cropped);
		
		//まだ文字列の状態なので、画像リソース化
		$image = imagecreatefromstring($cropped);
	 
		imagesavealpha($image, TRUE); // 透明色の有効
		imagepng($image ,$save_dir.$fname_new_name[1]);
		
		$common_image -> create_thumbnail($save_dir.$up_file_name, "", $save_dir, 670);
		/*
		$new_end_name="_1";
		$fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $shop_id, $text_img_1, "");
		*/
		if($fname_new_name[1]!="")
		{
			$dbup = "update shop_img set ";
			$dbup .= " img='".$fname_new_name[1]."',";
			$dbup .= " up_date='$datetime' where shop_id='".$shop_id."' and shop_img_id='".$s_shop_img_id."'";
	
			$db_result = $common_dao->db_update($dbup);
		}
	}
	
	$common_connect-> Fn_javascript_move("サブイメージを登録・修正しました", "shop_extramainimg.php");
?>
</body>
</html>