<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>メイン画像登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	$common_connect -> Fn_shop_check();
	$shop_id = $_SESSION['shop_id'];
	
	foreach($_POST as $key => $value)
	{ 
		//$$key = trim($common_dao->db_string_escape($value));
		$$key = $value;
	}
	

	//Folder生成
	$up_file_name = "main_img.png";
	$save_dir = $global_path.global_shop_dir.$shop_id."/";
	$fname_new_name[1] = $up_file_name;
	
	//Folder生成
	$common_image -> create_folder ($save_dir);
	//ヘッダに「data:image/png;base64,」が付いているので、それは外す
	$cropped = preg_replace("/data:[^,]+,/i","",$cropped);
	
	//残りのデータはbase64エンコードされているので、デコードする
	$cropped = base64_decode($cropped);
	
	//まだ文字列の状態なので、画像リソース化
	$image = imagecreatefromstring($cropped);
 
	imagesavealpha($image, TRUE); // 透明色の有効
	imagepng($image ,$save_dir.$fname_new_name[1]);
	
	$common_image -> create_thumbnail($save_dir.$up_file_name, "", $save_dir, 670);
	/*
	$new_end_name="_1";
	$fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $shop_id, $text_img_1, "");
	*/
	
	$datetime = date("Y/m/d H:i:s");
	//
	$arr_where = array();
	$arr_where["shop_id"] = $shop_id;
	
	$main_img = $fname_new_name[1];
	$up_date = $datetime;
	
	//array
	$arr_db_field = array("shop_thumbnail", "up_date");
	
	foreach($arr_db_field as $value) {
		$arr_data[$value] = $$value;
	}
	$common_shop->Fn_shop_update ($arr_data, $arr_where);
	
	$common_connect-> Fn_javascript_move("ショップ登録・修正しました", "shop_mainimg.php");
?>
</body>
</html>