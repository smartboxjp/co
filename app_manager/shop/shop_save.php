<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>店舗登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	//ショップチェック
	$common_connect -> Fn_shop_check();
	$shop_id = $_SESSION['shop_id'];
	
	foreach($_POST as $key => $value)
	{ 
		//$$key = trim($common_dao->db_string_escape($value));
		$$key = htmlspecialchars($value);
	}
	
	if($session_shop_id!=$shop_id)
	{
		$common_connect-> Fn_javascript_back("変更するログイン情報が違います。再度ログインしてください。");
	}

	if($open_time_to == "" || $job_comment == "")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}

	
	$datetime = date("Y/m/d H:i:s");
	$user_agent = substr(getenv("HTTP_USER_AGENT"), 0, 250);
	
	//基本情報
	$arr_where = array();
	$arr_where["shop_id"] = $shop_id;
	
	//array
	$arr_db_field = array("catchcopy", "shop_address", "shop_tel", "job_comment", "job_salary_pre", "job_price");
	$arr_db_field = array_merge($arr_db_field, array("job_salary", "job_style", "job_years_from", "job_years_to"));
	$arr_db_field = array_merge($arr_db_field, array("job_time_from", "job_time_to", "job_special", "hp_job", "hp_offical"));
	$arr_db_field = array_merge($arr_db_field, array("penalty", "job_url", "email_kanri", "email_user"));
	$arr_db_field = array_merge($arr_db_field, array("open_time_from", "open_time_to", "shop_pr"));
	$arr_db_field = array_merge($arr_db_field, array("user_agent"));
	
	foreach($arr_db_field as $value) {
		$arr_data[$value] = $$value;
	}

	$common_shop->Fn_shop_update ($arr_data, $arr_where);
	



	if ($shop_id == "")
	{
		//自動生成されてるID出力
		$sql = "select last_insert_id() as last_id";  
		$db_result = $common_dao->db_query_bind($sql);
		if($db_result)
		{
			$shop_id = $common_connect -> str_htmlspecialchars($db_result[0]["last_id"]);
		}
	}

	$common_connect-> Fn_javascript_move("ショップ登録・修正しました", "shop.php?shop_id=".$shop_id);
?>
</body>
</html>