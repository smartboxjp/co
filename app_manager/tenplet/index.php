<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/config.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_start.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_head.php';
?>

<!-- Page content -->
<div id="page-content">

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_headnavi.php'; ?>

    <!-- Dashboard 2 Content -->
    <div class="row">
    
    
<!--左-->
                            <div class="col-md-2">
                     
                            </div>
<!--左-->    
    
<!--真ん中　タイムライン---->
                            <div class="col-md-6">

        					</div>
<!--真ん中　タイムライン---->


<!--右---->

        					<div class="col-md-4">
        					</div>

<!--右---->




    </div>
    <!-- END Dashboard 2 Content -->
</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_footer.php'; ?>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="/app_manager/js/helpers/excanvas.min.js"></script><![endif]-->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_scripts.php'; ?>

<!-- Google Maps API + Gmaps Plugin, must be loaded in the page you would like to use maps -->
<script src="//maps.google.com/maps/api/js"></script>
<script src="/app_manager/js/helpers/gmaps.min.js"></script>

<!-- Load and execute javascript code used only in this page -->
<script src="/app_manager/js/pages/index2.js"></script>
<script>$(function(){ Index2.init(); });</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_end.php'; ?>