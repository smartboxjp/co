<div class="block">

	<div class="widget">

		<!-- 口コミデータ -->
		<div class="block-title">
			<div class="block-options pull-right">
				<a href="/app_manager/tutorial/index.php">
					<div class="btn btn-sm btn-alt btn-default" data-toggle="tooltip" title="チュートリアル">
						<i class="fa fa-exclamation"></i>
					</div>
				</a>
			</div>
			<h2>口コミ評価</h2>
		</div>



		<!-- Widget Main -->

		<div class="widget-main">


			<div class="list-group remove-margin">

				<div class="list-group-item bottomspc">
					<span class="pull-right"><strong>8</strong></span>
					<p class="list-group-item-heading remove-margin"><i class="fa fa-comments-o" aria-hidden="true"></i>お店の口コミ数</p>
				</div>


				<div class="list-group-item">
					<span class="pull-right"><strong>1</strong></span>
					<p class="list-group-item-heading remove-margin">素晴らしい</p>
				</div>

				<div class="list-group-item">
					<span class="pull-right"><strong>2</strong></span>
					<p class="list-group-item-heading remove-margin">平均的</p>
					<p class="list-group-item-text"></p>
				</div>

				<div class="list-group-item">
					<span class="pull-right"><strong>2</strong></span>
					<p class="list-group-item-heading remove-margin">イマイチ</p>
					<p class="list-group-item-text"></p>
				</div>

				<div class="list-group-item">
					<span class="pull-right"><strong>3</strong></span>
					<p class="list-group-item-heading remove-margin">二度と働かない</p>
					<p class="list-group-item-text"></p>
				</div>

			</div>
		</div>
		<!-- END Widget Main -->

	</div>
</div>