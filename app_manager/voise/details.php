<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/config.php';
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/template_start.php';
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . '/app_manager/inc/page_head.php';
?>




<!-- Page content -->
<div id="page-content">

	<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_headnavi.php'; ?>



	<!-- パンクズリスト -->
	<ul class="breadcrumb breadcrumb-top">
		<li><a href="">管理画面</a>
		</li>
		<li><a href="">投稿された口コミ</a>
		</li>
		<li>ID 4586</li>
	</ul>
	<!-- パンクズリスト -->

	<!-- Dashboard 2 Content -->
	<div class="row">


		<div class="col-md-3">
		
				<?	
				require_once $_SERVER['DOCUMENT_ROOT']."/app_manager/voise/common/voisedeta.php"; 
				?>

		</div>



		<div class="col-md-9">

            <div class="block full">
                <!-- View Message Title -->
                <div class="block-title">
                   	<div class="block-options pull-right">
						<div class="btn btn-sm btn-alt btn-default" data-toggle="tooltip" title="ユーザー観覧ページで見る"><i class="fa fa-desktop" aria-hidden="true"></i></div>
					</div>
                    <h2><strong>口コミタイトルが表示されますよ</strong></h2>
                </div>
                <!-- END View Message Title -->

                <!-- Message Meta -->
                <table class="table table-borderless table-vcenter remove-margin">
                    <tbody>
                        <tr>
                            <td class="text-center" style="width: 80px;">
                                <a href="page_ready_user_profile.php" class="pull-left">
                                   <img src="/app_manager/img/test/123.gif" alt="Avatar" class="img-circle" width="60px">
                                </a>
                            </td>
                            <td class="hidden-xs">
                                <a href="page_ready_user_profile.php"><strong>りずりさ</strong></a>
                                <div class="text-warning">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                    <span class="text-danger point">【3.17点】</span>
                                </div>
                                <span class="text-muted">素晴らしい</span>
                            </td>
                            <td class="text-right">
                            	<strong>2014/08/25投稿</strong>
                            	<div class="text-muted">ID 45874</div>
                            	<img src="/app_manager/img/rankss.png">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <!-- END Message Meta -->

                <!-- Message Body -->
                <p><strong>【働いた期間】</strong></p>
                <p>1年未満</p>
                <p><strong>【お店のよかった所】</strong></p>
                <p>特になし</p>
                <p><strong>【お店の悪かった所】</strong></p>
                <p>特になし</p>
                <hr>
                <p><strong>【口コミ詳細】</strong></p>
                
                <p>口コミ詳細が表示されます。口コミ詳細が表示されます。口コミ詳細が表示されます。口コミ詳細が表示されます。
                口コミ詳細が表示されます。口コミ詳細が表示されます。口コミ詳細が表示されます。口コミ詳細が表示されます。
                口コミ詳細が表示されます。口コミ詳細が表示されます。口コミ詳細が表示されます。口コミ詳細が表示されます。</p>
                <hr>
                <!-- END Message Body -->
				<div class="clearfix">
				<a href="/app_manager/mail/mail.php" target="_blank">
					<button type="submit" class="btn btn-sm btn-primary pull-right" title="コソットにメールする"><i class="fa fa-envelope-o" aria-hidden="true"></i></button>
				</a>
				</div>

            </div>
            <!-- END View Message Block -->

			</div>




		</div><!--low-->
		
		<div class="text-right">
        	<ul class="pagination">
            	<li class="disabled"><a href="javascript:void(0)">&laquo;</a></li>
                <li><a href="javascript:void(0)">&raquo;</a></li>
             </ul>
        </div>


</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_footer.php'; ?>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="/app_manager/js/helpers/excanvas.min.js"></script><![endif]-->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_scripts.php'; ?>

<!-- プログレスバー-->
<script src="/app_manager/js/pages/formsWizard.js"></script>
<script>
	$( function () {
		FormsWizard.init();
	} );
</script>




<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_end.php'; ?>