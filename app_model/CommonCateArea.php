<?php

class CommonCateArea extends CommonDao{
	
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
	
	//area大
	public function Fn_cate_area_l_list ($arr_where=null) 
	{ 
		$arr_bind = array();
		$where = "";
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
		}
		
		$sql = " SELECT cate_area_l_id, cate_area_l_title FROM cate_area_l ";
		$sql .= " where 1 ".$where;
		$sql .= " order by view_level ";
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//area小
	public function Fn_cate_area_s_list ($arr_where=null) 
	{ 
		$arr_bind = array();
		$where = "";
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
		}
		
		$sql = " SELECT cate_area_l_id, cate_area_s_id, cate_area_s_title FROM cate_area_s ";
		$sql .= " where 1 ".$where;
		$sql .= " order by view_level ";
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//area中・小
	public function Fn_cate_area_data ($arr_data, $arr_where=null) 
	{ 
		$arr_bind = array();
		$where = "";
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM cate_area_s s inner join cate_area_l l on s.cate_area_l_id=l.cate_area_l_id  ";
		$sql .= " where l.flag_open=1 and s.flag_open=1 ".$where;
		$sql .= " order by l.view_level, s.view_level  ";
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//area良く検索されてる地域
	public function Fn_cate_area_usual_list () 
	{ 
		$sql = " SELECT s.cate_area_s_id, cate_area_l_title, cate_area_s_title ";
		$sql .= " FROM cate_area_l l inner join cate_area_s s on l.cate_area_l_id=s.cate_area_l_id ";
		$sql .= " where l.flag_open=1 and s.flag_open=1 and flag_search=1 ";
		$sql .= " order by l.view_level, s.view_level ";

		$db_result = $this->db_query_bind($sql, null);
		return $db_result;
	} 
	
	//地域リスト
	public function Fn_cate_area_list () 
	{
		$sql = " select s.cate_area_s_id, s.cate_area_l_id, cate_area_l_title, cate_area_s_title FROM cate_area_l l inner join cate_area_s s on l.cate_area_l_id=s.cate_area_l_id order by s.view_level" ;

		$db_result = $this->db_query_bind($sql, null);
		return $db_result;
	} 
	
	//検索条件に件数表示
	public function Fn_cate_area_s_count () 
	{
		$sql = " SELECT ca.cate_area_s_id, count(shop_id) as shop_count ";
		$sql .= " FROM cate_area_s ca inner join shop s on ca.cate_area_s_id=s.cate_area_s_id ";
		$sql .= " where s.flag_open=1 and  ca.flag_open=1";
		$sql .= " group by ca.cate_area_s_id" ;

		$db_result = $this->db_query_bind($sql, null);
		return $db_result;
	} 
	
}


?>
