<?php

class CommonCateJob extends CommonDao{
	
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
	
	
	//職種リスト
	public function Fn_db_cate_job_list ($arr_data, $arr_where=null) 
	{ 
		$arr_bind = array();
		$where = "";
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM cate_job  ";
		$sql .= " where 1 ".$where;
		$sql .= " order by view_level ";
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//
	public function Fn_cate_job_data ($arr_data, $arr_where=null) 
	{ 
		$arr_bind = array();
		$where = "";
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM cate_job ";
		$sql .= " where flag_open=1 ".$where;
		$sql .= " order by view_level  ";
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//職種すべて
	public function Fn_db_cate_job_all () 
	{
		$sql = " select cate_job_id, cate_job_title FROM cate_job order by view_level" ; 
	
		$db_result = $this->db_query_bind($sql, null);
		return $db_result;
	} 
	
	//職種
	public function Fn_db_cate_job ($cate_job_id) 
	{ 
		$where = " cate_job_id=:cate_job_id ";
		$sql = " select cate_job_id, cate_job_title FROM cate_job where ".$where ; 

		$arr_bind = array();
		$arr_bind["cate_job_id"] = $cate_job_id;
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//検索条件に件数表示
	public function Fn_cate_job_count () 
	{
		$sql = " SELECT cj.cate_job_id, count(shop_id) as shop_count ";
		$sql .= " FROM cate_job cj inner join shop s on cj.cate_job_id=s.cate_job_id ";
		$sql .= " where s.flag_open=1 and  cj.flag_open=1";
		$sql .= " group by cj.cate_job_id" ;

		$db_result = $this->db_query_bind($sql, null);
		return $db_result;
	} 
}


?>
