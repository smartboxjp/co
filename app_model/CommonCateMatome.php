<?php

class CommonCateMatome extends CommonDao{
	
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
	
	//area大
	public function Fn_cate_matome_list ($arr_where=null) 
	{ 
		$arr_bind = array();
		$where = "";
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
		}
		
		$sql = " SELECT cate_matome_id, cate_matome_title, cate_matome_color FROM cate_matome ";
		$sql .= " where 1 ".$where;
		$sql .= " order by view_level ";
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	
}


?>
