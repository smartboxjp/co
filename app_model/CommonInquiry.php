<?php

class CommonInquiry extends CommonDao{
	
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
	
	public function Fn_db_inquiry_data ($arr_data, $arr_where=null, $arr_where_not=null) 
	{ 
		$arr_bind = array();
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."=:".$key;
				}
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."!=:".$key;
				}
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM inquiry where 1 ".$where ;


		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//登録
	public function Fn_inquiry_insert ($arr_data) 
	{ 
		$arr_bind = array();
		$datetime = date("Y-m-d H:i:s");
		
		if(!is_null($arr_data))
		{
			$db_insert = "insert into inquiry ( inquiry_id, ";
			foreach($arr_data as $key=>$value)
			{
				$db_insert .= $key.", ";
			}
			$db_insert .= " regi_date, up_date ) values ( '', ";
			foreach($arr_data as $key=>$value)
			{
				$arr_bind[$key] = $value;
				$db_insert .= ":".$key.", ";
			}
			$db_insert .= " '".$datetime."', '".$datetime."')  ";

			$db_result = $this->db_update($db_insert, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	public function Fn_inquiry_update ($arr_data, $arr_where) 
	{ 
		$arr_bind = array();
		$where = "";
		$datetime = date("Y-m-d H:i:s");
		
		if(!is_null($arr_data))
		{
			$db_up = "update inquiry set ";
			foreach($arr_data as $key=>$value)
			{
				$arr_bind[$key] = $value;
				$db_up .= $key."= :".$key.", ";
			}
			$db_up .= " up_date= '".$datetime."' ";
			foreach($arr_where as $key=>$value)
			{
				$arr_bind[$key] = $value;
				$where .= "and ".$key."= :".$key." ";
			}
			$db_up .= " where 1  ".$where;

			$db_result = $this->db_update($db_up, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	//自動番号
	public function Fn_db_inquiry_auto()  
	{ 
		$sql = "select last_insert_id() as last_id " ;

		$db_result = $this->db_query_bind($sql, null);
		return $db_result;
	} 
	
	//count
	public function Fn_inquiry_all_count ($arr_where=null, $arr_where_not=null) 
	{ 
		$arr_bind = array();
		
		$var = "s_keyword";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$var_word_1 = "s_word_1";
			$arr_bind[$var_word_1] = '%'.$arr_where[$var].'%';
			$var_word_2 = "s_word_2";
			$arr_bind[$var_word_2] = '%'.$arr_where[$var].'%';
			$var_word_3 = "s_word_3";
			$arr_bind[$var_word_3] = '%'.$arr_where[$var].'%';
			$where .= " and (inquiry_comment collate utf8_unicode_ci like :".$var_word_1." or inquiry_name collate utf8_unicode_ci like :".$var_word_2." or inquiry_email collate utf8_unicode_ci like :".$var_word_3." or inquiry_id = :".$var." ) ";
		}
		
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."=:".$key;
				}
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."!=:".$key;
				}
			}
		}
		
		$sql = " SELECT count(inquiry_id) as all_count FROM inquiry where 1 ".$where ;
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	
	public function Fn_inquiry_all_list ($arr_data, $arr_where, $arr_etc) 
	{ 
		$arr_bind = array();
		
		$var = "s_keyword";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$var_word_1 = "s_word_1";
			$arr_bind[$var_word_1] = '%'.$arr_where[$var].'%';
			$var_word_2 = "s_word_2";
			$arr_bind[$var_word_2] = '%'.$arr_where[$var].'%';
			$var_word_3 = "s_word_3";
			$arr_bind[$var_word_3] = '%'.$arr_where[$var].'%';
			$where .= " and (inquiry_comment collate utf8_unicode_ci like :".$var_word_1." or inquiry_name collate utf8_unicode_ci like :".$var_word_2." or inquiry_email collate utf8_unicode_ci like :".$var_word_3." or inquiry_id = :".$var." ) ";
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM inquiry where 1 ".$where ;
		if(!is_null($arr_etc["order_name"]))
		{
			$sql .= " order by ".$arr_etc["order_name"]." ".$arr_etc["order"];
		}
		else
		{
			$sql .= " order by regi_date desc";
		}
		
		if(!is_null($arr_etc["offset"]) && !is_null($arr_etc["view_count"]))
		{
			$sql .= " limit ".$arr_etc["offset"].", ".$arr_etc["view_count"];	
		}
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	public function Fn_inquiry_delete ($inquiry_id) 
	{ 
		if($inquiry_id!="")
		{
			$db_del = "Delete from inquiry where ";
			$db_del .= " inquiry_id= :inquiry_id ";
			$arr_bind["inquiry_id"] = $inquiry_id;

			$db_result = $this->db_update($db_del, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	public function Fn_inquiry_status() {
		$arr_return["0"] = "未読";
		$arr_return["1"] = "既読";
		
		return $arr_return;
	}
}

?>
