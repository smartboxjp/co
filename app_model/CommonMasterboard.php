<?php

class CommonMasterboard extends CommonDao{
    
    //コンストラクタ
    function __construct(){
        parent::__construct();
    }

    //デストラクタ
    function __destruct(){
        parent::__destruct();
    }
    
    /*
    /masterdashboard/dashboard/index.php
    */
    public function Fn_top_user_count()
    { 
        $all_count = 0;
        $day_differ = "";
        //公開されている会員の登録数
        $sql_count = "SELECT count(member_id) as all_count FROM member where flag_open=1 ";

        $db_result_count = $this->db_query_bind($sql_count);
        if($db_result_count)
        {
            $all_count = $db_result_count[0]["all_count"];
        }
        //最後に登録された日
        $sql = "SELECT regi_date FROM member where flag_open=1 order by regi_date desc limit 0, 1";
        $db_result = $this->db_query_bind($sql);
        if($db_result)
        {
            $datetime1 = date("Y-m-d");
            $datetime2 = substr($db_result[0]["regi_date"], 0, 10);
            $day_differ =  (strtotime($datetime1)-strtotime($datetime2))/(3600*24);
        }
        $color_label = $this->Fn_top_color($day_differ);
        ?>
            <div class="ibox-title">
                <span class="label <? echo $color_label;?> pull-right"><i class="fa fa-clock-o"></i><? if($day_differ==0) { echo "本日";} else {echo $day_differ."前";};?></span>
                <h5>User</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins"><? echo number_format($all_count);?>人</h1>
                <small>会員登録数</small>
            </div>
        <?
    } 
    
    /*
    /masterdashboard/dashboard/index.php
    */
    public function Fn_top_voice_count()
    { 
        $all_count = 0;
        $day_differ = "";
        //公開されているblogの登録数
        $sql_count = "SELECT count(voice_id) as all_count FROM voice v inner join shop s on v.shop_id=s.shop_id where v.flag_open=1 and s.flag_open=1";

        $db_result_count = $this->db_query_bind($sql_count);
        if($db_result_count)
        {
            $all_count = $db_result_count[0]["all_count"];
        }
        //最後に登録された日
        $sql = "SELECT v.regi_date FROM voice v inner join shop s on v.shop_id=s.shop_id where v.flag_open=1 and s.flag_open=1 order by v.regi_date desc limit 0, 1";
        $db_result = $this->db_query_bind($sql);
        if($db_result)
        {
            $datetime1 = date("Y-m-d");
            $datetime2 = substr($db_result[0]["regi_date"], 0, 10);
            $day_differ =  (strtotime($datetime1)-strtotime($datetime2))/(3600*24);
        }
        $color_label=$this->Fn_top_color($day_differ);
        ?>
            <div class="ibox-title">
                <span class="label label-success pull-right"><i class="fa fa-clock-o"></i><? if($day_differ==0) { echo "本日";} else {echo $day_differ."前";};?></span>
                <h5>Voice</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins"><? echo number_format($all_count);?>件</h1>
                <small>最新口コミ</small>
            </div>
        <?
    } 

    /*
    /masterdashboard/dashboard/index.php
    */
    public function Fn_top_blog_count()
    { 
        $all_count = 0;
        $day_differ = "";
        //公開されているblogの登録数
        $sql_count = "SELECT count(shop_blog_id) as all_count FROM shop_blog b inner join shop s on b.shop_id=s.shop_id where b.flag_open=1 and s.flag_open=1";

        $db_result_count = $this->db_query_bind($sql_count);
        if($db_result_count)
        {
            $all_count = $db_result_count[0]["all_count"];
        }
        //最後に登録された日
        $sql = "SELECT b.regi_date FROM shop_blog b inner join shop s on b.shop_id=s.shop_id where b.flag_open=1 and s.flag_open=1 order by b.regi_date desc limit 0, 1";
        $db_result = $this->db_query_bind($sql);
        if($db_result)
        {
            $datetime1 = date("Y-m-d");
            $datetime2 = substr($db_result[0]["regi_date"], 0, 10);
            $day_differ =  (strtotime($datetime1)-strtotime($datetime2))/(3600*24);
        }
        $color_label=$this->Fn_top_color($day_differ);
        ?>
            <div class="ibox-title">
                <span class="label <? echo $color_label;?> pull-right"><i class="fa fa-clock-o"></i><? if($day_differ==0) { echo "本日";} else {echo $day_differ."前";}?></span>
                <h5>Blog</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins"><? echo number_format($all_count);?>件</h1>
                <small>お店のブログ</small>
            </div>
        <?
    } 
    
    
    /*
    /masterdashboard/dashboard/index.php
    */
    public function Fn_top_shop_pay_count()
    { 
        $all_count = 0;
        $day_differ = "";
        //公開されているblogの登録数
        $sql_count = "SELECT count(shop_id) as all_count FROM shop where flag_open=1 and shop_view_level=30";

        $db_result_count = $this->db_query_bind($sql_count);
        if($db_result_count)
        {
            $all_count = $db_result_count[0]["all_count"];
        }
        //最後に更新された日
        $sql = "SELECT up_date FROM shop where flag_open=1 and shop_view_level=30 order by up_date desc limit 0, 1";
        $db_result = $this->db_query_bind($sql);
        if($db_result)
        {
            $datetime1 = date("Y-m-d");
            $datetime2 = substr($db_result[0]["up_date"], 0, 10);
            $day_differ =  (strtotime($datetime1)-strtotime($datetime2))/(3600*24);
        }
        $color_label = $this->Fn_top_color($day_differ);
        ?>
            <div class="ibox-title">
                <span class="label <? echo $color_label;?> pull-right"><i class="fa fa-clock-o"></i><? if($day_differ==0) { echo "本日";} else {echo $day_differ."前";}?></span>
                <h5>shop A</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins"><? echo number_format($all_count);?>店</h1>
                
                <small>有料掲載店</small>
            </div>
        <?
    } 

    /*
    /masterdashboard/dashboard/index.php
    */
    public function Fn_top_shop_free_count()
    { 

        $all_count = 0;
        $day_differ = "";
        //公開されているblogの登録数
        $sql_count = "SELECT count(shop_id) as all_count FROM shop where flag_open=1 and shop_view_level=99";

        $db_result_count = $this->db_query_bind($sql_count);
        if($db_result_count)
        {
            $all_count = $db_result_count[0]["all_count"];
        }
        //最後に更新された日
        $sql = "SELECT up_date FROM shop where flag_open=1 and shop_view_level=99 order by up_date desc limit 0, 1";
        $db_result = $this->db_query_bind($sql);
        if($db_result)
        {
            $datetime1 = date("Y-m-d");
            $datetime2 = substr($db_result[0]["up_date"], 0, 10);
            $day_differ =  (strtotime($datetime1)-strtotime($datetime2))/(3600*24);
        }
        $color_label = $this->Fn_top_color($day_differ);
        ?>
            <div class="ibox-title">
                <span class="label <? echo $color_label;?> pull-right"><i class="fa fa-clock-o"></i><? if($day_differ==0) { echo "本日";} else {echo $day_differ."前";}?></span>
                <h5>Shop B</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins"><? echo number_format($all_count);?>店</h1>
                
                <small>無料店掲載</small>
            </div>
        <?
    } 

    public function Fn_top_color($day_differ)
    { 
        $color_label = "label-danger";
        if($day_differ==0) { $color_label = "label-success";}
        elseif($day_differ<3) { $color_label = "label-info";}
        return $color_label;
    } 
}


?>
