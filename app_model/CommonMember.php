<?php

class CommonMember extends CommonDao{
	
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
	
	
	public function Fn_member_all_count ($arr_where=null, $arr_where_not=null) 
	{ 
		$arr_bind = array();
		
		$var = "s_keyword";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$var_word_1 = "nickname";
			$arr_bind[$var_word_1] = '%'.$arr_where[$var].'%';
			$var_word_2 = "login_email";
			$arr_bind[$var_word_2] = '%'.$arr_where[$var].'%';
			$where .= " and (nickname collate utf8_unicode_ci like :".$var_word_1." or login_email collate utf8_unicode_ci like :".$var_word_2." or member_id = :".$var." ) ";
		}
		
		$var = "s_flag_open";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " and flag_open=:".$var;
		}
		
		$sql = " SELECT count(member_id) as all_count FROM member where 1 ".$where ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	public function Fn_db_member_data_select ($arr_data, $arr_where=null, $arr_where_not=null) 
	{ 
		$arr_bind = array();
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."=:".$key;
				}
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."!=:".$key;
				}
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM member where 1 ".$where ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		
		return $db_result;
	} 
	
	public function Fn_db_member_data ($member_id, $arr_data, $arr_where=null, $arr_where_not=null) 
	{ 
		$arr_bind = array();
		
		$where = " and member_id=:member_id ";
		$arr_bind["member_id"] = $member_id;
		
		$var = "flag_open";
		if($arr_where[$var]!="all")
		{
			$arr_bind[$var] = 1;
			$where .= " and flag_open=:".$var;
		}
		if(!is_null($arr_where)) { 
			foreach($arr_where as $value) {
				if($value!="" && $var != "flag_open")
				{
					$arr_bind[$var] = $arr_where[$var];
					$where .= " and ".$var."=:".$var;
				}
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $value) {
				if($value!="")
				{
					$arr_bind[$var] = $arr_where_not[$var];
					$where .= " and ".$var."!=:".$var;
				}
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM member where 1 ".$where ;
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	public function Fn_member_all_list ($arr_data, $arr_where, $arr_etc) 
	{ 
		$arr_bind = array();
		
		$var = "s_keyword";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$var_word_1 = "nickname";
			$arr_bind[$var_word_1] = '%'.$arr_where[$var].'%';
			$var_word_2 = "login_email";
			$arr_bind[$var_word_2] = '%'.$arr_where[$var].'%';
			$where .= " and (nickname collate utf8_unicode_ci like :".$var_word_1." or login_email collate utf8_unicode_ci like :".$var_word_2." or member_id = :".$var." ) ";
		}
		
		$var = "s_flag_open";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " and flag_open=:".$var;
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM member where 1 ".$where ;
		if(!is_null($arr_etc["order_name"]))
		{
			$sql .= " order by ".$arr_etc["order_name"]." ".$arr_etc["order"];
		}
		else
		{
			$sql .= " order by regi_date desc";
		}
		if(!is_null($arr_etc["offset"]) && !is_null($arr_etc["view_count"]))
		{
			$sql .= " limit ".$arr_etc["offset"].", ".$arr_etc["view_count"];	
		}
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
		
	//ログイン
	public function Fn_login ($arr_where) 
	{ 
		$arr_bind = array();
		
		$var = "login_email";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " and login_email = :".$var." ";
		}
		
		$var = "login_pw";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " and login_pw=:".$var;
		}
		$arr_db_field = array("member_id", "login_email", "login_pw", "flag_open", "nickname");
		
		$sql = "SELECT ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM member where 1 ".$where ;
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//会員情報
	public function Fn_member_info ($arr_where) 
	{ 
		$arr_bind = array();
		
		$var = "s_member_id";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " and member_id = :".$var." ";
		}
		
		$var = "s_flag_open";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " and flag_open=:".$var;
		}
		$arr_db_field = array("member_id", "login_email", "login_pw", "nickname");
		$arr_db_field = array_merge($arr_db_field, array("twitter", "facebook", "yahoo", "cate_area_l_id", "cate_area_s_id"));
		
		$sql = "SELECT ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM member where 1 ".$where ;
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	
	//既存会員データがあれば削除
	public function Fn_db_member_del ($arr_where) 
	{ 
		$arr_bind = array();
		
		$var = "login_email";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " login_email=:".$var;
			$sql = "Delete from member where  ".$where ;
			$db_result = $this->db_update($sql, $arr_bind);
			
			return true;
		}
		
		return false;
	} 
	
	//insert
	public function Fn_member_insert ($arr_data) 
	{ 
		$arr_bind = array();
		$datetime = date("Y-m-d H:i:s");
		
		if(!is_null($arr_data))
		{
			$db_insert = "insert into member ( ";
			$db_insert .= " member_id, ";
			foreach($arr_data as $key=>$value)
			{
					$db_insert .= $key.", ";
			}
			$db_insert .= " regi_date, up_date ";
			$db_insert .= " ) values ( ";
			$db_insert .= " '', ";
			foreach($arr_data as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$db_insert .= " :".$key.", ";
			}
			$db_insert .= " '$datetime', '$datetime')";
			
			$db_result = $this->db_update($db_insert, $arr_bind);
			return true;
		}
		
		return false;
	} 
	
	
	public function Fn_member_update ($arr_data, $arr_where) 
	{ 
		$arr_bind = array();
		$where = "";
		$datetime = date("Y-m-d H:i:s");
		
		if(!is_null($arr_data))
		{
			$db_up = "update member set ";
			foreach($arr_data as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$db_up .= $key."= :".$key.", ";
			}
			$db_up .= " up_date= '".$datetime."' ";
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
			$db_up .= " where 1  ".$where;

			$db_result = $this->db_update($db_up, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	//sessionから情報
	public function Fn_session_member_info ($arr_data) 
	{ 
		$member_id = $_SESSION["member_id"];
		
		$arr_bind = array();
		
		$var = "member_id";
		$arr_bind[$var] = $member_id;
		$where .= " and member_id = :".$var." ";
				
		$var = "flag_open";
		$arr_bind[$var] = "1";
		$where .= " and flag_open=:".$var;
		
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM member where 1 ".$where ;
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//自動番号
	public function Fn_db_apply_auto()  
	{ 
		$sql = "select last_insert_id() as last_id " ;

		$db_result = $this->db_query_bind($sql, null);
		return $db_result;
	} 
	
	
	public function Fn_member_delete ($arr_where) 
	{ 
		if(!is_null($arr_where))
		{
			$where = "";
			
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
			
			$db_del = "Delete from member where 1  ".$where;

			$db_result = $this->db_update($db_del, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	
	public function Fn_member_ranking() {
		$arr_return["10"] = "S";
		$arr_return["20"] = "A";
		$arr_return["30"] = "B";
		$arr_return["40"] = "C";
		$arr_return["50"] = "D";
		
		return $arr_return;
	}
	
	public function Fn_member_active() {
		$arr_return["99"] = "禁止";
		$arr_return["1"] = "許可";
		
		return $arr_return;
	}
}


?>
