<?php
/* 仮登録 */
class CommonMemberTemp extends CommonDao{
	
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
	
	public function Fn_db_member_temp_data ($arr_data, $arr_where=null, $arr_where_not=null) 
	{ 
		$arr_bind = array();
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."=:".$key;
				}
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."!=:".$key;
				}
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM member_temp where 1 ".$where ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 

	
	//既存会員データがあれば削除
	public function Fn_db_member_temp_del ($arr_where) 
	{ 
		$where = "";
		if(!is_null($arr_where))
		{
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
			$where .= " where 1  ".$where;
			$db_up = "Delete from member_temp  ".$where ;
			$db_result = $this->db_update($db_up, $arr_bind);
			
			return true;
		}
		
		return false;
	} 
	
	//仮登録
	public function Fn_member_temp_insert_temp ($arr_data) 
	{ 
		$arr_bind = array();
		$datetime = date("Y-m-d H:i:s");
		
		if(!is_null($arr_data))
		{
			$db_insert = "insert into member_temp ( ";
			$db_insert .= " member_temp_id, ";
			foreach($arr_data as $key=>$value)
			{
					$db_insert .= $key.", ";
			}
			$db_insert .= " regi_date, up_date ";
			$db_insert .= " ) values ( ";
			$db_insert .= " '', ";
			foreach($arr_data as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$db_insert .= " :".$key.", ";
			}
			$db_insert .= " '$datetime', '$datetime')";

			$db_result = $this->db_update($db_insert, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	
	public function Fn_member_temp_update ($arr_data, $arr_where) 
	{ 
		$arr_bind = array();
		$where = "";
		$datetime = date("Y-m-d H:i:s");
		
		if(!is_null($arr_data))
		{
			$db_up = "update member_temp set ";
			foreach($arr_data as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$db_up .= $key."= :".$key.", ";
			}
			$db_up .= " up_date= '".$datetime."' ";
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
			$db_up .= " where 1  ".$where;

			$db_result = $this->db_update($db_up, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
}


?>
