<?php

class CommonPr extends CommonDao{
	
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
	
	
	public function Fn_pr_list ($arr_data, $arr_where=null) 
	{ 
		$arr_bind = array();
		
		$where = "";
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM pr where 1 ".$where ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//登録
	public function Fn_pr_insert ($arr_data) 
	{ 
		$arr_bind = array();
		$datetime = date("Y-m-d H:i:s");
		
		if(!is_null($arr_data))
		{
			$db_insert = "insert into pr ( pr_id, ";
			foreach($arr_data as $key=>$value)
			{
				$db_insert .= $key.", ";
			}
			$db_insert .= " regi_date, up_date ) values ( '', ";
			foreach($arr_data as $key=>$value)
			{
				$arr_bind[$key] = $value;
				$db_insert .= ":".$key.", ";
			}
			$db_insert .= " '".$datetime."', '".$datetime."')  ";
			$db_result = $this->db_update($db_insert, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	//自動番号
	public function Fn_db_apply_auto()  
	{ 
		$sql = "select last_insert_id() as last_id " ;

		$db_result = $this->db_query_bind($sql, null);
		return $db_result;
	} 
	
	public function Fn_pr_update ($arr_data, $arr_where) 
	{ 
		$arr_bind = array();
		$where = "";
		$datetime = date("Y-m-d H:i:s");
		
		if(!is_null($arr_data))
		{
			$db_up = "update pr set ";
			foreach($arr_data as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$db_up .= $key."= :".$key.", ";
			}
			$db_up .= " up_date= '".$datetime."' ";
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
			$db_up .= " where 1  ".$where;

			$db_result = $this->db_update($db_up, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	public function Fn_pr_delete ($arr_where) 
	{ 
		if(!is_null($arr_where))
		{
			$where = "";
			
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
			
			$db_del = "Delete from pr where 1  ".$where;

			$db_result = $this->db_update($db_del, $arr_bind);

			return true;
		}
		
		return false;
	} 
}


?>
