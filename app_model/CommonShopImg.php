<?php

class CommonShopImg extends CommonDao{
	
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
	
	//サブイメージ
	public function Fn_db_shop_img($shop_id, $arr_where=null)  
	{ 
		$arr_bind = array();
		
		$arr_bind["shop_id"] = $shop_id;
		$where = " and shop_id=:shop_id ";
		
		$var = "s_flag_open";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " and flag_open=:".$var;
		}
		
		//リスト表示
		$arr_db_field = array("shop_img_id", "shop_id", "img", "comment");

		$sql = "SELECT ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM shop_img where 1 ".$where ;
		$sql .= " order by view_level, up_date desc ";

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
}


?>
