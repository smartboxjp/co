<?php exit;?>
<?

	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonAdmin.php";
	$common_admin = new CommonAdmin();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	//カテゴリ
	$arr_where = array();
	$arr_where["cate_pr_group"] = $cate_pr_group;
	
	$arr_data = array("cate_pr_id", "cate_pr_title", "pr_size", "pr_position");
	$arr_pr = $common_catepr->Fn_cate_pr_list($arr_data);
	if(!is_null($arr_pr))
	{
		foreach($arr_pr as $arr_key=>$arr_value)
		{
			$arr_cate_pr_title[$arr_value["cate_pr_id"]] = $arr_value["cate_pr_title"];
			$arr_cate_pr_size[$arr_value["cate_pr_id"]] = $arr_value["pr_size"];
			$arr_cate_pr_position[$arr_value["cate_pr_id"]] = $arr_value["pr_position"];
		}
	}
	
	//リスト
	public function Fn_pr_list ($arr_data, $arr_where=null) 
	{ 
		$arr_bind = array();
		
		$where = "";
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM pr where 1 ".$where ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//ログイン
	if($_COOKIE['shop'] != '')
	{
		$arr_data = array();
		$arr_data[] = "shop_name";
		$arr_data[] = "shop_id";
		$arr_data[] = "flag_open";
		
		$arr_where = array();
		$arr_where["flag_open"] = "all";
		$login_cookie = $_COOKIE['shop'];
		$db_result = $common_shop->Fn_db_shop_login_cookie ($login_cookie, $arr_data, $arr_where=null);
		
		if($db_result)
		{
			$db_shop_id = $db_result[0]["shop_id"];
			$db_shop_name = $db_result[0]["shop_name"];
			$db_flag_open = $db_result[0]["flag_open"];
		}
		
		if($db_shop_id!="" && $db_flag_open=="1")
		{
			$_SESSION['shop_id']=$db_shop_id;
			$_SESSION['shop_name']=$db_shop_name;
			
			$common_connect->Fn_redirect(global_ssl."/app_manager/dashboard/");
		}
	}
	
	
	
	public function Fn_db_shop_data ($shop_id, $arr_data, $arr_where=null) 
	{ 
		$arr_bind = array();
		
		$where = " and shop_id=:shop_id ";
		$arr_bind["shop_id"] = $shop_id;
		
		$var = "flag_open";
		if($arr_where[$var]!="all")
		{
			$arr_bind[$var] = 1;
			$where .= " and flag_open=:".$var;
		}
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
		}
		
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM shop where 1 ".$where ;


		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//ログイン
	public function Fn_db_shop_login_cookie ($login_cookie, $arr_data, $arr_where=null) 
	{ 
		$arr_bind = array();
		
		$where = " and login_cookie=:login_cookie ";
		$arr_bind["login_cookie"] = $login_cookie;
		
		$var = "flag_open";
		if($arr_where[$var]!="all")
		{
			$arr_bind[$var] = 1;
			$where .= " and flag_open=:".$var;
		}
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM shop where 1 ".$where ;


		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	
	//削除
	public function Fn_shop_delete ($shop_id) 
	{ 
		if($shop_id!="")
		{
			$db_del = "Delete from shop where ";
			$db_del .= " shop_id= :shop_id ";
			$arr_bind["shop_id"] = $shop_id;

			$db_result = $this->db_update($db_del, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	public function Fn_matome_all_count ($arr_where) 
	{ 
		$arr_bind = array();
		
		$var = "s_keyword";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$var_word_1 = "s_word_1";
			$arr_bind[$var_word_1] = '%'.$arr_where[$var].'%';
			$where .= " and (matome_title collate utf8_unicode_ci like :".$var_word_1." or matome_id = :".$var." ) ";
		}
		
		$where = "";
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
		}
		$sql = " SELECT count(matome_id) as all_count FROM matome where 1 ".$where ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	public function Fn_member_update ($arr_data, $arr_where) 
	{ 
		$arr_bind = array();
		$where = "";
		$datetime = date("Y-m-d H:i:s");
		
		if(!is_null($arr_data))
		{
			$db_up = "update member set ";
			foreach($arr_data as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$db_up .= $key."= :".$key.", ";
			}
			$db_up .= " up_date= '".$datetime."' ";
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
			$db_up .= " where 1  ".$where;

			$db_result = $this->db_update($db_up, $arr_bind);

			return true;
		}
		
		return false;
	} 
?>