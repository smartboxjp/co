<div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="グーグル" class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>

            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Cossot管理画面</span>
                </li>



                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="mail_box.html">
                        <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
                    </a>
                    
                </li>



                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="list_voice.html">
                        <i class="fa fa-comments"></i>  <span class="label label-primary">8</span>
                    </a>
                </li>


                <li>
                    <a href="login.html">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>

            </ul>

        </nav>
</div>