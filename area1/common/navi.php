    <nav class="navbar-default navbar-static-side" role="navigation">

        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="img/profile_small.jpg" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Team-avalanche</strong>
                             </span> <span class="text-muted text-xs block">各種ページ <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="profile.html">cossot</a></li>
                            <li><a href="contacts.html">調査</a></li>
                            <li><a href="mailbox.html">avalanche</a></li>
                            <li class="divider"></li>
                            <li><a href="login.html">logout</a></li>
                        </ul>
                    </div>

                    <div class="logo-element">
                        Icon
                    </div>
                </li>

                <li class="active">
                    <a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label">管理画面</span></a>
                </li>

                <li>
                    <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">各種登録データ</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="list_shop.html">登録店</a></li>
                        <li><a href="list_blog.html">お店ブログ一覧</a></li>
                        <li><a href="list_movie.html">お店動画一覧</a></li>
                        <li><a href="list_user.html">登録ユーザー</a></li>
                        <li><a href="list_voice.html">口コミ一覧</a></li>
			<li><a href="list_img.html">画像一覧</a></li>
                    </ul>
                </li>


                <li>
                    <a href="mailbox.html"><i class="fa fa-envelope"></i> <span class="nav-label">Mailbox </span><span class="label label-warning pull-right">未読24</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="mail_box.html">コソットへお問い合わせ</a></li>
                        <li><a href="mail_delete.html">口コミ削除依頼</a></li>
                        <li><a href="mail_entry.html">店舗掲載依頼</a></li>
                    </ul>
                </li>
                <li>
                    <a href="metrics.html"><i class="fa fa-pie-chart"></i> <span class="nav-label">店舗登録</span>  </a>
                </li>
                <li>
                    <a href="widgets.html"><i class="fa fa-flask"></i> <span class="nav-label">ユーザー登録</span></a>
                </li>

                <li>
                    <a href="index.html"><i class="fa fa-edit"></i> <span class="nav-label">まとめ記事投稿</span></a>
                </li>

                <li>
                    <a href="#"><i class="fa fa-desktop"></i> <span class="nav-label">広告コントロール</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="contacts.html">PC:</a></li>
                        <li><a href="profile.html">PC:</a></li>
                        <li><a href="profile_2.html">PC:</a></li>
                        <li><a href="contacts_2.html">Contacts v.2</a></li>
                        <li><a href="projects.html">Projects</a></li>
                        <li><a href="project_detail.html">Project detail</a></li>
                        <li><a href="teams_board.html">Teams board</a></li>
                        <li><a href="social_feed.html">Social feed</a></li>
                        <li><a href="clients.html">Clients</a></li>
                        <li><a href="full_height.html">Outlook view</a></li>
                        <li><a href="vote_list.html">Vote list</a></li>
                        <li><a href="file_manager.html">File manager</a></li>
                        <li><a href="calendar.html">Calendar</a></li>
                        <li><a href="issue_tracker.html">Issue tracker</a></li>
                        <li><a href="blog.html">Blog</a></li>
                        <li><a href="article.html">Article</a></li>
                        <li><a href="faq.html">FAQ</a></li>
                        <li><a href="timeline.html">Timeline</a></li>
                        <li><a href="pin_board.html">Pin board</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-files-o"></i> <span class="nav-label">ランキング編集</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="search_results.html">平均点ランキング</a></li>
                        <li><a href="lockscreen.html">アクセスランキング</a></li>
                        <li><a href="invoice.html">ブログランキング</a></li>
                    </ul>
                </li>
                <li>
                    <a href="index.html"><i class="fa fa-edit"></i> <span class="nav-label">トピックス</span></a>
                </li>

                <li>
                    <a href="#"><i class="fa fa-picture-o"></i> <span class="nav-label">各種お知らせ</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="basic_gallery.html">サイト内告知</a></li>
                        <li><a href="slick_carousel.html">店舗管理告知</a></li>
                        <li><a href="carousel.html">ユーザー管理告知</a></li>

                    </ul>
                </li>

                <li>
                    <a href="index.html"><i class="fa fa-edit"></i> <span class="nav-label">メルマガ</span></a>
                </li>

                <li>
                    <a href="index.html"><i class="fa fa-edit"></i> <span class="nav-label">コソットアクセス</span></a>
                </li>



                <li>
                    <a href="#"><i class="fa fa-picture-o"></i> <span class="nav-label">他求人サイト</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="basic_gallery.html">イチゴナビ</a></li>
                        <li><a href="slick_carousel.html">バニラ</a></li>
                        <li><a href="carousel.html">ガールズヘブン</a></li>

                    </ul>
                </li>

            </ul>

        </div>
    </nav>