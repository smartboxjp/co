<?php
/**
 * config.php
 *
 * Author: pixelcave
 *
 * Configuration file. It contains variables used in the template as well as the primary navigation array from which the navigation is created
 *
 */

/* Template variables */
$template = array(
    'name'              => 'cossot',
    'version'           => '3.5',
    'author'            => 'pixelcave',
    'robots'            => 'noindex, nofollow',
    'title'             => 'ProUI - Responsive Bootstrap Admin Template',
    'description'       => 'ProUI is a Responsive Bootstrap Admin Template created by pixelcave and published on Themeforest.',
    // true                     enable page preloader
    // false                    disable page preloader
    'page_preloader'    => false,
    // true                     enable main menu auto scrolling when opening a submenu
    // false                    disable main menu auto scrolling when opening a submenu
    'menu_scroll'       => true,
    // 'navbar-default'         for a light header
    // 'navbar-inverse'         for a dark header
    'header_navbar'     => 'navbar-default',
    // ''                       empty for a static layout
    // 'navbar-fixed-top'       for a top fixed header / fixed sidebars
    // 'navbar-fixed-bottom'    for a bottom fixed header / fixed sidebars
    'header'            => '',
    // ''                                               for a full main and alternative sidebar hidden by default (> 991px)
    // 'sidebar-visible-lg'                             for a full main sidebar visible by default (> 991px)
    // 'sidebar-partial'                                for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
    // 'sidebar-partial sidebar-visible-lg'             for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
    // 'sidebar-mini sidebar-visible-lg-mini'           for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
    // 'sidebar-mini sidebar-visible-lg'                for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)
    // 'sidebar-alt-visible-lg'                         for a full alternative sidebar visible by default (> 991px)
    // 'sidebar-alt-partial'                            for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
    // 'sidebar-alt-partial sidebar-alt-visible-lg'     for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)
    // 'sidebar-partial sidebar-alt-partial'            for both sidebars partial which open on mouse hover, hidden by default (> 991px)
    // 'sidebar-no-animations'                          add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!
    'sidebar'           => 'sidebar-partial sidebar-visible-lg sidebar-no-animations',
    // ''                       empty for a static footer
    // 'footer-fixed'           for a fixed footer
    'footer'            => '',
    // ''                       empty for default style
    // 'style-alt'              for an alternative main style (affects main page background as well as blocks style)
    'main_style'        => '',
    // ''                           Disable cookies (best for setting an active color theme from the next variable)
    // 'enable-cookies'             Enables cookies for remembering active color theme when changed from the sidebar links (the next color theme variable will be ignored)
    'cookies'           => '',
    // 'night', 'amethyst', 'modern', 'autumn', 'flatie', 'spring', 'fancy', 'fire', 'coral', 'lake',
    // 'forest', 'waterlily', 'emerald', 'blackberry' or '' leave empty for the Default Blue theme
    'theme'             => '',
    // ''                       for default content in header
    // 'horizontal-menu'        for a horizontal menu in header
    // This option is just used for feature demostration and you can remove it if you like. You can keep or alter header's content in page_head.php
    'header_content'    => '',
    'active_page'       => basename($_SERVER['PHP_SELF'])
);

/* Primary navigation array (the primary navigation will be created automatically based on this array, up to 3 levels deep) */
$primary_nav = array(
    array(
        'name'  => '管理画面',
        'url'   => 'shop_admin.php',
        'icon'  => 'fa fa-home'
    ),
	    array(
	        'name'  => 'お店をアピールする <i class="fa fa-angle-down"></i>',
	        'url'   => 'header',
	    ),
    array(
        'name'  => 'ブログ投稿',
        'url'   => '',
        'icon'  => 'fa fa-pencil'
    ),
    array(
        'name'  => '動画投稿',
        'url'   => '',
        'icon'  => 'fa fa-video-camera'
    ),
	    array(
	       'name'  => 'お店の評価 <i class="fa fa-angle-down"></i>',
	       'url'   => 'header',
	    ),
    array(
        'name'  => 'お店の口コミ一覧',
        'url'   => '',
        'icon'  => 'fa fa-commenting-o'
    ),
    array(
        'name'  => 'お店の画像一覧',
        'url'   => '',
        'icon'  => 'fa fa-picture-o'
    ),
	    array(
	        'name'  => 'ユーザーからの問い合わせ <i class="fa fa-angle-down"></i>',
	         'url'   => 'header',
	    ),
    array(
        'name'  => '問い合わせメール',
        'url'   => '',
        'icon'  => 'fa fa-envelope-o'
    ),
	    array(
	        'name'  => 'お店ページを作る <i class="fa fa-angle-down"></i>',
	         'url'   => 'header',
	    ),
    array(
        'name'  => '各種情報更新',
        'icon'  => 'fa fa-laptop',
        'sub'   => array(
	    array(
                'name'  => 'お店基本情報',
                'url'   => 'shop_maindeta.php'
            ),
            array(
                'name'  => 'お店のサムネイル画像',
                'url'   => 'shop_thumbnail.php'
            ),
            array(
                'name'  => 'お店のメイン画像',
                'url'   => 'shop_mainimg.php'
            ),
            array(
                'name'  => 'お店のメイン画像(有料)',
                'url'   => 'shop_extramainimg.php'
            ),
            array(
                'name'  => '有料店用情報',
                'url'   => 'shop_extradeta.php'
            ),
        )
    ),
    array(
        'name'  => 'ページの確認',
        'icon'  => 'fa fa-eye',
        'sub'   => array(
            array(
                'name'  => 'お店ページ確認',
                'url'   => ''
            ),
            array(
                'name'  => 'cossotを確認',
                'url'   => ''
            ),
        )
    ),
	    array(
	        'name'  => 'その他 <i class="fa fa-angle-down"></i>',
	        'url'   => 'header',
    ),
    array(
        'name'  => '設定変更',
        'url'   => '',
        'icon'  => 'fa fa-cog'
    ),
    array(
        'name'  => 'ツールボックス',
        'icon'  => 'fa fa-briefcase',
        'sub'   => array(
            array(
                'name'  => '口コミ、画像の削除依頼',
                'url'   => ''
            ),
            array(
                'name'  => '有料へ変更依頼',
                'url'   => ''
            ),
            array(
                'name'  => '項目1',
                'url'   => ''
            ),
           array(
                'name'  => '項目2',
                'url'   => ''
            ),
        )
    ),
    array(
                 'name'  => 'ログアウト',
                 'url'   => '',
                'icon'  => 'fa fa-reply'
    )
);