<!-- Dashboard 2 Header -->
    <div class="content-header">
        <ul class="nav-horizontal text-center">
            <li>
                <a href="javascript:void(0)"><i class="fa fa-home"></i> Home</a>
            </li>
            <li>
                <a href="javascript:void(0)"><i class="fa fa-level-up"></i> 上位表示</a>
            </li>
            <li>
                <a href="javascript:void(0)"><i class="fa fa-pencil"></i> ブログ</a>
            </li>
            <li>
                <a href="javascript:void(0)"><i class="fa fa-commenting-o"></i> 口コミ</a>
            </li>
            <li>
                <a href="javascript:void(0)"><i class="fa fa-picture-o"></i> 画像</a>
            </li>
            <li>
                <a href="javascript:void(0)"><i class="fa fa-envelope-o"></i> メール</a>
            </li>
        </ul>
    </div>
    <!-- END Dashboard 2 Header -->