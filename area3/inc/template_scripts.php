<?php
/**
 * template_scripts.php
 *
 * Author: pixelcave
 *
 * All vital JS scripts are included here
 *
 */
?>

<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
<script src="js/vendor/jquery-1.12.0.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/app.js"></script>
<!-- Image cropper -->
<script src="js/tool/cropper/cropper.js"></script>
<script src="js/tool/cropper/croppermain.js"></script>
<!-- Vegas 2 -->
<script src="js/tool/anime/vegas.min.js"></script>