<?php include 'inc/config.php'; ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">

<?php include 'inc/page_headnavi.php'; ?>

    <!-- Dashboard 2 Content -->
    <div class="row">
<!--左半分タイムライン---->
                            <div class="col-md-6">
                                <!-- ブロック -->
                                <div class="block">
                                    <!-- タイトル -->
                                    <div class="block-title">
                                        <div class="block-options pull-left">
                                            <a href="http://yafoo.co.jp" target="_blank" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="タイムラインとは？"><i class="fa fa-question"></i></a>
                                        </div>
                                        <h2>タイムライン</h2>
                                    </div>
                                    <!-- タイトル -->

                                    <!-- タイムラインコンテンツ -->
                                    <div class="block-content-full">
                                        <div class="timeline">

                                            <ul class="timeline-list">
                                    <!-- 自分のお店に画像投稿の場合 -->
                                                <li class="active">
                                                    <div class="timeline-icon"><i class="fa fa-picture-o"></i></div>
                                                    <div class="timeline-time"><small>今</small></div>
                                                    <div class="timeline-content">
                                                        <p class="push-bit"><strong>お店の関連する画像が投稿されました。</strong></p>
                                                        <div class="row push">
                                                            <div class="col-sm-6 col-md-4">
                                                                <a href="img/placeholders/photos/photo1.jpg" data-toggle="lightbox-image">
                                                                    <img src="img/test/tesut2.JPG">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <p class="push-bit"><a href="page_ready_user_profile.html" target="_blank">投稿者名</a></p>
                                                    </div>
                                                </li>

                                    <!-- 自分のお店に口コミの場合 -->
                                                <li class="active">
                                                    <div class="timeline-icon"><i class="fa fa-commenting-o"></i></div>
                                                    <div class="timeline-time"><small>1時間前</small></div>
                                                    <div class="timeline-content">
                                                        <p class="push-bit"><strong>お店へ口コミが投稿されました。</strong></p>
                                                        口コミタイトルが最大で20文字まで表示されます。
							<p class="push-bit"><img src="img/test/hosi1m.png"><img src="img/test/hosi1m.png"><img src="img/test/hosi2m.png"><img src="img/test/hosi3m.png"><img src="img/test/hosi3m.png"><span class="text-danger point">【3.15】</span>点</p>							
							<p class="push-bit"><a href="page_ready_user_profile.html" target="_blank">投稿者名</a></p>
                                                    </div>
                                                </li>
                                    <!-- 自分のお店の評価に変動がある場合 -->
                                                <li class="active">
                                                    <div class="timeline-icon"><i class="fa fa-star-o"></i></div>
                                                    <div class="timeline-time"><small>3時間前</small></div>
                                                    <div class="timeline-content">
                                                        <p class="push-bit"><strong>お店の評価点に変動がありました</strong></p>
							<span class="text-danger point">【3.15】→【3.20】</span>と変更されました。
                                                    </div>
                                                </li>

                                    <!-- 同じ地域にお店が登録された場合 -->
                                                <li class="active">
                                                    <div class="timeline-icon"><i class="fa fa-home"></i></div>
                                                    <div class="timeline-time"><small>昨日</small></div>
                                                    <div class="timeline-content">
                                                        <p class="push-bit"><strong>同じ地域に新しくお店が掲載されました。</strong></p>
                                                        <div class="row push">
                                                            <div class="col-sm-6 col-md-4">
                                                                <a href="img/placeholders/photos/photo23.jpg" data-toggle="lightbox-image">
                                                                    <a href="" target="_blank"><img src="img/test/sanple1.jpg"></a>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <p class="push-bit"><a href="page_ready_user_profile.html" target="_blank">【店名が最大で20文字まで】</a></p>
                                                    </div>
                                                </li>

                                    <!-- 同じ地域（第二エリア）のお店の評価が変動した場合 -->
                                                <li class="active">
                                                    <div class="timeline-icon"><i class="fa fa-star-o"></i></div>
                                                    <div class="timeline-time"><small>一昨日</small></div>
                                                    <div class="timeline-content">
                                                        <p class="push-bit"><strong>【横浜】地域の【同じ第二エリアのお店】の評価に変動がありました。</strong></p>
                                                        【暮らしモア横浜店】の評価が<span class="text-danger point">【3.15】→【3.13】</span>と変更されました。
                                                    </div>
                                                </li>

                                    <!-- 同じ第一エリアでユーザーが登録された場合 -->
                                                <li class="active">
                                                    <div class="timeline-icon"><i class="fa fa-frown-o"></i></div>
                                                    <div class="timeline-time"><small>昨日</small></div>
                                                    <div class="timeline-content">
                                                        <p class="push-bit"><strong>登録地域【神奈川県】で新規ユーザーの登録がありました。</strong></p>
                                                        <div class="row push">
                                                            <div class="col-sm-6 col-md-4">
                                                                <a href="" target="_blank">
                                                                    <div class="timelineuserimg"><img src="img/test/prfimg.gif"></div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <p class="push-bit"><a href="" target="_blank">【リズリサ】さん</a></p>
                                                    </div>
                                                </li>

                                    <!-- 1コンテンツ -->
                                                <li class="active">
                                                    <div class="timeline-icon"><i class="fa fa-star-o"></i></div>
                                                    <div class="timeline-time"><small>一昨日</small></div>
                                                    <div class="timeline-content">
                                                        <p class="push-bit"><strong>【横浜】地域の【同じ第二エリアのお店】の評価に変動がありました。</strong></p>
                                                        【暮らしモア横浜店】の評価が<span class="text-danger point">【3.15】→【3.13】</span>と変更されました。
                                                    </div>
                                                </li>
                                    <!-- 1コンテンツ -->
                                                <li class="active">
                                                    <div class="timeline-icon"><i class="fa fa-star-o"></i></div>
                                                    <div class="timeline-time"><small>一昨日</small></div>
                                                    <div class="timeline-content">
                                                        <p class="push-bit"><strong>【横浜】地域の【同じ第二エリアのお店】の評価に変動がありました。</strong></p>
                                                        【暮らしモア横浜店】の評価が<span class="text-danger point">【3.15】→【3.13】</span>と変更されました。
                                                    </div>
                                                </li>
                                    <!-- 1コンテンツ -->
                                                <li class="active">
                                                    <div class="timeline-icon"><i class="fa fa-star-o"></i></div>
                                                    <div class="timeline-time"><small>一昨日</small></div>
                                                    <div class="timeline-content">
                                                        <p class="push-bit"><strong>【横浜】地域の【同じ第二エリアのお店】の評価に変動がありました。</strong></p>
                                                        【暮らしモア横浜店】の評価が<span class="text-danger point">【3.15】→【3.13】</span>と変更されました。
                                                    </div>
                                                </li>
                                    <!-- 1コンテンツ -->
                                                <li class="active">
                                                    <div class="timeline-icon"><i class="fa fa-star-o"></i></div>
                                                    <div class="timeline-time"><small>一昨日</small></div>
                                                    <div class="timeline-content">
                                                        <p class="push-bit"><strong>【横浜】地域の【同じ第二エリアのお店】の評価に変動がありました。</strong></p>
                                                        【暮らしモア横浜店】の評価が<span class="text-danger point">【3.15】→【3.13】</span>と変更されました。
                                                    </div>
                                                </li>
                                    <!-- 1コンテンツ -->


                                            </ul>

                                        </div>
                                    </div>
                                    <!-- タイムラインコンテンツ -->
                                </div>
                                <!-- ブロック -->

        		</div>



<!--右半分---->



        <div class="col-md-6">


            <!-- お店データ -->

		<div class="block full">
			<div class="block-title">
				<div class="block-options pull-right">
					<a href="shop_mainimg.php" class="btn btn-sm btn-alt btn-default" data-toggle="tooltip" title="画像変更"><i class="fa fa-picture-o"></i></a>
				</div>
				<h2>ミリオングループ那覇のデータ</h2>
			</div>

			<div class="widget">
				<div class="widget-advanced widget-advanced-alt">
					 <div class="widget-header text-center">
					 	<div class="mainimgbox"><img src="img/test/shop1.jpg""></div>
					</div>
				</div>
			</div>


                    <!-- deta Main -->
			<div class="widget-main">
				<div class="row text-center">
					<div class="col-xs-6 col-lg-3">
						<h3>
						<strong>10</strong> <small>口コミ</small><br>
						<small>お店の口コミ</small>
						</h3>
					</div>
					<div class="col-xs-6 col-lg-3">
						<h3>
						<strong>80</strong> <small>枚</small><br>
						<small>お店関連画像</small>
						</h3>
					</div>
					<div class="col-xs-6 col-lg-3">
						<h3>
						<strong>60</strong> <small>vew</small><br>
						<small>昨日のView</small>
						</h3>
					</div>
					<div class="col-xs-6 col-lg-3">
						<h3>
						<strong>5</strong> <small>通</small><br>
						<small>未読メール</small>
					</h3>
					</div>
				</div>
			</div>
                    <!-- END deta Main -->
		</div>
            <!-- END お店データ-->



 <!-- 口コミボックス -->
            <div class="block full">
                <!-- Twitter Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="" class="btn btn-sm btn-alt btn-default" data-toggle="tooltip" title="口コミ一覧"><i class="fa fa-comment-o"></i></a>
                    </div>
                    <h2>お店の最新口コミ</h2>
                </div>
                <!-- END Twitter Title -->

                <ul class="media-list">
                    <li class="media">
                        <a href="" class="pull-left">
                            <img src="img/placeholders/avatars/avatar2.jpg" alt="user" class="img-circle">
                        </a>
                        <div class="media-body">
                            <span class="text-muted pull-right"><small>03/25 17:35</small></span>
                            <a href=""><strong>リズリサ</strong></a>
			　　<p class="push-bit"><img src="img/test/hosi1m.png"><img src="img/test/hosi1m.png"><img src="img/test/hosi2m.png"><img src="img/test/hosi3m.png"><img src="img/test/hosi3m.png"><span class="text-danger point">【3.15】</span>点</p>	
                            <p>私にとってはじめてのお店ですが働きやすかった！</p>
                        </div>
                    </li>

                    <li class="media">
                        <a href="page_ready_user_profile.php" class="pull-left">
                            <img src="img/placeholders/avatars/avatar2.jpg" alt="Avatar" class="img-circle">
                        </a>
                        <div class="media-body">
                            <span class="text-muted pull-right"><small>03/25 17:35</small></span>
                            <a href=""><strong>金城なぎさ</strong></a>
			　　<p class="push-bit"><img src="img/test/hosi1m.png"><img src="img/test/hosi1m.png"><img src="img/test/hosi2m.png"><img src="img/test/hosi3m.png"><img src="img/test/hosi3m.png"><span class="text-danger point">【3.15】</span>点</p>	
                            <p>絶対にもう働きたくない！</p>
                        </div>
                    </li>

                    <li class="media">
                        <a href="page_ready_user_profile.php" class="pull-left">
                            <img src="img/placeholders/avatars/avatar2.jpg" alt="Avatar" class="img-circle">
                        </a>
                        <div class="media-body">
                            <span class="text-muted pull-right"><small>03/25 17:35</small></span>
                            <a href=""><strong>ゆうちゃんラブ</strong></a>
			　　<p class="push-bit"><img src="img/test/hosi1m.png"><img src="img/test/hosi1m.png"><img src="img/test/hosi2m.png"><img src="img/test/hosi3m.png"><img src="img/test/hosi3m.png"><span class="text-danger point">【3.15】</span>点</p>	
                            <p>古株さんがいて働きにくい</p>
                        </div>
                    </li>

                </ul>
                <!-- END Twitter Content -->
            </div>
   <!-- 口コミボックス -->


  <!-- 関連画像ここまで -->
            <div class="block">
                <!-- Photos Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                    </div>
                    <h2>お店の投稿された画像 <small>&bull; <a href="javascript:void(0)">25 Albums</a></small></h2>
                </div>
                <!-- END Photos Title -->

                <!-- Photos Content -->
                <div class="gallery" data-toggle="lightbox-gallery">
                    <div class="row">
                        <div class="col-xs-6 col-sm-3">
                            <a href="img/placeholders/photos/photo12.jpg" class="gallery-link" title="Image Info">
                                <img src="img/placeholders/photos/photo12.jpg" alt="image">
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <a href="img/placeholders/photos/photo15.jpg" class="gallery-link" title="Image Info">
                                <img src="img/placeholders/photos/photo15.jpg" alt="image">
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <a href="img/placeholders/photos/photo3.jpg" class="gallery-link" title="Image Info">
                                <img src="img/placeholders/photos/photo3.jpg" alt="image">
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <a href="img/placeholders/photos/photo4.jpg" class="gallery-link" title="Image Info">
                                <img src="img/placeholders/photos/photo4.jpg" alt="image">
                            </a>
                        </div>
                    </div>
                </div>
                <!-- END Photos Content -->
            </div>
            <!-- 関連画像ここまで -->


<!-- Info  -->
            <div class="block">
                <!-- Info Title -->
                <div class="block-title">
                    
                    <h2>コソットからのお知らせ<a href="javascript:void(0)" data-toggle="tooltip" title="Download Bio in PDF">Bio</a></small></h2>
                </div>
                <!-- END Info Title -->

                <!-- Info Content -->
                <table class="table table-borderless table-striped">
                    <tbody>
                        <tr>
                            <td style="width: 20%;"><strong>2016/08/25</strong></td>
                            <td>Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non.</td>
                        </tr>
                        <tr>
                            <td><strong>2016/08/25</strong></td>
                            <td>
				投稿タイトルが表示されます
                            </td>
                        </tr>
                        <tr>
                            <td><strong>2016/08/25</strong></td>
                            <td>
				投稿タイトルが表示されます
                            </td>
                        </tr>
                        <tr>
                            <td><strong>2016/08/25</strong></td>
                            <td>
				投稿タイトルが表示されます
                            </td>
                        </tr>
                        <tr>
                            <td><strong>2016/08/25</strong></td>
                            <td>
				投稿タイトルが表示されます
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- END Info Content -->
            </div>
            <!-- END Info  -->



	<!-- tool Block -->
            <div class="block">
                <!-- Share Title -->
                <div class="block-title">
                    <h2>ツール</h2>
                </div>
                <!-- END Share Title -->

                <!-- Share Content -->
                <div class="block-section text-center">
                    <div class="btn-group">
                        <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Facebook"><i class="si si-facebook"></i></a>
                        <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Twitter"><i class="si si-twitter"></i></a>
                        <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Google Plus"><i class="si si-google_plus"></i></a>
                        <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Pinterest"><i class="si si-pinterest"></i></a>
                    </div>
                </div>
                <!-- END Share Content -->
            </div>
            <!-- END Share Block -->


        </div>


<!--左右のコンテンツ終了6:6-->




    </div>
    <!-- END Dashboard 2 Content -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="js/helpers/excanvas.min.js"></script><![endif]-->

<?php include 'inc/template_scripts.php'; ?>

<!-- Google Maps API + Gmaps Plugin, must be loaded in the page you would like to use maps -->
<script src="//maps.google.com/maps/api/js"></script>
<script src="js/helpers/gmaps.min.js"></script>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/index2.js"></script>
<script>$(function(){ Index2.init(); });</script>

<?php include 'inc/template_end.php'; ?>