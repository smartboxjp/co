<?php include 'inc/config.php'; ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">

<?php include 'inc/page_headnavi.php'; ?>

    <!-- パンクズリスト -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="">管理画面</a></li>
        <li><a href="">各種情報更新</a></li>
        <li>有料用情報</li>
    </ul>
    <!-- END Forms General Header -->






    <div class="row">


<!--右ナビゲーションimg-->
	<div class="col-sm-2">
            <div class="block">

		<div class="block"><p><img src="img/navimg/exdeta.png" class="img-responsive center-block"></p></div>
	    </div>
	</div>

<!--右ナビゲーションimg-->


<!--残り文字数表示-->
	<script type="text/javascript"><!--
	function CountDownLength( idn, str, mnum ) {
	document.getElementById(idn).innerHTML = "(あと" + (mnum - str.length) + "文字以内)";
	}
	--></script>
<!--残り文字数表示-->



<!--右店舗入力フォーム-->


        <div class="col-md-10">

            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2>有料店用の情報入力</h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form action="page_forms_general.php" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" onsubmit="return false;">



<!--折り畳みPRコメント -->

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-textarea-input">PRコメント</label>
                        <div class="col-md-9">
                            <textarea id="example-textarea-input" name="example-textarea-input" rows="10" cols="48" onkeyup="CountDownLength( 'cdlength1' , value , 480 );" rap="hard" class="form-control" placeholder="例)どこよりも高待遇で、働きやすい職場を..."></textarea>
				<span class="help-block">お店のPRコメントを入力出来ます。48文字で改行されスペースや改行も一文字と計算されます。</span>
				<p id="cdlength1">(あと480文字以内)</p>
                        </div>
                    </div>




<!-- ラインID -->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-text-input">ラインID</label>
                        <div class="col-md-9">
                            <input type="text" id="example-text-input" name="example-text-input" cols="20" onkeyup="CountDownLength('cdlength2', value, 20);" class="form-control" placeholder="cossot">
                            <span class="help-block">女の子とやり取り出来るラインのIDを入力出来ます。半角英数20文字まで</span>
			    <p id="cdlength2">(あと20文字以内)</p>
                        </div>
                    </div>



<!-- メイン画像コメント -->

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-text-input">画像1コメント</label>
                        <div class="col-md-9">
				<div class="imgbottomspace">
					<img src="img/test/photo1.jpg" width="500px" height="281px">
				</div>
                            <input type="text" id="example-text-input" name="example-text-input" cols="30" onkeyup="CountDownLength('cdlength3', value, 30);" class="form-control" placeholder="例)今なら求人強化月間！">
                            <span class="help-block">1枚目のメイン画像下にコメントを入れることが出来ます。30文字まで</span>
			    <p id="cdlength3">(あと30文字以内)</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-text-input">画像2コメント</label>
                        <div class="col-md-9">
				<div class="imgbottomspace">
					<img src="img/test/photo2.jpg" width="500px" height="281px">
				</div>
                            <input type="text" id="example-text-input" name="example-text-input" cols="36" onkeyup="CountDownLength('cdlength4', value, 30);" class="form-control" placeholder="例)今なら求人強化月間！">
                            <span class="help-block">2枚目のメイン画像下にコメントを入れることが出来ます。30文字まで</span>
			    <p id="cdlength4">(あと30文字以内)</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-text-input">画像3コメント</label>
                        <div class="col-md-9">
				<div class="imgbottomspace">
					<img src="img/test/photo3.jpg" width="500px" height="281px">
				</div>
                            <input type="text" id="example-text-input" name="example-text-input" cols="30" onkeyup="CountDownLength('cdlength5', value, 30);" class="form-control" placeholder="例)今なら求人強化月間！">
                            <span class="help-block">3枚目のメイン画像下にコメントを入れることが出来ます。30文字まで</span>
			    <p id="cdlength5">(あと30文字以内)</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-text-input">画像4コメント</label>
                        <div class="col-md-9">
				<div class="imgbottomspace">
					<img src="img/test/photo4.jpg" width="500px" height="281px">
				</div>
                            <input type="text" id="example-text-input" name="example-text-input" cols="36" onkeyup="CountDownLength('cdlength6', value, 30);" class="form-control" placeholder="例)今なら求人強化月間！">
                            <span class="help-block">4枚目のメイン画像下にコメントを入れることが出来ます。30文字まで</span>
			    <p id="cdlength6">(あと30文字以内)</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-text-input">画像5コメント</label>
                        <div class="col-md-9">
				<div class="imgbottomspace">
					<img src="img/test/photo5.jpg" width="500px" height="281px">
				</div>
                            <input type="text" id="example-text-input" name="example-text-input" cols="36" onkeyup="CountDownLength('cdlength7', value, 30);" class="form-control" placeholder="例)今なら求人強化月間！">
                            <span class="help-block">5枚目のメイン画像下にコメントを入れることが出来ます。30文字まで</span>
			    <p id="cdlength7">(あと30文字以内)</p>
                        </div>
                    </div>






                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> 登録</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> 修正</button>
                        </div>
                    </div>



                </form>
                <!-- END Basic Form Elements Content -->

            </div>
            <!-- END Block -->

        </div>
	<!--col-md-8-->
<!--右店舗入力フォーム-->




    </div>
       <!--low-->

</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/formsGeneral.js"></script>
<script>$(function(){ FormsGeneral.init(); });</script>

<?php include 'inc/template_end.php'; ?>