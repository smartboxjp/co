<?php include 'inc/config.php'; ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">

<?php include 'inc/page_headnavi.php'; ?>

    <!-- パンクズリスト -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="">管理画面</a></li>
        <li><a href="">各種情報更新</a></li>
        <li>基本情報</li>
    </ul>
    <!-- END Forms General Header -->






    <div class="row">


<!--右ナビゲーションimg-->
	<div class="col-sm-2">
            <div class="block">
		<div class="block"><p><img src="img/navimg/maindeta.png" class="img-responsive center-block"></p></div>
	    </div>
	</div>

<!--右ナビゲーションimg-->


<!--残り文字数表示-->
	<script type="text/javascript"><!--
	function CountDownLength( idn, str, mnum ) {
	document.getElementById(idn).innerHTML = "(あと" + (mnum - str.length) + "文字以内)";
	}
	--></script>
<!--残り文字数表示-->



<!--右店舗入力フォーム-->


        <div class="col-md-10">

            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2>お店の基本情報入力</h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form action="page_forms_general.php" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" onsubmit="return false;">


<!-- 営業時間 -->
                    <div class="form-group">
                            <label class="col-md-3 control-label" for="example-daterange1">お店の営業時間</label>
                            <div class="col-md-9">
                                <div class="input-group">
				    <select id="val_skill" name="val_skill" class="form-control">
                                    	<option value="">開始時間</option>
                                    	<option value="朝6時から">朝6時から</option>
                                    	<option value="朝7時から">朝7時から</option>
                                    	<option value="朝8時から">朝8時から</option>
                                    	<option value="朝9時から">朝9時から</option>
                                    	<option value="10時から">10時から</option>
                                    	<option value="11時から">11時から</option>
                                    	<option value="12時から">12時から</option>
                                    	<option value="13時から">13時から</option>
                                    	<option value="14時から">14時から</option>
                                    	<option value="15時から">15時から</option>
                                    	<option value="16時から">16時から</option>
                                    	<option value="17時から">17時から</option>
                                    	<option value="18時から">18時から</option>
                                    	<option value="19時から">19時から</option>
                                    	<option value="20時から">20時から</option>
                                    	<option value="21時から">21時から</option>
                                    	<option value="22時から">22時から</option>
                                    	<option value="23時から">23時から</option>
                                    	<option value="24時から">24時から</option>
                                    	<option value="深夜から">深夜から</option>
                                    	<option value="早朝から">早朝から</option>
                                    	<option value="お昼から">お昼から</option>
                                    	<option value="不定期">不定期</option>
                                    	<option value="24時間営業">24時間営業</option>
                                    	</select>

                                    <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>

                                    <select id="val_skill" name="val_skill" class="form-control">
                                    	<option value="">終了時間</option>
                                    	<option value="朝6時まで">朝6時まで</option>
                                    	<option value="朝7時まで">朝7時まで</option>
                                    	<option value="朝8時まで">朝8時まで</option>
                                    	<option value="朝9時まで">朝9時から</option>
                                    	<option value="10時まで">10時まで</option>
                                    	<option value="11時まで">11時まで</option>
                                    	<option value="12時まで">12時まで</option>
                                    	<option value="13時まで">13時まで</option>
                                    	<option value="14時まで">14時まで</option>
                                    	<option value="15時まで">15時まで</option>
                                    	<option value="16時まで">16時まで</option>
                                    	<option value="17時まで">17時まで</option>
                                    	<option value="18時まで">18時まで</option>
                                    	<option value="19時まで">19時まで</option>
                                    	<option value="20時まで">20時まで</option>
                                    	<option value="21時まで">21時まで</option>
                                    	<option value="22時まで">22時まで</option>
                                    	<option value="23時まで">23時まで</option>
                                    	<option value="24時まで">24時まで</option>
                                    	<option value="深夜まで">深夜まで</option>
                                    	<option value="早朝まで">早朝まで</option>
                                    	<option value="お昼まで">お昼まで</option>
                                    	<option value="不定期">不定期</option>
                                    	<option value="24時間営業">24時間営業</option>
                                    </select>
                                </div>
				<span class="help-block">お店の営業時間を選択して下さい。(24時間)の場合は開始だけ選択して下さい</span>
                            </div>
                     </div>

<!-- 住所 -->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-text-input">住所</label>
                        <div class="col-md-9">
                            <input type="text" id="example-text-input" name="example-text-input" cols="36" onkeyup="CountDownLength('cdlength1', value, 36);" class="form-control" placeholder="例)東京都渋谷区道玄坂付近(渋谷駅南口徒歩5分)">
                            <span class="help-block">お店や事務所の住所をある程度まで記載。または最寄り駅名。36文字まで。</span>
			    <p id="cdlength1">(あと32文字以内)</p>
                        </div>
                    </div>

<!-- 電話番号 -->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-email-input">電話番号</label>
                        <div class="col-md-9">
                            <input type="text" id="example-email-input" name="example-email-input" class="form-control" placeholder="例)03-000-0000">
                            <span class="help-block">直接お問合せする場合の電話番号。</span>
                        </div>
                    </div>

<!-- メールアドレス -->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-email-input">メールアドレス</label>
                        <div class="col-md-9">
                            <input type="email" id="example-email-input" name="example-email-input" class="form-control" placeholder="例)cossot@gmail.com">
                            <span class="help-block">直接お問合せする場合のメールアドレス。</span>
                        </div>
                    </div>
<!-- お仕事内容 -->

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-textarea-input">お仕事の内容</label>
                        <div class="col-md-9">
                            <textarea id="example-textarea-input" name="example-textarea-input" rows="2" cols="36" onkeyup="CountDownLength( 'cdlength2' , value , 72 );" rap="hard" class="form-control" placeholder="例)当店のお仕事内容はソープランドという業種になりますので..."></textarea>
				<span class="help-block">お店の仕事内容を最大72文字以内で記載して下さい。改行も1文字に含まれます。</span>
				<p id="cdlength2">(あと72文字以内)</p>
                        </div>
                    </div>


<!-- お給料体形 -->
                    <div class="form-group">
                            <label class="col-md-3 control-label" for="example-daterange1">お給料体系</label>
                            <div class="col-md-9">
                                <div class="input-group">
				    <select id="val_skill" name="val_skill" class="form-control">
                                    	<option value="">計算方法</option>
                                    	<option value="単価制">単価制</option>
                                    	<option value="時給制">時給制</option>
                                    	<option value="日給制">日給制</option>
					<option value="月給制">月給制</option>
                                    	<option value="その他">その他</option>
                                    </select>

                                    <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>
					<input type="text" id="example-text-input" name="example-text-input" cols="36" onkeyup="CountDownLength('cdlength3', value, 36);" class="form-control" placeholder="例)60分8500円～">
                                </div>
				<span class="help-block">お給料の計算方法と代表的な料金の例を入力してください。</span>
				<p id="cdlength3">(あと36文字以内)</p>
                            </div>
                     </div>
<!-- 業務形態 -->
			<div class="form-group">
                            <label class="col-md-3 control-label" for="val_skill">業務形態</label>
                            <div class="col-md-9">
                                <select id="val_skill" name="val_skill" class="form-control">
                                    <option value="">集客スタイル</option>
                                    <option value="店舗型">店舗型</option>
                                    <option value="デリバリー型">デリバリー型</option>
                                    <option value="その他">その他</option>
                                </select>
                            </div>
                        </div>

<!-- 募集年齢 -->
                    <div class="form-group">
                            <label class="col-md-3 control-label" for="example-daterange1">募集資格年齢</label>
                            <div class="col-md-9">
                                <div class="input-group">
				    <select id="val_skill" name="val_skill" class="form-control">
                                    	<option value="">何歳から</option>
                                    	<option value="18歳">18歳</option>
                                    	<option value="19歳">19歳</option>
                                    	<option value="20歳">20歳</option>
                                    	<option value="21歳">21歳</option>
                                    	<option value="22歳">22歳</option>
                                    	<option value="23歳">23歳</option>
                                    	<option value="24歳">24歳</option>
                                    	<option value="25歳">25歳</option>
                                    	<option value="26歳">26歳</option>
                                    	<option value="27歳">27歳</option>
                                    	<option value="28歳">28歳</option>
                                    	<option value="29歳">29歳</option>
                                    	<option value="30歳">30歳</option>
                                    	<option value="31歳">31歳</option>
                                    	<option value="32歳">32歳</option>
                                    	<option value="33歳">33歳</option>
                                    	<option value="34歳">34歳</option>
                                    	<option value="35歳">35歳</option>
                                    	<option value="36歳">36歳</option>
                                    	<option value="37歳">37歳</option>
                                    	<option value="38歳">38歳</option>
					<option value="39歳">39歳</option>
                                    	<option value="40歳">40歳</option>
                                    	<option value="41歳">41歳</option>
                                    	<option value="42歳">42歳</option>
                                    	<option value="43歳">43歳</option>
                                    	<option value="44歳">44歳</option>
                                    	<option value="45歳以上">45歳以上</option>
                                    	<option value="不問">不問</option>
                                    	</select>

                                    <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>

                                    <select id="val_skill" name="val_skill" class="form-control">
                                    	<option value="">何歳位まで</option>
                                    	<option value="19歳位まで">19歳位まで</option>
                                    	<option value="20歳位まで">20歳位まで</option>
                                    	<option value="21歳位まで">21歳位まで</option>
                                    	<option value="22歳位まで">22歳位まで</option>
                                    	<option value="23歳位まで">23歳位まで</option>
                                    	<option value="24歳位まで">24歳位まで</option>
                                    	<option value="25歳位まで">25歳位まで</option>
                                    	<option value="26歳位まで">26歳位まで</option>
                                    	<option value="27歳位まで">27歳位まで</option>
                                    	<option value="28歳位まで">28歳位まで</option>
                                    	<option value="29歳位まで">29歳位まで</option>
                                    	<option value="30歳位まで">30歳位まで</option>
                                    	<option value="31歳位まで">31歳位まで</option>
                                    	<option value="32歳位まで">32歳位まで</option>
                                    	<option value="33歳位まで">33歳位まで</option>
                                    	<option value="34歳位まで">34歳位まで</option>
                                    	<option value="35歳位まで">35歳位まで</option>
                                    	<option value="36歳位まで">36歳位まで</option>
                                    	<option value="37歳位まで">37歳位まで</option>
                                    	<option value="38歳位まで">38歳位まで</option>
					<option value="39歳位まで">39歳位まで</option>
                                    	<option value="40歳位まで">40歳位まで</option>
                                    	<option value="41歳位まで">41歳位まで</option>
                                    	<option value="42歳位まで">42歳位まで</option>
                                    	<option value="43歳位まで">43歳位まで</option>
                                    	<option value="44歳位まで">44歳位まで</option>
                                    	<option value="45歳以上">45歳以上</option>
                                    	<option value="不問">不問</option>
                                    </select>
                                </div>
				<span class="help-block">(不問)と(45歳以上)の場合は開始だけ選択してください。</span>
                            </div>
                     </div>

<!-- 募集時間 -->
                    <div class="form-group">
                            <label class="col-md-3 control-label" for="example-daterange1">特に募集したい時間帯</label>
                            <div class="col-md-9">
                                <div class="input-group">
				    <select id="val_skill" name="val_skill" class="form-control">
                                    	<option value="">開始時間</option>
                                    	<option value="朝6時から">朝6時から</option>
                                    	<option value="朝7時から">朝7時から</option>
                                    	<option value="朝8時から">朝8時から</option>
                                    	<option value="朝9時から">朝9時から</option>
                                    	<option value="10時から">10時から</option>
                                    	<option value="11時から">11時から</option>
                                    	<option value="12時から">12時から</option>
                                    	<option value="13時から">13時から</option>
                                    	<option value="14時から">14時から</option>
                                    	<option value="15時から">15時から</option>
                                    	<option value="16時から">16時から</option>
                                    	<option value="17時から">17時から</option>
                                    	<option value="18時から">18時から</option>
                                    	<option value="19時から">19時から</option>
                                    	<option value="20時から">20時から</option>
                                    	<option value="21時から">21時から</option>
                                    	<option value="22時から">22時から</option>
                                    	<option value="23時から">23時から</option>
                                    	<option value="24時から">24時から</option>
                                    	<option value="深夜から">深夜から</option>
                                    	<option value="早朝から">早朝から</option>
                                    	<option value="お昼から">お昼から</option>
                                    	<option value="営業時間帯全て">営業時間帯全て</option>
                                    	<option value="24時間全て">24時間全て</option>
                                    	</select>

                                    <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>

                                    <select id="val_skill" name="val_skill" class="form-control">
                                    	<option value="">終了時間</option>
                                    	<option value="朝6時まで">朝6時まで</option>
                                    	<option value="朝7時まで">朝7時まで</option>
                                    	<option value="朝8時まで">朝8時まで</option>
                                    	<option value="朝9時まで">朝9時から</option>
                                    	<option value="10時まで">10時まで</option>
                                    	<option value="11時まで">11時まで</option>
                                    	<option value="12時まで">12時まで</option>
                                    	<option value="13時まで">13時まで</option>
                                    	<option value="14時まで">14時まで</option>
                                    	<option value="15時まで">15時まで</option>
                                    	<option value="16時まで">16時まで</option>
                                    	<option value="17時まで">17時まで</option>
                                    	<option value="18時まで">18時まで</option>
                                    	<option value="19時まで">19時まで</option>
                                    	<option value="20時まで">20時まで</option>
                                    	<option value="21時まで">21時まで</option>
                                    	<option value="22時まで">22時まで</option>
                                    	<option value="23時まで">23時まで</option>
                                    	<option value="24時まで">24時まで</option>
                                    	<option value="深夜まで">深夜まで</option>
                                    	<option value="早朝まで">早朝まで</option>
                                    	<option value="お昼まで">お昼まで</option>
                                    	<option value="営業時間帯全て">営業時間帯全て</option>
                                    	<option value="24時間全て">24時間営業全て</option>
                                    </select>
                                </div>
				<span class="help-block">お店の営業時間内で特に女の子を募集してる時間帯を選んで下さい。(営業時間帯全て)(24時間全て)の場合は開始だけ選択してください。</span>
                            </div>
                     </div>

<!-- 営業用オフィシャルページ -->
                    <div class="form-group">
                         <label class="col-md-3 control-label" for="example-password-input">営業オフィシャルページ</label>
                         <div class="col-md-9">
                                <div class="input-group">
                                    <input type="text" id="val_website" name="val_website" class="form-control" value="http://">
                                    <span class="help-block">お店の営業ページのURLを記載して下さい。</span>
                                </div>
                            </div>
                    </div>
<!-- 求人用オフィシャルページ -->
                    <div class="form-group">
                         <div class="form-group">
                         <label class="col-md-3 control-label" for="example-email-input">求人オフィシャルページ</label>
                         <div class="col-md-9">
                                <div class="input-group">
                                    <input type="text" id="val_website" name="val_website" class="form-control" value="http://">
                                    <span class="help-block">お店の求人サイトのURLを記載して下さい。</span>
                                </div>
                            </div>
                    </div>

<!-- 罰金ノルマ -->

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-text-input">罰金ノルマ等</label>
                        <div class="col-md-9">
                            <input type="text" id="example-text-input" name="example-text-input" cols="36" onkeyup="CountDownLength('cdlength4', value, 36);" class="form-control" placeholder="例)無断欠勤の場合は罰金が発生する場合があります">
                            <span class="help-block">お店の罰金やノルマ、ルールなどがあれば記載して下さい。36文字まで。</span>
			    <p id="cdlength4">(あと32文字以内)</p>
                        </div>
                    </div>

<!-- お店からのPRコメント -->

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-textarea-input">お店からのPRコメント</label>
                        <div class="col-md-9">
                            <textarea id="example-textarea-input" name="example-textarea-input" rows="3" cols="36" onkeyup="CountDownLength( 'cdlength5' , value , 108 );" rap="hard" class="form-control" placeholder="例)まだオープンして間もないですが、スタッフ全員が仲が良く..."></textarea>
				<span class="help-block">お店のPRコメントを最大108文字以内で記載して下さい。改行も1文字に含まれます。</span>
				<p id="cdlength5">(あと108文字以内)</p>
                        </div>
                    </div>



                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> 登録</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> 修正</button>
                        </div>
                    </div>



                </form>
                <!-- END Basic Form Elements Content -->

            </div>
            <!-- END Block -->

        </div>
	<!--col-md-8-->
<!--右店舗入力フォーム-->





    </div>
       <!--low-->

</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/formsGeneral.js"></script>
<script>$(function(){ FormsGeneral.init(); });</script>

<?php include 'inc/template_end.php'; ?>