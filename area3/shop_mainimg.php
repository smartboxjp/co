<?php include 'inc/config.php'; ?>
<?php include 'inc/template_start_croppermainimg.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">

<?php include 'inc/page_headnavi.php'; ?>

    <!-- パンクズリスト -->
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="">管理画面</a></li>
        <li><a href="">ツール</a></li>
        <li>お店メイン画像編集</li>
    </ul>
    <!-- END Forms General Header -->


	<div class="row"><!--外枠-->



<!--右ナビゲーションimg-->
	<div class="col-sm-2">
            <div class="block">
		<div class="block"><p><img src="img/navimg/mainimg.png" class="img-responsive center-block"></p></div>
	    </div>
	</div>

<!--右ナビゲーションimg-->


		<div class="col-md-10"><!--グリット-->


<!--ブロック-->
			<div class="block"><!--コンテンツ外枠-->


				<div class="block-title">
                    			<h2>お店のメイン画像(670px×377px)を登録する事が出来ます。</h2>
                		</div>



<!-- クリッパー ------------------------------------------------------------------>
  <!--[if lt IE 9]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->

  <!-- Content -->
  <div class="container">
    <div class="row">

<!-- キャンパス -->
      <div class="col-md-9">
        <!-- <h3 class="page-header">Demo:</h3> -->
        <div class="img-container">
          <img src="img/default/picture.jpg" alt="Picture">
        </div>
      </div>
<!-- キャンパス -->

      <div class="col-md-3"><!--右ブロック-->

<!-- プレビューブロック -->
        <!-- <h3 class="page-header">Preview:</h3> -->
        <div class="docs-preview clearfix">
          <div class="img-preview preview-lg"></div>
        </div>

<!-- サイズ、座標 -->
        <!-- <h3 class="page-header">Data:</h3> -->
        <div class="docs-data">
          <div class="input-group input-group-sm">
            <label class="input-group-addon" for="dataX">座標(横)</label>
            <input type="text" class="form-control" id="dataX" placeholder="x">
            <span class="input-group-addon">px</span>
          </div>
          <div class="input-group input-group-sm">
            <label class="input-group-addon" for="dataY">座標(縦)</label>
            <input type="text" class="form-control" id="dataY" placeholder="y">
            <span class="input-group-addon">px</span>
          </div>
          <div class="input-group input-group-sm">
            <label class="input-group-addon" for="dataWidth">横幅サイズ</label>
            <input type="text" class="form-control" id="dataWidth" placeholder="width">
            <span class="input-group-addon">px</span>
          </div>
          <div class="input-group input-group-sm">
            <label class="input-group-addon" for="dataHeight">高さサイズ</label>
            <input type="text" class="form-control" id="dataHeight" placeholder="height">
            <span class="input-group-addon">px</span>
          </div>
          <div class="input-group input-group-sm">
            <label class="input-group-addon" for="dataRotate">角度</label>
            <input type="text" class="form-control" id="dataRotate" placeholder="rotate">
            <span class="input-group-addon">deg</span>
          </div>

        </div>
<!-- サイズ、座標 -->

      </div><!--<div class="col-md-3"><!--右ブロック-->

    </div><!--low-->






<!--コントロールパネル-->
    <div class="row" id="actions">
      <div class="col-md-9 docs-buttons">

        <div class="btn-group">
          <button type="button" class="btn btn-primary" data-method="zoom" data-option="0.1" title="Zoom In">
            <span class="docs-tooltip" data-toggle="tooltip" title="画像を拡大">
              <span class="fa fa-search-plus"></span>
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-method="zoom" data-option="-0.1" title="Zoom Out">
            <span class="docs-tooltip" data-toggle="tooltip" title="画像を縮小">
              <span class="fa fa-search-minus"></span>
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-method="setDragMode" data-option="move" title="Move">
            <span class="docs-tooltip" data-toggle="tooltip" title="画像をドラックで移動">
              <span class="fa fa-arrows"></span>
            </span>
          </button>
        </div>

        <div class="btn-group">
          <button type="button" class="btn btn-primary" data-method="move" data-option="-10" data-second-option="0" title="Move Left">
            <span class="docs-tooltip" data-toggle="tooltip" title="左へ移動">
              <span class="fa fa-arrow-left"></span>
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-method="move" data-option="10" data-second-option="0" title="Move Right">
            <span class="docs-tooltip" data-toggle="tooltip" title="右へ移動">
              <span class="fa fa-arrow-right"></span>
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="-10" title="Move Up">
            <span class="docs-tooltip" data-toggle="tooltip" title="上へ移動">
              <span class="fa fa-arrow-up"></span>
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="10" title="Move Down">
            <span class="docs-tooltip" data-toggle="tooltip" title="下へ移動">
              <span class="fa fa-arrow-down"></span>
            </span>
          </button>
        </div>

        <div class="btn-group">
          <button type="button" class="btn btn-primary" data-method="rotate" data-option="-45" title="Rotate Left">
            <span class="docs-tooltip" data-toggle="tooltip" title="45°左へ回転">
              <span class="fa fa-rotate-left"></span>
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-method="rotate" data-option="45" title="Rotate Right">
            <span class="docs-tooltip" data-toggle="tooltip" title="45°右へ回転">
              <span class="fa fa-rotate-right"></span>
            </span>
          </button>
        </div>

        <div class="btn-group">
          <button type="button" class="btn btn-primary" data-flip="horizontal" data-method="scaleX" data-option="-1" title="Flip Horizontal">
            <span class="docs-tooltip" data-toggle="tooltip" title="左右反転">
              <span class="fa fa-arrows-h"></span>
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-flip="vertical" data-method="scaleY" data-option="-1" title="Flip Vertical">
            <span class="docs-tooltip" data-toggle="tooltip" title="上下反転">
              <span class="fa fa-arrows-v"> </span>
            </span>
          </button>
        </div>

        <div class="btn-group">
          <button type="button" class="btn btn-primary" data-method="crop" title="Crop">
            <span class="docs-tooltip" data-toggle="tooltip" title="カット機能ON">
              <span class="fa fa-check"></span>
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-method="clear" title="Clear">
            <span class="docs-tooltip" data-toggle="tooltip" title="カット機能OFF">
              <span class="fa fa-remove"></span>
            </span>
          </button>
        </div>

        <div class="btn-group">
          <button type="button" class="btn btn-primary" data-method="disable" title="Disable">
            <span class="docs-tooltip" data-toggle="tooltip" title="ロック">
              <span class="fa fa-lock"></span>
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-method="enable" title="Enable">
            <span class="docs-tooltip" data-toggle="tooltip" title="ロック解除">
              <span class="fa fa-unlock"></span>
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-method="reset" title="Reset">
            <span class="docs-tooltip" data-toggle="tooltip" title="カーソルリセット">
              <span class="fa fa-refresh"></span>
            </span>
          </button>
        </div>

        <div class="btn-group btn-group-crop">
          <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
            <input type="file" class="sr-only" id="inputImage" name="file" accept="image/*">
            <span class="docs-tooltip" data-toggle="tooltip" title="画像をアップロードする">
              <span class="fa fa-upload"> 編集画像をアップする</span>
            </span>
          </label>
	</div>



<!--クリップボード用ボタン-->
        <div class="btn-group btn-group-crop">
          <button type="button" class="btn btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;width&quot;: 670, &quot;height&quot;: 377 }">
            <span class="docs-tooltip" data-toggle="tooltip" title="保存用にクリップボードに張り付け">
              <spna class="fa fa-share"> クリップボードに張り付け</span>
            </span>
          </button>
        </div>

        <!-- クリップボード -->
        <div class="modal fade docs-cropped" id="getCroppedCanvasModal" role="dialog" aria-hidden="true" aria-labelledby="getCroppedCanvasTitle" tabindex="-1">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="getCroppedCanvasTitle">ダウンロードか右クリックで保存して下さい。<small>※Safariでは保存出来ません。</small></h4>
              </div>
              <div class="modal-body"></div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a class="btn btn-primary" id="download" href="javascript:void(0);" download="cropped.jpg">Download</a>
                
              </div>
            </div>
          </div>
        </div>
<!-- クリップボード -->


      </div><!-- /.docs-buttons -->


<!-- 決定ボタン -->
      <div class="col-md-3 docs-toggles">

        <div class="btn-group btn-group-justified" data-toggle="buttons">
          <label class="btn btn-danger">
            <span class="docs-tooltip" data-toggle="tooltip" title="画像は670×377か、それより大きいサイズでカットしてください">
              これで決定する
            </span>
          </label>
        </div>

        <div class="btn-group btn-group-justified">
		<a href="http://www.yahoo.co.jp" target="_blank"><i class="fa fa-question"> 使い方について</i></a>
        </div>

      </div>
<!-- 決定ボタン -->



    </div>
  </div>

<!-- クリッパー ------------------------------------------------------------------>


			</div><!--block-->

		</div><!--col-md-12-->




	</div><!--low-->


</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/formsGeneral.js"></script>
<script>$(function(){ FormsGeneral.init(); });</script>


  
  <?php include 'inc/template_end.php'; ?>