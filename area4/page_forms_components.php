<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/config.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_start.php';
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_head.php';
?>


<!-- Page content -->
<div id="page-content">


    <div class="block">

        <!-- CKEditor Content -->
        <form action="page_forms_components.php" method="post" class="form-horizontal form-bordered" onsubmit="return false;">

            <fieldset>
                <div class="form-group">
                    <div class="col-xs-12">
                        <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"></textarea>
                    </div>
                </div>
            </fieldset>
            
            <fieldset>
                <div class="form-group">
                    <div class="col-xs-12">
    
						<div id="summernote">Hello Summernote</div>
						<script>
							$(document).ready(function() {
  							$('#summernote').summernote();
							});
						</script>	

                    </div>
                </div>
            </fieldset>

        </form>
        <!-- END CKEditor Content -->
    </div>
    <!-- END CKEditor Block -->
</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/page_footer.php'; ?>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_scripts.php'; ?>
		
<script src="/app_manager/js/tool/summernote/summernote.js"></script>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_manager/inc/template_end.php'; ?>