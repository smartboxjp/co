<?php

// 日本語関連の設定
date_default_timezone_set('Asia/Tokyo');
mb_language('ja');
mb_internal_encoding('utf-8');

// エラーログの設定
error_reporting(E_ALL);
//error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_startup_errors', false);
ini_set('display_errors', false);
ini_set('log_errors', true);
ini_set('error_log', dirname(__FILE__) . '/log/error.log');

// データベースのパスワード読み込み
global $db_host, $db_username, $db_password, $db_name;
eval(file_get_contents(dirname(__FILE__) . '/.db_password'));

//error_log(var_export($_REQUEST, TRUE));

	$mysql = new mysqli($db_host, $db_username, $db_password, $db_name);
	if (! $mysql->set_charset('utf8'))
		error_log($mysql->error);
	
	if ($mysql->connect_error) {
		error_log($mysql->connect_errno . ':' . $mysql->connect_error);
	}

if ($_REQUEST['action'] == 'removeEvent'){
	$params_sql = array();
	foreach ($_REQUEST['event'] as $key => $p){
		$params_sql[$key] = $mysql->real_escape_string($p);
	}

	if (isset($params_sql['_id']) && $params_sql['_id']){
		$sql = "DELETE FROM `fc_event` WHERE `id` = {$params_sql['_id']} ";

		$delete_result = $mysql->query($sql);

		if ($delete_result){
			echo json_encode(array('result' => true));
		}
		else {
			error_log($mysql->error);
			echo json_encode(array('result' => false));
		}
	}
	else {
		echo json_encode(array('result' => false));
	}
}
else if ($_REQUEST['action'] == 'createEvent'){
	$params_sql = array();
	foreach ($_REQUEST['event'] as $key => $p){
		$params_sql[$key] = $mysql->real_escape_string($p);
	}

	if (isset($params_sql['_id']) && $params_sql['_id']){
		$sql = "INSERT INTO `fc_event` (`id`, `title`, `start`, `end`, `allDay`) VALUES ('{$params_sql['_id']}', '{$params_sql['title']}', '{$params_sql['start']}', '{$params_sql['end']}', {$params_sql['allDay']} ) "
			. " ON DUPLICATE KEY UPDATE `title` = '{$params_sql['title']}', `start` = '{$params_sql['start']}', `end` = '{$params_sql['end']}', `allDay` = '{$params_sql['allDay']}' ";	// allDayの値はtrue/falseなので引用符なし
	}
	else {
		$sql = "INSERT INTO `fc_event` (`title`, `start`, `end`, `allDay`) VALUES ('{$params_sql['title']}', '{$params_sql['start']}', '{$params_sql['end']}', {$params_sql['allDay']} ) ";	// allDayの値はtrue/falseなので引用符なし
	}

	$insert_result = $mysql->query($sql);

	if ($insert_result){
		$event = array(
			'title' => $_REQUEST['event']['title'],
			'start' => $_REQUEST['event']['start'],
			'end' => $_REQUEST['event']['end'],
			'allDay' => $_REQUEST['event']['allDay'] ? 'true' : '',
			'_id' => (string)$mysql->insert_id ?: $_REQUEST['event']['_id'],	// idではなく_id
		);
		echo json_encode(array('result' => true, 'event' => $event));
	}
	else {
		error_log($mysql->error);
		echo json_encode(array('result' => false));
	}
}
else if ($_REQUEST['action'] == 'getEvents'){

	$sql = "SELECT * FROM `fc_event` WHERE 1";
	$select_result = $mysql->query($sql);

	if ($select_result){
		$events = array();
		while ($row = $select_result->fetch_assoc()){
			$events[] = array(
				'title' => $row['title'],
				'start' => $row['start'],
				'end' => $row['end'],
				'allDay' => $row['allDay'] ? 'true' : '',
				'_id' => $row['id'],	// idではなく_id
			);
		}
		echo json_encode($events);	// FullCalendarが読み込める形式で
	}
	else {
		echo json_encode(false);
	}
}

$mysql->close();

