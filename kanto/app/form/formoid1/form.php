<?php

define('EMAIL_FOR_REPORTS', 'daikonnno@gmail.com');
define('RECAPTCHA_PRIVATE_KEY', '@privatekey@');
define('FINISH_URI', 'http://');
define('FINISH_ACTION', 'message');
define('FINISH_MESSAGE', '口コミが投稿されました。
反映はすぐにされますが、信憑性の
評価には時間がかかる事があります。');
define('UPLOAD_ALLOWED_FILE_TYPES', 'doc, docx, xls, csv, txt, rtf, html, zip, jpg, jpeg, png, gif');

define('_DIR_', str_replace('\\', '/', dirname(__FILE__)) . '/');
require_once _DIR_ . '/handler.php';

?>

<?php if (frmd_message()): ?>
<link rel="stylesheet" href="<?php echo dirname($form_path); ?>/formoid-default-skyblue.css" type="text/css" />
<span class="alert alert-success"><?php echo FINISH_MESSAGE; ?></span>
<?php else: ?>
<!-- Start Formoid form-->
<link rel="stylesheet" href="<?php echo dirname($form_path); ?>/formoid-default-skyblue.css" type="text/css" />
<script type="text/javascript" src="<?php echo dirname($form_path); ?>/jquery.min.js"></script>
<form class="formoid-default-skyblue" style="background-color:#FFFFFF;font-size:14px;font-family:Arial,Helvetica,sans-serif;color:#666666;max-width:680px;min-width:150px" method="post"><div class="title"><h2>口コミを投稿する</h2></div>
	<div class="element-select<?php frmd_add_class("select"); ?>"><label class="title">このお店で働いた期間は？</label><div class="medium"><span><select name="select" >

		<option value="未回答">未回答</option>
		<option value="面接だけ">面接だけ</option>
		<option value="1ヶ月未満">1ヶ月未満</option>
		<option value="半年未満">半年未満</option>
		<option value="1年未満">1年未満</option>
		<option value="1年以上">1年以上</option></select><i></i></span></div></div>
	<div class="element-textarea<?php frmd_add_class("textarea2"); ?>" title="※15文字以上"><label class="title">お店のいいと思う所※15文字以上<span class="required">*</span></label><textarea class="small" name="textarea2" cols="20" rows="5" required="required"></textarea></div>
	<div class="element-textarea<?php frmd_add_class("textarea1"); ?>"><label class="title">お店の悪いと思う所※15文字以上<span class="required">*</span></label><textarea class="small" name="textarea1" cols="20" rows="5" required="required"></textarea></div>
	<div class="element-rating<?php frmd_add_class("rating"); ?>"><label class="title">あなたの評価<span class="required">*</span></label><div class="rating"><input type="radio" class="rating-input" id="rating-10" name="rating" value="10" /><label for="rating-10" class="rating-star"></label><input type="radio" class="rating-input" id="rating-9" name="rating" value="9" /><label for="rating-9" class="rating-star"></label><input type="radio" class="rating-input" id="rating-8" name="rating" value="8" /><label for="rating-8" class="rating-star"></label><input type="radio" class="rating-input" id="rating-7" name="rating" value="7" /><label for="rating-7" class="rating-star"></label><input type="radio" class="rating-input" id="rating-6" name="rating" value="6" /><label for="rating-6" class="rating-star"></label><input type="radio" class="rating-input" id="rating-5" name="rating" value="5" /><label for="rating-5" class="rating-star"></label><input type="radio" class="rating-input" id="rating-4" name="rating" value="4" /><label for="rating-4" class="rating-star"></label><input type="radio" class="rating-input" id="rating-3" name="rating" value="3" /><label for="rating-3" class="rating-star"></label><input type="radio" class="rating-input" id="rating-2" name="rating" value="2" /><label for="rating-2" class="rating-star"></label><input type="radio" class="rating-input" id="rating-1" name="rating" value="1" /><label for="rating-1" class="rating-star"></label></div></div>
	<div class="element-textarea<?php frmd_add_class("textarea"); ?>"><label class="title">口コミ内容※130文字以上<span class="required">*</span></label><textarea class="medium" name="textarea" cols="20" rows="5" required="required"></textarea></div>
<div class="submit"><input type="submit" value="投稿"/></div></form><script type="text/javascript" src="<?php echo dirname($form_path); ?>/formoid-default-skyblue.js"></script>

<!-- Stop Formoid form-->
<?php endif; ?>

<?php frmd_end_form(); ?>