<?php

define('EMAIL_FOR_REPORTS', '');
define('RECAPTCHA_PRIVATE_KEY', '@privatekey@');
define('FINISH_URI', 'http://');
define('FINISH_ACTION', 'message');
define('FINISH_MESSAGE', 'Thanks for filling out my form!');
define('UPLOAD_ALLOWED_FILE_TYPES', 'doc, docx, xls, csv, txt, rtf, html, zip, jpg, jpeg, png, gif');

define('_DIR_', str_replace('\\', '/', dirname(__FILE__)) . '/');
require_once _DIR_ . '/handler.php';

?>

<?php if (frmd_message()): ?>
<link rel="stylesheet" href="<?php echo dirname($form_path); ?>/formoid-default-skyblue.css" type="text/css" />
<span class="alert alert-success"><?php echo FINISH_MESSAGE; ?></span>
<?php else: ?>
<!-- Start Formoid form-->
<link rel="stylesheet" href="<?php echo dirname($form_path); ?>/formoid-default-skyblue.css" type="text/css" />
<script type="text/javascript" src="<?php echo dirname($form_path); ?>/jquery.min.js"></script>
<form enctype="multipart/form-data" class="formoid-default-skyblue" style="background-color:#FFFFFF;font-size:13px;font-family:'Open Sans','Helvetica Neue','Helvetica',Arial,Verdana,sans-serif;color:#666666;max-width:480px;min-width:150px" method="post"><div class="title"><h2>必須項目はこの2つだけ</h2></div>
	<div class="element-name<?php frmd_add_class("name"); ?>"><label class="title">名前<span class="required">*</span></label><span class="nameFirst"><input  type="text" size="8" name="name[first]" required="required"/><label class="subtitle">苗字</label></span><span class="nameLast"><input  type="text" size="14" name="name[last]" required="required"/><label class="subtitle">名前</label></span></div>
	<div class="element-email<?php frmd_add_class("email"); ?>" title="メールアドレス"><label class="title">mail<span class="required">*</span></label><input class="large" type="email" name="email" value="" required="required"/></div>
	<div class="element-separator"><hr><h3 class="section-break-title">Section Break</h3></div>
	<div class="element-input<?php frmd_add_class("input"); ?>"><label class="title">今住んでるエリアは？</label><input class="large" type="text" name="input" /></div>
	<div class="element-multiple<?php frmd_add_class("multiple"); ?>"><label class="title">年齢はいくつですか？</label><div class="large"><select data-no-selected="Nothing selected" name="multiple[]" multiple="multiple" >

		<option value="18歳">18歳</option>
		<option value="19歳">19歳</option>
		<option value="20歳">20歳</option>
		<option value="21歳">21歳</option>
		<option value="22歳">22歳</option>
		<option value="23歳">23歳</option>
		<option value="24歳">24歳</option>
		<option value="25歳">25歳</option>
		<option value="26歳">26歳</option>
		<option value="27歳">27歳</option>
		<option value="28歳">28歳</option>
		<option value="29歳">29歳</option>
		<option value="30歳">30歳</option>
		<option value="31歳">31歳</option>
		<option value="32歳">32歳</option>
		<option value="33歳">33歳</option>
		<option value="34歳">34歳</option>
		<option value="35歳">35歳</option>
		<option value="36歳">36歳</option>
		<option value="37歳">37歳</option>
		<option value="38歳">38歳</option>
		<option value="39歳">39歳</option>
		<option value="40歳">40歳</option>
		<option value="41歳">41歳</option>
		<option value="42歳">42歳</option>
		<option value="43歳">43歳</option>
		<option value="44歳">44歳</option>
		<option value="45歳以上">45歳以上</option></select></div></div>
	<div class="element-radio<?php frmd_add_class("radio"); ?>"><label class="title">業界の経験はありますか？</label>		<div class="column column2"><label><input type="radio" name="radio" value="経験無し" /><span>経験無し</span></label></div><span class="clearfix"></span>
		<div class="column column2"><label><input type="radio" name="radio" value="経験あり" /><span>経験あり</span></label></div><span class="clearfix"></span>
</div>
	<div class="element-checkbox<?php frmd_add_class("checkbox"); ?>"><label class="title">お店選びで重要視してる所は？</label>		<div class="column column2"><label><input type="checkbox" name="checkbox[]" value="業種"/ ><span>業種</span></label><label><input type="checkbox" name="checkbox[]" value="お給料単価"/ ><span>お給料単価</span></label><label><input type="checkbox" name="checkbox[]" value="託児所"/ ><span>託児所</span></label><label><input type="checkbox" name="checkbox[]" value="寮有り"/ ><span>寮有り</span></label><label><input type="checkbox" name="checkbox[]" value="罰金雑費なし"/ ><span>罰金雑費なし</span></label><label><input type="checkbox" name="checkbox[]" value="車通勤"/ ><span>車通勤</span></label></div><span class="clearfix"></span>
		<div class="column column2"><label><input type="checkbox" name="checkbox[]" value="エリア"/ ><span>エリア</span></label><label><input type="checkbox" name="checkbox[]" value="保証制度"/ ><span>保証制度</span></label><label><input type="checkbox" name="checkbox[]" value="客層"/ ><span>客層</span></label><label><input type="checkbox" name="checkbox[]" value="個室待機"/ ><span>個室待機</span></label><label><input type="checkbox" name="checkbox[]" value="送迎希望"/ ><span>送迎希望</span></label><label><input type="checkbox" name="checkbox[]" value="前借り"/ ><span>前借り</span></label></div><span class="clearfix"></span>
</div>
	<div class="element-file<?php frmd_add_class("file"); ?>"><label class="title">あなたの写メを送ってみませんか？</label><label class="large" ><div class="button">画像を探す</div><input type="file" class="file_input" name="file" /><div class="file_text">No file selected</div></label></div>
	<div class="element-radio<?php frmd_add_class("radio1"); ?>"><label class="title">お問い合わせ内容</label>		<div class="column column2"><label><input type="radio" name="radio1" value="面接希望" /><span>面接希望</span></label></div><span class="clearfix"></span>
		<div class="column column2"><label><input type="radio" name="radio1" value="質問したい" /><span>質問したい</span></label></div><span class="clearfix"></span>
</div>
	<div class="element-multiple<?php frmd_add_class("multiple1"); ?>"><label class="title">面接希望日</label><div class="large"><select data-no-selected="Nothing selected" name="multiple1[]" multiple="multiple" >

		<option value="今日">今日</option>
		<option value="明日">明日</option>
		<option value="明後日">明後日</option>
		<option value="3日後以降">3日後以降</option></select></div></div>
	<div class="element-textarea<?php frmd_add_class("textarea"); ?>"><label class="title">質問やご要望など</label><textarea class="medium" name="textarea" cols="20" rows="5" ></textarea></div>
<div class="submit"><input type="submit" value="メールする"/></div></form><script type="text/javascript" src="<?php echo dirname($form_path); ?>/formoid-default-skyblue.js"></script>

<!-- Stop Formoid form-->
<?php endif; ?>

<?php frmd_end_form(); ?>