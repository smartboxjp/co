<?php
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_model/CommonCateMatome.php";
	$common_catematome = new CommonCateMatome();
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_model/CommonMatome.php";
	$common_matome = new CommonMatome();
	

	foreach ( $_GET as $key => $value ) {
		$$key = $common_connect->h($value);
	}

	//カテゴリ
	$arr_matome = $common_catematome->Fn_cate_matome_list();
	if ( !is_null( $arr_matome ) ) {
		foreach ( $arr_matome as $arr_key => $arr_value ) {
			$arr_cate_matome_title[ $arr_value[ "cate_matome_id" ] ] = $arr_value[ "cate_matome_title" ];
			$arr_cate_matome_color[ $arr_value[ "cate_matome_id" ] ] = $arr_value[ "cate_matome_color" ];
		}
	}

	if ( $matome_id != "" ) {
		$arr_where = array();
		$arr_where[ "matome_id" ] = $matome_id;
		$arr_where[ "flag_open" ] = 1;

		$arr_data = array( "matome_id", "matome_title", "cate_matome_id", "img_1", "matome_comment", "flag_open" );
		$arr_data = array_merge( $arr_data, array( "regi_date", "up_date" ) );

		$db_result_matome = $common_matome->Fn_matome_list( $arr_data, $arr_where );
		foreach ( $db_result_matome as $arr_matome ) {
			foreach ( $arr_matome as $key => $value ) {
				$$key = $value;
			}
		}
	}

	$meta_title = $matome_title;
	$meta_description = "description です";
	$meta_keywords = "キーワードです";


	$pv_good = 0;
	$sql = "SELECT pv_good from matome where matome_id='".$matome_id."' ";;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
		$pv_good = $db_result[0]["pv_good"];
	}
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta_share.php";?>

<!--ページ専用のCSS-->
<link href="/kanto/beautymatome/css/matome.css" rel="stylesheet" type="text/css"/>

<!--アイコン用CSS-->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

</head>





<body>

	<div id="container">

		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>

		<div id="contentsin">
			<!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>


			<div id="mainboxmatome">
				<!--コンテンツの外枠-->

				<ul id="pan">
	    			<li><a href="/kanto/">トップページ</a></li>
	    			<li><a href="/kanto/beautymatome/">まとめ記事</a></li>
	    			<? if($cate_matome_id!="") { ?>
	    			<li><a href="/kanto/beautymatome/?cate_matome_id=<? echo $cate_matome_id;?>"><? echo $arr_cate_matome_title[$cate_matome_id];?></a></li>
	    			<? } ?>
	    			<li><? echo $matome_title;?></li>
				</ul>


				<!--左記事種類-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/beautymatome/common/matomemenu.php"); ?>
				<!--左記事種類-->

				<div class="matomedetails">

					<div class="imgblok">
						<?
							$var = "img_1";
							if($$var!="") {
								echo "<img src='/".global_matome_dir.$matome_id."/".$$var."' height='100px'>";
							}
						?>
					</div>

					<div class="titleblok">
						<p class="kizitime"><? echo substr($regi_date, 0, 10);?>投稿<span class="<? echo $arr_cate_matome_color[$cate_matome_id];?>"><? echo $arr_cate_matome_title[$cate_matome_id];?></span></p>
						<? echo $matome_title;?>
					</div>


					<ul class="matomedeta">
						<li>
							<div class="iinebotan">
								<a href="vote.php?matome_id=<? echo $matome_id;?>" title="参考になった"><span><i class="fa fa-heart fa-1.5x"></i></span>参考(<? echo $pv_good;?>)</a>
							</div>

						</li>

						<li>
							<div class="fb-share-button" data-href="http://www.cossot.com/kanto/beautymatome/details.php" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.cossot.com%2Fkanto%2Fbeautymatome%2Fdetails.php&amp;src=sdkpreparse">シェア</a></div>
							<div id="fb-root"></div>
								<script>(function(d, s, id) {
  								var js, fjs = d.getElementsByTagName(s)[0];
  								if (d.getElementById(id)) return;
  								js = d.createElement(s); js.id = id;
  								js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.8";
  							fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));</script>
						</li>

						<li>
							<a href="https://twitter.com/share" class="twitter-share-button" data-lang="ja">ツイート</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
						</li>

						<li>
							<div class="line-it-button" style="display: none;" data-type="share-a" data-lang="ja" ></div>
							<script src="//scdn.line-apps.com/n/line_it/thirdparty/loader.min.js" async="async" defer="defer" ></script>
						</li>


					</ul>

					<div class="titleovicenter">
						<!--フロート解除-->
					</div>
					
					
					<!--記事内容-->
					<div class="matomemaincontent">
						<? echo $matome_comment;?>
					</div>
					<!--記事内容-->
					
					<ul class="matomedeta">
						<li>
							<div class="iinebotan">
								<a href="vote.php?matome_id=<? echo $matome_id;?>" title="参考になった"><span><i class="fa fa-heart fa-1.5x"></i></span>参考(<? echo $pv_good;?>)</a>
							</div>

						</li>

						<li>
							<div class="fb-share-button" data-href="http://www.cossot.com/kanto/beautymatome/details.php" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.cossot.com%2Fkanto%2Fbeautymatome%2Fdetails.php&amp;src=sdkpreparse">シェア</a></div>
							<div id="fb-root"></div>
								<script>(function(d, s, id) {
  								var js, fjs = d.getElementsByTagName(s)[0];
  								if (d.getElementById(id)) return;
  								js = d.createElement(s); js.id = id;
  								js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.8";
  							fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));</script>
						</li>

						<li>
							<a href="https://twitter.com/share" class="twitter-share-button" data-lang="ja">ツイート</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
						</li>

						<li>
							<div class="line-it-button" style="display: none;" data-type="share-a" data-lang="ja" ></div>
							<script src="//scdn.line-apps.com/n/line_it/thirdparty/loader.min.js" async="async" defer="defer" ></script>
						</li>


					</ul>

					
					<!---おすすめ記事-->
					<div class="picupkizibox"><!--記事外枠-->
							
					<hr class="sikiri">
						
							<div class="picupkizibigtaitl"><!--記事外枠-->
								<i class="fa fa-book"></i>よく読まれてる記事
							</div>
							<?
							$arr_db_field = array( "matome_id", "matome_title", "cate_matome_id", "img_1", "matome_comment");
							$arr_db_field = array_merge( $arr_db_field, array( "regi_date", "up_date" ) );
							$sql = "SELECT ";
							foreach($arr_db_field as $val)
							{
								$sql .= $val.", ";
							}
							$sql .= " 1 FROM matome where flag_open=1 order by rand() limit 0, 3 ";

						    $db_result = $common_dao->db_query_bind($sql);
						    if($db_result)
						    {
						        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
						        {
									foreach($arr_db_field as $val)
									{
										$$val = $db_result[$db_loop][$val];
									}

							?>

							<div class="cntercontent"><!--1記事-->

								<div class="picupkizileft">
									<?
										$var = "img_1";
										if($$var!="") {
									?>
										<a href="details.php?matome_id=<? echo $matome_id;?>">
									<?
											echo "<img src='/".global_matome_dir.$matome_id."/".$$var."' height='60px'>";
									?>
										</a>
									<?
										}
									?>
								</div>

								<div class="picupkiziright">
									<a href="details.php?matome_id=<? echo $matome_id;?>""><? echo $common_connect->Fn_shot_string($matome_title, 26, "…");?></a>
									<div class="kizibun"><? echo $common_connect->Fn_shot_string(trim(strip_tags($matome_comment)), 32, "…");?></div>
									<div class="kiziizmtime"><? echo $arr_cate_matome_title[$cate_matome_id];?>|<? echo substr($regi_date, 5, 2);?>月<? echo substr($regi_date, 8, 2);?>日</div>
								</div>

								<div class="titleovicenter"><!--フロート解除-->
								</div>

							</div><!--1記事ここまで-->
							<?
								}//for
							}//if
							?>

					</div>
					<!---おすすめ記事-->
					
					
					

				</div>
				<!--matomedetails-->

				<div class="titleovicenter">
					<!--フロート解除-->
				</div>
				
				


		
			<div class="push"></div>
			</div>
			<!--main boxコンテンツの外枠-->



			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>
		</div>
		<!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
		<!--contentsin-->

		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
	</div>
	<!--container-->



</body> 
</html>