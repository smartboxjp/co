<?php
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_model/CommonCateMatome.php";
	$common_catematome = new CommonCateMatome();
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_model/CommonMatome.php";
	$common_matome = new CommonMatome();
	

	foreach ( $_GET as $key => $value ) {
		$$key = $common_connect->h($value);
	}

	//カテゴリをArrayへ
	$sql = "SELECT cate_matome_id, cate_matome_title FROM cate_matome where flag_open=1 order by view_level";
	$db_result = $common_dao->db_query_bind($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_cate_matome_title[$db_result[$db_loop]["cate_matome_id"]] = $db_result[$db_loop]["cate_matome_title"];
			//$arr_color[$db_result[$db_loop][cate_news_id]] = $db_result[$db_loop][color];
		}
	}

	$meta_title = "まとめ記事一覧";
	$meta_description = "女の子の為のちょっと役立つ情報をお届け";
	$meta_keywords = "まとめ,キュレーション,綺麗,お役立ち,";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

<!--ページ専用のCSS-->
<link href="/kanto/beautymatome/css/matome.css" rel="stylesheet" type="text/css" />

<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

</head>




<body>

<div id="container">

<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>

	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>
		

		<div id="mainbox"><!--コンテンツの外枠-->


		<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /start /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
			<ul id="pan">
    			<li><a href="/kanto/">トップページ</a></li>
    			<? if($cate_matome_id!="") { ?>
    			<li><a href="/kanto/beautymatome/">まとめ記事</a></li>
    			<li><? echo $arr_cate_matome_title[$cate_matome_id];?></li>
    			<? } else { ?>
    			<li>まとめ記事</li>
    			<? } ?>
  			</ul>
  			

			<div id="titleovi"><!--タイトル帯、幅1000-->

				<div class="titleovileft">
					<img src="/kanto/beautymatome/img/matometop.jpg" width="150" height="100" alt="" />
				</div>

				<div class="titleoviright">
					あなたがもっと輝く情報を。綺麗になるまとめ
					<div class="titleovirightsab">
						もっと可愛く、もっとお洒落に。仕事でプライベートで、愛される女の子になる為に知っておかなければならない事。<br>
						ファッション、ビューティー、グルメ、ダイエット、旅行、恋愛、仕事、7つの気になる情報をまとめて随時更新中。
					</div>
				</div>

				<div class="titleovicenter">

				</div>

			</div><!--タイトル帯、幅1000-->



			<div id="matomemabody"><!--コンテンツ左680幅-->

				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/beautymatome/common/matomemenu.php"); ?>

				<div class="kizibox"><!--記事外枠-->

					<div class="kizibigtaitl"><!--記事外枠-->
					最新の投稿記事【全て】
					</div>

<?
						$view_count = 10; // List count
						$offset = 0;

						if ( !$page ) {
							$page = 1;
						}
						Else {
							$offset = $view_count * ( $page - 1 );
						}

						$arr_where = array();
						$arr_where["flag_open"] = 1;
						$var = "cate_matome_id";
						if ( $$var != "" ) {
							$arr_where[ $var ] = $$var;
						}

						//合計
						$all_count = 0;
						$arr_matome_all = $common_matome->Fn_matome_all_count( $arr_where );
						$all_count = $arr_matome_all[ 0 ][ "all_count" ];



						//リスト表示
						$arr_etc[ "order_name" ] = $order_name;
						$arr_etc[ "order" ] = $order;
						$arr_etc[ "offset" ] = $offset;
						$arr_etc[ "view_count" ] = $view_count;

						$arr_data = array( "matome_id", "matome_title", "cate_matome_id", "img_1", "matome_comment", "flag_open" );
						$arr_data = array_merge( $arr_data, array( "regi_date", "up_date" ) );

						$arr_matome_list = $common_matome->Fn_matome_all_list( $arr_data, $arr_where, $arr_etc );

						foreach ( $arr_matome_list as $arr_key => $arr_value ) {
							foreach ( $arr_value as $key => $value ) {
								$$key = $value;
							}
						?>
					<div class="cntercontent"><!--1記事-->

						<div class="cntercontentkizileft">
							
							<?
								$var = "img_1";
								if($$var!="") {
							?>
								<a href="details.php?matome_id=<? echo $matome_id;?>">
							<?
									echo "<img src='/".global_matome_dir.$matome_id."/".$$var."'>";
							?>
								</a>
							<?
								}
							?>
							
						</div>

						<div class="cntercontentkiziright">
								<div class="kizititle">
									<a href="details.php?matome_id=<? echo $matome_id;?>"><? echo $matome_title;?>
									</a>
								</div>
							<div class="kizibun">
							<? echo $common_connect->Fn_shot_string(trim(strip_tags($matome_comment)), 30, "…");?>
							</div>
							<div class="kiziizmtime"><? echo $arr_cate_matome_title[$cate_matome_id];?>|<? echo substr($regi_date, 5, 2);?>月<? echo substr($regi_date, 8, 2);?>日</div>
						</div>

						<div class="titleovicenter"><!--フロート解除-->
						</div>

					</div><!--1記事ここまで-->
					<?
						}
					?>


					<div class="kzinextbox">
						<div id="nextcaunt">
							<?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
						</div>
						<div class="frotcria"></div>
					</div><!--1記事ここまで-->



				</div><!--kizibox-->


			</div>
			
			

			<div id="sub"><!--sub------------------------------------------------------------------------->


    			<!--検索-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
				<!--検索-->

			<!--PR店舗、ページ表示の度に有料店舗から3店舗ランダムで表示。-->
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/light/prshop.php"); ?>
			<!--PR店舗、ページ表示の度に有料店舗から3店舗ランダムで表示。-->

				<!--検索リスト-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
				<!--検索リスト-->

				<!--バナー-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner2.php"); ?>
				<!--バナー-->



			</div>
			<!--/sub------------------------------------------------------->

		</div>
<!--main boxコンテンツの外枠-->

		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>
		</div><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
<!--contentsin-->

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
	</div>
<!--container-->

	<!--全ページ共通スクリプト-->
	<script type="text/javascript" src="/kanto/app/textcount/textcount.js"></script>
	<script type="text/javascript" src="/kanto/app/resize/imgLiquid-min.js"></script>
	<script type="text/javascript">
$(document).ready(function() {
    $(".cntercontentkizileft").imgLiquid({
    });
});
</script>



</body>
</html>

