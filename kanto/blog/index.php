<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "ブログ一覧";
	$meta_description = "お店から投稿されたブログ一覧です。";
	$meta_keywords = "ブログ,blog,情報";
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

<!--ページ専用のCSS-->
<link href="/kanto/ranking/css/rank.css" rel="stylesheet" type="text/css" />
<!--ブログ一覧用css-->
<link href="/kanto/blog/css/jobblog.css" rel="stylesheet" type="text/css" />

<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

</head>


<body>

<div id="container">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>

	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>
		

		<div id="mainbox"><!--コンテンツの外枠-->

     			<div id="main" class="pt6"><!--コンテンツ左680幅-->

				<ul id="pan">
        				<li><a href="index.html">トップページ</a></li>
        				<li><a href="index.html">*****</a></li>
        				<li><a href="index.html">*****</a></li>
        				<li>*****</li>
      				</ul>


  
			

				<div class="jobbulogboxtop">

					<div class="ranktitle">
						<img src="/kanto/ranking/img/ranktitle3.png" width="680" height="27" alt="" />
					</div>

				<!--ここはブログのランキング上位3位までがはいるので、ランキング連動-->
<?php
    $where = " ";
    $where .= " and b.shop_id in (select shop_id from shop where flag_open=1 and shop_id=b.shop_id ) ";
    $where .= " and r.regi_date=(SELECT max(regi_date) FROM shop_blog_ranking where regi_date<='".date("Y/m/d")."') " ;

	$arr_db_field = array("shop_blog_title", "ranking", "img_1");

	$sql = "SELECT ";
	$sql .= " b.shop_id, b.shop_blog_id, b.regi_date, ";
	foreach($arr_db_field as $val)
	{
	    $sql .= $val.", ";
	}
	$sql .= " 1 FROM shop_blog b inner join shop_blog_ranking r on b.shop_blog_id=r.shop_blog_id where 1 ".$where ;
	$sql .= " order by ranking limit 0, 3";

	$arr_list = $common_dao->db_query_bind($sql);
    if($arr_list)
    {
        for($db_loop=0 ; $db_loop < count($arr_list) ; $db_loop++)
        {
          foreach($arr_db_field as $val)
          {
            $$val = $arr_list[$db_loop][$val];
          }
          $shop_blog_id = $arr_list[$db_loop]["shop_blog_id"];
          $shop_id = $arr_list[$db_loop]["shop_id"];
          $regi_date = $arr_list[$db_loop]["regi_date"];
?>

					<div class="boxtop<? if($ranking<=3) { echo "3";} else { echo "10";}?>">
						<div class="rankblogmark"><img src="/kanto/ranking/img/rank<? echo $ranking;?>.png"/></div>
						<div class="blogtimetoprank"><? echo date("Y/m/d", strtotime($regi_date));?>投稿</div>
						<p class="img-blogblock">
							<a href="/kanto/shop/blog_details.php?shop_blog_id=<? echo $shop_blog_id;?>">
							<? if($img_1!="") { ?>
							<img src="<? echo "/".global_shop_blog_dir.$shop_blog_id."/".$img_1;?>" width=90>
							<? } else {?>
							<img src="/img/noimg/shop-blog.png">
							<? } ?>
							</a>
						</p>
						<div class="blogtitletoprank">
							<a href="/kanto/shop/blog_details.php?shop_blog_id=<? echo $shop_blog_id;?>"><? echo $shop_blog_title;?></a>
						</div>
					</div>
<?
		}
	}
?>


				</div>



<?php
$view_count=12;   // List count
$offset=0;


if(!$page)
{
    $page=1;
}
Else
{
    $offset=$view_count*($page-1);
}
$where = "";
if($s_keyword != "")
{
    $where .= " and (shop_blog_title like '%".$s_keyword."%' or shop_blog_id='".$s_keyword."') ";
}
$where .= " and s.flag_open='1' and b.flag_open='1' ";

//合計
$sql_count = "SELECT count(shop_blog_id) as all_count FROM shop_blog b inner join shop s on s.shop_id=b.shop_id where 1 ".$where ;

$db_result_count = $common_dao->db_query_bind($sql_count);
if($db_result_count)
{
    $all_count = $db_result_count[0]["all_count"];
}

?>

				<div class="toukoublog">
					これより下は投稿順に掲載されます。
				</div>
				<!--ここから通常投稿　投稿順に並べる-->

				<div class="blogsearchkleft">
					投稿ブログ<span class="blogkazu"><? echo $all_count;?>件</span>
				</div>


				<!--検索バー-->
				<div class="blogsearchright">
					<form action="./" method="get" name="form_shop_search">
					<? $var = "s_keyword";?>
					<input name="<? echo $var;?>" type="search" placeholder="ブログを検索" value="<? echo $$var;?>">
					<input type="image" value="検索" src="/kanto/common/light/img/search01.png">
					</form>
				</div>
				<!--検索バー-->


				<div class="cria"></div>






				<div class="jobbulogbox"><!--2列目-->
<?

//リスト表示
$arr_db_field = array("shop_blog_id", "shop_blog_title", "count_comment");

$sql = "SELECT ";
$sql .= " s.shop_name, s.shop_id, b.view_level, b.flag_open, b.regi_date, b.up_date, b.img_1 as blog_img, ";
foreach($arr_db_field as $val)
{
    $sql .= $val.", ";
}
$sql .= " 1 FROM shop_blog b inner join shop s on s.shop_id=b.shop_id where 1 ".$where ;
if($order_name != "")
{
    $sql .= " order by ".$order_name." ".$order;
}
else
{
    $sql .= " order by b.view_level, b.regi_date desc";
}
$sql .= " limit $offset,$view_count";

$db_result = $common_dao->db_query_bind($sql);
	if($db_result)
	{
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
          foreach($arr_db_field as $val)
          {
            $$val = $db_result[$db_loop][$val];
          }
          $blog_img = $db_result[$db_loop]["blog_img"];

          
?>
					<div class="box5">
						<div class="blogtimetoprank"><? echo substr($regi_date, 0, 10);?>投稿</div>
						<p class="img-blogblock">
							<a href="/kanto/shop/blog_details.php?shop_blog_id=<? echo $shop_blog_id;?>">
								<? if($blog_img!="") { ?>
								<img src="<? echo "/".global_shop_blog_dir.$shop_blog_id."/".$blog_img;?>">
								<? } else {?>
								<img src="/img/noimg/shop-blog.png">
								<? } ?>
							</a>
						</p>
						<div class="blogtitletoprank">
						<a href="/kanto/shop/blog_details.php?shop_blog_id=<? echo $shop_blog_id;?>"><? echo $shop_blog_title;?></a>
						</div>
					</div>
<?
		}
	}
?>
				</div>



				<!--アフィリ広告スペース-->
				<div class="afiblogjob">
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/app/pr/contents_bottom.php"); ?>
				</div>
				<!--アフィリ広告スペース-->



				<div class="anderjob">

					<div id="nextcaunt">
						<?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
					</div>
					<div class="boxcria"></div>


				</div>


				<p class="bloginfo">
				※各店舗が配信する宣伝ブログのランキングはアクセス、口コミ、評価を含め当社独自の基準で集計、発表を</br>
				しております。またブログの内容に、嘘や大げさな内容が含まれている場合がある時は<a href="#">管理者まで</a>ご連絡を下さい。
				</p>

			</div>
      
		<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /end /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
  
  
  
  
  
			<div id="sub"><!--sub------------------------------------------------------------------------->
        			<!--検索-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
        			<!--検索-->
        
        			<!--検索リスト-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
        			<!--検索リスト-->
      			</div>
		</div>
  

		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>

	</div><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
<!--contentsin-->

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
</div>
<!--container-->

	<!--全ページ共通スクリプト-->
	<script type="text/javascript" src="/kanto/app/textcount/textcount.js"></script>
	<script type="text/javascript" src="/kanto/app/resize/imgLiquid-min.js"></script>
	<script type="text/javascript">
$(document).ready(function() {
    $(".img-blogblock").imgLiquid({
    });
});
</script>

</body>
</html>

