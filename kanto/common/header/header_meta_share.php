<?
	if($meta_title == "")
	{
		$meta_title = "口コミ高収入サイトコソット";
	}
	if($meta_description == "")
	{
		$meta_description = "働いた女の子の口コミで間違いないお店選びを。口コミ情報で探せる高収入求人情報サイトです。働いてよかったお店、駄目だったお店の情報満載。";
	}
	if($meta_keywords == "")
	{
		$meta_keywords = "口コミ,高収入,風俗,東京,神奈川,千葉,埼玉,栃木,群馬,茨城";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="Content-Style-Type" content="text/css"/>
	<meta http-equiv="Content-Script-Type" content="text/javascript"/>
	<title>
		<? echo $meta_title;?>
	</title>
	<meta name="copyright" content="avalanche"/>
	<meta name="description" content="<? echo $meta_description;?>"/>
	<meta name="keywords" content="<? echo $meta_keywords;?>"/>
	<!--シェア用メタタグ-->

	<meta name="description" content="記事タイトルが表示されます。">
	<meta property="og:title" content="記事タイトル/【cossot.com】綺麗になるまとめ"/>
	<meta property="og:description" content="記事タイトルが表示されます。"/>
	<meta property="og:image" content="記事サムネイル画像の絶対パス"/>
	<meta property="og:url" content="ページURL"/>
	<meta property="og:type" content="article"/>
	<meta name="twitter:card" content="summary">
	<!--共通部分のCSS-->
	<link href="/kanto/common/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="/kanto/common/light/css/light.css" rel="stylesheet" type="text/css"/>
	<link href="/kanto/common/footer/css/footer.css" rel="stylesheet" type="text/css"/>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>