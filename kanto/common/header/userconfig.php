
			<ul class="usertitle">
				<li><a href="/kanto/user/admin/useradmincrender.php"><i class="fa fa-calendar" aria-hidden="true"></i>カレンダー</a> |</li>
				<li><a href=""><i class="fa fa-desktop" aria-hidden="true"></i>自分のページを見る</a> |</li>
				<li><i class="fa fa-cog" aria-hidden="true"></i>設定の変更をする
					<ul class="child">
            					<li><a href="/kanto/user/admin/useradmin_prfimg.php"><i class="fa fa-camera" aria-hidden="true" fa-fw></i>画像の設定</a></li>
            					<li><a href="/kanto/user/admin/useradminconfig_plof.php"><i class="fa fa-user" aria-hidden="true" fa-fw></i>プロフィール設定</a></li>
						<li><a href="/kanto/user/admin/useradminconfig_mail.php"><i class="fa fa-envelope-o" aria-hidden="true" fa-fw></i>メールアドレス設定</a></li>
						<li><a href="/kanto/user/admin/useradminconfig_pass.php"><i class="fa fa-key" aria-hidden="true" fa-fw></i>パスワード設定</a></li>
        				</ul>
				|</li>
				<li><a class="modal-syncer button-link" data-target="modal-content-01"><i class="fa fa-search"></i>お仕事を検索する</a> |</li>
			</ul>
