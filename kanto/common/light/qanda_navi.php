<!--お問い合わせについて-->
		<div class="kozinhed">
			※個人情報の取り扱いつにいて※
		</div>

		<div class="kozininfo">
			当社に送って頂いたメールアドレス、名前などの個人情報が個人情報保護法に基づき
			厳正な管理状態の元保管に勤めて参ります。
			個人情報を徹底的に保護するための管理体制の構築、サイト内で個人を特定される
			場合の書き込みや画像の即刻削除、頂いた個人情報への適切な利用、運営スタッフ全ての
			個人情報取り扱いに対する教育、以上を徹底し皆様の安全で適切なサイト運営に努めてまいりたいと思います。

		</div>



		<div id="faq">
			<div class="question"><i class="fa fa-angle-down"></i> ログインについてよくある質問</div>
			<div class="answer">

				<ul class="qalist">
					<li>
						<a href="/kanto/help/qanda_login/q1.php">ログインパスワードの再発行</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_login/q1.php">質問その1</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_login/q1.php">質問その1</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_login/q1.php">質問その1</a>
					</li>
				</ul>
			</div>

			<div class="question"><i class="fa fa-angle-down"></i> 口コミについてよくある質問</div>
			<div class="answer">

				<ul class="qalist">
					<li>
						<a href="/kanto/help/qanda_voice/q1.php">ログインパスワードの再発行</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_voice/q1.php">質問その1</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_voice/q1.php">質問その1</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_voice/q1.php">質問その1</a>
					</li>
				</ul>
			</div>

			<div class="question"><i class="fa fa-angle-down"></i> 画像についてのよくある質問</div>
			<div class="answer">

				<ul class="qalist">
					<li>
						<a href="/kanto/help/qanda_img/q1.php">ログインパスワードの再発行</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_img/q1.php">質問その1</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_img/q1.php">質問その1</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_img/q1.php">質問その1</a>
					</li>
				</ul>
			</div>

			<div class="question"><i class="fa fa-angle-down"></i> 信憑性についてのよくある質問</div>
			<div class="answer">

				<ul class="qalist">
					<li>
						<a href="/kanto/help/qanda_evaluation/q1.php">ログインパスワードの再発行</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_evaluation/q1.php">質問その1</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_evaluation/q1.php">質問その1</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_evaluation/q1.php">質問その1</a>
					</li>
				</ul>
			</div>

			<div class="question"><i class="fa fa-angle-down"></i> 面接申し込みについてのよくある質問</div>
			<div class="answer">

				<ul class="qalist">
					<li>
						<a href="/kanto/help/qanda_interview/q1.php">ログインパスワードの再発行</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_interview/q1.php">質問その1</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_interview/q1.php">質問その1</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_interview/q1.php">質問その1</a>
					</li>
				</ul>
			</div>

			<div class="question"><i class="fa fa-angle-down"></i> マイページについてのよくある質問</div>
			<div class="answer">

				<ul class="qalist">
					<li>
						<a href="/kanto/help/qanda_mypage/q1.php">ログインパスワードの再発行</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_mypage/q1.php">質問その1</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_mypage/q1.php">質問その1</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_mypage/q1.php">質問その1</a>
					</li>
				</ul>
			</div>

			<div class="question"><i class="fa fa-angle-down"></i> ランキングについてのよくある質問</div>
			<div class="answer">

				<ul class="qalist">
					<li>
						<a href="/kanto/help/qanda_ranking/q1.php">ログインパスワードの再発行</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_ranking/q1.php">質問その1</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_ranking/q1.php">質問その1</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_ranking/q1.php">質問その1</a>
					</li>
				</ul>
			</div>

			<div class="question"><i class="fa fa-angle-down"></i> その他のよくある質問</div>
			<div class="answer">

				<ul class="qalist">
					<li>
						<a href="/kanto/help/qanda_other/q1.php">ログインパスワードの再発行</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_other/q1.php">質問その1</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_other/q1.php">質問その1</a>
					</li>
					<li>
						<a href="/kanto/help/qanda_other/q1.php">質問その1</a>
					</li>
				</ul>
			</div>

		</div>


