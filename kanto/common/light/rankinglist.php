
<div class="ranktext">

	<div class="ranktexthed">
	ランキングメニュー
	</div>

	<ul class="ranktextul">
		<li><a href="company.html">平均評価の高いお店</a></li>
		<li><a href="link.html">アクセス数の多いお店</a></li>
		<li><a href="#">よく読まれたブログ記事</a></li>
	</ul>

</div>



<div class="rankinfohed">
	※ランキングについて※
</div>

<div class="rankinfo">
毎週、独自の集計を元にランキングを更新しています。お店選びの基準としてお役立て下さい。
但しランキングはあくまでお店選びの基準の一つとなるだけで、必ずしも貴方に合うお店とは限りません。
クチコミは勿論の事、お店への電話やメール、面接時での対応、待遇などをしっかり確認して理想のお店を探してください
</div>

