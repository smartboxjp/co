<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateJob.php";
	$common_cate_area = new CommonCateArea();
	$common_cate_job = new CommonCateJob();
?>
				<div class="arealist">

					<h3 class="text">良く検索されてる地域</h3>

					<ul class="textarealist">
<?php
	//area 大
	$result_cate_area_l = $common_cate_area -> Fn_cate_area_usual_list () ;
	
	foreach($result_cate_area_l as $arr_cate_area_l)
	{
		$cate_area_s_id = $arr_cate_area_l["cate_area_s_id"];
		$cate_area_s_title = $arr_cate_area_l["cate_area_s_title"];
		$cate_area_l_title = $arr_cate_area_l["cate_area_l_title"];
?>
						<li><img src="/kanto/common/img/iconsankaku.png" width="15" height="15" /><a href="/kanto/search/search_result.php?as[]=<? echo $cate_area_s_id;?>"><? echo $cate_area_l_title;?> / <? echo $cate_area_s_title;?></a></li>

<?
	}
?>
					</ul>

					<h3 class="text">業種から探す</h3>

					<ul class="textarealist">
<?php
	//職種
	$arr_where = array();
	$arr_where["flag_open"] = "1";
	
	$arr_data = array();
	$arr_data[] = "cate_job_id";
	$arr_data[] = "cate_job_title";
			
	$result_cate_job = $common_cate_job -> Fn_db_cate_job_list ($arr_data, $arr_where) ;
	
	foreach($result_cate_job as $arr_cate_job)
	{
		$cate_job_id = $arr_cate_job["cate_job_id"];
		$cate_job_title = $arr_cate_job["cate_job_title"];
?>
						<li><img src="/kanto/common/img/iconsankaku.png" width="15" height="15" /><a href="/kanto/search/search_result.php?j[]=<?php echo $cate_job_id; ?>"><?php echo $cate_job_title; ?></a></li>

<?
	}
?>
					</ul>

				</div>