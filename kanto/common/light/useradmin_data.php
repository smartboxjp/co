
	<div class="pdata">
		<div class="darea_1 rightbox-top">

			<dl class="clearfix">
				<?
							$result_ranking_count = $common_voice -> Fn_ranking_count ($member_id);
							$sum_ranking_count = 0;
							if(!is_null($result_ranking_count)) {
								foreach($result_ranking_count as $arr_key=>$arr_value)
								{
									$arr_ranking_count[$arr_value["ranking"]] = $arr_value["voice_count"];
									$sum_ranking_count += $arr_value["voice_count"];
								}
							}
							?>
							
				<div class="datehed">
					<h2>投稿口コミ数<span><? echo $sum_ranking_count;?>件</span></h2>
				</div>
							
				<div class="charts">
					 <canvas id="doughnut-chart" width="165px" height="165px"></canvas>
				</div>

			</dl>

		</div>


		<div class="darea_2 rightbox-bottom">
		
			<h2>ユーザーのデータ</h2>
			<dl class="clearfix">
				<dt>参考になった</dt>
				<dd>17回</dd>
				<dt>投稿画像枚数</dt>
				<dd>8枚</dd>
				<dt>平均評価点</dt>
				<dd>3.71点</dd>
				<dt>お気に入り数</dt>
				<dd>5件</dd>
				<dt>問い合わせ店舗</dt>
				<dd>8件</dd>
				<dt>受信メール</dt>
				<dd>7通</dd>
			</dl>
		</div>
		
		
	</div>


