<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "コソットへお問い合わせ";
	$meta_description = "description です";
	$meta_keywords = "キーワードです";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>


<!--ページ専用のCSS-->
	<link href="/kanto/help/css/lnquiry.css" rel="stylesheet" type="text/css" />
<!--アイコンで使用CSS-->
  	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--フォームで使用-->
	<link rel="stylesheet" href="/kanto/app/form/formoid1/formoid-default-skyblue.css" type="text/css" />
  
<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input("inquiry_sel");
			err_check_count += check_input("inquiry_name");
			err_check_count += check_input_email("inquiry_email");
			err_check_count += check_input("inquiry_comment");
			
			
			if(err_check_count!=0)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
		});
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div style='color:#F00'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		function check_input_email($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div style='color:#F00'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str).val()) == false)
			{
				err ="<div style='color:#F00'>メールアドレスは半角英数字でご入力ください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}

		//メールチェック
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}

	});

</script>
</head>



<body>

<div id="container">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>

  <div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
    <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>
    <div id="mainbox"><!--コンテンツの外枠-->


<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /start /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
      <!--パンくず-->
      <ul id="pan">
        <li><a href="index.html">トップページ</a></li>
        <li><a href="index.html">*****</a></li>
        <li><a href="index.html">*****</a></li>
        <li>*****</li>
      </ul>
      <!--パンくず-->
  
      <div id="main" class="pt6"><!--コンテンツ左680幅-->
		<h2 class="text">コソットへお問い合わせ</h2>
			<div class="mailinfo">当サイトへのサービスにつきましては下記フォームよりお問い合わせ下さい。<br>※なるべく素早い対応を心がけますが、時期や質問内容などにより返信が遅くなる場合があります。ご理解下さいませ。</div>

			<!-- form-->
			<form action="inquiry_save.php" class="formoid-default-skyblue" style="font-size:13px;color:#666666;max-width:580px;min-width:150px" method="post">
			<div class="element-select">
				<label class="title">問い合わせ内容</label>
				<div class="large">
					<span>
          <? $var = "inquiry_sel";?>
          <?
          	$arr_inquiry_sel[] = "ログインが出来ない";
          	$arr_inquiry_sel[] = "口コミについて";
          	$arr_inquiry_sel[] = "投稿画像";
          	$arr_inquiry_sel[] = "信憑性について";
          	$arr_inquiry_sel[] = "機能改善の要望";
          	$arr_inquiry_sel[] = "広告内容";
          	$arr_inquiry_sel[] = "広告掲載希望";
          	$arr_inquiry_sel[] = "その他";
					?>
					<select name="<? echo $var;?>" id="<? echo $var;?>" >
          <? foreach($arr_inquiry_sel as $value) { ?>
						<option value="<? echo $value;?>"><? echo $value;?></option>
          <? } ?>
					</select>
          <label id="err_<?=$var;?>"></label>
					<i></i>
					</span>
				</div>
			</div>

			<div class="element-input">
				<label class="title">名前<span class="required">*</span></label>
        <? $var = "inquiry_name";?>
				<input name="<? echo $var;?>" id="<? echo $var;?>" class="large" type="text" />
        <label id="err_<?=$var;?>"></label>
			</div>

			<div class="element-email">
				<label class="title">メールアドレス<span class="required">*</span></label>
        <? $var = "inquiry_email";?>
				<input name="<? echo $var;?>" id="<? echo $var;?>" placeholder="半角英数字　例：sample@cossot.com" class="large" type="email" required/>
        <label id="err_<?=$var;?>"></label>
			</div>

			<div class="element-textarea">
        <label class="title">お問い合わせ内容<span class="required">*</span></label>
        <? $var = "inquiry_comment";?>
        <textarea name="<? echo $var;?>" id="<? echo $var;?>" class="medium" cols="20" rows="5" ></textarea>
        <label id="err_<?=$var;?>"></label>
			</div>

			<div class="submit">
					<input name="form_confirm" id="form_confirm" type="submit" value="お問合せを送信する"/>
			</div>
			</form>
			<script type="text/javascript" src="/kanto/app/form/formoid1/formoid-default-skyblue.js"></script>

      </div><!--main-->
      
<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /end /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
  
  
  
  
  
      <div id="sub"><!--sub------------------------------------------------------------------------->
				<!--よくある質問-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/qanda_navi.php"); ?>
				<!--よくある質問-->

      </div>



<script>
//最初以外を隠す
$('#faq .answer').hide();
//クリックイベント
$('#faq .question').click(function() {
     //スライドの処理
    if($(this).next('.answer').is(':visible')) {
        $(this).next('.answer').slideUp(300);
    } else {
        $(this).next('.answer').slideDown(300).siblings('.answer').slideUp(300);
    }
})

</script>


    </div>
    <!--main boxコンテンツの外枠-->
  
  
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>
  </div><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
  <!--contentsin-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
</div><!--container-->
</body>
</html>

