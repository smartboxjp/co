<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	$common_email = new CommonEmail(); //メール関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonInquiry.php";
	$common_inquiry = new CommonInquiry();
	
	$meta_title = "コソットへお問い合わせ";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/".$global_area."/common/header/header_meta.php";?>
</head>

<body>
<?
	$datetime = date("Y-m-d H:i:s");
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($inquiry_sel=="" || $inquiry_name=="" || $inquiry_email=="" || $inquiry_comment=="" )
	{
		$common_connect-> Fn_javascript_back("正しく入力して下さい。");
	}
	
	$member_id = $_SESSION['member_id'];
	$inquiry_status = "0";
	//array
	$arr_data = array();
	$arr_db_field = array("member_id", "inquiry_sel", "inquiry_name", "inquiry_email", "inquiry_comment", "inquiry_status");
	$arr_db_field = array_merge($arr_db_field, array("inquiry_email", "inquiry_comment", "inquiry_status"));

	
	//基本情報
	foreach($arr_db_field as $value) {
		$arr_data[$value] = $$value;
	}
	$common_inquiry -> Fn_inquiry_insert ($arr_data);
	
	//Thank youメール
	if ($inquiry_email != "")
	{
		$subject = "『 コソット 』お問い合わせありがとうございます。";
		
		$body = file_get_contents("./mail/inquiry.php");
		$body = str_replace("[inquiry_sel]", $inquiry_sel, $body);
		$body = str_replace("[inquiry_name]", $inquiry_name, $body);
		$body = str_replace("[inquiry_email]", $inquiry_email, $body);
		$body = str_replace("[inquiry_comment]", $inquiry_comment, $body);
		$body = str_replace("[datetime]", $datetime, $body);
		$body = str_replace("[global_send_mail]", $global_send_mail, $body);
		$body = str_replace("[global_email_footer]", $global_email_footer, $body);
		
		$common_email-> Fn_send_utf($inquiry_email."<".$inquiry_email.">",$subject,$body,$global_mail_from,$global_send_mail);
	}
	
	//$common_connect -> Fn_email_log($inquiry_email, $subject, $body); //メールログ
	$common_email-> Fn_send_utf($global_send_mail."<".$global_send_mail.">",$subject,$body,$global_mail_from,$global_send_mail);

	$common_connect-> Fn_javascript_move("お問い合わせありがとうございます。", global_no_ssl."/".$global_area."/member/");
?>

</body>

</html>
