
<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "タイトル";
	$meta_description = "description です";
	$meta_keywords = "キーワードです";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>


<!--jquery-->
	<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>
<!--ページ専用のCSS-->
	<link href="/kanto/help/css/lnquiry.css" rel="stylesheet" type="text/css" />
<!--アイコンで使用CSS-->
  	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--フォームで使用-->
	<link rel="stylesheet" href="/kanto/app/form/formoid1/formoid-default-skyblue.css" type="text/css" />
</head>



<body>

<div id="container">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>

  <div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
    <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>
    <div id="mainbox"><!--コンテンツの外枠-->


<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /start /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
      <!--パンくず-->
      <ul id="pan">
        <li><a href="index.html">トップページ</a></li>
        <li><a href="index.html">*****</a></li>
        <li><a href="index.html">*****</a></li>
        <li>*****</li>
      </ul>
      <!--パンくず-->
  
      <div id="main" class="pt6"><!--コンテンツ左680幅-->

		<h2 class="text">ログインについてよくある質問</h2>

			<div class="qandatitle">
				<div class="icon">
					<img src="/kanto/help/img/icon_q.png">
				</div>
				ログインパスワードを忘れてしましました。再発行は可能でしょうか？
			</div>

			<div class="qandablok">
				<div class="icon">
					<img src="/kanto/help/img/icon_a.png">
				</div>
				パスワードの再発行は可能です。【パスワードを忘れたら】から登録時のメールアドレスを入力して送信して下さい。
				折り返し、パスワードリセットのURLを送付したメールをお送りします。
			</div>



      </div><!--main-->
      
<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /end /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
  
  
  
  
  
      <div id="sub"><!--sub------------------------------------------------------------------------->

				<!--よくある質問-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/qanda_navi.php"); ?>
				<!--よくある質問-->
      </div>



<script>
//最初以外を隠す
$('#faq .answer').hide();
//クリックイベント
$('#faq .question').click(function() {
     //スライドの処理
    if($(this).next('.answer').is(':visible')) {
        $(this).next('.answer').slideUp(300);
    } else {
        $(this).next('.answer').slideDown(300).siblings('.answer').slideUp(300);
    }
})

</script>


    </div>
    <!--main boxコンテンツの外枠-->
  
  
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>
  </div><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
  <!--contentsin-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
</div><!--container-->
</body>
</html>

