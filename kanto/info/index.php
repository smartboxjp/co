<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "タイトル";
	$meta_description = "description です";
	$meta_keywords = "キーワードです";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

<!--ページ専用のCSS-->
<link href="/kanto/info/css/info.css" rel="stylesheet" type="text/css" />

<!--アイコン用CSS-->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

</head>


<body>

<div id="container">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>

	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>
		

		<div id="mainbox"><!--コンテンツの外枠-->


  
      			<div id="main" class="pt6"><!--コンテンツ左680幅-->
				<ul id="pan">
        				<li><a href="index.html">トップページ</a></li>
        				<li><a href="index.html">*****</a></li>
        				<li><a href="index.html">*****</a></li>
        				<li>*****</li>
      				</ul>

			
				<h2 class="text">編集部からのお知らせ</h2>

				<div class="infoblok">

					<div class="info">
						<div class="infotime">2019/08/25配信</div>
						<p class="infotitle">お店の評価が11段階まで増えました</p>
						<div class="infocont">
						10月1日からお店の口コミ評価が最大で11段階まで変更できるようになりました。
						これによりより細かく評価を設定することが可能になってます。
						</div>

					</div>



					<div id="nextcaunt">
						<ul>
							<li><a href="#">最初</a></li>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#">6</a></li>
							<li><a href="#">7</a></li>
							<li><a href="#">8</a></li>
							<li><a href="#">9</a></li>
							<li><a href="#">10</a></li>
							<li><a href="#">最後</a></li>
						</ul> 
					</div>
					<div class="frot"></div>

				</div>




			</div>
  
  
  
  
			<div id="sub"><!--sub------------------------------------------------------------------------->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
      			</div>
		</div>
  

		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>

	</div><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
<!--contentsin-->

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
</div>
<!--container-->



</body>
</html>

