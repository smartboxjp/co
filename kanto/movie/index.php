<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "タイトル";
	$meta_description = "description です";
	$meta_keywords = "キーワードです";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

<!--ページ専用のCSS-->
<link href="/kanto/css/maintop.css" rel="stylesheet" type="text/css" />
<link href="/kanto/movie/css/movie.css" rel="stylesheet" type="text/css" />

<!--アイコン用CSS-->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

</head>


<body>

<div id="container">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>

	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>
		

		<div id="mainbox"><!--コンテンツの外枠-->

			<div id="main" class="pt6"><!--コンテンツ左680幅-->
				<ul id="pan">
        				<li><a href="index.html">トップページ</a></li>
        				<li><a href="index.html">*****</a></li>
        				<li><a href="index.html">*****</a></li>
        				<li>*****</li>
      				</ul>


				<img src="/kanto/img/pinkline2.png" width="680" height="25" alt="" />

					<h2 class="text">ピックアップ動画</h2>

						<div class="movie">
						<!--ピックアップ動画  ここはランダムで動画が一本UPされる。マスター管理画面で店舗動画リストにチェック入れるとここに表示-->

							<video controls poster="/kanto/movie/img/vstart.png" width="640" height="360">
							<source src="/kanto/movie/deta/tesuto.webm">
							<source src="/kanto/movie/deta/tesuto.mp4">
							<p>動画を再生するには、videoタグをサポートしたブラウザが必要です。</p>
							</video> 

							<!--ピックアップ動画の店名-->
							<div class="kutikomirisutoshop"><a href="" target="_blank">2016/05/25投稿◆アネージュ～池袋支店～◆</a></div>



							<div class="cmcommentt">
								お店からのコメント
							</div>

							<div class="cmcomment">
								ミリオン新規オープンのお店ですのでスタートみんな一緒！だから当然派閥もいじめのなし！簡単研修システムで誰でも月収70万円はすぐに稼げますので心配はいりません！
							</div>

						</div>

						<div class="movielist">


							<div class="movies2">

							<table>
								<tr>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov1.jpg" width="170" height="95" alt="" /><br>店名最大で二十文字文字まで表示可能ですよ</a></td>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov3.jpg" width="170" height="95" alt="" /><br>店名最大で20文字まで表示可能ですよ</a></td>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov4.jpg" width="170" height="95" alt="" /><br>店名最大で20文字まで表示可能ですよ</a></td>
								</tr>

								<tr>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov5.jpg" width="170" height="95" alt="" /><br>店名最大で20文字まで表示可能ですよ</a></td>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov6.jpg" width="170" height="95" alt="" /><br>店名最大で20文字まで表示可能ですよ</a></td>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov7.jpg" width="170" height="95" alt="" /><br>店名最大で20文字まで表示可能ですよ</a></td>
								</tr>

								<tr>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov5.jpg" width="170" height="95" alt="" /><br>店名最大で20文字まで表示可能ですよ</a></td>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov6.jpg" width="170" height="95" alt="" /><br>店名最大で20文字まで表示可能ですよ</a></td>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov7.jpg" width="170" height="95" alt="" /><br>店名最大で20文字まで表示可能ですよ</a></td>
								</tr>
							</table>
							</div>





							<div class="mainprmovie">
								<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/app/pr/contents_bottom.php"); ?>

							</div>


							<div class="movies2">
							<table>
								<tr>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov1.jpg" width="170" height="95" alt="" /><br>店名最大で二十文字文字まで表示可能ですよ</a></td>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov3.jpg" width="170" height="95" alt="" /><br>店名最大で20文字まで表示可能ですよ</a></td>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov4.jpg" width="170" height="95" alt="" /><br>店名最大で20文字まで表示可能ですよ</a></td>
								</tr>

								<tr>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov5.jpg" width="170" height="95" alt="" /><br>店名最大で20文字まで表示可能ですよ</a></td>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov6.jpg" width="170" height="95" alt="" /><br>店名最大で20文字まで表示可能ですよ</a></td>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov7.jpg" width="170" height="95" alt="" /><br>店名最大で20文字まで表示可能ですよ</a></td>
								</tr>

								<tr>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov5.jpg" width="170" height="95" alt="" /><br>店名最大で20文字まで表示可能ですよ</a></td>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov6.jpg" width="170" height="95" alt="" /><br>店名最大で20文字まで表示可能ですよ</a></td>
								<td><span class="movietime">2016/05/25投稿</span><br><a href="" target="_blank"><img src="/kanto/movie/img/test/mov7.jpg" width="170" height="95" alt="" /><br>店名最大で20文字まで表示可能ですよ</a></td>
								</tr>
							</table>
							</div>


						</div><!--movielist-->



						<div class="anderjob">

							<div id="nextcaunt">
								<ul>
								<li><a href="#">最初</a></li>
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">最後</a></li>
								</ul> 
							</div>
							<div class="clear"></div>
						</div>


						<div class="mainprmovie">
							<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/app/pr/contents_bottom.php"); ?>
						</div>


					</div><!--main-->

      
		<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /end /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
  
  
  
  
  
			<div id="sub"><!--sub------------------------------------------------------------------------->
        			<!--検索-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
        			<!--検索-->
        
        			<!--PR店舗、ページ表示の度に有料店舗から3店舗ランダムで表示。-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prshop.php"); ?>
        			<!--PR店舗、ページ表示の度に有料店舗から3店舗ランダムで表示。-->

        			<!--広告部分その1　280×250-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner.php"); ?>
        			<!--広告部分その1　280×250-->
        
        			<!--検索リスト-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
        			<!--検索リスト-->
        
        			<!--広告部分その2　280×250-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner2.php"); ?>
        			<!--広告部分その2　280×250-->

        			<!--検索-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
        			<!--検索-->
      
        			<!--編集部からのお知らせ-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/info.php"); ?>
        			<!--編集部からのお知らせ->
        
        			<!--お役立ちツール ノープログラム-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/tool.php"); ?>
        			<!--お役立ちツール-->
      			</div>
		</div>
  

		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>

	</div><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
<!--contentsin-->

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
</div>
<!--container-->



</body>
</html>

