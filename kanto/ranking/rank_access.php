<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連

    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMasterboard.php";
    $Common_masterboard = new CommonMasterboard();

    foreach($_GET as $key => $value)
    {
        $$key = $common_connect->h($value);
    }
    
    //エリア
    $sql = " select s.cate_area_s_id, s.cate_area_l_id, cate_area_l_title, cate_area_s_title FROM cate_area_l l inner join cate_area_s s on l.cate_area_l_id=s.cate_area_l_id order by s.view_level" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_area[$db_result[$db_loop]["cate_area_s_id"]] = $db_result[$db_loop]["cate_area_l_title"].":".$db_result[$db_loop]["cate_area_s_title"];
        }
    }
  
    //職種
    $sql = " select cate_job_id, cate_job_title FROM cate_job order by view_level" ; 
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_job[$db_result[$db_loop]["cate_job_id"]] = $db_result[$db_loop]["cate_job_title"];
        }
    }
    
	$meta_title = "タイトル";
	$meta_description = "description です";
	$meta_keywords = "キーワードです";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>


<!--アイコン用CSS-->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--専用CSS-->
<link href="/kanto/ranking/css/rank.css" rel="stylesheet" type="text/css" />
<link href="/kanto/search/css/searchresult.css" rel="stylesheet" type="text/css" />
<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

</head>


<body>

<div id="container">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>

	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>
		

		<div id="mainbox"><!--コンテンツの外枠-->


		<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /start /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
  
      			<div id="main" class="pt6"><!--コンテンツ左680幅-->
				<ul id="pan">
        				<li><a href="index.html">トップページ</a></li>
        				<li><a href="index.html">*****</a></li>
        				<li><a href="index.html">*****</a></li>
        				<li>*****</li>
      				</ul>







			<!--ランキング種類-->
				<ul class="rankchoice">
					<li><a href="/kanto/ranking/rank_point.php"><img src="/kanto/ranking/img/acclaimed.jpg" onmouseover="this.src='/kanto/ranking/img/acclaimed2.jpg'" onmouseout="this.src='/kanto/ranking/img/acclaimed.jpg'" /></a></li>
					<li><img src="/kanto/ranking/img/accessrank2.jpg" onmouseover="this.src='/kanto/ranking/img/accessrank2.jpg'" onmouseout="this.src='/kanto/ranking/img/accessrank2.jpg'" /></li>
					<li><a href="/kanto/ranking/rank_blog.php"><img src="/kanto/ranking/img/bestblog.jpg" onmouseover="this.src='/kanto/ranking/img/bestblog2.jpg'" onmouseout="this.src='/kanto/ranking/img/bestblog.jpg'" /></a></li>


				</ul>

				<ul class="rankchoice">

					<li>平均評価の高いお店</li>
					<li>アクセス数の多いお店</li>
					<li>よく読まれたブログ記事</li>

				</ul>

<?
    $sql = "SELECT max(regi_date) as max_yyyymmdd FROM shop_ranking where regi_date<='".date("Y/m/d")."'";
    $arr_max_yyyymmdd = $common_dao->db_query_bind($sql);
    if($arr_max_yyyymmdd)
    {
        $max_yyyymmdd = $arr_max_yyyymmdd[0]["max_yyyymmdd"];
    }
?>
                <h2 class="text">【<? echo date("Y年m月d日", strtotime($max_yyyymmdd));?>更新】</h2>


				<div class="ranktop10"><!--店舗サムネイル外枠-->

					<div class="ranktitle">
						<img src="/kanto/ranking/img/ranktitle2.png" width="680" height="27" alt="" />
					</div>


<!--店舗サムネイルデザインランク1--------------------------------------------------------------------------------------------------------->
<?php

    //リスト表示
    $arr_db_field = array( "shop_name", "cate_area_s_id", "cate_job_id", "shop_title", "img_1", "shop_thumbnail", "ranking", "evaluation", "catchcopy", "open_time_from", "open_time_to");
    $arr_db_field = array_merge( $arr_db_field, array( "s.shop_id" ) );
    
    $sql = "SELECT r.regi_date, ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM shop_access_ranking r inner join shop s on r.shop_id=s.shop_id ";
    $sql .= " where s.flag_open=1 and r.regi_date=(SELECT max(regi_date) FROM shop_access_ranking where regi_date<='".date("Y/m/d")."') " ;
    $sql .= " order by r.ranking ";
    $sql .= " limit 0, 10";

    $arr_list = $common_dao->db_query_bind($sql);
    if($arr_list)
    {
        for($db_loop=0 ; $db_loop < count($arr_list) ; $db_loop++)
        {
          foreach($arr_db_field as $val)
          {
            $$val = $arr_list[$db_loop][$val];
          }
          $shop_id = $arr_list[$db_loop]["shop_id"];
          $regi_date = $arr_list[$db_loop]["regi_date"];　
?>
                    <div class="shopouterframe">
                    
                        <div class="shopinformationtop">
                            <h3 class="shopinfocatchcopyrank"><? echo $catchcopy;?></h3><!--26文字-->

                            <div class="shopinformationbookmark">
                                <a href="/kanto/shop/?shop_id=<? echo $shop_id;?>">
                                <img src="/kanto/common/img/bookmark.png" onmouseover="this.src='/kanto/common/img/bookmarkhava.png'" onmouseout="this.src='/kanto/common/img/bookmark.png'" />
                                </a>
                            </div>
                            <div class="shopinformationmail">
                                <a href="/kanto/shop/?shop_id=<? echo $shop_id;?>">
                                    <img src="/kanto/common/img/mailfroms.png" onmouseover="this.src='/kanto/common/img/mailfromshava.png'" onmouseout="this.src='/kanto/common/img/mailfroms.png'" />
                                </a>
                            </div>
                            <div class="frotclear"><!--フロートクリア--></div>

                        </div>
<!--ショップ左部分-->
                        <div class="shopinformationleftrank">
                        <!--kokokara-->

                            <? if($ranking<=3) { ?>
                            <div class="imgmedaru">
                                <img src="/kanto/common/img/imgmedaru<? echo $ranking;?>.png">
                            </div>
                            <? } ?>

                        <!--kokomade-->
                            <div class="shopimage120ranktop1<? if($ranking>3) { echo "0";}?>">
                                <a href="/kanto/shop/?shop_id=<? echo $shop_id;?>" class="drop">
                                <?
                                if($shop_thumbnail!="")
                                {
                                    echo "<img src='/".global_shop_dir.$shop_id."/".$shop_thumbnail."'>";
                                } else {
                                    echo "<img src='/img/noimg/shop-main.png'>";
                                }
                                ?>
                                </a>
                            </div>
                            <?
                            $result_ranking = "-";
                            $sql_ranking = "SELECT ranking FROM shop_ranking where shop_id='".$shop_id."' and regi_date<'".$regi_date."' order by regi_date desc limit 0, 1 " ;
                            
                            $db_result_ranking = $common_dao->db_query_bind($sql_ranking);
                            if($db_result_ranking)
                            {
                                $result_ranking = $db_result_ranking[0]["ranking"];
                            }
                            ?>
                            <div class="rankshopimg">
                                <div class="rankoonament"><img src="/kanto/ranking/img/rank<? echo $result_ranking;?>.png" alt="" />前回から
                                <? if($result_ranking==$ranking) { ?>
                                <span class="keepgeen">keep</span>
                                <? } elseif($result_ranking>$ranking || $result_ranking=="-") { ?>
                                <span class="upred">UP</span>
                                <? } elseif($result_ranking<$ranking) { ?>
                                <span class="downblue">down</span>
                                <? }  ?>
                                </div>
                            </div>


                        </div>


                        <div class="shopinformationrightrank">

                            
                            <div class="shoptitlerank">
                                <a href="/kanto/shop/?shop_id=<? echo $shop_id;?>"><? echo $shop_name;?></a>
                            </div>
                            

                            <div class="shoprankframe"><!--星、口コミ数、点数の外枠-->

                                <div class="hosi"><img src="/kanto/common/img/star<? echo sprintf('%.1f',$evaluation);?>.png"></div>

                                <div class="scor">【<? if($evaluation==0){ echo "-.--";} else { echo sprintf('%.2f',$evaluation);}?>点】</div>

                                <div class="kutikazu"><i class="fa fa-comment-o"></i> 投稿口コミ数<span class="talk">13<!--お店の口コミ数が表示--></SPAN>件</div>

                                <div class="shophomepage"><i class="fa fa-desktop"></i> <?php if($hp_offical=="") { echo "なし"; } else { ?><a href="<?php echo $hp_offical;?>" target="_blank">営業ページを見る</a><?php } ?></div>
                            </div>


                            <div class="shopstatusframe">
                                <dl>
                                <dd><span><img src="/kanto/common/img/i-con/world.gif"></span> エリア:<? echo $arr_cate_area[$cate_area_s_id];?></dd><!--エリア-->
                                <dd><span><img src="/kanto/common/img/i-con/card.gif"></span> 業種:<? echo $arr_cate_job[$cate_job_id];?></dd><!--業種-->
                                <dd><span><img src="/kanto/common/img/i-con/time.gif"></span> 営業時間:<? echo $open_time_from;?><? if($open_time_to!=""){ echo "-".$open_time_to;}?></dd><!--営業時間-->
                                </dl>
                            </div><!--エリア、業種、営業時間の外枠-->

                        </div>

                        <div class="shopinformationbottom">

                        </div>


                    </div>
<!--店舗デザインここまで--------------------------------------------------------------------------------------------->
<?
        }
    }
?>






<!------------------------------------1ページ10店舗まで表示------------------------------------->

				</div>

				<ul class="rankchoice">
					<li><a href="/kanto/ranking/rank_point.php"><img src="/kanto/ranking/img/acclaimed.jpg" onmouseover="this.src='/kanto/ranking/img/acclaimed2.jpg'" onmouseout="this.src='/kanto/ranking/img/acclaimed.jpg'" /></a></li>
					<li><img src="/kanto/ranking/img/accessrank2.jpg" onmouseover="this.src='/kanto/ranking/img/accessrank2.jpg'" onmouseout="this.src='/kanto/ranking/img/accessrank2.jpg'" /></li>
					<li><a href="/kanto/ranking/rank_blog.php"><img src="/kanto/ranking/img/bestblog.jpg" onmouseover="this.src='/kanto/ranking/img/bestblog2.jpg'" onmouseout="this.src='/kanto/ranking/img/bestblog.jpg'" /></a></li>


				</ul>

				<ul class="rankchoice">

					<li>平均評価の高いお店</li>
					<li>アクセス数の多いお店</li>
					<li>よく読まれたブログ記事</li>

				</ul>



			

			</div>
      
		<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /end /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
  
  
  
  
  
			<div id="sub"><!--sub------------------------------------------------------------------------->
        			<!--検索-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
        			<!--検索-->
        

        			<!--ランキングメニュー-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/rankinglist.php"); ?>
             			<!--ランキングメニュー-->
        
        			<!--検索リスト-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
        			<!--検索リスト-->
        
      			</div>
		</div>
  

		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>

	</div><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
<!--contentsin-->

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
</div>
<!--container-->



</body>
</html>

