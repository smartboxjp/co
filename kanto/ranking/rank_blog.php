<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "タイトル";
	$meta_description = "description です";
	$meta_keywords = "キーワードです";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>


<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--専用CSS-->
<link href="/kanto/ranking/css/rank.css" rel="stylesheet" type="text/css" />
<link href="/kanto/search/css/searchresult.css" rel="stylesheet" type="text/css" />
<link href="/kanto/blog/css/jobblog.css" rel="stylesheet" type="text/css" />
<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

</head>


<body>

<div id="container">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>

	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>
		

		<div id="mainbox"><!--コンテンツの外枠-->


  			<div id="main" class="pt6"><!--コンテンツ左680幅-->
				<ul id="pan">
        				<li><a href="index.html">トップページ</a></li>
        				<li><a href="index.html">*****</a></li>
        				<li><a href="index.html">*****</a></li>
        				<li>*****</li>
      				</ul>


			<!--ランキング種類-->
				<ul class="rankchoice">
					<li><a href="/kanto/ranking/rank_point.php"><img src="/kanto/ranking/img/acclaimed.jpg" onmouseover="this.src='/kanto/ranking/img/acclaimed2.jpg'" onmouseout="this.src='/kanto/ranking/img/acclaimed.jpg'" /></li>
					<li><a href="/kanto/ranking/rank_access.php"><img src="/kanto/ranking/img/accessrank.jpg" onmouseover="this.src='/kanto/ranking/img/accessrank2.jpg'" onmouseout="this.src='/kanto/ranking/img/accessrank.jpg'" /></a></li>
					<li><img src="/kanto/ranking/img/bestblog2.jpg" onmouseover="this.src='/kanto/ranking/img/bestblog2.jpg'" onmouseout="this.src='/kanto/ranking/img/bestblog2.jpg'" /></li>


				</ul>

				<ul class="rankchoice">

					<li>平均評価の高いお店</li>
					<li>アクセス数の多いお店</li>
					<li>よく読まれたブログ記事</li>

				</ul>



<?
	$sql = "SELECT max(regi_date) as max_yyyymmdd FROM shop_blog_ranking where regi_date<='".date("Y/m/d")."'";
	$arr_max_yyyymmdd = $common_dao->db_query_bind($sql);
	if($arr_max_yyyymmdd)
	{
		$max_yyyymmdd = $arr_max_yyyymmdd[0]["max_yyyymmdd"];
	}
?>
				<h2 class="text">【<? echo date("Y年m月d日", strtotime($max_yyyymmdd));?>更新】</h2>

<!--ブログランキング-------------------------------------------------------------->

				<div class="ranktop10"><!--店舗サムネイル外枠-->


				<div class="ranktitle">
					<img src="/kanto/ranking/img/ranktitle3.png" width="680" height="27" alt="" />
				</div>


<!--ブログランキング1位から3位まで-------------------------------------------------------------->

				<div class="rankbox">
<?php
    $where = " ";
    $where .= " and b.shop_id in (select shop_id from shop where flag_open=1 and shop_id=b.shop_id ) ";
    $where .= " and r.regi_date=(SELECT max(regi_date) FROM shop_blog_ranking where regi_date<='".date("Y/m/d")."') " ;

	$arr_db_field = array("shop_blog_title", "ranking", "img_1");

	$sql = "SELECT ";
	$sql .= " b.shop_id, b.shop_blog_id, b.regi_date, ";
	foreach($arr_db_field as $val)
	{
	    $sql .= $val.", ";
	}
	$sql .= " 1 FROM shop_blog b inner join shop_blog_ranking r on b.shop_blog_id=r.shop_blog_id where 1 ".$where ;
	$sql .= " order by ranking";

	$arr_list = $common_dao->db_query_bind($sql);
    if($arr_list)
    {
        for($db_loop=0 ; $db_loop < count($arr_list) ; $db_loop++)
        {
          foreach($arr_db_field as $val)
          {
            $$val = $arr_list[$db_loop][$val];
          }
          $shop_blog_id = $arr_list[$db_loop]["shop_blog_id"];
          $shop_id = $arr_list[$db_loop]["shop_id"];
          $regi_date = $arr_list[$db_loop]["regi_date"];
?>

					<div class="boxtop<? if($ranking<=3) { echo "3";} else { echo "10";}?>">
						<div class="rankblogmark"><img src="/kanto/ranking/img/rank<? echo $ranking;?>.png"/></div>
						<div class="blogtimetoprank"><? echo date("Y/m/d", strtotime($regi_date));?>投稿</div>
						<p class="img-blogblock">
							<a href="/kanto/shop/blog_details.php?shop_blog_id=<? echo $shop_blog_id;?>">
							<? if($img_1!="") { ?>
							<img src="<? echo "/".global_shop_blog_dir.$shop_blog_id."/".$img_1;?>" width=90>
							<? } else {?>
							<img src="/img/noimg/shop-blog.png">
							<? } ?>
						</p>
						<div class="blogtitletoprank">
							<a href="/kanto/shop/blog_details.php?shop_blog_id=<? echo $shop_blog_id;?>"><? echo $shop_blog_title;?></a>
						</div>
					</div>
<?
		}
	}
?>




				</div>

<!--ブログランキング-------------------------------------------------------------->






				</div><!--店舗サムネイル外枠<div class="ranktop10">ここまで-->



				<ul class="rankchoice">
					<li><a href="/kanto/ranking/rank_point.php"><img src="/kanto/ranking/img/acclaimed.jpg" onmouseover="this.src='/kanto/ranking/img/acclaimed2.jpg'" onmouseout="this.src='/kanto/ranking/img/acclaimed.jpg'" /></li>
					<li><a href="/kanto/ranking/rank_access.php"><img src="/kanto/ranking/img/accessrank.jpg" onmouseover="this.src='/kanto/ranking/img/accessrank2.jpg'" onmouseout="this.src='/kanto/ranking/img/accessrank.jpg'" /></a></li>
					<li><img src="/kanto/ranking/img/bestblog2.jpg" onmouseover="this.src='/kanto/ranking/img/bestblog2.jpg'" onmouseout="this.src='/kanto/ranking/img/bestblog2.jpg'" /></li>


				</ul>

				<ul class="rankchoice">

					<li>平均評価の高いお店</li>
					<li>アクセス数の多いお店</li>
					<li>よく読まれたブログ記事</li>

				</ul>

<!--ランキング種類-->


			

			</div>
      
			<div id="sub"><!--sub------------------------------------------------------------------------->
        			<!--検索-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
        			<!--検索-->
        

        			<!--ランキングメニュー-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/rankinglist.php"); ?>
             			<!--ランキングメニュー-->
        
        			<!--検索リスト-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
        			<!--検索リスト-->
        
      			</div>
		</div>
  

		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>

	</div><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
<!--contentsin-->

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
</div>
<!--container-->


	<!--全ページ共通スクリプト-->
	<script type="text/javascript" src="/kanto/app/textcount/textcount.js"></script>
	<script type="text/javascript" src="/kanto/app/resize/imgLiquid-min.js"></script>
	<script type="text/javascript">
$(document).ready(function() {
    $(".img-blogblock").imgLiquid({
    });
});
</script>


</body>
</html>

