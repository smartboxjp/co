<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateJob.php";
	$common_cate_area = new CommonCateArea();
	$common_cate_job = new CommonCateJob();
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

  <!--アイコン用CSS-->
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <!--フォームデザイン-->
  <link rel="stylesheet" href="/kanto/app/form/formoid1/formoid-default-skyblue.css" type="text/css" />
  <!--ページ専用のCSS-->
  <link href="/kanto/search/css/searcharea.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	$(function() {
		$('input:checkbox').click(function() {
			//勤務地の大分類
			if($(this).attr("id").substr(0, 2) == "ll")
			{
				if ($(this).is(':checked')) {
					$('.'+$(this).attr("id")).prop('checked',true);
				} else {
					$('.'+$(this).attr("id")).prop('checked',false);
				}
			}
			
			//勤務地の大分類
			if($(this).attr("class").substr(0, 2) == "ll")
			{
				//alert($(this).attr("class"));
				//alert($('.'+$(this).attr("class")+':checked').length );
				if($('.'+$(this).attr("class")).length==$('.'+$(this).attr("class")+':checked').length) {
					$('#'+$(this).attr("class")).prop('checked',true);
				} else {
					$('#'+$(this).attr("class")).prop('checked',false);
				}
			}
		});
	});
	
//-->
</script>
</head>

<body>

<div id="container">

	<!--グローバルナビゲーション-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?><!--ナビゲーション-->
	<!--グローバルナビゲーション-->


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<!--ログインナビゲーション-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?><!--ログインナビゲーション-->	
		<!--ログインナビゲーション-->

		<div id="mainbox">

			<div id="main" class="pt6"><!--コンテンツ左680幅-->

			<!--パンくず-->
				<ul id="pan">
          			<li><a href="index.html">トップページ</a></li>
          			<li><a href="index.html">*****</a></li>
          			<li><a href="index.html">*****</a></li>
          			<li>*****</li>
				</ul>
			<!--パンくず-->

        <form action="search_result.php" method="get">
				<h2 class="text">働きたいエリアを絞る</h2>
        
				<?
				//area count
				$result_cate_area_count = $common_cate_area -> Fn_cate_area_s_count();
				if(!is_null($result_cate_area_count))
				{
					foreach($result_cate_area_count as $key=>$value)
					{
						$arr_cate_area_count[$value["cate_area_s_id"]] = $value["shop_count"];
					}
				}
				
				//area 大
				$arr_where = array();
				$arr_where["flag_open"] = "1"; //公開のみ
				$result_cate_area_l = $common_cate_area -> Fn_cate_area_l_list ($arr_where) ;
        ?>
				<table class="searchjob">
					<tbody>
          <?
					foreach($result_cate_area_l as $arr_cate_area_l)
					{
						$cate_area_l_id = $arr_cate_area_l["cate_area_l_id"];
						$cate_area_l_title = $arr_cate_area_l["cate_area_l_title"];
					?>
					<tr>
						<th><? echo $cate_area_l_title;?></th>

						<td>
							<input type="checkbox" name="al[]" id="ll<? echo $cate_area_l_id;?>" value="<? echo $cate_area_l_id;?>"><label for="ll<? echo $cate_area_l_id;?>" class="check">全てチェック</label>
							</br>
              <?
							//area 小
							$arr_where_s= array();
							$arr_where_s["flag_open"] = "1"; //公開のみ
							$arr_where_s["cate_area_l_id"] = $cate_area_l_id;
							$result_cate_area_s =  $common_cate_area->Fn_cate_area_s_list ($arr_where_s) ;
							foreach($result_cate_area_s as $arr_cate_area_s)
							{
								$cate_area_s_id = $arr_cate_area_s["cate_area_s_id"];
								$cate_area_s_title = $arr_cate_area_s["cate_area_s_title"];
              ?>
							<input type="checkbox" name="as[]" value="<? echo $cate_area_s_id;?>" id="area_<? echo $cate_area_s_id;?>" class="ll<? echo $cate_area_l_id;?>"><label for="area_<? echo $cate_area_s_id;?>" class="check" ><? echo $cate_area_s_title;?>(<? if($arr_cate_area_count[$cate_area_s_id]=="") { echo "0";} else{ echo $arr_cate_area_count[$cate_area_s_id];}?>)</label>
              <?
							}
							?>
						</td>
					</tr>
					<?
					} //foreach $arr_cate_area_l
          ?>

					</tbody>
				</table>




<!--絞込み-->

				<div class="searchbon">
					<div class="formoid-default-skyblue"><input type="submit" value="この条件で検索をする"></div>
				</div>
<!--絞込み-->



				<h2 class="text">業種や口コミで更に絞る</h2>
				<table class="searchjob">
					<tbody>

					<tr>
						<th>働きたい業種</th>

						<td>
							<input type="checkbox" name="j[]" id="ll_job" value="1"><label for="ll_job" class="check">全てチェック</label>
							</br>

							<?
							//job count
							$result_cate_job_count = $common_cate_job -> Fn_cate_job_count();
							if(!is_null($result_cate_job_count))
							{
								foreach($result_cate_job_count as $key=>$value)
								{
									$arr_cate_job_count[$value["cate_job_id"]] = $value["shop_count"];
								}
							}
							
              //職種
							$arr_where = array();
							$arr_where["flag_open"] = "1";//公開のみ
							
							$arr_data = array();
							$arr_data[] = "cate_job_id";
							$arr_data[] = "cate_job_title";
									
							$result_cate_job = $common_cate_job -> Fn_db_cate_job_list ($arr_data, $arr_where) ;
              
              foreach($result_cate_job as $arr_cate_job)
              {
                $cate_job_id = $arr_cate_job["cate_job_id"];
                $cate_job_title = $arr_cate_job["cate_job_title"];
              ?>
							<input type="checkbox" name="job[]" value="<? echo $cate_job_id;?>" id="job_<? echo $cate_job_id;?>" class="ll_job"><label for="job_<? echo $cate_job_id;?>" class="check" ><? echo $cate_job_title;?>(<? if($arr_cate_job_count[$cate_job_id]=="") { echo "0";} else{ echo $arr_cate_job_count[$cate_job_id];}?>)</label>
							<?	
							}
              ?>
						</td>


					</tr>

					</tbody>

				</table>




				<table class="searchjob">
					<tbody>

					<tr>
						<th>口コミ数が○○以上</th>
						<td>
            	<? $var = "vc"; $voice_count=1;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>"><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"><? echo $voice_count;?>件以上</label>
            	
							<? $var = "vc"; $voice_count=5;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>"><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"><? echo $voice_count;?>件以上</label>
              
            	<? $var = "vc"; $voice_count=10;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>"><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"><? echo $voice_count;?>件以上</label>
              
            	<? $var = "vc"; $voice_count=14;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>"><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"><? echo $voice_count;?>件以上</label>
              
						</td>
					</tr>

					<tr>
						<th>平均評価が○○以上</th>
						<td>
            	<? $var = "ep"; $voice_count=3;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>"><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"><? echo $voice_count;?>件以上</label>
            	<? $var = "ep"; $voice_count=3.5;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>"><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"><? echo $voice_count;?>件以上</label>
            	<? $var = "ep"; $voice_count=4;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>"><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"><? echo $voice_count;?>件以上</label>
            	<? $var = "ep"; $voice_count=4.5;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>"><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"><? echo $voice_count;?>件以上</label>
						</td>

					</tr>

					<tr>
						<th>さらにPR店のみに絞る</th>
						<td>
            	<? $var = "s_pickup"; $voice_count=0;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>"><label for="<? echo $var;?><? echo $voice_count;?>" class="radio">絞らない</label>
              
            	<? $var = "s_pickup"; $voice_count=1;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>"><label for="<? echo $var;?><? echo $voice_count;?>" class="radio">絞る</label>

						</td>
					</tr>
					</tbody>
				</table>




<!--絞込み-->

				<div class="searchbon">
					<div class="formoid-default-skyblue"><input type="submit" value="この条件で検索をする"></div>
				</div>
<!--絞込み-->
        </form>





<!--広告スペースメインボックス下 マスター管理から画像のアップ、リンク先の変更可能　ランダム表示 jpg,gif共にアップ可能で枚数は最大5枚まで-->
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/app/pr/contents_bottom.php"); ?>
<!--広告スペースメインボックス下 マスター管理から画像のアップ、リンク先の変更可能　ランダム表示 jpg,gif共にアップ可能で枚数は最大5枚まで-->




			</div>
<!--/main-->
<!--コンテンツここまで-->



			<div id="sub"><!--sub------------------------------------------------------------------------->
				<!--検索-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
				<!--検索-->

				<!--広告部分その1　280×250-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner.php"); ?>
				<!--広告部分その1　280×250-->

				<!--検索リスト-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
				<!--検索リスト-->

				<!--新店。掲載1ヶ月、または表示件数が4件以上から消える 最新順-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/newshop.php"); ?>
				<!--新店。掲載1ヶ月、または表示件数が4件以上から消える-->

				<!--広告部分その2　280×250-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner2.php"); ?>
				<!--広告部分その2　280×250-->

				<!--お役立ちツール ノープログラム-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/tool.php"); ?>
				<!--お役立ちツール-->
			</div>
			<!--/sub------------------------------------------------------->


		</div>
<!--main boxコンテンツの外枠-->


		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>

	</div><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
  <!--contentsin-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
</div><!--container-->
</body>
</html>

