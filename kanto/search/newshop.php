<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateJob.php";
	$common_cate_area = new CommonCateArea();
	$common_cate_job = new CommonCateJob();
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>
<!--ページ専用のCSS-->
<link href="/kanto/ranking/css/rank.css" rel="stylesheet" type="text/css" />
<link href="/kanto/search/css/searchresult.css" rel="stylesheet" type="text/css" />
<link href="/kanto/search/css/newshop.css" rel="stylesheet" type="text/css" />
<!--アイコン用CSS-->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
</head>



<body>

<div id="container">


	<!--グローバルナビゲーション-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?><!--ナビゲーション-->
	<!--グローバルナビゲーション-->


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->



		<!--ログインナビゲーション-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?><!--ログインナビゲーション-->	
		<!--ログインナビゲーション-->



		<div id="mainbox"><!--コンテンツの外枠-->



			<div id="main" class="pt6"><!--コンテンツ左680幅-->
			<!--ランキング3種類、選択中のランキング部分のボタンは～2.jpgのマウスオーバ時の画像を使用。-->


			<!--パンくず-->
				<ul id="pan">
				<li><a href="index.html">トップページ</a></li>
				<li><a href="index.html">*****</a></li>
				<li><a href="index.html">*****</a></li>
				<li>*****</li>
				</ul>
			<!--パンくず-->



				<h2 class="text">新着店舗</h2>


				<div class="newshophed">
					<i class="fa fa-exclamation-triangle"></i>
					業種、エリア関係なく登録から2ヶ月までの全ての店舗が表示されます。
				</div>


				<div class="ranktop10"><!--店舗サムネイル外枠-->


<!--店舗サムネイルデザイン--------------------------------------------------------------------------------------------------------->

				<div class="shopday">【08月17日登録】</div>
					<div class="shopouterframenewshop"><!--店舗サムネイルデザイン外枠-->


					<!--ショップヘッダー部分-->
					<div class="shopinformationtopnewshop">
						<h3 class="shopinfocatchcopyrank">キャッチコピー。店舗側で登録可能で文字数は最大で二十六</h3><!--26文字-->


						<div class="shopinformationbookmark"><a href="about.html"><img src="/kanto/search/img/bookmark.png" onmouseover="this.src='/kanto/search/img/bookmarkhava.png'" onmouseout="this.src='/kanto/search/img/bookmark.png'" /></a></div><!--ブックマークボタン-->
						<div class="shopinformationmail"><a href="shopfrom.html" target="_blank"><img src="/kanto/search/img/mailfroms.png" onmouseover="this.src='/kanto/search/img/mailfromshava.png'" onmouseout="this.src='/kanto/search/img/mailfroms.png'" /></a></div><!--メールボタン-->
						<div class="frotclear"><!--フロートクリア--></div>


					</div><!--ショップヘッダー部分-->


					<!--ショップ左部分-->
					<div class="shopinformationleftrank">
						<div class="shopimage120newshop"><a href="" target="_blank" class="drop"><img src="/kanto/shop/img/test/sanple1.jpg" alt="" /></a></div><!--ショップイメージ画像-->
						<div class="frotclearnewshop"></div><!--フロートクリア-->


					</div><!--ショップ左部分-->	




					<!--ショップ右部分-->
					<div class="shopinformationrightrank">

						<div class="shoptitlebackground"><!--ショップの名前部分背景デザイン-->
							<div class="shoptitlerank"><a href="" target="_blank">ただアナタに逢いたくて店名最大で二十文字</a></div><!--ショップの名前　11文字まで-->
						</div><!--ショップの名前部分背景デザイン-->

						<div class="shoprankframe"><!--星、口コミ数、点数の外枠-->
							<!--評価☆-->
							<div class="hosi"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"></div>
							<!--評価点数-->
							<div class="scor">【-.--点】</div>
							<!--投稿口コミ数-->
							<div class="kutikazu"><i class="fa fa-comment-o"></i> 投稿口コミ数<span class="talk">0<!--お店の口コミ数が表示--></SPAN>件</div>
							<!--営業ページをみる-->
							<div class="shophomepage"><i class="fa fa-desktop"></i> <a href="" target="_blank">営業ページを見る</a></div>
						</div><!--星、口コミ数、点数の外枠-->




						<div class="shopstatusframe"><!--エリア、業種、営業時間の外枠-->
							<dl>
							<dd><i class="fa fa-globe"></i> エリア:東京都/池袋 巣鴨 大塚</dd><!--エリア-->
							<dd><i class="fa fa-cube"></i> 業種:デリバリーヘルス</dd><!--業種-->
							<dd><i class="fa fa-clock-o"></i> 営業時間:10:00-24:00</dd><!--営業時間-->
							</dl>
						</div><!--エリア、業種、営業時間の外枠-->


					</div><!--ショップ右部分-->



					<!--ショップフッター部分外枠　最新口コミ-->
					<div class="shopinformationbottom">
					</div><!--ショップフッター部分-->


				</div>
<!--店舗デザインここまで--------------------------------------------------------------------------------------------->





<!--店舗サムネイルデザイン--------------------------------------------------------------------------------------------------------->

				<div class="shopday">【08月17日登録】</div>
					<div class="shopouterframenewshop"><!--店舗サムネイルデザイン外枠-->


					<!--ショップヘッダー部分-->
					<div class="shopinformationtopnewshop">
						<h3 class="shopinfocatchcopyrank">キャッチコピー。店舗側で登録可能で文字数は最大で二十六</h3><!--26文字-->


						<div class="shopinformationbookmark"><a href="about.html"><img src="/kanto/search/img/bookmark.png" onmouseover="this.src='/kanto/search/img/bookmarkhava.png'" onmouseout="this.src='/kanto/search/img/bookmark.png'" /></a></div><!--ブックマークボタン-->
						<div class="shopinformationmail"><a href="shopfrom.html" target="_blank"><img src="/kanto/search/img/mailfroms.png" onmouseover="this.src='/kanto/search/img/mailfromshava.png'" onmouseout="this.src='/kanto/search/img/mailfroms.png'" /></a></div><!--メールボタン-->
						<div class="frotclear"><!--フロートクリア--></div>


					</div><!--ショップヘッダー部分-->


					<!--ショップ左部分-->
					<div class="shopinformationleftrank">
						<div class="shopimage120newshop"><a href="" target="_blank" class="drop"><img src="/kanto/shop/img/test/sanple1.jpg" alt="" /></a></div><!--ショップイメージ画像-->
						<div class="frotclearnewshop"></div><!--フロートクリア-->


					</div><!--ショップ左部分-->	




					<!--ショップ右部分-->
					<div class="shopinformationrightrank">

						<div class="shoptitlebackground"><!--ショップの名前部分背景デザイン-->
							<div class="shoptitlerank"><a href="" target="_blank">ただアナタに逢いたくて店名最大で二十文字</a></div><!--ショップの名前　11文字まで-->
						</div><!--ショップの名前部分背景デザイン-->

						<div class="shoprankframe"><!--星、口コミ数、点数の外枠-->
							<!--評価☆-->
							<div class="hosi"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"></div>
							<!--評価点数-->
							<div class="scor">【-.--点】</div>
							<!--投稿口コミ数-->
							<div class="kutikazu"><i class="fa fa-comment-o"></i> 投稿口コミ数<span class="talk">0<!--お店の口コミ数が表示--></SPAN>件</div>
							<!--営業ページをみる-->
							<div class="shophomepage"><i class="fa fa-desktop"></i> <a href="" target="_blank">営業ページを見る</a></div>
						</div><!--星、口コミ数、点数の外枠-->




						<div class="shopstatusframe"><!--エリア、業種、営業時間の外枠-->
							<dl>
							<dd><i class="fa fa-globe"></i> エリア:東京都/池袋 巣鴨 大塚</dd><!--エリア-->
							<dd><i class="fa fa-cube"></i> 業種:デリバリーヘルス</dd><!--業種-->
							<dd><i class="fa fa-clock-o"></i> 営業時間:10:00-24:00</dd><!--営業時間-->
							</dl>
						</div><!--エリア、業種、営業時間の外枠-->


					</div><!--ショップ右部分-->



					<!--ショップフッター部分外枠　最新口コミ-->
					<div class="shopinformationbottom">
					</div><!--ショップフッター部分-->


				</div>
<!--店舗デザインここまで--------------------------------------------------------------------------------------------->




<!--店舗サムネイルデザイン--------------------------------------------------------------------------------------------------------->

				<div class="shopday">【08月17日登録】</div>
					<div class="shopouterframenewshop"><!--店舗サムネイルデザイン外枠-->


					<!--ショップヘッダー部分-->
					<div class="shopinformationtopnewshop">
						<h3 class="shopinfocatchcopyrank">キャッチコピー。店舗側で登録可能で文字数は最大で二十六</h3><!--26文字-->


						<div class="shopinformationbookmark"><a href="about.html"><img src="/kanto/search/img/bookmark.png" onmouseover="this.src='/kanto/search/img/bookmarkhava.png'" onmouseout="this.src='/kanto/search/img/bookmark.png'" /></a></div><!--ブックマークボタン-->
						<div class="shopinformationmail"><a href="shopfrom.html" target="_blank"><img src="/kanto/search/img/mailfroms.png" onmouseover="this.src='/kanto/search/img/mailfromshava.png'" onmouseout="this.src='/kanto/search/img/mailfroms.png'" /></a></div><!--メールボタン-->
						<div class="frotclear"><!--フロートクリア--></div>


					</div><!--ショップヘッダー部分-->


					<!--ショップ左部分-->
					<div class="shopinformationleftrank">
						<div class="shopimage120newshop"><a href="" target="_blank" class="drop"><img src="/kanto/shop/img/test/sanple1.jpg" alt="" /></a></div><!--ショップイメージ画像-->
						<div class="frotclearnewshop"></div><!--フロートクリア-->


					</div><!--ショップ左部分-->	




					<!--ショップ右部分-->
					<div class="shopinformationrightrank">

						<div class="shoptitlebackground"><!--ショップの名前部分背景デザイン-->
							<div class="shoptitlerank"><a href="" target="_blank">ただアナタに逢いたくて店名最大で二十文字</a></div><!--ショップの名前　11文字まで-->
						</div><!--ショップの名前部分背景デザイン-->

						<div class="shoprankframe"><!--星、口コミ数、点数の外枠-->
							<!--評価☆-->
							<div class="hosi"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"></div>
							<!--評価点数-->
							<div class="scor">【-.--点】</div>
							<!--投稿口コミ数-->
							<div class="kutikazu"><i class="fa fa-comment-o"></i> 投稿口コミ数<span class="talk">0<!--お店の口コミ数が表示--></SPAN>件</div>
							<!--営業ページをみる-->
							<div class="shophomepage"><i class="fa fa-desktop"></i> <a href="" target="_blank">営業ページを見る</a></div>
						</div><!--星、口コミ数、点数の外枠-->




						<div class="shopstatusframe"><!--エリア、業種、営業時間の外枠-->
							<dl>
							<dd><i class="fa fa-globe"></i> エリア:東京都/池袋 巣鴨 大塚</dd><!--エリア-->
							<dd><i class="fa fa-cube"></i> 業種:デリバリーヘルス</dd><!--業種-->
							<dd><i class="fa fa-clock-o"></i> 営業時間:10:00-24:00</dd><!--営業時間-->
							</dl>
						</div><!--エリア、業種、営業時間の外枠-->


					</div><!--ショップ右部分-->



					<!--ショップフッター部分外枠　最新口コミ-->
					<div class="shopinformationbottom">
					</div><!--ショップフッター部分-->


				</div>
<!--店舗デザインここまで--------------------------------------------------------------------------------------------->





<!--店舗サムネイルデザイン--------------------------------------------------------------------------------------------------------->

				<div class="shopday">【08月17日登録】</div>
					<div class="shopouterframenewshop"><!--店舗サムネイルデザイン外枠-->


					<!--ショップヘッダー部分-->
					<div class="shopinformationtopnewshop">
						<h3 class="shopinfocatchcopyrank">キャッチコピー。店舗側で登録可能で文字数は最大で二十六</h3><!--26文字-->


						<div class="shopinformationbookmark"><a href="about.html"><img src="/kanto/search/img/bookmark.png" onmouseover="this.src='/kanto/search/img/bookmarkhava.png'" onmouseout="this.src='/kanto/search/img/bookmark.png'" /></a></div><!--ブックマークボタン-->
						<div class="shopinformationmail"><a href="shopfrom.html" target="_blank"><img src="/kanto/search/img/mailfroms.png" onmouseover="this.src='/kanto/search/img/mailfromshava.png'" onmouseout="this.src='/kanto/search/img/mailfroms.png'" /></a></div><!--メールボタン-->
						<div class="frotclear"><!--フロートクリア--></div>


					</div><!--ショップヘッダー部分-->


					<!--ショップ左部分-->
					<div class="shopinformationleftrank">
						<div class="shopimage120newshop"><a href="" target="_blank" class="drop"><img src="/kanto/shop/img/test/sanple1.jpg" alt="" /></a></div><!--ショップイメージ画像-->
						<div class="frotclearnewshop"></div><!--フロートクリア-->


					</div><!--ショップ左部分-->	




					<!--ショップ右部分-->
					<div class="shopinformationrightrank">

						<div class="shoptitlebackground"><!--ショップの名前部分背景デザイン-->
							<div class="shoptitlerank"><a href="" target="_blank">ただアナタに逢いたくて店名最大で二十文字</a></div><!--ショップの名前　11文字まで-->
						</div><!--ショップの名前部分背景デザイン-->

						<div class="shoprankframe"><!--星、口コミ数、点数の外枠-->
							<!--評価☆-->
							<div class="hosi"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"></div>
							<!--評価点数-->
							<div class="scor">【-.--点】</div>
							<!--投稿口コミ数-->
							<div class="kutikazu"><i class="fa fa-comment-o"></i> 投稿口コミ数<span class="talk">0<!--お店の口コミ数が表示--></SPAN>件</div>
							<!--営業ページをみる-->
							<div class="shophomepage"><i class="fa fa-desktop"></i> <a href="" target="_blank">営業ページを見る</a></div>
						</div><!--星、口コミ数、点数の外枠-->




						<div class="shopstatusframe"><!--エリア、業種、営業時間の外枠-->
							<dl>
							<dd><i class="fa fa-globe"></i> エリア:東京都/池袋 巣鴨 大塚</dd><!--エリア-->
							<dd><i class="fa fa-cube"></i> 業種:デリバリーヘルス</dd><!--業種-->
							<dd><i class="fa fa-clock-o"></i> 営業時間:10:00-24:00</dd><!--営業時間-->
							</dl>
						</div><!--エリア、業種、営業時間の外枠-->


					</div><!--ショップ右部分-->



					<!--ショップフッター部分外枠　最新口コミ-->
					<div class="shopinformationbottom">
					</div><!--ショップフッター部分-->


				</div>
<!--店舗デザインここまで--------------------------------------------------------------------------------------------->

<!--店舗サムネイルデザイン--------------------------------------------------------------------------------------------------------->

				<div class="shopday">【08月17日登録】</div>
					<div class="shopouterframenewshop"><!--店舗サムネイルデザイン外枠-->


					<!--ショップヘッダー部分-->
					<div class="shopinformationtopnewshop">
						<h3 class="shopinfocatchcopyrank">キャッチコピー。店舗側で登録可能で文字数は最大で二十六</h3><!--26文字-->


						<div class="shopinformationbookmark"><a href="about.html"><img src="/kanto/search/img/bookmark.png" onmouseover="this.src='/kanto/search/img/bookmarkhava.png'" onmouseout="this.src='/kanto/search/img/bookmark.png'" /></a></div><!--ブックマークボタン-->
						<div class="shopinformationmail"><a href="shopfrom.html" target="_blank"><img src="/kanto/search/img/mailfroms.png" onmouseover="this.src='/kanto/search/img/mailfromshava.png'" onmouseout="this.src='/kanto/search/img/mailfroms.png'" /></a></div><!--メールボタン-->
						<div class="frotclear"><!--フロートクリア--></div>


					</div><!--ショップヘッダー部分-->


					<!--ショップ左部分-->
					<div class="shopinformationleftrank">
						<div class="shopimage120newshop"><a href="" target="_blank" class="drop"><img src="/kanto/shop/img/test/sanple1.jpg" alt="" /></a></div><!--ショップイメージ画像-->
						<div class="frotclearnewshop"></div><!--フロートクリア-->


					</div><!--ショップ左部分-->	




					<!--ショップ右部分-->
					<div class="shopinformationrightrank">

						<div class="shoptitlebackground"><!--ショップの名前部分背景デザイン-->
							<div class="shoptitlerank"><a href="" target="_blank">ただアナタに逢いたくて店名最大で二十文字</a></div><!--ショップの名前　11文字まで-->
						</div><!--ショップの名前部分背景デザイン-->

						<div class="shoprankframe"><!--星、口コミ数、点数の外枠-->
							<!--評価☆-->
							<div class="hosi"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"><img src="/kanto/common/img/hosi3.png"></div>
							<!--評価点数-->
							<div class="scor">【-.--点】</div>
							<!--投稿口コミ数-->
							<div class="kutikazu"><i class="fa fa-comment-o"></i> 投稿口コミ数<span class="talk">0<!--お店の口コミ数が表示--></SPAN>件</div>
							<!--営業ページをみる-->
							<div class="shophomepage"><i class="fa fa-desktop"></i> <a href="" target="_blank">営業ページを見る</a></div>
						</div><!--星、口コミ数、点数の外枠-->




						<div class="shopstatusframe"><!--エリア、業種、営業時間の外枠-->
							<dl>
							<dd><i class="fa fa-globe"></i> エリア:東京都/池袋 巣鴨 大塚</dd><!--エリア-->
							<dd><i class="fa fa-cube"></i> 業種:デリバリーヘルス</dd><!--業種-->
							<dd><i class="fa fa-clock-o"></i> 営業時間:10:00-24:00</dd><!--営業時間-->
							</dl>
						</div><!--エリア、業種、営業時間の外枠-->


					</div><!--ショップ右部分-->



					<!--ショップフッター部分外枠　最新口コミ-->
					<div class="shopinformationbottom">
					</div><!--ショップフッター部分-->


				</div>
<!--店舗デザインここまで--------------------------------------------------------------------------------------------->





<!------------------------------------1ページ10店舗まで表示------------------------------------->

			</div>



			<div class="anderjob">

				<div id="nextcaunt">
					<ul>
					<li><a href="#">最初</a></li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">最後</a></li>
					</ul> 
				</div>

			</div>

		</div><!--main-->




		<div id="sub">

		<!--検索-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
		<!--検索-->

		<!--PR店舗、ページ表示の度に有料店舗から3店舗ランダムで表示。-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prshop.php"); ?>
		<!--PR店舗、ページ表示の度に有料店舗から3店舗ランダムで表示。-->

		<!--検索リスト-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
		<!--検索リスト-->

		<!--アフィリエイト、管理ページから画像とリンク先の設定可能280×250-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner.php"); ?>
		<!--アフィリエイト、管理ページから画像とリンク先の設定可能280×250-->

		<!--検索-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
		<!--検索-->


		<!--広告部分その2　280×250-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner2.php"); ?>
		<!--広告部分その2　280×250-->

		<!--編集部からのお知らせ-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/info.php"); ?>
		<!--編集部からのお知らせ-


		<!--お役立ちツール ノープログラム-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/tool.php"); ?>
		<!--お役立ちツール-->


		</div>
<!--/sub-->



	</div>
<!--main boxコンテンツの外枠-->


<!--ページトップ-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/footer/page_top.php"); ?>
	</div><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
<!--contentsin-->



<!--footer-->
	<!--フッター-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
	<!--フッター-->
<!--/footer-->






</div><!--container-->
</body>
</html>

