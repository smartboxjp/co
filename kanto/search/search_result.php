<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonContents.php";
	$common_contents = new CommonContents();
	
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_cate_area = new CommonCateArea();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateJob.php";
	$common_cate_job = new CommonCateJob();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonVoice.php";
	$common_voice = new CommonVoice();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
		
	$arr_voice_ranking = $common_voice ->Fn_voice_ranking();
	
	//エリア
	$arr_area = $common_cate_area->Fn_cate_area_list();
	if(!is_null($arr_area))
	{
		foreach($arr_area as $arr_key=>$arr_value)
		{
			$arr_cate_area_s_id[$arr_value["cate_area_s_id"]] = $arr_value["cate_area_l_title"]."/".$arr_value["cate_area_s_title"];
		}
	}
	
	//職種
	$arr_job = $common_cate_job->Fn_db_cate_job_all();
	if(!is_null($arr_job))
	{
		foreach($arr_job as $arr_key=>$arr_value)
		{
			$arr_cate_job_id[$arr_value["cate_job_id"]] = $arr_value["cate_job_title"];
		}
	}
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}
	
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>
<!--ページ専用のCSS-->
<link href="/kanto/search/css/searchresult.css" rel="stylesheet" type="text/css" />

<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script type="text/javascript">
	$(function() {
		$('input:checkbox').click(function() {
			//勤務地の大分類
			if($(this).attr("id").substr(0, 2) == "ll")
			{
				if ($(this).is(':checked')) {
					$('.'+$(this).attr("id")).prop('checked',true);
				} else {
					$('.'+$(this).attr("id")).prop('checked',false);
				}
			}
			
			//勤務地の大分類
			if($(this).attr("class").substr(0, 2) == "ll")
			{
				//alert($(this).attr("class"));
				//alert($('.'+$(this).attr("class")+':checked').length );
				if($('.'+$(this).attr("class")).length==$('.'+$(this).attr("class")+':checked').length) {
					$('#'+$(this).attr("class")).prop('checked',true);
				} else {
					$('#'+$(this).attr("class")).prop('checked',false);
				}
			}
		});
	});
	
//-->
</script>
</head>
<body>

<div id="container">

<!--グローバルナビゲーション-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?><!--ナビゲーション-->
	<!--グローバルナビゲーション-->




	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->


		<!--ログインナビゲーション-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?><!--ログインナビゲーション-->	
		<!--ログインナビゲーション-->



		<div id="mainbox"><!--コンテンツの外枠-->



			<div id="main" class="pt6"><!--コンテンツ左680幅-->
			<!--ランキング3種類、選択中のランキング部分のボタンは～2.jpgのマウスオーバ時の画像を使用。-->


			<!--パンくず-->
				<ul id="pan">
				<li><a href="index.html">トップページ</a></li>
				<li><a href="index.html">*****</a></li>
				<li><a href="index.html">*****</a></li>
				<li>*****</li>
				</ul>
			<!--パンくず-->




				<h2 class="text">現在選択中の項目</h2>

					<table class="table_01">
						<tr>
							<th>選択中エリア市町村</th>
							<td>
              <?
							$str_aera_s = "";
              if(is_null($as)) {
								echo "指定なし";
							} else {
								foreach($as as $arr_key=>$arr_value)
								{
									$str_aera_s .= $arr_cate_area_s_id[$arr_value].",";
								}
							}
							echo substr($str_aera_s, 0, strlen($str_aera_s)-1);
							?>
              </td>
						</tr>

						<tr>
							<th>選択中業種</th>
							<td>
              <?
							$str_job = "";
              if(is_null($job)) {
								echo "指定なし";
							} else {
								foreach($job as $arr_key=>$arr_value)
								{
									$str_job .= $arr_cate_job_id[$arr_value].",";
								}
							}
							echo substr($str_job, 0, strlen($str_job)-1);
							?>
              </td>
						</tr>

						<tr>
							<th>口コミ数が○○以上</th>
							<td><? if($vc>0) { echo $vc."件以上";} else { echo "指定なし";}?></td>
						</tr>

						<tr>
							<th>平均評価が○○以上</th>
							<td><? if($ep>0) { echo $ep."点以上";} else { echo "指定なし";}?></td>
						</tr>

						<tr>
							<th>さらにPR店のみに絞る</th>
							<td><? if($s_pickup=="0") { echo "絞らない";} elseif($s_pickup=="1") { echo "絞る";} else { echo "指定なし";}?></td>
						</tr>

					</table>


<?

	$view_count=10;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$arr_where = array();
	$arr_where["flag_open"] = 1;
	$var = "s_keyword";
	if($$var != "")
	{
		$arr_where[$var] = $$var;
	}
	
	$var = "vc";
	if($$var != "")
	{
		$arr_where["voice_count"] = $$var;
	}
	
	$var = "ep";
	if($$var != "")
	{
		$arr_where[$var] = $$var;
	}
	
	$var = "s_pickup";
	if($$var != "")
	{
		if($$var=="1") {
			$arr_where["shop_view_level"] = "30";
		} elseif($$var=="0") {
			$arr_where["shop_view_level"] = "99";
		}
	}
	
	//エリア
	$arr_where_area_s_id = array();
	if(!is_null($_GET["as"])) {
		foreach($_GET["as"] as $key=>$value) {
			$arr_where_area_s_id[] = $value;
		}
	}
	
	//職種
	$arr_where_job_id = array();
	if(!is_null($_GET["job"])) {
		foreach($_GET["job"] as $key=>$value) {
			$arr_where_job_id[] = $value;
		}
	}
	
	
	//合計
	$all_count = 0;
	$arr_shop_all = $common_shop->Fn_shop_count_index($arr_where, null, $arr_where_area_s_id, $arr_where_job_id);
	$all_count = $arr_shop_all[0]["all_count"];
	
	//リスト表示
	$arr_etc["order_name"] = $order_name;
	$arr_etc["order"] = $order;
	$arr_etc["offset"] = $offset;
	$arr_etc["view_count"] = $view_count;

	$arr_data = array("shop_id", "shop_pw", "shop_name", "cate_area_s_id", "cate_job_id", "open_time_from", "open_time_to");
	$arr_data = array_merge($arr_data, array("catchcopy", "shop_view_level", "flag_open", "evaluation", "shop_thumbnail"));
	$arr_data = array_merge($arr_data, array("regi_date", "up_date"));
	
	$arr_shop_list = $common_shop -> Fn_shop_list_index ($arr_data, $arr_where, null, $arr_etc, $arr_where_area_s_id, $arr_where_job_id);
?>
					<div class="hed"><!--ヘッド外枠-->

						<div class="searchresult"><!--結果店舗-->
							検索結果：<span class="searchdeta"><? echo $all_count;?></span>店舗
						</div>

						<div class="anderjob">
							<div id="nextcaunt">
								<?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
							</div>
						</div>


						<div class="frotclear"><!--フロートクリア-->
							<img src="/kanto/search/img/shoptitlelinelong.gif"/>
						</div>


					</div><!--ヘッド外枠-->


<?
		$query_order = "";
		foreach($_GET as $key => $value){ 
			if($key!="order_name" && $key!="order")
			{
				if(is_array($value))
				{
					foreach($_GET[$key] as $key_arr => $value_arr){ 
						$query_order .= "&".$key."[]=".trim($value_arr);
					}
				}
				else
				{
					$query_order .= "&".$key."=".trim($value);
				}
			}
		}
?>
					<div class="tesutosrot">
						<ul class="sort">
						<li>並び替え<I class="fa fa-arrow-right"></I></li>
						<li><a href="<? echo $_SERVER['PHP_SELF'];?>?order_name=voice_regi_date&order=desc<? echo $query_order;?>"><I class="fa fa-comment">口コミ更新順</I></a></li> 
						<li><a href="<? echo $_SERVER['PHP_SELF'];?>?order_name=evaluation&order=desc<? echo $query_order;?>"><I class="fa fa-bar-chart">評価の高い順</I></a></li> 
						<li><a href="<? echo $_SERVER['PHP_SELF'];?>?order_name=voice_count&order=desc<? echo $query_order;?>"><I class="fa fa-comments-o">口コミ投稿の多い順</I></a></li> 
						<li><a href="<? echo $_SERVER['PHP_SELF'];?>?order_name=shop_view_level<? echo $query_order;?>"><I class="fa fa-star">オススメ店上位順</I></a></li> 
						<li class="last"><a href="<? echo $_SERVER['PHP_SELF'];?>"><I class="fa fa-refresh"></I></a></li>
						</ul> 
        
					</div>




<?
	foreach($arr_shop_list as $arr_key=>$arr_value)
	{
		foreach($arr_value as $key=>$value)
		{
			$$key=$value;
		}
		$voice_count = $arr_value["voice_count"];
?>
					<div class="shopouterframe<? if(($common_contents -> Fn_db_shop_view_level($shop_view_level)))	{ echo "extra";}//有料 ?>"><!--店舗サムネイルデザイン外枠-->

<!--ショップヘッダー部分-->
						<div class="shopinformationtop<? if(($common_contents -> Fn_db_shop_view_level($shop_view_level)))	{ echo "extra";}//有料 ?>">
							<h3 class="shopinformationcatchcopy"><? echo $catchcopy;?></h3><!--26文字-->

							<div class="shopinformationbookmark"><a href="about.html"><img src="/kanto/search/img/bookmark.png" onmouseover="this.src='/kanto/search/img/bookmarkhava.png'" onmouseout="this.src='/kanto/search/img/bookmark.png'" /></a></div><!--ブックマークボタン-->
							<div class="shopinformationmail"><a href="shopfrom.html" target="_blank"><img src="/kanto/search/img/mailfroms.png" onmouseover="this.src='/kanto/search/img/mailfromshava.png'" onmouseout="this.src='/kanto/search/img/mailfroms.png'" /></a></div><!--メールボタン-->
							<div class="frotclear"><!--フロートクリア--></div>

						</div><!--ショップヘッダー部分-->

<!--ショップ左部分-->
						<div class="shopinformationleft">
							<div class="shopimage120"><a href="/kanto/shop/?shop_id=<? echo $shop_id;?>">
								<?
                  if($shop_thumbnail!="")
                  {
                    echo "<img src='/".global_shop_dir.$shop_id."/".$shop_thumbnail."'>";
                  }
                  ?>
              </a></div><!--ショップイメージ画像-->
						</div><!--ショップ左部分-->

<!--ショップ右部分-->
						<div class="shopinformationright">

							<div class="shoptitlebackground"><!--ショップの名前部分背景デザイン-->
								<div class="shoptitle"><a href="/kanto/shop/?shop_id=<? echo $shop_id;?>"><? echo $shop_name;?></a></div><!--ショップの名前　11文字まで-->
							</div><!--ショップの名前部分背景デザイン-->

							<div class="shoprankframe"><!--星、口コミ数、点数の外枠-->
							<!--評価☆-->
								<div class="hosi"><img src="/kanto/common/img/star<? echo sprintf('%.1f',$evaluation);?>.png"></div>
                <div class="scor">【<? if($evaluation==0){ echo "-.--";} else { echo sprintf('%.2f',$evaluation);}?>点】</div>
                
							<!--投稿口コミ数-->
								<div class="kutikazu"><i class="fa fa-comment-o"></i> 投稿口コミ数<span class="talk"><? echo $voice_count;?></SPAN>件</div>
							<!--営業ページをみる-->
								<div class="shophomepage"><i class="fa fa-desktop"></i> <a href="index.html" target="_blank">営業ページを見る</a></div>
							</div><!--星、口コミ数、点数の外枠-->

							<div class="shopstatusframe"><!--エリア、業種、営業時間の外枠-->

								<div class="shopstatusframeleft"><!--店舗情報左-->
									<dl>
										<dd><i class="fa fa-globe"></i> エリア:<? echo $arr_cate_area_s_id[$cate_area_s_id];?></dd><!--エリア-->
										<dd><i class="fa fa-cube"></i> 業種:<? echo $arr_cate_job_id[$cate_job_id];?></dd><!--業種-->
										<dd><i class="fa fa-clock-o"></i> 営業時間:<? echo $open_time_from;?><? if($open_time_to!=""){ echo "-".$open_time_to;}?></dd><!--営業時間-->
									</dl>
								</div><!--店舗情報左-->
                
                <? if(($common_contents -> Fn_db_shop_view_level($shop_view_level)))	{ //有料 ?>
								<div class="detail"><!--オーナメント-->
									<img src="/kanto/common/img/pickup3.png">
								</div><!--詳細ボタン-->
                <? } ?>

								<div class="frotclear"><!--フロートクリア-->
								</div>

							</div><!--エリア、業種、営業時間の外枠-->


						</div><!--右枠　shopinformationright-->




<?
	//ショップの基本情報
	$arr_where = array();
	$arr_where["voice_id"] = $voice_id;
	
	$arr_etc = array();
	$arr_etc["offset"]=0;
	$arr_etc["view_count"]=1;
	
	$arr_data = array("voice_id", "shop_name", "member_id", "v.cate_area_s_id", "v.cate_job_id");
	$arr_data = array_merge($arr_data, array("voice_title", "voice_period", "voice_star"));
	$arr_data = array_merge($arr_data, array("voice_comment_1", "voice_comment_2", "voice_comment_3", "ranking", "v.regi_date", "v.up_date"));
	
	$db_result_voice = $common_voice -> Fn_voice_list ($arr_data, $arr_where, null, $arr_etc) ;
	if(!is_null($db_result_voice))
	{
		foreach($db_result_voice as $arr_key=>$arr_value)
		{
			foreach($arr_value as $key=>$value)
			{
				$$key=$value;
			}
		}
?>
						<!--ショップフッター部分外枠　最新口コミ-->
						<div class="shopinformationbottom">
							<div class="shopinformationnewkutikomi">
								<i class="fa fa-comments-o"></i>最新口コミ
							</div><!--フッター左中枠shopinformationnewkutikomi-->


							<div class="shopbottomleft"><!--フッター左中枠　口コミは35文字まで表示。あとは省略-->
								<div class="newkutikomipointwaku">
                <img src="/kanto/common/img/star<? echo sprintf('%.1f',($voice_star/2));?>s.png">
                <span class="newkutikomipoint">【<? echo sprintf('%.2f',($voice_star/2));?>点】</span>
                <span class="newkutikomitime"><? echo substr($regi_date, 0, 10);?>投稿</span></div><!--投稿者の評価と点数表示-->
								<p class="newkutikomibunitibu"><a href=""><? echo $voice_title;?></a></p>
							</div><!--フッター左中枠-->

							<?
              $arr_data = array();
              $arr_data[]="member_id";
              $arr_data[]="nickname";
              $arr_data[]="member_img";
              $return_member = $common_member->Fn_db_member_data ($member_id, $arr_data);
              if(!is_null($return_member)) {
              ?>
							<div class="shopbottomright"><!--フッター右-->
								<!--投稿者プロフィール画像-->
								<div class="plofimg">
                    <div class="plofgazou">
                    <?
                     if($return_member[0]["member_img"]!="") { 
                    ?>
                      <img src="/<? echo global_member_dir.$return_member[0]["member_id"]."/".$return_member[0]["member_img"];?>" width="60" height="60" alt="<? echo $return_member[0]["nickname"];?>" />
                    <? } else { ?>
                      <img src="/img/userimg.png" width="60" height="60" alt="<? echo $return_member[0]["nickname"];?>" />
                    <? } ?>
                      </div>
                    <div class="plofimgname"><? echo $return_member[0]["nickname"];?></div>
								</div>
							</div><!--フッター右部分-->
							<? } ?>


							<div class="frotclear"><!--フロートクリア-->
							</div>

						</div><!--shopinformationnewkutikomi-->

					</div>
<?
	} //db_result_voice
?>
<?
	}
?>





        <div class="hed">
          <div class="searchresult"><!--結果店舗-->
          検索結果：<span class="searchdeta"><? echo $all_count;?></span>店舗
          </div>
          <div class="anderjob">
            <div id="nextcaunt">
              <?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
            </div>
          </div>
			</div>
		</div><!--main-->






			<div id="sub"><!--sub------------------------------------------------------------------------->

			<!--アフィリエイト、管理ページから画像とリンク先の設定可能280×250-->




      <!--検索-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
			<!--検索-->


			<form action="search_result.php" method="get">
<!--再検索エリア-->
				<h3 class="text">検索し直す</h3>
				<div id="lesearch">
					<div class="lesearchtitle">【エリアを選ぶ】</div>
					<ul class="lesearchtext">
					<?
					
					//area count
					$result_cate_area_count = $common_cate_area -> Fn_cate_area_s_count();
					if(!is_null($result_cate_area_count))
					{
						foreach($result_cate_area_count as $key=>$value)
						{
							$arr_cate_area_count[$value["cate_area_s_id"]] = $value["shop_count"];
						}
					}
					
          //area 大
          $arr_where = array();
          $arr_where["flag_open"] = "1"; //公開のみ
          $result_cate_area_l = $common_cate_area -> Fn_cate_area_l_list ($arr_where) ;
					
					foreach($result_cate_area_l as $arr_cate_area_l)
					{
						$cate_area_l_id = $arr_cate_area_l["cate_area_l_id"];
						$cate_area_l_title = $arr_cate_area_l["cate_area_l_title"];
					?>
							
						<li><input type="checkbox" name="al[]" id="ll<? echo $cate_area_l_id;?>" value="<? echo $cate_area_l_id;?>" <? if(!is_null($_GET["al"])){ if(in_array($cate_area_l_id, $_GET["al"])){ echo " checked ";}; } ?>><label for="ll<? echo $cate_area_l_id;?>" class="check"><? echo $cate_area_l_title;?>エリア全て</label></li>
              <?
							//area 小
							$arr_where_s= array();
							$arr_where_s["flag_open"] = "1"; //公開のみ
							$arr_where_s["cate_area_l_id"] = $cate_area_l_id;
							$result_cate_area_s =  $common_cate_area->Fn_cate_area_s_list ($arr_where_s) ;
							foreach($result_cate_area_s as $arr_cate_area_s)
							{
								$cate_area_s_id = $arr_cate_area_s["cate_area_s_id"];
								$cate_area_s_title = $arr_cate_area_s["cate_area_s_title"];
              ?>
							<li><input type="checkbox" name="as[]" value="<? echo $cate_area_s_id;?>" id="area_<? echo $cate_area_s_id;?>" class="ll<? echo $cate_area_l_id;?>" <? if(!is_null($_GET["as"])){ if(in_array($cate_area_s_id, $_GET["as"])){ echo " checked ";}; } ?>><label for="area_<? echo $cate_area_s_id;?>" class="check" ><? echo $cate_area_s_title;?>(<? if($arr_cate_area_count[$cate_area_s_id]=="") { echo "0";} else{ echo $arr_cate_area_count[$cate_area_s_id];}?>)</label></li>
              <?
							}
							?>

						<div class="boxline"><!--仕切り線-->
							<img src="/kanto/search/img/shoptitlelines.gif"/>
						</div>
					<?
					}
					?>



						<div class="lesearchbotan"><!--検索ボタン-->
							<input class="endbtn" id="setDrag" type="submit" value="この条件で絞り込む" />
						</div>

						<div class="boxline"><!--仕切り線-->
							<img src="/kanto/search/img//shoptitlelines.gif"/>
						</div>

<!--再検索業種-->

						<li><input type="checkbox" name="j[]" id="ll_job" <? if(!is_null($_GET["j"])){ echo " checked "; } ?> value="1"><label for="ll_job" class="check"> 業種全て</label></li>
							<?	
							//job count
							$result_cate_job_count = $common_cate_job -> Fn_cate_job_count();
							if(!is_null($result_cate_job_count))
							{
								foreach($result_cate_job_count as $key=>$value)
								{
									$arr_cate_job_count[$value["cate_job_id"]] = $value["shop_count"];
								}
							}
							
              //職種
							$arr_where = array();
							$arr_where["flag_open"] = "1";//公開のみ
							
							$arr_data = array();
							$arr_data[] = "cate_job_id";
							$arr_data[] = "cate_job_title";
									
							$result_cate_job = $common_cate_job -> Fn_db_cate_job_list ($arr_data, $arr_where) ;
              
              foreach($result_cate_job as $arr_cate_job)
              {
                $cate_job_id = $arr_cate_job["cate_job_id"];
                $cate_job_title = $arr_cate_job["cate_job_title"];
              ?>
							<li><input type="checkbox" name="job[]" value="<? echo $cate_job_id;?>" id="job_<? echo $cate_job_id;?>" class="ll_job" <? if(!is_null($job)){ if(in_array($cate_job_id, $job)){ echo " checked ";}}?>><label for="job_<? echo $cate_job_id;?>" class="check" ><? echo $cate_job_title;?>(<? if($arr_cate_job_count[$cate_job_id]=="") { echo "0";} else{ echo $arr_cate_job_count[$cate_job_id];}?>)</label></li>
							<?	
							}
              ?>

						<div class="boxline"><!--仕切り線-->
							<img src="/kanto/search/img/shoptitlelines.gif"/>
						</div>

<!--絞り込み-->
						<div class="lesearchtitle">【口コミ数で絞り込み】</div>
							<li>
            	<? $var = "vc"; $voice_count=1;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>" <? if($$var==$voice_count){ echo " checked ";}?>><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"> <? echo $voice_count;?>件～</label>
            	
							<? $var = "vc"; $voice_count=5;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>" <? if($$var==$voice_count){ echo " checked ";}?>><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"> <? echo $voice_count;?>件～</label>
              
            	<? $var = "vc"; $voice_count=10;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>" <? if($$var==$voice_count){ echo " checked ";}?>><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"> <? echo $voice_count;?>件～</label>
              
            	<? $var = "vc"; $voice_count=14;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>" <? if($$var==$voice_count){ echo " checked ";}?>><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"> <? echo $voice_count;?>件～</label>
              </li>
						<div class="lesearchtitle">【平均点で絞り込み】</div>
							<li>
            	<? $var = "ep"; $voice_count=3;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>" <? if($$var==$voice_count){ echo " checked ";}?>><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"> <? echo $voice_count;?>点～ </label>
            	<? $var = "ep"; $voice_count=3.5;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>" <? if($$var==$voice_count){ echo " checked ";}?>><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"> <? echo $voice_count;?>点～ </label>
            	<? $var = "ep"; $voice_count=4;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>" <? if($$var==$voice_count){ echo " checked ";}?>><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"> <? echo $voice_count;?>点～ </label>
            	<? $var = "ep"; $voice_count=4.5;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>" <? if($$var==$voice_count){ echo " checked ";}?>><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"> <? echo $voice_count;?>点～ </label>
              </li>
						<div class="lesearchtitle">【オススメ店のみ表示】</div>
							<li>
            	<? $var = "s_pickup"; $voice_count=0;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>" <? if($$var==$voice_count && $$var!=""){ echo " checked ";}?>><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"> 絞らない</label>
              
            	<? $var = "s_pickup"; $voice_count=1;?>
							<input type="radio" name="<? echo $var;?>" value="<? echo $voice_count;?>" id="<? echo $var;?><? echo $voice_count;?>" <? if($$var==$voice_count && $$var!=""){ echo " checked ";}?>><label for="<? echo $var;?><? echo $voice_count;?>" class="radio"> 絞る</label>
              </li>

						<div class="lesearchbotan"><!--検索ボタン-->
							<input type="submit" class="endbtn" id="setDrag" value="この条件で絞り込む" />
						</div>


					</ul>
				</div>
        </form>
<!--/form-->



<!--アフィリエイト、管理ページから画像とリンク先の設定可能280×250-->
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner.php"); ?>
<!--アフィリエイト、管理ページから画像とリンク先の設定可能280×250-->


			</div>
<!--/sub------------------------------------------------------->





		</div>
<!--main boxコンテンツの外枠-->


<!--ページトップ-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/footer/page_top.php"); ?>
	</div><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
<!--contentsin-->





<!--footer-->
	<!--フッター-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
	<!--フッター-->
<!--/footer-->



</div><!--container-->
</body>
</html>

