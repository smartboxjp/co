<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonContents.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateJob.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_contents = new CommonContents();
	$common_shop = new CommonShop();
	$common_cate_job = new CommonCateJob();
	$common_cate_area = new CommonCateArea();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonVoice.php";
	$common_voice = new CommonVoice();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	$pankuzu_txt = "応募する";

	foreach($_GET as $key => $value)
	{ 
		$$key = trim($common_dao->db_string_escape($value));
		//$$key = $value;
	}
	if($shop_id=="") { $common_connect -> Fn_javascript_back("正しく入力してください。"); }

	//ショップの基本情報
	$arr_where = array();
	$var = "flag_open";
	$arr_where[$var] = "1"; //公開のみ
	$var = "shop_id";
	$arr_where[$var] = $$var;
	
	$arr_data = array("shop_id", "shop_name", "shop_kana", "cate_area_s_id", "area_detail", "cate_job_id", "evaluation");
	$arr_data = array_merge($arr_data, array("catchcopy", "shop_title", "shop_address", "shop_tel", "job_comment", "job_salary_pre"));
	$arr_data = array_merge($arr_data, array("job_price", "job_salary", "job_style", "job_years_from", "job_years_to"));
	$arr_data = array_merge($arr_data, array("job_time_from", "job_time_to", "job_special", "hp_job", "hp_offical", "penalty", "shop_pr", "job_url"));
	$arr_data = array_merge($arr_data, array("open_time_from", "open_time_to", "main_img", "shop_thumbnail", "pr_comment"));
	$arr_data = array_merge($arr_data, array("shop_view_level", "line_id", "img_1"));
	
	$db_result_shop = $common_shop -> Fn_db_shop_data ($arr_data, $arr_where);
	foreach($db_result_shop as $arr_shop)
	{
		foreach($arr_shop as $key=>$value)
		{
			$$key = $value;
		}
	}
	
	if($shop_name=="")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
?>
<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

<!--ページ専用のCSS-->
<link href="/kanto/shop/css/shopjob.css" rel="stylesheet" type="text/css" />

<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--口コミ投稿ボタン用CSS-->
<link rel="stylesheet" href="/kanto/app/desing/githubbutton/gh-buttons.css">
<!--アプリケーション/フォームデザイン-->
<link href="/kanto/app/form/formoid2/forms2.css" rel="stylesheet" type="text/css" />
<!--アプリケーション/ポップアップ-->
<link rel="stylesheet" href="/kanto/app/pop/lity.min.css">
<script src="/kanto/app/pop/lity.min.js"></script>
<!--アプリケーション/画像拡-->
<link rel="stylesheet" href="/kanto/app/zoom/css/lightbox.css">

<!-- 下へアンカー -->
<script type="text/javascript">
$(function(){
    $('a[href^=#]').click(function(){
        var speed = 500;
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $("html, body").animate({scrollTop:position}, speed, "swing");
        return false;
    });
});
</script>

<script type="text/javascript">
	$(function() {
		$('#i_form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input("i_nickname");
			err_check_count += check_input_email("i_email");
			
			
			if(err_check_count!=0)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#i_form_confirm', "body").submit();
				return true;
			}
		});
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div style='color:#F00'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		function check_input_email($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div style='color:#F00'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str).val()) == false)
			{
				err ="<div style='color:#F00'>メールアドレスは半角英数字でご入力ください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}

		//メールチェック
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}

	});

</script>

<body>

<div id="container">

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<!--ログインナビゲーション-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?><!--ログインナビゲーション-->	
		<!--ログインナビゲーション-->

<!--外枠-->	<div id="mainbox">



			<!--パンくず-->
			<ul id="pan">
				<li><a href="/">トップページ</a></li>
				<li><a href="/kanto/">関東</a></li>
				<li><a href="/kanto/search/">検索一覧</a></li>
				<li>テスト'!"#$%%&'"　応募する</li>
			</ul>
			<!--パンくず-->

			

			<div id="main" class="pt6"><!--コンテンツ左680幅-->


				<div id="shoptop">
        <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/shop_summery.php"); ?>
				</div><!--shoptop-->
				<!--shopメインヘッダーここまで----------------------------------------------------->


				<!--ショップ基本情報-->
				<div class="mainshopdeta">


					<div id="wrapper">
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/middle_navi.php"); ?>
						<div class="tabContents">

							<div class="cntentsubtitle">
							お店にお問合せのメールをしてみませんか？面接は勿論、質問などでも気軽にメールして見てください。<br>
							<span>会員登録をしてるとメールアドレスを非公開でメールを送る事が出来ます。</span>会員登録がまだなら<a href="/kanto/member/entry_1.php">→こちらから</a>
							</div>

							<div class="cntenttitle">お店に質問や面接希望のメールをする</div>

							<div class="shopcntentwaku">

								<div class="mailfrombox">

							<form action="./sub_apply_save.php" enctype="multipart/form-data" class="formoid-default-skyblue" method="post">
									<? $var = "i_shop_id";?>
                  <input name="<? echo $var;?>" type="hidden" value="<? echo $shop_id;?>"/>

								<div class="mailfromboxtop">
									<div class="mailfromboxtitle"><i class="fa fa-envelope-o"></i>必須項目は二つだけでOK</div>

									<div class="element-name">
                  							<?
									if($common_connect->Fn_member_login_check()) { 
										$arr_data = array();
										$arr_data[] = "login_email";
										$arr_data[] = "nickname";
										
										$db_result = $common_member->Fn_session_member_info ($arr_data);
										$i_login_email = $db_result [0]["login_email"];
										$i_nickname = $db_result [0]["nickname"];
									}
									?>
										<label class="title">ニックネーム：</label>
                  								<? $var = "i_nickname";?>
										<span class="nameFirst">
                    								<input name="<? echo $var;?>" id="<? echo $var;?>" type="text" size="8" required placeholder="ニックネーム※必須" value="<? echo $i_nickname;?>" <? if($i_nickname!="") { echo " style=\"display:none;\"";}?>/><? echo $i_nickname;?>
										</span>
											
										<div class="element-email" title="メールアドレス">
											<label class="title">メールアドレス：</label>
											<? $var = "i_email";?>
											<input name="<? echo $var;?>" id="<? echo $var;?>" class="large" type="email" required placeholder="返信用のメールアドレス※必須" value="<? echo $i_login_email;?>" <? if($i_login_email!="") { echo " style=\"display:none;\"";}?>/><? echo $i_login_email;?>
											<label id="err_<?=$var;?>"></label>
										</div>
									</div>

								</div><!-- mailfromboxtop-->





								<div class="mailfromboxbottom">
									<div class="mailfromboxtitle"><i class="fa fa-envelope-o"></i>さらに詳しく教えてください</div>
									<div class="mailfromsps">

										<div class="element-input">
                      <label class="title">お住まいはどこですか？</label>
                      <? $var = "i_address";?>
                      <input name="<? echo $var;?>" id="<? echo $var;?>" class="large" type="text" placeholder="東京都渋谷区" />
                    </div>
									</div>

									<div class="mailfromsps">

										<div class="element-select">
                      <label class="title">年齢はいくつですか？</label>
                      <div class="large">
                      <?
											$var = "i_years";
											$arr_i_years[] = "18歳-20歳";
											$arr_i_years[] = "21歳-23歳";
											$arr_i_years[] = "24歳-26歳";
											$arr_i_years[] = "27歳-30歳";
											$arr_i_years[] = "31歳-34歳";
											$arr_i_years[] = "35歳-37歳";
											$arr_i_years[] = "38歳-40歳";
											$arr_i_years[] = "41歳-43歳";
											$arr_i_years[] = "43歳-45歳";
											$arr_i_years[] = "46歳以上";
											?>
                        <select name="<? echo $var;?>" id="<? echo $var;?>" data-no-selected="おおよその年齢を教えてください" >
                        <option value="">---</option>
                        <?
      foreach($arr_i_years as $value) {
                        ?>
                        <option value="<? echo $value;?>"><? echo $value;?></option>
                        <? } ?>
                        </select>
											</div>
										</div>
									</div>

									<div class="element-radio">
										<label class="title">業界の経験はありますか？</label>
											<? $var = "i_sel";?>
                      <?
										$arr_i_sel[] = "経験無し";
										$arr_i_sel[] = "経験あり";
										
										foreach($arr_i_sel as $value)
										{
										?>
										<div class="column column2"><input name="<? echo $var;?>" id="<? echo $var;?>" type="radio" value="<? echo $value;?>" />
											<span><? echo $value;?></span>
										</div>
                    <span class="clearfix"></span>
                    <?
										}
										?>

										<hr class="formsikiri">
										<div class="element-checkbox">
											<label class="title">
											<div class="mailfromspm">お店選びで重要視してる所は？</div>
											</label>
											<div class="column column2">
												<?
												$var = "i_point";
												$arr_i_point[] = "業種";
												$arr_i_point[] = "お給料単価";
												$arr_i_point[] = "託児所";
												$arr_i_point[] = "寮有り";
												$arr_i_point[] = "罰金雑費なし";
												$arr_i_point[] = "車通勤";
											
												foreach($arr_i_point as $value)
												{
												?>
												<label>
                          <input name="<? echo $var;?>[]" id="<? echo $var;?>" type="checkbox" value="<? echo $value;?>"/ >
                            <span><? echo $value;?></span>
												</label>
												<?
												}
												?>
                    
											</div>

											<span class="clearfix"></span>
											<div class="column column2">
												<?
												$arr_i_point = array();
												$arr_i_point[] = "エリア";
												$arr_i_point[] = "保証制度";
												$arr_i_point[] = "客層";
												$arr_i_point[] = "個室待機";
												$arr_i_point[] = "送迎希望";
												$arr_i_point[] = "前借り";
											
												foreach($arr_i_point as $value)
												{
												?>
												<label>
                          <input name="<? echo $var;?>[]" id="<? echo $var;?>" type="checkbox" value="<? echo $value;?>"/ >
                            <span><? echo $value;?></span>
												</label>
												<?
												}
												?>
											</div>
											<span class="clearfix"></span>
										</div>

										<hr class="formsikiri">

										<div class="element-file">
											<label class="title">あなたの写メを送ってみませんか？</label>
											<label class="large" >
												<div class="button">画像を探す</div>
												<? $var = "img_1";?>
												<input name="<? echo $var;?>" id="<? echo $var;?>" type="file" class="file_input" accept="image/*"/>
												<div class="file_text">No file selected</div>
											</label>
										</div>

										<hr class="formsikiri">

										<div class="element-radio">		
											<label class="title">お問い合わせ内容</label>
											<? $var = "i_exprience";?>
                        <?
												$i_otoi[] = "面接希望";
												$i_otoi[] = "質問したい";
												
												foreach($i_otoi as $value)
												{
											?>
											<div class="column column2"><input name="<? echo $var;?>" id="<? echo $var;?>" type="radio" value="<? echo $value;?>" />
												<span><? echo $value;?></span>
											</div>
                      <span class="clearfix"></span>
                      <?
											}
											?>
                  
										</div>

										<hr class="formsikiri">

										<div class="mailfromsps">
											<div class="element-multiple">
												<label class="title">面接希望日</label>
												<div class="large">
												<?
												$var = "i_interview";
												$arr_i_interview[] = "今日";
												$arr_i_interview[] = "明日";
												$arr_i_interview[] = "明後日";
												$arr_i_interview[] = "3日後以降";
										
												foreach($arr_i_interview as $value)
												{
												?>
												<label>
                          <input name="<? echo $var;?>[]" id="<? echo $var;?>" type="checkbox" value="<? echo $value;?>"/ >
                            <span><? echo $value;?></span>
												</label>
												<?
												}
												?>
												</div>
											</div>

											<hr class="formsikiri">

											<div class="element-textarea">
												<label class="title">質問やご要望など</label>
												<? $var = "i_comment";?>
												<textarea name="<? echo $var;?>" id="<? echo $var;?>" class="medium" name="textarea" cols="20" rows="5" ></textarea>
											</div>
										</div>


										<div class="submit">
											<? $var = "i_form_confirm";?>
                        <input name="<? echo $var;?>" id="<? echo $var;?>" type="submit" value="メールする"/>
										</div>
									</div>
								</div>
							</form>
									<script type="text/javascript" src="/kanto/app/form/formoid2/formoid-default-skyblue.js"></script>
									<!-- Stop Formoid form-->

								</div><!-- mailfromboxbottom-->

							</div><!--shopcntentwaku-->




						</div><!--tabContents-->


					</div><!--wrapper-->
          
          <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/shop_info.php"); ?>


				</div>

			<!--広告スペースメインボックス下 マスター管理から画像のアップ、リンク先の変更可能　ランダム表示 jpg,gif共にアップ可能で枚数は最大5枚まで-->

			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/app/pr/contents_bottom.php"); ?>
			<!--広告スペース-->



		</div><!--<div id="mainbox">-->





			<div id="sub"><!--sub----------------------------------------------------->

				<!--ライン-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/shop_line.php"); ?>
				<!--ライン-->
        
				<!--検索-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
				<!--検索-->

				<!--似た求人-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/similar.php"); ?>
				<!--似た求人-->

				<!--観覧履歴-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/history.php"); ?>
				<!--観覧履歴-->

				<!--広告部分その1　280×250-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner.php"); ?>
				<!--広告部分その1　280×250-->
        
				<!--検索リスト-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
				<!--検索リスト-->

				<!--検索-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
				<!--検索-->
        
				<!--広告部分その2　280×250-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner2.php"); ?>
				<!--広告部分その2　280×250-->

			</div>
<!--/sub------------------------------------------------------->



		</div><!--main box-->


		<p id="pagetop">
			<a href="#container"><i class="fa fa-caret-up" aria-hidden="true"></i></a>
		</p>

	
	</div><!--contentsinコンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->




	
	<!--フッター-->
		<div id="footer">
		<a href="./">『口コミと評価で探せる高収入サイトコソット』</a><br />
		<a href="" target="_blank">Produced by (Team avalanche)</a><br />


		<table class="tafooter">
			<tr>
				<td><a href="/kanto/member/">会員登録/ログイン</a></td><td><a href="/">TOPページへ</a></td><td><a href="./">最新口コミ一覧</a></td><td><a href="./">人気ランキング</a></td><td><a href="./">お仕事検索</a></td><td><a href="./">求人ブログ</a></td><td><a href="./">動画一覧</a></td><td><a href="./">綺麗になるまとめ</a></td>
			</tr>

			<tr>
				<td><a href="./">都道府県で仕事探し</a></td><td><a href="./">東京で仕事探し</a></td><td><a href="./">神奈川で仕事探し</a></td><td><a href="./">千葉でお仕事探し</a></td><td><a href="./">埼玉で仕事探し</a></td><td><a href="./">茨城で仕事探し</a></td><td><a href="./">栃木で仕事探し</a></td><td><a href="./">群馬で仕事探し</a></td>
			</tr>

			<tr>
				<td><a href="./">コソットとは</a></td><td><a href="/kanto/help/inquiry.php">広告掲載について</a></td><td><a href="./">口コミ評価の注意点</a></td><td><a href="./">点数について</a></td><td><a href="./">利用規約</a></td><td><a href="./">運営会社</a></td><td><a href="/kanto/help/inquiry.php">お問合せ</td><td><a href="./">お役立ちリンク</a></td>
			</tr>
		</table>

		<p class="prfut">当サイトは18歳未満の方の利用は出来ません。また掲載されている写真、イラスト、画像、文章等の無断掲載は一切禁止しております。</p>
	</div>	<!--フッター-->



</div><!--container-->


</body>
</html>