<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonContents.php";
	$common_contents = new CommonContents();
	
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}

    if($shop_blog_id!="")
    {
        $arr_data = array("shop_blog_id", "shop_blog_title", "shop_blog_comment");
		$arr_data = array_merge($arr_data, array("shop_name", "shop_kana", "cate_area_s_id", "area_detail", "cate_job_id", "evaluation"));
		$arr_data = array_merge($arr_data, array("catchcopy", "shop_title", "shop_address", "shop_tel", "job_comment", "job_salary_pre"));
		$arr_data = array_merge($arr_data, array("job_price", "job_salary", "job_style", "job_years_from", "job_years_to"));
		$arr_data = array_merge($arr_data, array("job_time_from", "job_time_to", "job_special", "hp_job", "hp_offical", "penalty", "shop_pr", "job_url"));
		$arr_data = array_merge($arr_data, array("open_time_from", "open_time_to", "main_img", "shop_thumbnail", "pr_comment"));
		$arr_data = array_merge($arr_data, array("shop_view_level", "line_id"));
            
        $sql = "SELECT shop_blog_id, b.img_1 as shop_blog_img_1, s.img_1 as img_1, ";
        $sql .= " s.shop_id as shop_id, b.regi_date as regi_date, ";
        foreach($arr_data as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM shop_blog b inner join shop s on b.shop_id=s.shop_id where shop_blog_id='".$shop_blog_id."'";
        $sql .= " and s.flag_open=1 and b.flag_open=1 ";
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            foreach($arr_data as $val)
            {
                $$val = $db_result[0][$val];
            }
        }

        $shop_blog_comment = htmlspecialchars_decode($shop_blog_comment);
        $shop_blog_img_1 = $db_result[0]["shop_blog_img_1"];
        $img_1 = $db_result[0]["img_1"];
        $shop_blog_id = $db_result[0]["shop_blog_id"];
        $shop_id = $db_result[0]["shop_id"];
        $regi_date = $db_result[0]["regi_date"];
    }


	if($shop_name=="")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
?>
<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

<!--ページ専用のCSS-->
<link href="/kanto/shop/css/shopjob.css" rel="stylesheet" type="text/css" />

<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--口コミ投稿ボタン用CSS-->
<link rel="stylesheet" href="/kanto/app/desing/githubbutton/gh-buttons.css">

<!--↓アプリケーション-->
<script src="/kanto/app/jquery.min.js" type="text/javascript" ></script>

</head>



<body>

<div id="container">

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<!--ログインナビゲーション-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?><!--ログインナビゲーション-->	
		<!--ログインナビゲーション-->



		<div id="mainbox">


			<div id="main" class="pt6"><!--コンテンツ左680幅-->

				<!--パンくず-->
				<ul id="pan">
					<li><a href="/">トップページ</a></li>
					<li><a href="index.html">*****</a></li>
					<li><a href="index.html">*****</a></li>
					<li>*****</li>
				</ul>



			<!--SHOPメインヘッダー部分-->




				<div id="shoptop">
        

				<!--キャッチコピー部分　お店側が26文字でお店をPR出来る-->

					<div class="catchcopy">
						<div class="ankrdownleft"><i class="fa fa-hand-o-right">ポイント</i><? echo $catchcopy;?></div>
						<div class="floatclear"></div>
					</div>


					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/shop_summery.php"); ?>



				</div><!--shoptop-->
				<!--shopメインヘッダーここまで----------------------------------------------------->



				<div class="blogcontents">

					<h1><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <? echo $shop_blog_title;?></h1>
<?
	$ranking = "";
	$sql = "select ranking from shop_blog_ranking ";
	$sql .= " where regi_date=(SELECT MAX( regi_date ) AS max_yyyymmdd ";
	$sql .= " FROM shop_blog_ranking ";
	$sql .= " WHERE regi_date <=  '".date("Y/m/d")."') and shop_blog_id='".$shop_blog_id."' ";

	$arr_max_yyyymmdd = $common_dao->db_query_bind($sql);
	if($arr_max_yyyymmdd)
	{
		$ranking = $arr_max_yyyymmdd[0]["ranking"];
	}
	if($ranking!="")
	{
?>
					<p class="right-arrow"><i class="fa fa-trophy" aria-hidden="true"></i> ランクインブログ</p>
<?
	}
?>
					<div class="blogtime"><? echo substr($regi_date, 0, 10);?>投稿</div>

<? if($shop_blog_img_1!="") { ?>
					<div class="blogcontentstopimg">
						<img src="<? echo "/".global_shop_blog_dir.$shop_blog_id."/".$shop_blog_img_1;?>">
					</div>
<? } ?>


					<div class="blogcontentsbun">
					<? echo $shop_blog_comment;?>


							<div class="line_icon">
								<div class="line-it-button" style="display: none;" data-type="share-a" data-lang="ja" ></div>
								<script src="//scdn.line-apps.com/n/line_it/thirdparty/loader.min.js" async="async" defer="defer" ></script>
							</div>
								
							<ul class="blogmeun">
									<li><a href="mailto:info@infoinfo?subject=%E3%83%96%E3%83%AD%E3%82%B0%E5%89%8A%E9%99%A4%E4%BE%9D%E9%A0%BC&amp;body=ID4354%81%A6ID%94%D4%8D%86%82%F0%8F%C1%82%B3%82%B8%82%C9%8D%ED%8F%9C%88%CB%97%8A%82%CC%97%9D%97R%82%F0%8BL%8D%DA%82%B5%82%C4%83%81%81%5B%83%8B%82%F0%91%97%82%C1%82%C4%89%BA%82%B3%82%A2"><i class="fa fa-times-circle-o"> このブログを通報</i></a></li>
									<li><a href=""><i class="fa fa-desktop"> お店の求人ページ</i></a></li>
									<li><a href="/kanto/shop/apply.php?shop_id=<? echo $shop_id;?>"><i class="fa fa-envelope-o"> お店にメール</i></a></li>
							</ul>
					</div>

					<div class="blogcontentsbottom">
						<div id="blognationbotan">
							<ul>
<?
	$max_shop_blog_id = "";
	$sql = "select max(shop_blog_id) as max_shop_blog_id ";
	$sql .= " FROM shop_blog ";
	$sql .= " WHERE shop_id='".$shop_id."' and shop_blog_id<'".$shop_blog_id."' ";

	$arr_blog = $common_dao->db_query_bind($sql);
	if($arr_blog)
	{
		$max_shop_blog_id = $arr_blog[0]["max_shop_blog_id"];
	}
	if($max_shop_blog_id!="")
	{
?>
							<li><a href="blog_details.php?shop_blog_id=<? echo $max_shop_blog_id;?>"><i class="fa fa-chevron-left"></i>古いブログを見る</a></li>
<?
	}
	else
	{
?>
							<li><i class="fa fa-chevron-left"></i>古いブログを見る</li>
<?
	}
?>
							<li><a href="./?shop_id=<? echo $shop_id;?>"><i class="fa fa-undo"></i>お店ページに戻る</a></li>
<?
	$min_shop_blog_id = "";
	$sql = "select min(shop_blog_id) as min_shop_blog_id ";
	$sql .= " FROM shop_blog ";
	$sql .= " WHERE shop_id='".$shop_id."' and shop_blog_id>'".$shop_blog_id."' ";

	$arr_blog = $common_dao->db_query_bind($sql);
	if($arr_blog)
	{
		$min_shop_blog_id = $arr_blog[0]["min_shop_blog_id"];
	}

	if($min_shop_blog_id!="")
	{
?>
							<li><a href="blog_details.php?shop_blog_id=<? echo $min_shop_blog_id;?>">新しいブログを見る<i class="fa fa-chevron-right"></i></a></li>
<?
	}
	else
	{
?>
							<li><i class="fa fa-chevron-right"></i>新しいブログを見る</li>
<?
	}
?>
							</ul> 
						</div>
						<div class="floatclear"></div>
					</div>



				</div><!--blogcontents-->


<!--広告スペースメインボックス下 マスター管理から画像のアップ、リンク先の変更可能　ランダム表示 jpg,gif共にアップ可能で枚数は最大5枚まで-->

			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/app/pr/contents_bottom.php"); ?>
<!--広告スペース-->


			</div>


			<div id="sub"><!--sub----------------------------------------------------->


				<!--検索-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
				<!--検索-->

				<!--似た求人-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/similar.php"); ?>
				<!--似た求人-->

				<!--観覧履歴-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/history.php"); ?>
				<!--観覧履歴-->

				<!--広告部分その1　280×250-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner.php"); ?>
				<!--広告部分その1　280×250-->

				<!--検索リスト-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
				<!--検索リスト-->

				<!--検索-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
				<!--検索-->

				<!--広告部分その2　280×250-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner2.php"); ?>
				<!--広告部分その2　280×250-->

			</div>
<!--/sub------------------------------------------------------->

		</div><!--main box-->



		<!--ページトップ-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/footer/page_top.php"); ?>
	
	</div><!--contentsinコンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->




	
	<!--フッター-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
	<!--フッター-->



</div><!--container-->

</body>
</html>