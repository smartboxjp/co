
<!--※管理側で編集可能　表示、非表示を管理画面で出します。 調査結果情報-->
					<div class="datatable2">
						<table class="jobdata2">
							<tr>
								<td colspan="2" style="color:#e80f57; text-align:center; font-size: 14px;">※このお店は当社で調査を行いました。赤枠の中は調査をして得た情報を掲載してます。</td>
							</tr>

							<tr>
								<th>項目1</th>
								<td>内容</td>
							</tr>

							<tr>
								<th>項目2</th>
								<td>内容</td>
							</tr>

							<tr>
								<th>項目3</th>
								<td>内容</td>
							</tr>

							<tr>
								<th>項目4</th>
								<td>内容</td>
							</tr>


							<tr>
								<th class="t_top2" width="180">お店のお給料</th>
								<td class="t_top2">60分5800円</td>
							</tr>

							<tr>
								<th>罰金制度</th>
								<td>遅刻1500円</td>
							</tr>


							<tr>
								<th>待機室</th>
								<td>集団待機○個室待機○車待機○自宅待機○外待機○</td>
							</tr>

							<tr>
								<th>お給料の昇給</th>
								<td>一律制、ランク制、昇給制</td>
							</tr>

							<tr>
								<th>退店時の写真の削除</th>
								<td>OK</td>
							</tr>

							<tr>
								<th class="t_bottom2" width="180">系列、他の同店が運営してるお店</th>
								<td class="t_bottom2">渋谷OLクラブ</td>
							</tr>

						</table>
					</div>

<!--※管理側で編集可能　表示、非表示を管理画面で出します。 調査結果情報-->



					<div id="ankerbottom" class="datatable">
            お店情報
            <table class="jobdata">
              <tr>
                <th class="t_top" width="180">業種</th>
                <td class="t_top">
                <?php
                
                //職種
                $result_cate_job = $common_cate_job -> Fn_db_cate_job($cate_job_id);
                
                foreach($result_cate_job as $arr_cate_job)
                {
									$cate_job_id = $arr_cate_job["cate_job_id"];
									$cate_job_title = $arr_cate_job["cate_job_title"];
                }
                
                echo $cate_job_title;
                ?>
                </td>
              </tr>
              
              <?php if($open_time_from!="" || $open_time_to!="") { ?>
              <tr>
                <th>営業時間</th>
                <td><?php echo $open_time_from;?><?php if($open_time_to!="") { echo " - ".$open_time_to;}?></td>
              </tr>
              <?php } ?>
              
              <?php if($shop_address!="") { ?>
              <tr>
                <th>詳しい住所（最寄駅）</th>
                <td><?php echo $shop_address;?></td>
              </tr>
              <?php } ?>
              
              <?php if($shop_tel!="" || $open_time_to!="") { ?>
              <tr>
                <th>直接の連絡</th>
                <td>
									<?php if($shop_tel!="") { echo "TEL&nbsp;".$shop_tel."&nbsp;&nbsp;";} ?>
                  <?php if($email_user!="") { echo "Mail&nbsp;".$email_user;} ?>
                </td>
              </tr>
              <?php } ?>
              
              </table>
              </div>
              
              <div class="datatable">
              募集情報
              <table class="jobdata">
								<?php if($job_comment!="") { ?>
                <tr>
                  <th class="t_top" width="180">お仕事の内容</th>
                  <td class="t_top"><?php echo nl2br($job_comment);?></td>
                </tr>
                <?php } ?>
                
                <?php if($job_salary_pre!="" && $job_salary!="") { ?>
                <tr>
                  <th>お給料体系</th>
                  <td><?php echo $job_salary_pre;?>　<?php echo $job_salary;?></td>
                </tr>
                <?php } ?>
                
                <?php if($job_style!="") { ?>
                <tr>
                  <th>営業形態</th>
                  <td><?php echo $job_style;?></td>
                </tr>
                <?php } ?>
                
                <?php if($job_years_from!="" || $job_years_to!="") { ?>
                <tr>
                  <th>募集資格年齢</th>
                  <td><?php echo $job_years_from;?> <?php echo $job_years_to;?></td>
                </tr>
                <?php } ?>
                
                <?php if($open_time_from!="" || $open_time_to!="") { ?>
                <tr>
                  <th>特に募集したい時間帯</th>
                  <td><?php echo $open_time_from;?> <?php echo $open_time_to;?></td>
                </tr>
                <?php } ?>
                
                <?php if($job_special!="") { ?>
                <tr>
                  <th>待遇</th>
                  <td><?php echo nl2br($job_special);?></td>
                </tr>
                <?php } ?>
              </table>
            </div>
            
            <div class="datatable">
              <table class="jobdata">
                  <tr>
                    <th class="t_top" width="180">営業用オフィシャルページ</th>
                    <td class="t_top"><?php if($hp_offical=="") { echo "なし"; } else { ?><a href="<?php echo $hp_offical;?>" target="_blank">→クリック←</a><?php } ?></td>
                  </tr>
                  
                  <tr>
                    <th>公式求人ページ</th>
                    <td><?php if($hp_offical=="") { echo "なし"; } else { ?><a href="<?php echo $hp_offical;?>" target="_blank">→クリック←</a><?php } ?></td>
                  </tr>
                  
                  <?php if($penalty!="") { ?>
                  <tr>
                    <th>罰金、雑費の情報</th>
                    <td><?php echo nl2br($penalty);?></td>
                  </tr>
                  <?php } ?>
                  
                  <?php if($shop_pr!="") { ?>
                  <tr>
                    <th>お店からのコメント</th>
                    <td><?php echo nl2br($shop_pr);?></td>
                  </tr>
                  <?php } ?>
              </table>
            </div>
				<!--ショップ基本情報ここまで-->