<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonVoice.php";
	$common_voice = new CommonVoice();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateJob.php";
	$common_cate_job = new CommonCateJob();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_cate_area = new CommonCateArea();
?>
				<!--ヘッダー1段目----------------------->
					<div class="header1">
					<!--ボックス左----------------------->
					<!--店舗名BOX-->
						<div class="hedshopname">
					<!--店名左ボックス-->
							<h1 class="shopname"><?php echo $shop_name;?></h1><!--店名20文字まで-->
					<!--業種、エリア出力左-->
							<div class="jobtype">
              	<?
								$arr_where = array();
								$var = "cate_area_s_id";
								$arr_where[$var] = $$var;
								
								$arr_data = array();
								$arr_data[] = "cate_area_l_title";
								$arr_data[] = "cate_area_s_title";
								
								$return_area = $common_cate_area->Fn_cate_area_data($arr_data, $arr_where);
								
								$arr_where = array();
								$var = "cate_job_id";
								$arr_where[$var] = $$var;
								
								$arr_data = array();
								$arr_data[] = "cate_job_title";
								
								$return_job = $common_cate_job->Fn_cate_job_data($arr_data, $arr_where);
								
								?>
							(<? echo $return_area[0]["cate_area_l_title"]." - ".$return_area[0]["cate_area_s_title"];?>/<? echo $return_job[0]["cate_job_title"];?>)
							</div>
						</div>


					<!--ボタンBOX-->
						<div class="hedbutton">
							<div class="button-group">
                <a href="/kanto/voice/posting_voice.php?shop_id=<? echo $shop_id;?>" class="button primary rightborder"><i class="fa fa-comment-o"></i>お店に口コミを投稿する</a>
                <a href="/kanto/voice/input.php?shop_id=<? echo $shop_id;?>" class="button leftborder"><i class="fa fa-star-o"></i>お気に入り</a>
              </div>
						</div>
					</div>
					<!--ヘッダー1段目ここまで----------------------->

					<div class="headercnter">
						<hr class="sikiri">
					</div>

				<!--ヘッダー2段目----------------------->
					<div class="header2">

					<!--評価☆-->
						<div class="hosi"><img src="/kanto/common/img/star<? echo sprintf('%.1f',$evaluation);?>.png"></div>
              <div class="heikinscor">【<? if($evaluation==0){ echo "-.--";} else { echo sprintf('%.2f',$evaluation);}?>点】</div>
					<!--投稿口コミ数-->
          <?
						$all_voice_count = 0;
						$arr_where = array();
						$arr_where["flag_open"] = "1";
						$arr_where["shop_id"] = $shop_id;
						
						$return_voice_count = $common_voice->Fn_voice_user_count ($arr_where);
						if(!is_null($return_voice_count)) {
							$all_voice_count = $return_voice_count[0]["all_count"];
						}
          ?>
						<div class="kutikazu"><i class="fa fa-comment"></i>投稿口コミ数<span class="talk"><? echo $all_voice_count;?></SPAN>件</div>
					<!--口コミと点数について-->
						<div class="explain"><a href="#"><i class="fa fa-question-circle"></i>口コミと点数について</a></div>
					<!--掲載してみませんか？-->
						<div class="admission"><a href="#"><i class="fa fa-heartbeat"></i>広告掲載について</a></div>
					</div>
					<!--ヘッダー2段目まで-->
					<div class="headercnter">
					</div>