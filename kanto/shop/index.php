<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonContents.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShopImg.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateJob.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_contents = new CommonContents();
	$common_shop = new CommonShop();
	$common_shopimg = new CommonShopImg();
	$common_cate_job = new CommonCateJob();
	$common_cate_area = new CommonCateArea();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonVoice.php";
	$common_voice = new CommonVoice();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	$pankuzu_txt = "応募する";
?>
<?php
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}
	if($shop_id=="") { $common_connect -> Fn_javascript_back("正しく入力してください。"); }

	//ショップの基本情報
	$arr_where = array();
	$var = "flag_open";
	$arr_where[$var] = "1"; //公開のみ
	$var = "shop_id";
	$arr_where[$var] = $$var;
	
	$arr_data = array("shop_id", "shop_name", "shop_kana", "cate_area_s_id", "area_detail", "cate_job_id", "evaluation");
	$arr_data = array_merge($arr_data, array("catchcopy", "shop_title", "shop_address", "shop_tel", "job_comment", "job_salary_pre"));
	$arr_data = array_merge($arr_data, array("job_price", "job_salary", "job_style", "job_years_from", "job_years_to"));
	$arr_data = array_merge($arr_data, array("job_time_from", "job_time_to", "job_special", "hp_job", "hp_offical", "penalty", "shop_pr", "job_url"));
	$arr_data = array_merge($arr_data, array("open_time_from", "open_time_to", "main_img", "shop_thumbnail", "pr_comment"));
	$arr_data = array_merge($arr_data, array("shop_view_level", "line_id", "img_1"));
	
	$db_result_shop = $common_shop -> Fn_db_shop_data ($arr_data, $arr_where);
	foreach($db_result_shop as $arr_shop)
	{
		foreach($arr_shop as $key=>$value)
		{
			$$key = $value;
		}
	}
	
	if($shop_name=="")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
?>
<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

<!--ページ専用のCSS-->
<link href="/kanto/shop/css/shopjob.css" rel="stylesheet" type="text/css" />

<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--口コミ投稿ボタン用CSS-->
<link rel="stylesheet" href="/kanto/app/desing/githubbutton/gh-buttons.css">

<!--↓アプリケーション-->
<script src="/kanto/app/jquery.min.js" type="text/javascript" ></script>

<!--アプリケーション/フォームデザイン-->
<link href="/kanto/app/form/formoid2/forms2.css" rel="stylesheet" type="text/css" />
<!--アプリケーション/ポップアップ-->
<link rel="stylesheet" href="/kanto/app/pop/lity.min.css">
<script src="/kanto/app/pop/lity.min.js"></script>
<!--アプリケーション/画像拡-->
<link rel="stylesheet" href="/kanto/app/zoom/css/lightbox.css">

<!-- 下へアンカー -->
<script type="text/javascript">
$(function(){
    $('a[href^=#]').click(function(){
        var speed = 500;
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $("html, body").animate({scrollTop:position}, speed, "swing");
        return false;
    });
});
</script>

<?
	//有料
	if($common_contents -> Fn_db_shop_view_level($shop_view_level))
	{
?>
<!-- ※有料店のみ出力 折り畳みパネル -->
	<script type="text/javascript">

		function showMoreJq(btn) {
			var targetId = btn.getAttribute("href");
			$(targetId).slideDown("slow");
			$(btn.parentNode).slideUp("fast");
			return false;
		}

		$(function(){
			// ▽続きの表示エリアを非表示にする
			$(".readmore-area").hide();
			// ▽「続きを読む」ボタンがクリックされた際の処理を割り当てる
			$(".readmore-button-box a").click( function() { return showMoreJq(this); } );
		});

	</script>
<!-- ※有料店のみ出力 折り畳みパネル -->



<!--※　有料店のみ出力 スライドショー-->
<link rel="stylesheet" href="/kanto/app/shopslider/slick.css"/>
<link rel="stylesheet" href="/kanto/app/shopslider/slick-theme.css"/>
<script src="/kanto/app/shopslider/slick.min.js"></script>
<script>
$(function() {
	$('.slider-for').slick({
		infinite: true,
		dots:true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 8000,
		fade: true,
	});
});
</script>
<!--※　有料店のみ出力 スライドショー-->
<? } else { ?>

<!--無料のお店の場合だけ出力-->
<!--アプリケーション/メイン画像リサイズ-->
<script src="/kanto/app/resize/imgLiquid-min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(".mainimgresize").imgLiquid({
        fill: false // 縦横比を維持
    });
});
</script>
<!--無料のお店の場合だけ出力-->
<? } ?>


</head>



<body>

<div id="container">

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>





<!--外枠-->	<div id="mainbox">



			<!--パンくず-->
				<ul id="pan">
					<li><a href="/">トップページ</a></li>
					<li><a href="/kanto/">関東</a></li>
					<li><a href="/kanto/search/">検索一覧</a></li>
					<li><? echo $shop_name;?>　<? echo $pankuzu_txt;?></li>
				</ul>
			<!--パンくず-->



			

			<div id="main" class="pt6"><!--コンテンツ左680幅-->
<?
    //有料
    if($common_contents -> Fn_db_shop_view_level($shop_view_level))
    {
        $arr_where = array();
        $var = "s_flag_open";
        $arr_where[$var] = "1"; //公開のみ
        $result_shop_img = $common_shopimg -> Fn_db_shop_img($shop_id, $arr_where);
        
        if($result_shop_img)
        {
?>
				<div id="extorahed">
					
				<ul class="slider-for slider">
<?
            for($db_loop=0 ; $db_loop < count($result_shop_img) ; $db_loop++)
            {
?>
					<li><img data-lazy="/<? echo global_shop_dir.$shop_id."/".$result_shop_img[$db_loop]["img"];?>" alt="<? echo $result_shop_img[$db_loop]["comment"];?>"><div class="slidercomment"><? echo $result_shop_img[$db_loop]["comment"];?></div></li>
<?
            }
?>
				</ul>
				</div>
<?
				}
		//無料
		} else {
?>
<!--画像一枚※無料店16：7で最大表示は500×281-->
				<div class="shopmainimg-base">
					<div class="mainimgresize">
						<img src="/kanto/shop/img/test/noimge.png">
					</div>
				</div>
<!--画像一枚※無料店16：7で最大表示は500×281-->
<?
		}
?>

				<div id="shoptop">
        
        
				<!--キャッチコピー部分　お店側が26文字でお店をPR出来る-->
					<div class="catchcopy">
						<div class="ankrdownleft"><i class="fa fa-hand-o-right">ポイント</i>&nbsp;&nbsp;<? echo $catchcopy;?></div>
						<div class="ankrdownright"><a href="#ankerbottom">▼お店の基本情報を見る</a></div>
						<div class="floatclear"></div>
					</div>
          
          <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/shop_summery.php"); ?>


				</div><!--shoptop-->
				<!--shopメインヘッダーここまで----------------------------------------------------->


				<!--ショップ基本情報-->
				<div class="mainshopdeta">


<?
    //有料
    if($common_contents -> Fn_db_shop_view_level($shop_view_level))
    {
			if($pr_comment!="")
			{
?>
					<!--ショップPRコメント　有料店のみ-->
					<div class="samplearea">

						<p><!--　一列48文字で改行。96文字までここに文字出力されます　-->
						<? echo nl2br(mb_substr($pr_comment, 0, 96, "utf-8"));?></p>
						<p class="readmore-button-box">
						<a href="#readmore1">お店のPRコメントを見る</a>
						</p>
						<!--　96文字以上からここに文字が出力される-->
						<p id="readmore1" class="readmore-area">
						<? echo nl2br(mb_substr($pr_comment, 96, mb_strlen($pr_comment ,"utf-8") ,"utf-8"));?> 
						</p>

					</div>
<?
			}//if $pr_comment
		}
?>






					<div id="wrapper">
        	
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/middle_navi.php"); ?>
						<div class="tabContents">

							<div class="tubtop">このお店に寄せられた口コミ</div>

							<div class="tubnakawaku"><!--口コミエリア外枠-->

          <!--口コミ-------------------------------------------------------------------->
<?
	$arr_voice_ranking = $common_voice ->Fn_voice_ranking();
	$view_count=2;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$arr_where = array();
	$arr_where["flag_open"] = "1";
	$arr_where["shop_id"] = $shop_id;
	
	$return_voice_count = $common_voice->Fn_voice_user_count ($arr_where);
	if(!is_null($return_voice_count)) {
		$all_count = $return_voice_count[0]["all_count"];
	}
	
	//リスト表示
	$arr_etc["order_name"] = $order_name;
	$arr_etc["order"] = $order;
	$arr_etc["offset"] = $offset;
	$arr_etc["view_count"] = $view_count;
	
	$arr_data = array("voice_id", "member_id", "voice_title", "voice_period");
	$arr_data = array_merge($arr_data, array("voice_star", "voice_comment_1", "voice_comment_2", "voice_comment_3", "ranking", "regi_date"));
	
	$arr_voice_list = $common_voice->Fn_voice_user_list ($arr_data, $arr_where, null, $arr_etc);
	if(!is_null($arr_voice_list)) {
		foreach($arr_voice_list as $arr_key=>$arr_value)
		{
			foreach($arr_value as $key=>$value)
			{
				$$key=$value;
			}
?>
						<div class="kutikomis">
							<div class="kutikomibox">
								<div class="plofimg">
								<?
								$arr_data = array();
								$arr_data[]="member_id";
								$arr_data[]="nickname";
								$arr_data[]="member_img";
                  $return_member = $common_member->Fn_db_member_data ($member_id, $arr_data);
                  if(!is_null($return_member)) {
                ?>
                  <a href="?member_id=<? echo $member_id;?>" class="usernamehavar">
                  <? if($return_member[0]["member_img"]!="") { ?>
                    <img src="/<? echo global_member_dir.$return_member[0]["member_id"]."/".$return_member[0]["member_img"];?>" width="60" height="60" alt="<? echo $return_member[0]["nickname"];?>" /></br>
                  <? } else { ?>
                  	
                  <? } ?>
									<? echo $return_member[0]["nickname"];?>
                  </a>
                <? } ?>
                
									<? if($arr_voice_ranking[$ranking]!="") { ?>
                  <div class="shopkutikomiranks">
										<? echo $arr_voice_ranking[$ranking];?>
									</div>
                  <? } ?>
								</div>

								<div class="shopkutikomi shopkutikomi-left">
								<!--★、点数、投稿時間-->
									<div class="indexsterpink">
									<img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi2.png" width="20px"><img src="/kanto/common/img/hosi3.png" width="20px">
									<span>【<? echo sprintf('%.2f',($voice_star/2));?>点】</span><span class="time"><? echo substr($regi_date, 0, 10);?>投稿</span>
									</div>
									<div class="kutikomititleindex"><a href="/kanto/voice/detail.php?voice_id=<? echo $voice_id;?>"><? echo $voice_title;?></a></div>
									<hr class="titleborder">
									<div class="kutikomihonbun"><? echo $voice_comment_1;?></div>
									<div class="kutikomihonbunnext"><a href="/kanto/voice/detail.php?voice_id=<? echo $voice_id;?>">続きを見る..</a></div>
								</div>

							</div>

						</div>
<?
		}
	}
?>

							</div>

<?
	$count_voice_id = 0;
	$sql = "SELECT ";
	$sql .= " count(voice_id) as count_voice_id ";
	$sql .= " FROM voice where shop_id='".$shop_id."' and flag_open = 1 ";
    $result_count = $common_dao->db_query_bind($sql);
    if($result_count)
    {
    	$count_voice_id = $result_count[0]["count_voice_id"];
    }
?>
							<div class="newshoka">
								<img src="/kanto/img/iconsankaku.png" width="15" height="15" /><a href="/kanto/shop/voice.php?shop_id=<? echo $shop_id;?>">お店の口コミ一覧を見る（全<? echo $count_voice_id;?>件）</a>
							</div>


<!--口コミここまで-------------------------------------------------------------------->

<?php

	$all_count = 0;

    $where = "";
    $where .= " and flag_open=1 ";
    $where .= " and shop_id='".$shop_id."' ";


    //合計
    $sql_count = "SELECT count(shop_id) as all_count FROM voice_img where 1 ".$where ;
    
    $db_result_count = $common_dao->db_query_bind($sql_count);
    if($db_result_count)
    {
        $all_count = $db_result_count[0]["all_count"];
    }
    
    //リスト表示
    $arr_db_field = array("voice_img_id", "member_id");
    
    $sql = "SELECT shop_id, regi_date, up_date, voice_img_title, img_1, up_date, ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM voice_img where 1 ".$where ;
    $sql .= " order by regi_date desc";
    $sql .= " limit 0, 4";
    
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
?>
							<div class="tubtop">このお店に関係する画像</div>

							<div class="tubnakawaku"><!--画像エリア外枠-->
								
								<div class="imgteble">

<?
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            foreach($arr_db_field as $val)
            {
                $$val = $db_result[$db_loop][$val];
            }
            $shop_id = $db_result[$db_loop]["shop_id"];
            $regi_date = $db_result[$db_loop]["regi_date"];
            $up_date = $db_result[$db_loop]["up_date"];
            $voice_img_title = $db_result[$db_loop]["voice_img_title"];
            $img_1 = $db_result[$db_loop]["img_1"];
?>
									<div class="imglistboxsoto">
										<div class="imageboxnaka">
											<div class="img-shopsblok">
                                                <? if($img_1!="") { ?>
                                                <a href="/<? echo global_voice_img_dir.$voice_img_id."/".$img_1;?>" class="example-image-link" data-lightbox="example-1">
                                                <img src="/<? echo global_voice_img_dir.$voice_img_id."/".$img_1;?>">
                                                </a>
                                                <? } ?>
											</div>
											<div class="caption"><? echo $voice_img_title;?></div>
											<div class="imgshop">
                                            <?
                                            if($member_id==0)
                                            {
                                            ?>
                                            【お店から投稿】
                                            <?
                                            }
                                            else
                                            {

                                                $sql_member = " select nickname from member where member_id='".$member_id."' and flag_open=1";
                                                $db_result_member = $common_dao->db_query_bind($sql_member);
                                                if($db_result_member)
                                                {
                                            ?>
                                                <a href="<? echo "/user/mypage/user_img.php?member_id=".$member_id;?>">【<? echo $db_result_member[0]["nickname"];?>】</a>
                                            <?
                                                }
                                            ?>
                                            <?
                                            }
                                            ?>
                                            </div>
										</div>
									</div>
<?
		}
?>


								</div>

							</div><!--tubnakawaku-->
							<div class="newshoka">
								<img src="/kanto/img/iconsankaku.png" width="15" height="15" /><a href="/kanto/shop/image.php?shop_id=<? echo $shop_id;?>">お店の画像一覧を見る（全<? echo $all_count;?>枚）</a>
							</div>

<!--画像ここまで-------------------------------------------------------------------->
<?
	}
?>


							<div class="tubtop">このお店からの求人ニュース</div>

								<div class="tubnakawaku"><!--外枠-->

							
							
								<div class="newsshop">

									<div class="blogplofimg">
										<div class="img-blogblock">
											<a href="">
												<img src="/kanto/img/test/neko.jpg">
											</a>
										</div>
									</div>

									<div class="shopnews">
										<div class="blogtitletop">
											<a href="item.html">投稿ブログのタイトル文字数は最大で三十文字まで入力可能です。</a>
										</div>
										<hr class="sikiri">
										<p class="shoppegeblogbun">ここにはブログ文章の一部が表示されます。文字数は一列につき最大で56文字まで表示され、32文字で改行されます。</p>
										<p class="blognewshonbunnext"><a href="item.html">続きを見る..</a></p> <p class="timeblog">2001/12/25投稿</p>
									</div>

									<div class="frotcria"></div>
								</div>

								<div class="newsshop">

									<div class="blogplofimg">
										<div class="img-blogblock">
											<a href="">
												<img src="/kanto/img/test/neko.jpg">
											</a>
										</div>
									</div>

									<div class="shopnews">
										<div class="blogtitletop">
											<a href="item.html">投稿ブログのタイトル文字数は最大で三十文字まで入力可能です。</a>
										</div>
										<hr class="sikiri">
										<p class="shoppegeblogbun">ここにはブログ文章の一部が表示されます。文字数は一列につき最大で56文字まで表示され、32文字で改行されます。</p>
										<p class="blognewshonbunnext"><a href="item.html">続きを見る..</a></p> <p class="timeblog">2001/12/25投稿</p>
									</div>

									<div class="frotcria"></div>
								</div>
								
								
								<div class="newsshop">

									<div class="blogplofimg">
										<div class="img-blogblock">
											<a href="">
												<img src="/kanto/img/test/neko.jpg">
											</a>
										</div>
									</div>

									<div class="shopnews">
										<div class="blogtitletop">
											<a href="item.html">投稿ブログのタイトル文字数は最大で三十文字まで入力可能です。</a>
										</div>
										<hr class="sikiri">
										<p class="shoppegeblogbun">ここにはブログ文章の一部が表示されます。文字数は一列につき最大で56文字まで表示され、32文字で改行されます。</p>
										<p class="blognewshonbunnext"><a href="item.html">続きを見る..</a></p> <p class="timeblog">2001/12/25投稿</p>
									</div>

									<div class="frotcria"></div>
								</div>

							</div>

							<div class="newshoka">
								<img src="/kanto/img/iconsankaku.png" width="15" height="15" /><a href="/kanto/shop/blog.php/">お店のブログ一覧を見る（全23枚）</a>
							</div>

							<script src="/kanto/app/zoom/js/lightbox.min.js"></script>

<!--ブログここまで-------------------------------------------------------------------->

						</div><!--tabContents-->


					</div><!--wrapper-->
          
          <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/shop_info.php"); ?>
			</div>

			<!--広告スペースメインボックス下 マスター管理から画像のアップ、リンク先の変更可能　ランダム表示 jpg,gif共にアップ可能で枚数は最大5枚まで-->

			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/app/pr/contents_bottom.php"); ?>
			<!--広告スペース-->



		</div><!--<div id="mainbox">-->






			<div id="sub"><!--sub----------------------------------------------------->

				<!--ライン-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/shop_line.php"); ?>
				<!--ライン-->
        
				<!--検索-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
				<!--検索-->

				<!--似た求人-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/similar.php"); ?>
				<!--似た求人-->

				<!--観覧履歴-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/history.php"); ?>
				<!--観覧履歴-->

				<!--広告部分その1　280×250-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner.php"); ?>
				<!--広告部分その1　280×250-->
        
				<!--検索リスト-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
				<!--検索リスト-->

				<!--検索-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
				<!--検索-->
        
				<!--広告部分その2　280×250-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner2.php"); ?>
				<!--広告部分その2　280×250-->

			</div>
<!--/sub------------------------------------------------------->

		</div><!--main box-->


		<p id="pagetop">
			<a href="#container"><i class="fa fa-caret-up" aria-hidden="true"></i></a>
		</p>


	
	</div><!--contentsinコンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->




	
	<!--フッター-->
		<div id="footer">
		<a href="./">『口コミと評価で探せる高収入サイトコソット』</a><br />
		<a href="" target="_blank">Produced by (Team avalanche)</a><br />


		<table class="tafooter">
			<tr>
				<td><a href="/kanto/member/">会員登録/ログイン</a></td><td><a href="/">TOPページへ</a></td><td><a href="./">最新口コミ一覧</a></td><td><a href="./">人気ランキング</a></td><td><a href="./">お仕事検索</a></td><td><a href="./">求人ブログ</a></td><td><a href="./">動画一覧</a></td><td><a href="./">綺麗になるまとめ</a></td>
			</tr>

			<tr>
				<td><a href="./">都道府県で仕事探し</a></td><td><a href="./">東京で仕事探し</a></td><td><a href="./">神奈川で仕事探し</a></td><td><a href="./">千葉でお仕事探し</a></td><td><a href="./">埼玉で仕事探し</a></td><td><a href="./">茨城で仕事探し</a></td><td><a href="./">栃木で仕事探し</a></td><td><a href="./">群馬で仕事探し</a></td>
			</tr>

			<tr>
				<td><a href="./">コソットとは</a></td><td><a href="/kanto/help/inquiry.php">広告掲載について</a></td><td><a href="./">口コミ評価の注意点</a></td><td><a href="./">点数について</a></td><td><a href="./">利用規約</a></td><td><a href="./">運営会社</a></td><td><a href="/kanto/help/inquiry.php">お問合せ</td><td><a href="./">お役立ちリンク</a></td>
			</tr>
		</table>

		<p class="prfut">当サイトは18歳未満の方の利用は出来ません。また掲載されている写真、イラスト、画像、文章等の無断掲載は一切禁止しております。</p>
	</div>	<!--フッター-->



</div><!--container-->

	<!--全ページ共通スクリプト-->
	<script type="text/javascript" src="/kanto/app/textcount/textcount.js"></script>
	<script type="text/javascript" src="/kanto/app/resize/imgLiquid-min.js"></script>
	<script type="text/javascript">
$(document).ready(function() {
    $(".img-shopsblok,.img-blogblock").imgLiquid({
    });
});
</script>

<?
//PV /shop/index.phpのみ
$web_sp="w";
	
if ($shop_id!="" && $web_sp!="")
{
	$sql = "select shop_id from shop_pv where shop_id='".$shop_id."' and yyyymm='".date("Ym")."' and web_sp='".$web_sp."'";
	$db_result = $common_dao->db_query_bind($sql);
	if($db_result)
	{
		$db_insert = "update shop_pv set p_".date("d")."=p_".date("d")."+1  ";
		$db_insert .= " where shop_id='$shop_id' and yyyymm='".date("Ym")."' and web_sp='".$web_sp."'";
		$db_result = $common_dao->db_update($db_insert);
	}
	else
	{
		$db_insert = "insert into shop_pv (shop_id, yyyymm, web_sp, p_".date("d").") values ('$shop_id','".date("Ym")."','".$web_sp."',1)";
		$db_result = $common_dao->db_update($db_insert);
	}
}
?>
</body>
</html>