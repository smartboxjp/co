この度は『コソット』のご利用いただき誠にありがとうございます。

下記の内容にてお問い合わせをお受けしました。
担当よりご連絡させていただきます。

──────────────────────────────

--------------------------------------------------
お問い合わせ内容
--------------------------------------------------
【お店】 [i_shop_name]
【名前】 [i_nickname]
【メールアドレス】 [i_email]
【お問い合わせ内容】 [i_comment]

【受 付 日】[datetime]

──────────────────────────────

※なお、このメールに憶えのない方は、
下記メールアドレスまでご連絡ください。
[global_send_mail]

[global_email_footer]