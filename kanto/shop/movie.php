<?
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonContents.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShopImg.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateJob.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
    $common_contents = new CommonContents();
    $common_shop = new CommonShop();
    $common_shopimg = new CommonShopImg();
    $common_cate_job = new CommonCateJob();
    $common_cate_area = new CommonCateArea();
    
?>
<?php
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}

	//ショップの基本情報
	$arr_where = array();
	$var = "flag_open";
	$arr_where[$var] = "1"; //公開のみ
	$var = "shop_id";
	$arr_where[$var] = $$var;
	
	$arr_data = array("shop_id", "shop_name", "shop_kana", "cate_area_s_id", "area_detail", "cate_job_id", "evaluation");
	$arr_data = array_merge($arr_data, array("catchcopy", "shop_title", "shop_address", "shop_tel", "job_comment", "job_salary_pre"));
	$arr_data = array_merge($arr_data, array("job_price", "job_salary", "job_style", "job_years_from", "job_years_to"));
	$arr_data = array_merge($arr_data, array("job_time_from", "job_time_to", "job_special", "hp_job", "hp_offical", "penalty", "shop_pr", "job_url"));
	$arr_data = array_merge($arr_data, array("open_time_from", "open_time_to", "main_img", "shop_thumbnail", "pr_comment"));
	$arr_data = array_merge($arr_data, array("shop_view_level", "line_id", "img_1"));
	
	$db_result_shop = $common_shop -> Fn_db_shop_data ($arr_data, $arr_where);
	foreach($db_result_shop as $arr_shop)
	{
		foreach($arr_shop as $key=>$value)
		{
			$$key = $value;
		}
	}
	
	if($shop_name=="")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}

    $pankuzu_txt = "求人動画";


	$arr_where = array();
	$var = "cate_area_s_id";
	$arr_where[$var] = $$var;
	
	$arr_data = array();
	$arr_data[] = "cate_area_l_title";
	$arr_data[] = "cate_area_s_title";
	
	$return_area = $common_cate_area->Fn_cate_area_data($arr_data, $arr_where);
	
	$arr_where = array();
	$var = "cate_job_id";
	$arr_where[$var] = $$var;
	
	$arr_data = array();
	$arr_data[] = "cate_job_title";
	
	$return_job = $common_cate_job->Fn_cate_job_data($arr_data, $arr_where);

	$meta_title = $shop_name."の動画";
	$meta_description = $shop_name."(".$return_area[0]["cate_area_l_title"]."/".$return_job[0]["cate_job_title"].")が投稿した動画です。";
	$meta_keywords = "動画,".$shop_name.",".$return_area[0]["cate_area_l_title"].",".$return_area[0]["cate_area_s_title"].",業種";
?>
<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

<!--ページ専用のCSS-->
<link href="/kanto/shop/css/shopjob.css" rel="stylesheet" type="text/css" />

<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--口コミ投稿ボタン用CSS-->
<link rel="stylesheet" href="/kanto/app/desing/githubbutton/gh-buttons.css">
<!--アプリケーション/フォームデザイン-->
<link href="/kanto/app/form/formoid2/forms2.css" rel="stylesheet" type="text/css" />
<!--アプリケーション/ポップアップ-->
<link rel="stylesheet" href="/kanto/app/pop/lity.min.css">
<script src="/kanto/app/pop/lity.min.js"></script>
<!--アプリケーション/画像拡-->
<link rel="stylesheet" href="/kanto/app/zoom/css/lightbox.css">

<!-- 下へアンカー -->
<script type="text/javascript">
$(function(){
    $('a[href^=#]').click(function(){
        var speed = 500;
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $("html, body").animate({scrollTop:position}, speed, "swing");
        return false;
    });
});
</script>



<body>

<div id="container">

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>

	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>

<!--外枠-->	<div id="mainbox">

			<!--パンくず-->
			<ul id="pan">
				<li><a href="/">トップページ</a></li>
				<li><a href="/kanto/">関東</a></li>
				<li><a href="/kanto/search/">検索一覧</a></li>
				<li><? echo $shop_name;?>の動画</li>
			</ul>
			<!--パンくず-->

			

			<div id="main" class="pt6"><!--コンテンツ左680幅-->


				<div id="shoptop">
        <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/shop_summery.php"); ?>
				</div><!--shoptop-->
				<!--shopメインヘッダーここまで----------------------------------------------------->


				<!--ショップ基本情報-->
				<div class="mainshopdeta">


					<div id="wrapper">
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/middle_navi.php"); ?>
						<div class="tabContents">

							<div class="cntentsubtitle">
							※求人ムービーは、どのお店でも新しいムービーが投稿されると。上書きされ古いムービーは削除されてしまいます。
							動画がストックされることはありませんので、動画がアップされたらなるべく早く視聴する事をお勧めします。
							</div>

							<div class="cntenttitle">このお店が投稿したブログ一覧</div>


							<div class="shopcntentwaku">
								<div class="shopcm">
<?
    $sql = "SELECT shop_movie_title, movie_1, m.regi_date FROM shop_movie m inner join shop s on m.shop_id=s.shop_id where s.flag_open=1 and s.shop_id='".$shop_id."' ";

    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $shop_movie_title = $db_result[0]["shop_movie_title"];
        $movie_1 = $db_result[0]["movie_1"];
        $regi_date = $db_result[0]["regi_date"];
?>
									<div class="moviebox">
									<? if($movie_1!="") { ?>
										<video controls poster="/kanto/movie/img/vstart.png" width="640" height="360">
											<source src="/<? echo global_shop_movie_dir.$shop_id."/".$movie_1;?>">
											<p>動画を再生するには、videoタグをサポートしたブラウザが必要です。</p>
										</video> 
									<? } ?>
									</div>
									<div class="cmtime">
										公開日:<? echo date("Y/m/d", strtotime($regi_date));?>
									</div>


									<div class="cmcommentt">
									お店からのコメント
									</div>
									<div class="cmcomment">
										<? echo nl2br($shop_movie_title);?>
									</div>


<?
	}
	else
	{
?>
								<img src="/kanto/shop/img/test/sample1.jpg">
<?
	}
?>
								</div><!--shopcm-->

							</div><!--shopcntentwaku-->




						</div><!--tabContents-->


					</div><!--wrapper-->
          
          <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/shop_info.php"); ?>



			</div>

			<!--広告スペースメインボックス下 マスター管理から画像のアップ、リンク先の変更可能　ランダム表示 jpg,gif共にアップ可能で枚数は最大5枚まで-->

			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/app/pr/contents_bottom.php"); ?>
			<!--広告スペース-->



		</div><!--<div id="mainbox">-->





			<div id="sub"><!--sub----------------------------------------------------->

				<!--ライン-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/shop_line.php"); ?>
				<!--ライン-->
        
				<!--検索-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
				<!--検索-->

				<!--似た求人-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/similar.php"); ?>
				<!--似た求人-->

				<!--観覧履歴-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/history.php"); ?>
				<!--観覧履歴-->

				<!--広告部分その1　280×250-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner.php"); ?>
				<!--広告部分その1　280×250-->
        
				<!--検索リスト-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
				<!--検索リスト-->

				<!--検索-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
				<!--検索-->
        
				<!--広告部分その2　280×250-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner2.php"); ?>
				<!--広告部分その2　280×250-->

			</div>
<!--/sub------------------------------------------------------->


		</div><!--main box-->


		<p id="pagetop">
			<a href="#container"><i class="fa fa-caret-up" aria-hidden="true"></i></a>
		</p>

	
	</div><!--contentsinコンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->




	
	<!--フッター-->
		<div id="footer">
		<a href="./">『口コミと評価で探せる高収入サイトコソット』</a><br />
		<a href="" target="_blank">Produced by (Team avalanche)</a><br />


		<table class="tafooter">
			<tr>
				<td><a href="/kanto/member/">会員登録/ログイン</a></td><td><a href="/">TOPページへ</a></td><td><a href="./">最新口コミ一覧</a></td><td><a href="./">人気ランキング</a></td><td><a href="./">お仕事検索</a></td><td><a href="./">求人ブログ</a></td><td><a href="./">動画一覧</a></td><td><a href="./">綺麗になるまとめ</a></td>
			</tr>

			<tr>
				<td><a href="./">都道府県で仕事探し</a></td><td><a href="./">東京で仕事探し</a></td><td><a href="./">神奈川で仕事探し</a></td><td><a href="./">千葉でお仕事探し</a></td><td><a href="./">埼玉で仕事探し</a></td><td><a href="./">茨城で仕事探し</a></td><td><a href="./">栃木で仕事探し</a></td><td><a href="./">群馬で仕事探し</a></td>
			</tr>

			<tr>
				<td><a href="./">コソットとは</a></td><td><a href="/kanto/help/inquiry.php">広告掲載について</a></td><td><a href="./">口コミ評価の注意点</a></td><td><a href="./">点数について</a></td><td><a href="./">利用規約</a></td><td><a href="./">運営会社</a></td><td><a href="/kanto/help/inquiry.php">お問合せ</td><td><a href="./">お役立ちリンク</a></td>
			</tr>
		</table>

		<p class="prfut">当サイトは18歳未満の方の利用は出来ません。また掲載されている写真、イラスト、画像、文章等の無断掲載は一切禁止しております。</p>
	</div>	<!--フッター-->



</div><!--container-->


</body>
</html>