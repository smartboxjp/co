<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像

	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	$common_email = new CommonEmail(); //メール関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonApply.php";
	$common_apply = new CommonApply();
	
	$meta_title = "コソットへお問い合わせ";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/".$global_area."/common/header/header_meta.php";?>
</head>

<body>
<?
	$datetime = date("Y-m-d H:i:s");
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($i_nickname=="" || $i_email=="" || $i_shop_id=="" )
	{
		$common_connect-> Fn_javascript_back("正しく入力して下さい。");
	}
	
	$shop_id = $i_shop_id;
	
	//ショップの基本情報
	$arr_where = array();
	$var = "flag_open";
	$arr_where[$var] = "1"; //公開のみ
	$db_result_shop = $common_shop -> Fn_db_shop($shop_id, $arr_where);
	foreach($db_result_shop as $arr_shop)
	{
		$shop_name = $db_result_shop[0]["shop_name"];
	}
	
	if($shop_name=="")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
	
	$datetime = date("Y/m/d H:i:s");
	$member_id = $_SESSION['member_id'];
	if($member_id=="") { $member_id = 0;}
	$apply_sel = "";
	if($i_sel !="")
	{
		$apply_sel = mb_substr($i_sel, 0, 2, "utf-8");
	}

	$nickname = $i_nickname;
	$apply_email = $i_email;
	$apply_status = "未読";
	
	$i_point_view = "";
	if(!is_null($_POST["i_point"])) {
		foreach($_POST["i_point"] as $value) {
			$i_point_view .= $value."　";
		}
	}
	
	$i_interview_view = "";
	if(!is_null($_POST["i_interview"])) {
		foreach($_POST["i_interview"] as $value) {
			$i_interview_view .= $value."　";
		}
	}
	
$apply_comment = <<<EOF

・お住まいはどこですか？
$i_address
・年齢はいくつですか？
$i_years
・業界の経験はありますか？
$i_sel
・お店選びで重要視してる所は？
$i_point_view
・お問い合わせ内容
$i_exprience
・面接希望日
$i_interview_view
・質問やご要望など
$i_comment

EOF;
	
	//array
	$arr_data = array();
	$arr_db_field = array("member_id", "apply_sel", "shop_id", "nickname");
	$arr_db_field = array_merge($arr_db_field, array("apply_email", "apply_comment", "apply_status"));
	
	
	//基本情報
	foreach($arr_db_field as $value) {
		$arr_data[$value] = $$value;
	}
	$common_apply -> Fn_apply_insert ($arr_data);

	$result_apply_id = $common_apply -> Fn_db_apply_auto();
	$apply_id = $result_apply_id[0]["last_id"];


	//Folder生成
	$save_dir = $global_path.global_apply_dir.$apply_id."/";
	$common_image -> create_folder ($save_dir);
	
	$new_end_name="_1";
	$fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $apply_id, $text_img_1, "");//400
	
	$arr_data = array();
	$arr_data["img_1"] = $fname_new_name[1];
	
	$arr_where = array();
	$arr_where["apply_id"] = $apply_id;

	$common_apply -> Fn_apply_update ($arr_data, $arr_where);

	//Thank youメール
	if ($apply_email != "")
	{
		$subject = "『 コソット 』お店にお問い合わせありがとうございます。";

		$body = file_get_contents("../mail/sub_inquiry.php");
		$body = str_replace("[i_shop_name]", $shop_name, $body);
		$body = str_replace("[i_nickname]", $i_nickname, $body);
		$body = str_replace("[i_email]", $i_email, $body);
		$body = str_replace("[i_comment]", $apply_comment, $body);
		$body = str_replace("[datetime]", $datetime, $body);
		$body = str_replace("[global_send_mail]", $global_send_mail, $body);
		$body = str_replace("[global_email_footer]", $global_email_footer, $body);
		
		$common_email-> Fn_send_utf($apply_email."<".$apply_email.">",$subject,$body,$global_mail_from,$global_send_mail);
	}
	
	//$common_connect -> Fn_email_log($apply_email, $subject, $body); //メールログ
	//$common_email-> Fn_send_utf($global_send_mail."<".$global_send_mail.">",$subject,$body,$global_mail_from,$global_send_mail);

	$common_connect-> Fn_javascript_move("お問い合わせありがとうございます。", global_no_ssl."/".$global_area."/shop/?shop_id=".$shop_id);
?>

</body>

</html>
