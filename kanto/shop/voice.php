<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonContents.php";
	$common_contents = new CommonContents();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonVoice.php";
	$common_voice = new CommonVoice();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_cate_area = new CommonCateArea();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateJob.php";
	$common_cate_job = new CommonCateJob();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = trim($common_dao->db_string_escape($value));
		//$$key = $value;
	}
	if($shop_id=="") { $common_connect -> Fn_javascript_back("正しく入力してください。"); }
	
	
	//ショップの基本情報
	$arr_where = array();
	$var = "flag_open";
	$arr_where[$var] = "1"; //公開のみ
	$var = "shop_id";
	$arr_where[$var] = $$var;
	
	$arr_data = array("shop_id", "shop_name", "shop_kana", "cate_area_s_id", "area_detail", "cate_job_id", "evaluation");
	$arr_data = array_merge($arr_data, array("catchcopy", "shop_title", "shop_address", "shop_tel", "job_comment", "job_salary_pre"));
	$arr_data = array_merge($arr_data, array("job_price", "job_salary", "job_style", "job_years_from", "job_years_to"));
	$arr_data = array_merge($arr_data, array("job_time_from", "job_time_to", "job_special", "hp_job", "hp_offical", "penalty", "shop_pr", "job_url"));
	$arr_data = array_merge($arr_data, array("open_time_from", "open_time_to", "main_img", "shop_thumbnail", "pr_comment"));
	$arr_data = array_merge($arr_data, array("shop_view_level", "line_id", "img_1"));
	
	$db_result_shop = $common_shop -> Fn_db_shop_data ($arr_data, $arr_where);
	foreach($db_result_shop as $arr_shop)
	{
		foreach($arr_shop as $key=>$value)
		{
			$$key = $value;
		}
	}
	
	if($shop_name=="")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
	
	$meta_title = $shop_name."タイトル";
	$meta_description = "description です";
	$meta_keywords = "キーワードです";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>
<!--ページ専用のCSS-->
<link href="/kanto/shop/css/shopjob.css" rel="stylesheet" type="text/css" />

<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--口コミ投稿ボタン用CSS-->
<link rel="stylesheet" href="/kanto/app/desing/githubbutton/gh-buttons.css">
<!--アプリケーション/フォームデザイン-->
<link href="/kanto/app/form/formoid2/forms2.css" rel="stylesheet" type="text/css" />
<!--アプリケーション/ポップアップ-->
<link rel="stylesheet" href="/kanto/app/pop/lity.min.css">
<script src="/kanto/app/pop/lity.min.js"></script>
<!--アプリケーション/画像拡-->
<link rel="stylesheet" href="/kanto/app/zoom/css/lightbox.css">

<!-- 下へアンカー -->
<script type="text/javascript">
$(function(){
    $('a[href^=#]').click(function(){
        var speed = 500;
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $("html, body").animate({scrollTop:position}, speed, "swing");
        return false;
    });
});
</script>



<body>

<div id="container">

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>

	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>

<!--外枠-->	<div id="mainbox">



			<!--パンくず-->
			<ul id="pan">
				<li><a href="/">トップページ</a></li>
				<li><a href="/kanto/">関東</a></li>
				<li><a href="/kanto/search/">検索一覧</a></li>
				<li>テスト'!"#$%%&'"　応募する</li>
			</ul>
			<!--パンくず-->

			

			<div id="main" class="pt6"><!--コンテンツ左680幅-->


				<div id="shoptop">
        <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/shop_summery.php"); ?>
				</div><!--shoptop-->
				<!--shopメインヘッダーここまで----------------------------------------------------->


				<!--ショップ基本情報-->
				<div class="mainshopdeta">

					<div id="wrapper">
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/middle_navi.php"); ?>
						<div class="tabContents">

							<div class="cntentsubtitle">
							※口コミはあくまでユーザーの個人的な意見であり、<span>中には私情が大きく絡んでる口コミもあります。</span>
							お店の評価は人によって様々であり、また時期や時間帯によっても大きく異なる場合があります。
							あくまで一つの参考程度で観覧して下さい。<span>また記載された情報に誤りがある場合がありますので
							必ずお店に確認</span>をして下さい。	
							</div>

							<div class="cntenttitle">このお店に寄せられた口コミ</div>

							<div class="shopcntentwaku"><!--口コミエリア外枠-->

          <!--口コミ-------------------------------------------------------------------->
<?
	$arr_voice_ranking = $common_voice ->Fn_voice_ranking();
	$view_count=10;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$arr_where = array();
	$arr_where["flag_open"] = "1";
	$arr_where["shop_id"] = $shop_id;
	
	$return_voice_count = $common_voice->Fn_voice_user_count ($arr_where);
	if(!is_null($return_voice_count)) {
		$all_count = $return_voice_count[0]["all_count"];
	}
	
	//リスト表示
	$arr_etc["order_name"] = $order_name;
	$arr_etc["order"] = $order;
	$arr_etc["offset"] = $offset;
	$arr_etc["view_count"] = $view_count;
	
	$arr_data = array("voice_id", "member_id", "voice_title", "voice_period");
	$arr_data = array_merge($arr_data, array("voice_star", "voice_comment_1", "voice_comment_2", "voice_comment_3", "ranking", "regi_date"));
	
	$arr_voice_list = $common_voice->Fn_voice_user_list ($arr_data, $arr_where, null, $arr_etc);
	if(!is_null($arr_voice_list)) {
		foreach($arr_voice_list as $arr_key=>$arr_value)
		{
			foreach($arr_value as $key=>$value)
			{
				$$key=$value;
			}
?>
						<div class="kutikomis">
							<div class="kutikomibox">
								<div class="plofimg">
								<?
                  $arr_data = array();
									$arr_data[]="member_id";
									$arr_data[]="nickname";
									$arr_data[]="member_img";
                  $return_member = $common_member->Fn_db_member_data ($member_id, $arr_data);
                  if(!is_null($return_member)) {
                ?>
                  <a href="?member_id=<? echo $member_id;?>" class="usernamehavar">
                  <? if($return_member[0]["member_img"]!="") { ?>
                    <img src="/<? echo global_member_dir.$return_member[0]["member_id"]."/".$return_member[0]["member_img"];?>" width="60" height="60" alt="<? echo $return_member[0]["nickname"];?>" /></br>
                  <? } ?>
									<? echo $return_member[0]["nickname"];?>
                  </a>
                <? } ?>
                
									<? if($arr_voice_ranking[$ranking]!="") { ?>
                  <div class="shopkutikomiranks">
										<? echo $arr_voice_ranking[$ranking];?>
									</div>
                  <? } ?>
								</div>

								<div class="shopkutikomi shopkutikomi-left">
								<!--★、点数、投稿時間-->
									<div class="indexsterpink">
                    <img src="/kanto/common/img/star<? echo sprintf('%.1f',($voice_star/2));?>m.png">
                    <span>【<? echo sprintf('%.2f',($voice_star/2));?>点】</span><span class="time"><? echo substr($regi_date, 0, 10);?>投稿</span>
                  </div>
									<div class="kutikomititleindex"><a href="/kanto/voice/detail.php?voice_id=<? echo $voice_id;?>"><? echo $voice_title;?></a></div>
									<hr class="titleborder">
									<div class="kutikomihonbun"><? echo $voice_comment_1;?></div>
									<div class="kutikomihonbunnext"><a href="/kanto/voice/detail.php?voice_id=<? echo $voice_id;?>">続きを見る..</a></div>
								</div>

							</div>

						</div>
<?
		}
	}
?>
          <!--口コミ-------------------------------------------------------------------->

							</div>


							<div class="ander">
								<div id="nextcaunt">
									<?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
								</div>
								<div class="floatclear"></div>
							</div>

						</div><!--tabContents-->


					</div><!--wrapper-->



          <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/shop_info.php"); ?>



			</div>

			<!--広告スペースメインボックス下 マスター管理から画像のアップ、リンク先の変更可能　ランダム表示 jpg,gif共にアップ可能で枚数は最大5枚まで-->

			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/app/pr/contents_bottom.php"); ?>
			<!--広告スペース-->



		</div><!--<div id="mainbox">-->







			<div id="sub"><!--sub----------------------------------------------------->

				<!--ライン-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/shop_line.php"); ?>
				<!--ライン-->
        
				<!--検索-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
				<!--検索-->

				<!--似た求人-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/similar.php"); ?>
				<!--似た求人-->

				<!--観覧履歴-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/history.php"); ?>
				<!--観覧履歴-->

				<!--広告部分その1　280×250-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner.php"); ?>
				<!--広告部分その1　280×250-->
        
				<!--検索リスト-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
				<!--検索リスト-->

				<!--検索-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
				<!--検索-->
        
				<!--広告部分その2　280×250-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner2.php"); ?>
				<!--広告部分その2　280×250-->

			</div>
<!--/sub------------------------------------------------------->

		</div><!--main box-->


		<p id="pagetop">
			<a href="#container"><i class="fa fa-caret-up" aria-hidden="true"></i></a>
		</p>

	
	</div><!--contentsinコンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->




	
	<!--フッター-->
		<div id="footer">
		<a href="./">『口コミと評価で探せる高収入サイトコソット』</a><br />
		<a href="" target="_blank">Produced by (Team avalanche)</a><br />


		<table class="tafooter">
			<tr>
				<td><a href="/kanto/member/">会員登録/ログイン</a></td><td><a href="/">TOPページへ</a></td><td><a href="./">最新口コミ一覧</a></td><td><a href="./">人気ランキング</a></td><td><a href="./">お仕事検索</a></td><td><a href="./">求人ブログ</a></td><td><a href="./">動画一覧</a></td><td><a href="./">綺麗になるまとめ</a></td>
			</tr>

			<tr>
				<td><a href="./">都道府県で仕事探し</a></td><td><a href="./">東京で仕事探し</a></td><td><a href="./">神奈川で仕事探し</a></td><td><a href="./">千葉でお仕事探し</a></td><td><a href="./">埼玉で仕事探し</a></td><td><a href="./">茨城で仕事探し</a></td><td><a href="./">栃木で仕事探し</a></td><td><a href="./">群馬で仕事探し</a></td>
			</tr>

			<tr>
				<td><a href="./">コソットとは</a></td><td><a href="/kanto/help/inquiry.php">広告掲載について</a></td><td><a href="./">口コミ評価の注意点</a></td><td><a href="./">点数について</a></td><td><a href="./">利用規約</a></td><td><a href="./">運営会社</a></td><td><a href="/kanto/help/inquiry.php">お問合せ</td><td><a href="./">お役立ちリンク</a></td>
			</tr>
		</table>

		<p class="prfut">当サイトは18歳未満の方の利用は出来ません。また掲載されている写真、イラスト、画像、文章等の無断掲載は一切禁止しております。</p>
	</div>	<!--フッター-->



</div><!--container-->


</body>
</html>