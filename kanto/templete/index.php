<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "タイトル";
	$meta_description = "description です";
	$meta_keywords = "キーワードです";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

<!--ページ専用のCSS-->
<link href="/kanto/ranking/css/rank.css" rel="stylesheet" type="text/css" />
<!--ブログ一覧用css-->
<link href="/kanto/blog/css/jobblog.css" rel="stylesheet" type="text/css" />

<!--アイコン用CSS-->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

</head>


<body>

<div id="container">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>

	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>
		

		<div id="mainbox"><!--コンテンツの外枠-->


		<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /start /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
			<ul id="pan">
        			<li><a href="index.html">トップページ</a></li>
        			<li><a href="index.html">*****</a></li>
        			<li><a href="index.html">*****</a></li>
        			<li>*****</li>
      			</ul>


  
      			<div id="main" class="pt6"><!--コンテンツ左680幅-->
			

				<div class="jobbulogboxtop">

					<div class="ranktitle">
						<img src="/kanto/common/img/ranktitle3.png" width="680" height="27" alt="" />
					</div>

				<!--ここはブログのランキング上位3位までがはいるので、ランキング連動-->

					<div class="box5">
						<div class="rankblogmark"><img src="/kanto/blog/img/test/rank1.png"/></div>
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="images/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>


					<div class="box5">
						<div class="rankblogmark"><img src="/kanto/blog/img/test/rank2.png"/></div>
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="images/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>

					<div class="box5">
						<div class="rankblogmark"><img src="/kanto/blog/img/test/rank3.png"/></div>
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="images/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>

					<div class="boxcria"></div>

				</div>



				<div class="toukoublog">
					これより下は投稿順に掲載されます。
				</div>
				<!--ここから通常投稿　投稿順に並べる-->


				<!--ソート-->
				<div class="blogsearchkleft">

					<ul class="blogsout">
					<li><i class="fa fa-refresh"></i>並び替え→</li>
					<li><a href="#">『業種順』</a></li>
					<li><a href="#">『エリア順』</a></li>
					</ul> 
				</div>
				<!--ソート-->


				<!--検索バー-->
				<div class="blogsearchright">
					<form>
					<input type="search" placeholder="ブログを検索">
					<input type="image" value="検索" src="/kanto/common/img/search01.png">
					</form>
				</div>
				<!--検索バー-->


				<div class="cria"></div>






				<div class="jobbulogbox"><!--2列目-->

					<div class="box5">
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="/kanto/blog/img/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>


					<div class="box5">
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="/kanto/blog/img/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>

					<div class="box5">
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="/kanto/blog/img/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>


					<div class="boxcria"></div><!--frotクリア-->
				</div><!--jobbulogbox-->



				<div class="jobbulogbox"><!--3列目-->

					<div class="box5">
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="/kanto/blog/img/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>


					<div class="box5">
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="/kanto/blog/img/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>

					<div class="box5">
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="/kanto/blog/img/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>


					<div class="boxcria"></div><!--frotクリア-->
				</div><!--jobbulogbox-->


<!--アフィリ広告スペース-->
<div class="afiblogjob">
<a href="item.html"><img src="images/afijobblog.jpg"/></a>
</div>
<!--アフィリ広告スペース-->



				<div class="jobbulogbox"><!--4列目-->

					<div class="box5">
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="/kanto/blog/img/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>


					<div class="box5">
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="/kanto/blog/img/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>

					<div class="box5">
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="/kanto/blog/img/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>


					<div class="boxcria"></div><!--frotクリア-->
				</div><!--jobbulogbox-->



				<div class="jobbulogbox"><!--5列目-->

					<div class="box5">
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="/kanto/blog/img/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>


					<div class="box5">
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="/kanto/blog/img/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>

					<div class="box5">
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="/kanto/blog/img/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>


					<div class="boxcria"></div><!--frotクリア-->
				</div><!--jobbulogbox-->



				<div class="jobbulogbox"><!--6列目-->

					<div class="box5">
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="/kanto/blog/img/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>


					<div class="box5">
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="/kanto/blog/img/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>

					<div class="box5">
						<div class="blogtimetoprank">2015/08/25投稿</div>
						<p class="img"><a href="" target="_blank"><img src="/kanto/blog/img/sample.jpg" width="190" height="133" alt="" /></a></p>
						<div class="blogtitletoprank"><a href="" target="_blank">ここはブログのタイトルが入りますね文字数最大で三十文字まで。</a></div>
						<div class="def"></div>
					</div>


					<div class="boxcria"></div><!--frotクリア-->
				</div><!--jobbulogbox-->


				<div class="anderjob">

					<div id="nextcaunt">
						<ul>
						<li><a href="#">最初</a></li>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">6</a></li>
						<li><a href="#">7</a></li>
						<li><a href="#">8</a></li>
						<li><a href="#">9</a></li>
						<li><a href="#">10</a></li>
						<li><a href="#">最後</a></li>
						</ul> 
					</div>



				</div>


				<p class="bloginfo">
				※各店舗が配信する宣伝ブログのランキングはアクセス、口コミ、評価を含め当社独自の基準で集計、発表を</br>
				しております。またブログの内容に、嘘や大げさな内容が含まれている場合がある時は<a href="#">管理者まで</a>ご連絡を下さい。
				</p>

			</div>
      
		<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /end /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
  
  
  
  
  
			<div id="sub"><!--sub------------------------------------------------------------------------->
        			<!--検索-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
        			<!--検索-->
        
        			<!--検索リスト-->
        			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
        			<!--検索リスト-->
      			</div>
		</div>
  

		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>

	</div><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
<!--contentsin-->

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
</div>
<!--container-->



</body>
</html>

