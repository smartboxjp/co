<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "タイトル";
	$meta_description = "description です";
	$meta_keywords = "キーワードです";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/".$global_area."/common/header/header_meta.php";?>

  <!--ページ専用のCSS-->
  <link href="/<? echo $global_area;?>/css/maintop.css" rel="stylesheet" type="text/css" />
  <link href="/<? echo $global_area;?>/topics/templete/templete.css" rel="stylesheet" type="text/css" />
  <!--アイコン用CSS-->
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

</head>



<body>



<div id="container">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/header/header.php"); ?>


  <div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
    <?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/header/headerlogin.php"); ?>
    <div id="mainbox"><!--コンテンツの外枠-->


<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /start /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>



			<div id="main" class="pt6"><!--コンテンツ左680幅-->





			</div><!--main-->


<!--mainコンテンツここまで------------------------------------------------------->

  
			<div id="sub"><!--lightエリア-------------->

				<!--検索-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/light/searchfrom.php"); ?>
				<!--検索-->

				<!--PR店舗、ページ表示の度に有料店舗から3店舗ランダムで表示。-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/light/prshop.php"); ?>
				<!--PR店舗、ページ表示の度に有料店舗から3店舗ランダムで表示。-->

				<!--検索リスト-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/light/searchlist.php"); ?>
				<!--検索リスト-->

				<!--広告部分その1　280×250-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/light/prbanner.php"); ?>
				<!--広告部分その1　280×250-->

				<!--新店。掲載1ヶ月、または表示件数が4件以上から消える 最新順-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/light/newshop.php"); ?>
				<!--新店。掲載1ヶ月、または表示件数が4件以上から消える-->

				<!--検索-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/light/searchfrom.php"); ?>
				<!--検索-->

				<!--広告部分その2　280×250-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/light/prbanner2.php"); ?>
				<!--広告部分その2　280×250-->

				<!--編集部からのお知らせ-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/light/info.php"); ?>
				<!--編集部からのお知らせ->

				<!--お役立ちツール ノープログラム-->
				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/light/tool.php"); ?>
				<!--お役立ちツール-->

			
			</div>
<!--/sub------------------------------------------------------->


		</div><!--main box-->



  
  
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/footer/page_top.php"); ?>
  </div>



	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/footer/footer.php"); ?>
</div>


</body>
</html>

