<?php
require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_include/connect.php";
$common_connect = new CommonConnect();
$common_dao = new CommonDao(); //DB関連


require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_model/CommonVoice.php";
$common_voice = new CommonVoice();
require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_model/CommonCateArea.php";
$common_cate_area = new CommonCateArea();
require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_model/CommonCateJob.php";
$common_cate_job = new CommonCateJob();
require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_model/CommonShop.php";
$common_shop = new CommonShop();
require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_model/CommonMember.php";
$common_member = new CommonMember();

foreach ( $_GET as $key => $value ) {
	$$key = trim( $common_connect->db_string_escape( $value ) );
	//$$key = $value;
}

$arr_voice_ranking = $common_voice->Fn_voice_ranking();

//ショップの基本情報
$arr_where = array();
$arr_where[ "voice_id" ] = $voice_id;

$arr_data = array( "voice_id", "v.shop_id", "shop_name", "member_id", "v.cate_area_s_id", "v.cate_job_id" );
$arr_data = array_merge( $arr_data, array( "voice_title", "voice_period", "voice_star" ) );
$arr_data = array_merge( $arr_data, array( "voice_comment_1", "voice_comment_2", "voice_comment_3", "ranking", "v.regi_date", "v.up_date" ) );

$db_result_voice = $common_voice->Fn_voice_list( $arr_data, $arr_where, null, null );
if ( !is_null( $db_result_voice ) ) {
	foreach ( $db_result_voice as $arr_key => $arr_value ) {
		foreach ( $arr_value as $key => $value ) {
			$$key = $value;
		}
	}
} else {
	$common_connect->Fn_javascript_back( "正しく入力してください。" );
}

$meta_title = "タイトル";
$meta_description = "description です";
$meta_keywords = "キーワードです";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>
<!--ページ専用のCSS 口コミ部分のデザイン-->
<link href="/kanto/shop/css/shopjob.css" rel="stylesheet" type="text/css"/>
<link href="/kanto/voice/css/kutikomipage.css" rel="stylesheet" type="text/css"/>

<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--口コミ投稿ボタン用CSS-->
<link rel="stylesheet" href="/kanto/app/desing/githubbutton/gh-buttons.css">

</head>


<body>

	<div id="container">
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/header/header.php"); ?>

		<div id="contentsin">
			<!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/header/headerlogin.php"); ?>
			<div id="mainbox">
				<!--コンテンツの外枠-->


				<div id="main" class="pt6">
					<!--コンテンツ左680幅-->


					<ul id="pan">
						<li><a href="index.html">トップページ</a>
						</li>
						<li><a href="index.html">*****</a>
						</li>
						<li><a href="index.html">*****</a>
						</li>
						<li>*****</li>
					</ul>


					<div id="shoptop">
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/shop_summery.php"); ?>
					</div>
					<!--shoptop-->






					<?
        	$result_voice_before = $common_voice->Fn_db_voice_before ($voice_id);
        	$result_voice_next = $common_voice->Fn_db_voice_next ($voice_id);
				?>
					<div class="kutikomizoom">
						<div class="kutikomipagenation">
							<div id="kutikomipagenationbotan">
								<ul>
									<? if(!is_null($result_voice_before[0]["voice_id"])) { ?>
									<li><a href="<? echo $_SERVER['PHP_SELF'];?>?voice_id=<? echo $result_voice_before[0][" voice_id "];?>"><i class="fa fa-chevron-left"></i>前の口コミを見る</a>
									</li>
									<? } ?>
									<li><a href="./"><i class="fa fa-undo"></i>口コミリストに戻る</a>
									</li>
									<? if(!is_null($result_voice_next[0]["voice_id"])) { ?>
									<li><a href="<? echo $_SERVER['PHP_SELF'];?>?voice_id=<? echo $result_voice_next[0][" voice_id "];?>">次の口コミを見る<i class="fa fa-chevron-right"></i></a>
									</li>
									<? } ?>
								</ul>
							</div>
						</div>
						<!--kutikomipagenation-->


						<div class="kutikomizoomupcenter">
							※掲載される口コミは、あくまで投稿者の個人的主観です。どれだけ働いたのか、そのお店で稼いだ金額やタイミングなどにより意見は人それぞれです。 お店選びをする上でのあくまで参考の一つとしてお役立てください。

						</div>


						<div id="balloon-1-bottom">
							<!--口コミトップ部分-->
							<!--ユーザー画像-->
							<div class="kutikomizoomupleft1">
								<?
              $arr_data = array();
              $arr_data[]="member_id";
              $arr_data[]="nickname";
              $arr_data[]="member_img";
              $return_member = $common_member->Fn_db_member_data ($member_id, $arr_data);
              if(!is_null($return_member)) {
            ?>
								<? if($return_member[0]["member_img"]!="") { ?>
								<img src="/<? echo global_member_dir.$return_member[0][" member_id "]."/ ".$return_member[0]["member_img "];?>" width="60" height="60" alt="<? echo $return_member[0][" nickname "];?>"/>
								<? } else { ?>
								<img src="/img/userimg.png" width="60" height="60" alt="<? echo $return_member[0][" nickname "];?>"/>
								<? } ?>
								<?
							}
            ?>
							</div>

							<div class="usernameeria">
								<a href=""><? echo $return_member[0]["nickname"];?></a><br><span class="oltoukousuu">口コミ数73(東京/池袋)</span>
							</div>

							<? if($arr_voice_ranking[$ranking]!="") { ?>
							<div class="kutikomizoomupright1">
								<span class="kutikomirankicon"><img src="/kanto/common/img/rank<? echo $ranking;?>.png" alt="<? echo $arr_voice_ranking[$ranking];?>" /></span>
								<!--口コミ信憑性スタンプ-->
							</div>

							<div class="kutikomizoomupcenterbottom">
								<!--口コミヘッダー下。信憑性により変化-->
								【この口コミは
								<? echo $arr_voice_ranking[$ranking];?>と判断されました。】
							</div>
							<? } ?>

							<div class="clear"></div>

						</div>


						<div class="kutikomizoomup-bottom">
							<!--口コミ下部分-->

							<div class="kutikomizoomupleft2">
								<!--口コミ下左-->
								<div class="kutikomizoomuptitel">
									<? echo $voice_title;?>
								</div>

								<div class="userhosi">
									<img src="/kanto/common/img/star<? echo sprintf('%.1f',($voice_star/2));?>m.png">
									<span class="userscor">
										<? echo sprintf('%.2f',($voice_star/2));?>点</span>
									<!--点数-->
									<span class="userhiyouka">【イマイチ】と評価。</span>
									<!--点数-->
								</div>
							</div>

							<div class="kutikomizoomupright2">
								<!--口コミ下左-->
								<div class="kutikomizoomtime">
									<? echo substr($regi_date, 0, 10);?>投稿</div>
							</div>
							<!--kutikomizoomupright2-->


							<div class="headercnter">
								<hr class="sikiri">
							</div>


							<div class="kutikomizoomupnakawaku">
								<!--口コミ本文の外枠-->


								<? if($voice_period!="") { ?>
								<div class="kutikomizoomupkoumokuwaku">
									<span class="kutikomizoomupkoumokutitle">
										<!--口コミ本文各項目タイトル-->
										このお店で働いた期間はどの位ですか？
									</span>
								</div>

								<div class="kutikomizoomupkoumokuhonbun">
									<? echo $voice_period;?>
								</div>
								<? } ?>

								<? if($voice_comment_1!="") { ?>
								<div class="kutikomizoomupkoumokuwaku">
									<span class="kutikomizoomupkoumokutitle">
										<!--口コミ本文各項目タイトル-->
										お店のいいと思った所を教えて下さい。
									</span>
								</div>

								<div class="kutikomizoomupkoumokuhonbun">
									<? echo $voice_comment_1;?>
								</div>
								<? } ?>

								<? if($voice_comment_2!="") { ?>
								<div class="kutikomizoomupkoumokuwaku">
									<span class="kutikomizoomupkoumokutitle">
										<!--口コミ本文各項目タイトル-->
										お店の改善してほしい所を教えて下さい。
									</span>
								</div>

								<div class="kutikomizoomupkoumokuhonbun">
									<? echo nl2br($voice_comment_2)?>
								</div>
								<? } ?>

								<? if($voice_comment_3!="") { ?>
								<div class="kutikomizoomupkoumokuwakul">
									<span class="kutikomizoomupkoumokutitle">
										<!--口コミ本文各項目タイトル-->
										評価の内容を出来るだけ詳しく教えて下さい。
									</span>
								</div>

								<div class="kutikomizoomupkoumokuhonbun">
									<!--口コミ本文各項目内容-->
									<? echo nl2br($voice_comment_3)?>
								</div>
								<? } ?>


							</div>




							<div class="kutikomizoomupbottmwaku">
								<!--口コミ本文下の枠-->



								<div class="kutikomizoomupbottmwakuleft"><a href="#"><i class="fa fa-heart"></i>参考になった(24)</a>
								</div>
								<div class="kutikomizoomupbottmwakuright"><a href="mailto:info@example.com?subject=No【】の口コミの違反報告&amp;body=口コミNo【】の口コミが違反してます。">この口コミを報告</a>
								</div>
								<div class="kutikomizoomupbottmwakucen"></div>

							</div>
						</div>


						<div class="kutikomipagenation">
							<div id="kutikomipagenationbotan">
								<ul>
									<? if(!is_null($result_voice_before[0]["voice_id"])) { ?>
									<li><a href="<? echo $_SERVER['PHP_SELF'];?>?voice_id=<? echo $result_voice_before[0][" voice_id "];?>"><i class="fa fa-chevron-left"></i>前の口コミを見る</a>
									</li>
									<? } ?>
									<li><a href="./"><i class="fa fa-undo"></i>口コミリストに戻る</a>
									</li>
									<? if(!is_null($result_voice_next[0]["voice_id"])) { ?>
									<li><a href="<? echo $_SERVER['PHP_SELF'];?>?voice_id=<? echo $result_voice_next[0][" voice_id "];?>">次の口コミを見る<i class="fa fa-chevron-right"></i></a>
									</li>
									<? } ?>
								</ul>
							</div>
						</div>
						<!--kutikomipagenation-->

						<div class="prvoicebottom">
							<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/app/pr/voicebottom.php"); ?>
						</div>


					</div>
					<!--zoom-->
				</div>
				<!--mainbox-->



				<div id="sub">

					<!--検索-->
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
					<!--検索-->

					<!--似た求人-->
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/similar.php"); ?>
					<!--似た求人-->

					<!--観覧履歴-->
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/history.php"); ?>
					<!--観覧履歴-->

					<!--広告部分その1　280×250-->
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner.php"); ?>
					<!--広告部分その1　280×250-->

					<!--検索リスト-->
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
					<!--検索リスト-->

					<!--検索-->
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchfrom.php"); ?>
					<!--検索-->

					<!--広告部分その2　280×250-->
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner2.php"); ?>
					<!--広告部分その2　280×250-->

				</div>


			</div>
			<!--mainbox-->



			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>

		</div>
		<!--contentsin-->



		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>

	</div>
	<!--container-->
</body> <
/html>