<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonVoice.php";
	$common_voice = new CommonVoice();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_cate_area = new CommonCateArea();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateJob.php";
	$common_cate_job = new CommonCateJob();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
	
	//エリア
	$arr_area = $common_cate_area->Fn_cate_area_list();
	if(!is_null($arr_area))
	{
		foreach($arr_area as $arr_key=>$arr_value)
		{
			$arr_cate_area_s_id[$arr_value["cate_area_s_id"]] = $arr_value["cate_area_l_title"];
			$arr_cate_area_s_ids[$arr_value["cate_area_l_id"]][] = $arr_value["cate_area_s_id"];
		}
	}
	
	//職種
	$arr_job = $common_cate_job->Fn_db_cate_job_all();
	if(!is_null($arr_job))
	{
		foreach($arr_job as $arr_key=>$arr_value)
		{
			$arr_cate_job_id[$arr_value["cate_job_id"]] = $arr_value["cate_job_title"];
		}
	}
	
	foreach($_GET as $key => $value)
	{ 
        $$key = $common_connect->h($value);
	}
	
	$meta_title = "ユーザーが投稿した口コミ一覧";
	$meta_description = "実際に働いたり面接のにいった女の子の口コミでお店選びの参考に！";
	$meta_keywords = "口コミ,大エリア,小エリア,業種";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>
<!--ページ専用のCSS 口コミ部分のデザイン-->
<link href="/kanto/voice/css/kutimomirist.css" rel="stylesheet" type="text/css" />
<link href="/kanto/voice/css/ristkutikomidezain.css" rel="stylesheet" type="text/css" />

<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<script type="text/javascript">
	$(function() {
		$('input:checkbox').click(function() {
			
			if($(this).attr("id").substr(0, 2) == "ll")
			{
				if ($(this).is(':checked')) {
					$('.'+$(this).attr("id")).prop('checked',true);
				} else {
					$('.'+$(this).attr("id")).prop('checked',false);
				}
			}
			
			if($(this).attr("class").substr(0, 2) == "ll")
			{
				//alert($(this).attr("class"));
				//alert($('.'+$(this).attr("class")+':checked').length );
				if($('.'+$(this).attr("class")).length==$('.'+$(this).attr("class")+':checked').length) {
					$('#'+$(this).attr("class")).prop('checked',true);
				} else {
					$('#'+$(this).attr("class")).prop('checked',false);
				}
			}
		});
	});
	
//-->
</script>
</head>
<body>

<div id="container">

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>


  <div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
    <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>
    <div id="mainbox"><!--コンテンツの外枠-->


	<div id="main" class="pt6"><!--コンテンツ左680幅-->


		<ul id="pan">
			<li><a href="index.html">トップページ</a></li>
			<li><a href="index.html">*****</a></li>
			<li><a href="index.html">*****</a></li>
			<li>*****</li>
		</ul>

		<div class="kutikomiristwaku">



<?php
	$arr_voice_ranking = $common_voice ->Fn_voice_ranking();
	
	$view_count=10;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$arr_where = array();
	$var = "s_keyword";
	if($$var != "")
	{
		$arr_where[$var] = $$var;
	}
	
	$var = "flag_open";
	if($$var != "")
	{
		$arr_where[$var] = $$var;
	}
	
	$var = "s_voice_star";
	if($$var != "")
	{
		$arr_where["voice_star"] = $$var;
	}
	
	$var = "s_pickup";
	if($$var != "")
	{
		$arr_where["shop_view_level"] = $$var;
	}
	
	
	
	//エリア
	$arr_where_area_s_id = array();
	if(!is_null($_GET["al"])) {
		foreach($_GET["al"] as $key=>$value) {
			foreach($arr_cate_area_s_ids[$value] as $cate_s_value) {
				$arr_where_area_s_id[] = $cate_s_value;
			}
		}
	}
	
	//職種
	$arr_where_job_id = array();
	if(!is_null($_GET["job"])) {
		foreach($_GET["job"] as $key=>$value) {
			$arr_where_job_id[] = $value;
		}
	}
	
	//合計
	$all_count = 0;

    $arr_bind = array();
    $where = "";
    $where .= " and s.flag_open=1 and v.flag_open=1  ";
    
    //エリア
    $where_area = "";
    if(!is_null($arr_where_area_s_id)) { 
        foreach($arr_where_area_s_id as $key=>$value) {
            $where_area .= ",".$value;
        }
    }
    if($where_area != "") {
        $where .= " and v.cate_area_s_id in (".substr($where_area, 1).")";
    }
    
    //職種
    $where_job = "";
    if(!is_null($arr_where_job_id)) { 
        foreach($arr_where_job_id as $key=>$value) {
            $where_job .= ",".$value;
        }
    }
    if($where_job != "") {
        $where .= " and v.cate_job_id in (".substr($where_job, 1).")";
    }

    if($s_keyword!="")
    {
        $where .= " and (s.shop_name collate utf8_unicode_ci like '%".$s_keyword."%' or voice_title collate utf8_unicode_ci like '%".$s_keyword."%') ";
    }
    if($voice_star!="")
    {
        $where .= " and voice_star>='".$voice_star."'' ";
    }
    $sql = " SELECT count(v.voice_id) as all_count FROM voice v inner join shop s on s.shop_id=v.shop_id where 1 ".$where ;

    $arr_voice_all = $common_dao->db_query_bind($sql, $arr_bind);
	$all_count = $arr_voice_all[0]["all_count"];
	
	//リスト表示
	$arr_data = array("voice_id", "shop_name", "member_id", "v.cate_area_s_id", "v.cate_job_id");
	$arr_data = array_merge($arr_data, array("voice_title", "voice_period", "voice_star"));
	$arr_data = array_merge($arr_data, array("voice_comment_1", "voice_comment_2", "voice_comment_3", "ranking", "v.regi_date", "v.up_date"));


    $sql = "SELECT ";
    foreach($arr_data as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM voice v inner join shop s on s.shop_id=v.shop_id where 1 ".$where ;
    if($order_name!="")
    {
        $sql .= " order by ".$order_name." ".$order;
    }
    else
    {
        $sql .= " order by v.regi_date desc";
    }
    $sql .= " limit $offset,$view_count";
    $arr_voice_list = $common_dao->db_query_bind($sql, $arr_bind);
?>
		<!--口コミヘッダー-->
			<div class="kutikomihed"><!--口コミヘッダー-->

				<div class="kutikomihednaka"><!--口コミ件数-->
					<span class="kutikomikazu">
					<? if($all_count>($page*$view_count)) { echo ($view_count*($page-1))+1;} else { echo $all_count;} ?>
          ～
					<? if($all_count<($page*$view_count)) { echo $all_count;} else { echo ($view_count*$page);}?>件
          </span>
          まで表示中 / 投稿口コミ数<span class="kutikomikazu"><? echo number_format($all_count);?>件</span>
				</div>
				<div class="kutikomihedsita">
					口コミはあくまで投稿者の『主観』による部分で左右される傾向があります。あくまで口コミは一つの『目安』です。一つの基準、参考としてください。個人的な中傷や恨みなどの書き込み、個人情報記載などがありましたら編集部までご連絡ください。
					またこの情報は投稿者が働いていた時期の情報の可能性が高いです。よって現在とは事実が異なる場合がありますのでご注意下さい。
				</div>


				<div class="kutikomisearchwaku"><!--検索バー、並び替え外枠-->

				<!--ソート-->
<?
		$query_order = "";
		foreach($_GET as $key => $value){ 
			if($key!="order_name" && $key!="order")
			{
				if(is_array($value))
				{
					foreach($_GET[$key] as $key_arr => $value_arr){ 
						$query_order .= "&".$key."[]=".trim($value_arr);
					}
				}
				else
				{
					$query_order .= "&".$key."=".trim($value);
				}
			}
		}
?>
					<div class="kutikomisearchkleft">
						<ul class="kutikomisout">
							<li><i class="fa fa-refresh"></i>並び替え→</li>
							<li><a href="<? echo $_SERVER['PHP_SELF'];?>?order_name=voice_star&order=desc<? echo $query_order;?>">『評価の高い順』</a></li>
							<li><a href="<? echo $_SERVER['PHP_SELF'];?>?order_name=regi_date&order=desc<? echo $query_order;?>">『新着順』</a></li>
							<li><a href="<? echo $_SERVER['PHP_SELF'];?>?order_name=cate_job_id<? echo $query_order;?>">『業種順』</a></li>
							<li><a href="<? echo $_SERVER['PHP_SELF'];?>?order_name=cate_area_s_id<? echo $query_order;?>">『エリア順』</a></li>
						</ul> 

					</div>
				<!--ソート-->

				<!--検索バー-->
					<div class="kutikomisearchright">
						<form method="get" action="<? echo $_SERVER['PHP_SELF']?>">
							<input name="s_keyword" type="search" placeholder="お店の名前で検索" value="<? echo $s_keyword;?>">
							<input type="image" value="検索" src="/kanto/common/light/img/search01.png">
						</form>
					</div>
				<!--検索バー-->







				</div><!--検索バー、並び替え外枠-->


			</div>
<!--口コミヘッダー-->





			
				<div class="mycatch"> 新着口コミ一覧 </div>
				<div class="kutikomis-waku">
				
<?

	foreach($arr_voice_list as $arr_key=>$arr_value)
	{
		foreach($arr_value as $key=>$value)
		{
			$$key=$value;
		}
?>

<!--1口コミ-------------------------------------------------------------------->
				<div class="kutikomibox">

				<!--プロフィール画像-->
					<div class="plofimg">
						<?
              $arr_data = array();
              $arr_data[]="member_id";
              $arr_data[]="nickname";
              $arr_data[]="member_img";
              $return_member = $common_member->Fn_db_member_data ($member_id, $arr_data);
              if(!is_null($return_member)) {
            ?>
              <? if($return_member[0]["member_img"]!="") { ?>
                <img src="/<? echo global_member_dir.$return_member[0]["member_id"]."/".$return_member[0]["member_img"];?>" width="60" height="60" alt="<? echo $return_member[0]["nickname"];?>" />
              <? } else { ?>
              	<img src="/img/noimg/user-plf.png" width="60" height="60" alt="<? echo $return_member[0]["nickname"];?>" />
              <? } ?>
              <br />
              <? echo $return_member[0]["nickname"];?>
            <? } ?>

            <? if($arr_voice_ranking[$ranking]!="") { ?>
              <div class="shopkutikomiranks">
                <? echo $arr_voice_ranking[$ranking];?>
              </div>
            <? } ?>

					</div>

					<div class="shopkutikomi shopkutikomi-left">
						<div class="kutikomirisutoshop">
							<?
                $arr_data = array(); $arr_data[]="shop_name";
                $arr_where = array(); $arr_where["shop_id"]=$shop_id;
								$return_shop = $common_shop->Fn_db_shop_data ($arr_data, $arr_where);
								if(!is_null($return_shop)) {
							?>
            		<a href="/kanto/shop/?shop_id=<? echo $shop_id;?>"><? echo $return_shop[0]["shop_name"];?></a>
              <? } ?>
              <span class="workareakutikomi">(<? echo $arr_cate_area_s_id[$cate_area_s_id];?>/<? echo $arr_cate_job_id[$cate_job_id];?>)</span>
            </div>
							<hr class="indexsikiri">
						<div class="kutikomititleindex"><a href="./detail.php?voice_id=<? echo $voice_id;?>"><? echo $voice_title;?></a></div>
						<div class="indexsterpink"><img src="/kanto/common/img/star<? echo sprintf('%.1f',($voice_star/2));?>m.png"><span>【<? echo sprintf('%.2f',($voice_star/2));?>点】</span></div>

						<div class="kutikomiristhonbun">
							<? echo nl2br($voice_comment_1);?>
						</div>

						<div class="nextkutikomis"><a href="./detail.php?voice_id=<? echo $voice_id;?>">続きを見る..</a></div>
							<div class="time"><? echo substr($regi_date, 0, 10);?>投稿</div>
					</div>

				</div>
				
<!--1口コミ-------------------------------------------------------------------->
<?
	}
?>
		
			</div>


			<div class="ander">
				<div id="nextcaunt">
					<?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
				</div>
				<div class="ander-clear"></div>
			</div>

		</div><!--kutikomiristwaku-->


	</div><!--main-->










	<div id="sub"><!--sub------------------------------------------------------------------------->

        <!--口コミを絞り込む-->
      <form method="get" action="<? echo $_SERVER['PHP_SELF']?>">
      <input name="s_keyword" type="hidden" value="<? echo $s_keyword;?>">
      <h3 class="text">口コミを絞り込む</h3>
        <div id="lesearch">
          <div class="lesearchtitle">【エリアから口コミを絞る】</div>
          
          <ul class="lesearchtext">
            <li>
              <label><input type="checkbox" name="area_l[]" id="ll_area">全エリア</label>
            </li>
            <?
				$arr_where = array();
				$arr_where["flag_open"] = "1";
				$result_area_l = $common_cate_area->Fn_cate_area_l_list($arr_where);
				
				foreach($result_area_l as $arr_area_l) {
			?>
            <li><label><input type="checkbox" name="al[]" value="<? echo $arr_area_l["cate_area_l_id"];?>" class="ll_area" id="ll<? echo $arr_area_l["cate_area_l_id"];?>" <? if(!is_null($_GET["al"])) { if(in_array($arr_area_l["cate_area_l_id"], $_GET["al"])) { echo " checked ";};}?>><? echo $arr_area_l["cate_area_l_title"];?></label></li>
            <? } ?>
          </ul>
      
          <div class="boxline"><!--仕切り線-->
          <img src="/kanto/common/light/img/shoptitlelines.gif"/>
          </div>
          
          <!--再検索業種-->
          <div class="lesearchtitle">【職種から口コミを絞る】</div>
      
          <ul class="lesearchtext">
            <li>
              <label><input type="checkbox" name="j[]" id="ll_job">業種全て</label>
            </li>
            <?
				$arr_where = array();
				$arr_where["flag_open"] = "1";//公開のみ
				
				$arr_data = array();
				$arr_data[] = "cate_job_id";
				$arr_data[] = "cate_job_title";
				
				$result_job = $common_cate_job->Fn_db_cate_job_list($arr_data, $arr_where);
				
				foreach($result_job as $arr_job) {
                $cate_job_id = $arr_job["cate_job_id"];
                $cate_job_title = $arr_job["cate_job_title"];
			?>
            <li><label><input type="checkbox" name="job[]" value="<? echo $cate_job_id;?>" class="ll_job" id="ll<? echo $cate_job_id;?>" <? if(!is_null($_GET["job"])) { if(in_array($cate_job_id, $_GET["job"])) { echo " checked ";};}?>><? echo $cate_job_title;?></label></li>
            <? } ?>
            
            
            
            <div class="boxline"><!--仕切り線-->
            <img src="/kanto/common/light/img/shoptitlelines.gif"/>
            </div>
            
            <!--絞り込み-->
            <div class="lesearchtitle">【評価点で口コミを絞る】</div>
            <li>
                <? $var = "s_voice_star";?>
                <label><input type="radio" name="<? echo $var;?>" value="6" <? if($$var=="6") { echo " checked ";}?>>3点～</label> 
                <label><input type="radio" name="<? echo $var;?>" value="7" <? if($$var=="7") { echo " checked ";}?>>3.5点～</label> 
                <label><input type="radio" name="<? echo $var;?>" value="8" <? if($$var=="8") { echo " checked ";}?>>4点～</label> 
                <label><input type="radio" name="<? echo $var;?>" value="9" <? if($$var=="9") { echo " checked ";}?>>4.5点～</label>
            </li>
            <div class="lesearchtitle">【オススメ店のみの口コミを表示】</div>
            <li>
                <? $var = "s_pickup";?>
              <label><input type="radio" name="<? echo $var;?>" value="30" <? if($$var=="30") { echo " checked ";}?>>絞らない</label>
              <label><input type="radio" name="<? echo $var;?>" value="99" <? if($$var=="99") { echo " checked ";}?>>絞る</label>
            </li>
            
            <div class="lesearchbotan"><!--検索ボタン-->
            <button class="endbtn" id="setDrag" type="submit">この条件で絞り込む</button>
            </div>
          </ul>
      
      </div>
      </form>
      <!--検索リストブロックここまで-->
        <!--口コミを絞り込む-->
	<!--バナー-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner3.php"); ?>
	<!--バナー-->
	</div>




</div>
<!--main boxコンテンツの外枠-->


		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>
</div><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
<!--contentsin-->



	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>

</div><!--container-->
</body>
</html>

