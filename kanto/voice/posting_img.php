<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonContents.php";
	$common_contents = new CommonContents();
	
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShopImg.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateJob.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_shop = new CommonShop();
	$common_shopimg = new CommonShopImg();
	$common_cate_job = new CommonCateJob();
	$common_cate_area = new CommonCateArea();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = trim($common_dao->db_string_escape($value));
		//$$key = $value;
	}
	if($shop_id=="") { $common_connect -> Fn_javascript_back("正しく入力してください。"); }
	
	//ショップの基本情報
	$arr_where = array();
	$var = "flag_open";
	$arr_where[$var] = "1"; //公開のみ
	$var = "shop_id";
	$arr_where[$var] = $$var;
	
	$arr_data = array("shop_id", "shop_name", "shop_kana", "cate_area_s_id", "area_detail", "cate_job_id", "shop_title", "evaluation");

	$db_result_shop = $common_shop -> Fn_db_shop_data ($arr_data, $arr_where);
	foreach($db_result_shop as $arr_shop)
	{
		foreach($arr_shop as $key=>$value)
		{
			$$key = $value;
		}
	}
	
	if($shop_name=="")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
	
	$meta_title = $shop_name."タイトル";
	$meta_description = "description です";
	$meta_keywords = "キーワードです";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!--ページ専用のCSS-->
<link href="/kanto/shop/css/shopjob.css" rel="stylesheet" type="text/css" />
<link href="/kanto/voice/css/kutikomifromshopextra.css" rel="stylesheet" type="text/css" />

<!--口コミ投稿ボタン用CSS-->
<link rel="stylesheet" href="/kanto/app/desing/githubbutton/gh-buttons.css">
<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

<!--アプリケーション/フォームデザイン-->
<link href="/kanto/app/form/formoid1/formoid-default-skyblue.css" rel="stylesheet" type="text/css" />


<!--アプリーション文字カウント-->

<script type="text/javascript">
$(function(){
    var countMax = 10;
    $('input').bind('keydown keyup keypress change',function(){
        var thisValueLength = $(this).val().length;
        var countDown = (countMax)-(thisValueLength);
        $('.count').html(countDown);
 
        if(countDown < 0){
            $('.count').css({color:'#ff0000',fontWeight:'bold'});
        } else {
            $('.count').css({color:'#000000',fontWeight:'normal'});
        }
    });
    $(window).load(function(){
        $('.count').html(countMax);
    });
});

</script>

<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input_in("voice_img_title", 10);
			err_check_count += check_input_sel("img_1");
			
			
			
			if(err_check_count!=0)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				$('#form_confirm').submit();
				//$('#form_confirm', "body").submit();
				return true;
			}
		});
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div style='color:#F00'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		
		//以内
		function check_input_in($str, $moji_count) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div style='color:#F00'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if($('#'+$str).val().replace(/\s+/g,'').length>$moji_count)
			{
				err ="<div style='color:#F00'>"+$moji_count+"文字以内正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}
		
		function check_input_sel($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div style='color:#F00'>正しく選択してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		

	});

</script>
</head>



<body>

<div id="container">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>

  <div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
    <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>
    <div id="mainbox"><!--コンテンツの外枠-->


	<div id="main" class="pt6"><!--コンテンツ左680幅-->


		<ul id="pan">
			<li><a href="index.html">トップページ</a></li>
			<li><a href="index.html">*****</a></li>
			<li><a href="index.html">*****</a></li>
			<li>*****</li>
		</ul>

		<div id="shoptop">
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/shop_summery.php"); ?>
		</div><!--shoptop-->


		<div class="toukouwaku">

			<div class="toukouhed">
				<span style="font-size: 20px; color: #e80f57;"><i class="fa fa-comments"></i></span>クラブミリオンで面接又は働いた事のある方は、知ってるお店の情報を教えてくれませんか？
			</div>

			<div class="toukoutixyuui">
				<span class="redtext">
					<i class="fa fa-exclamation-triangle"></i>口コミを一度投稿するとユーザーからは削除が出来ません。</span>記入は責任を持ってよく考えてから行ってください。</br>
					個人の中傷や恨み、他者の個人情報、思い込みで決め付けた内容、あきらかな嘘などの投稿は一切禁止をしてます。</br>
					お店には関係のない情報などの書き込み、勧誘やPRなどの書き込みも全て禁止をしております。</br>
					発覚した場合は削除、アカウントの停止の（永久登録不可）可能性もありますのでご注意下さい。</br>
					他、詳しい投稿のルールは<a href=""target="_blank" >口コミ投稿のルール</a>を確認して下さい。またコソットでは、</br>
					正しい情報を得る為、店舗側からの口コミ投稿を禁止してます。<a href=""target="_blank" >詳しくはコソットが出来た意味</a>をご覧下さい。</br>
					全ての働く女性が正しい情報の中で、自分に合ったお店選びを出来る事をコソットは目的としてます。
			</div>

			<div id="postingmenu">
				<ul>
					<li><a href="/kanto/voice/posting_voice.php?shop_id=<? echo $shop_id;?>">口コミを投稿する</a></li>
					<li><span class="activ">画像を投稿する</span></li>
				</ul> 
			</div>

      <form action="posting_img_save.php" class="formoid-default-skyblue" style="color:#666666;" method="post" enctype="multipart/form-data">
      <? $var = "shop_id";?>
      <input type="hidden" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>">
			<div class="postblok">
				
				<div class="kutikomitaitle"><i class="fa fa-camera"></i>このお店に関連する画像などを投稿してくれませんか？</div>
				
				<div class="toukoutixyuui">
					<span class="redtext">※お店に関係する画像のみの投稿をお願いします。全く関係のない画像、宣伝、ロゴなどは削除の対象になります。</span></br>
					お店の待機室の雰囲気、寮などの写真があると信憑性もありと判断され口コミ評価も上がります。
				</div>

				<p id="response"></p>
				<div id="dropto">
					<span class="upcamera">ここにファイルをドラック＆ドロップして下さい。</span>
					<div class="dropicon"><i class="fa fa-camera"></i></div>
				</div>

				<div id="progress"><div id="percent"></div></div>
        <? $var = "img_1";?>
				または<input name="<? echo $var;?>" id="<? echo $var;?>" type="file" accept="image/*">
        <label id="err_<?=$var;?>"></label>

<script>
(function() {
	var dropFileUpload = function(arg) {
		this.dropElm  = document.querySelector(arg.dropElm);
		this.progress = document.querySelector(arg.progress);
		this.response = document.querySelector(arg.response);
		this.inputFile = document.querySelector(arg.inputFile);
	};

	dropFileUpload.prototype = {
		init: function() {
			var self = this;
			this.dropElm.addEventListener('drop', function(e) {
				self.drop.call(self, e);
			}, false);
			this.dropElm.addEventListener('dragover', function(e) {
				self.dragOver.call(self, e);
			}, false);
			this.inputFile.addEventListener('change', function(e) {
				self.handleFiles.call(self, e);
			}, false);
		},
		dragOver: function(e) {
			e.preventDefault();
			this.dropElm.setAttribute('class', 'over');
		},
		drop: function(e) {
			e.preventDefault();
			this.dropElm.setAttribute('class', '');
			var readerFile =  e.dataTransfer.files[0];
			var file =  e.dataTransfer.files;
			this.handleFileSelect.call(this, readerFile, file);
		},
		handleFiles: function(e) {
			var readerFile = e.target.files[0];
			var file =  e.target.files;
			this.handleFileSelect.call(this, readerFile, file);
		},
		handleFileSelect: function(readerFile, file) {
			var self = this;
			if(!file && !readerFile)
				return;
			self.progress.style.width = '0%';
			self.progress.textContent = '0%';
			var reader = new FileReader();
			reader.addEventListener('error', function(e) {
				self.fileErrorHandler.call(self, e);
			}, false);
			reader.addEventListener('progress', function(e) {
				self.updateProgress.call(self, e);
			}, false);
		    reader.onabort = function(e) {
		      self.response.innerHTML = 'File read cancelled';
		    };
			reader.onload = (function(e) {
				self.upload.call(self, file);
		    });
		    reader.readAsBinaryString(readerFile);
		},
	  	updateProgress: function(e) {
		  	if (e.lengthComputable) {//全体のサイズがわかっているかどうか
		  		var percentLoaded = Math.round((e.loaded / e.total) * 100);
		  		//if (percentLoaded < 100) {
		  			this.progress.style.width = percentLoaded + '%';
		  			this.progress.textContent = percentLoaded + '%';
		  		//}
		  	}
		 },
		fileErrorHandler: function(e) {
		    switch(e.target.error.code) {
				case e.target.error.NOT_FOUND_ERR:
					this.response.innerHTML = 'File Not Found!';
					break;
				case e.target.error.NOT_READABLE_ERR:
					this.response.innerHTML = 'File is not readable';
					break;
				case e.target.error.ABORT_ERR:
					break;
				default:
					this.response.innerHTML = 'An error occurred reading this file.';
		    };
	  	},
		upload: function (files) {
		    self.response.innerHTML = 'アップロードに成功しました';
		}
	};

	var df = new dropFileUpload({
		dropElm: '#dropto',
		progress: '#percent',
		response: '#response',
		inputFile: '#input-file'
	});
	df.init();
})();
</script>

			<hr class="sikiri">

				<div class="element-textarea">
					<label class="title">アップした画像はお店のどんな画像ですか？(<span class="redcount">※10文字以内で</span>入力して下さい)</label>
          <? $var = "voice_img_title"; ?>
						<input class="small" type="text" name="<? echo $var;?>" id="<? echo $var;?>" required placeholder="10文字以内※必須項目"><br>
            <span class="count">0</span><span class="zantext">文字以内</span><span class="icon-place"></span>
            <label id="err_<?=$var;?>"></label>
				</div>


				<div class="toukoutixyuui">※口コミも画像投稿もよく考えて投稿をお願い致します。</br>
					全ての投稿は<a href="">口コミのルール</a>をしっかり読み、全項目に同意したと判断されます。
				</div>


					<div class="submit">
							<? $var = "form_confirm";?>
              <input name="<? echo $var;?>" id="<? echo $var;?>" type="submit" value="規約を守り画像を投稿する"/>
					</div>

			</div>
      </form>

<!--SHOPタブメニューここまで-->




		</div><!--toukouwaku-->

	</div><!--main-->
      
<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /end /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
  
  
  
  
  
      <div id="sub"><!--sub------------------------------------------------------------------------->

      
        <!--似た求人-->
        <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/similar.php"); ?>
        <!--似た求人-->
        
        <!--観覧履歴-->
        <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/history.php"); ?>
        <!--観覧履歴-->
        
        <!--広告部分その1　280×250-->
        <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner.php"); ?>
        <!--広告部分その1　280×250-->
        
        <!--検索リスト-->
        <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
        <!--検索リスト-->

        <!--広告部分その2　280×250-->
        <?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner2.php"); ?>
        <!--広告部分その2　280×250-->
        

      </div>

    </div>
    <!--main boxコンテンツの外枠-->
  
  
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>
  </div><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
  <!--contentsin-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
</div><!--container-->
</body>
</html>

