<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像

	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	$common_email = new CommonEmail(); //メール関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonVoiceImg.php";
	$common_voice_img = new CommonVoiceImg();
	
	$meta_title = "画像を投稿する";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/".$global_area."/common/header/header_meta.php";?>
</head>

<body>
<?
	$datetime = date("Y-m-d H:i:s");
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($voice_img_title=="" || $shop_id=="" )
	{
		$common_connect-> Fn_javascript_back("正しく入力して下さい。");
	}
	
	//ショップの基本情報
	$arr_data = array();
	$arr_data[] = "cate_area_s_id"; 
	$arr_data[] = "cate_job_id"; 
	$arr_data[] = "shop_name"; 
	
	$arr_where = array();
	$arr_where["flag_open"] = "1"; //公開のみ
	$arr_where["shop_id"] = $shop_id;
	$db_result_shop = $common_shop -> Fn_db_shop_data($arr_data, $arr_where);
	if(!is_null($db_result_shop))
	{
		$shop_name = $db_result_shop[0]["shop_name"];
		$cate_area_s_id = $db_result_shop[0]["cate_area_s_id"];
		$cate_job_id = $db_result_shop[0]["cate_job_id"];
	}
	
	if($shop_name=="")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
	
	$datetime = date("Y/m/d H:i:s");
	$member_id = $_SESSION['member_id'];
	if($member_id=="") { $member_id = 0;}
	$flag_open = 1;
	
	//array
	$arr_data = array();
	$arr_data["voice_img_title"] = $voice_img_title;
	$arr_data["member_id"] = $member_id;
	$arr_data["shop_id"] = $shop_id;
	$arr_data["flag_open"] = "1";

	$common_voice_img -> Fn_voice_img_insert ($arr_data);

	$result_voice_img_id = $common_voice_img -> Fn_db_voice_img_auto();
	$voice_img_id = $result_voice_img_id[0]["last_id"];

	//Folder生成
	$save_dir = $global_path.global_voice_img_dir.$voice_img_id."/";
	$common_image -> create_folder ($save_dir);
	
	$new_end_name="_1";
	$fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $voice_img_id, $text_img_1, "");//400
	
	$arr_data = array();
	$arr_data["img_1"] = $fname_new_name[1];
	
	$arr_where = array();
	$arr_where["voice_img_id"] = $voice_img_id;

	$common_voice_img -> Fn_voice_img_update ($arr_data, $arr_where);

	$common_connect-> Fn_javascript_move("画像投稿ありがとうございます。", global_no_ssl."/".$global_area."/shop/?shop_id=".$shop_id);
?>

</body>

</html>
