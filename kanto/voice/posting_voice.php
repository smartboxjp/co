<?php
require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_include/connect.php";
$common_connect = new CommonConnect();
$common_dao = new CommonDao(); //DB関連

require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_include/CommonContents.php";
$common_contents = new CommonContents();


require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_model/CommonShop.php";
require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_model/CommonShopImg.php";
require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_model/CommonCateJob.php";
require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_model/CommonCateArea.php";
$common_shop = new CommonShop();
$common_shopimg = new CommonShopImg();
$common_cate_job = new CommonCateJob();
$common_cate_area = new CommonCateArea();

foreach ( $_GET as $key => $value ) {
	$$key = trim( $common_dao->db_string_escape( $value ) );
	//$$key = $value;
}
if ( $shop_id == "" ) {
	$common_connect->Fn_javascript_back( "正しく入力してください。" );
}


//ショップの基本情報
$arr_where = array();
$var = "flag_open";
$arr_where[ $var ] = "1"; //公開のみ
$var = "shop_id";
$arr_where[ $var ] = $$var;

$arr_data = array( "shop_id", "shop_name", "shop_kana", "cate_area_s_id", "area_detail", "cate_job_id", "shop_title", "evaluation" );

$db_result_shop = $common_shop->Fn_db_shop_data( $arr_data, $arr_where );
foreach ( $db_result_shop as $arr_shop ) {
	foreach ( $arr_shop as $key => $value ) {
		$$key = $value;
	}
}

if ( $shop_name == "" ) {
	$common_connect->Fn_javascript_back( "正しく入力してください。" );
}

$meta_title = $shop_name . "タイトル";
$meta_description = "description です";
$meta_keywords = "キーワードです";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!--ページ専用のCSS-->
<link href="/kanto/shop/css/shopjob.css" rel="stylesheet" type="text/css"/>
<link href="/kanto/voice/css/kutikomifromshopextra.css" rel="stylesheet" type="text/css"/>

<!--口コミ投稿ボタン用CSS-->
<link rel="stylesheet" href="/kanto/app/desing/githubbutton/gh-buttons.css">
<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

<!--アプリケーション/フォームデザイン-->
<link href="/kanto/app/form/formoid1/formoid-default-skyblue.css" rel="stylesheet" type="text/css"/>


<!--アプリーション文字カウント-->

<script type="text/javascript">
	$( function () {
		var countMax = 30;
		$( 'input' ).bind( 'keydown keyup keypress change', function () {
			var thisValueLength = $( this ).val().length;
			var countDown = ( countMax ) - ( thisValueLength );
			$( '.count' ).html( countDown );

			if ( countDown < 0 ) {
				$( '.count' ).css( {
					color: '#ff0000',
					fontWeight: 'bold'
				} );
			} else {
				$( '.count' ).css( {
					color: '#000000',
					fontWeight: 'normal'
				} );
			}
		} );
		$( window ).load( function () {
			$( '.count' ).html( countMax );
		} );
	} );

	$( function () {
		$( '.comment1,.comment2,.comment3' ).bind( 'keyup', function () {
			for ( num = 1; num <= 3; num++ ) {
				var thisValueLength = $( ".comment" + num ).val().replace( /\s+/g, '' ).length; // ←★replace()を追加
				$( ".count" + num ).html( thisValueLength );
			}
		} );
	} );
</script>


<script type="text/javascript">
	$( function () {
		$( '#form_confirm' ).click( function () {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input_in( "voice_title", 30 );
			err_check_count += check_input_sel( "voice_period" );
			err_check_count += check_radio( "voice_star" );
			err_check_count += check_input_over( "voice_comment_1", 15 );
			err_check_count += check_input_over( "voice_comment_2", 15 );
			err_check_count += check_input_over( "voice_comment_3", 130 );



			if ( err_check_count != 0 ) {
				alert( "入力に不備があります" );
				return false;
			} else {
				//$('#form_confirm').submit();
				$( '#form_confirm', "body" ).submit();
				return true;
			}
		} );

		function check_input( $str ) {
			$( "#err_" + $str ).html( err_default );
			$( "#" + $str ).css( background, bgcolor_default );

			if ( $( '#' + $str ).val() == "" ) {
				err = "<div style='color:#F00'>正しく入力してください。</div>";
				$( "#err_" + $str ).html( err );
				$( "#" + $str ).css( background, bgcolor_err );

				return 1;
			}
			return 0;
		}


		//以内
		function check_input_in( $str, $moji_count ) {
			$( "#err_" + $str ).html( err_default );
			$( "#" + $str ).css( background, bgcolor_default );

			if ( $( '#' + $str ).val() == "" ) {
				err = "<div style='color:#F00'>正しく入力してください。</div>";
				$( "#err_" + $str ).html( err );
				$( "#" + $str ).css( background, bgcolor_err );

				return 1;
			} else if ( $( '#' + $str ).val().replace( /\s+/g, '' ).length > $moji_count ) {
				err = "<div style='color:#F00'>" + $moji_count + "文字以内正しく入力してください。</div>";
				$( "#err_" + $str ).html( err );
				$( "#" + $str ).css( background, bgcolor_err );

				return 1;
			}

			return 0;
		}

		//以上
		function check_input_over( $str, $moji_count ) {
			$( "#err_" + $str ).html( err_default );
			$( "#" + $str ).css( background, bgcolor_default );

			if ( $( '#' + $str ).val() == "" ) {
				err = "<div style='color:#F00'>正しく入力してください。</div>";
				$( "#err_" + $str ).html( err );
				$( "#" + $str ).css( background, bgcolor_err );

				return 1;
			} else if ( $( '#' + $str ).val().replace( /\s+/g, '' ).length < $moji_count ) {
				err = "<div style='color:#F00'>" + $moji_count + "文字以上正しく入力してください。</div>";
				$( "#err_" + $str ).html( err );
				$( "#" + $str ).css( background, bgcolor_err );

				return 1;
			}

			return 0;
		}

		function check_input_sel( $str ) {
			$( "#err_" + $str ).html( err_default );
			$( "#" + $str ).css( background, bgcolor_default );

			if ( $( '#' + $str ).val() == "" ) {
				err = "<div style='color:#F00'>正しく選択してください。</div>";
				$( "#err_" + $str ).html( err );
				$( "#" + $str ).css( background, bgcolor_err );

				return 1;
			}
			return 0;
		}

		function check_radio( $str ) {
			err = "";
			$( "#err_" + $str ).html( err_default );

			if ( $( "input[name='" + $str + "']:checked" ).val() == undefined ) {
				err = "<br /><span style='color:#F00'>正しく選択してください。</span>";
			}

			if ( err != "" ) {
				$( "#err_" + $str ).html( err );
				return 1;
			}
			return 0;
		}


	} );
</script> <
/head>



<body>

	<div id="container">
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>

		<div id="contentsin">
			<!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>
			<div id="mainbox">
				<!--コンテンツの外枠-->


				<div id="main" class="pt6">
					<!--コンテンツ左680幅-->


					<ul id="pan">
						<li><a href="index.html">トップページ</a>
						</li>
						<li><a href="index.html">*****</a>
						</li>
						<li><a href="index.html">*****</a>
						</li>
						<li>*****</li>
					</ul>

					<div id="shoptop">
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/shop/include/shop_summery.php"); ?>
					</div>
					<!--shoptop-->



					<div class="toukouwaku">

						<div class="toukouhed">
							<span style="font-size: 20px; color: #e80f57;"><i class="fa fa-comments"></i></span>クラブミリオンで面接又は働いた事のある方は、知ってるお店の情報を教えてくれませんか？
						</div>

						<div class="toukoutixyuui">
							<span class="redtext">
					<i class="fa fa-exclamation-triangle"></i>口コミを一度投稿するとユーザーからは削除が出来ません。</span>
							記入は責任を持ってよく考えてから行ってください。</br>
							個人の中傷や恨み、他者の個人情報、思い込みで決め付けた内容、あきらかな嘘などの投稿は一切禁止をしてます。</br>
							お店には関係のない情報などの書き込み、勧誘やPRなどの書き込みも全て禁止をしております。</br>
							発覚した場合は削除、アカウントの停止の（永久登録不可）可能性もありますのでご注意下さい。</br>
							他、詳しい投稿のルールは<a href="" target="_blank">口コミ投稿のルール</a>を確認して下さい。またコソットでは、</br>
							正しい情報を得る為、店舗側からの口コミ投稿を禁止してます。<a href="" target="_blank">詳しくはコソットが出来た意味</a>をご覧下さい。</br>
							全ての働く女性が正しい情報の中で、自分に合ったお店選びを出来る事をコソットは目的としてます。
						</div>



						<div id="postingmenu">
							<ul>
								<li><span class="activ">口コミを投稿する</span>
								</li>
								<li><a href="/kanto/voice/posting_img.php?shop_id=<? echo $shop_id;?>">画像を投稿する</a>
								</li>
							</ul>
						</div>

						<div class="postblok">


							<div class="kutikomitaitle"><i class="fa fa-comments"></i>下記の項目を記入して投稿ボタンを押してください。</div>

							<div class="kutikomifrom">

								<form action="./posting_voice_save.php" style="color:#413324;" class="formoid-default-skyblue" method="post">
									<? $var = "shop_id";?>
									<input name="<? echo $var;?>" type="hidden" value="<? echo $shop_id;?>"/>
									<div class="element-input">
										<label class="title">1、口コミのタイトルを入れて下さい。(<span class="redcount">※30文字以内</span>で入力して下さい)</label>
										<div class="item-cont">
											<? $var = "voice_title";?>
											<input name="<? echo $var;?>" id="<? echo $var;?>" class="large" type="text" required placeholder="タイトルは30文字以内※必須項目"><span class="count">0</span><span class="zantext">文字</span><span class="icon-place"></span>
											<label id="err_<?=$var;?>"></label>
										</div>
									</div>

									<div class="element-select">
										<label class="title">2、このお店で働いた期間は？</label>
										<div class="medium">
											<span>
												<? $var = "voice_period";?>
												<select name="<? echo $var;?>" id="<? echo $var;?>">
													<option value="">未回答</option>
													<option value="面接だけ">面接だけ</option>
													<option value="1ヶ月未満">1ヶ月未満</option>
													<option value="半年未満">半年未満</option>
													<option value="1年未満">1年未満</option>
													<option value="1年以上">1年以上</option>
												</select><i></i>
											</span>
											<label id="err_<?=$var;?>"></label>
										</div>
									</div>


									<div class="element-rating">
										<label class="title">3、あなたのお店への評価 </label>
										<div class="hoshikizixyun">
											※星の目安【10～9　素晴らしい】【8～5　平均的】【4～2　イマイチ】【1～0　もう働きたくない】
										</div>

										<div class="rating">
											<? $var = "voice_star";?>
											<? for($loop=10 ; $loop>0 ; $loop--) { ?>
											<input type="radio" class="rating-input" id="rating-<? echo $loop;?>" name="<? echo $var;?>" value="<? echo $loop;?>"/>
											<label for="rating-<? echo $loop;?>" class="rating-star"></label>
											<? } ?>
											<hr class="sikiri100">
											<label id="err_<?=$var;?>"></label>
										</div>
									</div>

									<div class="element-textarea">
										<label class="title">4、お店のいいと思った所を教えて下さい。(<span class="redcount">※15文字以上</span>入力して下さい)</label>
										<? $var = "voice_comment_1";?>
										<textarea class="small comment1" name="<? echo $var;?>" id="<? echo $var;?>" required placeholder="15文字以上※必須項目"></textarea>
										<span class="count1">0</span><span class="zantext">文字</span>
										<label id="err_<?=$var;?>"></label>
									</div>

									<div class="element-textarea">
										<label class="title">5、お店の改善してほしい所を教えて下さい(<span class="redcount">※15文字以上</span>入力して下さい)</label>
										<? $var = "voice_comment_2";?>
										<textarea class="small comment2" name="<? echo $var;?>" id="<? echo $var;?>" required placeholder="15文字以上※必須項目"></textarea>
										<span class="count2">0</span><span class="zantext">文字</span>
										<label id="err_<?=$var;?>"></label>
									</div>

									<div class="element-textarea">
										<label class="title">6、評価の内容を出来るだけ詳しく教えて下さい。(<span class="redcount">※130文字以上</span>入力して下さい)</label>
										<? $var = "voice_comment_3";?>
										<textarea class="medium comment3" name="<? echo $var;?>" id="<? echo $var;?>" required placeholder="130文字以上※必須項目"></textarea>
										<span class="count3">0</span><span class="zantext">文字</span>
										<label id="err_<?=$var;?>"></label>
									</div>

									<div class="toukoutixyuui">※口コミも画像投稿もよく考えて投稿をお願い致します。</br>
										全ての投稿は<a href="">【口コミのルール】</a>をしっかり読み、全項目に同意したと判断されます。
									</div>

									<div class="submit">
										<? $var = "form_confirm";?>
										<input name="<? echo $var;?>" id="<? echo $var;?>" type="submit" value="規約を守り口コミを投稿する"/>
									</div>

								</form>
								<script type="text/javascript" src="/kanto/app/form/formoid1/formoid-default-skyblue.js"></script>

							</div>

						</div>

					</div>
					<!--toukouwaku-->





				</div>
				<!--main-->

				<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /end /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>





				<div id="sub">
					<!--sub------------------------------------------------------------------------->


					<!--似た求人-->
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/similar.php"); ?>
					<!--似た求人-->

					<!--観覧履歴-->
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/history.php"); ?>
					<!--観覧履歴-->

					<!--広告部分その1　280×250-->
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner.php"); ?>
					<!--広告部分その1　280×250-->

					<!--検索リスト-->
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/searchlist.php"); ?>
					<!--検索リスト-->

					<!--広告部分その2　280×250-->
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanner2.php"); ?>
					<!--広告部分その2　280×250-->


				</div>



			</div>
			<!--main boxコンテンツの外枠-->


			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>
		</div>
		<!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
		<!--contentsin-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
	</div>
	<!--container-->
</body> <
/html>