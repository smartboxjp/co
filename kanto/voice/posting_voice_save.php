<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonVoice.php";
	$common_voice = new CommonVoice();
	
	$meta_title = "口コミ投稿";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/".$global_area."/common/header/header_meta.php";?>
</head>

<body>
<?
	$datetime = date("Y-m-d H:i:s");
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($voice_title=="" || $voice_period=="" || $voice_star=="" || $voice_comment_1=="" || $shop_id=="" )
	{
		$common_connect-> Fn_javascript_back("正しく入力して下さい。");
	}
	
	//ショップの基本情報
	$arr_data = array();
	$arr_data[] = "cate_area_s_id"; 
	$arr_data[] = "cate_job_id"; 
	$arr_data[] = "shop_name"; 
	
	$arr_where = array();
	$arr_where["flag_open"] = "1"; //公開のみ
	$arr_where["shop_id"] = $shop_id;
	$db_result_shop = $common_shop -> Fn_db_shop_data($arr_data, $arr_where);
	if(!is_null($db_result_shop))
	{
		$shop_name = $db_result_shop[0]["shop_name"];
		$cate_area_s_id = $db_result_shop[0]["cate_area_s_id"];
		$cate_job_id = $db_result_shop[0]["cate_job_id"];
	}
	
	if($shop_name=="")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
	
	$datetime = date("Y/m/d H:i:s");
	$member_id = $_SESSION['member_id'];
	if($member_id=="") { $member_id = 0;}
	$flag_open = 1;
	
	//array
	$arr_data = array();
	$arr_db_field = array("shop_id", "member_id", "voice_title", "voice_period", "voice_star", "cate_area_s_id", "cate_job_id");
	$arr_db_field = array_merge($arr_db_field, array("voice_comment_1", "voice_comment_2", "voice_comment_3", "flag_open"));
	
	//基本情報
	foreach($arr_db_field as $value) {
		$arr_data[$value] = $$value;
	}

	$common_voice -> Fn_voice_insert ($arr_data);

	$result_voice_id = $common_voice -> Fn_db_voice_auto();
	$voice_id = $result_voice_id[0]["last_id"];


	$common_connect-> Fn_javascript_move("口コミ投稿ありがとうございます。", global_no_ssl."/".$global_area."/shop/?shop_id=".$shop_id);
?>

</body>

</html>
