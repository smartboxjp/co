<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    $common_image = new CommonImage(); //画像
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>店舗削除</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php   
    foreach($_GET as $key => $value)
    { 
        //$$key = trim($common_dao->db_string_escape($value));
        $$key = $value;
    }
    //管理者チェック
    $common_connect -> Fn_admin_check();
    
    $shop_count = 0;
    foreach($blog_id as $value) {
        if($value!="")
        {
            $shop_count++;
        }
    }
    
    if($shop_count==0)
    {
        $common_connect -> Fn_javascript_back("正しく選択してください。");
    }
    
    //削除処理
    foreach($blog_id as $value) {
        if($value!="")
        {
            $db_del = "Delete from shop_blog where shop_blog_id='".$value."' ";
            $common_dao->db_update($db_del);
            
            if(file_exists($_SERVER['DOCUMENT_ROOT']."/".global_shop_blog_dir.$value))
            {
                $common_connect -> Fn_deldir($_SERVER['DOCUMENT_ROOT']."/".global_shop_blog_dir.$value);
            }
        }
    }
    
    $common_connect-> Fn_javascript_move("店舗削除しました", "list_blog.php");
?>
</body>
</html>