

        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="グーグル" class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="/masterdashboard/shop/shop.php"><i class="fa fa-home"></i> お店の詳細ページへ</a>
                </li>
                <li>
                    <a href="login.html"><i class="fa fa-caret-square-o-right"></i> お店の観覧ページへ</a>
                </li>
                <li>
                    <a href="login.html"><i class="fa fa-caret-square-o-right"></i> お店の管理ページへ</a>
                </li>

                <li>
                    <a href="">
                        <div class="text-danger"><i class="fa fa-times-circle"></i> このお店を削除</div>
                    </a>
                </li>
            </ul>

        </nav>
        </div>