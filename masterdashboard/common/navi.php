<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonInquiry.php";
	$common_inquiry = new CommonInquiry();
?>


    <nav class="navbar-default navbar-static-side" role="navigation">

        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="/masterdashboard/img/profile_small.jpg" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Team-avalanche</strong>
                             </span> <span class="text-muted text-xs block">各種ページ <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="/masterdashboard/profile.html">cossot</a></li>
                            <li><a href="/masterdashboard/contacts.html">調査</a></li>
                            <li><a href="/masterdashboard/mailbox.html">avalanche</a></li>
                            <li class="divider"></li>
                            <li><a href="/masterdashboard/login/logout.php">logout</a></li>
                        </ul>
                    </div>

                    <div class="logo-element">
                        Icon
                    </div>
                </li>

                <li class="active">
                    <a href="/masterdashboard/dashboard/index.php"><i class="fa fa-th-large fa-fw"></i> <span class="nav-label">管理画面</span></a>
                </li>

                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> <span class="nav-label">各種登録データ</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="/masterdashboard/shop/list.php">登録店</a></li>
                        <li><a href="/masterdashboard/blog/list_blog.php">お店ブログ一覧</a></li>
                        <li><a href="/masterdashboard/movie/list_movie.php">お店動画一覧</a></li>
                        <li><a href="/masterdashboard/member/list.php">登録ユーザー</a></li>
                        <li><a href="/masterdashboard/voice/list.php">口コミ一覧</a></li>
						<li><a href="/masterdashboard/picture/list_img.php">画像一覧</a></li>
                    </ul>
                </li>


                <li>
                    <a href="/masterdashboard/mail_box/list.php"><i class="fa fa-envelope fa-fw"></i> <span class="nav-label">Mailbox </span>
                    <?
					//array
					$arr_where = array();
					$arr_where["inquiry_status"] = "0";
				
					$return_inquiry = $common_inquiry -> Fn_inquiry_all_count ($arr_where)  ;
					$return_inquiry_count=0;
					if(!is_null($return_inquiry))
					{
						$return_inquiry_count = $return_inquiry[0]["all_count"];
					}
					if($return_inquiry_count!=0) {
                    ?>
                    <span class="label label-warning pull-right">未読<? echo $return_inquiry_count;?></span>
                    <? } ?>
                    <span class="fa arrow"></span></a>
                </li>
                <li>
                    <a href="/masterdashboard/shop/entry.php"><i class="fa fa-plus fa-fw" aria-hidden="true"></i> <span class="nav-label">店舗登録</span>  </a>
                </li>
                <li>
                    <a href="/masterdashboard/member/entry.php"><i class="fa fa-user-plus fa-fw"></i> <span class="nav-label">ユーザー登録</span></a>
                </li>

                <li>
                    <a href="/masterdashboard/matome/index.php"><i class="fa fa-pencil-square-o fa-fw"></i> <span class="nav-label">まとめ記事投稿</span></a>
                </li>

                <li>
                    <a href="#"><i class="fa fa-file-image-o fa-fw"></i> <span class="nav-label">広告コントロール</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="/masterdashboard/pr/list.php">サイト各所ランダム</a></li>
                        <li><a href="/masterdashboard/pr/list_fix.php">サイト各所固定</a></li>

                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-star-o fa-fw"></i><span class="nav-label">ランキング編集</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="/masterdashboard/ranking/rank_point.php">平均点ランキング</a></li>
                        <li><a href="/masterdashboard/ranking/rank_access.php">アクセスランキング</a></li>
                        <li><a href="/masterdashboard/ranking/rank_blog.php">ブログランキング</a></li>
                        <li><a href="/masterdashboard/ranking/rank_user.php">ピックアップユーザー</a></li>
                    </ul>
                </li>
                <li>
                    <a href="/masterdashboard/topics/list.php"><i class="fa fa-desktop fa-fw"></i> <span class="nav-label">トピックス</span></a>
                </li>

                <li>
                    <a href="/masterdashboard/info/info.php"><i class="fa fa-info-circle fa-fw"></i> <span class="nav-label">お知らせ</span></a>
                </li>

                <li>
                    <a href="/masterdashboard/mailmagazine/mail_magazine.php"><i class="fa fa-paper-plane-o fa-fw"></i> <span class="nav-label">メルマガ</span></a>
                </li>

                <li>
                    <a href=""><i class="fa fa-pie-chart fa-fw"></i> <span class="nav-label">コソットアクセス</span></a>
                </li>



                <li>
                    <a href="#"><i class="fa fa-exclamation-triangle fa-fw"></i> <span class="nav-label">他求人サイト</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="http://www.15navi.com/" target="_blank">イチゴナビ</a></li>
                        <li><a href="http://qzin.jp/" target="_blank">バニラ</a></li>
                        <li><a href="http://www.girlsheaven-job.net/" target="_blank">ガールズヘブン</a></li>

                    </ul>
                </li>

            </ul>

        </div>
    </nav>