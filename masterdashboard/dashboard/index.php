<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連

    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMasterboard.php";
    $Common_masterboard = new CommonMasterboard();

    foreach($_GET as $key => $value)
    {
        $$key = $common_connect->h($value);
    }
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>ダッシュボード</title>

    <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Morris -->
    <link href="/masterdashboard/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">

    <link href="/masterdashboard/css/animate.css" rel="stylesheet">
    <link href="/masterdashboard/css/style.css" rel="stylesheet">
    <link href="/masterdashboard/css/plus.css" rel="stylesheet">

</head>

<body>
    <div id="wrapper">



<!--左ナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
<!--左ナビゲーションここまで -->



	<div id="page-wrapper" class="gray-bg">



<!--コンテンツ　ヘッダーナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
  
<!--コンテンツ　ヘッダーナビゲーションここまで -->


     <div class="wrapper wrapper-content">
        <div class="row">
            <? $Common_masterboard->Fn_top_voice_count();?>
            <div class="col-lg-2">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right"><i class="fa fa-clock-o"></i>一時間前</span>
                        <h5>Img</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">25,800枚</h1>
                        <small>トータル投稿数</small>
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="ibox float-e-margins">
                <? $Common_masterboard->Fn_top_user_count();?>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="ibox float-e-margins">
                <? $Common_masterboard->Fn_top_blog_count();?>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="ibox float-e-margins">
                <? $Common_masterboard->Fn_top_shop_pay_count();?>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="ibox float-e-margins">
                <? $Common_masterboard->Fn_top_shop_free_count();?>
                </div>
            </div>
        </div>






        <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>サイトアクセス</h5>
 
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                <div class="col-lg-9">
                                    <div class="flot-chart">
                                        <div class="flot-chart-content" id="flot-dashboard-chart"></div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <ul class="stat-list">
                                        <li>
                                            <h2 class="no-margins">2,346</h2>
                                            <small>ビジットアクセス数</small>
                                            <div class="stat-percent">48% <i class="fa fa-level-up text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: 48%;" class="progress-bar"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <h2 class="no-margins ">4,422</h2>
                                            <small>ページビュー数</small>
                                            <div class="stat-percent">60% <i class="fa fa-level-down text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: 60%;" class="progress-bar"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <h2 class="no-margins ">9,180</h2>
                                            <small>スマートフォンアクセス</small>
                                            <div class="stat-percent">22% <i class="fa fa-bolt text-navy"></i></div>
                                            <div class="progress progress-mini">
                                                <div style="width: 22%;" class="progress-bar"></div>
                                            </div>
                                        </li>
                                        </ul>
                                    </div>
                                </div>
                                </div>

                            </div>
                        </div>
                    </div>


               

<!--新着登録ユーザーデータ -->
   			 <div class="row">
                            <div class="col-lg-6">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>最新登録ユーザー</h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                            <a class="close-link">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="ibox-content">
                                        <table class="table table-hover no-margins">
                                            <thead>
                                            <tr>
                                                <th>名前</th>
                                                <th>登録日</th>
                                                <th>第一エリア</th>
                                                <th>第二エリア</th>
						<th>アドレス</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><small><a href="user.html">かりなちゃんだよ</a></small></td>
                                                <td><small>11/24</small></td>
                                                <td><small>神奈川県</small></td>
                                                <td><small>横浜市</small></td>
						<td><small>kohrfqrqrqajrajrfa@gmail.com</small></td>
                                            </tr>

                                            <tr>
                                                <td><small><a href="user.html">かりなちゃんだよ</a></small></td>
                                                <td><small>11/24</small></td>
                                                <td><small>神奈川県</small></td>
                                                <td><small>横浜市</small></td>
						<td><small>kohrfqrqrqajrajrfa@gmail.com</small></td>
                                            </tr>

                                            <tr>
                                                <td><small><a href="user.html">かりなちゃんだよ</a></small></td>
                                                <td><small>11/24</small></td>
                                                <td><small>神奈川県</small></td>
                                                <td><small>横浜市</small></td>
						<td><small>kohrfqrqrqajrajrfa@gmail.com</small></td>
                                            </tr>

                                            <tr>
                                                <td><small><a href="user.html">かりなちゃんだよ</a></small></td>
                                                <td><small>11/24</small></td>
                                                <td><small>神奈川県</small></td>
                                                <td><small>横浜市</small></td>
						<td><small>kohrfqrqrqajrajrfa@gmail.com</small></td>
                                            </tr>

                                            <tr>
                                                <td><small><a href="user.html">かりなちゃんだよ</a></small></td>
                                                <td><small>11/24</small></td>
                                                <td><small>神奈川県</small></td>
                                                <td><small>横浜市</small></td>
						<td><small>kohrfqrqrqajrajrfa@gmail.com</small></td>
                                            </tr>

                                            <tr>
                                                <td><small><a href="user.html">かりなちゃんだよ</a></small></td>
                                                <td><small>11/24</small></td>
                                                <td><small>神奈川県</small></td>
                                                <td><small>横浜市</small></td>
						<td><small>kohrfqrqrqajrajrfa@gmail.com</small></td>
                                            </tr>

                                            <tr>
                                                <td><small><a href="user.html">かりなちゃんだよ</a></small></td>
                                                <td><small>11/24</small></td>
                                                <td><small>神奈川県</small></td>
                                                <td><small>横浜市</small></td>
						<td><small>kohrfqrqrqajrajrfa@gmail.com</small></td>
                                            </tr>
                                            </tbody>
                                        </table>
					<div class="nextlist"><button type="button" class="btn btn-outline btn-info"><a href="list_user.html">ユーザー一覧</a></button></div>
                                    </div>
                                </div>
                            </div>
<!--新着登録ユーザーデータ -->

<!--新着登録お店データ -->
                            <div class="col-lg-6">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>最新登録店舗</h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                            <a class="close-link">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="ibox-content">
                                        <table class="table table-hover no-margins">
                                            <thead>
                                            <tr>
                                                <th>店名</th>
                                                <th>登録日</th>
                                                <th>第一エリア</th>
                                                <th>第二エリア</th>
						<th>業種</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <tr>
                                                <td><small><a href="shop.html">ムラムラM字妻ほかとなかまたち</a></small></td>
                                                <td><small>3/17</small></td>
                                                <td><small>東京都</small></td>
                                                <td><small>新宿,新大久保..</small></td>
						<td><small>デリヘル</small></td>
                                            </tr>
                                            <tr>
                                                <td><small><a href="shop.html">ムラムラM字妻ほかとなかまたち</a></small></td>
                                                <td><small>3/17</small></td>
                                                <td><small>東京都</small></td>
                                                <td><small>新宿,新大久保..</small></td>
						<td><small>デリヘル</small></td>
                                            </tr>
                                            <tr>
                                                <td><small><a href="shop.html">ムラムラM字妻ほかとなかまたち</a></small></td>
                                                <td><small>3/17</small></td>
                                                <td><small>東京都</small></td>
                                                <td><small>新宿,新大久保..</small></td>
						<td><small>デリヘル</small></td>
                                            </tr>
                                           <tr>
                                                <td><small><a href="shop.html">ムラムラM字妻ほかとなかまたち</a></small></td>
                                                <td><small>3/17</small></td>
                                                <td><small>東京都</small></td>
                                                <td><small>新宿,新大久保..</small></td>
						<td><small>デリヘル</small></td>
                                            </tr>
                                           <tr>
                                                <td><small><a href="shop.html">ムラムラM字妻ほかとなかまたち</a></small></td>
                                                <td><small>3/17</small></td>
                                                <td><small>東京都</small></td>
                                                <td><small>新宿,新大久保..</small></td>
						<td><small>デリヘル</small></td>
                                            </tr>
                                            <tr>
                                                <td><small><a href="shop.html">ムラムラM字妻ほかとなかまたち</a></small></td>
                                                <td><small>3/17</small></td>
                                                <td><small>東京都</small></td>
                                                <td><small>新宿,新大久保..</small></td>
						<td><small>デリヘル</small></td>
                                            </tr>
                                            <tr>
                                                <td><small><a href="shop.html">ムラムラM字妻ほかとなかまたち</a></small></td>
                                                <td><small>3/17</small></td>
                                                <td><small>東京都</small></td>
                                                <td><small>新宿,新大久保..</small></td>
						<td><small>デリヘル</small></td>
                                            </tr>
                                            </tbody>
                                        </table>
					<div class="nextlist"><button type="button" class="btn btn-outline btn-info"><a href="list_shop.html">店舗一覧</a></button></div>
                                    </div>
                                </div>
                            </div>
			</div>
<!--新着登録お店データ -->

<!--新着登録口コミデータ -->

        <div class="row">

        <div class="col-lg-12">
        <div class="ibox float-e-margins">

        <div class="ibox-title">
            <h5>新着口コミ</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>

        <div class="ibox-content">
           

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>登録日</th>
			<th>投稿時間</th>
                        <th>投稿者</th>
                        <th>評価</th>
                        <th>対象店</th>
			<th>第一エリア</th>
			<th>第二エリア</th>
			<th>業種</th>
                        <th>タイトル</th>
                        <th>本文</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">紳士服のコナカ</a></td>
                        <td>3.14点</td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.大塚.巣鴨</td>
			<td>デリバリーヘルス</td>
                        <td><a href="voice.html">ここで働いてみて思ったので..</a></td>
                        <td><a href="voice.html">スタッフの対応なども含め私的に..</a></td>
                    </tr>
                    <tr>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">紳士服のコナカ</a></td>
                        <td>3.14点</td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.大塚.巣鴨</td>
			<td>デリバリーヘルス</td>
                        <td><a href="voice.html">ここで働いてみて思ったので..</a></td>
                        <td><a href="voice.html">スタッフの対応なども含め私的に..</a></td>
                    </tr>
                    <tr>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">紳士服のコナカ</a></td>
                        <td>3.14点</td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.大塚.巣鴨</td>
			<td>デリバリーヘルス</td>
                        <td><a href="voice.html">ここで働いてみて思ったので..</a></td>
                        <td><a href="voice.html">スタッフの対応なども含め私的に..</a></td>
                    </tr>
                    <tr>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">紳士服のコナカ</a></td>
                        <td>3.14点</td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.大塚.巣鴨</td>
			<td>デリバリーヘルス</td>
                        <td><a href="voice.html">ここで働いてみて思ったので..</a></td>
                        <td><a href="voice.html">スタッフの対応なども含め私的に..</a></td>
                    </tr>
                    <tr>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">紳士服のコナカ</a></td>
                        <td>3.14点</td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.大塚.巣鴨</td>
			<td>デリバリーヘルス</td>
                        <td><a href="voice.html">ここで働いてみて思ったので..</a></td>
                        <td><a href="voice.html">スタッフの対応なども含め私的に..</a></td>
                    </tr>
                   <tr>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">紳士服のコナカ</a></td>
                        <td>3.14点</td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.大塚.巣鴨</td>
			<td>デリバリーヘルス</td>
                        <td><a href="voice.html">ここで働いてみて思ったので..</a></td>
                        <td><a href="voice.html">スタッフの対応なども含め私的に..</a></td>
                    </tr>
                    <tr>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">紳士服のコナカ</a></td>
                        <td>3.14点</td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.大塚.巣鴨</td>
			<td>デリバリーヘルス</td>
                        <td><a href="voice.html">ここで働いてみて思ったので..</a></td>
                        <td><a href="voice.html">スタッフの対応なども含め私的に..</a></td>
                    </tr>
                    <tr>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">紳士服のコナカ</a></td>
                        <td>3.14点</td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.大塚.巣鴨</td>
			<td>デリバリーヘルス</td>
                        <td><a href="voice.html">ここで働いてみて思ったので..</a></td>
                        <td><a href="voice.html">スタッフの対応なども含め私的に..</a></td>
                    </tr>
                    <tr>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">紳士服のコナカ</a></td>
                        <td>3.14点</td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.大塚.巣鴨</td>
			<td>デリバリーヘルス</td>
                        <td><a href="voice.html">ここで働いてみて思ったので..</a></td>
                        <td><a href="voice.html">スタッフの対応なども含め私的に..</a></td>
                    </tr>
                    <tr>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">紳士服のコナカ</a></td>
                        <td>3.14点</td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.大塚.巣鴨</td>
			<td>デリバリーヘルス</td>
                        <td><a href="voice.html">ここで働いてみて思ったので..</a></td>
                        <td><a href="voice.html">スタッフの対応なども含め私的に..</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
		<div class="nextlist"><button type="button" class="btn btn-outline btn-info"><a href="list_voice.html">口コミ一覧</a></button></div>
        </div>
        </div>
        </div>

<!--新着登録口コミデータ -->


<!--新着登録画像データ -->

<div class="col-lg-12">
        <div class="ibox float-e-margins">

        <div class="ibox-title">
            <h5>新着画像</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>


        <div class="ibox-content">
           

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>サムネイル</th>
			<th>登録日</th>
                        <th>投稿時間</th>
                        <th>投稿者</th>
                        <th>対象店</th>
			<th>第一エリア</th>
			<th>第二エリア</th>
			<th>業種</th>
                        <th>コメント</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
			<td><img src="/masterdashboard/img//gallery/123.jpg" width="50px" height="50px"></td>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">リズリサさん</a></td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.巣鴨.大塚</td>
			<td>デリバリーヘルス</td>
                        <td>待機室の画像です！</td>
                    </tr>
                    <tr>
			<td><img src="/masterdashboard/img//gallery/123.jpg" width="50px" height="50px"></td>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">リズリサさん</a></td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.巣鴨.大塚</td>
			<td>デリバリーヘルス</td>
                        <td>待機室の画像です！</td>
                    </tr>
                    <tr>
			<td><img src="/masterdashboard/img//gallery/123.jpg" width="50px" height="50px"></td>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">リズリサさん</a></td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.巣鴨.大塚</td>
			<td>デリバリーヘルス</td>
                        <td>待機室の画像です！</td>
                    </tr>
                    <tr>
			<td><img src="/masterdashboard/img//gallery/123.jpg" width="50px" height="50px"></td>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">リズリサさん</a></td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.巣鴨.大塚</td>
			<td>デリバリーヘルス</td>
                        <td>待機室の画像です！</td>
                    </tr>
                    <tr>
			<td><img src="/masterdashboard/img//gallery/123.jpg" width="50px" height="50px"></td>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">リズリサさん</a></td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.巣鴨.大塚</td>
			<td>デリバリーヘルス</td>
                        <td>待機室の画像です！</td>
                    </tr>
                    <tr>
			<td><img src="/masterdashboard/img//gallery/123.jpg" width="50px" height="50px"></td>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">リズリサさん</a></td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.巣鴨.大塚</td>
			<td>デリバリーヘルス</td>
                        <td>待機室の画像です！</td>
                    </tr>
                    <tr>
			<td><img src="/masterdashboard/img//gallery/123.jpg" width="50px" height="50px"></td>
                        <td>3/17</td>
			<td>07:45</td>
                        <td><a href="user.html">リズリサさん</a></td>
                        <td><a href="shop.html">夜の動物園西葛西本店営業部</a></td>
			<td>東京都</td>
			<td>池袋.巣鴨.大塚</td>
			<td>デリバリーヘルス</td>
                        <td>待機室の画像です！</td>
                    </tr>
                        
                    </tbody>
                </table>
            </div>
		<div class="nextlist"><button type="button" class="btn btn-outline btn-info"><a href="list_img.html">画像一覧</a></button></div>
        </div>

	</div>
</div>

<!--新着登録画像データ -->


<!--新着登録ブログデータ -->

   <div class="col-lg-12">
        <div class="ibox float-e-margins">

        <div class="ibox-title">
            <h5>新着ブログ</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>

        <div class="ibox-content">
            

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>登録日</th>
			<th>投稿時間</th>
                        <th>投稿店</th>
                        <th>第一エリア</th>
                        <th>第二エリア</th>
                        <th>業種</th>
                        <th>タイトル</th>
                        <th>本文</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>3/17</td>
                        <td>12：24</td>
                        <td><a href="shop.html">那覇むらむらMじつましんじょうてん</a></td>
                        <td>神奈川県</td>
                        <td>横浜</td>
                        <td>デリバリーヘルス</td>
                        <td><a href="blog.html">やっと夏本番！ここでがっつり..</a></td>
			<td><a href="blog.html">夏休みを利用して一緒に稼ぎませ..</a></td>
                    </tr>
                    <tr>
                        <td>3/17</td>
                        <td>12：24</td>
                        <td><a href="shop.html">那覇むらむらMじつましんじょうてん</a></td>
                        <td>神奈川県</td>
                        <td>横浜</td>
                        <td>デリバリーヘルス</td>
                        <td><a href="blog.html">やっと夏本番！ここでがっつり..</a></td>
			<td><a href="blog.html">夏休みを利用して一緒に稼ぎませ..</a></td>
                    </tr>
                    <tr>
                        <td>3/17</td>
                        <td>12：24</td>
                        <td><a href="shop.html">那覇むらむらMじつましんじょうてん</a></td>
                        <td>神奈川県</td>
                        <td>横浜</td>
                        <td>デリバリーヘルス</td>
                        <td><a href="blog.html">やっと夏本番！ここでがっつり..</a></td>
			<td><a href="blog.html">夏休みを利用して一緒に稼ぎませ..</a></td>
                    </tr>
                    <tr>
                        <td>3/17</td>
                        <td>12：24</td>
                        <td><a href="shop.html">那覇むらむらMじつましんじょうてん</a></td>
                        <td>神奈川県</td>
                        <td>横浜</td>
                        <td>デリバリーヘルス</td>
                        <td><a href="blog.html">やっと夏本番！ここでがっつり..</a></td>
			<td><a href="blog.html">夏休みを利用して一緒に稼ぎませ..</a></td>
                    </tr>
                    <tr>
                        <td>3/17</td>
                        <td>12：24</td>
                        <td><a href="shop.html">那覇むらむらMじつましんじょうてん</a></td>
                        <td>神奈川県</td>
                        <td>横浜</td>
                        <td>デリバリーヘルス</td>
                        <td><a href="blog.html">やっと夏本番！ここでがっつり..</a></td>
			<td><a href="blog.html">夏休みを利用して一緒に稼ぎませ..</a></td>
                    </tr>
                    <tr>
                        <td>3/17</td>
                        <td>12：24</td>
                        <td><a href="shop.html">那覇むらむらMじつましんじょうてん</a></td>
                        <td>神奈川県</td>
                        <td>横浜</td>
                        <td>デリバリーヘルス</td>
                        <td><a href="blog.html">やっと夏本番！ここでがっつり..</a></td>
			<td><a href="blog.html">夏休みを利用して一緒に稼ぎませ..</a></td>
                    </tr>
                    <tr>
                        <td>3/17</td>
                        <td>12：24</td>
                        <td><a href="shop.html">那覇むらむらMじつましんじょうてん</a></td>
                        <td>神奈川県</td>
                        <td>横浜</td>
                        <td>デリバリーヘルス</td>
                        <td><a href="blog.html">やっと夏本番！ここでがっつり..</a></td>
			<td><a href="blog.html">夏休みを利用して一緒に稼ぎませ..</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
		<div class="nextlist"><button type="button" class="btn btn-outline btn-info"><a href="list_blog.html">ブログ一覧</a></button></div>
        </div>
        </div>
  </div>
<!--新着登録ブログデータ -->


                </div>
                </div>
<!--フッター部分 -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
<!--フッター部分 -->

  </div>

    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
    <script src="/masterdashboard/js/bootstrap.min.js"></script>
    <script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="/masterdashboard/js/plugins/flot/jquery.flot.js"></script>
    <script src="/masterdashboard/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="/masterdashboard/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="/masterdashboard/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="/masterdashboard/js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="/masterdashboard/js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="/masterdashboard/js/plugins/flot/jquery.flot.time.js"></script>

    <!-- Peity -->
    <script src="/masterdashboard/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="/masterdashboard/js/demo/peity-demo.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/masterdashboard/js/inspinia.js"></script>
    <script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="/masterdashboard/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- Jvectormap -->
    <script src="/masterdashboard/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="/masterdashboard/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

    <!-- EayPIE -->
    <script src="/masterdashboard/js/plugins/easypiechart/jquery.easypiechart.js"></script>

    <!-- Sparkline -->
    <script src="/masterdashboard/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="/masterdashboard/js/demo/sparkline-demo.js"></script>

    <script>
        $(document).ready(function() {
            $('.chart').easyPieChart({
                barColor: '#f8ac59',
//                scaleColor: false,
                scaleLength: 5,
                lineWidth: 4,
                size: 80
            });

            $('.chart2').easyPieChart({
                barColor: '#1c84c6',
//                scaleColor: false,
                scaleLength: 5,
                lineWidth: 4,
                size: 80
            });

            var data2 = [
                [gd(2012, 1, 1), 7], [gd(2012, 1, 2), 6], [gd(2012, 1, 3), 4], [gd(2012, 1, 4), 8],
                [gd(2012, 1, 5), 9], [gd(2012, 1, 6), 7], [gd(2012, 1, 7), 5], [gd(2012, 1, 8), 4],
                [gd(2012, 1, 9), 7], [gd(2012, 1, 10), 8], [gd(2012, 1, 11), 9], [gd(2012, 1, 12), 6],
                [gd(2012, 1, 13), 4], [gd(2012, 1, 14), 5], [gd(2012, 1, 15), 11], [gd(2012, 1, 16), 8],
                [gd(2012, 1, 17), 8], [gd(2012, 1, 18), 11], [gd(2012, 1, 19), 11], [gd(2012, 1, 20), 6],
                [gd(2012, 1, 21), 6], [gd(2012, 1, 22), 8], [gd(2012, 1, 23), 11], [gd(2012, 1, 24), 13],
                [gd(2012, 1, 25), 7], [gd(2012, 1, 26), 9], [gd(2012, 1, 27), 9], [gd(2012, 1, 28), 8],
                [gd(2012, 1, 29), 5], [gd(2012, 1, 30), 8], [gd(2012, 1, 31), 25]
            ];

            var data3 = [
                [gd(2012, 1, 1), 800], [gd(2012, 1, 2), 500], [gd(2012, 1, 3), 600], [gd(2012, 1, 4), 700],
                [gd(2012, 1, 5), 500], [gd(2012, 1, 6), 456], [gd(2012, 1, 7), 800], [gd(2012, 1, 8), 589],
                [gd(2012, 1, 9), 467], [gd(2012, 1, 10), 876], [gd(2012, 1, 11), 689], [gd(2012, 1, 12), 700],
                [gd(2012, 1, 13), 500], [gd(2012, 1, 14), 600], [gd(2012, 1, 15), 700], [gd(2012, 1, 16), 786],
                [gd(2012, 1, 17), 345], [gd(2012, 1, 18), 888], [gd(2012, 1, 19), 888], [gd(2012, 1, 20), 888],
                [gd(2012, 1, 21), 987], [gd(2012, 1, 22), 444], [gd(2012, 1, 23), 999], [gd(2012, 1, 24), 567],
                [gd(2012, 1, 25), 786], [gd(2012, 1, 26), 666], [gd(2012, 1, 27), 888], [gd(2012, 1, 28), 900],
                [gd(2012, 1, 29), 178], [gd(2012, 1, 30), 555], [gd(2012, 1, 31), 993]
            ];


            var dataset = [
                {
                    label: "Number of orders",
                    data: data3,
                    color: "#1ab394",
                    bars: {
                        show: true,
                        align: "center",
                        barWidth: 24 * 60 * 60 * 600,
                        lineWidth:0
                    }

                }, {
                    label: "Payments",
                    data: data2,
                    yaxis: 2,
                    color: "#1C84C6",
                    lines: {
                        lineWidth:1,
                            show: true,
                            fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 0.2
                            }, {
                                opacity: 0.4
                            }]
                        }
                    },
                    splines: {
                        show: false,
                        tension: 0.6,
                        lineWidth: 1,
                        fill: 0.1
                    },
                }
            ];


            var options = {
                xaxis: {
                    mode: "time",
                    tickSize: [3, "day"],
                    tickLength: 0,
                    axisLabel: "Date",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Arial',
                    axisLabelPadding: 10,
                    color: "#d5d5d5"
                },
                yaxes: [{
                    position: "left",
                    max: 1070,
                    color: "#d5d5d5",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Arial',
                    axisLabelPadding: 3
                }, {
                    position: "right",
                    clolor: "#d5d5d5",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: ' Arial',
                    axisLabelPadding: 67
                }
                ],
                legend: {
                    noColumns: 1,
                    labelBoxBorderColor: "#000000",
                    position: "nw"
                },
                grid: {
                    hoverable: false,
                    borderWidth: 0
                }
            };

            function gd(year, month, day) {
                return new Date(year, month - 1, day).getTime();
            }

            var previousPoint = null, previousLabel = null;

            $.plot($("#flot-dashboard-chart"), dataset, options);

            var mapData = {
                "US": 298,
                "SA": 200,
                "DE": 220,
                "FR": 540,
                "CN": 120,
                "AU": 760,
                "BR": 550,
                "IN": 200,
                "GB": 120,
            };

            $('#world-map').vectorMap({
                map: 'world_mill_en',
                backgroundColor: "transparent",
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        "fill-opacity": 0.9,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 0
                    }
                },

                series: {
                    regions: [{
                        values: mapData,
                        scale: ["#1ab394", "#22d6b1"],
                        normalizeFunction: 'polynomial'
                    }]
                },
            });
        });
    </script>
</body>
</html>
