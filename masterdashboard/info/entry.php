<?php
require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_include/connect.php";
$common_connect = new CommonConnect();
$common_dao = new CommonDao(); //DB関連

foreach ( $_GET as $key => $value ) {
	$$key = $common_connect->h($value);
}
?>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>メール詳細 | Mailbox</title>

	<link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
	<link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="/masterdashboard/css/animate.css" rel="stylesheet">
	<link href="/masterdashboard/css/style.css" rel="stylesheet">
	<link href="/masterdashboard/css/pius.css" rel="stylesheet">

<script language="javascript"> 
    function fnChangeSel(i) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './info_del.php?info_id='+i;
        } 
    }
</script>
</head>

<body>
<?
    //管理者チェック
    $common_connect -> Fn_admin_check();
    if ($info_id != "")
    {
        //リスト表示
        $arr_db_field = array("info_date", "info_title", "info_comment", "flag_user", "flag_member", "flag_shop", "regi_date", "up_date");
        
        $sql = "SELECT ";
        foreach($arr_db_field as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM info where info_id='".$info_id."' " ;
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            foreach($arr_db_field as $val)
            {
                $$val = $db_result[0][$val];
            }
        }
    }
?>

	<div id="wrapper">

		<!--左ナビゲーション -->
		<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
		<!--左ナビゲーションここまで -->

		<div id="page-wrapper" class="gray-bg">

			<!--コンテンツ　ヘッダーナビゲーション -->
			<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
			<!--コンテンツ　ヘッダーナビゲーションここまで -->

			<div class="wrapper wrapper-content">
				<div class="row">
					<div class="col-lg-12 animated fadeInRight">
						<div class="mail-box-header">

							<h2>
                    インフォメーション
                </h2>
						

							<div class="mail-tools tooltip-demo m-t-md">

								<h3>
                        <span class="font-noraml">タイトル: <? echo $info_title;?></span>
                    </h3>
							

								<h5>
                        <span class="pull-right font-noraml"><? echo $regi_date;?>投稿</span>
                        <span class="font-noraml">
                        表示ページ:&nbsp;&nbsp;&nbsp;&nbsp;
                        <? if($flag_user=="1") {?><i class="fa fa-check text-navy"></i><? } ?>
                        ユーザーページ&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;
                        <? if($flag_member=="1") {?><i class="fa fa-check text-navy"></i><? } ?>
                        ユーザー管理ページ&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;
                        <? if($flag_shop=="1") {?><i class="fa fa-check text-navy"></i><? } ?>
                        店舗管理ページ
                        </span>
                    </h5>
							

							</div>
						</div>
						<div class="mail-box">

							<div class="mail-body">

								<p>
									<? echo nl2br($info_comment);?>
								</p>
							</div>

							<div class="mail-body text-right tooltip-demo">
								<button title="編集" data-placement="top" data-toggle="tooltip" type="button" data-original-title="" class="btn btn-sm btn-white" onclick="location.href='./info.php?info_id=<? echo $info_id;?>'"><i class="fa fa-pencil"></i></button>
								<button title="削除" data-placement="top" data-toggle="tooltip" data-original-title="" class="btn btn-sm btn-white" onclick="fnChangeSel('<? echo $info_id;?>');""><i class="fa fa-trash-o"></i></button>
							</div>
							<div class="clearfix"></div>

						</div>
					</div>
				</div>
			</div>

			<!--フッター部分 -->
			<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
			<!--フッター部分 -->

		</div>
	</div>

	<!-- Mainly scripts -->
	<script src="/masterdashboard/js/jquery-2.1.1.js"></script>
	<script src="/masterdashboard/js/bootstrap.min.js"></script>
	<script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

	<!-- Custom and plugin javascript -->
	<script src="/masterdashboard/js/inspinia.js"></script>
	<script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>

</body>

</html>