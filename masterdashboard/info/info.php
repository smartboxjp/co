<?php
require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_include/connect.php";
$common_connect = new CommonConnect();
$common_dao = new CommonDao(); //DB関連

foreach ( $_GET as $key => $value ) {
	$$key = $common_connect->h($value);
}
?>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>INSPINIA | File Upload</title>

	<link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
	<link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">

	<link href="/masterdashboard/css/animate.css" rel="stylesheet">
	<link href="/masterdashboard/css/plugins/dropzone/basic.css" rel="stylesheet">
	<link href="/masterdashboard/css/plugins/dropzone/dropzone.css" rel="stylesheet">
	<link href="/masterdashboard/css/plugins/lity/lity.css" rel="stylesheet">
	<link href="/masterdashboard/css/plugins/summernote/summernote.css" rel="stylesheet">
	<link href="/masterdashboard/css/style.css" rel="stylesheet">
	<link href="/masterdashboard/css/plus.css" rel="stylesheet">
	<link href="/masterdashboard/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

<script src="/masterdashboard/js/jquery-2.1.1.js"></script>
<script type="text/javascript">
	$(function() {
			
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";
			
			err_check_count += check_input("info_title");
			err_check_count += check_input("info_comment");
			if(err_check_count!=0)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
				
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}


	});
	

    $(function() {
		//チェックリストを一括削除
      $('#check_delete').click(function() {
		if(confirm('削除しますか？'))
		{
			check_del_list = $('[class="check_del"]:checked').map(function(){
				//$(this)でjQueryオブジェクトが取得できる。val()で値をvalue値を取得。
				return $(this).val();
			}).get().join('&info_id[]=');
			document.location.href = './info_list_del.php?info_id[]='+check_del_list;
		}
      });
    });
//-->
</script>
</head>

<body>
<?
    //管理者チェック
    $common_connect -> Fn_admin_check();
    if ($info_id != "")
    {
        //リスト表示
        $arr_db_field = array("info_date", "info_title", "info_comment", "flag_user", "flag_member", "flag_shop", "regi_date", "up_date");
        
        $sql = "SELECT ";
        foreach($arr_db_field as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM info where info_id='".$info_id."' " ;
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            foreach($arr_db_field as $val)
            {
                $$val = $db_result[0][$val];
            }
        }
    }
?>
	<div id="wrapper">



		<!--左ナビゲーション -->
		<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
		<!--左ナビゲーションここまで -->

		<div id="page-wrapper" class="gray-bg">


			<!--コンテンツ　ヘッダーナビゲーション -->
			<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>

			<!--コンテンツ　ヘッダーナビゲーションここまで -->



			<!--コンテンツヘッダー -->
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">

					<h2>インフォメーション</h2>

				</div>
				<div class="col-lg-2">

				</div>
			</div>
			<!--コンテンツヘッダーここまで -->

			<!--コンテンツここから -->
			<div class="wrapper wrapper-content  animated fadeInRight">


				<div class="row">

					<div class="col-lg-12">

						<div class="ibox">

							<div class="ibox-title">
								インフォメーションの投稿
							</div>

							<div class="ibox-content">

								<div class="panel-group payments-method" id="accordion">
									<div class="panel panel-default">



										<div id="collapseOne" class="panel-collapse collapse">
											<div class="panel-body">

												<div class="row">
													<div class="col-md-10">
														<h2>注意事項</h2>
														<strong>Product:</strong>: Name of product <br/>
														<strong>Price:</strong>: <span class="text-navy">$452.90</span>

														<p class="m-t">
															Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
														</p>

														<a class="btn btn-success">
														<i class="fa fa-cc-paypal">
														Purchase via PayPal
														</i>
													</a>
													

													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel panel-default">


										<div id="collapseTwo" class="panel-collapse collapse in">
											<div class="panel-body">

												<div class="row">
													<div class="col-md-4">
														<h2>注意事項</h2>
														<strong>Product:</strong>: Name of product <br/>
														<strong>Price:</strong>: <span class="text-navy">$452.90</span>

														<p class="m-t">
															Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
														</p>

														<p>
															Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat.
														</p>
													</div>

													<div class="col-md-8">

														<form action="./info_save.php" method="POST" name="form_write" role="form" id="payment-form" >
														<? $var = "info_id";?>
														<input type="hidden" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" />
															<div class="row">
																<div class="col-xs-12">
																	<div class="form-group">
																		<label>インフォメーションタイトル</label>
																		<div class="input-group">
																			<? $var = "info_title";?>
																			<input type="text" class="form-control" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" placeholder="インフォメーションタイトル" required/>
																			<span class="input-group-addon"><i class="fa fa-info-circle"></i></span>
																		</div>
																	</div>
																</div>
															</div>

															<div class="row">
																<div class="col-xs-7 col-md-7">
																	<div class="form-group">
																		<label>更新日時</label>
																		<? $var = "info_date";?>
																		<input type="text" class="form-control date_added" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" placeholder="投稿日時" required/>
																	</div>
																</div>

																<div class="col-xs-5 col-md-5 pull-right">
																	<div class="form-group">
																		<label>表示させたいページ</label>
																		<br>
																		<div class="chekblok">
																			<? $var = "flag_user";?>
																			<label><input type="checkbox" name="<? echo $var;?>" id="<? echo $var;?>" value="1" <? if($$var==1){ echo " checked ";}?>> ユーザー</label>&nbsp;&nbsp;&nbsp;&nbsp;

																			<? $var = "flag_member";?>
																			<label><input type="checkbox" name="<? echo $var;?>" id="<? echo $var;?>" value="1" <? if($$var==1){ echo " checked ";}?>> ユーザー管理</label>&nbsp;&nbsp;&nbsp;&nbsp;

																			<? $var = "flag_shop";?>
																			<label><input type="checkbox" name="<? echo $var;?>" id="<? echo $var;?>" value="1" <? if($$var==1){ echo " checked ";}?>> お店管理</label>
																		</div>
																	</div>
																</div>
															</div>

															<div class="row">
																<div class="col-xs-12">
																	<div class="form-group">
																		<label>内容</label>
																		<? $var = "info_comment";?>
																		<textarea name="<? echo $var;?>" id="<? echo $var;?>" class="form-control" rows="4" cols="40" placeholder="インフォメーション内容"><? echo $$var;?></textarea>
																	</div>
																</div>
															</div>


															<div class="row">
																<div class="col-xs-12">
																	<? $var = "form_confirm";?>
																	<button class="btn btn-primary" type="submit" name="<? echo $var;?>" id="<? echo $var;?>">情報の更新</button>
																</div>
															</div>
														</form>

													</div>

												</div>


											</div>
										</div>
									</div>
								</div>

							</div>

						</div>

					</div>

				</div>



				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>インフォメーション履歴</h5>

								<div class="ibox-tools">
								
									<button id="check_delete" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="一括削除"><i class="fa fa-trash-o"></i></button>
									
									<button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="ページを確認する"><i class="fa fa-desktop"></i></button>

								</div>
							</div>

							<div class="ibox-content">

								<table class="footable table table-stripped toggle-arrow-tiny" data-page-size="8">
									<thead>
										<tr>
											<th></th>
											<th data-toggle="true">インフォメーションタイトル</th>
											<th>投稿日時</th>
											<th>ユーザーページ</th>
											<th>会員管理ページ</th>
											<th>お店管理ページ</th>
											<th>編集</th>
										</tr>
									</thead>

									<tbody>
<?php
$view_count=20;   // List count
$offset=0;
$where = " ";
$keyword = preg_replace("/( |　|   )/", "", $keyword );

if($keyword!="")
{
    $where .= " and (info_title collate utf8_unicode_ci like '%".$keyword."%' or info_comment collate utf8_unicode_ci like '%".$keyword."%' ) ";
}

if(!$page)
{
    $page=1;
}
Else
{
    $offset=$view_count*($page-1);
}

//合計
$sql_count = "SELECT count(info_id) as all_count FROM info where 1 ".$where ;

$db_result_count = $common_dao->db_query_bind($sql_count);
if($db_result_count)
{
    $all_count = $db_result_count[0]["all_count"];
}

//リスト表示
$arr_db_field = array("info_id", "info_date", "info_title", "info_comment", "flag_user", "flag_member", "flag_shop", "regi_date", "up_date");

$sql = "SELECT ";
foreach($arr_db_field as $val)
{
    $sql .= $val.", ";
}
$sql .= " 1 FROM info ";
$sql .= " where 1 ".$where ;

if($order_name != "")
{
    $sql .= " order by ".$order_name." ".$order;
}
else
{
    $sql .= " order by info_date desc";
}
$sql .= " limit $offset,$view_count";

$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
    $inner_count = count($db_result);

    for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
    {
      foreach($arr_db_field as $val)
      {
        $$val = $db_result[$db_loop][$val];
      }
?>

										<tr>
											<td><input type="checkbox" class="check_del" value="<? echo $info_id;?>">
											</td>
											<td><a href="/masterdashboard/info/entry.php?info_id=<? echo $info_id;?>"><? echo $info_title;?></a>
											</td>
											<td><? echo $info_date;?></td>
											<td><? if($flag_user=="1") {?><i class="fa fa-check text-navy"></i><? } ?></td>
											<td><? if($flag_member=="1") {?><i class="fa fa-check text-navy"></i><? } ?></td>
											<td><? if($flag_shop=="1") {?><i class="fa fa-check text-navy"></i><? } ?></td>
											<td><button class="btn btn-white btn-xs" title="編集" onclick="location.href='./info.php?info_id=<? echo $info_id;?>'"><i class="fa fa-pencil"></i></button>
											</td>
										</tr>
<?php
    }
}
?>
									</tbody>

									<tfoot>
										<tr>
											<td colspan="5">
												<ul class="pagination pull-right"></ul>
											</td>
										</tr>
									</tfoot>

								</table>
								<div class="nextlist">
									<div class="btn-group">
										<?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
									</div>
								</div> 

							</div>
						</div>
					</div>
				</div>



			</div>
			<!--コンテンツここまで -->

			<!--フッター部分 -->
			<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
			<!--フッター部分 -->

		</div>

	</div>



	<!-- Mainly scripts -->
	<script src="/masterdashboard/js/jquery-2.1.1.js"></script>
	<script src="/masterdashboard/js/bootstrap.min.js"></script>
	<script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

	<!-- Custom and plugin javascript -->
	<script src="/masterdashboard/js/inspinia.js"></script>
	<script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>

	<!-- SUMMERNOTE -->
	<script src="/masterdashboard/js/plugins/summernote/summernote.min.js"></script>

	<!-- Dropzone -->
	<script src="/masterdashboard/js/plugins/dropzone/dropzone.js"></script>

	<!-- lity -->
	<script src="/masterdashboard/js/plugins/lity/lity.js"></script>

	<!-- Data picker -->
	<script src="/masterdashboard/js/plugins/datapicker/bootstrap-datepicker.js"></script>
	<script src="/masterdashboard/js/plugins/datapicker/datepicker-ja.min.js"></script>

	<script>
		$( '.date_added' ).datepicker( {
			format: 'yyyy/mm/dd',
			language: 'ja',
			autoclose: true,
			clearBtn: true,
		} );
		$( '.date_added' ).datepicker().datepicker( 'setDate', 'today' );
	</script>



</body>

</html>