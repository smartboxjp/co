<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>削除</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php
    //管理者チェック
    $common_connect -> Fn_admin_check();

    if(count($_GET["info_id"])==0)
    {
        $common_connect -> Fn_javascript_back("正しく選択してください。");
    }

    
    //削除処理
    foreach($_GET["info_id"] as $value) {
        if($value!="")
        {
            $db_del = "Delete from info where info_id='".$value."' ";
            $common_dao->db_update($db_del);
        }
    }
    
    $common_connect-> Fn_javascript_move("削除しました", "info.php");
?>
</body>
</html>