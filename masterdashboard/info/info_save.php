<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php   

    //管理者チェック
    $common_connect -> Fn_admin_check();
    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
    

    if($info_title == "" || $info_comment == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力してください。");
    }
    
    $datetime = date("Y/m/d H:i:s");


    //array
    $arr_db_field = array("info_date", "info_title", "info_comment", "flag_user", "flag_member", "flag_shop");
    if($info_id=="")
    {
        $db_insert = "insert into info ( ";
        $db_insert .= " info_id, ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val.", ";
        }
        $db_insert .= " regi_date, up_date ";
        $db_insert .= " ) values ( ";
        $db_insert .= " '', ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= " '".$$val."', ";
        }
        $db_insert .= " '$datetime', '$datetime')";
    }
    else
    {

        $db_insert = "update info set ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val."='".$$val."', ";
        }
        $db_insert .= " up_date='".$datetime."' ";
        $db_insert .= " where info_id='".$info_id."'";
    }
    $db_result = $common_dao->db_update($db_insert);

    if ($info_id == "")
    {
        //自動生成されてるID出力
        $sql = "select last_insert_id() as last_id";  
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            $info_id = $common_connect->h($db_result[0]["last_id"]);
        }
    }

    
    $common_connect-> Fn_javascript_move("登録・修正しました", "info.php?info_id=".$info_id);
?>
</body>
</html>