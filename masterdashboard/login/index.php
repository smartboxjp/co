<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonAdmin.php";
	$common_admin = new CommonAdmin();
	
	//ログイン
	if($_COOKIE['account'] != '')
	{
		$arr_data = array();
		$arr_data[] = "admin_name";
		$arr_data[] = "admin_id";
		$arr_data[] = "flag_open";
		
		$arr_where = array();
		$arr_where["flag_open"] = "all";
		$login_cookie = $_COOKIE['account'];
		$db_result = $common_admin->Fn_db_admin_login_cookie ($login_cookie, $arr_data, $arr_where=null);
		
		if($db_result)
		{
			$db_admin_id = $db_result[0]["admin_id"];
			$db_admin_name = $db_result[0]["admin_name"];
			$db_flag_open = $db_result[0]["flag_open"];
		}
		
		if($db_admin_id!="" && $db_flag_open=="1")
		{
			$_SESSION['admin_id']=$db_admin_id;
			$_SESSION['admin_name']=$db_admin_name;
			$_SESSION['admin_level']=$db_dmin_level;
			
			$common_connect->Fn_redirect(global_ssl."/masterdashboard/dashboard/");
		}
	}
	
	if($_COOKIE['account'] != '')
	{
		$arr_admin = $common_admin->Fn_login_cookie($_COOKIE['account']);
		
		if(!is_null($arr_admin))
		{
			foreach($arr_admin[0] as $key=>$value)
			{
				$$key=$value;
			}
		}
		
		if($admin_id!="" && $flag_open=="1")
		{
			$_SESSION['admin_id']=$admin_id;
			$_SESSION['admin_name']=$admin_name;
			$_SESSION['admin_level']=$dadmin_level;
			
			$common_connect->Fn_redirect(global_ssl."/masterdashboard/dashboard/");
		}
	}
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>ログイン | Login</title>

    <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="/masterdashboard/css/animate.css" rel="stylesheet">
    <link href="/masterdashboard/css/style.css" rel="stylesheet">
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">cst</h1>

            </div>
            <h3>cossot管理システム</h3>
            <p>管理者の指示に従ってログインをして下さい。</p>
      
            </p>
            <p>2016 System Produce  teamavalanche</p>
            <? $var = "form_regist"; ?>
            <form class="m-t" role="form" action="/masterdashboard/login/login_check.php" name="<? echo $var;?>" id="<? echo $var;?>" method="post">
                <div class="form-group">
                    <? $var = "admin_login";?>
                    <input type="email" name="<?=$var;?>" id="<?=$var;?>" class="form-control" placeholder="ID" required>
                    <label id="err_<?=$var;?>"></label>
                </div>
                <div class="form-group">
                    <? $var = "admin_pw";?>
                    <input type="password" name="<?=$var;?>" id="<?=$var;?>" class="form-control" placeholder="PASS" required>
                    <label id="err_<?=$var;?>"></label>
                </div>
                <? $var = "form_confirm";?>
                <button name="<?=$var;?>" id="<?=$var;?>" type="submit" class="btn btn-primary block full-width m-b">Login</button>

                <a href="http://cossot.com" target="_blank"><small>cossot.com</small></a>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="mailto:teamavalanche.and@gmail.com?subject=管理パスワード発行願い">help</a>
            </form>
            <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/bootstrap.min.js"></script>

</body>

</html>
