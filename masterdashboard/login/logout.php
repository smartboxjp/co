<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonAdmin.php";
	$common_admin = new CommonAdmin();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ログアウト</title>
</head>

<body>
<?
	session_start();
	//session_destroy();
	$admin_id = $_SESSION['admin_id'];
	$_SESSION['admin_id']="";
	$_SESSION['admin_name']="";

	setcookie("account", "");
	
	if($admin_id!="")
	{
		$arr_data = array();
		$arr_data["login_cookie"] = "";
		
		$arr_where = array();
		$arr_where["admin_id"] = $admin_id;
		
		$common_admin->Fn_admin_update ($arr_data, $arr_where) ;
		
	}
	
	echo ("<meta http-equiv='Refresh' content='0; URL=/masterdashboard/login/'>");
?>
</body>
</html>