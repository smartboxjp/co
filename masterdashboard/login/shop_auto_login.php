<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonAdmin.php";
    $common_admin = new CommonAdmin();
    
    //管理者チェック
    $common_connect -> Fn_admin_check();

    $redirect_url = $common_dao->db_string_escape($_GET["redirect_url"]);
    $shop_id = $common_dao->db_string_escape($_GET["shop_id"]);
    
    $sql = "SELECT shop_name FROM shop where shop_id='".$shop_id."' ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $shop_name = $db_result[0]["shop_name"];
    }

    if($shop_name=="")
    {
        $common_connect->Fn_javascript_back("正しく入力してください。");
    }

    $_SESSION['shop_id']=$shop_id;
    $_SESSION['shop_name']=$shop_name;
    
    $common_connect->Fn_redirect($redirect_url);
?>