<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonInquiry.php";
	$common_inquiry = new CommonInquiry();
	
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>メール詳細 | Mailbox</title>

    <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/masterdashboard/css/animate.css" rel="stylesheet">
    <link href="/masterdashboard/css/style.css" rel="stylesheet">
    <link href="/masterdashboard/css/pius.css" rel="stylesheet">
</head>

<body>
<?

	foreach($_GET as $key => $value)
	{ 
		//$$key = trim($common_dao->db_string_escape($value));
		$$key = $value;
	}
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	
	if($inquiry_id=="")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}

	$arr_data = array("inquiry_id", "member_id", "inquiry_sel", "inquiry_name", "inquiry_email", "inquiry_comment", "inquiry_status");
	$arr_data = array_merge($arr_data, array("regi_date", "up_date"));
	
	$db_result_inquiry = $common_inquiry -> Fn_db_inquiry_data($arr_data);
	foreach($db_result_inquiry as $arr_inquiry)
	{
		foreach($arr_inquiry as $key=>$value)
		{
			$$key = $value;
		}
	}
?>
    <div id="wrapper">

 <!--左ナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
<!--左ナビゲーションここまで -->

        <div id="page-wrapper" class="gray-bg">

<!--コンテンツ　ヘッダーナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
<!--コンテンツ　ヘッダーナビゲーションここまで -->

        <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12 animated fadeInRight">
            <div class="mail-box-header">

                <h2>
                    メッセージ
                </h2>
                <div class="mail-tools tooltip-demo m-t-md">

                    <h3>
                        <span class="font-noraml">タイトル: </span><? echo $inquiry_name;?>
                    </h3>
                    <h5>
                        <span class="pull-right font-noraml"><? echo $regi_date;?></span>
                        <span class="font-noraml">From: </span><? echo $inquiry_email;?>
                    </h5>
                </div>
            </div>
            <div class="mail-box">


                <div class="mail-body">

                    <p>
                        <? echo nl2br($inquiry_comment);?>
                    </p>
                </div>

                  <div class="mail-body text-right tooltip-demo">
                    <a class="btn btn-sm btn-white" href="mail_compose.html"><i class="fa fa-reply"></i>返信</a>
                    <button title="" data-placement="top" data-toggle="tooltip" type="button" data-original-title="" class="btn btn-sm btn-white"><i class="fa fa-print"></i>印刷</button>
                    <button title="" data-placement="top" data-toggle="tooltip" data-original-title="" class="btn btn-sm btn-white"><i class="fa fa-trash-o"></i>削除</button>
                  </div>
                  <div class="clearfix"></div>

                </div>
            </div>
        </div>
        </div>

<!--フッター部分 -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
<!--フッター部分 -->

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
    <script src="/masterdashboard/js/bootstrap.min.js"></script>
    <script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/masterdashboard/js/inspinia.js"></script>
    <script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>

</body>

</html>
