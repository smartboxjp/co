<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonInquiry.php";
	$common_inquiry = new CommonInquiry();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>お問い合わせ削除</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	foreach($_GET as $key => $value)
	{ 
		//$$key = trim($common_dao->db_string_escape($value));
		$$key = $value;
	}
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	$inquiry_count = 0;
	foreach($inquiry_id as $value) {
		if($value!="")
		{
			$inquiry_count++;
		}
	}
	
	if($inquiry_count==0)
	{
		$common_connect -> Fn_javascript_back("正しく選択してください。");
	}
	
	//削除処理
	foreach($inquiry_id as $value) {
		if($value!="")
		{
			$db_result = $common_inquiry->Fn_inquiry_delete ($value);

		}
	}
	
	$common_connect-> Fn_javascript_move("削除しました", "list.php");
?>
</body>
</html>