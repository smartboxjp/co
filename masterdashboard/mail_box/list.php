<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonInquiry.php";
	$common_inquiry = new CommonInquiry();
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>お問合せ | Mailbox</title>

    <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/masterdashboard/css/animate.css" rel="stylesheet">
    <link href="/masterdashboard/css/style.css" rel="stylesheet">

  <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
	<script type="text/javascript">
    $(function() {
			//チェックリストを一括削除
      $('#check_delete').click(function() {
				if(confirm('削除しますか？'))
				{
					check_del_list = $('[class="check_del"]:checked').map(function(){
						//$(this)でjQueryオブジェクトが取得できる。val()で値をvalue値を取得。
						return $(this).val();
					}).get().join('&inquiry_id[]=');
					document.location.href = './inquiry_del.php?inquiry_id[]='+check_del_list;
				}
      });
			
    });
		
  </script>
</head>

<body>

    <div id="wrapper">

<!--左ナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
<!--左ナビゲーションここまで -->

        <div id="page-wrapper" class="gray-bg">

<!--コンテンツ　ヘッダーナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
<!--コンテンツ　ヘッダーナビゲーションここまで -->


<!--メールボックス-->
        <div class="wrapper wrapper-content">
        <div class="row">

            <div class="col-lg-12 animated fadeInRight">
            <div class="mail-box-header">

                <form method="get" action="index.html" class="pull-right mail-search">
                    <div class="input-group">
                        <input type="text" class="form-control input-sm" name="search" placeholder="投稿者タイトルで検索">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-sm btn-primary">
                                Search
                            </button>
                        </div>
                    </div>
                </form>
                <h2>
                <?
									$return_inquiry = $common_inquiry -> Fn_inquiry_all_count ()  ;
									$return_inquiry_count=0;
									if(!is_null($return_inquiry))
									{
										$return_inquiry_count = $return_inquiry[0]["all_count"];
									}
                ?>
                    お問合せ<? if($return_inquiry_count!="0") { echo "（".$return_inquiry_count."）";}?>
                </h2>
                <div class="mail-tools tooltip-demo m-t-md">
                    <div class="btn-group pull-right">
                        <button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i></button>
                        <button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i></button>

                    </div>
                    <button id="check_delete" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="一括削除"><i class="fa fa-trash-o"></i> </button>

                </div>
            </div>
                <div class="mail-box">

                <table class="table table-hover table-mail">
                <tbody>
<?php
	$arr_inquiry_status = $common_inquiry->Fn_inquiry_status();
	$view_count=2;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$arr_where = array();
	$var = "s_keyword";
	if($$var != "")
	{
		$arr_where[$var] = $$var;
	}

	//合計
	$all_count = 0;
	$arr_inquiry_all = $common_inquiry -> Fn_inquiry_all_count ($arr_where);
	$all_count = $arr_inquiry_all[0]["all_count"];
	
	//リスト表示
	$arr_etc["order_name"] = $order_name;
	$arr_etc["order"] = $order;
	$arr_etc["offset"] = $offset;
	$arr_etc["view_count"] = $view_count;
	
	$arr_data = array("inquiry_id", "member_id", "inquiry_sel", "inquiry_name", "inquiry_email", "inquiry_comment", "inquiry_status");
	$arr_data = array_merge($arr_data, array("regi_date", "up_date"));
	
	$arr_inquiry_list = $common_inquiry -> Fn_inquiry_all_list ($arr_data, $arr_where, $arr_etc);

	foreach($arr_inquiry_list as $arr_key=>$arr_value)
	{
		foreach($arr_value as $key=>$value)
		{
			$$key=$value;
		}
?>
                <tr class="read">
                    <td class="check-mail">
                        <input type="checkbox" class="check_del" value="<? echo $inquiry_id;?>">
                    </td>
                    <td class="mail-ontact">
                      <a href="./entry.php?inquiry_id=<? echo $inquiry_id;?>"><? echo $inquiry_name;?></a>
                      <? if($inquiry_status!="1") { ?>
                      <span class="label label-danger pull-right"><? echo $arr_inquiry_status[$inquiry_status];?></span>
                      <? } ?>
                    </td>
                    <td class="mail-subject"><a href="./entry.php?inquiry_id=<? echo $inquiry_id;?>"><? echo $inquiry_sel;?></a></td>
                    <td class=""></td>
                    <td class="text-right mail-date"><? echo $regi_date;?></td>
                </tr>
<?
	}
?>

                </tbody>
                </table>


                </div>
            </div>
        </div>
        </div>

<!--フッター部分 -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
<!--フッター部分 -->

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
    <script src="/masterdashboard/js/bootstrap.min.js"></script>
    <script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/masterdashboard/js/inspinia.js"></script>
    <script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>

</body>

</html>
