<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | File Upload</title>

    <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="/masterdashboard/css/animate.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/lity/lity.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/masterdashboard/css/style.css" rel="stylesheet">
    <link href="/masterdashboard/css/plus.css" rel="stylesheet">

</head>

<body>

<div id="wrapper">

   

<!--左ナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
<!--左ナビゲーションここまで -->

	<div id="page-wrapper" class="gray-bg">


        <!--コンテンツ　ヘッダーナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
  
	<!--コンテンツ　ヘッダーナビゲーションここまで -->



	<!--コンテンツヘッダー -->
		<div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-lg-10">
				
			</div>
			<div class="col-lg-2">

                	</div>
		</div>
	<!--コンテンツヘッダーここまで -->

	<!--コンテンツここから -->
		<div class="wrapper wrapper-content  animated fadeInRight">
			<div class="row">
				<div class="col-lg-9">
					<div class="ibox ">
						<div class="ibox-title">
							<h5>データインポート</h5>
						</div>
						<div class="ibox-content">
							<strong>メルマガ送信の為のインポートするデータを選んで下さい</strong>
							<div class="row text-center">

								<div class="col-lg-4 h-100 p-lg">
									<p><i class="fa fa-download"></i> ユーザーのアドレスをインポート</p>
									<button class="btn btn-warning btn-sm demo1">登録ユーザー</button>
								</div>
								<div class="col-lg-4 h-100 p-lg">
									<p><i class="fa fa-download"></i> お店のアドレスをインポート</p>
									<button class="btn btn-success btn-sm demo2">登録の全店舗</button>
								</div>
								<div class="col-lg-4 h-100 p-lg">
									<p><i class="fa fa-download"></i> 全てのアドレスをインポート</p>
									<button class="btn btn-danger btn-sm demo3">ユーザー×店舗</button>
								</div>
							</div>

							<div class="row text-center">

								<div class="col-lg-12 h-100 p-lg">
									<p>メルマガを配信する</p>
									<button class="btn btn-primary btn-sm demo3"><i class="fa fa-envelope-o"></i>メルマガ送信サイト【】へ</button>
								</div>
							</div>

							<div class="m-t-md">
                            	<p>
                                 ※メルマガの配信は注意をよく守り送信してください。
                                </p>

<pre>
メルマガシステム注意事項
1、メルマガの配信は原則週に一回程度にする事。
2、配信時間帯を22時頃にすること。
3、HTMLメールでの画像はロゴを含め5枚まで。
4、最大で35文字で改行。
5、必ず配信前に各デバイスでテストメールをする事。
6、
</pre>
                            </div>


						</div>
					</div>
				</div>


				<!--データ部分-->
				<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/right_deta.php"; ?>
				<!--データ部分-->
			</div>
		</div>
	<!--コンテンツここまで -->

<!--フッター部分 -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
<!--フッター部分 -->

        </div>

   </div>



    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
    <script src="/masterdashboard/js/bootstrap.min.js"></script>
    <script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/masterdashboard/js/inspinia.js"></script>
    <script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>

    <!-- SUMMERNOTE -->
    <script src="/masterdashboard/js/plugins/summernote/summernote.min.js"></script>

    <!-- Dropzone -->
    <script src="/masterdashboard/js/plugins/dropzone/dropzone.js"></script>

    <!-- lity -->
    <script src="/masterdashboard/js/plugins/lity/lity.js"></script>


</body>

</html>
