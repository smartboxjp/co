<?php
require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_include/connect.php";
$common_connect = new CommonConnect();
$common_dao = new CommonDao(); //DB関連

require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_model/CommonCateMatome.php";
$common_catematome = new CommonCateMatome();
require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_model/CommonMatome.php";
$common_matome = new CommonMatome();

//カテゴリ
$arr_matome = $common_catematome->Fn_cate_matome_list();
if ( !is_null( $arr_matome ) ) {
	foreach ( $arr_matome as $arr_key => $arr_value ) {
		$arr_cate_matome_title[ $arr_value[ "cate_matome_id" ] ] = $arr_value[ "cate_matome_title" ];
		$arr_cate_matome_color[ $arr_value[ "cate_matome_id" ] ] = $arr_value[ "cate_matome_color" ];
	}
}

foreach ( $_GET as $key => $value ) {
	$$key = $common_connect->h($value);
}

if ( $matome_id != "" ) {
	$arr_where = array();
	$arr_where[ "matome_id" ] = $matome_id;

	$arr_data = array( "matome_id", "matome_title", "cate_matome_id", "img_1", "matome_comment", "flag_open" );
	$arr_data = array_merge( $arr_data, array( "regi_date", "up_date" ) );

	$db_result_matome = $common_matome->Fn_matome_list( $arr_data, $arr_where );
	foreach ( $db_result_matome as $arr_matome ) {
		foreach ( $arr_matome as $key => $value ) {
			$$key = $value;
		}
	}
}
?>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>INSPINIA | Text Editor</title>

	<link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
	<link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="/masterdashboard/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="/masterdashboard/css/animate.css" rel="stylesheet">
	<link href="/masterdashboard/css/plugins/summernote/summernote.min.css" rel="stylesheet">
	<link href="/masterdashboard/css/style.css" rel="stylesheet">
	<link href="/masterdashboard/css/plus.css" rel="stylesheet">
	<script src="/masterdashboard/js/jquery-2.1.1.js"></script>
	<script>
		$( function () {
			var setFileInput = $( '.imgInput' );

			setFileInput.each( function () {
				var selfFile = $( this ),
					selfInput = $( this ).find( 'input[type=file]' );

				selfInput.change( function () {
					var file = $( this ).prop( 'files' )[ 0 ],
						fileRdr = new FileReader(),
						selfImg = selfFile.find( '.imgView' );

					if ( !this.files.length ) {
						if ( 0 < selfImg.size() ) {
							selfImg.remove();
							return;
						}
					} else {
						if ( file.type.match( 'image.*' ) ) {
							if ( !( 0 < selfImg.size() ) ) {
								selfFile.append( '<img alt="" class="imgView">' );
							}
							var prevElm = selfFile.find( '.imgView' );
							fileRdr.onload = function () {
								prevElm.attr( 'src', fileRdr.result );
							}
							fileRdr.readAsDataURL( file );
						} else {
							if ( 0 < selfImg.size() ) {
								selfImg.remove();
								return;
							}
						}
					}
				} );
			} );
		} );
	</script>

	<script type="text/javascript">
		$( function () {

			//イメージクリック
			$( '#insert_img_1' ).click( function () {
				$( ".note-editable" ).append( "<img src='/n" + $( this ).attr( "src" ) + "'>/n" );
			} );

			$( '#form_confirm' ).click( function () {
				$( "#matome_comment" ).val( $( ".note-editable" ).html() );
				err_default = "";
				err_check_count = 0;
				bgcolor_default = "#FFFFFF";
				bgcolor_err = "#FFCCCC";
				background = "background-color";

				err_check_count += check_input( "matome_title" );
				err_check_count += check_input( "cate_matome_id" );

				if ( err_check_count != 0 ) {
					alert( "入力に不備があります" );
					return false;
				} else {
					$( '#form_confirm' ).submit();
					//$( '#form_confirm', "body" ).submit();
					return true;
				}
			} );

			function check_input( $str ) {
				$( "#err_" + $str ).html( err_default );
				$( "#" + $str ).css( background, bgcolor_default );

				if ( $( '#' + $str ).val() == "" ) {
					err = "<span class='errorForm'>正しく入力してください。</span>";
					$( "#err_" + $str ).html( err );
					$( "#" + $str ).css( background, bgcolor_err );

					return 1;
				}
				return 0;
			}

			function check_input_id( $str ) {
				$( "#err_" + $str ).html( err_default );
				$( "#" + $str ).css( background, bgcolor_default );

				if ( $( '#' + $str ).val() == "" ) {
					err = "<span class='errorForm'>正しく入力してください。</span>";
					$( "#err_" + $str ).html( err );
					$( "#" + $str ).css( background, bgcolor_err );

					return 1;
				} else if ( checkID( $( '#' + $str ).val() ) == false ) {
					err = "<span class='errorForm'>英数半角で入力してください。</span>";
					$( "#err_" + $str ).html( err );
					$( "#" + $str ).css( background, bgcolor_err );

					return 1;
				} else if ( $( '#' + $str ).val().length < 3 ) {
					err = "<span class='errorForm'>3文字以上で入力してください。</span>";
					$( "#err_" + $str ).html( err );
					$( "#" + $str ).css( background, bgcolor_err );

					return 1;
				}
				return 0;
			}

		} );

	    $(function() {
			//チェックリストを一括削除
	      $('#check_delete').click(function() {
			if(confirm('削除しますか？'))
			{
				check_del_list = $('[class="check_del"]:checked').map(function(){
					//$(this)でjQueryオブジェクトが取得できる。val()で値をvalue値を取得。
					return $(this).val();
				}).get().join('&matome_id[]=');
				document.location.href = './matome_del.php?matome_id[]='+check_del_list;
			}
	      });
				
	    });
		//-->
	</script>

	<script type="text/javascript">
		$( function () {
			$( '#img_1' ).change( function () {
				$( '#dummy_file' ).val( $( this ).val() );
			} );
		} )
	</script>

</head>

<body>

	<div id="wrapper">

		<!--左ナビゲーション -->
		<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
		<!--左ナビゲーションここまで -->

		<div id="page-wrapper" class="gray-bg">


			<!--コンテンツ　ヘッダーナビゲーション -->
			<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
			<!--コンテンツ　ヘッダーナビゲーションここまで -->



			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">
					<h2>綺麗になるまとめ</h2>

				</div>
				<div class="col-lg-2">

				</div>
			</div>


			<div class="wrapper wrapper-content">

				<div class="row">
					<form action="./matome_save.php" method="POST" name="form_write" id="form_regist" enctype="multipart/form-data" class="form-horizontal">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">

								<div class="ibox-title">
									<h5>記事の投稿をする</h5>
									<div class="ibox-tools">
										<a class="collapse-link">
				                        <i class="fa fa-chevron-up"></i>
				                    </a>
									




										<a class="close-link">
					                        <i class="fa fa-times"></i>
					                    </a>
									



									</div>
								</div>



								<? $var = "matome_id"; ?>
								<input type="hidden" name="<? echo $var;?>" value='<? echo $$var;?>'>
								<? $var = "matome_comment"; ?>
								<input type="hidden" name="<? echo $var;?>" id="<? echo $var;?>" value='<? echo $$var;?>'>

								<div class="ibox-content">
									<!--記事の種類ボックス-->
									<div class="form-group"><label class="col-sm-2 control-label">記事の種類を選択</label>
										<div class="col-sm-6">
											<? $var = "cate_matome_id";?>
											<select class="form-control m-b" name="<? echo $var;?>" id="<? echo $var;?>">
											<? foreach($arr_cate_matome_title as $key=>$value) { ?>
												<option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
											<? } ?>
											</select>
										
										</div>
									</div>
									<!--記事の種類ボックス-->

									<!--記事のタイトルボックス-->
									<div class="form-group"><label class="col-sm-2 control-label">タイトル</label>
										<div class="col-sm-6">
											<? $var = "matome_title";?>
											<input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="記事タイトルを30文字以内で記入してください" class="form-control">
											<label id="err_<?php echo $var;?>"></label>
										</div>
									</div>
									<!--記事のタイトルボックス-->

									<!--サムネイル画像-->
									<div class="form-group">
										<label class="col-sm-2 control-label">サムネイル画像</label>
										<div class="col-sm-4">

											<!--ここから-->
											<div class="input-group">
												<? $var = "img_1";?>
												<input type="file" id="<? echo $var;?>" name="<? echo $var;?>" style="display: none;" accept="image/*">
												<input type="hidden" name="text_<?=$var;?>" value="<?=$$var;?>">
												<span class="input-group-btn">
													<button class="btn btn-default" type="button" onclick="$('#<? echo $var;?>').click();"><i class="glyphicon glyphicon-folder-open"></i></button>
												</span>
											



												<input id="dummy_file" type="text" class="form-control" placeholder="select file..." disabled>

											</div>
											<?
												if($$var!="") {
													echo "<img src='/".global_matome_dir.$matome_id."/".$$var."?d=".date(his)."' class='primgrandom' id='insert_".$var."'>";
												}
											?>
											<label id="err_<?php echo $var;?>"></label>
										</div>
										
										<div class="col-sm-4">
										<p class="topmargin">※サムネイルは3：2比率で制作すること</p>
										</div>


									</div>
									<!--サムネイル画像-->
									

									<div class="form-group"><label class="col-sm-2 control-label">内容</label>
										<div class="col-sm-8">
											<div class="summernote">
												
												<? echo $matome_comment;?>
												
											</div>
										
										</div>
									</div>
										
											
											<script type="text/javascript">
												$( function () {
													$( '.summernote' ).summernote( {
														width: 756,
														height: 300,
														disableDragAndDrop: true,
														toolbar: [
        													['style', ['style']],
        													['font', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
        													['fontname', ['fontname']],
        													['fontsize', ['fontsize']],
        													['color', ['color']],
        													['para', ['ul', 'ol', 'paragraph']],
        													['height', ['height']],
        													['table', ['table']],
        													['insert', ['link', 'picture', 'video', 'hr']],
        													['view', ['fullscreen', 'codeview']],
        													['insert', ['template']],
        													['help', ['help']]
      														],
														defaultFontName: 'Meiryo',
														fontNames: ["YuGothic","Yu Gothic","Hiragino Kaku Gothic Pro","sans-serif", "Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana"],
														fontSizes: ['11', '12', '13', '16', '18', '20', '24', '28'],
  														template: {
													        path: './template', // path to your template folder
													        list: [ // list of your template (without the .html extension)
													            'template1',
													            'template2'
													        ]
													    },
														callbacks: {
															onImageUpload: function ( files, editor, welEditable ) {
																console.log( 'image upload:', files );
																$.each( files, function ( i, val ) {
																	sendFile( files[ i ], editor, welEditable );
																} );
															},
														}
													} );

													function sendFile( file, editor, welEditable ) {
														data = new FormData();
														data.append( "file", file );
														$.ajax( {
															url: "saveimage.php", // image 저장 소스
															data: data,
															cache: false,
															contentType: false,
															processData: false,
															type: 'POST',
															success: function ( data ) {
																//       alert(data);
																var image = $( '<img style="max-width:100%;">' ).attr( 'src', '' + data ); // 에디터에 img 태그로 저장을 하기 위함
																$( '.summernote' ).summernote( "insertNode", image[ 0 ] ); // summernote 에디터에 img 태그를 보여줌
																//       editor.insertImage(welEditable, data);
															},
															error: function ( jqXHR, textStatus, errorThrown ) {
																console.log( textStatus + " " + errorThrown );
															}
														} );
													}
													/*
   $('form').on('submit', function (e) {
    e.preventDefault();
//     alert($('.summernote').summernote('code'));
//     alert($('.summernote').val());
   });
	 */
												} );

											</script>


								</div>
								<!--ibox-content-->


							</div>
							<!--ibox-title-->


						</div>
						<!--ibox float-e-margins-->

						<!--登録ボタン-->
						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<div class="col-sm-4 col-sm-offset-2">
								<? $var = "form_confirm";?>
								<button class="btn btn-primary" type="submit" id="<? echo $var;?>">記事を登録</button>
							</div>
						</div>
						<!--登録ボタン-->
				</div>
				<!--col-lg-12-->
				</form>

			</div>
			<!--row-->


			<!--ここから記事データ-->



			<div class="wrapper wrapper-content  animated fadeInRight">
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox">

							<div class="ibox-title">
								<h5>投稿した記事リスト</h5>
							</div>

							<div class="ibox-content">
								<div class="m-b-lg">
									<form action="index.php" method="GET">
									<div class="input-group">
										<input name="s_keyword" value="<? echo $s_keyword;?>" type="text" placeholder="投稿した記事を検索する" class=" form-control">
										<span class="input-group-btn">
	                                        <button type="submit" class="btn btn-white">検索</button>
	                                    </span>
									</div>
									</form>


<?php
    $view_count=20;   // List count
    $offset=0;
    $where = " ";
    $keyword = preg_replace("/( |　|   )/", "", $keyword );

    if($keyword!="")
    {
    	$where .= " and (matome_title collate utf8_unicode_ci like :".$var_word_1." or or matome_comment collate utf8_unicode_ci like :".$var_word_2."  or matome_id = :".$var." ) ";
    }

    if(!$page)
    {
        $page=1;
    }
    Else
    {
        $offset=$view_count*($page-1);
    }
    
    //合計
    $sql_count = "SELECT count(matome_id) as all_count FROM matome where 1 ".$where ;
    
    $db_result_count = $common_dao->db_query_bind($sql_count);
    if($db_result_count)
    {
        $all_count = $db_result_count[0]["all_count"];
    }
    
    //リスト表示
    $arr_db_field = array( "matome_id", "matome_title", "cate_matome_id", "img_1", "matome_comment", "flag_open" );
    $arr_db_field = array_merge( $arr_db_field, array( "regi_date", "up_date" ) );
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM matome ";
    $sql .= " where 1 ".$where ;
    
    if($order_name != "")
    {
        $sql .= " order by ".$order_name." ".$order;
    }
    else
    {
        $sql .= " order by matome_id desc";
    }
    $sql .= " limit $offset,$view_count";
?>
									<div class="m-t-md">
										<div class="pull-right">
											<button id="check_delete" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="一括削除"><i class="fa fa-trash-o"></i> </button>

											<button 削class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="ページを確認する"><i class="fa fa-desktop"></i> </button>
										</div>
										<strong>投稿数(<? echo $all_count;?>)</strong>
									</div>

								</div>

								<div class="table-responsive">
									<table class="table table-hover issue-tracker">
										<tbody>
<?php
    $arr_matome_list = $common_dao->db_query_bind($sql);
    if($arr_matome_list)
    {
        $inner_count = count($arr_matome_list);
    
        for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
        {
          foreach($arr_db_field as $val)
          {
            $$val = $arr_matome_list[$db_loop][$val];
          }
          $reserve_day = $arr_matome_list[$db_loop]["reserve_day"];　
?>
											<tr>

												<td>
													<td><input type="checkbox" class="check_del" value="<? echo $matome_id;?>">
												</td>
												</td>
												<td>
													<span class="label <? echo $arr_cate_matome_color[$cate_matome_id]; ?>"><? echo $arr_cate_matome_title[$cate_matome_id];?></span>
													<!--label-peach 恋愛-->
												</td>
												<td>
													<td>
													<?
														$var = "img_1";
														if($$var!="") {
															echo "<img src='/".global_matome_dir.$matome_id."/".$$var."' height='40px'>";
														}
													?>
													</td>
												</td>
												<td class="issue-info">
													<a href="?matome_id=<? echo $matome_id;?>"><? echo $matome_title;?></a>
												


													<small><? if(mb_strlen(trim(strip_tags($matome_comment)), "utf-8")>32) {echo mb_substr(trim(strip_tags($matome_comment)), 0, 32, "utf-8")."..."; } else {echo mb_substr(trim(strip_tags($matome_comment)), 0, 32, "utf-8");}?></small>
												



												</td>
												<td>
													<? echo substr($regi_date, 0, 10);?>
												</td>
												<td>
													<? echo substr($regi_date, 11, 5);?>投稿
												</td>
												<td class="text-right">
													<button class="btn btn-white btn-xs" title="編集" onclick="location.href='?matome_id=<? echo $matome_id;?>'"> <i class="fa fa-pencil"></i></button>
													

												</td>
											</tr>
<?
		}
	}
?>

										</tbody>

									</table>

									<div class="nextlist">
										<div class="btn-group">
											<?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
										</div>
									</div>
								</div>
							</div>
							<!--ibox-content-->

						</div>
						<!--ibox-->
					</div>
					<!--col-lg-12-->
				</div>
				<!--row-->
			</div>
			<!--記事データここまで-->





			<!--フッター部分 -->
			<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
			<!--フッター部分 -->
		</div>
	</div>



	<!-- Mainly scripts -->

	<script src="/masterdashboard/js/bootstrap.min.js"></script>
	<script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

	<!-- Peity -->
	<script src="/masterdashboard/js/plugins/peity/jquery.peity.min.js"></script>

	<!-- Custom and plugin javascript -->
	<script src="/masterdashboard/js/inspinia.js"></script>
	<script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>

	<!-- iCheck -->
	<script src="/masterdashboard/js/plugins/iCheck/icheck.min.js"></script>

	<!-- SUMMERNOTE -->
	<script src="/masterdashboard/js/plugins/summernote/summernote.js"></script>
	<script src="/masterdashboard/js/plugins/summernote/summernote-ext-template.js"></script>

	<!-- Peity -->
	<script src="/masterdashboard/js/demo/peity-demo.js"></script>

	<script>
		$( document ).ready( function () {
			$( '.i-checks' ).iCheck( {
				checkboxClass: 'icheckbox_square-green',
				radioClass: 'iradio_square-green',
			} );
		} );
	</script>



</body>

</html>