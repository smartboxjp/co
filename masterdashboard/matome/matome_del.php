<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>削除</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php
    //管理者チェック
    $common_connect -> Fn_admin_check();

    if(count($_GET["matome_id"])==0)
    {
        $common_connect -> Fn_javascript_back("正しく選択してください。");
    }

    
    //削除処理
    foreach($_GET["matome_id"] as $value) {
        if($value!="")
        {
            $db_del = "Delete from matome where matome_id='".$value."' ";
            $common_dao->db_update($db_del);
            
            if(file_exists($_SERVER['DOCUMENT_ROOT']."/".global_matome_dir.$value))
            {
                $common_connect -> Fn_deldir($_SERVER['DOCUMENT_ROOT']."/".global_matome_dir.$value);
            }
        }
    }
    
    $common_connect-> Fn_javascript_move("削除しました", "index.php");
?>
</body>
</html>