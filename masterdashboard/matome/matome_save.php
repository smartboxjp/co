<?php
	//error_reporting(0);
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMatome.php";
	$common_matome = new CommonMatome();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	

	foreach($_POST as $key => $value)
	{ 
		$$key = $value;
		//$$key = $common_connect->h($value);
	}
	//管理者チェック
	$common_connect -> Fn_admin_check();
	

	if($matome_title == "" || $cate_matome_id == "")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
	
	$datetime = date("Y/m/d H:i:s");
	$user_agent = substr(getenv("HTTP_USER_AGENT"), 0, 250);
	$flag_open = 1;
	
	//array
	$arr_data = array();
	$arr_db_field = array("matome_title", "cate_matome_id", "matome_comment");
	$arr_db_field = array_merge($arr_db_field, array("flag_open"));
	
	$arr_data = array();
	//基本情報
	if($matome_id=="")
	{
		foreach($arr_db_field as $value) {
			$arr_data[$value] = $$value;
		}
		$common_matome -> Fn_matome_insert($arr_data);
		
		$result = $common_matome -> Fn_db_apply_auto();
		$matome_id = $result[0]["last_id"];
	}
	else
	{
		$arr_where = array();
		$var = "matome_id"; $arr_where[$var] = $$var;
		foreach($arr_db_field as $value) {
			$arr_data[$value] = $$value;
		}
		$common_matome -> Fn_matome_update($arr_data, $arr_where);
	}
	
	//Folder生成
	$save_dir = $global_path.global_matome_dir.$matome_id."/";
	$common_image -> create_folder ($save_dir);

	
	$new_end_name="_1";
	$fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $matome_id, $text_img_1, "");
	
	
	$arr_where = array();
	$arr_data = array();
	$var = "matome_id"; $arr_where[$var] = $$var;
	$arr_data["img_1"] = $fname_new_name[1];
	$common_matome -> Fn_matome_update($arr_data, $arr_where);

	/* 画像処理 start */
	$save_dir_content = $global_path.global_matome_dir.$matome_id."/content/";
	$common_image -> create_folder ($save_dir_content);

	$dom = new domDocument;
	libxml_use_internal_errors(true);
	$dom->loadHTML(html_entity_decode($matome_comment));
	$dom->preserveWhiteSpace = false;
	$imgs  = $dom->getElementsByTagName("img");
	$links = array();
	$files = glob($path . "*.*");
	for($i = 0; $i < $imgs->length; $i++) {
		 $links[] = $imgs->item($i)->getAttribute("src");
	}

	$arr_basename = array();
	foreach ($links as $key => $value) {
		$pathData = pathinfo($global_path.$value);
		$arr_basename[] = $pathData["basename"];
		if(file_exists($global_path.global_admin_temp_dir.$pathData["basename"])) {
			rename($global_path.global_admin_temp_dir.$pathData["basename"], $save_dir_content.$pathData["basename"]);
		}
	}


	$matome_comment = str_replace(global_admin_temp_dir, global_matome_dir.$matome_id."/content/", $matome_comment);

	$arr_where = array();
	$arr_where["matome_id"] = $matome_id;
	$arr_data = array();
	$arr_data["matome_comment"] = $matome_comment;
	foreach($arr_db_field as $value) {
		$arr_data[$value] = $$value;
	}
	$common_matome -> Fn_matome_update($arr_data, $arr_where);
	/* 画像処理 end */


	/* 登録されているファイル以外削除 start */
	$dir = opendir($global_path.global_matome_dir.$matome_id."/content/");
	while (false !== ($file = readdir($dir))){
		if($file[0] != "."){

			if(!in_array($file, $arr_basename))
			{
				unlink ($global_path.global_matome_dir.$matome_id."/content/".$file);
			}
		}
	}
	closedir($dir);
	/* 登録されているファイル以外削除 end */

	/* 古いファイル削除 start */
	$delete_day = strtotime("48 hours ago");

	$dir = opendir($global_path.global_admin_temp_dir);
	while (false !== ($file = readdir($dir))){
		if($file[0] != "."){
			if ($delete_day > filemtime($global_path.global_admin_temp_dir.$file)) {
				unlink($global_path.global_admin_temp_dir.$file);
			}
		}
	}
	closedir($dir);
	/* 古いファイル削除 end */

	
	$common_connect-> Fn_javascript_move("登録・修正しました", "index.php?matome_id=".$matome_id);
?>
</body>
</html>