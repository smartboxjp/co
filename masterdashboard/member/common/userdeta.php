            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-content mailbox-content">
                        <div class="file-manager">


                            <div class="m-b-sm">
                                <img alt="image" class="img-circle" src="/masterdashboard/img/11.gif"style="width: 60px">
								<strong>リズリサさん</strong>
                            </div>

                            <a class="btn btn-block btn-primary compose-mail" href="/masterdashboard/member/user_mailform.php"><i class="fa fa-envelope-o"></i> ユーザーへメール</a>
                            <div class="space-25"></div>
                            <h5>Sランク</h5>
                            <ul class="folder-list m-b-md" style="padding: 0">
                                <li> <i class="fa fa-calendar-o"></i>登録日<span class="pull-right">2017/08/25</span></li>
								<li> <i class="fa fa-pencil-square-o"></i>ユーザーID<span class="pull-right">u2546</span></li>
								<li> <i class="fa fa-envelope-o"></i>アドレス<span class="pull-right">hjfkarafrfa@faserjhfa</span></li>
                                <li> <i class="fa fa-envelope-square"></i>受け取ったメッセージ<span class="label label-info pull-right">12</span></li>
                                <li> <i class="fa fa-globe"></i>第一エリア<span class="pull-right">東京都</span></li>
                                <li> <i class="fa fa-globe"></i>第二エリア<span class="pull-right">池袋.大塚.巣鴨</span></li>
                                <li> <i class="fa fa-desktop"></i>最終ログイン<span class="pull-right">2016/08/25(08:17)</span></li>
                                <li> <i class="fa fa-times-circle-o"></i>アクセス禁止<span class="pull-right"><div class="onoffswitch"><input type="checkbox" checked class="onoffswitch-checkbox" id="example2"><label class="onoffswitch-label" for="example2"><span class="onoffswitch-inner"></span><span class="onoffswitch-switch"></span></label></div></span></li>
                            </ul>

			　　			<div class="space-25"></div>
                            <h5>投稿分析</h5>
                            <ul class="folder-list m-b-md" style="padding: 0">
                                <li> <i class="fa fa-comment-o"></i>投稿口コミ数<span class="pull-right">30件</span></li>
								<li> <i class="fa fa-picture-o"></i>投稿画像枚数<span class="pull-right">8枚</span></li>
                                <li> <i class="fa fa-line-chart"></i>平均評価点<span class="pull-right">3.65点</span></li>
                                <li> <i class="fa fa-thumbs-o-up"></i>参考になった<span class="pull-right">17回</span></li>
                            </ul>

                            <h5>お店への口コミの傾向</h5>
                            <ul class="folder-list m-b-md" style="padding: 0">
							<li><i class="fa fa-chevron-right"></i>【素晴らしい】<span class="pull-right">17店舗</span></l>
							<li><i class="fa fa-chevron-right"></i>【平均的】<span class="pull-right">1店舗</span></li>
							<li><i class="fa fa-chevron-right"></i>【イマイチ】<span class="pull-right">1店舗</span></li>
							<li><i class="fa fa-chevron-right"></i>【二度と働かない】<span class="pull-right">7店舗</span></li>
                            </ul>

                            <h5>投稿口コミの信憑性</h5>
                            <ul class="category-list" style="padding: 0">
                                <li><i class="fa fa-circle text-danger"></i>信憑性大<span class="pull-right">17軒</span></li>
                                <li><i class="fa fa-circle text-warning"></i>信憑性小<span class="pull-right">9軒</span></li>
                                <li><i class="fa fa-circle text-navy"></i>信憑性イマイチ<span class="pull-right">17軒</span></li>
                                <li><i class="fa fa-circle text-primary"></i>信憑性なし<span class="pull-right">17軒</span></li>
                                <li><i class="fa fa-circle text-muted"></i>判定できず<span class="pull-right">17軒</span></li>
                            </ul>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
