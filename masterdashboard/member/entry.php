<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_catearea = new CommonCateArea();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	//エリア
	$arr_area = $common_catearea->Fn_cate_area_list();
	if(!is_null($arr_area))
	{
		foreach($arr_area as $arr_key=>$arr_value)
		{
			foreach($arr_value as $key=>$value)
			{
				$arr_cate_area_s_id[$arr_value["cate_area_s_id"]] = $arr_value["cate_area_l_title"]." - ".$arr_value["cate_area_s_title"];
			}
		}
	}
	
	foreach($_GET as $key => $value)
	{ 
		//$$key = trim($common_dao->db_string_escape($value));
		$$key = $value;
	}
	
	$arr_where = array();
	$var = "s_flag_open";
	//$arr_where[$var] = "1"; //公開のみ
	
	$arr_data = array("member_id", "login_email", "login_pw", "member_login_cookie", "nickname");
	$arr_data = array_merge($arr_data, array("twitter", "facebook", "yahoo", "cate_area_l_id", "cate_area_s_id", "member_img"));
	$arr_data = array_merge($arr_data, array("member_comment", "flag_open", "lastlogin_date", "ranking"));
	$arr_data = array_merge($arr_data, array("regi_date", "up_date"));
	
	$db_result_member = $common_member -> Fn_db_member_data($member_id, $arr_data, $arr_where);
	foreach($db_result_member as $arr_member)
	{
		foreach($arr_member as $key=>$value)
		{
			$$key = $value;
		}
	}
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>ユーザー登録・修正 | ダッシュボード</title>

    <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/iCheck/custom.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/chosen/chosen.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/cropper/cropper.min.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/switchery/switchery.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/nouslider/jquery.nouislider.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/select2/select2.min.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">

    <link href="/masterdashboard/css/animate.css" rel="stylesheet">
    <link href="/masterdashboard/css/style.css" rel="stylesheet">


    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>

</head>

<body>
<?
	//管理者チェック
	$common_connect -> Fn_admin_check();
?>

    <div id="wrapper">

<!--左ナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
   <!--左ナビゲーションここまで -->



	<div id="page-wrapper" class="gray-bg">



<!--コンテンツ　ヘッダーナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
<!--コンテンツ　ヘッダーナビゲーションここまで -->




<!--店舗登録フォームヘッダー-->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>新規ユーザー登録</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/masterdashboard/shop/entry.php">店舗登録</a>
                        </li>
                        <li>
                            <strong>ユーザー登録</strong></a>
                        </li>

                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
<!--店舗登録フォームヘッダー-->

<!--コンテンツ-->
        <div class="wrapper wrapper-content animated fadeInRight">


           

<!--登録フォームここから-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
<!--基本情報ヘッダー-->
                        <div class="ibox-title">
                            <h5>基本情報登録<small>-ユーザーの基本情報は全て入力が必要です。</small></h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
<!--基本情報ヘッダー-->
                        <div class="ibox-content">
                        	<form action="./entry_save.php" method="POST" name="form_write" id="form_regist" enctype="multipart/form-data" class="form-horizontal">
                          <? $var = "member_id"; ?>
                          <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">

<!--テキストフォームコメント付き-->
                                <div class="form-group"><label class="col-sm-2 control-label">メール</label>
                                    <div class="col-sm-6">
                                    <? $var = "login_email"; ?>
                                    <? if($$var=="") { $$var=date("Ymd").$common_connect->Fn_random_password(10)."@cossot.com";}?>
                                    <input type="text" class="form-control demo" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>"> 
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                
                                <div class="form-group"><label class="col-sm-2 control-label">パスワード</label>
                                    <div class="col-sm-6">
                                    <? $var = "login_pw"; ?>
                                    <? if($$var=="") { $$var=date("Ymd").$common_connect->Fn_random_password(10);}?>
                                    <input type="text" class="form-control demo" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>"> 
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                
                                <div class="form-group"><label class="col-sm-2 control-label">ユーザーの名前</label>
                                    <div class="col-sm-6">
                                    <? $var = "nickname"; ?>
                                    <input type="text" class="form-control demo" maxlength="8" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>"> 
                                    <span class="help-block m-b-none">ユーザーの名前を最大8文字以内で入力して下さい。</span>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
<!---->


<!--エリア-->
                                <div class="form-group"><label class="col-sm-2 control-label">エリア</label>

                                    <div class="col-sm-6">
                                    <? $var = "cate_area_s_id";?>
                                    <select name="<?php echo $var;?>" id="<?php echo $var;?>" class="form-control m-b" >
                                      <option value="">お店や事務所のある場所はどこですか？</option>
                                        <?
																			foreach($arr_cate_area_s_id as $key=>$value)
																			{
																		?>
                                    		<option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
																		<?
																			}
																		?>
                                    
                                    </select>
                                    
                                    </div>
                                </div>
				
<!--エリア-->



<!--イメージクリッパー-->
                    <div class="ibox-title  back-change">
                        <h5>ユーザーのサムネイル画像を登録<small>※無登録でも可能。またユーザー管理で登録や編集も出来ます。</small></h5>

                    </div>
                    <div class="ibox-content">
                        <p>
                            ユーザーのサムネイル画像をトリミングで編集登録が出来ます。60px×60pxで表示されます。
                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="image-crop">
                                    <img src="/masterdashboard/img/p3.jpg">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h4>Preview image</h4>
                                <div class="img-preview img-preview-sm"></div>
                                <p>
                                    画像の好きな位置に枠をあわせて決定を押してください。
                                </p>
                                <div class="btn-group">
                                    <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                        <input type="file" accept="image/*" name="file" id="inputImage" class="hide">
                                        カットしたい画像をアップ
                                    </label>
                                </div>

                                <div class="btn-group">
                                    <button class="btn btn-white" id="zoomIn" type="button">Zoom In</button>
                                    <button class="btn btn-white" id="zoomOut" type="button">Zoom Out</button>
                                    <button class="btn btn-white" id="rotateLeft" type="button">Rotate Left</button>
                                    <button class="btn btn-white" id="rotateRight" type="button">Rotate Right</button>
                                </div>
				<br>
                                <div class="btn-group">
                                    サムネイルを反映する
                                  <div class="onoffswitch">
                                  <? $var = "check_img";?>
                                  <input type="checkbox" name="<? echo $var;?>" value="1" class="onoffswitch-checkbox" id="checkbox_<? echo $member_id;?>">
                                  <label class="onoffswitch-label" for="checkbox_<? echo $member_id;?>">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                  </label>
                                </div>
                                  <?
																	if($member_img!="")
																	{
																		echo "<img src='/".global_member_dir.$member_id."/".$member_img."?".date("his")."'>";
																	}
																	?>
                                </div>
                            </div>
                        </div>
                    </div>
<!--イメージクリッパー-->

<input type="hidden" name="cropped" class="cropped">


<!--登録ボタン-->
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                    <? $var = "form_confirm";?>
                                    	<button class="btn btn-primary" type="submit" id="<? echo $var;?>">登録</button>
                                      <button class="btn btn-white" type="submit">キャンセル</button>
                                    </div>
                                </div>
<!--登録ボタン-->

                            </form>
                        </div><!--ibox-content-->
                    </div><!--ibox float-e-margins-->
                </div><!--col-lg-12-->
            </div><!--row-->
<!--登録フォームここまで-->


        </div>
<!--コンテンツここまで-->


<!--フッター部分 -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
<!--フッター部分 -->

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/masterdashboard/js/inspinia.js"></script>
    <script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>
    <script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Chosen -->
    <script src="/masterdashboard/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="/masterdashboard/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="/masterdashboard/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="/masterdashboard/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="/masterdashboard/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="/masterdashboard/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="/masterdashboard/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="/masterdashboard/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="/masterdashboard/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="/masterdashboard/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="/masterdashboard/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="/masterdashboard/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="/masterdashboard/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="/masterdashboard/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="/masterdashboard/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

<script>
	$(document).ready(function(){

			var $image = $(".image-crop > img")
			$($image).cropper({
					aspectRatio: 1,//トリミング比率サイズ
					preview: ".img-preview",
					done: function(data) {
							// Output the result data for cropping image.
						$('.cropped').val($image.cropper("getDataURL"));
					}
			});

			var $inputImage = $("#inputImage");
			if (window.FileReader) {
					$inputImage.change(function() {
							var fileReader = new FileReader(),
											files = this.files,
											file;

							if (!files.length) {
									return;
							}

							file = files[0];

							if (/^image\/\w+$/.test(file.type)) {
									fileReader.readAsDataURL(file);
									fileReader.onload = function () {
										$('#checkbox_<? echo $member_id;?>').prop("checked",true); 
											$inputImage.val("");
											$image.cropper("reset", true).cropper("replace", this.result);
									};
							} else {
									showMessage("Please choose an image file.");
							}
					});
			} else {
					$inputImage.addClass("hide");
			}


			$("#download").click(function() {
					window.open($image.cropper("getDataURL"));
			});

			$("#zoomIn").click(function() {
					$image.cropper("zoom", 0.1);
			});

			$("#zoomOut").click(function() {
					$image.cropper("zoom", -0.1);
			});

			$("#rotateLeft").click(function() {
					$image.cropper("rotate", 45);
			});

			$("#rotateRight").click(function() {
					$image.cropper("rotate", -45);
			});

			$("#setDrag").click(function() {
					$image.cropper("setDragMode", "crop");
			});

			$('#data_1 .input-group.date').datepicker({
					todayBtn: "linked",
					keyboardNavigation: false,
					forceParse: false,
					calendarWeeks: true,
					autoclose: true
			});

			$('#data_2 .input-group.date').datepicker({
					startView: 1,
					todayBtn: "linked",
					keyboardNavigation: false,
					forceParse: false,
					autoclose: true,
					format: "dd/mm/yyyy"
			});

			$('#data_3 .input-group.date').datepicker({
					startView: 2,
					todayBtn: "linked",
					keyboardNavigation: false,
					forceParse: false,
					autoclose: true
			});

			$('#data_4 .input-group.date').datepicker({
					minViewMode: 1,
					keyboardNavigation: false,
					forceParse: false,
					autoclose: true,
					todayHighlight: true
			});

			$('#data_5 .input-daterange').datepicker({
					keyboardNavigation: false,
					forceParse: false,
					autoclose: true
			});

			var elem = document.querySelector('.js-switch');
			var switchery = new Switchery(elem, { color: '#1AB394' });

			var elem_2 = document.querySelector('.js-switch_2');
			var switchery_2 = new Switchery(elem_2, { color: '#ED5565' });

			var elem_3 = document.querySelector('.js-switch_3');
			var switchery_3 = new Switchery(elem_3, { color: '#1AB394' });

			$('.i-checks').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green'
			});

			$('.demo1').colorpicker();

			var divStyle = $('.back-change')[0].style;
			$('#demo_apidemo').colorpicker({
					color: divStyle.backgroundColor
			}).on('changeColor', function(ev) {
									divStyle.backgroundColor = ev.color.toHex();
							});

			$('.clockpicker').clockpicker();

			$('input[name="daterange"]').daterangepicker();

			$('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

			$('#reportrange').daterangepicker({
					format: 'MM/DD/YYYY',
					startDate: moment().subtract(29, 'days'),
					endDate: moment(),
					minDate: '01/01/2012',
					maxDate: '12/31/2015',
					dateLimit: { days: 60 },
					showDropdowns: true,
					showWeekNumbers: true,
					timePicker: false,
					timePickerIncrement: 1,
					timePicker12Hour: true,
					ranges: {
							'Today': [moment(), moment()],
							'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
							'Last 7 Days': [moment().subtract(6, 'days'), moment()],
							'Last 30 Days': [moment().subtract(29, 'days'), moment()],
							'This Month': [moment().startOf('month'), moment().endOf('month')],
							'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
					},
					opens: 'right',
					drops: 'down',
					buttonClasses: ['btn', 'btn-sm'],
					applyClass: 'btn-primary',
					cancelClass: 'btn-default',
					separator: ' to ',
					locale: {
							applyLabel: 'Submit',
							cancelLabel: 'Cancel',
							fromLabel: 'From',
							toLabel: 'To',
							customRangeLabel: 'Custom',
							daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
							monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
							firstDay: 1
					}
			}, function(start, end, label) {
					console.log(start.toISOString(), end.toISOString(), label);
					$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			});

			$(".select2_demo_1").select2();
			$(".select2_demo_2").select2();
			$(".select2_demo_3").select2({
					placeholder: "Select a state",
					allowClear: true
			});


			$(".touchspin1").TouchSpin({
					buttondown_class: 'btn btn-white',
					buttonup_class: 'btn btn-white'
			});

			$(".touchspin2").TouchSpin({
					min: 0,
					max: 100,
					step: 0.1,
					decimals: 2,
					boostat: 5,
					maxboostedstep: 10,
					postfix: '%',
					buttondown_class: 'btn btn-white',
					buttonup_class: 'btn btn-white'
			});

			$(".touchspin3").TouchSpin({
					verticalbuttons: true,
					buttondown_class: 'btn btn-white',
					buttonup_class: 'btn btn-white'
			});


	});
	var config = {
					'.chosen-select'           : {},
					'.chosen-select-deselect'  : {allow_single_deselect:true},
					'.chosen-select-no-single' : {disable_search_threshold:10},
					'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
					'.chosen-select-width'     : {width:"95%"}
					}
			for (var selector in config) {
					$(selector).chosen(config[selector]);
			}

	$("#ionrange_1").ionRangeSlider({
			min: 0,
			max: 5000,
			type: 'double',
			prefix: "$",
			maxPostfix: "+",
			prettify: false,
			hasGrid: true
	});

	$("#ionrange_2").ionRangeSlider({
			min: 0,
			max: 10,
			type: 'single',
			step: 0.1,
			postfix: " carats",
			prettify: false,
			hasGrid: true
	});

	$("#ionrange_3").ionRangeSlider({
			min: -50,
			max: 50,
			from: 0,
			postfix: "°",
			prettify: false,
			hasGrid: true
	});

	$("#ionrange_4").ionRangeSlider({
			values: [
					"January", "February", "March",
					"April", "May", "June",
					"July", "August", "September",
					"October", "November", "December"
			],
			type: 'single',
			hasGrid: true
	});

	$("#ionrange_5").ionRangeSlider({
			min: 10000,
			max: 100000,
			step: 100,
			postfix: " km",
			from: 55000,
			hideMinMax: true,
			hideFromTo: false
	});

	$(".dial").knob();

	$("#basic_slider").noUiSlider({
			start: 40,
			behaviour: 'tap',
			connect: 'upper',
			range: {
					'min':  20,
					'max':  80
			}
	});

	$("#range_slider").noUiSlider({
			start: [ 40, 60 ],
			behaviour: 'drag',
			connect: true,
			range: {
					'min':  20,
					'max':  80
			}
	});

	$("#drag-fixed").noUiSlider({
			start: [ 40, 60 ],
			behaviour: 'drag-fixed',
			connect: true,
			range: {
					'min':  20,
					'max':  80
			}
	});


</script>

   <!-- コメントカウントダウン -->
    <script src="/masterdashboard/js/plugins/count/bootstrap-maxlength.js"></script>
<script>
$(document).ready(function () {
  $('.demo').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>

</body>

</html>
