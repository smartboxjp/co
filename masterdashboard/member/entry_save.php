<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_catearea = new CommonCateArea();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	foreach($_POST as $key => $value)
	{ 
		//$$key = trim($common_dao->db_string_escape($value));
		$$key = $value;
	}
	//管理者チェック
	$common_connect -> Fn_admin_check();
	

	if($login_email	 == "" || $nickname == "")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
	
	if($member_id=="")
	{
		$arr_data = array();
		$arr_data[] = "login_email";
		
		$arr_where = array();
		$arr_where["login_email"] = $login_email;
		
		$arr_where_not = array();
		$arr_where_not["member_id"] = $member_id;
		
		$db_result = $common_member->Fn_db_member_data_select ($arr_data, $arr_where, $arr_where_not);
		if($db_result)
		{
			if($login_email	 == $db_result[0]["login_email	"])
			{
				$common_connect -> Fn_javascript_back("既に登録されているIDです。");
			}
		}
	}
	
	//大エリア
	$arr_where = array();
	$arr_where["cate_area_s_id"] = $cate_area_s_id;
	
	$db_result = $common_catearea->Fn_cate_area_s_list ($arr_where); 
	$cate_area_l_id = $db_result[0]["cate_area_l_id"];
	
	
	$datetime = date("Y/m/d H:i:s");
	$user_agent = substr(getenv("HTTP_USER_AGENT"), 0, 250);
	
	//array
	$arr_db_field = array("login_email", "login_pw", "nickname");
	$arr_db_field = array_merge($arr_db_field, array("cate_area_l_id", "cate_area_s_id"));
	

	$arr_data = array();
	//基本情報
	if($member_id=="")
	{
		foreach($arr_db_field as $value) {
			$arr_data[$value] = $$value;
		}
		$common_member -> Fn_member_insert($arr_data);
		
		$result = $common_member -> Fn_db_apply_auto();
		$member_id = $result[0]["last_id"];
	}
	else
	{
		$arr_where = array();
		$var = "member_id"; $arr_where[$var] = $$var;
		foreach($arr_db_field as $value) {
			$arr_data[$value] = $$value;
		}
		$common_member -> Fn_member_update($arr_data, $arr_where);
	}

	if($check_img=="1")
	{
		//Folder生成
		$up_file_name = "member_thumbnail.png";
		$save_dir = $global_path.global_member_dir.$member_id."/";
		$fname_new_name[1] = $up_file_name;
		
		//Folder生成
		$common_image -> create_folder ($save_dir);
		//ヘッダに「data:image/png;base64,」が付いているので、それは外す
		$cropped = preg_replace("/data:[^,]+,/i","",$cropped);
		
		//残りのデータはbase64エンコードされているので、デコードする
		$cropped = base64_decode($cropped);
		
		//まだ文字列の状態なので、画像リソース化
		$image = imagecreatefromstring($cropped);
	 
		imagesavealpha($image, TRUE); // 透明色の有効
		imagepng($image ,$save_dir.$fname_new_name[1]);
		
		$common_image -> create_thumbnail($save_dir.$up_file_name, "", $save_dir, 150);
		/*
		$new_end_name="_1";
		$fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $member_id, $text_img_1, "");
		*/
		
		//画像
		$arr_where = array();
		$arr_where["member_id"] = $member_id;
		
		$member_img = $fname_new_name[1];
		$up_date = $datetime;
		
		//array
		$arr_db_field = array("member_img", "up_date");
		
		$arr_data = array();
		foreach($arr_db_field as $value) {
			$arr_data[$value] = $$value;
		}
	
		$common_member->Fn_member_update ($arr_data, $arr_where);

	}
	
	$common_connect-> Fn_javascript_move("ユーザー登録・修正しました", "list.php?member_id=".$member_id);
?>
</body>
</html>