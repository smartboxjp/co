<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_catearea = new CommonCateArea();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	//エリア
	$arr_area = $common_catearea->Fn_cate_area_list($sql);
	if(!is_null($arr_area))
	{
		foreach($arr_area as $arr_key=>$arr_value)
		{
			foreach($arr_value as $key=>$value)
			{
				$arr_cate_area_s_id[$arr_value["cate_area_s_id"]] = $arr_value["cate_area_l_title"]."<br />".$arr_value["cate_area_s_title"];
			}
		}
	}
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_connect->db_string_escape($value);
	}
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>ユーザーデータ|ダッシュボード</title>

    <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/masterdashboard/css/animate.css" rel="stylesheet">
    <link href="/masterdashboard/css/style.css" rel="stylesheet">
    <link href="/masterdashboard/css/plus.css" rel="stylesheet">
  <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
	<script type="text/javascript">
    $(function() {
			//チェックリストを解除する
      $('#check_off').click(function() {
        $('.check_del').prop('checked',false);
      });
			
			
			//チェックリストを一括削除
      $('#check_delete').click(function() {
				if(confirm('削除しますか？'))
				{
					check_dell_list = $('[class="check_del"]:checked').map(function(){
						//$(this)でjQueryオブジェクトが取得できる。val()で値をvalue値を取得。
						return $(this).val();
					}).get().join('&member_id[]=');
					document.location.href = './member_del.php?member_id[]='+check_dell_list;
				}
      });
			
			//ランキングを一括修正
      $('#check_ranking').click(function() {
				if(confirm('ランクを修正しますか？'))
				{
					check_up_list = $('.check_rank option:selected').map(function(){
						//$(this)でjQueryオブジェクトが取得できる。val()で値をvalue値を取得。
						return $(this).val();
					}).get().join('');
					
					document.location.href = './member_rank_up.php?'+check_up_list;
				}
      });
			
			
    });
		
		//有料・無料切り替え
		function fnChangeSel(i, j) { 
			var result = confirm('変更しますか？'); 
			if(result){ 
				document.location.href = './member_up.php?member_id='+i+'&flag_open='+j;
			} 
		}
  </script>
</head>

<body>
<?
	//管理者チェック
	$common_connect -> Fn_admin_check();
?>

    <div id="wrapper">

<!--左ナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
<!--左ナビゲーションここまで -->

	<div id="page-wrapper" class="gray-bg">

<!--コンテンツ　ヘッダーナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
<!--コンテンツ　ヘッダーナビゲーションここまで -->



<!--コンテンツユーザーデータ-->

  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>登録ユーザー</h2>
        <?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/include/header_link.php"; ?>
      </div>

      <div class="col-lg-2">
      </div>
  </div>



  <div class="row"><!---->
      <div class="col-lg-12"><!---->
          <div class="ibox float-e-margins"><!---->

              <div class="ibox-title">
                  <h5>登録ユーザー一覧 </h5>
                  <div class="ibox-tools">
                      <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                      </a>
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-user">
                        <li><button id="check_off" type="button" class="btn btn-default btn-xs">チェックリストを解除する</button>
                        </li>
                        <li><button id="check_delete" type="button" class="btn btn-primary btn-xs">チェックリストを一括削除</button>
                        </li>
                        <li><button id="check_ranking" type="button" class="btn btn-primary btn-xs">ランクを一括修正</button>
                        </li>
                      </ul>
                      <a class="close-link">
                          <i class="fa fa-times"></i>
                      </a>
                  </div><!--ibox-tools-->
              </div><!--ibox-title-->

              <div class="ibox-content"><!--第二ヘッダー-->

                  <div class="row">
<!--第二ヘッダー-->

<!--ソート機能-->
                      <div class="col-sm-8 m-b-xs">
                          <div data-toggle="buttons" class="btn-group">
                              <label class="btn btn-sm btn-white active"> <input type="radio" id="option1" name="options"> 新着順 </label>
                              <label class="btn btn-sm btn-white"> <input type="radio" id="option2" name="options"> 地域順 </label>
                              <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options"> 口コミ数順</label>
<label class="btn btn-sm btn-white"> <input type="radio" id="option2" name="options">平均点数順</label>
                              <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options">最新ログイン順</label>
                          </div>
                      </div>
<!--データワード検索-->
                      <form action="<?php echo $_SERVER['PHP_SELF']?>" method="get" name="form_member_search">
                      <div class="col-sm-4">
                        <div class="input-group">
                          <? $var = "s_keyword";?>
                          <input name="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="ユーザー名又はID検索" class="input-sm form-control"> 
                          <span class="input-group-btn">
                            <button type="submit" class="btn btn-sm btn-primary"> Go!</button> 
                          </span>
                        </div>
                      </div>
                      </form>

                  </div><!--row-->

                  <div class="table-responsive">
<?php
	$arr_active = $common_member ->Fn_member_active();
	
	$view_count=20;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$arr_where = array();
	$var = "s_keyword";
	if($$var != "")
	{
		$arr_where[$var] = $$var;
	}
	
	$var = "s_flag_open";
	if($$var != "")
	{
		$arr_where[$var] = $$var;
	}

	//合計
	$all_count = 0;
	$arr_member_all = $common_member -> Fn_member_all_count ($arr_where);
	$all_count = $arr_member_all[0]["all_count"];
	
	//リスト表示
	$arr_etc["order_name"] = $order_name;
	$arr_etc["order"] = $order;
	$arr_etc["offset"] = $offset;
	$arr_etc["view_count"] = $view_count;
	
	$arr_data = array("member_id", "login_email", "login_pw", "member_login_cookie", "nickname");
	$arr_data = array_merge($arr_data, array("twitter", "facebook", "yahoo", "cate_area_l_id", "cate_area_s_id", "member_img"));
	$arr_data = array_merge($arr_data, array("member_comment", "flag_open", "lastlogin_date", "ranking"));
	$arr_data = array_merge($arr_data, array("regi_date", "up_date"));
	
	$arr_member_list = $common_member -> Fn_member_all_list ($arr_data, $arr_where, $arr_etc);
	
	
$table_header = <<<EOF
			<thead><!--項目名-->
				<tr>
					<th>削除</th>
					<th>管理ID</th>
					<th>登録日</th>
					<th>名前</th>
					<th>エリア</th>
					<th>平均点</th>
					<th>口コミ数</th>
					<th>画像数</th>
					<th>お気に入り</th>
					<th>最終ログイン</th>
					<th>ランク</th>
					<th>アク禁止</th>
				</tr>
			</thead>

EOF;
?>

				<table class="table table-striped table-bordered table-hover dataTables-example" >
        <?php echo $table_header;?>
          <tbody>
                                    
<?

	foreach($arr_member_list as $arr_key=>$arr_value)
	{
		foreach($arr_value as $key=>$value)
		{
			$$key=$value;
		}
?>
          <tr>
            <td><input type="checkbox" class="check_del" value="<? echo $member_id;?>"></td>
            <td><small><? echo $member_id;?></small></td>
            <td><small><? echo substr($regi_date, 0, 10);?></small></td>
            <td><a href="./entry.php?member_id=<? echo $member_id;?>"><? echo $nickname;?></a></td>
            <td><small><? echo $arr_cate_area_s_id[$cate_area_s_id];?></small></td>
            <td><small>3.55点</small></td>
            <td><small>17件</small></td>
            <td><small>9枚</small></td>
            <td><small>9件</small></td>
            <td><small><? if($lastlogin_date!="0000-00-00 00:00:00"){echo substr($lastlogin_date, 0, 10)."<br />".substr($lastlogin_date, 10);}?></small></td>
            <td>
            <select class="from-control check_rank">
            <option value="&member_id[<? echo $member_id;?>]=0">未定</option>
            <? foreach($common_member -> Fn_member_ranking() as $key=>$value) { ?>
            <option value="&member_id[<? echo $member_id;?>]=<? echo $key;?>" <? if($ranking==$key){ echo " selected ";}?>><? echo $value;?>ランク</option>
            <? } ?>
            </select>
            </td>
            <td>	
            		<? /* 1:許可 0:禁止*/ ?>
                <a href="#" onClick='fnChangeSel("<? echo $member_id;?>", "<? if($flag_open=="1") { echo "99";} else { echo "1";}?>");'><? if($flag_open=="1") { echo "<span style='color:#1ab394; font-weight:bold;'>".$arr_active[1]."</span>";} else { echo $arr_active[99];}?></a>
            </td>
          </tr>
<?
			if($db_loop%10==9) { echo $table_header;}

	}
?>
                          
                          
                          </tbody>


                      </table>


	  <div class="nextlist">
             <div class="btn-group">
             <?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>

            </div>
           </div>   

                  </div><!--table-responsive-->

              </div><!--ibox-content-->
          </div><!--ibox float-e-margins-->

      </div><!--col-lg-12-->

  </div><!--row-->





<!--フッター部分 -->
<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
<!--フッター部分 -->

</div>
</div>



    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
    <script src="/masterdashboard/js/bootstrap.min.js"></script>
    <script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Peity -->
    <script src="/masterdashboard/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/masterdashboard/js/inspinia.js"></script>
    <script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>

    <!-- iCheck -->
    <script src="/masterdashboard/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Peity -->
    <script src="/masterdashboard/js/demo/peity-demo.js"></script>

    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>

</body>

</html>
