<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー削除</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	foreach($_GET as $key => $value)
	{ 
		//$$key = trim($common_dao->db_string_escape($value));
		$$key = $value;
	}
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	$member_count = 0;
	foreach($member_id as $value) {
		if($value!="")
		{
			$member_count++;
		}
	}
	
	if($member_count==0)
	{
		$common_connect -> Fn_javascript_back("正しく選択してください。");
	}
	
	//削除処理
	foreach($member_id as $value) {
		if($value!="")
		{
			$arr_where = array();
			$arr_where["member_id"] = $value;
			$db_result = $common_member->Fn_member_delete ($arr_where);
			
			if(file_exists($_SERVER['DOCUMENT_ROOT']."/".global_member_dir.$value))
			{
				$common_connect -> Fn_deldir($_SERVER['DOCUMENT_ROOT']."/".global_member_dir.$value);
			}
		}
	}
	
	$common_connect-> Fn_javascript_move("ユーザーを削除しました", "list.php");
?>
</body>
</html>