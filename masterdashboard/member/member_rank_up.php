<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー変更</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	

	foreach($_GET["member_id"] as $key => $value)
	{ 
		$$key = trim($common_dao->db_string_escape($value));
		//$$key = $value;
		$arr_rank[$key]=$value;
	}
	

	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	if(is_null($arr_rank))
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
	
	foreach($arr_rank as $key=>$value) {	
		$arr_data = array();
		$arr_data["ranking"] = $value;
		
		$arr_where = array();
		$arr_where["member_id"] = $key;
		
		$db_result = $common_member->Fn_member_update ($arr_data, $arr_where);
		
	}

	
	$common_connect-> Fn_javascript_move("修正しました", "list.php");
?>
</body>
</html>