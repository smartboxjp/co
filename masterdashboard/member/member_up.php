<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー変更</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	foreach($_GET as $key => $value)
	{ 
		$$key = trim($common_dao->db_string_escape($value));
		//$$key = $value;
	}
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	if($member_id=="" || $flag_open=="")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}

	$arr_data = array();
	$arr_data["flag_open"] = $flag_open;
	
	$arr_where = array();
	$arr_where["member_id"] = $member_id;
	//30:有料,99:無料
	$db_result = $common_member->Fn_member_update ($arr_data, $arr_where);

	
	$common_connect-> Fn_javascript_move("修正しました", "list.php");
?>
</body>
</html>