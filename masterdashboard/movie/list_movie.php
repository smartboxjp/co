<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連

    foreach($_GET as $key => $value)
    {
        $$key = $common_connect->h($value);
    }
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>動画データ|ダッシュボード</title>

    <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/masterdashboard/css/animate.css" rel="stylesheet">
    <link href="/masterdashboard/css/style.css" rel="stylesheet">
    <link href="/masterdashboard/css/plus.css" rel="stylesheet">
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
    <script type="text/javascript">
    $(function() {
        //チェックリストを解除する
      $('#check_off').click(function() {
        $('.check_del').prop('checked',false);
      });
            
            
            //チェックリストを一括削除
      $('#check_delete').click(function() {
            if(confirm('削除しますか？'))
            {
                check_del_list = $('[class="check_del"]:checked').map(function(){
                    //$(this)でjQueryオブジェクトが取得できる。val()で値をvalue値を取得。
                    return $(this).val();
                }).get().join('&blog_id[]=');
                document.location.href = './blog_del.php?blog_id[]='+check_del_list;
            }
      });
            
    });
        
    //有料・無料切り替え
    function fnChangeSel(i, j) { 
        var result = confirm('変更しますか？'); 
        if(result){ 
            document.location.href = './blog_up.php?blog_id='+i+'&blog_view_level='+j;
        } 
    }
    </script>

</head>

<body>

    <div id="wrapper">

<!--左ナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
<!--左ナビゲーションここまで -->



	<div id="page-wrapper" class="gray-bg">



<!--コンテンツ　ヘッダーナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
<!--コンテンツ　ヘッダーナビゲーションここまで -->


<!--コンテンツユーザーデータ-->

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>登録動画</h2>
					<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/include/header_link.php"; ?>
                </div>

                <div class="col-lg-2">

                </div>
            </div>



            <div class="row"><!---->
                <div class="col-lg-12"><!---->
                    <div class="ibox float-e-margins"><!---->

                        <div class="ibox-title">
                            <h5>動画一覧 </h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#"><button type="button" class="btn btn-default btn-xs">チェックリストを解除する</button></a>
                                    </li>
                                    <li><a href="#"><button type="button" class="btn btn-primary btn-xs">チェックリストを一括削除</button></a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div><!--ibox-tools-->
                        </div><!--ibox-title-->

                        <div class="ibox-content"><!--第二ヘッダー-->

                            <div class="row">
				<!--第二ヘッダー-->

				<!--ソート機能-->
                                <div class="col-sm-8 m-b-xs">
                                    <div data-toggle="buttons" class="btn-group">
                                        <label class="btn btn-sm btn-white active"> <input type="radio" id="option1" name="options"> 新着順 </label>
                                        <label class="btn btn-sm btn-white"> <input type="radio" id="option2" name="options"> 地域順 </label>
                                        <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options">業種順</label>
                                    </div>
                                </div>
				<!--データワード検索-->
                                <div class="col-sm-4">
                                    <div class="input-group"><input type="text" placeholder="店名又はIDで検索" class="input-sm form-control"> <span class="input-group-btn">
                                        <button type="button" class="btn btn-sm btn-primary"> Go!</button> </span></div>
                                </div>

                            </div><!--row-->

                            <div class="table-responsive">
				<table class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead><!--項目名-->
                                    <tr>
                                        <th>削除</th>
                                        <th>管理ID</th>
                                        <th>登録日</th>
                                        <th>容量</th>
                                        <th>投稿店</th>
                                        <th>第一エリア</th>
					<th>業種</th>
                                        <th>動画コメント</th>
                                        <th>編集</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr>
                                        <td><input type="checkbox" class="i-checks" name="input[]"></td>
                                        <td><small>m23725</small></td>
                                        <td><small>2017/03/15</small></td>
					<td><small>4M</small></td>
                                        <td><a href="./shop.php">深夜俳諧専門店あどまっちっく天国</a></td>
                                        <td><small>埼玉県</small></td>
                                        <td><small>デリバリーヘルス</small></td>
					<td><small>今ならこんなに楽しい職場で働けますよ！きっと素敵な仲間にあえるし、たっぷりお金も稼げる！</small></td>
					<td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" class="i-checks" name="input[]"></td>
                                        <td><small>m23725</small></td>
                                        <td><small>2017/03/15</small></td>
					<td><small>4M</small></td>
                                        <td><a href="./shop.php">深夜俳諧専門店あどまっちっく天国</a></td>
                                        <td><small>埼玉県</small></td>
                                        <td><small>デリバリーヘルス</small></td>
					<td><small>今ならこんなに楽しい職場で働けますよ！きっと素敵な仲間にあえるし、たっぷりお金も稼げる！</small></td>
					<td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" class="i-checks" name="input[]"></td>
                                        <td><small>m23725</small></td>
                                        <td><small>2017/03/15</small></td>
					<td><small>4M</small></td>
                                        <td><a href="./shop.php">深夜俳諧専門店あどまっちっく天国</a></td>
                                        <td><small>埼玉県</small></td>
                                        <td><small>デリバリーヘルス</small></td>
					<td><small>今ならこんなに楽しい職場で働けますよ！きっと素敵な仲間にあえるし、たっぷりお金も稼げる！</small></td>
					<td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" class="i-checks" name="input[]"></td>
                                        <td><small>m23725</small></td>
                                        <td><small>2017/03/15</small></td>
					<td><small>4M</small></td>
                                        <td><a href="./shop.php">深夜俳諧専門店あどまっちっく天国</a></td>
                                        <td><small>埼玉県</small></td>
                                        <td><small>デリバリーヘルス</small></td>
					<td><small>今ならこんなに楽しい職場で働けますよ！きっと素敵な仲間にあえるし、たっぷりお金も稼げる！</small></td>
					<td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button></td>
                                    </tr>
                                    
 				     <tr>
                                        <td><input type="checkbox" class="i-checks" name="input[]"></td>
                                        <td><small>m23725</small></td>
                                        <td><small>2017/03/15</small></td>
					<td><small>4M</small></td>
                                        <td><a href="./shop.php">深夜俳諧専門店あどまっちっく天国</a></td>
                                        <td><small>埼玉県</small></td>
                                        <td><small>デリバリーヘルス</small></td>
					<td><small>今ならこんなに楽しい職場で働けますよ！きっと素敵な仲間にあえるし、たっぷりお金も稼げる！</small></td>
					<td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button></td>
                                    </tr>

				     <tr>
                                        <td><input type="checkbox" class="i-checks" name="input[]"></td>
                                        <td><small>m23725</small></td>
                                        <td><small>2017/03/15</small></td>
					<td><small>4M</small></td>
                                        <td><a href="./shop.php">深夜俳諧専門店あどまっちっく天国</a></td>
                                        <td><small>埼玉県</small></td>
                                        <td><small>デリバリーヘルス</small></td>
					<td><small>今ならこんなに楽しい職場で働けますよ！きっと素敵な仲間にあえるし、たっぷりお金も稼げる！</small></td>
					<td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button></td>
                                    </tr>

                                  <tr>
                                        <td><input type="checkbox" class="i-checks" name="input[]"></td>
                                        <td><small>m23725</small></td>
                                        <td><small>2017/03/15</small></td>
					<td><small>4M</small></td>
                                        <td><a href="./shop.php">深夜俳諧専門店あどまっちっく天国</a></td>
                                        <td><small>埼玉県</small></td>
                                        <td><small>デリバリーヘルス</small></td>
					<td><small>今ならこんなに楽しい職場で働けますよ！きっと素敵な仲間にあえるし、たっぷりお金も稼げる！</small></td>
					<td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button></td>
                                    </tr>

                                    <tr>
                                        <td><input type="checkbox" class="i-checks" name="input[]"></td>
                                        <td><small>m23725</small></td>
                                        <td><small>2017/03/15</small></td>
					<td><small>4M</small></td>
                                        <td><a href="./shop.php">深夜俳諧専門店あどまっちっく天国</a></td>
                                        <td><small>埼玉県</small></td>
                                        <td><small>デリバリーヘルス</small></td>
					<td><small>今ならこんなに楽しい職場で働けますよ！きっと素敵な仲間にあえるし、たっぷりお金も稼げる！</small></td>
					<td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button></td>
                                    </tr>

                                     <tr>
                                        <td><input type="checkbox" class="i-checks" name="input[]"></td>
                                        <td><small>m23725</small></td>
                                        <td><small>2017/03/15</small></td>
					<td><small>4M</small></td>
                                        <td><a href="./shop.php">深夜俳諧専門店あどまっちっく天国</a></td>
                                        <td><small>埼玉県</small></td>
                                        <td><small>デリバリーヘルス</small></td>
					<td><small>今ならこんなに楽しい職場で働けますよ！きっと素敵な仲間にあえるし、たっぷりお金も稼げる！</small></td>
					<td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button></td>
                                    </tr>

                                     <tr>
                                        <td><input type="checkbox" class="i-checks" name="input[]"></td>
                                        <td><small>m23725</small></td>
                                        <td><small>2017/03/15</small></td>
					<td><small>4M</small></td>
                                        <td><a href="./shop.php">深夜俳諧専門店あどまっちっく天国</a></td>
                                        <td><small>埼玉県</small></td>
                                        <td><small>デリバリーヘルス</small></td>
					<td><small>今ならこんなに楽しい職場で働けますよ！きっと素敵な仲間にあえるし、たっぷりお金も稼げる！</small></td>
					<td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button></td>
                                    </tr>


                                    <tr>
                                        <th>削除</th>
                                        <th>管理ID</th>
                                        <th>登録日</th>
                                        <th>容量</th>
                                        <th>投稿店</th>
                                        <th>第一エリア</th>
					<th>業種</th>
                                        <th>動画コメント</th>
                                        <th>編集</th>
                                    </tr>

                                     <tr>
                                        <td><input type="checkbox" class="i-checks" name="input[]"></td>
                                        <td><small>m23725</small></td>
                                        <td><small>2017/03/15</small></td>
					<td><small>4M</small></td>
                                        <td><a href="./shop.php">深夜俳諧専門店あどまっちっく天国</a></td>
                                        <td><small>埼玉県</small></td>
                                        <td><small>デリバリーヘルス</small></td>
					<td><small>今ならこんなに楽しい職場で働けますよ！きっと素敵な仲間にあえるし、たっぷりお金も稼げる！</small></td>
					<td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button></td>
                                    </tr>



                                    </tbody>

				
                                </table>


					<div class="nextlist">
                      		         <div class="btn-group">
                               			 <button type="button" class="btn btn-white"><i class="fa fa-chevron-left"></i></button>
                                		 <button class="btn btn-white">1</button>
                                		 <button class="btn btn-white  active">2</button>
                                		 <button class="btn btn-white">3</button>
                                		 <button class="btn btn-white">4</button>
                                		 <button type="button" class="btn btn-white"><i class="fa fa-chevron-right"></i> </button>
                            		</div>
				       </div>                              		    


                            </div><!--table-responsive-->

                        </div><!--ibox-content-->
                    </div><!--ibox float-e-margins-->

                </div><!--col-lg-12-->

            </div><!--row-->






<!--フッター部分 -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
<!--フッター部分 -->

        </div>
        </div>



    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
    <script src="/masterdashboard/js/bootstrap.min.js"></script>
    <script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Peity -->
    <script src="/masterdashboard/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/masterdashboard/js/inspinia.js"></script>
    <script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>

    <!-- iCheck -->
    <script src="/masterdashboard/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Peity -->
    <script src="/masterdashboard/js/demo/peity-demo.js"></script>

    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>

</body>

</html>
