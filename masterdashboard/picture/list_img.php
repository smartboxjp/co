<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
    $common_catearea = new CommonCateArea();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateJob.php";
    $common_catejob = new CommonCateJob();

    //エリア
    $arr_area = $common_catearea->Fn_cate_area_list($sql);
    if(!is_null($arr_area))
    {
        foreach($arr_area as $arr_key=>$arr_value)
        {
            foreach($arr_value as $key=>$value)
            {
                $arr_cate_area_l_title[$arr_value["cate_area_s_id"]] = $arr_value["cate_area_l_title"];
                $arr_cate_area_s_title[$arr_value["cate_area_s_id"]] = $arr_value["cate_area_s_title"];
            }
        }
    }
    
    //職種
    $arr_job = $common_catejob->Fn_db_cate_job_all();
    if(!is_null($arr_job))
    {
        foreach($arr_job as $arr_key=>$arr_value)
        {
            foreach($arr_value as $key=>$value)
            {
                $arr_cate_job_id[$arr_value["cate_job_id"]] = $arr_value["cate_job_title"];
            }
        }
    }

    foreach($_GET as $key => $value)
    {
        $$key = $common_connect->h($value);
    }
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>画像データ|ダッシュボード</title>

    <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/masterdashboard/css/animate.css" rel="stylesheet">
    <link href="/masterdashboard/css/style.css" rel="stylesheet">
    <link href="/masterdashboard/css/plus.css" rel="stylesheet">
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
    <script type="text/javascript">
    $(function() {
        //チェックリストを解除する
      $('#check_off').click(function() {
        $('.check_del').prop('checked',false);
      });
            
            
            //チェックリストを一括削除
      $('#check_delete').click(function() {
            if(confirm('削除しますか？'))
            {
                check_del_list = $('[class="check_del"]:checked').map(function(){
                    //$(this)でjQueryオブジェクトが取得できる。val()で値をvalue値を取得。
                    return $(this).val();
                }).get().join('&voice_img_id[]=');
                document.location.href = './voice_img_del.php?voice_img_id[]='+check_del_list;
            }
      });
            
    });
        
    //有料・無料切り替え
    function fnChangeSel(i, j) { 
        var result = confirm('変更しますか？'); 
        if(result){ 
            document.location.href = './voice_img_up.php?voice_img_id='+i+'&voice_img_view_level='+j;
        } 
    }
    </script>
</head>

<body>

    <div id="wrapper">

<!--左ナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
<!--左ナビゲーションここまで -->

	<div id="page-wrapper" class="gray-bg">


<!--コンテンツ　ヘッダーナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
<!--コンテンツ　ヘッダーナビゲーションここまで -->




<!--コンテンツユーザーデータ-->

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>登録画像</h2>
					<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/include/header_link.php"; ?>
                </div>

                <div class="col-lg-2">

                </div>
            </div>



            <div class="row"><!---->
                <div class="col-lg-12"><!---->
                    <div class="ibox float-e-margins"><!---->

                        <div class="ibox-title">
                            <h5>登録画像一覧 </h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><button type="button" id="check_off" class="btn btn-default btn-xs">チェックリストを解除する</button>
                                    </li>
                                    <li><button type="button" id="check_delete" class="btn btn-primary btn-xs">チェックリストを一括削除</button>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div><!--ibox-tools-->
                        </div><!--ibox-title-->

                        <div class="ibox-content"><!--第二ヘッダー-->

                            <div class="row">
				<!--第二ヘッダー-->

				<!--ソート機能-->
                                <div class="col-sm-8 m-b-xs">
                                    <div data-toggle="buttons" class="btn-group">
                                        <label class="btn btn-sm btn-white active"> <input type="radio" id="option1" name="options"> 新着順 </label>
                                    </div>
                                </div>
				<!--データワード検索-->
                              <form action="<?php echo $_SERVER['PHP_SELF']?>" method="get" name="form_shop_search">
                              <div class="col-sm-4">
                                <div class="input-group">
                                  <? $var = "s_keyword";?>
                                  <input name="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="画像タイトル又はID" class="input-sm form-control"> 
                                  <span class="input-group-btn">
                                    <button type="submit" class="btn btn-sm btn-primary"> Go!</button> 
                                  </span>
                                </div>
                              </div>
                              </form>

                            </div><!--row-->

<?php
$view_count=5;   // List count
$offset=0;

if(!$page)
{
    $page=1;
}
Else
{
    $offset=$view_count*($page-1);
}

$where = "";
if($s_keyword != "")
{
    $where .= " and (voice_img_title like '%".$s_keyword."%' or voice_img_id='".$s_keyword."') ";
}
if($s_flag_open != "")
{
    $where .= " and flag_open='".$s_flag_open."' ";
}

//合計
$sql_count = "SELECT count(voice_img_id) as all_count FROM voice_img where 1 ".$where ;
$db_result_count = $common_dao->db_query_bind($sql_count);
if($db_result_count)
{
    $all_count = $db_result_count[0]["all_count"];
}

//リスト表示
$arr_db_field = array("voice_img_id", "voice_img_title", "voice_img_title", "cate_area_s_id", "cate_job_id");

$sql = "SELECT ";
$sql .= " s.shop_name, s.shop_id, b.flag_open, b.regi_date, b.up_date, b.img_1, b.member_id, ";
foreach($arr_db_field as $val)
{
    $sql .= $val.", ";
}
$sql .= " 1 FROM voice_img b inner join shop s on s.shop_id=b.shop_id where 1 ".$where ;
if($order_name != "")
{
    $sql .= " order by ".$order_name." ".$order;
}
else
{
    $sql .= " order by b.regi_date desc";
}
$sql .= " limit $offset,$view_count";

$db_result = $common_dao->db_query_bind($sql);
?>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead><!--項目名-->
                                    <tr>

                                        <th>削除</th>
										<th>サムネイル</th>
                                        <th>管理ID</th>
                                        <th>登録日</th>
										<th>登録時間</th>
										<th>投稿ユーザー</th>
                                        <th>店名</th>
                                        <th>第一エリア</th>
                                        <th>第二エリア</th>
                                        <th>業種</th>
                                        <th>画像コメント</th>

                                    </tr>
                                    </thead>

                                    <tbody>
<?php
if($db_result)
{
    $inner_count = count($db_result);
    for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
    {
        $shop_id=$db_result[$db_loop]["shop_id"];
        $shop_name=$db_result[$db_loop]["shop_name"];
        $view_level=$db_result[$db_loop]["view_level"];
        $flag_open=$db_result[$db_loop]["flag_open"];
        $regi_date=$db_result[$db_loop]["regi_date"];
        $up_date=$db_result[$db_loop]["up_date"];
        $img_1=$db_result[$db_loop]["img_1"];
        $member_id=$db_result[$db_loop]["member_id"];

        foreach($arr_db_field as $val)
        {
            $$val = $db_result[$db_loop][$val];
        }
?>
                                     <tr>
                                        <td><input type="checkbox" class="check_del" value="<? echo $voice_img_id;?>"></td>
                                        <td>
                                        <? if($img_1!="") { ?>
                                        <img src="/<? echo global_voice_img_dir.$voice_img_id."/".$img_1."?d=".date(his);?>" width="50px" height="50px">
                                        <? } ?>
                                        </td>
                                        <td><small><? echo $voice_img_id;?></small></td>
                                        <td><small><? echo date('Y'.'/'.'m'.'/'.'d', strtotime($regi_date));?></small></td>
                                        <td><small><? echo date('H'.':'.'i', strtotime($regi_date));?></small></td>
<?
        $nickname = "";
        if($member_id!="0")
        {
            $sql_member = " select nickname from member where member_id='".$member_id."' ";
            $db_result_member = $common_dao->db_query_bind($sql_member);
            if($db_result_member)
            {
                $nickname = "<a href=\"/masterdashboard/member/entry.php?member_id=".$member_id."\">".$db_result_member[0]["nickname"]."</a>";
            }
        }
?>
                                        <td><small><? echo $nickname;?></small></td>
                                        
                                        <td><a href="../shop/shop.php?shop_id=<? echo $shop_id;?>"><? echo $common_connect->Fn_shot_string($shop_name, 10, $last_str="…");?></a></td>
                                        <td><small><? echo $arr_cate_area_l_title[$cate_area_s_id];?></small></td>
                                        <td><small><? echo $arr_cate_area_s_title[$cate_area_s_id];?></small></td>
                                        <td><small><? echo $arr_cate_job_id[$cate_job_id];?></small></td>
                                        <td><small><? echo $common_connect->Fn_shot_string($voice_img_title, 10, $last_str="…");?></small></td>
                                    </tr>

<?php
        
    }
}
?>

                                    </tbody>

				
                                </table>


                                <div class="nextlist">
                                    <div class="btn-group">
                                        <?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
                                    </div>
                                </div>                         		    


                            </div><!--table-responsive-->

                        </div><!--ibox-content-->
                    </div><!--ibox float-e-margins-->

                </div><!--col-lg-12-->

            </div><!--row-->




<!--フッター部分 -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
<!--フッター部分 -->




	</div><!--id="page-wrapper" class="gray-bg"-->
        </div><!--wrapper-->



    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
    <script src="/masterdashboard/js/bootstrap.min.js"></script>
    <script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Peity -->
    <script src="/masterdashboard/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/masterdashboard/js/inspinia.js"></script>
    <script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>

    <!-- iCheck -->
    <script src="/masterdashboard/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Peity -->
    <script src="/masterdashboard/js/demo/peity-demo.js"></script>

    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>

</body>

</html>
