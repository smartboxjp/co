<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonVoice.php";
    $common_voice = new CommonVoice();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>画像データ削除</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php
    $voice_count = 0;
    foreach($_GET["voice_img_id"] as $key => $value)
    {
        $voice_count++;
    }
    //管理者チェック
    $common_connect -> Fn_admin_check();
    
    
    if($voice_count==0)
    {
        $common_connect -> Fn_javascript_back("正しく選択してください。");
    }
    
    //削除処理
    foreach($_GET["voice_img_id"] as $value) {
        if($value!="")
        {
            if(file_exists($_SERVER['DOCUMENT_ROOT']."/".global_voice_img_dir.$value))
            {
                $common_connect -> Fn_deldir($_SERVER['DOCUMENT_ROOT']."/".global_voice_img_dir.$value);
            }
            //削除
            $dbup = "delete from voice_img where voice_img_id='".$value."' ";
            $db_result = $common_dao->db_update($dbup);
        }
    }
    
    $common_connect-> Fn_javascript_move("削除しました", "list_img.php");
?>
</body>
</html>