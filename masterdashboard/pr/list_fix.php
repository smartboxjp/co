<?php
    $cate_pr_group = 2;
    
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    
    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCatePr.php";
    $common_catepr = new CommonCatePr();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonPrFix.php";
    $common_pr_fix = new CommonPrFix();
    
    //カテゴリ
    $arr_where = array();
    $arr_where["cate_pr_group"] = $cate_pr_group;
    
    $arr_data = array("cate_pr_id", "cate_pr_title", "pr_size", "pr_position");
    $arr_pr = $common_catepr->Fn_cate_pr_list($arr_data, $arr_where);
    if(!is_null($arr_pr))
    {
        foreach($arr_pr as $arr_key=>$arr_value)
        {
            $arr_cate_pr_title[$arr_value["cate_pr_id"]] = $arr_value["cate_pr_title"];
            $arr_cate_pr_size[$arr_value["cate_pr_id"]] = $arr_value["pr_size"];
            $arr_cate_pr_position[$arr_value["cate_pr_id"]] = $arr_value["pr_position"];
        }
    }
    
    foreach($_GET as $key => $value)
    { 
        $$key = $value;
    }
    
    if($pr_fix_id!="")
    {
        $arr_where = array();
        $arr_where["pr_fix_id"] = $pr_fix_id;
        
        $arr_data = array("pr_fix_id", "pr_fix_link", "cate_pr_id", "img_1", "pr_fix_place", "flag_repeat", "flag_open");
        $arr_data = array_merge($arr_data, array("regi_date", "up_date"));
        
        $db_result_pr_fix = $common_pr_fix -> Fn_pr_fix_list($arr_data, $arr_where);
        foreach($db_result_pr_fix as $arr_pr_fix)
        {
            foreach($arr_pr_fix as $key=>$value)
            {
                $$key = $value;
            }
        }
    }
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | File Upload</title>

    <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="/masterdashboard/css/animate.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/lity/lity.css" rel="stylesheet">
    <link href="/masterdashboard/css/style.css" rel="stylesheet">
    <link href="/masterdashboard/css/plus.css" rel="stylesheet">
  <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
<script type="text/javascript">
    $(function() {
            
        $('#form_confirm').click(function() {
            err_default = "";
            err_check_count = 0;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";
            
            err_check_count += check_input("cate_pr_id");
            //err_check_count += check_input("pr_fix_link");
            
            if(err_check_count!=0)
            {
                alert("入力に不備があります");
                return false;
            }
            else
            {
                //$('#form_confirm').submit();
                $('#form_confirm', "body").submit();
                return true;
            }
            
            
        });
                
        function check_input($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);
            
            if($('#'+$str).val()=="")
            {
                err ="<span class='errorForm'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }

    });
    
//-->
</script>
<script language="javascript"> 
    function fnChangeSel(i) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './pr_fix_del.php?pr_fix_id='+i;
        } 
    }
    
    function fnImgDel(i, j, k) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './img_one_del.php?pr_fix_id='+i+'&img='+j+'&img_name='+k;
        } 
    }
</script>

</head>

<body>

    <div id="wrapper">



        <!--左ナビゲーション -->
        <?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
        <!--左ナビゲーションここまで -->

        <div id="page-wrapper" class="gray-bg">


            <!--コンテンツ　ヘッダーナビゲーション -->
            <?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>

            <!--コンテンツ　ヘッダーナビゲーションここまで -->



            <!--コンテンツここから -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>ホームページ内広告コントロール(PCページ)</h2>
                    <ol class="breadcrumb">

                        <li class="active">
                            <strong>ランダム広告</strong>
                        </li>

                        <li>
                            <a href="/masterdashboard/pr/imgup_fixing.php">固定広告</a>
                        </li>


                        <li>
                            <a href="/masterdashboard/pr/imgup_flag.php">トップページ背景広告</a>
                        </li>

                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeIn">
                <div class="row">

                    <!--第一ブロック-->
                    <div class="col-lg-12">

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>メインページコンテンツセンター(最大5枚までランダム表示)</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                      <i class="fa fa-chevron-up"></i>
                    </a>
                                
                                    <a class="close-link">
                      <i class="fa fa-times"></i>
                    </a>
                                
                                </div>
                            </div>


                            <div class="ibox-content">

                                <!--ここから-->
                                <form class="form-inline" enctype="multipart/form-data" action="./pr_fix_save.php" name="form" method="post">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-stripped">
                                        <tr>
                                            <th>
                                                登録ID
                                            </th>
                                            <td>
                                                <? $var = "pr_fix_id";?>
                                                <?
                                                if(isset($$var)) 
                                                {
                                                ?>
                                                    <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
                                                <?
                                                    echo $$var;
                                                } 
                                                else
                                                 { echo "自動生成";}
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                カテゴリ
                                            </th>
                                            <td>
                                                <? $var = "cate_pr_id";?>
                          <select class="form-control m-b" name="<? echo $var;?>" id="<? echo $var;?>">
                          <? foreach($arr_cate_pr_title as $key=>$value) { ?>
                            <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
                          <? } ?>
                          </select>
                          <label id="err_<?php echo $var;?>"></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                表示されてる画像
                                            </th>
                                            <td>
                          <div class="form-group" style="width:200px;">
                            <div class="input-group">
                              <? $var = "img_1";?>  
                              <input type="file" id="<? echo $var;?>" name="<? echo $var;?>" style="display: none;" accept="image/*">
                              <input type="hidden" name="text_<?=$var;?>" value="<?=$$var;?>">
                              <span class="input-group-btn">
                              <button class="btn btn-default" type="button" onclick="$('#<? echo $var;?>').click();"><i class="glyphicon glyphicon-folder-open"></i></button>
                              </span>
                            
                              <div class="input-group">
                                <input id="dummy_file" type="text" class="form-control" placeholder="select file..." disabled>
                              </div>
                            </div>
                          </div>
                          <br />
                          <?
                                                    if($$var!="") {
                                                        echo "<a href='/".global_pr_fix_dir.$pr_fix_id."/".$$var."' data-lity='data-lity'><img src='/".global_pr_fix_dir.$pr_fix_id."/".$$var."?d=".date(his)."' class='primgrandom'></a>";
                                                    }
                                                    ?>
                          <label id="err_<?php echo $var;?>"></label>
                                            </td>
                                        </tr>
                  
                                        <tr>
                                            <th>
                                                登録されてるリンク先
                                            </th>
                                            <td>
                                            <? $var = "pr_fix_link";?>  
                                            <input id="<? echo $var;?>" name="<? echo $var;?>" value="<? echo $$var;?>" type="text" class="form-control" style="width: 100%;" placeholder="http://www.cossot.com">
                                            <label id="err_<?php echo $var;?>"></label>
                                            </td>
                                        </tr>
                  
                                        <tr>
                                            <th>
                                                位置
                                            </th>
                                            <td>
                                            <? $var = "pr_fix_place";?> 
                                            <?
                                                $arr_pr_fix_place[] = "上";
                                                $arr_pr_fix_place[] = "中";
                                                $arr_pr_fix_place[] = "下";
                                                $arr_pr_fix_place[] = "左";
                                                $arr_pr_fix_place[] = "右";
                                            ?>
                                              <select class="form-control m-b" name="<? echo $var;?>" id="<? echo $var;?>">
                                              <? foreach($arr_pr_fix_place as $key=>$value) { ?>
                                                <option value="<? echo $value;?>" <? if($$var==$value) { echo " selected ";}?>><? echo $value;?></option>
                                              <? } ?>
                                              </select>
                                              <label id="err_<?php echo $var;?>"></label>
                                            </td>
                                        </tr>
                  
                                        <tr>
                                            <th>
                                                リピート
                                            </th>
                                            <td>
                                            <? $var = "flag_repeat";?> 
                                            <?
                                                $arr_flag_repeat[1] = "ON";
                                                $arr_flag_repeat[0] = "OFF";
                                            ?>
                                              <select class="form-control m-b" name="<? echo $var;?>" id="<? echo $var;?>">
                                              <? foreach($arr_flag_repeat as $key=>$value) { ?>
                                                <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
                                              <? } ?>
                                              </select>
                                              <label id="err_<?php echo $var;?>"></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align:center;">
                          <div class="form-group">
                            <? $var = "form_confirm";?>
                            <button class="btn btn-primary" type="submit" id="<? echo $var;?>">決定</button>
                          </div>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                                <script type="text/javascript">
                                    $( function () {
                                        $( '#img_1' ).change( function () {
                                            $( '#dummy_file' ).val( $( this ).val() );
                                        } );
                                    } )
                                </script>

                                </div>

                            </div>
                            <!--ibox-content-->


                        </div>
                        <!--ibox float-e-margins-->
                    </div>
                    <!--col-lg-12-->


                    <? foreach($arr_cate_pr_title as $key=>$value) { ?>
                    <div class="col-lg-12">

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5><? echo $value;?></h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                      <i class="fa fa-chevron-up"></i>
                    </a>
                                
                                    <a class="close-link">
                      <i class="fa fa-times"></i>
                    </a>
                                
                                </div>
                            </div>


                            <div class="ibox-content">
                                <!--ここから-->
                                <div class="table-responsive">
                                    <table class="table table-bordered table-stripped">
                                        <thead>
                                            <tr>

                                                <th>
                                                    登録順番
                                                </th>

                                                <th>
                                                    表示されてる画像
                                                </th>
                                                <th>
                                                    登録されてるリンク先
                                                </th>
                                                <th>
                                                    アップされた画像のサイズ※推奨(468×60)
                                                </th>
                                                <th>
                                                    位置
                                                </th>
                                                <th>
                                                    リピート
                                                </th>
                                                <th>
                                                    削除
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?
        $loop = 1;
        $arr_where = array();
        $arr_where["cate_pr_id"] = $key;
        
        $arr_data = array("pr_fix_id", "pr_fix_link", "cate_pr_id", "img_1", "pr_fix_place", "flag_repeat", "flag_open");
        $arr_data = array_merge($arr_data, array("regi_date", "up_date"));
        
        $db_result_pr_fix = $common_pr_fix -> Fn_pr_fix_list($arr_data, $arr_where);
        foreach($db_result_pr_fix as $arr_pr_fix)
        {
            foreach($arr_pr_fix as $key=>$value)
            {
                $$key = $value;
            }
?>
                                            <tr>
                                                <td>
                                                    <? echo $loop++;?>
                                                </td>
                                                <td>
                          <?
                                                    $var = "img_1";
                                                    if($$var!="") {
                                                        echo "<a href='/".global_pr_fix_dir.$pr_fix_id."/".$$var."' data-lity='data-lity'><img src='/".global_pr_fix_dir.$pr_fix_id."/".$$var."?d=".date(his)."' class='primgrandom'></a>";
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" value="<? echo $pr_fix_link;?>">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled="" placeholder="<? echo $arr_cate_pr_size[$cate_pr_id];?>">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled="" placeholder="<? echo $pr_fix_place;?>">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled="" placeholder="<? echo $arr_flag_repeat[$flag_repeat];?>">
                                                </td>
                                                <td>
                                                    <button class="btn btn-white" title="削除" onClick="fnChangeSel('<?php echo $pr_fix_id;?>')"><i class="fa fa-trash"></i> </button>
                          <a href="?pr_fix_id=<?php echo $pr_fix_id;?>">修正</a>
                                                </td>
                                            </tr>
<?
        }
?>

                                        </tbody>

                                    </table>



                                </div>

                            </div>
                            <!--ibox-content-->


                        </div>
                        <!--ibox float-e-margins-->
                    </div>
                    <!--col-lg-12-->
          <? } ?>

                </div>
                <!--row-->


            </div>


            <!--フッター部分 -->
            <?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
            <!--フッター部分 -->

        </div>
    </div>



    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
    <script src="/masterdashboard/js/bootstrap.min.js"></script>
    <script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/masterdashboard/js/inspinia.js"></script>
    <script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>

    <!-- DROPZONE -->
    <script src="/masterdashboard/js/plugins/dropzone/dropzone.js"></script>

    <!-- lity -->
    <script src="/masterdashboard/js/plugins/lity/lity.js"></script>

    <script>
        $( document ).ready( function () {

            Dropzone.options.myAwesomeDropzone = {

                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 100,
                maxFiles: 100,

                // Dropzone settings
                init: function () {
                    var myDropzone = this;

                    this.element.querySelector( "button[type=submit]" ).addEventListener( "click", function ( e ) {
                        e.preventDefault();
                        e.stopPropagation();
                        myDropzone.processQueue();
                    } );
                    this.on( "sendingmultiple", function () {} );
                    this.on( "successmultiple", function ( files, response ) {} );
                    this.on( "errormultiple", function ( files, response ) {} );
                }

            }

        } );
    </script>

</body>

</html>