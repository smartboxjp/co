<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonPr.php";
	$common_pr = new CommonPr();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>削除</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	

	foreach($_GET as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	//DBへ保存
	if ($pr_id != "")
	{
		$arr_where = array();
		$arr_where["pr_id"] = $value;
		$db_result = $common_pr->Fn_pr_delete ($arr_where);
		
		if(file_exists($_SERVER['DOCUMENT_ROOT']."/".global_pr_dir.$value))
		{
			$common_connect -> Fn_deldir($_SERVER['DOCUMENT_ROOT']."/".global_pr_dir.$value);
		}
	}
	
	$common_connect -> Fn_javascript_move("削除しました。", "list.php");
?>
</body>
</html>