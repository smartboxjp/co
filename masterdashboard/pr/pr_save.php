<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonPr.php";
	$common_pr = new CommonPr();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	

	foreach($_POST as $key => $value)
	{ 
		$$key = $value;
		//$$key = $common_connect->h($value);
	}
	//管理者チェック
	$common_connect -> Fn_admin_check();
	

	if($pr_link == "" || $cate_pr_id == "")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
	
	
	$datetime = date("Y/m/d H:i:s");
	$user_agent = substr(getenv("HTTP_USER_AGENT"), 0, 250);
	$flag_open = 1;
	
	//array
	$arr_data = array();
	$arr_db_field = array("pr_link", "cate_pr_id");
	$arr_db_field = array_merge($arr_db_field, array("flag_open"));
	
	$arr_data = array();
	//基本情報
	if($pr_id=="")
	{
		foreach($arr_db_field as $value) {
			$arr_data[$value] = $$value;
		}
		$common_pr -> Fn_pr_insert($arr_data);
		
		$result = $common_pr -> Fn_db_apply_auto();
		$pr_id = $result[0]["last_id"];
	}
	else
	{
		$arr_where = array();
		$var = "pr_id"; $arr_where[$var] = $$var;
		foreach($arr_db_field as $value) {
			$arr_data[$value] = $$value;
		}
		$common_pr -> Fn_pr_update($arr_data, $arr_where);
	}
	
	//Folder生成
	$save_dir = $global_path.global_pr_dir.$pr_id."/";
	
	//Folder生成
	$common_image -> create_folder ($save_dir);
	
	$new_end_name="_1";
	$fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $pr_id, $text_img_1, "");
	
	
	$arr_where = array();
	$arr_data = array();
	$var = "pr_id"; $arr_where[$var] = $$var;
	$arr_data["img_1"] = $fname_new_name[1];
	$common_pr -> Fn_pr_update($arr_data, $arr_where);
	
	$common_connect-> Fn_javascript_move("登録・修正しました", "list.php?pr_id=".$pr_id);
?>
</body>
</html>