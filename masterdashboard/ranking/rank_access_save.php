<?php
    //error_reporting(0);
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php   

    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
    //管理者チェック
    $common_connect -> Fn_admin_check();
    

    if($regi_date == "" || $shop_id == "" || $ranking == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力してください。");
    }

    $sql = "SELECT shop_id FROM shop where shop_id='".$shop_id."' ";
    $db_result = $common_dao->db_query_bind($sql);
    if(!$db_result)
    {
        $common_connect -> Fn_javascript_back("登録されてないIDです。");
    }

    $db_del = "Delete from shop_access_ranking where shop_id='".$shop_id."' and regi_date='".$regi_date."' ";
    $common_dao->db_update($db_del);

    $db_del = "Delete from shop_access_ranking where ranking='".$ranking."' and regi_date='".$regi_date."' ";
    $common_dao->db_update($db_del);

    $db_insert = "insert into shop_access_ranking ( shop_id, regi_date, ranking ) values ";
    $db_insert .= " ( '".$shop_id."', '".$regi_date."', '".$ranking."') ";
    $common_dao->db_update($db_insert);
    
    $common_connect-> Fn_javascript_move("登録・修正しました", "rank_access.php");
?>
</body>
</html>