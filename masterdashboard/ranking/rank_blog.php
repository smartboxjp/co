<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連

    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMasterboard.php";
    $Common_masterboard = new CommonMasterboard();

    foreach($_GET as $key => $value)
    {
        $$key = $common_connect->h($value);
    }
    
    //エリア
    $sql = " select s.cate_area_s_id, s.cate_area_l_id, cate_area_l_title, cate_area_s_title FROM cate_area_l l inner join cate_area_s s on l.cate_area_l_id=s.cate_area_l_id order by s.view_level" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_area[$db_result[$db_loop]["cate_area_s_id"]] = $db_result[$db_loop]["cate_area_l_title"].":".$db_result[$db_loop]["cate_area_s_title"];
        }
    }
  
    //職種
    $sql = " select cate_job_id, cate_job_title FROM cate_job order by view_level" ; 
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_job[$db_result[$db_loop]["cate_job_id"]] = $db_result[$db_loop]["cate_job_title"];
        }
    }
?>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>ブログランキング編集 | ダッシュボード</title>

	<link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
	<link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="/masterdashboard/css/animate.css" rel="stylesheet">
	<link href="/masterdashboard/css/style.css" rel="stylesheet">
	<link href="/masterdashboard/css/plus.css" rel="stylesheet">
	<link href="/masterdashboard/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
    <script type="text/javascript">
        $( function () {

            $( '#form_confirm' ).click( function () {
                err_default = "";
                err_check_count = 0;
                bgcolor_default = "#FFFFFF";
                bgcolor_err = "#FFCCCC";
                background = "background-color";

                err_check_count += check_input( "regi_date" );
                err_check_count += check_input( "shop_blog_id" );

                if ( err_check_count != 0 ) {
                    alert( "入力に不備があります" );
                    return false;
                } else {
                    $( '#form_confirm' ).submit();
                    //$( '#form_confirm', "body" ).submit();
                    return true;
                }
            } );

            function check_input( $str ) {
                $( "#err_" + $str ).html( err_default );
                $( "#" + $str ).css( background, bgcolor_default );

                if ( $( '#' + $str ).val() == "" ) {
                    err = "<span class='errorForm'>正しく入力してください。</span>";
                    $( "#err_" + $str ).html( err );
                    $( "#" + $str ).css( background, bgcolor_err );

                    return 1;
                }
                return 0;
            }

        } );

            
        function fnChangeSel(i, j, k) { 
            var result = confirm('削除しますか？'); 
            if(result){ 
                document.location.href = './rank_blog_del.php?shop_blog_id='+i+'&regi_date='+j+'&ranking='+k;
            } 
        }    
        //-->
    </script>

</head>

<body>

	<div id="wrapper">

		<!--左ナビゲーション -->
		<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
		<!--左ナビゲーションここまで -->



		<div id="page-wrapper" class="gray-bg">



			<!--コンテンツ　ヘッダーナビゲーション -->
			<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
			<!--コンテンツ　ヘッダーナビゲーションここまで -->

			<!--ランキング登録-->
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">
					<h2>ブログランキング</h2>
					<ol class="breadcrumb">
						<li>
							<a href="/masterdashboard/ranking/rank_point.php">平均評価ランキング</a>
						</li>
						<li>
							<a href="/masterdashboard/ranking/rank_access.php">アクセスランキング</a>
						</li>
						<li>
							<strong>ブログランキング</strong>
						</li>
						<li>
							<a href="/masterdashboard/ranking/rank_user.php">ピックアップユーザー</a>
						</li>
					</ol>
				</div>
				<div class="col-lg-2">

				</div>
			</div>

			<div class="wrapper wrapper-content animated fadeInRight">
            <form action="./rank_blog_save.php" method="POST" name="form_write" id="form_regist" class="form-horizontal">

				<div class="row">
					<div class="col-md-4">
						<div class="ibox">
							<div class="ibox-title">
								<label class="control-label">更新日※ランキングを更新した日を表示させて下さい。</label>
							</div>

							<div class="ibox-content">

								<div class="table-responsive">
									<table class="table shoping-cart-table">

										<tbody>
											<tr>
												<td>
													<div class="input-group date">
                                                        <? $var = "regi_date"; ?>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" class="form-control">
                                                        <label id="err_<?php echo $var;?>"></label>
													</div>
												</td>

											</tr>
										</tbody>

									</table>
								</div>

							</div>


						</div>
					</div>

					<div class="col-md-8">
						<div class="ibox">
							<div class="ibox-title">
								<label class="control-label">登録させたいブログのIDと順位を入力して下さい。
									<strong><a href="/masterdashboard/blog/list_blog.php">(<i class="fa fa-share"></i>ブログ一覧へ)</a>
								</label>
							</div>

							<div class="ibox-content">

								<div class="table-responsive">
									<table class="table shoping-cart-table">

										<tbody>
											<tr>

												<td>
                                                    <? $var = "ranking"; ?>
													<select class="form-control" name="<? echo $var;?>" id="<? echo $var;?>" >
                                                    <? for($loop=1 ; $loop<=10 ; $loop++) { ?>
														<option value="<? echo $loop;?>" <? if($loop==$var){ echo " selected";}?>><? echo $loop;?>位</option>
                                                    <? } ?>
													</select>

												</td>

												<td>
                                                    <? $var = "shop_blog_id"; ?>

													<input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>"class="form-control" placeholder="ブログID入力">
                                                    <label id="err_<?php echo $var;?>"></label>

												</td>

												<td>
													<div class="entbottom"><button class="btn btn-primary" name="form_confirm" id="form_confirm" type="submit">ランキングに入力</button>
													</div>
												</td>
											</tr>
										</tbody>

									</table>
								</div>

							</div>


						</div>
					</div>


				</div>
            </form>


            <form action="./rank_blog.php" method="GET" name="form_write" id="form_regist" class="form-horizontal">
                <div>
                    <div class="ibox">
                        <div class="ibox-title">
                            <label class="control-label">検索する日を入れて検索して下さい。</label>
                        </div>

                        <div class="ibox-content">

                            <div class="table-responsive">
                                <table class="table shoping-cart-table">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="input-group date">
                                                    <? $var = "s_regi_date"; ?>
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? if($$var=="") { echo date("Y/m/d");} else {echo $$var;}?>" class="form-control">
                                                    <label id="err_<?php echo $var;?>"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="entbottom"><button class="btn btn-primary" name="form_confirm" id="form_confirm" type="submit">日付で検索する</button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>

                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </form>



<?php
    $where = " ";
    if($s_regi_date=="")
    {
        $s_regi_date = date("Ymd");
    }
    $s_regi_date = date("Y/m/d", strtotime($s_regi_date));
    $where .= " and b.shop_id in (select shop_id from shop where flag_open=1 and shop_id=b.shop_id ) and r.regi_date='".$s_regi_date."' ";

    
    //合計
	$sql_count = "SELECT count(b.shop_blog_id) as all_count FROM shop_blog b inner join shop_blog_ranking r on b.shop_blog_id=r.shop_blog_id where 1 ".$where ;
    
    $db_result_count = $common_dao->db_query_bind($sql_count);
    if($db_result_count)
    {
        $all_count = $db_result_count[0]["all_count"];
    }
    
    //リスト表示
	$arr_db_field = array("shop_blog_title", "ranking");

	$sql = "SELECT ";
	$sql .= " b.shop_id, b.shop_blog_id, b.regi_date, ";
	foreach($arr_db_field as $val)
	{
	    $sql .= $val.", ";
	}
	$sql .= " 1 FROM shop_blog b inner join shop_blog_ranking r on b.shop_blog_id=r.shop_blog_id where 1 ".$where ;
	$sql .= " order by ranking";
?>
				<div class="row">
					<div class="col-md-9">

						<div class="ibox-title">
							<span class="pull-right">(<strong>全<? echo $all_count;?>店舗</strong>)</span>
							<h5>ブログをよく読み、ユーザーにとって有意義なものをランキングに入れてください。※更新日は毎週月曜日11時までに</h5>
						</div>
						<!--ランキングデータ部分-->
<?php
    $arr_list = $common_dao->db_query_bind($sql);
    if($arr_list)
    {
        for($db_loop=0 ; $db_loop < count($arr_list) ; $db_loop++)
        {
          foreach($arr_db_field as $val)
          {
            $$val = $arr_list[$db_loop][$val];
          }
          $shop_blog_id = $arr_list[$db_loop]["shop_blog_id"];
          $shop_id = $arr_list[$db_loop]["shop_id"];
          $regi_date = $arr_list[$db_loop]["regi_date"];


          $sql_shop = "select shop_name, cate_area_s_id, cate_job_id from shop where shop_id='".$shop_id."' ";
          $arr_shop = $common_dao->db_query_bind($sql_shop);
          $shop_name = $arr_shop[0]["shop_name"];
          $cate_area_s_id = $arr_shop[0]["cate_area_s_id"];
          $cate_job_id = $arr_shop[0]["cate_job_id"];

?>
						<div class="vote-item">
							<div class="row">
								<div class="col-sm-8">
									<div class="vote-actions">
										<a href="/kanto/shop/blog_details.php?shop_blog_id=<? echo $shop_blog_id;?>">
									<i class="fa fa-chevron-up"> </i>
									</a>
										<div><? echo $ranking;?>位</div>
										<a href="/kanto/shop/blog_details.php?shop_blog_id=<? echo $shop_blog_id;?>">
									<i class="fa fa-chevron-down"> </i>
                                        				</a>
									</div>

									<p class="blogrankshop">
										<a href="/kanto/shop/blog_details.php?shop_blog_id=<? echo $shop_blog_id;?>" class="text-navy"><? echo $shop_name;?></a>
									</p>

									<a href="/kanto/shop/blog_details.php?shop_blog_id=<? echo $shop_blog_id;?>" class="vote-title"><? echo $shop_blog_title;?></a>
								



									<div class="vote-info">
										<i class="fa fa-star-o"></i>ID：<? echo $shop_id;?>｜
										<i class="fa fa-globe"></i>エリア:<? echo $arr_cate_area[$cate_area_s_id];?>｜
										<i class="fa fa-female"></i>業種:<? echo $arr_cate_job[$cate_job_id];?>
									</div>
								</div>

								<div class="col-sm-3">
									<span class="blogrank-time"><? echo $regi_date;?>投稿</span>
								


								</div>

								<div class="col-sm-1">
									<a href="#" class="text-muted" title="削除" onClick="fnChangeSel('<?php echo $shop_blog_id;?>', '<?php echo $s_regi_date;?>', '<? echo $ranking;?>')"><i class="fa fa-trash"></i></a>
								</div>


							</div>
						</div>
<?
		}
	}
?>



						<!--ランキングデータ部分-->

					</div>
					<!--col-lg-9-->




					<!--データ部分-->
					<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/right_deta.php"; ?>
					<!--データ部分-->
				</div>
				<!--row-->

			</div>
			<!--wrapper wrapper-content animated fadeInRight-->


			<!--フッター部分 -->
			<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
			<!--フッター部分 -->

		</div>
	</div>



	<!-- Mainly scripts -->
	<script src="/masterdashboard/js/bootstrap.min.js"></script>
	<script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

	<!-- Custom and plugin javascript -->
	<script src="/masterdashboard/js/inspinia.js"></script>
	<script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>

	<!-- Data picker -->
	<script src="/masterdashboard/js/plugins/datapicker/bootstrap-datepicker.js"></script>
	<script src="/masterdashboard/js/plugins/datapicker/datepicker-ja.min.js"></script>

	<script>
		$( '#regi_date' ).datepicker( {
			format: 'yyyy/mm/dd',
			language: 'ja',
			autoclose: true,
			clearBtn: true,
		} );
		$( '#regi_date' ).datepicker().datepicker( 'setDate', 'today' );
	</script>

</body>

</html>