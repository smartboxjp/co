<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連

    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMasterboard.php";
    $Common_masterboard = new CommonMasterboard();

    foreach($_GET as $key => $value)
    {
        $$key = $common_connect->h($value);
    }
    
    //エリア
    $sql = " select s.cate_area_s_id, s.cate_area_l_id, cate_area_l_title, cate_area_s_title FROM cate_area_l l inner join cate_area_s s on l.cate_area_l_id=s.cate_area_l_id order by s.view_level" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_area[$db_result[$db_loop]["cate_area_s_id"]] = $db_result[$db_loop]["cate_area_l_title"].":".$db_result[$db_loop]["cate_area_s_title"];
        }
    }
  
    //職種
    $sql = " select cate_job_id, cate_job_title FROM cate_job order by view_level" ; 
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_job[$db_result[$db_loop]["cate_job_id"]] = $db_result[$db_loop]["cate_job_title"];
        }
    }
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>評価ランキング編集 | ダッシュボード</title>

    <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/masterdashboard/css/animate.css" rel="stylesheet">
    <link href="/masterdashboard/css/style.css" rel="stylesheet">
    <link href="/masterdashboard/css/plus.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
    <script type="text/javascript">
        $( function () {

            $( '#form_confirm' ).click( function () {
                err_default = "";
                err_check_count = 0;
                bgcolor_default = "#FFFFFF";
                bgcolor_err = "#FFCCCC";
                background = "background-color";

                err_check_count += check_input( "regi_date" );
                err_check_count += check_input( "shop_id" );

                if ( err_check_count != 0 ) {
                    alert( "入力に不備があります" );
                    return false;
                } else {
                    $( '#form_confirm' ).submit();
                    //$( '#form_confirm', "body" ).submit();
                    return true;
                }
            } );

            function check_input( $str ) {
                $( "#err_" + $str ).html( err_default );
                $( "#" + $str ).css( background, bgcolor_default );

                if ( $( '#' + $str ).val() == "" ) {
                    err = "<span class='errorForm'>正しく入力してください。</span>";
                    $( "#err_" + $str ).html( err );
                    $( "#" + $str ).css( background, bgcolor_err );

                    return 1;
                }
                return 0;
            }

        } );

            
        function fnChangeSel(i, j, k) { 
            var result = confirm('削除しますか？'); 
            if(result){ 
                document.location.href = './rank_point_del.php?shop_id='+i+'&regi_date='+j+'&ranking='+k;
            } 
        }    
        //-->
    </script>

</head>

<body>


<div id="wrapper">

<!--左ナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
<!--左ナビゲーションここまで -->



	<div id="page-wrapper" class="gray-bg">



<!--コンテンツ　ヘッダーナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
<!--コンテンツ　ヘッダーナビゲーションここまで -->

<!--ランキング登録-->
		<div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-lg-10">
				<h2>平均評価ランキング</h2>
				<ol class="breadcrumb">
					<li>
						<strong>平均評価ランキング</strong>
					</li>
					<li>
						<a href="/masterdashboard/ranking/rank_access.php">アクセスランキング</a>
					</li>
					<li>
						<a href="/masterdashboard/ranking/rank_blog.php">ブログランキング</a>
					</li>
					<li>
						<a href="/masterdashboard/ranking/rank_user.php">ピックアップユーザー</a>
					</li>
				</ol>
			</div>
			<div class="col-lg-2">

			</div>
		</div>


		<div class="wrapper wrapper-content animated fadeInRight">
            <form action="./rank_point_save.php" method="POST" name="form_write" id="form_regist" class="form-horizontal">

				<div class="row">
					<div class="col-md-4">
						<div class="ibox">
							<div class="ibox-title">
								<label class="control-label">更新日※ランキングを更新した日を表示させて下さい。</label>
							</div>

							<div class="ibox-content">

								<div class="table-responsive">
									<table class="table shoping-cart-table">
										<tbody>
											<tr>
												<td>
													<div class="input-group date">
                                                        <? $var = "regi_date"; ?>
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" class="form-control">
                                                        <label id="err_<?php echo $var;?>"></label>
													</div>
												</td>
											</tr>
										</tbody>

									</table>
								</div>

							</div>


						</div>
					</div>

					<div class="col-md-8">
						<div class="ibox">
							<div class="ibox-title">
								<label class="control-label">登録させたいお店のIDと順位を入力して下さい。
									<strong><a href="/masterdashboard/shop/list.php">(<i class="fa fa-share"></i>お店一覧へ)</a>
								</label>
							</div>

							<div class="ibox-content">

								<div class="table-responsive">
									<table class="table shoping-cart-table">
										<tbody>
											<tr>
												<td>
                                                    <? $var = "ranking"; ?>
													<select class="form-control" name="<? echo $var;?>" id="<? echo $var;?>" >
                                                    <? for($loop=1 ; $loop<=10 ; $loop++) { ?>
														<option value="<? echo $loop;?>" <? if($loop==$var){ echo " selected";}?>><? echo $loop;?>位</option>
                                                    <? } ?>
													</select>

												</td>

												<td>
                                                    <? $var = "shop_id"; ?>

													<input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>"class="form-control" placeholder="お店ID入力">
                                                    <label id="err_<?php echo $var;?>"></label>

												</td>

												<td>
													<div class="entbottom"><button class="btn btn-primary" name="form_confirm" id="form_confirm" type="submit">ランキングに入力</button>
													</div>
												</td>
											</tr>
										</tbody>

									</table>
								</div>

							</div>


						</div>
					</div>


				</div>
            </form>



            <form action="./rank_point.php" method="GET" name="form_write" id="form_regist" class="form-horizontal">
                <div>
                    <div class="ibox">
                        <div class="ibox-title">
                            <label class="control-label">検索する日を入れて検索して下さい。</label>
                        </div>

                        <div class="ibox-content">

                            <div class="table-responsive">
                                <table class="table shoping-cart-table">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="input-group date">
                                                    <? $var = "s_regi_date"; ?>
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? if($$var=="") { echo date("Y/m/d");} else {echo $$var;}?>" class="form-control">
                                                    <label id="err_<?php echo $var;?>"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="entbottom"><button class="btn btn-primary" name="form_confirm" id="form_confirm" type="submit">日付で検索する</button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>

                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </form>

<!--ランキングデータ部分-->
<?php
    $where = " ";
    if($s_regi_date=="")
    {
        $s_regi_date = date("Ymd");
    }
    $s_regi_date = date("Y/m/d", strtotime($s_regi_date));
    $where .= " and r.regi_date='".$s_regi_date."' ";

    
    //合計
    $sql_count = "SELECT count(r.shop_id) as all_count FROM shop_ranking r inner join shop s on r.shop_id=s.shop_id where s.flag_open=1 ".$where ;
    
    $db_result_count = $common_dao->db_query_bind($sql_count);
    if($db_result_count)
    {
        $all_count = $db_result_count[0]["all_count"];
    }
    
    //リスト表示
    $arr_db_field = array( "shop_name", "cate_area_s_id", "cate_job_id", "shop_title", "img_1", "shop_thumbnail", "ranking" );
    $arr_db_field = array_merge( $arr_db_field, array( "s.shop_id" ) );
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM shop_ranking r inner join shop s on r.shop_id=s.shop_id ";
    $sql .= " where s.flag_open=1 ".$where ;
    $sql .= " order by r.ranking";
?>
			<div class="row">
				<div class="col-md-9">

					<div class="ibox">
						<div class="ibox-title">
							<span class="pull-right">(<strong>全<? echo $all_count;?>店舗</strong>)</span>
							<h5>平均点数を元にランキングを更新してください。※更新日は毎週月曜日11時までに</h5>
						</div>
<?php
    $arr_list = $common_dao->db_query_bind($sql);
    if($arr_list)
    {
        for($db_loop=0 ; $db_loop < count($arr_list) ; $db_loop++)
        {
          foreach($arr_db_field as $val)
          {
            $$val = $arr_list[$db_loop][$val];
          }
          $shop_id = $arr_list[$db_loop]["shop_id"];　
?>
                        <div class="ibox-content">
                        <div class="table-responsive">
                        <table class="table shoping-cart-table">

                        <tbody>
                        <tr>
                        <td rowspan="2">
                        	<div class="vote-actions">
                        		<p><? echo $ranking;?>位</p>
                        	</div>	
                        </td>

                        <td width="90">
                        <div class="cart-product-imitation">
                        <a href="/kanto/shop/?shop_id=<? echo $shop_id;?>" target="_blank">
                        <?
                        if($shop_thumbnail!="")
                        {
                            echo "<img src='/".global_shop_dir.$shop_id."/".$shop_thumbnail."' width='60'>";
                        }
                        ?>
                        </a>
                        </div>
                        </td>

                        <td class="desc">
                        <h3>
                        <a href="/kanto/shop/?shop_id=<? echo $shop_id;?>" class="text-navy">
                        <? echo $shop_name;?>
                        </a>
                        </h3>

                        <dl class="small m-b-none">
                        <dt>エリア/業種</dt>
                        <dd><? echo $arr_cate_area[$cate_area_s_id];?>/<? echo $arr_cate_job[$cate_job_id];?></dd>
                        </dl>

                        </td>

                        <td>
                        <h4>
                        <?
                        $result_voice = 0;
                        $sql_voice = "SELECT count(voice_id) as count_voice FROM voice where shop_id='".$shop_id."' and flag_open='1' " ;
                        $db_result_voice = $common_dao->db_query_bind($sql_voice);
                        if($db_result_voice) { $result_voice = $db_result_voice[0]["count_voice"]; }
                        ?>
                        <a href="/kanto/shop/?shop_id=<? echo $shop_id;?>"><i class="fa fa-comment-o"></i> <? echo $result_voice;?>件</a>
                        </h4>
                        </td>

                        <td>
                        <h4>
                        【<? if($evaluation==0){ echo "-.--";} else { echo sprintf('%.2f',$evaluation);}?>点】
                        </h4>
                        </td>

                        </tr>

                        <tr>

                        <td colspan="3">
                            <?
                            $yyyymmdd_0 = date("Y/m/d");
                            $yyyymmdd_1 = date("Y/m/d", strtotime("-1 day"));
                            $yyyymmdd_2 = date("Y/m/d", strtotime("-2 day"));

                            $arr_pv[0] = "-";
                            $arr_pv[1] = "-";
                            $arr_pv[2] = "-";
                            $sql_pv = "SELECT p_".date("d", strtotime($yyyymmdd_0))." as pv FROM shop_pv where shop_id='".$shop_id."' and yyyymm='".date("Ym", strtotime($yyyymmdd_0))."' " ;
                            $db_result_pv = $common_dao->db_query_bind($sql_pv);
                            if($db_result_pv) { $arr_pv[0] = $db_result_pv[0]["pv"]; }

                            $sql_pv = "SELECT p_".date("d", strtotime($yyyymmdd_1))." as pv FROM shop_pv where shop_id='".$shop_id."' and yyyymm='".date("Ym", strtotime($yyyymmdd_1))."' " ;
                            $db_result_pv = $common_dao->db_query_bind($sql_pv);
                            if($db_result_pv) { $arr_pv[1] = $db_result_pv[0]["pv"]; }

                            $sql_pv = "SELECT p_".date("d", strtotime($yyyymmdd_2))." as pv FROM shop_pv where shop_id='".$shop_id."' and yyyymm='".date("Ym", strtotime($yyyymmdd_2))."' " ;
                            $db_result_pv = $common_dao->db_query_bind($sql_pv);
                            if($db_result_pv) { $arr_pv[2] = $db_result_pv[0]["pv"]; }

                            ?>
                            <span class="text-muted"><i class="fa fa-star-o"></i>ID：<? echo $shop_id;?></span>
                            |
                            <span class="text-muted"><i class="fa fa-area-chart"></i> 今日のアクセス(<? echo $arr_pv[0];?>)</span>
                            |
                            <span class="text-muted"><i class="fa fa-area-chart"></i> 昨日のアクセス(<? echo $arr_pv[1];?>)</span>
                            |
                            <span class="text-muted"><i class="fa fa-area-chart"></i> 一昨日のアクセス(<? echo $arr_pv[2];?>)</span>
                            |
                            <?
                            $result_ranking = "-";
                            $sql_ranking = "SELECT ranking FROM shop_ranking where shop_id='".$shop_id."' and regi_date<'".$s_regi_date."' order by regi_date desc limit 0, 1 " ;
                            
                            $db_result_ranking = $common_dao->db_query_bind($sql_ranking);
                            if($db_result_ranking)
                            {
                                $result_ranking = $db_result_ranking[0]["ranking"];
                            }
                            ?>
                            <span class="text-muted"><i class="fa fa-gift"></i> 前回ランク:<? echo $result_ranking;?>位</span>
                        </td>


                        <td>
                        <div class="vote-iconsmall">
                        <a href="#" class="text-muted" title="削除" onClick="fnChangeSel('<?php echo $shop_id;?>', '<?php echo $s_regi_date;?>', '<? echo $ranking;?>')"><i class="fa fa-trash"></i></a>
                        </div>
                        </td>
                        </tr>

                        </tbody>
                        </table>
                        </div>

                        </div>
<?
        }
    }
?>


					</div><!--ibox-->



				</div><!--col-md-9-->





<!--データ部分-->
				<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/right_deta.php"; ?>
<!--データ部分-->
			</div><!--low-->

		</div><!--wrapper wrapper-content animated fadeInRight-->

<!--フッター部分 -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
<!--フッター部分 -->

        </div>
        </div>



    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/bootstrap.min.js"></script>
    <script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/masterdashboard/js/inspinia.js"></script>
    <script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>
    <!-- Data picker -->
    <script src="/masterdashboard/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <!-- FooTable -->
    <script src="/masterdashboard/js/plugins/footable/footable.all.min.js"></script>

    <!-- Data picker -->
    <script src="/masterdashboard/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="/masterdashboard/js/plugins/datapicker/datepicker-ja.min.js"></script>

    <script>

$('#regi_date').datepicker({
	format: 'yyyy/mm/dd',
	language: 'ja',
	autoclose: true,
	clearBtn: true,
});
$('#regi_date').datepicker().datepicker('setDate','today');


$('#s_regi_date').datepicker({
    format: 'yyyy/mm/dd',
    language: 'ja',
    autoclose: true,
    clearBtn: true,
});
    </script>



</body>

</html>
