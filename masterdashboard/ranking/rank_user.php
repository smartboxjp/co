<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連

    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMasterboard.php";
    $Common_masterboard = new CommonMasterboard();

    foreach($_GET as $key => $value)
    {
        $$key = $common_connect->h($value);
    }
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>評価ランキング編集 | ダッシュボード</title>

    <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/masterdashboard/css/animate.css" rel="stylesheet">
    <link href="/masterdashboard/css/style.css" rel="stylesheet">
    <link href="/masterdashboard/css/plus.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

</head>

<body>


<div id="wrapper">

<!--左ナビゲーション -->
    <?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
<!--左ナビゲーションここまで -->



    <div id="page-wrapper" class="gray-bg">



<!--コンテンツ　ヘッダーナビゲーション -->
    <?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
<!--コンテンツ　ヘッダーナビゲーションここまで -->

<!--ランキング登録-->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>ピックアップユーザー</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="/masterdashboard/ranking/rank_point.php">平均評価ランキング</a>
                    </li>
                    <li>
                        <a href="/masterdashboard/ranking/rank_access.php">アクセスランキング</a>
                    </li>
                    <li>
                        <a href="/masterdashboard/ranking/rank_blog.php">ブログランキング</a>
                    </li>
                    <li>
                        <strong>ピックアップユーザー</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>


        <div class="wrapper wrapper-content animated fadeInRight">
            <form action="./matome_save.php" method="POST" name="form_write" id="form_regist" enctype="multipart/form-data" class="form-horizontal">
            <div class="row">
                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-title">
                            <label class="control-label">更新日※ランキングを更新した日を表示させて下さい。</label>
                        </div>

                        <div class="ibox-content">

                            <div class="table-responsive">
                                <table class="table shoping-cart-table">

                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input id="date_added" type="text" class="form-control">
                                                </div>
                                            </td>

                                        </tr>
                                    </tbody>

                                </table>
                            </div>

                        </div>


                    </div>
                </div>

                <div class="col-md-8">
                    <div class="ibox">
                        <div class="ibox-title">
                            <label class="control-label">登録させたいユーザーのIDと順位を入力して下さい。
                                <strong><a href="/masterdashboard/member/list.php">(<i class="fa fa-share"></i>ユーザ一覧へ)</a>
                            </label>
                        </div>

                        <div class="ibox-content">

                            <div class="table-responsive">
                                <table class="table shoping-cart-table">

                                    <tbody>
                                        <tr>

                                            <td>
                                                <select class="form-control" name="account">
                                                    <option>1位</option>
                                                    <option>2位</option>
                                                    <option>3位</option>
                                                    <option>4位</option>
                                                    <option>5位</option>
                                                    <option>6位</option>
                                                    <option>7位</option>
                                                    <option>8位</option>
                                                    <option>9位</option>
                                                    <option>10位</option>
                                                </select>

                                            </td>

                                            <td>

                                                <input type="text" class="form-control" placeholder="ユーザーID入力">

                                            </td>

                                            <td>
                                                <div class="entbottom"><button class="btn btn-primary" type="submit">ランキングに入力</button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>

                                </table>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
            </form>




<!--口コミランキング-->

            <div class="row">
                <div class="col-md-9">

                    <div class="ibox">
                        <div class="ibox-title">
                            <span class="pull-right">(<strong>全5人まで</strong>)</span>
                            <h5>口コミ投稿数を元にランキングを更新してください。※更新日は毎週月曜日11時までに</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table shoping-cart-table">

                                <tbody>
                                    <tr>
                                        <td rowspan="2">
                                            <div class="vote-actions">
                                                                    <a href="#">
                                                                        <i class="fa fa-chevron-up"> </i>
                                                                    </a>
                                                                    <div>1位</div>
                                                                    <a href="#">
                                                                        <i class="fa fa-chevron-down"></i>
                                                                    </a>
                                                                </div>  
                                        </td>

                                        <td width="90">
                                            <div class="cart-product-imitation">
                                                <a href="" target="_blank" class="usernamehavar">
                                                <img src="/masterdashboard/member/img/test/sample1.jpg" width="60" height="60" alt="" />
                                                </a>
                                            </div>
                                        </td>

                                        <td class="desc">
                                            <h3>
                                                                        <a href="#" class="text-navy">
                                                                        ユーザーの名前
                                                                        </a>
                                                                    </h3>

                                                                    <dl class="small m-b-none">
                                                                    <dt>エリア</dt>
                                                                    <dd>東京:池袋,大塚,巣鴨</dd>
                                                                    </dl>

                                                            </td>

                                        <td>
                                            <h4>
                                            2014/08/25登録
                                            </h4>
                                        </td>

                                                            <td>
                                                                    <h4>
                                                                    ランク:S
                                                                    </h4>
                                                            </td>

                                                        </tr>

                                                        <tr>

                                        <td colspan="3">
                                                                    <span class="text-muted"><i class="fa fa-star-o"></i>ID：s5668</span>
                                                                    |
                                                                    <span class="text-muted"><i class="fa fa-area-chart"></i> 口コミ投稿数:17件</span>
                                                                    |
                                                                    <span class="text-muted"><i class="fa fa-area-chart"></i> 平均点:3.17</span>
                                                                    |
                                                                    <span class="text-muted"><i class="fa fa-area-chart"></i> 信憑性大数:3件</span>
                                            |
                                                                    <span class="text-muted"><i class="fa fa-gift"></i> 最終ログイン:2016-08-25 14:37</span>
                                        </td>


                                        <td>
                                            <div class="vote-iconsmall">
                                            <a href="#" class="text-muted" title="削除"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>

                                    </div>


                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table shoping-cart-table">

                                <tbody>
                                    <tr>
                                        <td rowspan="2">
                                            <div class="vote-actions">
                                                                    <a href="#">
                                                                        <i class="fa fa-chevron-up"> </i>
                                                                    </a>
                                                                    <div>2位</div>
                                                                    <a href="#">
                                                                        <i class="fa fa-chevron-down"></i>
                                                                    </a>
                                                                </div>  
                                        </td>

                                        <td width="90">
                                            <div class="cart-product-imitation">
                                                <a href="" target="_blank" class="usernamehavar">
                                                <img src="/masterdashboard/member/img/test/sample1.jpg" width="60" height="60" alt="" />
                                                </a>
                                            </div>
                                        </td>

                                        <td class="desc">
                                            <h3>
                                                                        <a href="#" class="text-navy">
                                                                        ユーザーの名前
                                                                        </a>
                                                                    </h3>

                                                                    <dl class="small m-b-none">
                                                                    <dt>エリア</dt>
                                                                    <dd>東京:池袋,大塚,巣鴨</dd>
                                                                    </dl>

                                                            </td>

                                        <td>
                                            <h4>
                                            2014/08/25登録
                                            </h4>
                                        </td>

                                                            <td>
                                                                    <h4>
                                                                    ランク:S
                                                                    </h4>
                                                            </td>

                                                        </tr>

                                                        <tr>

                                        <td colspan="3">
                                                                    <span class="text-muted"><i class="fa fa-star-o"></i>ID：s5668</span>
                                                                    |
                                                                    <span class="text-muted"><i class="fa fa-area-chart"></i> 口コミ投稿数:17件</span>
                                                                    |
                                                                    <span class="text-muted"><i class="fa fa-area-chart"></i> 平均点:3.17</span>
                                                                    |
                                                                    <span class="text-muted"><i class="fa fa-area-chart"></i> 信憑性大数:3件</span>
                                            |
                                                                    <span class="text-muted"><i class="fa fa-gift"></i> 最終ログイン:2016-08-25 14:37</span>
                                        </td>


                                        <td>
                                            <div class="vote-iconsmall">
                                            <a href="#" class="text-muted" title="削除"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>

                                    </div>

                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table shoping-cart-table">

                                <tbody>
                                    <tr>
                                        <td rowspan="2">
                                            <div class="vote-actions">
                                                                    <a href="#">
                                                                        <i class="fa fa-chevron-up"> </i>
                                                                    </a>
                                                                    <div>3位</div>
                                                                    <a href="#">
                                                                        <i class="fa fa-chevron-down"></i>
                                                                    </a>
                                                                </div>  
                                        </td>

                                        <td width="90">
                                            <div class="cart-product-imitation">
                                                <a href="" target="_blank" class="usernamehavar">
                                                <img src="/masterdashboard/member/img/test/sample1.jpg" width="60" height="60" alt="" />
                                                </a>
                                            </div>
                                        </td>

                                        <td class="desc">
                                            <h3>
                                                                        <a href="#" class="text-navy">
                                                                        ユーザーの名前
                                                                        </a>
                                                                    </h3>

                                                                    <dl class="small m-b-none">
                                                                    <dt>エリア</dt>
                                                                    <dd>東京:池袋,大塚,巣鴨</dd>
                                                                    </dl>

                                                            </td>

                                        <td>
                                            <h4>
                                            2014/08/25登録
                                            </h4>
                                        </td>

                                                            <td>
                                                                    <h4>
                                                                    ランク:S
                                                                    </h4>
                                                            </td>

                                                        </tr>

                                                        <tr>

                                        <td colspan="3">
                                                                    <span class="text-muted"><i class="fa fa-star-o"></i>ID：s5668</span>
                                                                    |
                                                                    <span class="text-muted"><i class="fa fa-area-chart"></i> 口コミ投稿数:17件</span>
                                                                    |
                                                                    <span class="text-muted"><i class="fa fa-area-chart"></i> 平均点:3.17</span>
                                                                    |
                                                                    <span class="text-muted"><i class="fa fa-area-chart"></i> 信憑性大数:3件</span>
                                            |
                                                                    <span class="text-muted"><i class="fa fa-gift"></i> 最終ログイン:2016-08-25 14:37</span>
                                        </td>


                                        <td>
                                            <div class="vote-iconsmall">
                                            <a href="#" class="text-muted" title="削除"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>

                                    </div>


                        <div class="ibox-content">
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">ランキングの登録を確定</button>
                                                <button class="btn btn-white" type="submit">キャンセル</button>
                                </div>
                            </div>

                    </div><!--ibox-->



                </div><!--col-md-9-->



<!--データ部分-->
                <?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/right_deta.php"; ?>
<!--データ部分-->




            </div><!--low-->
<!--口コミ投稿-->

<!--信憑性ランキング-->
                    <div class="row">
                <div class="col-md-9">

                    <div class="ibox">
                        <div class="ibox-title">
                            <span class="pull-right">(<strong>全5人まで</strong>)</span>
                            <h5>口コミの信憑性を元にランキングを更新してください。※更新日は毎週月曜日11時までに</h5>
                        </div>

                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table shoping-cart-table">

                                <tbody>
                                    <tr>
                                        <td rowspan="2">
                                            <div class="vote-actions">
                                                                    <a href="#">
                                                                        <i class="fa fa-chevron-up"> </i>
                                                                    </a>
                                                                    <div>1位</div>
                                                                    <a href="#">
                                                                        <i class="fa fa-chevron-down"></i>
                                                                    </a>
                                                                </div>  
                                        </td>

                                        <td width="90">
                                            <div class="cart-product-imitation">
                                                <a href="" target="_blank" class="usernamehavar">
                                                <img src="/masterdashboard/member/img/test/sample1.jpg" width="60" height="60" alt="" />
                                                </a>
                                            </div>
                                        </td>

                                        <td class="desc">
                                            <h3>
                                                                        <a href="#" class="text-navy">
                                                                        ユーザーの名前
                                                                        </a>
                                                                    </h3>

                                                                    <dl class="small m-b-none">
                                                                    <dt>エリア</dt>
                                                                    <dd>東京:池袋,大塚,巣鴨</dd>
                                                                    </dl>

                                                            </td>

                                        <td>
                                            <h4>
                                            2014/08/25登録
                                            </h4>
                                        </td>

                                                            <td>
                                                                    <h4>
                                                                    ランク:S
                                                                    </h4>
                                                            </td>

                                                        </tr>

                                                        <tr>

                                        <td colspan="3">
                                                                    <span class="text-muted"><i class="fa fa-star-o"></i>ID：s5668</span>
                                                                    |
                                                                    <span class="text-muted"><i class="fa fa-area-chart"></i> 口コミ投稿数:17件</span>
                                                                    |
                                                                    <span class="text-muted"><i class="fa fa-area-chart"></i> 平均点:3.17</span>
                                                                    |
                                                                    <span class="text-muted"><i class="fa fa-area-chart"></i> 信憑性大数:3件</span>
                                            |
                                                                    <span class="text-muted"><i class="fa fa-gift"></i> 最終ログイン:2016-08-25 14:37</span>
                                        </td>


                                        <td>
                                            <div class="vote-iconsmall">
                                            <a href="#" class="text-muted" title="削除"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>

                                    </div>

                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table shoping-cart-table">

                                <tbody>
                                    <tr>
                                        <td rowspan="2">
                                            <div class="vote-actions">
                                                                    <a href="#">
                                                                        <i class="fa fa-chevron-up"> </i>
                                                                    </a>
                                                                    <div>2位</div>
                                                                    <a href="#">
                                                                        <i class="fa fa-chevron-down"></i>
                                                                    </a>
                                                                </div>  
                                        </td>

                                        <td width="90">
                                            <div class="cart-product-imitation">
                                                <a href="" target="_blank" class="usernamehavar">
                                                <img src="/masterdashboard/member/img/test/sample1.jpg" width="60" height="60" alt="" />
                                                </a>
                                            </div>
                                        </td>

                                        <td class="desc">
                                            <h3>
                                                                        <a href="#" class="text-navy">
                                                                        ユーザーの名前
                                                                        </a>
                                                                    </h3>

                                                                    <dl class="small m-b-none">
                                                                    <dt>エリア</dt>
                                                                    <dd>東京:池袋,大塚,巣鴨</dd>
                                                                    </dl>

                                                            </td>

                                        <td>
                                            <h4>
                                            2014/08/25登録
                                            </h4>
                                        </td>

                                                            <td>
                                                                    <h4>
                                                                    ランク:S
                                                                    </h4>
                                                            </td>

                                                        </tr>

                                                        <tr>

                                        <td colspan="3">
                                                                    <span class="text-muted"><i class="fa fa-star-o"></i>ID：s5668</span>
                                                                    |
                                                                    <span class="text-muted"><i class="fa fa-area-chart"></i> 口コミ投稿数:17件</span>
                                                                    |
                                                                    <span class="text-muted"><i class="fa fa-area-chart"></i> 平均点:3.17</span>
                                                                    |
                                                                    <span class="text-muted"><i class="fa fa-area-chart"></i> 信憑性大数:3件</span>
                                            |
                                                                    <span class="text-muted"><i class="fa fa-gift"></i> 最終ログイン:2016-08-25 14:37</span>
                                        </td>


                                        <td>
                                            <div class="vote-iconsmall">
                                            <a href="#" class="text-muted" title="削除"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>

                                    </div>


                        <div class="ibox-content">
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">ランキングの登録を確定</button>
                                                <button class="btn btn-white" type="submit">キャンセル</button>
                                </div>
                            </div>

                    </div><!--ibox-->



                </div><!--col-md-9-->
            </div>
<!--信憑性ランキング-->


        </div><!--wrapper wrapper-content animated fadeInRight-->

<!--フッター部分 -->
    <?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
<!--フッター部分 -->

        </div>
        </div>



    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
    <script src="/masterdashboard/js/bootstrap.min.js"></script>
    <script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/masterdashboard/js/inspinia.js"></script>
    <script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>
    <!-- Data picker -->
    <script src="/masterdashboard/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <!-- FooTable -->
    <script src="/masterdashboard/js/plugins/footable/footable.all.min.js"></script>

    <!-- Data picker -->
    <script src="/masterdashboard/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="/masterdashboard/js/plugins/datapicker/datepicker-ja.min.js"></script>

    <script>

$('#date_added').datepicker({
    format: 'yyyy/mm/dd',
    language: 'ja',
    autoclose: true,
    clearBtn: true,
});
$('#date_added').datepicker().datepicker('setDate','today');
    </script>



</body>

</html>
