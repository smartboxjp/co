					<div class="col-lg-3">
						<div class="ibox float-e-margins">
							<div class="ibox-content mailbox-content">
								<div class="file-manager">


									<div class="m-b-sm">
										<img src="/masterdashboard/img/a2.jpg" style="width: 60px">
									</div>

									<a class="btn btn-block btn-success compose-mail" href="/masterdashboard/shop/shop_mailform.php"><i class="fa fa-envelope-o"></i> お店へメール</a>
									<div class="space-25"></div>
									<ul class="folder-list m-b-md" style="padding: 0">
										<h5>オススメムラムラM字妻那覇店</h5>
										<li> <i class="fa fa-calendar-o"></i>登録日<span class="pull-right">2017/08/25</span>
										</li>
										<li> <i class="fa fa-pencil-square-o"></i>ショップID<span class="pull-right">s2546</span>
										</li>
										<li> <i class="fa fa-envelope-o"></i>アドレス<span class="pull-right">hjfkarafrfa@faserjhfa</span>
										</li>
										<li> <i class="fa fa-envelope-square"></i>女の子から受け取ったメール<span class="label label-info pull-right">12</span>
										</li>
										<li> <i class="fa fa-globe"></i>第一エリア<span class="pull-right">東京都</span>
										</li>
										<li> <i class="fa fa-globe"></i>第二エリア<span class="pull-right">池袋.大塚.巣鴨</span>
										</li>
										<li> <i class="fa fa-female"></i>業種<span class="pull-right">デリバリーヘルス</span>
										</li>
										<li> <i class="fa fa-desktop"></i>最終ログイン<span class="pull-right">2016/08/25(08:17)</span>
										</li>
										<li> <i class="fa fa-heart-o"></i>プラン切り替え
											<span class="pull-right">
												<div class="onoffswitch"><input type="checkbox" checked class="onoffswitch-checkbox" id="example1"><label class="onoffswitch-label" for="example1"><span class="onoffswitch-inner"></span><span class="onoffswitch-switch"></span></label>
												</div>
											</span>
										</li>
										<li> <i class="fa fa-eye"></i>調査結果表示
											<span class="pull-right">
												<div class="onoffswitch"><input type="checkbox" checked class="onoffswitch-checkbox" id="example2"><label class="onoffswitch-label" for="example2"><span class="onoffswitch-inner"></span><span class="onoffswitch-switch"></span></label>
												</div>
											</span>
										</li>
										<li> <i class="fa fa-book"></i>このお店を編集
											<span class="pull-right">
												<a href=""><i class="fa fa-pencil"></i>編集</a>
											</span>
										</li>
									</ul>

									<a class="btn btn-block btn-danger compose-mail" href="/masterdashboard/shop/mysteryshopping.php"><i class="fa fa-pencil"></i> 調査結果を登録</a>
									<div class="space-25"></div>
									<h5>現在のお店の状況</h5>
									<ul class="folder-list m-b-md" style="padding: 0">
										<li> <i class="fa fa-star-o"></i>点数<span class="pull-right">3.75点</span>
										</li>
										<li> <i class="fa fa-comment-o"></i>口コミ投稿数<span class="pull-right">17件</span>
										</li>
										<li> <i class="fa fa-picture-o"></i>画像投稿枚数<span class="pull-right">33枚</span>
										</li>
										<li> <i class="fa fa-check-square-o"></i>ユーザーお気に入り<span class="pull-right">33件</span>
										</li>
										<li> <i class="fa fa-area-chart"></i>アクセス本日(ユニーク)<span class="pull-right">117</span>
										</li>
										<li> <i class="fa fa-area-chart"></i>アクセス昨日(ユニーク)<span class="pull-right">324</span>
										</li>
										<li> <i class="fa fa-area-chart"></i>アクセス一昨日(ユニーク)<span class="pull-right">354</span>
										</li>
									</ul>



									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>