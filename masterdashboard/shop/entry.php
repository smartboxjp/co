<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_catearea = new CommonCateArea();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateJob.php";
	$common_catejob = new CommonCateJob();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
	
	//プラン
	$arr_shop_view_level = $common_shop -> Fn_shop_view_level();
	$arr_flag_open = $common_shop -> Fn_flag_open();
	
	foreach($_GET as $key => $value)
	{ 
		//$$key = trim($common_dao->db_string_escape($value));
		$$key = $value;
	}
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	//エリア
	$arr_area = $common_catearea->Fn_cate_area_list($sql);
	if(!is_null($arr_area))
	{
		foreach($arr_area as $arr_key=>$arr_value)
		{
			foreach($arr_value as $key=>$value)
			{
				$arr_cate_area_s_id[$arr_value["cate_area_s_id"]] = $arr_value["cate_area_l_title"]." - ".$arr_value["cate_area_s_title"];
			}
		}
	}
	
	//職種
	$arr_job = $common_catejob->Fn_db_cate_job_all();
	if(!is_null($arr_job))
	{
		foreach($arr_job as $arr_key=>$arr_value)
		{
			foreach($arr_value as $key=>$value)
			{
				$arr_cate_job_id[$arr_value["cate_job_id"]] = $arr_value["cate_job_title"];
			}
		}
	}
	
	$arr_where = array();
	$var = "s_flag_open";
	//$arr_where[$var] = "1"; //公開のみ
	
	$db_result_shop = $common_shop -> Fn_db_shop($shop_id, $arr_where);
	foreach($db_result_shop as $arr_shop)
	{
		foreach($arr_shop as $key=>$value)
		{
			$$key = $value;
		}
	}
	
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>お店登録 | ダッシュボード</title>

    <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/iCheck/custom.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/chosen/chosen.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/cropper/cropper.min.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/switchery/switchery.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/nouslider/jquery.nouislider.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/select2/select2.min.css" rel="stylesheet">

    <link href="/masterdashboard/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">

    <link href="/masterdashboard/css/animate.css" rel="stylesheet">
    <link href="/masterdashboard/css/style.css" rel="stylesheet">
    <link href="/masterdashboard/css/plus.css" rel="stylesheet">

    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
<script type="text/javascript">
	$(function() {
			
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";
			
			<? if($shop_id=="") { ?>
			err_check_count += check_input_id("shop_id");
			<? } ?>
			err_check_count += check_input("shop_name");
			err_check_count += check_input("cate_area_s_id");
			err_check_count += check_input("cate_job_id");
			
			err_check_count += check_input_email("email_kanri");
			err_check_count += check_input_pw("shop_pw");
			
			if(err_check_count!=0)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
				
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}

		function check_input_id($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkID($('#'+$str).val())==false)
			{
				err ="<span class='errorForm'>英数半角で入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if($('#'+$str).val().length<3)
			{
				err ="<span class='errorForm'>3文字以上で入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		//英数半角
		function checkID(value){
			if(value.match(/[^0-9a-z]+/) == null){
				return true;
			}else{
				return false;
			}
		} 

		//メールチェック
		function check_input_email($str_1) 
		{
			$("#err_"+$str_1).html(err_default);
			$("#"+$str_1).css(background,bgcolor_default);
			if($('#'+$str_1).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str_1).val()) == false)
			{
				err ="<span class='errorForm'>メールアドレスは半角英数字でご入力ください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}

		//メールチェック
		
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}
		
		function check_input_pw($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<span class='errorForm'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if($('#'+$str).val().length<6)
			{
				err ="<span class='errorForm'>６文字以上入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
						

	});
	
//-->
</script>

</head>

<body>
<?
	//管理者チェック
	$common_connect -> Fn_admin_check();
?>

    <div id="wrapper">

   <!--左ナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
   <!--左ナビゲーションここまで -->



	<div id="page-wrapper" class="gray-bg">



<!--コンテンツ　ヘッダーナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
<!--コンテンツ　ヘッダーナビゲーションここまで -->



<!--店舗登録フォームヘッダー-->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>新規店舗登録</h2>
                    <ol class="breadcrumb">
                        <li>
                            <strong>店舗登録</strong></a>
                        </li>
                        <li>
                            <a href="/masterdashboard/member/entry.php">ユーザー登録</a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
<!--店舗登録フォームヘッダー-->

<!--コンテンツ-->
        <div class="wrapper wrapper-content animated fadeInRight">

<!--登録フォームここから-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
<!--基本情報ヘッダー-->
                        <div class="ibox-title">
                            <h5>基本情報登録<small>-お店の基本情報は全て入力が必要です。</small></h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
<!--基本情報ヘッダー-->
                        <div class="ibox-content">
                            <form action="./entry_save.php" method="POST" name="form_write" id="form_regist" enctype="multipart/form-data" class="form-horizontal">


<!--お店ID-->
                                <div class="form-group"><label class="col-sm-2 control-label">お店のID</label>
                                    <div class="col-sm-6">
                                    	<? $var = "shop_id";?>
                                      <?php
										if($$var!="")
										{
											echo $$var;
										}
										?>
                                      <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" type="<? if($$var!=""){ echo "hidden";} else { echo " text ";}?>" class="form-control" placeholder="3文字以上の英語小文字、数字のみ入力可能"> 
                                      <input type="hidden" name="check_up" value="<? echo $shop_id;?>">
                                      <label id="err_<?php echo $var;?>"><? if($$var!=""){ echo "（変更出来ません）";} ?></label>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
<!--お店ID-->

<!--テキストフォームコメント付き-->
                                <div class="form-group"><label class="col-sm-2 control-label">お店の名前</label>
                                    <div class="col-sm-6">
                                      <? $var = "shop_name";?>
                                      <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control demo" maxlength="20" placeholder="お店の名前を最大20文字以内で入力して下さい。"> 
                                      <label id="err_<?php echo $var;?>"></label>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
<!---->


<!--セレクト1-->
                                <div class="form-group"><label class="col-sm-2 control-label">事務所本拠地</label>

                                    <div class="col-sm-6">
                                    <? $var = "cate_area_s_id";?>
                                    <select name="<?php echo $var;?>" id="<?php echo $var;?>" class="form-control m-b" >
                                      <option value="">お店や事務所のある場所はどこですか？</option>
                                        <?
											foreach($arr_cate_area_s_id as $key=>$value)
											{
										?>
                                    		<option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
										<?
											}
										?>
                                    
                                    </select>
                                    <label id="err_<?php echo $var;?>"></label>
                                    </div>
                                </div>
<!--セレクト1-->

				<div class="hr-line-dashed"></div>

<!--セレクト2-->
                                <div class="form-group"><label class="col-sm-2 control-label">業種</label>

                                    <div class="col-sm-6">
                                    <? $var = "cate_job_id";?>
                                    <select class="form-control m-b" name="<?php echo $var;?>" id="<?php echo $var;?>">
                                        <option value="">お店の業種を一つ選んで下さい</option>
                                        <?
                                        foreach($arr_cate_job_id as $key=>$value)
                                        {
                                        ?>
                                        <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
                                        <?
                                        }
                                        ?>
                                    </select>
                                    <label id="err_<?php echo $var;?>"></label>
                                    </div>
                                </div>
<!--セレクト2-->

				<div class="hr-line-dashed"></div>


<!--お店パスワード-->
                                <div class="form-group"><label class="col-sm-2 control-label">お店のパスワード</label>
                                    <div class="col-sm-6">
                                    	<? $var = "shop_pw";?>
                                      <? if($$var=="") { $$var=date("Ymd").$common_connect->Fn_random_password(10);}?>
                                      <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" type="text" class="form-control demo3" maxlength="20" placeholder="お店のパスワードを決めて入力して下さい。"> 
                                      <label id="err_<?php echo $var;?>"></label>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
<!--お店パスワード-->

<!--お店メールアドレス-->
                                <div class="form-group"><label class="col-sm-2 control-label">お店のメールアドレス</label>
                                    <div class="col-sm-6">
                                      <? $var = "email_kanri";?>
                                      <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" type="text" class="form-control demo2" maxlength="50"> 
                                      <label id="err_<?php echo $var;?>"></label>
                                      <span class="help-block m-b-none">メールアドレスを間違えるとお店にユーザーからの問い合わせが届かなくなりますので注意してください。
                                    </span>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
<!--お店メールアドレス-->


<!--有料・無料-->
                                <div class="form-group"><label class="col-sm-2 control-label">公開・非公開</label>
                                    <div class="col-sm-6">
                                      <? $var = "flag_open";?>
                                      <select name="<? echo $var;?>" id="<? echo $var;?>">
                                      <?
                                      foreach($arr_flag_open as $key=>$value)
                                      {
                                      ?>
                                        <option value="<? echo $key;?>" <? if($key == $$var) { echo " selected ";}?>><? echo $value;?></option>
                                      <?
                                      }
                                      ?>
                                      </select>
                                      <label id="err_<?php echo $var;?>"></label>
                                    </span>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
<!--有料・無料-->

<!--公開・非公開-->
                                <div class="form-group"><label class="col-sm-2 control-label">有料・無料</label>
                                    <div class="col-sm-6">
                                      <? $var = "shop_view_level";?>
                                      <select name="<? echo $var;?>" id="<? echo $var;?>">
                                      <?
                                      foreach($arr_shop_view_level as $key=>$value)
                                      {
                                      ?>
                                        <option value="<? echo $key;?>" <? if($key == $$var) { echo " selected ";}?>><? echo $value;?></option>
                                      <?
                                      }
                                      ?>
                                      </select>
                                      <label id="err_<?php echo $var;?>"></label>
                                    </span>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
<!--公開・非公開-->

<!--イメージクリッパー-->
                    <div class="ibox-title  back-change">
                        <h5>お店のサムネイル画像を登録<small>※無登録でも可能。また店舗管理で登録や編集も出来ます。</small></h5>

                    </div>
                    <div class="ibox-content">
                        <p>
                            お店のサムネイル画像をトリミングで編集登録が出来ます。120px×120pxで表示されます
                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="image-crop">
                                    <img src="/masterdashboard/img/p3.jpg">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h4>Preview image</h4>
                                <div class="img-preview img-preview-sm"></div>
                                <p>
                                    画像の好きな位置に枠をあわせて決定を押してください。
                                </p>
                                <div class="btn-group">
                                    <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                        <input type="file" accept="image/*" name="file" id="inputImage" class="hide">
                                        カットしたい画像をアップ
                                    </label>
                                    
                                </div>
				<br>
                                <div class="btn-group">
                                    <button class="btn btn-white" id="zoomIn" type="button">Zoom In</button>
                                    <button class="btn btn-white" id="zoomOut" type="button">Zoom Out</button>
                                    <button class="btn btn-white" id="rotateLeft" type="button">Rotate Left</button>
                                    <button class="btn btn-white" id="rotateRight" type="button">Rotate Right</button>
                                </div>
				<br>
                                <div class="btn-group">
                                    サムネイルを反映する
                                  <div class="onoffswitch">
                                  <? $var = "check_img";?>
                                  <input type="checkbox" name="<? echo $var;?>" value="1" class="onoffswitch-checkbox" id="checkbox_<? echo $shop_id;?>">
                                  <label class="onoffswitch-label" for="checkbox_<? echo $shop_id;?>">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                  </label>
                                </div>
                                  <?
																	if($shop_thumbnail!="")
																	{
																		echo "<img src='/".global_shop_dir.$shop_id."/".$shop_thumbnail."?".date("his")."'>";
																	}
																	?>
                                </div>
                            </div>
                        </div>
                    </div>
<!--イメージクリッパー-->

<input type="hidden" name="cropped" class="cropped">

<!--イメージクリッパーサムネイル画像表示-->

                        <div class="row">
                            <div class="col-md-6">
<script>
	$(document).ready(function(){

			var $image = $(".image-crop > img")
			$($image).cropper({
					aspectRatio: 1,//トリミング比率サイズ
					preview: ".img-preview",
					done: function(data) {
							// Output the result data for cropping image.
						$('.cropped').val($image.cropper("getDataURL"));
					}
			});

			var $inputImage = $("#inputImage");
			if (window.FileReader) {
					$inputImage.change(function() {
							var fileReader = new FileReader(),
											files = this.files,
											file;

							if (!files.length) {
									return;
							}

							file = files[0];

							if (/^image\/\w+$/.test(file.type)) {
									fileReader.readAsDataURL(file);
									fileReader.onload = function () {
										$('#checkbox_<? echo $shop_id;?>').prop("checked",true); 
											$inputImage.val("");
											$image.cropper("reset", true).cropper("replace", this.result);
									};
							} else {
									showMessage("Please choose an image file.");
							}
					});
			} else {
					$inputImage.addClass("hide");
			}

			$("#download").click(function() {
				 var img = $image.cropper("getDataURL");
				 $('.cropped').val(img);
				 //$('.cropped').append('<img src="'+img+'">');
			});

			$("#zoomIn").click(function() {
					$image.cropper("zoom", 0.1);
			});

			$("#zoomOut").click(function() {
					$image.cropper("zoom", -0.1);
			});

			$("#rotateLeft").click(function() {
					$image.cropper("rotate", 45);
			});

			$("#rotateRight").click(function() {
					$image.cropper("rotate", -45);
			});

			$("#setDrag").click(function() {
				// crop のデータを取得
				 /* start */
				 var data = $('#img').cropper('getData');
			
				 // 切り抜きした画像のデータ
				 // このデータを元にして画像の切り抜きが行われます
				 var image = {
					 width: Math.round(data.width),
					 height: Math.round(data.height),
					 x: Math.round(data.x),
					 y: Math.round(data.y),
					 _token: 'jf89ajtr234534829057835wjLA-SF_d8Z' // csrf用
					};
					
					// post 処理
				 $.post('/cropper', image, function(res){
					 // 成功すれば trueと表示されます
					 console.log(res);
				 });
				 /* end */

					$image.cropper("setDragMode", "crop");
			});

			$('#data_1 .input-group.date').datepicker({
					todayBtn: "linked",
					keyboardNavigation: false,
					forceParse: false,
					calendarWeeks: true,
					autoclose: true
			});

			$('#data_2 .input-group.date').datepicker({
					startView: 1,
					todayBtn: "linked",
					keyboardNavigation: false,
					forceParse: false,
					autoclose: true,
					format: "dd/mm/yyyy"
			});

			$('#data_3 .input-group.date').datepicker({
					startView: 2,
					todayBtn: "linked",
					keyboardNavigation: false,
					forceParse: false,
					autoclose: true
			});

			$('#data_4 .input-group.date').datepicker({
					minViewMode: 1,
					keyboardNavigation: false,
					forceParse: false,
					autoclose: true,
					todayHighlight: true
			});

			$('#data_5 .input-daterange').datepicker({
					keyboardNavigation: false,
					forceParse: false,
					autoclose: true
			});

			var elem = document.querySelector('.js-switch');
			var switchery = new Switchery(elem, { color: '#1AB394' });

			var elem_2 = document.querySelector('.js-switch_2');
			var switchery_2 = new Switchery(elem_2, { color: '#ED5565' });

			var elem_3 = document.querySelector('.js-switch_3');
			var switchery_3 = new Switchery(elem_3, { color: '#1AB394' });

			$('.i-checks').iCheck({
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green'
			});

			$('.demo1').colorpicker();

			var divStyle = $('.back-change')[0].style;
			$('#demo_apidemo').colorpicker({
					color: divStyle.backgroundColor
			}).on('changeColor', function(ev) {
									divStyle.backgroundColor = ev.color.toHex();
							});

			$('.clockpicker').clockpicker();

			$('input[name="daterange"]').daterangepicker();

			$('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

			$('#reportrange').daterangepicker({
					format: 'MM/DD/YYYY',
					startDate: moment().subtract(29, 'days'),
					endDate: moment(),
					minDate: '01/01/2012',
					maxDate: '12/31/2015',
					dateLimit: { days: 60 },
					showDropdowns: true,
					showWeekNumbers: true,
					timePicker: false,
					timePickerIncrement: 1,
					timePicker12Hour: true,
					ranges: {
							'Today': [moment(), moment()],
							'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
							'Last 7 Days': [moment().subtract(6, 'days'), moment()],
							'Last 30 Days': [moment().subtract(29, 'days'), moment()],
							'This Month': [moment().startOf('month'), moment().endOf('month')],
							'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
					},
					opens: 'right',
					drops: 'down',
					buttonClasses: ['btn', 'btn-sm'],
					applyClass: 'btn-primary',
					cancelClass: 'btn-default',
					separator: ' to ',
					locale: {
							applyLabel: 'Submit',
							cancelLabel: 'Cancel',
							fromLabel: 'From',
							toLabel: 'To',
							customRangeLabel: 'Custom',
							daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
							monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
							firstDay: 1
					}
			}, function(start, end, label) {
					console.log(start.toISOString(), end.toISOString(), label);
					$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			});

			$(".select2_demo_1").select2();
			$(".select2_demo_2").select2();
			$(".select2_demo_3").select2({
					placeholder: "Select a state",
					allowClear: true
			});


			$(".touchspin1").TouchSpin({
					buttondown_class: 'btn btn-white',
					buttonup_class: 'btn btn-white'
			});

			$(".touchspin2").TouchSpin({
					min: 0,
					max: 100,
					step: 0.1,
					decimals: 2,
					boostat: 5,
					maxboostedstep: 10,
					postfix: '%',
					buttondown_class: 'btn btn-white',
					buttonup_class: 'btn btn-white'
			});

			$(".touchspin3").TouchSpin({
					verticalbuttons: true,
					buttondown_class: 'btn btn-white',
					buttonup_class: 'btn btn-white'
			});


	});
	var config = {
					'.chosen-select'           : {},
					'.chosen-select-deselect'  : {allow_single_deselect:true},
					'.chosen-select-no-single' : {disable_search_threshold:10},
					'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
					'.chosen-select-width'     : {width:"95%"}
					}
			for (var selector in config) {
					$(selector).chosen(config[selector]);
			}

	$("#ionrange_1").ionRangeSlider({
			min: 0,
			max: 5000,
			type: 'double',
			prefix: "$",
			maxPostfix: "+",
			prettify: false,
			hasGrid: true
	});

	$("#ionrange_2").ionRangeSlider({
			min: 0,
			max: 10,
			type: 'single',
			step: 0.1,
			postfix: " carats",
			prettify: false,
			hasGrid: true
	});

	$("#ionrange_3").ionRangeSlider({
			min: -50,
			max: 50,
			from: 0,
			postfix: "°",
			prettify: false,
			hasGrid: true
	});

	$("#ionrange_4").ionRangeSlider({
			values: [
					"January", "February", "March",
					"April", "May", "June",
					"July", "August", "September",
					"October", "November", "December"
			],
			type: 'single',
			hasGrid: true
	});

	$("#ionrange_5").ionRangeSlider({
			min: 10000,
			max: 100000,
			step: 100,
			postfix: " km",
			from: 55000,
			hideMinMax: true,
			hideFromTo: false
	});

	$(".dial").knob();

	$("#basic_slider").noUiSlider({
			start: 40,
			behaviour: 'tap',
			connect: 'upper',
			range: {
					'min':  20,
					'max':  80
			}
	});

	$("#range_slider").noUiSlider({
			start: [ 40, 60 ],
			behaviour: 'drag',
			connect: true,
			range: {
					'min':  20,
					'max':  80
			}
	});

	$("#drag-fixed").noUiSlider({
			start: [ 40, 60 ],
			behaviour: 'drag-fixed',
			connect: true,
			range: {
					'min':  20,
					'max':  80
			}
	});


</script>
                            </div>
			</div>


<!--イメージクリッパーサムネイル画像表示-->

<!--登録ボタン-->
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                    <? $var = "form_confirm";?>
                                    	<button class="btn btn-primary" type="submit" id="<? echo $var;?>">登録</button>
                                      <button class="btn btn-white" type="submit">キャンセル</button>
                                    </div>
                                </div>
<!--登録ボタン-->

                            </form>
                        </div><!--ibox-content-->
                    </div><!--ibox float-e-margins-->
                </div><!--col-lg-12-->
            </div><!--row-->
<!--登録フォームここまで-->


        </div>
<!--コンテンツここまで-->


<!--フッター部分 -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
<!--フッター部分 -->

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/masterdashboard/js/inspinia.js"></script>
    <script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>
    <script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Chosen -->
    <script src="/masterdashboard/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="/masterdashboard/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="/masterdashboard/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="/masterdashboard/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="/masterdashboard/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="/masterdashboard/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="/masterdashboard/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="/masterdashboard/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="/masterdashboard/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="/masterdashboard/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="/masterdashboard/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="/masterdashboard/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="/masterdashboard/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="/masterdashboard/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="/masterdashboard/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

   <!-- コメントカウントダウン -->
    <script src="/masterdashboard/js/plugins/count/bootstrap-maxlength.js"></script>
<script>
$(document).ready(function () {
  $('.demo,.demo1,.demo2,.demo3').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>

</body>

</html>
