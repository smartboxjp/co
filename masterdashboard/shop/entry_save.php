<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>店舗登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	foreach($_POST as $key => $value)
	{ 
		//$$key = trim($common_dao->db_string_escape($value));
		$$key = $value;
	}
	//管理者チェック
	$common_connect -> Fn_admin_check();
	

	if($shop_id == "" || $shop_name == "")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
	
	if($check_up=="")
	{
		$db_result = $common_shop->Fn_db_shop ($shop_id);
		if($db_result)
		{
			if($shop_id == $db_result[0]["shop_id"])
			{
				$common_connect -> Fn_javascript_back("既に登録されているIDです。");
			}
		}
	}
	
	$datetime = date("Y/m/d H:i:s");
	$user_agent = substr(getenv("HTTP_USER_AGENT"), 0, 250);
	
	//array
	$arr_data = array();
	$arr_db_field = array("shop_pw", "shop_name", "cate_area_s_id", "cate_job_id");
	$arr_db_field = array_merge($arr_db_field, array("email_kanri", "shop_view_level", "flag_open"));
	

	//基本情報
	if($check_up=="")
	{
		$var = "shop_id"; $arr_data[$var] = $$var;
		foreach($arr_db_field as $value) {
			$arr_data[$value] = $$value;
		}
		$common_shop -> Fn_shop_insert($arr_data);
	}
	else
	{
		$arr_where = array();
		$var = "shop_id"; $arr_where[$var] = $$var;
		foreach($arr_db_field as $value) {
			$arr_data[$value] = $$value;
		}
		$common_shop -> Fn_shop_update($arr_data, $arr_where);
	}

	if($check_img=="1")
	{
		//Folder生成
		$up_file_name = "shop_thumbnail.png";
		$save_dir = $global_path.global_shop_dir.$shop_id."/";
		$fname_new_name[1] = $up_file_name;
		
		//Folder生成
		$common_image -> create_folder ($save_dir);
		//ヘッダに「data:image/png;base64,」が付いているので、それは外す
		$cropped = preg_replace("/data:[^,]+,/i","",$cropped);
		
		//残りのデータはbase64エンコードされているので、デコードする
		$cropped = base64_decode($cropped);
		
		//まだ文字列の状態なので、画像リソース化
		$image = imagecreatefromstring($cropped);
	 
		imagesavealpha($image, TRUE); // 透明色の有効
		imagepng($image ,$save_dir.$fname_new_name[1]);
		
		$common_image -> create_thumbnail($save_dir.$up_file_name, "", $save_dir, 150);
		/*
		$new_end_name="_1";
		$fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $shop_id, $text_img_1, "");
		*/
		
		//画像
		$arr_where = array();
		$arr_where["shop_id"] = $shop_id;
		
		$shop_thumbnail = $fname_new_name[1];
		$up_date = $datetime;
		
		//array
		$arr_db_field = array("shop_thumbnail", "up_date");
		
		$arr_data = array();
		foreach($arr_db_field as $value) {
			$arr_data[$value] = $$value;
		}
	
		$common_shop->Fn_shop_update ($arr_data, $arr_where);

	}
	
	$common_connect-> Fn_javascript_move("ショップ登録・修正しました", "list.php?shop_id=".$shop_id);
?>
</body>
</html>