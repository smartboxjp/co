<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();

	
	
  //エリア
  $sql = " select s.cate_area_s_id, s.cate_area_l_id, cate_area_l_title, cate_area_s_title FROM cate_area_l l inner join cate_area_s s on l.cate_area_l_id=s.cate_area_l_id order by s.view_level" ;
  $db_result = $common_dao->db_query_bind($sql);
  if($db_result)
  {
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
      $arr_cate_area[$db_result[$db_loop]["cate_area_s_id"]] = $db_result[$db_loop]["cate_area_l_title"]."/".$db_result[$db_loop]["cate_area_s_title"];
      $arr_cate_area_ids[$db_result[$db_loop]["cate_area_l_id"]][$db_result[$db_loop]["cate_area_s_id"]] = $db_result[$db_loop]["cate_area_s_id"];
    }
  }

  //大エリア
  $sql = " select cate_area_l_id, cate_area_l_title FROM cate_area_l order by view_level" ;
  $db_result = $common_dao->db_query_bind($sql);
  if($db_result)
  {
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
      $arr_cate_area_l[$db_result[$db_loop]["cate_area_l_id"]] = $db_result[$db_loop]["cate_area_l_title"];
    }
  }

  //職種
  $sql = " select cate_job_id, cate_job_title FROM cate_job order by view_level" ; 
  $db_result = $common_dao->db_query_bind($sql);
  if($db_result)
  {
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
      $arr_cate_job[$db_result[$db_loop]["cate_job_id"]] = $db_result[$db_loop]["cate_job_title"];
    }
  }
	
	foreach($_GET as $key => $value)
	{
		$$key = $common_connect->h($value);
	}
	
?>
<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>ショップデータ|ダッシュボード</title>

  <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
  <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link href="/masterdashboard/css/animate.css" rel="stylesheet">
  <link href="/masterdashboard/css/style.css" rel="stylesheet">
  <link href="/masterdashboard/css/plus.css" rel="stylesheet">
  <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
	<script type="text/javascript">
    $(function() {
			//チェックリストを解除する
      $('#check_off').click(function() {
        $('.check_del').prop('checked',false);
      });

      $('#option_new').click(function() {
        document.location.href = './list.php?order_name=regi_date&order=desc';
      });

      $('#option_evaluation').click(function() {
        document.location.href = './list.php?order_name=evaluation&order=desc';
      });

			
			
			//チェックリストを一括削除
      $('#check_delete').click(function() {
				if(confirm('削除しますか？'))
				{
					check_del_list = $('[class="check_del"]:checked').map(function(){
						//$(this)でjQueryオブジェクトが取得できる。val()で値をvalue値を取得。
						return $(this).val();
					}).get().join('&shop_id[]=');
					document.location.href = './shop_del.php?shop_id[]='+check_del_list;
				}
      });
			
    });
		
		//有料・無料切り替え
		function fnChangeSel(i, j) { 
			var result = confirm('変更しますか？'); 
			if(result){ 
				document.location.href = './shop_up.php?shop_id='+i+'&shop_view_level='+j;
			} 
		}
  </script>
</head>

<body>
<?
	//管理者チェック
	$common_connect -> Fn_admin_check();
?>
    <div id="wrapper">

<!--左ナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
<!--左ナビゲーションここまで -->

	<div id="page-wrapper" class="gray-bg">

<!--コンテンツ　ヘッダーナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
<!--コンテンツ　ヘッダーナビゲーションここまで -->


<!--コンテンツユーザーデータ-->

  <div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-10">
        <h2>登録店舗</h2>
        <?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/include/header_link.php"; ?>
      </div>

      <div class="col-lg-2">
      </div>
  </div>



  <div class="row"><!---->
      <div class="col-lg-12"><!---->
          <div class="ibox float-e-margins"><!---->

              <div class="ibox-title">
                  <h5>登録店舗一覧 </h5>
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                          <i class="fa fa-wrench"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-user">
                          <li><button id="check_off" type="button" class="btn btn-default btn-xs">チェックリストを解除する</button>
                          </li>
                          <li><button id="check_delete" type="button" class="btn btn-primary btn-xs">チェックリストを一括削除</button>
                          </li>
                      </ul>
                      <a class="close-link">
                          <i class="fa fa-times"></i>
                      </a>
                  </div><!--ibox-tools-->
              </div><!--ibox-title-->

              <div class="ibox-content"><!--第二ヘッダー-->

                  <div class="row">
<!--第二ヘッダー-->

<!--ソート機能-->
                      <div class="col-sm-3 m-b-xs">
                          <div data-toggle="buttons" class="btn-group">
                              <label class="btn btn-sm btn-white  <? if($order_name=="new" || $order_name=="") { echo " active ";}?>" id="option_new"> <input type="radio" name="order_name"> 新着順 </label>
                              <label class="btn btn-sm btn-white <? if($order_name=="evaluation") { echo " active ";}?>" id="option_evaluation"> <input type="radio" name="order_name">平均点数順</label>
                          </div>
                      </div>
<!--データワード検索-->
                      <form action="<?php echo $_SERVER['PHP_SELF']?>" method="get" name="form_shop_search">
                      <div class="col-sm-3">
                          <? $var = "s_cate_area_l_id";?>
                          <select name="<?php echo $var;?>" id="<?php echo $var;?>" class="form-control m-b" >
                          <option value="">お店や事務所のある場所はどこですか？</option>
                          <?
                          foreach($arr_cate_area_l as $key=>$value)
                          {
                          ?>
                          <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
                          <?
                          }
                          ?>
                          </select>
                      </div>
                      <div class="col-sm-3">
                        <? $var = "s_cate_job_id";?>
                        <select class="form-control m-b" name="<?php echo $var;?>" id="<?php echo $var;?>">
                            <option value="">お店の業種を一つ選んで下さい</option>
                            <?
                            foreach($arr_cate_job as $key=>$value)
                            {
                            ?>
                            <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
                            <?
                            }
                            ?>
                        </select>
                      </div>
                      <div class="col-sm-3">
                        <div class="input-group">

                          <? $var = "s_keyword";?>
                          <input name="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="店名又は管理ID" class="input-sm form-control"> 
                          <span class="input-group-btn">
                            <button type="submit" class="btn btn-sm btn-primary"> Go!</button> 
                          </span>
                        </div>
                      </div>
                      </form>

                  </div><!--row-->

                  <div class="table-responsive">
<?php
	$arr_shop_view_level = $common_shop ->Fn_shop_view_level();
	
	$view_count=20;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$arr_where = array();
	$var = "s_keyword";
	if($$var != "")
	{
		$arr_where[$var] = $$var;
	}
	
	$var = "s_flag_open";
	if($$var != "")
	{
		$arr_where[$var] = $$var;
	}

	//合計
	$all_count = 0;
  $sql = "SELECT shop_id FROM shop where 1 ".$where ;
  $arr_shop_all = $common_dao->db_query_bind($sql);
	$all_count = $arr_shop_all[0]["all_count"];
	
	//リスト表示
	$arr_data = array("shop_id", "shop_pw", "shop_name", "cate_area_s_id", "cate_job_id", "evaluation");
	$arr_data = array_merge($arr_data, array("email_kanri", "shop_view_level", "flag_open"));
	$arr_data = array_merge($arr_data, array("regi_date", "up_date"));
	

  if($s_keyword!="")
  {
    $where .= " and (shop_name collate utf8_unicode_ci like '%".$s_keyword."%' or shop_kana collate utf8_unicode_ci like '%".$s_keyword."%' or shop_title collate utf8_unicode_ci like '%".$s_keyword."%' or shop_id = '%".$s_keyword."%' ) ";
  }

  if($s_flag_open!="")
  {
    $where .= " and flag_open='".$s_flag_open."'";
  }

  if($s_cate_job_id!="")
  {
    $where .= " and cate_job_id='".$s_cate_job_id."'";
  }


  if($s_cate_area_l_id!="")
  {
    $where_area_s = "";
    foreach ($arr_cate_area_ids[$s_cate_area_l_id] as $key => $value) {
      $where_area_s .= " cate_area_s_id='".$key."' or ";
    }
    if($where_area_s!="")
    {
      $where .= " and (".rtrim($where_area_s, "or ").")";
    }
  }
  


  $sql = "SELECT ";
  foreach($arr_data as $val)
  {
    $sql .= $val.", ";
  }
  $sql .= " 1 FROM shop where 1 ".$where ;
  if($order_name != "")
  {
    $sql .= " order by ".$order_name." ".$order;
  }
  else
  {
    $sql .= " order by shop_view_level, up_date desc";
  }
  $sql .= " limit $offset,$view_count";

  $arr_shop_list = $common_dao->db_query_bind($sql);
	
	
$table_header = <<<EOF
          <thead><!--項目名-->
            <tr>
              <th>削除</th>
              <th>管理ID</th>
              <th>登録日</th>
              <th>店名</th>
              <th>エリア</th>
              <th>業種</th>
              <th>平均点</th>
              <th>口コミ数</th>
              <th>画像枚数</th>
              <th>ブログ数</th>
              <th>アクセス</th>
              <th>昨日</th>
              <th>一昨日</th>
              <th>有料</th>
              <th>調査</th>
              <th>表示</th>
              <th>編集</th>
            </tr>
          </thead>

EOF;
?>
				<table class="table table-striped table-bordered table-hover dataTables-example" >
        <?php echo $table_header;?>
          <tbody>
                                    
<?
  $db_loop = 0;
	foreach($arr_shop_list as $arr_key=>$arr_value)
	{
    $db_loop++;
		foreach($arr_value as $key=>$value)
		{
			$$key=$value;
		}
?>
          <tr>
            <td><input type="checkbox" class="check_del" value="<? echo $shop_id;?>"></td>
            <td><small><? echo $shop_id;?></small></td>
            <td><small><? echo substr($regi_date, 0, 10);?></small></td>
            <td><a href="./shop.php?shop_id=<? echo $shop_id;?>"><? echo $shop_name;?></a></td>
            <td><small><? echo $arr_cate_area[$cate_area_s_id];?></small></td>
            <td><small><? echo $arr_cate_job[$cate_job_id];?></small></td>
            <td><small><? if($evaluation==0){ echo "-.--";} else { echo sprintf('%.2f',$evaluation);}?>点</small></td>
            <?
            $result_voice = 0;
            $sql_voice = "SELECT count(voice_id) as count_voice FROM voice where shop_id='".$shop_id."' and flag_open='1' " ;
            $db_result_voice = $common_dao->db_query_bind($sql_voice);
            if($db_result_voice) { $result_voice = $db_result_voice[0]["count_voice"]; }
            ?>
            <td><small><? echo number_format($result_voice);?>件</small></td>
            <?
            //画像
            $result_voice_img = 0;
            $sql_voice_img = "SELECT count(voice_img_id) as count_voice_img FROM voice_img where shop_id='".$shop_id."' and flag_open='1' " ;
            $db_result_voice_img = $common_dao->db_query_bind($sql_voice_img);
            if($db_result_voice_img) { $result_voice_img = $db_result_voice_img[0]["count_voice_img"]; }
            ?>
            <td><small><? echo number_format($result_voice_img);?>枚</small></td>
            <?
            //グログ
            //投稿可能数
            $shop_blog_possible = 1;
            $sql = "SELECT c.blog_count FROM shop s inner join cate_shop c on s.shop_view_level=c.cate_shop_id where s.shop_id='".$shop_id."' ";
            $db_result = $common_dao->db_query_bind($sql);
            if($db_result)
            {
                $shop_blog_possible = $db_result[0]["blog_count"];
            }

            //登録可能件数
            $blog_count = 0;
            $sql = "SELECT blog_count FROM shop_blog_count where shop_id='".$shop_id."' and regi_date='".date("Y-m-d")."' ";
            $db_result = $common_dao->db_query_bind($sql);
            if($db_result)
            {
                $blog_count = $db_result["0"]["blog_count"];
            }
            ?>
            <td><small><? echo $blog_count;?>/<? echo $shop_blog_possible;?></small></td>
            <?
            //本日のアクセス数
            $s_pv = 0;
            $yyyymmdd = date("Y/m/d");
            $sql = "select (p_".date("d", strtotime($yyyymmdd)).") as s_pv from shop_pv where shop_id='".$shop_id."' and yyyymm='".date("Ym", strtotime($yyyymmdd))."' ";
            $db_result = $common_dao->db_query_bind($sql);
            if($db_result)
            {
              $s_pv = $db_result["0"]["s_pv"];
            }
            ?>
            <td><small><? echo $s_pv;?></small></td>
            <?
            //昨日のアクセス数
            $s_pv = 0;
            $yyyymmdd = date("Y/m/d", strtotime("-1 day"));
            $sql = "select (p_".date("d", strtotime($yyyymmdd)).") as s_pv from shop_pv where shop_id='".$shop_id."' and yyyymm='".date("Ym", strtotime($yyyymmdd))."' ";
            $db_result = $common_dao->db_query_bind($sql);
            if($db_result)
            {
              $s_pv = $db_result["0"]["s_pv"];
            }
            ?>
            <td><small><? echo $s_pv;?></small></td>
            <?
            //一昨日のアクセス数
            $s_pv = 0;
            $yyyymmdd = date("Y/m/d", strtotime("-2 day"));
            $sql = "select (p_".date("d", strtotime($yyyymmdd)).") as s_pv from shop_pv where shop_id='".$shop_id."' and yyyymm='".date("Ym", strtotime($yyyymmdd))."' ";
            $db_result = $common_dao->db_query_bind($sql);
            if($db_result)
            {
              $s_pv = $db_result["0"]["s_pv"];
            }
            ?>
            <td><small><? echo $s_pv;?></small></td>
            <td>	
              <div class="onoffswitch">
              <? /*30:有料,99:無料*/ ?>
                <a href="#" onClick='fnChangeSel("<? echo $shop_id;?>", "<? if($shop_view_level=="30") { echo "99";} else { echo "30";}?>");'><? if($shop_view_level=="30") { echo "<span style='color:#1ab394; font-weight:bold;'>".$arr_shop_view_level[30]."</span>";} else { echo $arr_shop_view_level[99];}?></a>
                
              </div>
            </td>
            <td><div class="onoffswitch"><input type="checkbox" checked class="onoffswitch-checkbox" id="example1"><label class="onoffswitch-label" for="example1"><span class="onoffswitch-inner"></span><span class="onoffswitch-switch"></span></label></div></td>
            <td>  
              <? /*1:表示,99:非表示*/ ?>
              <? if($flag_open=="99") { echo "<span style='color:#1ab394; font-weight:bold;'>非表示</span>";} else { echo "表示";}?>
            </td>
            <td><a href="./entry.php?shop_id=<? echo $shop_id;?>"><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button></a></td>
          </tr>
<?
			if($db_loop%10==9) { echo $table_header;}

	}
?>

        </tbody>
      </table>


      <div class="nextlist">
        <div class="btn-group">
					<?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
        </div>
      </div>                              		    


                  </div><!--table-responsive-->

              </div><!--ibox-content-->
          </div><!--ibox float-e-margins-->

      </div><!--col-lg-12-->

  </div><!--row-->





<!--フッター部分 -->
<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
<!--フッター部分 -->

</div>
</div>



    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/bootstrap.min.js"></script>
    <script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Peity -->
    <script src="/masterdashboard/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/masterdashboard/js/inspinia.js"></script>
    <script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>


    <!-- Peity -->
    <script src="/masterdashboard/js/demo/peity-demo.js"></script>


</body>

</html>
