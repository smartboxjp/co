<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>お店詳細ページ | ダッシュボード</title>

	<link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
	<link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="/masterdashboard/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="/masterdashboard/css/animate.css" rel="stylesheet">
	<link href="/masterdashboard/css/style.css" rel="stylesheet">
	<link href="/masterdashboard/css/plus.css" rel="stylesheet">
	<link href="/masterdashboard/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
	<link href="/masterdashboard/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

</head>

<body>

	<div id="wrapper">

		<!--左ナビゲーション -->
		<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
		<!--左ナビゲーションここまで -->


		<div id="page-wrapper" class="gray-bg">

			<!--ヘッダー-->
			<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/headerdetail.php"; ?>
			<!--ヘッダー-->




			<!--ユーザー情報右-->

			<div class="wrapper wrapper-content">
				<div class="row">
				<!--お店データ-->
					<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/shop/common/shopdeta.php"; ?>
				<!--お店データ-->



					<div class="col-lg-9 animated fadeInRight">


						<div class="ibox-title">
							<h5>調査結果を入力する※非公開</h5>
						<div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>

                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>

						</div>




						<div class="ibox-content m-b-sm border-bottom">

							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label" for="order_id">調査をした人</label>
										<input type="text" value="" class="form-control">
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label" for="date_added">調査日時</label>
										<div class="input-group date">
											<span class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</span>
											<input id="date_added" type="text" class="form-control" value="03/04/2014">
										</div>
									</div>
								</div>

								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label" for="date_added">面接時の時間</label>
										<div class="input-group clockpicker" data-autoclose="true">
											<span class="input-group-addon">
                                    		<span class="fa fa-clock-o"></span>
											</span>
											<input type="text" class="form-control" value="時刻">
										</div>


									</div>
								</div>

							</div>


							<div class="row">

								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label" for="status">面接場所</label>
										<select class="form-control m-b" name="account">
											<option>お店</option>
											<option>待機室</option>
											<option>喫茶店</option>
											<option>車の中</option>
											<option>写メ</option>
											<option>その他</option>
										</select>

									</div>
								</div>

								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label" for="date_modified">アドレス</label>
											<input type="text" value="" class="form-control">
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label" for="amount">電話番号</label>
										<input type="text" value="" class="form-control">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label" for="status">メールや電話の対応</label>
										<select class="form-control m-b" name="account">
											<option>とても丁寧</option>
											<option>丁寧</option>
											<option>普通</option>
											<option>悪い</option>
											<option>酷い</option>
											<option>判定不可</option>
										</select>

									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label" for="status">メールの返信スピード</label>
										<select class="form-control m-b" name="account">
											<option>すごく早い</option>
											<option>早い</option>
											<option>普通</option>
											<option>遅い</option>
											<option>酷い</option>
											<option>判定不可</option>
										</select>

									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label" for="amount">判明してる系列店</label>
										<input type="text" value="" class="form-control">
									</div>
								</div>
								<!----->
				

								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label" for="date_modified">問い合わせ媒体</label>
											<input type="text" value="" class="form-control">
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label" for="amount">問い合わせ方法</label>
										<select class="form-control m-b" name="account">
											<option>メール</option>
											<option>電話</option>
											<option>LINE</option>
											<option>紹介</option>
											<option>その他</option>
										</select>
									</div>
								</div>
							
								<!---->
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label" for="date_modified">備考</label>
											<textarea name="" class="form-control" rows="4" cols="40" placeholder="備考"></textarea>
									</div>
								</div>



								<div class="col-sm-12 col-sm-offset-9">
									<button class="btn btn-primary" type="submit">調査内容を保存</button>
								</div>
							</div>
							

						</div>





						<!-------------------------------->
						<div class="ibox-title">
							<h5>調査結果項目の追加</h5>
						</div>


						<div class="ibox-content m-b-sm border-bottom">



							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label" for="status">調査項目名</label>
										<input type="text" value="" class="form-control">
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label" for="status">調査項目内容</label>
										<input type="text" value="" class="form-control">
									</div>
								</div>


								<div class="col-sm-12 col-sm-offset-9">
									<button class="btn btn-primary" type="submit">テーブルに導入</button>
								</div>

							</div>




						</div>


						<!-------------------------------->

						<div class="row">
							<div class="col-lg-12">
								<div class="ibox float-e-margins">
									<div class="ibox-title">
										<h5>表示されてる調査結果</h5>

									</div>
									<div class="ibox-content">

										<table class="footable table table-stripped toggle-arrow-tiny" data-page-size="8">
											<thead>
												<tr>

													<th>並び替え</th>
													<th>追加項目</th>
													<th>追加内容</th>
													<th>削除</th>
													<th>表示/非表示</th>

												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														<div class="vote-actions">
															<a href="#">
                                            							<i class="fa fa-chevron-up"> </i>
                                        							</a>
															<div>1</div>
															<a href="#">
                                            							<i class="fa fa-chevron-down"></i>
                                        							</a>
														





														</div>
													</td>
													<td>
														<div class="mysrerytable">項目タイトル</div>
													</td>
													<td>
														<div class="mysrerytable">項目内容</div>
													</td>
													<td>
														<div class="vote-iconsmall mysrerytabletrash">
															<a href="#" class="text-muted" title="削除"><i class="fa fa-trash"></i></a>
														</div>
													</td>
													<td>
														<div class="mysrerytable">
															<div class="onoffswitch">
																<a href="#" onClick='fnChangeSel("test10", "30");'>表示</a>

															</div>
														</div>
													</td>
												</tr>
												<tr>
													<td>
														<div class="vote-actions">
															<a href="#">
                                            							<i class="fa fa-chevron-up"> </i>
                                        							</a>
															<div>2</div>
															<a href="#">
                                            							<i class="fa fa-chevron-down"></i>
                                        							</a>
														





														</div>
													</td>
													<td>
														<div class="mysrerytable">項目タイトル</div>
													</td>
													<td>
														<div class="mysrerytable">項目内容</div>
													</td>
													<td>
														<div class="vote-iconsmall mysrerytabletrash">
															<a href="#" class="text-muted" title="削除"><i class="fa fa-trash"></i></a>
														</div>
													</td>
													<td>
														<div class="mysrerytable">
															<div class="onoffswitch">
																<a href="#" onClick='fnChangeSel("test10", "30");'>非表示</a>

															</div>
														</div>
													</td>
												</tr>
												<tr>
													<td>
														<div class="vote-actions">
															<a href="#">
                                            							<i class="fa fa-chevron-up"> </i>
                                        							</a>
															<div>3</div>
															<a href="#">
                                            							<i class="fa fa-chevron-down"></i>
                                        							</a>
														





														</div>
													</td>
													<td>
														<div class="mysrerytable">項目タイトル</div>
													</td>
													<td>
														<div class="mysrerytable">項目内容</div>
													</td>
													<td>
														<div class="vote-iconsmall mysrerytabletrash">
															<a href="#" class="text-muted" title="削除"><i class="fa fa-trash"></i></a>
														</div>
													</td>
													<td>
														<div class="mysrerytable">
															<div class="onoffswitch">
																<a href="#" onClick='fnChangeSel("test10", "30");'>表示</a>

															</div>
														</div>
													</td>
												</tr>


											</tbody>

										</table>

									</div>
								</div>
							</div>
						</div>





					</div>
					<!--col-lg-9 animated fadeInRight-->



					<!--フッター部分 -->
					<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
					<!--フッター部分 -->

				</div>
			</div>




			<!-- Mainly scripts -->
			<script src="/masterdashboard/js/jquery-2.1.1.js"></script>
			<script src="/masterdashboard/js/bootstrap.min.js"></script>
			<script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
			<script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

			<!-- Custom and plugin javascript -->
			<script src="/masterdashboard/js/inspinia.js"></script>
			<script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>

			<!-- Clock picker -->
			<script src="/masterdashboard/js/plugins/clockpicker/clockpicker.js"></script>
			<script type="text/javascript">
				$( '.clockpicker' ).clockpicker();
			</script>

			<!-- Data picker -->
			<script src="/masterdashboard/js/plugins/datapicker/bootstrap-datepicker.js"></script>
			<script src="/masterdashboard/js/plugins/datapicker/datepicker-ja.min.js"></script>

			<script>
				$( '#date_added' ).datepicker( {
					format: 'yyyy/mm/dd',
					language: 'ja',
					autoclose: true,
					clearBtn: true,
				} );
				$( '#date_added' ).datepicker().datepicker( 'setDate', 'today' );
			</script>

			<!-- iCheck -->
			<script src="/masterdashboard/js/plugins/iCheck/icheck.min.js"></script>
			<script>
				$( document ).ready( function () {
					$( '.i-checks' ).iCheck( {
						checkboxClass: 'icheckbox_square-green',
						radioClass: 'iradio_square-green',
					} );
				} );
			</script>


</body>

</html>