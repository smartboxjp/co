<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>お店詳細ページ | ダッシュボード</title>

	<link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
	<link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="/masterdashboard/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="/masterdashboard/css/animate.css" rel="stylesheet">
	<link href="/masterdashboard/css/style.css" rel="stylesheet">
	<link href="/masterdashboard/css/plus.css" rel="stylesheet">
</head>

<body>

	<div id="wrapper">

		<!--左ナビゲーション -->
		<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
		<!--左ナビゲーションここまで -->


		<div id="page-wrapper" class="gray-bg">

			<!--ヘッダー-->
			<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/headerdetail.php"; ?>
			<!--ヘッダー-->


			<!--ユーザー情報右-->

			<div class="wrapper wrapper-content">
				<div class="row">

				<!--お店データ-->
					<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/shop/common/shopdeta.php"; ?>
				<!--お店データ-->


					<div class="col-lg-9 animated fadeInRight">


						<div class="mail-box-header">

						<h2>
                    		お店に投稿された口コミ
                		</h2>
						
							<div class="mail-tools tooltip-demo m-t-md">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-outline btn-info"><a href="list_user.html">口コミ一覧</a></button>

								</div>
								<button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="チェックリストをまとめて削除"><i class="fa fa-trash-o"></i> </button>

							</div>
						</div>

						<div class="mail-box">

							<table class="table table-hover table-mail">
								<tbody>
									<tr class="read">
										<td class="check-mail">
											<input type="checkbox" class="i-checks">
										</td>
										<td class="text-right mail-date">2017/08/25</td>
										<td class="text-right mail-date">08:25</td>
										<td class="mail-ontact"><a href="mail_detail.html">ユーザー名最大で8文字</a>
										</td>
									
										<td>3.25点</td>
										<td class="mail-subject"><a href="mail_detail.html">タイトル30文字</a>
										</td>
									
										<td>信憑性あり大</td>
									</tr>
									<tr class="read">
										<td class="check-mail">
											<input type="checkbox" class="i-checks">
										</td>
										<td class="text-right mail-date">2017/08/25</td>
										<td class="text-right mail-date">08:25</td>
										<td class="mail-ontact"><a href="mail_detail.html">ユーザー名最大で8文字</a>
										</td>
									
										<td>3.25点</td>
										<td class="mail-subject"><a href="mail_detail.html">タイトル30文字</a>
										</td>
									
										<td>信憑性あり大</td>
									</tr>
									<tr class="read">
										<td class="check-mail">
											<input type="checkbox" class="i-checks">
										</td>
										<td class="text-right mail-date">2017/08/25</td>
										<td class="text-right mail-date">08:25</td>
										<td class="mail-ontact"><a href="mail_detail.html">ユーザー名8文字まで</a>
										</td>
									
										<td>3.25点</td>
										<td class="mail-subject"><a href="mail_detail.html">口コミタイトル30文字までタイトル30文字までタイトル30文字まで</a>
										</td>
									
										<td>信憑性あり大</td>
									</tr>
									<tr class="read">
										<td class="check-mail">
											<input type="checkbox" class="i-checks">
										</td>
										<td class="text-right mail-date">2017/08/25</td>
										<td class="text-right mail-date">08:25</td>
										<td class="mail-ontact"><a href="mail_detail.html">ユーザー名最大で8文字</a>
										</td>
									
										<td>3.25点</td>
										<td class="mail-subject"><a href="mail_detail.html">タイトル30文字</a>
										</td>
									
										<td>信憑性あり大</td>
									</tr>
									<tr class="read">
										<td class="check-mail">
											<input type="checkbox" class="i-checks">
										</td>
										<td class="text-right mail-date">2017/08/25</td>
										<td class="text-right mail-date">08:25</td>
										<td class="mail-ontact"><a href="mail_detail.html">ユーザー名最大で8文字</a>
										</td>
									
										<td>3.25点</td>
										<td class="mail-subject"><a href="mail_detail.html">タイトル30文字</a>
										</td>
									
										<td>信憑性あり大</td>
									</tr>


								</tbody>
							</table>

						</div>




						<div class="mail-box-header">

							<h2>
                    お店に投稿された画像
                </h2>
						
							<div class="mail-tools tooltip-demo m-t-md">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-outline btn-info"><a href="list_user.html">画像一覧</a></button>

								</div>
								<button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="チェックリストをまとめて削除"><i class="fa fa-trash-o"></i> </button>

							</div>
						</div>

						<div class="mail-box">

							<table class="table table-hover table-mail">
								<tbody>
									<tr class="read">
										<td class="check-mail">
											<input type="checkbox" class="i-checks">
										</td>
										<td><img src="/masterdashboard/img/gallery/123.jpg" width="50px" height="50px">
										</td>
										<td class="text-right mail-date">2017/08/25</td>
										<td class="text-right mail-date">08:25</td>
										<td class="mail-ontact"><a href="mail_detail.html">投稿ユーザーの名前</a>
										</td>
										<td class="mail-subject"><a href="mail_detail.html">コメント</a>
										</td>
									</tr>
									<tr class="read">
										<td class="check-mail">
											<input type="checkbox" class="i-checks">
										</td>
										<td><img src="/masterdashboard/img/gallery/123.jpg" width="50px" height="50px">
										</td>
										<td class="text-right mail-date">2017/08/25</td>
										<td class="text-right mail-date">08:25</td>
										<td class="mail-ontact"><a href="mail_detail.html">投稿ユーザーの名前</a>
										</td>
										<td class="mail-subject"><a href="mail_detail.html">コメント最大で10文字</a>
										</td>
									</tr>
									<tr class="read">
										<td class="check-mail">
											<input type="checkbox" class="i-checks">
										</td>
										<td><img src="/masterdashboard/img/gallery/123.jpg" width="50px" height="50px">
										</td>
										<td class="text-right mail-date">2017/08/25</td>
										<td class="text-right mail-date">08:25</td>
										<td class="mail-ontact"><a href="mail_detail.html">投稿ユーザーの名前</a>
										</td>
										<td class="mail-subject"><a href="mail_detail.html">コメント</a>
										</td>
									</tr>
									<tr class="read">
										<td class="check-mail">
											<input type="checkbox" class="i-checks">
										</td>
										<td><img src="/masterdashboard/img/gallery/123.jpg" width="50px" height="50px">
										</td>
										<td class="text-right mail-date">2017/08/25</td>
										<td class="text-right mail-date">08:25</td>
										<td class="mail-ontact"><a href="mail_detail.html">投稿ユーザーの名前</a>
										</td>
										<td class="mail-subject"><a href="mail_detail.html">コメント</a>
										</td>
									</tr>
									<tr class="read">
										<td class="check-mail">
											<input type="checkbox" class="i-checks">
										</td>
										<td><img src="/masterdashboard/img/gallery/123.jpg" width="50px" height="50px">
										</td>
										<td class="text-right mail-date">2017/08/25</td>
										<td class="text-right mail-date">08:25</td>
										<td class="mail-ontact"><a href="mail_detail.html">投稿ユーザーの名前</a>
										</td>
										<td class="mail-subject"><a href="mail_detail.html">コメント</a>
										</td>
									</tr>


								</tbody>
							</table>

						</div>



						<div class="mail-box-header">

							<h2>
                    お店が投稿したブログ
                </h2>
						
							<div class="mail-tools tooltip-demo m-t-md">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-outline btn-info"><a href="list_user.html">ブログ一覧</a></button>

								</div>
								<button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="チェックリストをまとめて削除"><i class="fa fa-trash-o"></i> </button>

							</div>
						</div>

						<div class="mail-box">

							<table class="table table-hover table-mail">
								<tbody>
									<tr class="read">
										<td class="check-mail">
											<input type="checkbox" class="i-checks">
										</td>
										<td class="text-right mail-date">2017/08/25</td>
										<td class="text-right mail-date">08:25</td>
										<td class="mail-ontact"><a href="mail_detail.html">ブログタイトル</a>
										</td>
										<td class="mail-subject"><a href="mail_detail.html">ブログ本文の一部がここに表示されます。ブログ本文の一部が表示されます</a>
										</td>
									
										<td>2/3</td>
									
										<td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button>
										</td>
									</tr>
									<tr class="read">
										<td class="check-mail">
											<input type="checkbox" class="i-checks">
										</td>
										<td class="text-right mail-date">2017/08/25</td>
										<td class="text-right mail-date">08:25</td>
										<td class="mail-ontact"><a href="mail_detail.html">ブログタイトル</a>
										</td>
										<td class="mail-subject"><a href="mail_detail.html">ブログ本文の一部がここに表示されます。ブログ本文の一部が表示されます</a>
										</td>
									
										<td>2/3</td>
									
										<td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button>
										</td>
									</tr>
									<tr class="read">
										<td class="check-mail">
											<input type="checkbox" class="i-checks">
										</td>
										<td class="text-right mail-date">2017/08/25</td>
										<td class="text-right mail-date">08:25</td>
										<td class="mail-ontact"><a href="mail_detail.html">ブログタイトル</a>
										</td>
										<td class="mail-subject"><a href="mail_detail.html">ブログ本文の一部がここに表示されます。ブログ本文の一部が表示されます</a>
										</td>
									
										<td>2/3</td>
									
										<td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button>
										</td>
									</tr>
									<tr class="read">
										<td class="check-mail">
											<input type="checkbox" class="i-checks">
										</td>
										<td class="text-right mail-date">2017/08/25</td>
										<td class="text-right mail-date">08:25</td>
										<td class="mail-ontact"><a href="mail_detail.html">ブログタイトル</a>
										</td>
										<td class="mail-subject"><a href="mail_detail.html">ブログ本文の一部がここに表示されます。ブログ本文の一部が表示されます</a>
										</td>
									
										<td>2/3</td>
									
										<td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button>
										</td>
									</tr>
									<tr class="read">
										<td class="check-mail">
											<input type="checkbox" class="i-checks">
										</td>
										<td class="text-right mail-date">2017/08/25</td>
										<td class="text-right mail-date">08:25</td>
										<td class="mail-ontact"><a href="mail_detail.html">ブログタイトル</a>
										</td>
										<td class="mail-subject"><a href="mail_detail.html">ブログ本文の一部がここに表示されます。ブログ本文の一部が表示されます</a>
										</td>
									
										<td>2/3</td>
									
										<td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button>
										</td>
									</tr>


								</tbody>
							</table>

						</div>








						<div class="mail-box-header">
							<!--動画-->
							<h2>
                    	投稿動画
                	</h2>
						
							<div class="mail-tools tooltip-demo m-t-md">

								<button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="動画削除"><i class="fa fa-trash-o"></i> </button>
							</div>
						</div>
						<div class="mail-box-header">
							<p>
								お店側で動画のコメントが最大で30文字まで投稿できます。お店側で動画のコメントが最大で30文字まで投稿できます。お店側で動画のコメントが最大で30文字まで投稿できます。お店側で動画のコメントが最大で30文字まで投稿できます。お店側で動画のコメントが最大で30文字まで投稿できます。お店側で動画のコメントが最大で30文字まで投稿できます。
							</p>
							<button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="コメント編集"><i class="fa fa-pencil"></i> </button>
						</div>

						<div class="mail-box">
						
							<div class="mail-body">

								<figure>
									<iframe width="425" height="349" src="http://www.youtube.com/embed/bwj2s_5e12U" frameborder="0" allowfullscreen></iframe>
								</figure>

							</div>
						</div>
						<!--動画-->





					</div>
					<!--col-lg-9 animated fadeInRight-->



					<!--フッター部分 -->
					<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
					<!--フッター部分 -->

				</div>
			</div>




			<!-- Mainly scripts -->
			<script src="/masterdashboard/js/jquery-2.1.1.js"></script>
			<script src="/masterdashboard/js/bootstrap.min.js"></script>
			<script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
			<script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

			<!-- Custom and plugin javascript -->
			<script src="/masterdashboard/js/inspinia.js"></script>
			<script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>



			<!-- iCheck -->
			<script src="/masterdashboard/js/plugins/iCheck/icheck.min.js"></script>
			<script>
				$( document ).ready( function () {
					$( '.i-checks' ).iCheck( {
						checkboxClass: 'icheckbox_square-green',
						radioClass: 'iradio_square-green',
					} );
				} );
			</script>


</body>

</html>