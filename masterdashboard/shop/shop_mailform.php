<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>お店詳細ページ | ダッシュボード</title>

	<link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
	<link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="/masterdashboard/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="/masterdashboard/css/animate.css" rel="stylesheet">
	<link href="/masterdashboard/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
	<link href="/masterdashboard/css/style.css" rel="stylesheet">
	<link href="/masterdashboard/css/plus.css" rel="stylesheet">
</head>

<body>

	<div id="wrapper">

		<!--左ナビゲーション -->
		<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
		<!--左ナビゲーションここまで -->


		<div id="page-wrapper" class="gray-bg">

			<!--ヘッダー-->
			<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/headerdetail.php"; ?>
			<!--ヘッダー-->


			<!--ユーザー情報右-->

			<div class="wrapper wrapper-content">
				<div class="row">

				<!--お店データ-->
					<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/shop/common/shopdeta.php"; ?>
				<!--お店データ-->


					<div class="col-lg-9 animated fadeInRight">


            			<div class="mail-box-header">

                			
                			<h2>
                    		メールを送信する店名が最大で20文字まで表示されます。
                			</h2>
            			</div>
            			
            			
                		<div class="mail-box">
                			<div class="mail-body">

                    			<form class="form-horizontal" method="get">
                        		<div class="form-group"><label class="col-sm-2 control-label">送信先:</label>

                            		<div class="col-sm-10"><input type="text" class="form-control" value="お店の登録アドレスが表示される"></div>
                        		
                        		</div>
                        		
                        		<div class="form-group"><label class="col-sm-2 control-label">タイトル:</label>

                            		<div class="col-sm-10"><input type="text" class="form-control" value=""></div>
                        		</div>
                        		</form>

                			</div>

                    		<div class="mail-text h-200">

                        		<div class="summernote">
                            		<h3>コソットです。</h3>
                            		dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the industry's</strong> standard dummy text ever since the 1500s,
                            		when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                            		typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with
                            		<br/>
                            		<br/>

                        		</div>
								<div class="clearfix"></div>
                        	</div>
                        	
                    		<div class="mail-body text-right tooltip-demo">
                        		<a href="mailbox.html" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="メールを送信"><i class="fa fa-reply"></i> 
                        		送信する</a>
                        		<a href="mailbox.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="メールを削除"><i class="fa fa-trash-o"></i> 削除する</a>
                        		
                    		</div>
                    		
                    		
                    		<div class="clearfix"></div>
					  </div>



					</div>
					<!--col-lg-9 animated fadeInRight-->



					<!--フッター部分 -->
					<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
					<!--フッター部分 -->

				</div>
			</div>




			<!-- Mainly scripts -->
			<script src="/masterdashboard/js/jquery-2.1.1.js"></script>
			<script src="/masterdashboard/js/bootstrap.min.js"></script>
			<script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
			<script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

			<!-- Custom and plugin javascript -->
			<script src="/masterdashboard/js/inspinia.js"></script>
			<script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>


			<!-- iCheck -->
			<script src="/masterdashboard/js/plugins/iCheck/icheck.min.js"></script>
			<script>
				$( document ).ready( function () {
					$( '.i-checks' ).iCheck( {
						checkboxClass: 'icheckbox_square-green',
						radioClass: 'iradio_square-green',
					} );
				} );
			</script>
			
			<!-- SUMMERNOTE -->
    <script src="/masterdashboard/js/plugins/summernote/summernote.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });


            $('.summernote').summernote();

        });
        var edit = function() {
            $('.click2edit').summernote({focus: true});
        };
        var save = function() {
            var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
            $('.click2edit').destroy();
        };

    </script>


</body>

</html>