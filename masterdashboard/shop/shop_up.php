<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>店舗変更</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	foreach($_GET as $key => $value)
	{ 
		//$$key = trim($common_dao->db_string_escape($value));
		$$key = $value;
	}
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	if($shop_id=="" || $shop_view_level=="")
	{
		$common_connect -> Fn_javascript_back("正しく選択してください。");
	}

	$arr_data = array();
	$arr_data["shop_view_level"] = $shop_view_level;
	
	$arr_where = array();
	$arr_where["shop_id"] = $shop_id;
	//30:有料,99:無料
	$db_result = $common_shop->Fn_shop_update ($arr_data, $arr_where);

	
	$common_connect-> Fn_javascript_move("修正しました", "list.php");
?>
</body>
</html>