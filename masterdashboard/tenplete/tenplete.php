<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | File Upload</title>

    <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="/masterdashboard/css/animate.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/lity/lity.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/masterdashboard/css/style.css" rel="stylesheet">
    <link href="/masterdashboard/css/plus.css" rel="stylesheet">

</head>

<body>

<div id="wrapper">

   

<!--左ナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
<!--左ナビゲーションここまで -->

	<div id="page-wrapper" class="gray-bg">


        <!--コンテンツ　ヘッダーナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
  
	<!--コンテンツ　ヘッダーナビゲーションここまで -->



	<!--コンテンツヘッダー -->
		<div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-lg-10">

	
			</div>
			<div class="col-lg-2">

                	</div>
		</div>
	<!--コンテンツヘッダーここまで -->

	<!--コンテンツここから -->
		<div class="wrapper wrapper-content  animated fadeInRight">
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox ">
						<div class="ibox-title">
							<h5>Sweet Alert</h5>
						</div>
						<div class="ibox-content">
						</div>
					</div>
				</div>
			</div>
		</div>
	<!--コンテンツここまで -->

<!--フッター部分 -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
<!--フッター部分 -->

        </div>

   </div>



    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
    <script src="/masterdashboard/js/bootstrap.min.js"></script>
    <script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/masterdashboard/js/inspinia.js"></script>
    <script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>

    <!-- SUMMERNOTE -->
    <script src="/masterdashboard/js/plugins/summernote/summernote.min.js"></script>

    <!-- Dropzone -->
    <script src="/masterdashboard/js/plugins/dropzone/dropzone.js"></script>

    <!-- lity -->
    <script src="/masterdashboard/js/plugins/lity/lity.js"></script>


</body>

</html>
