<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
    
    if($topics_id!="")
    {
        $where = " and topics_id='".$topics_id."'";
        //リスト表示
        $arr_db_field = array( "topics_id", "topics_link", "topics_title", "topics_comment", "img_1", "flag_top" );
        $arr_db_field = array_merge( $arr_db_field, array("flag_open", "regi_date", "up_date"));
        
        $sql = "SELECT ";
        foreach($arr_db_field as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM topics ";
        $sql .= " where 1 ".$where ;
        

        $arr_list = $common_dao->db_query_bind($sql);
        if($arr_list)
        {
            for($db_loop=0 ; $db_loop < count($arr_list) ; $db_loop++)
            {
              foreach($arr_db_field as $val)
              {
                $$val = $arr_list[$db_loop][$val];
              }
            }
        }
    }
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | File Upload</title>

    <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="/masterdashboard/css/animate.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/lity/lity.css" rel="stylesheet">
    <link href="/masterdashboard/css/style.css" rel="stylesheet">
    <link href="/masterdashboard/css/plus.css" rel="stylesheet">
  <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
<script type="text/javascript">
    $(function() {
            
        $('#form_confirm').click(function() {
            err_default = "";
            err_check_count = 0;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";
            
            err_check_count += check_input("topics_link");
            
            if(err_check_count!=0)
            {
                alert("入力に不備があります");
                return false;
            }
            else
            {
                //$('#form_confirm').submit();
                $('#form_confirm', "body").submit();
                return true;
            }
            
            
        });
                
        function check_input($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);
            
            if($('#'+$str).val()=="")
            {
                err ="<span class='errorForm'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }

    });
    
//-->
</script>
<script language="javascript"> 
    function fnChangeSel(i) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './topics_del.php?topics_id='+i;
        } 
    }
    
    function fnImgDel(i, j, k) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './img_one_del.php?topics_id='+i+'&img='+j+'&img_name='+k;
        } 
    }
</script>

</head>

<body>
<?
    //管理者チェック
    $common_connect -> Fn_admin_check();
?>

    <div id="wrapper">



        <!--左ナビゲーション -->
        <?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
        <!--左ナビゲーションここまで -->

        <div id="page-wrapper" class="gray-bg">


            <!--コンテンツ　ヘッダーナビゲーション -->
            <?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>

            <!--コンテンツ　ヘッダーナビゲーションここまで -->



            <!--コンテンツここから -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>ホームページ内広告コントロール(PCページ)</h2>
                    <ol class="breadcrumb">

                        <li class="active">
                            <strong>ランダム広告</strong>
                        </li>

                        <li>
                            <a href="/masterdashboard/pr/imgup_fixing.php">固定広告</a>
                        </li>


                        <li>
                            <a href="/masterdashboard/pr/imgup_flag.php">トップページ背景広告</a>
                        </li>

                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeIn">
                <div class="row">

                    <!--第一ブロック-->
                    <div class="col-lg-12">

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>メインページコンテンツセンター(最大5枚までランダム表示)</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                      <i class="fa fa-chevron-up"></i>
                    </a>
                                
                                    <a class="close-link">
                      <i class="fa fa-times"></i>
                    </a>
                                
                                </div>
                            </div>


                            <div class="ibox-content">

                                <!--ここから-->
                                <form class="form-inline" enctype="multipart/form-data" action="./topics_save.php" name="form" method="post">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-stripped">
                                            <tr>
                                                <th>
                                                    登録ID
                                                </th>
                                                <td>
                                                    <? $var = "topics_id";?>
                                                    <?
                                                    if(isset($$var)) 
                                                    {
                                                    ?>
                                                        <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
                                                    <?
                                                        echo $$var;
                                                    } 
                                                    else
                                                     { echo "自動生成";}
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    表示されてる画像
                                                </th>
                                                <td>
                                                  <div class="form-group" style="width:200px;">
                                                    <div class="input-group">
                                                      <? $var = "img_1";?>  
                                                      <input type="file" id="<? echo $var;?>" name="<? echo $var;?>" style="display: none;" accept="image/*">
                                                      <input type="hidden" name="text_<?=$var;?>" value="<?=$$var;?>">
                                                      <span class="input-group-btn">
                                                      <button class="btn btn-default" type="button" onclick="$('#<? echo $var;?>').click();"><i class="glyphicon glyphicon-folder-open"></i></button>
                                                      </span>
                                                    
                                                      <div class="input-group">
                                                        <input id="dummy_file" type="text" class="form-control" placeholder="select file..." disabled>
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <br />
                                                  <?
                                                    if($$var!="") {
                                                        echo "<a href='/".global_topics_dir.$topics_id."/".$$var."' data-lity='data-lity'><img src='/".global_topics_dir.$topics_id."/".$$var."?d=".date(his)."' class='primgrandom'></a>";
                                                    }
                                                    ?>
                                                    <label id="err_<?php echo $var;?>"></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    タイトル
                                                </th>
                                                <td>
                                                <? $var = "topics_title";?>  
                                                  <input id="<? echo $var;?>" name="<? echo $var;?>" value="<? echo $$var;?>" type="text" class="form-control" style="width: 100%;" placeholder="タイトル">
                                                  <label id="err_<?php echo $var;?>"></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    コメント
                                                </th>
                                                <td>
                                                <? $var = "topics_comment";?>  
                                                  <textarea id="<? echo $var;?>" name="<? echo $var;?>" type="text" class="form-control" style="width: 100%;" placeholder="コメント"><? echo $$var;?></textarea>
                                                  <label id="err_<?php echo $var;?>"></label>
                                                </td>
                                            </tr>
                      
                                            <tr>
                                                <th>
                                                    登録されてるリンク先
                                                </th>
                                                <td>
                                                <? $var = "topics_link";?>  
                                                  <input id="<? echo $var;?>" name="<? echo $var;?>" value="<? echo $$var;?>" type="text" class="form-control" style="width: 100%;" placeholder="http://www.cossot.com">
                                                  <label id="err_<?php echo $var;?>"></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    位置
                                                </th>
                                                <td>
                                                <? $var = "flag_top";?> 
                                                <?
                                                    $arr_flag_top[0] = "リスト";
                                                    $arr_flag_top[1] = "トップの上";
                                                ?>
                                                  <select class="form-control m-b" name="<? echo $var;?>" id="<? echo $var;?>">
                                                  <? foreach($arr_flag_top as $key=>$value) { ?>
                                                    <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
                                                  <? } ?>
                                                  </select>
                                                  <label id="err_<?php echo $var;?>"></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    公開有無
                                                </th>
                                                <td>
                                                <? $var = "flag_open";?> 
                                                <?
                                                    $arr_flag_open[1] = "公開";
                                                    $arr_flag_open[0] = "非公開";
                                                ?>
                                                  <select class="form-control m-b" name="<? echo $var;?>" id="<? echo $var;?>">
                                                  <? foreach($arr_flag_open as $key=>$value) { ?>
                                                    <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
                                                  <? } ?>
                                                  </select>
                                                  <label id="err_<?php echo $var;?>"></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align:center;">
                                                  <div class="form-group">
                                                    <? $var = "form_confirm";?>
                                                    <button class="btn btn-primary" type="submit" id="<? echo $var;?>">決定</button>
                                                  </div>
                                                </td>
                                            </tr>
                                    </table>
                                </form>
                                <script type="text/javascript">
                                    $( function () {
                                        $( '#img_1' ).change( function () {
                                            $( '#dummy_file' ).val( $( this ).val() );
                                        } );
                                    } )
                                </script>

                                </div>

                            </div>
                            <!--ibox-content-->


                        </div>
                        <!--ibox float-e-margins-->
                    </div>
                    <!--col-lg-12-->


                    <div class="col-lg-12">

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>リスト</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                      <i class="fa fa-chevron-up"></i>
                    </a>
                                
                                    <a class="close-link">
                      <i class="fa fa-times"></i>
                    </a>
                                
                                </div>
                            </div>


                            <div class="ibox-content">
                                <!--ここから-->
                                <div class="table-responsive">
                                    <table class="table table-bordered table-stripped">
                                        <thead>
                                            <tr>

                                                <th>
                                                    登録順番
                                                </th>

                                                <th>
                                                    表示されてる画像
                                                </th>
                                                <th>
                                                    登録されてるリンク先
                                                </th>
                                                <th>
                                                    タイトル
                                                </th>
                                                <th>
                                                    位置
                                                </th>
                                                <th>
                                                    公開
                                                </th>
                                                <th>
                                                    削除
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?

        $where = "";
        $arr_db_field = array( "topics_id", "topics_link", "topics_title", "topics_comment", "img_1", "flag_top" );
        $arr_db_field = array_merge( $arr_db_field, array("flag_open", "regi_date", "up_date"));
        
        $sql = "SELECT ";
        foreach($arr_db_field as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM topics ";
        $sql .= " where 1 ".$where ;

        $arr_list = $common_dao->db_query_bind($sql);
        if($arr_list)
        {
            for($db_loop=0 ; $db_loop < count($arr_list) ; $db_loop++)
            {
              foreach($arr_db_field as $val)
              {
                $$val = $arr_list[$db_loop][$val];
              }
?>
                                            <tr>
                                                <td>
                                                    <? echo $topics_id;?>
                                                </td>
                                                <td>
                                                <?
                                                    $var = "img_1";
                                                    if($$var!="") {
                                                        echo "<a href='/".global_topics_dir.$topics_id."/".$$var."' data-lity='data-lity'><img src='/".global_topics_dir.$topics_id."/".$$var."?d=".date(his)."' class='topicsimgrandom'></a>";
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" value="<? echo $topics_link;?>">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled="" placeholder="<? echo $topics_title;?>">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled="" placeholder="<? echo $arr_flag_top[$flag_top];?>">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled="" placeholder="<? echo $arr_flag_open[$flag_open];?>">
                                                </td>
                                                <td>
                                                    <button class="btn btn-white" title="削除" onClick="fnChangeSel('<?php echo $topics_id;?>')"><i class="fa fa-trash"></i> </button>
                                                    <a href="?topics_id=<?php echo $topics_id;?>">修正</a>
                                                </td>
                                            </tr>
<?
        }
    }
?>

                                        </tbody>

                                    </table>



                                </div>

                            </div>
                            <!--ibox-content-->


                        </div>
                        <!--ibox float-e-margins-->
                    </div>
                    <!--col-lg-12-->


                </div>
                <!--row-->


            </div>


            <!--フッター部分 -->
            <?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
            <!--フッター部分 -->

        </div>
    </div>



    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
    <script src="/masterdashboard/js/bootstrap.min.js"></script>
    <script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/masterdashboard/js/inspinia.js"></script>
    <script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>

    <!-- DROPZONE -->
    <script src="/masterdashboard/js/plugins/dropzone/dropzone.js"></script>

    <!-- lity -->
    <script src="/masterdashboard/js/plugins/lity/lity.js"></script>

    <script>
        $( document ).ready( function () {

            Dropzone.options.myAwesomeDropzone = {

                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 100,
                maxFiles: 100,

                // Dropzone settings
                init: function () {
                    var myDropzone = this;

                    this.element.querySelector( "button[type=submit]" ).addEventListener( "click", function ( e ) {
                        e.preventDefault();
                        e.stopPropagation();
                        myDropzone.processQueue();
                    } );
                    this.on( "sendingmultiple", function () {} );
                    this.on( "successmultiple", function ( files, response ) {} );
                    this.on( "errormultiple", function ( files, response ) {} );
                }

            }

        } );
    </script>

</body>

</html>