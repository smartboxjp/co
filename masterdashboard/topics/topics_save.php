<?php
    //error_reporting(0);
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    $common_image = new CommonImage(); //画像
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php   

    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
    //管理者チェック
    $common_connect -> Fn_admin_check();
    

    if($topics_title == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力してください。");
    }
    
    $datetime = date("Y/m/d H:i:s");
    $user_agent = substr(getenv("HTTP_USER_AGENT"), 0, 250);
    
    //array
    $arr_db_field = array("topics_link", "topics_title", "topics_comment", "flag_top" );
    $arr_db_field = array_merge( $arr_db_field, array("flag_open"));
    
    //基本情報
    if($topics_id=="")
    {   
        $db_insert = "insert into topics ( ";
        $db_insert .= " topics_id, ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val.", ";
        }
        $db_insert .= " regi_date, up_date ";
        $db_insert .= " ) values ( ";
        $db_insert .= " '', ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= " '".$$val."', ";
        }
        $db_insert .= " '$datetime', '$datetime')";
    }
    else
    {
        $db_insert = "update topics set ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val."='".$$val."', ";
        }
        $db_insert .= " up_date='".$datetime."' ";
        $db_insert .= " where topics_id='".$topics_id."'";
    }
    $common_dao->db_update($db_insert);
                    
    if ($topics_id == "")
    {
        //自動生成されてるID出力
        $sql = "select last_insert_id() as last_id";  
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            $topics_id = $db_result[0]["last_id"];
        }
    }

    //Folder生成
    $save_dir = $global_path.global_topics_dir.$topics_id."/";
    $common_image -> create_folder ($save_dir);

    
    $new_end_name="_1";
    $fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $topics_id, $text_img_1, "");
    
    $db_insert = "update topics set ";
    $db_insert .= " img_1='".$fname_new_name[1]."', ";
    $db_insert .= " up_date='".$datetime."' ";
    $db_insert .= " where topics_id='".$topics_id."'";
    $common_dao->db_update($db_insert);

    
    $common_connect-> Fn_javascript_move("登録・修正しました", "list.php?topics_id=".$topics_id);
?>
</body>
</html>