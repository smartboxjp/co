<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonVoice.php";
	$common_voice = new CommonVoice();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_catearea = new CommonCateArea();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateJob.php";
	$common_catejob = new CommonCateJob();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
	
	//エリア
	$arr_area = $common_catearea->Fn_cate_area_list($sql);
	if(!is_null($arr_area))
	{
		foreach($arr_area as $arr_key=>$arr_value)
		{
			foreach($arr_value as $key=>$value)
			{
				$arr_cate_area_s_id[$arr_value["cate_area_s_id"]] = $arr_value["cate_area_l_title"];
			}
		}
	}
	
	//職種
	$arr_job = $common_catejob->Fn_db_cate_job_all();
	if(!is_null($arr_job))
	{
		foreach($arr_job as $arr_key=>$arr_value)
		{
			foreach($arr_value as $key=>$value)
			{
				$arr_cate_job_id[$arr_value["cate_job_id"]] = $arr_value["cate_job_title"];
			}
		}
	}
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>口コミデータ|ダッシュボード</title>

    <link href="/masterdashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/masterdashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/masterdashboard/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/masterdashboard/css/animate.css" rel="stylesheet">
    <link href="/masterdashboard/css/style.css" rel="stylesheet">
    <link href="/masterdashboard/css/plus.css" rel="stylesheet">
  <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
	<script type="text/javascript">
    $(function() {
			//チェックリストを解除する
      $('#check_off').click(function() {
        $('.check_del').prop('checked',false);
      });
			
			
			//チェックリストを一括削除
      $('#check_delete').click(function() {
				if(confirm('削除しますか？'))
				{
					check_dell_list = $('[class="check_del"]:checked').map(function(){
						//$(this)でjQueryオブジェクトが取得できる。val()で値をvalue値を取得。
						return $(this).val();
					}).get().join('&voice_id[]=');
					document.location.href = './voice_del.php?voice_id[]='+check_dell_list;
				}
      });
			
			//ランキングを一括修正
      $('#check_ranking').click(function() {
				if(confirm('ランクを修正しますか？'))
				{
					check_up_list = $('.check_rank option:selected').map(function(){
						//$(this)でjQueryオブジェクトが取得できる。val()で値をvalue値を取得。
						return $(this).val();
					}).get().join('');
					
					document.location.href = './voice_rank_up.php?'+check_up_list;
				}
      });
			
			
    });
		
		//有料・無料切り替え
		function fnChangeSel(i, j) { 
			var result = confirm('変更しますか？'); 
			if(result){ 
				document.location.href = './voice_up.php?voice_id='+i+'&flag_open='+j;
			} 
		}
  </script>
</head>

<body>
<?
	//管理者チェック
	$common_connect -> Fn_admin_check();
?>
    <div id="wrapper">

<!--左ナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/navi.php"; ?>
<!--左ナビゲーションここまで -->

        <div id="page-wrapper" class="gray-bg">

<!--コンテンツ　ヘッダーナビゲーション -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/header.php"; ?>
<!--コンテンツ　ヘッダーナビゲーションここまで -->
        

<!--コンテンツユーザーデータ-->

  <div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-10">
        <h2>登録口コミ</h2>
        <?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/include/header_link.php"; ?>
      </div>

      <div class="col-lg-2">
      </div>
  </div>



  <div class="row"><!---->
      <div class="col-lg-12"><!---->
          <div class="ibox float-e-margins"><!---->

              <div class="ibox-title">
                  <h5>口コミ一覧 </h5>
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                          <i class="fa fa-wrench"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-user">
                        <li><button id="check_off" type="button" class="btn btn-default btn-xs">チェックリストを解除する</button>
                        </li>
                        <li><button id="check_delete" type="button" class="btn btn-primary btn-xs">チェックリストを一括削除</button>
                        </li>
                        <li><button id="check_ranking" type="button" class="btn btn-primary btn-xs">信憑性の変更を適用する</button>
                        </li>
                      </ul>
                      <a class="close-link">
                          <i class="fa fa-times"></i>
                      </a>
                  </div><!--ibox-tools-->
              </div><!--ibox-title-->

            <div class="ibox-content"><!--第二ヘッダー-->
    
                <div class="row">
				<!--第二ヘッダー-->

				<!--ソート機能-->
                    <div class="col-sm-8 m-b-xs">
                        <div data-toggle="buttons" class="btn-group">
                            <label class="btn btn-sm btn-white <? if($order_name=="regi_date" || $order_name==""){ echo " active ";}?>" onClick="location.href='<? echo $_SERVER['PHP_SELF'];?>?order_name=regi_date'"> <input type="radio" id="option1" name="options"> 新着順 </label>
                            <label class="btn btn-sm btn-white <? if($order_name=="cate_area_s_id"){ echo " active ";}?>" onClick="location.href='<? echo $_SERVER['PHP_SELF'];?>?order_name=cate_area_s_id'"> <input type="radio" id="option2" name="options"> 地域順 </label>
                            <label class="btn btn-sm btn-white <? if($order_name=="ranking"){ echo " active ";}?>" onClick="location.href='<? echo $_SERVER['PHP_SELF'];?>?order_name=ranking&order=desc'"> <input type="radio" id="option3" name="options"> 信憑性の高い順</label>
<label class="btn btn-sm btn-white <? if($order_name=="voice_star"){ echo " active ";}?>" onClick="location.href='<? echo $_SERVER['PHP_SELF'];?>?order_name=voice_star&order=desc'"> <input type="radio" id="option2" name="options">平均点数順</label>
                            <label class="btn btn-sm btn-white <? if($order_name=="cate_job_id"){ echo " active ";}?>" onClick="location.href='<? echo $_SERVER['PHP_SELF'];?>?order_name=cate_job_id'"> <input type="radio" id="option3" name="options">業種順</label>
                        </div>
                    </div>
				<!--データワード検索-->
                      <form action="<?php echo $_SERVER['PHP_SELF']?>" method="get" name="form_shop_search">
                      <div class="col-sm-4">
                        <div class="input-group">
                          <? $var = "s_keyword";?>
                          <input name="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="口コミタイトル又はIDで検索" class="input-sm form-control"> 
                          <span class="input-group-btn">
                            <button type="submit" class="btn btn-sm btn-primary"> Go!</button> 
                          </span>
                        </div>
                      </div>
                      </form>

                </div><!--row-->

                <div class="table-responsive">
<?php
	$arr_voice_ranking = $common_voice ->Fn_voice_ranking();
	
	$view_count=20;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$arr_where = array();
	$var = "s_keyword";
	if($$var != "")
	{
		$arr_where[$var] = $$var;
	}
	
	$var = "s_flag_open";
	if($$var != "")
	{
		$arr_where[$var] = $$var;
	}

	//合計
	$all_count = 0;
	$arr_voice_all = $common_voice -> Fn_voice_all_count ($arr_where);
	$all_count = $arr_voice_all[0]["all_count"];
	
	//リスト表示
	$arr_etc["order_name"] = $order_name;
	$arr_etc["order"] = $order;
	$arr_etc["offset"] = $offset;
	$arr_etc["view_count"] = $view_count;
	
	$arr_data = array("voice_id", "shop_id", "member_id", "cate_area_s_id", "cate_job_id", "voice_title");
	$arr_data = array_merge($arr_data, array("voice_period", "voice_star", "voice_comment_1", "voice_comment_2"));
	$arr_data = array_merge($arr_data, array("voice_comment_3", "ranking", "flag_open"));
	$arr_data = array_merge($arr_data, array("regi_date", "up_date"));
	
	$arr_voice_list = $common_voice -> Fn_voice_all_list ($arr_data, $arr_where, $arr_etc);
	
	
$table_header = <<<EOF
          <thead><!--項目名-->
            <tr>
						<th>削除</th>
						<th>管理ID</th>
						<th>登録日</th>
						<th>登録時間</th>
						<th>投稿者</th>
						<th>投稿店舗</th>
						<th>第一エリア</th>
						<th>業種</th>
						<th>点数</th>
						<th>タイトル</th>
						<th>編集</th>
						<th>信憑性</th>
            </tr>
          </thead>

EOF;
?>
				<table class="table table-striped table-bordered table-hover dataTables-example" >
        <?php echo $table_header;?>
          <tbody>       
<?

	foreach($arr_voice_list as $arr_key=>$arr_value)
	{
		foreach($arr_value as $key=>$value)
		{
			$$key=$value;
		}
?>
          <tr>
          <tr>
            <td><input type="checkbox" class="check_del" value="<? echo $voice_id;?>"></td>
            <td><small><? echo $voice_id;?></small></td>
            <td><small><? echo substr($regi_date, 0, 10);?></small></td>
            <td><small><? echo substr($regi_date, 11);?></small></td>
            <td>
				<?
                    $arr_data = array(); $arr_data[]="nickname";
					$return_member = $common_member->Fn_db_member_data ($member_id, $arr_data);
					if(!is_null($return_member)) {
				?>
                <a href="/masterdashboard/member/entry.php?member_id=<? echo $member_id;?>"><? echo $return_member[0]["nickname"];?></a>
              <? } ?>
            </td>
            <td>
							<?
                $arr_data = array(); $arr_data[]="shop_name";
                $arr_where = array(); $arr_where["shop_id"]=$shop_id;
								$return_shop = $common_shop->Fn_db_shop_data ($arr_data, $arr_where);
								if(!is_null($return_shop)) {
							?>
            		<a href="/masterdashboard/shop/entry.php?shop_id=<? echo $shop_id;?>"><? echo $return_shop[0]["shop_name"];?></a>
              <? } ?>
            </td>
            <td><small><? echo $arr_cate_area_s_id[$cate_area_s_id];?></small></td>
            <td><small><? echo $arr_cate_job_id[$cate_job_id];?></small></td>
            <td><small><? echo sprintf('%.2f',($voice_star/2));?>点</small></td>
            <td><small><? echo $voice_title;?></small></td>
            <td><button type="button" class="btn btn-outline btn-default"><i class="fa fa-pencil"></i></button></td>
            <td>
            <select class="from-control check_rank">
            <option value="&voice_id[<? echo $voice_id;?>]=0">未定</option>
            <? foreach($common_voice -> Fn_voice_ranking() as $key=>$value) { ?>
            <option value="&voice_id[<? echo $voice_id;?>]=<? echo $key;?>" <? if($ranking==$key){ echo " selected ";}?>><? echo $value;?>ランク</option>
            <? } ?>
            </select>
            </td>
          </tr>
<?
			if($db_loop%10==9) { echo $table_header;}

	}
?>

          </tbody>
      </table>
                                
      <div class="nextlist">
        <div class="btn-group">
					<?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
        </div>
      </div>              		    


                            </div><!--table-responsive-->

                        </div><!--ibox-content-->
                    </div><!--ibox float-e-margins-->

                </div><!--col-lg-12-->

            </div><!--row-->



<!--フッター部分 -->
	<?php require_once $_SERVER['DOCUMENT_ROOT']."/masterdashboard/common/footer.php"; ?>
<!--フッター部分 -->

        </div>
        </div>



    <!-- Mainly scripts -->
    <script src="/masterdashboard/js/jquery-2.1.1.js"></script>
    <script src="/masterdashboard/js/bootstrap.min.js"></script>
    <script src="/masterdashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/masterdashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Peity -->
    <script src="/masterdashboard/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/masterdashboard/js/inspinia.js"></script>
    <script src="/masterdashboard/js/plugins/pace/pace.min.js"></script>

    <!-- iCheck -->
    <script src="/masterdashboard/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Peity -->
    <script src="/masterdashboard/js/demo/peity-demo.js"></script>

    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>

</body>

</html>
