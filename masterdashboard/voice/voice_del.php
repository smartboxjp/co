<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonVoice.php";
	$common_voice = new CommonVoice();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>口コミ削除</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	foreach($_GET as $key => $value)
	{ 
		//$$key = trim($common_dao->db_string_escape($value));
		$$key = $value;
	}
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	$voice_count = 0;
	foreach($voice_id as $value) {
		if($value!="")
		{
			$voice_count++;
		}
	}
	
	if($voice_count==0)
	{
		$common_connect -> Fn_javascript_back("正しく選択してください。");
	}
	
	//削除処理
	foreach($voice_id as $value) {
		if($value!="")
		{
			$arr_where = array();
			$arr_where["voice_id"] = $value;
			$db_result = $common_voice->Fn_voice_delete ($arr_where);
		}
	}
	
	$common_connect-> Fn_javascript_move("削除しました", "list.php");
?>
</body>
</html>