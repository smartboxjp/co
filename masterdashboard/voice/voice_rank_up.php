<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonVoice.php";
	$common_voice = new CommonVoice();
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>口コミ変更</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	

	foreach($_GET["voice_id"] as $key => $value)
	{ 
		$$key = trim($common_dao->db_string_escape($value));
		//$$key = $value;
		$arr_rank[$key]=$value;
	}
	

	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	if(is_null($arr_rank))
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
	
	
	//信憑性は更新
	foreach($arr_rank as $key=>$value) {	
		$arr_data = array();
		$arr_data["ranking"] = $value;
		
		$arr_where = array();
		$arr_where["voice_id"] = $key;
		
		$db_result = $common_voice->Fn_voice_update ($arr_data, $arr_where);
	}
	
	
	//重複してないショップID
	$return_rank = "";
	foreach($arr_rank as $key=>$value) {	
		$return_rank .= $key.",";
	}
	$return_rank = substr($return_rank, 0, strlen($return_rank)-1);
	$sql = "SELECT shop_id FROM voice WHERE voice_id in (".$return_rank.") group by shop_id";
	$arr_bind = array();
	$result_voice_shop_id = $common_dao->db_query_bind($sql, $arr_bind);
	foreach($result_voice_shop_id as $arr_key=>$arr_value)
	{
		foreach($arr_value as $key=>$value)
		{
			$common_voice->Fn_voice_ranking_up ($arr_value["shop_id"]);
		}
	}
	

	
	$common_connect-> Fn_javascript_move("修正しました", "list.php");
?>
</body>
</html>