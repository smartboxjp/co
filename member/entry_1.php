<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	
	$meta_title = "新規会員登録";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

	<!--ページ専用のCSS-->
	<link href="/member/css/entry.css" rel="stylesheet" type="text/css" />

	<!--アイコン用CSS-->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--フォームで使用-->
	<link rel="stylesheet" href="/kanto/app/form/formoid1/formoid-default-skyblue.css" type="text/css" />

<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input_email("login_email");
			
			
			
			if(err_check_count!=0)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
		});
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div style='color:#F00'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		function check_input_email($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div style='color:#F00'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str).val()) == false)
			{
				err ="<div style='color:#F00'>メールアドレスは半角英数字でご入力ください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}

		//メールチェック
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}

	});

</script>


</head>



<body>

<div id="container">

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<div id="mainbox" class="heightblok"><!--コンテンツの外枠-->


<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /start /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>

			<div class="pan">
			会員登録をして自分に合ったお店探しをしてみませんか？
			</div>


			<div class="menbarlogin">
				<a href="" target="_blank">既にアカウントをお持ちの方はこちらからログインして下さい。</a>
			</div>

			<div class="entrcontentuhed">



				<div class="entrcontentuleft">
					<p class="entrtitle">メールアドレスで新規登録を行う場合</p>

<!-- Start Formoid form-->

					<form action="entry_1_save.php" class="formoid-default-skyblue" style="background-color:#f1eae2;font-size:11px;" method="post">
					<div class="element-email">
						<label class="title"><i class="fa fa-envelope-o"></i>メールアドレス</label>
            <? $var = "login_email";?>
						<input class="large" type="email" name="<? echo $var;?>" id="<? echo $var;?>" placeholder="半角英数字　例：sample@cossot.com" />
            <label id="err_<?=$var;?>"></label>
					</div>

					<div class="entrinfo">コソットから、登録URLの記載されたメールが届きます。<br>
        				携帯電話のアドレスで登録する場合、PCからのメール受信を許可するか○○○○○@○○○○<br>からのメールを受信が出来るように設定をしてください。
					<a href="" target="_blank">→携帯各社のメール設定方法</a>
					</div>

					<div class="submit"><input name="form_confirm" id="form_confirm" type="submit" value="会員登録する"/></div>
					</form>
					<script type="text/javascript" src="/kanto/app/form/formoid1/formoid-default-skyblue.js"></script>

<!-- Stop Formoid form-->

				</div>

				<div class="entrcontenturight">
					<p class="entrtitle">他のサービスIDで新規登録を行う場合</p>


					<div class="entrcontenturightsns">
						<a href="./facebook/facebook_regist.php"><img src="/member/img/snsfa.png"/></a>
					</div>

					<div class="entrcontenturightsns">
						<a href="./twitter/twitter_regist.php"><img src="/member/img/snstw.png"/></a>
					</div>

				</div>


				<div class="entrcontentubottom">
					<div class="bottomkiyaku">登録したお客様の個人情報について</div>
					<div class="bottomkiyakunaka">
					・会員サービスのご登録にあたっては、パスワード及びPC/携帯電話のメールアドレスのご登録が必要です。<br>
					・ご登録頂いた個人情報は、お客さまの会員サービスご利用、また、健全なサイト運営以外で使用する事は一切ありません。<br>
						・ご登録頂いたアドレスにコソットから重要なお知らせ（不定期）をなどをお送りする場合があります。<br>
					・万が一、犯罪などが発生した場合しかるべき機関にお客様の個人情報を一時的に提供する場合があります。<br>
					※必ずお読みください<a href="" target="_blank">→利用規約</a>
					</div>

				</div>



			</div>

      
<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /end /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
  


		</div>
<!--main boxコンテンツの外枠-->


	</div>
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>

</div><!--container-->
</body>
</html>

