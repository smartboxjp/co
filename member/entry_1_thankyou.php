<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "会員登録/ログイン";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

	<!--ページ専用のCSS-->
	<link href="/member/css/entry.css" rel="stylesheet" type="text/css" />

	<!--アイコン用CSS-->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--フォームで使用-->
	<link rel="stylesheet" href="/kanto/app/form/formoid1/formoid-default-skyblue.css" type="text/css" />

</head>



<body>

<div id="container">

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<div id="mainbox" class="heightblok"><!--コンテンツの外枠-->


<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /start /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>

			<div class="pan">
				<i class="fa fa-envelope-o"></i>本登録のメールをお送りしました。
			</div>

			<div class="thanksblok">
				<p class="thankstitle">
				<span>下記のメールアドレスに登録用のURLをお送りしました。</span><br>
				※会員登録はまだ完了してません。24時間以内にメール内のURLから登録を完了して下さい。
				</p>

				<div class="thankscntent">
				<? echo $_GET["login_email"];?>
				</div>

				<p class="tanksbottom">
				お届いたメールアドレスにアクセスを行い、会員登録をして下さい。<br>
				会員登録は5分ほどで完了致します。
				</p>

			</div>


			<div class="tanksinfoblok">

				<ul class="tanksinfo">
					<li class="risttitle"><i class="fa fa-question-circle"></i>登録メールが届かない場合</li>
					<li>【入力したメールアドレスに間違いはありませんか？】</li>
					<li class="bottomspce">正しく入力してるつもりでも、-スラッシュや_アンダーバーなどの記号の入力違い、0ゼロやoオーなどの<br>間違が多いのでもう一度確認してみて下さい。</li>
					<li>【パソコンからのメールが受信できないようになってませんか？】</li>
					<li class="bottomspce">指定受信や迷惑メール設定などでパソコンからのメールが届かない場合があります。<br>メール設定の変更やコソットからのメールアドレス<? echo $global_send_mail;?>から受信できるようにして下さい。</li>
					<li>【迷惑メールフォルダに入ってませんか？】</li>
					<li class="bottomspce">お使いのメールソフトによっては迷惑メールフォルダにメールが届いてる場合があります。<br>迷惑メールのフォルダも確認してみてください。</li>
					<li>それでも届かない場合は→<a href="/kanto/help/inquiry.php">【問い合わせをする】</a></li>
				</ul>
			</div>




      
<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /end /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
  


		</div>
<!--main boxコンテンツの外枠-->


	</div>
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>

</div><!--container-->
</body>
</html>