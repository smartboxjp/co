<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "会員登録/ログイン";
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMemberTemp.php";
	$common_member_temp = new CommonMemberTemp();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_catearea = new CommonCateArea();

	foreach($_GET as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}

	if($temp_key=="")
	{
		$common_connect-> Fn_javascript_back("正しく入力して下さい。");
	}
	
	//発行されてキーが一致してるかをチェック
	$arr_where = array();
	$arr_where["temp_key"] = $temp_key;
	
	$arr_data = array();
	$arr_data[] = "member_temp_id";
	$arr_data[] = "login_email";
	$arr_data[] = "facebook";
	$arr_data[] = "twitter";
	
	$arr_temp_key = $common_member_temp->Fn_db_member_temp_data($arr_data, $arr_where)  ;

	if(is_null($arr_temp_key[0]))
	{
		$common_connect-> Fn_javascript_back("もう一度仮登録を行ってください。");
	}
	$facebook = $arr_temp_key[0]["facebook"];
	$twitter = $arr_temp_key[0]["twitter"];
	
	$login_pw = $common_connect-> Fn_random_password(10);
	$login_pw_confirm = $login_pw;
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

	<!--ページ専用のCSS-->
	<link href="/member/css/entry.css" rel="stylesheet" type="text/css" />

	<!--アイコン用CSS-->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<!--フォームで使用-->
	<link rel="stylesheet" href="/kanto/app/form/formoid1/formoid-default-skyblue.css" type="text/css" />


<script type="text/javascript">
	$(function() {
		
		var s_area_all = $("#cate_area_s_id option").clone();
		$("#cate_area_l_id").change(function () {
			var s_area_temp = $("#cate_area_l_id").val();
			$("#cate_area_s_id").removeAttr("disabled");
			$("#cate_area_s_id option").remove();
			$(s_area_all).appendTo("#cate_area_s_id");
			$("#cate_area_s_id option[class != a_"+s_area_temp+"]").remove();
			$("#cate_area_s_id").prepend('<option selected value="">-未選択-</option>');
		});

	});
	
//-->
</script>

<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			err_check = false;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input("nickname");
			err_check_count += check_input("cate_area_l_id");
			err_check_count += check_input("cate_area_s_id");
			err_check_count += check_input_password("login_pw", "login_pw_confirm");
			
			if(err_check_count)
			{
				//alert("入力に不備があります");
				return false;
			}
			else
			{
				$('#form_confirm').submit();
				return true;
			}
			
			
		});
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			$("#"+$str).removeClass("error").removeClass("reauired");
			
			if($('#'+$str).val().replace(/　/g," ").match(/^\s+$/))
			{
				err ="<span class='error'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				$("#"+$str).toggleClass("reauired").addClass("error");
				$("#"+$str).focus();
				
				return 1;
			}
			else if($('#'+$str).val()=="")
			{
				err ="<span class='error'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				$("#"+$str).toggleClass("reauired").addClass("error");
				$("#"+$str).focus();
				
				return 1;
			}
			return 0;
		}
		
		//メールチェック
		function check_input_email($str_1) 
		{
			$("#err_"+$str_1).html(err_default);
			$("#"+$str_1).css(background,bgcolor_default);
			$("#"+$str_1).removeClass("error").removeClass("reauired");
			
			if(checkIsEmail($('#'+$str_1).val()) == false)
			{
				err ="<span class='error'>メールアドレスは半角英数字でご入力ください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				$("#"+$str_1).toggleClass("reauired").addClass("error");
				$("#"+$str_1).focus();
				
				return 1;
			}
			
			return 0;
		}

		//メールチェック
		function check_input_password($str_1, $str_2) 
		{
			$("#err_"+$str_1).html(err_default);
			$("#"+$str_1).css(background,bgcolor_default);
			$("#"+$str_1).removeClass("error").removeClass("reauired");
			
			$("#err_"+$str_2).html(err_default);
			$("#"+$str_2).css(background,bgcolor_default);
			$("#"+$str_2).removeClass("error").removeClass("reauired");
			
			if($('#'+$str_1).val() == "" )
			{
				err ="<span class='error'>パスワードを正しく入力してください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				$("#"+$str_1).toggleClass("reauired").addClass("error");
				$("#"+$str_1).focus();
				
				return 1;
			}
			else if(checkIsPassword($('#'+$str_1).val()) == false)
			{
				err ="<span class='error'>パスワードは６文字以上の半角英数字「!#$%&@()*+,.」でご入力ください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				$("#"+$str_1).toggleClass("reauired").addClass("error");
				$("#"+$str_1).focus();
				
				return 1;
			}
			else if($('#'+$str_1).val().length < 6)
			{
				err ="<span class='error'>パスワードは６文字以上の半角英数字「!#$%&@()*+,.」でご入力ください。</span>";
				$("#err_"+$str_1).html(err);
				$("#"+$str_1).css(background,bgcolor_err);
				$("#"+$str_1).toggleClass("reauired").addClass("error");
				$("#"+$str_1).focus();
				
				return 1;
			}
			else if($('#'+$str_1).val() != $('#'+$str_2).val() )
			{
				err ="<span class='error'>パスワードを正しく入力してください。</span>";
				$("#err_"+$str_2).html(err);
				$("#"+$str_2).css(background,bgcolor_err);
				$("#"+$str_2).toggleClass("reauired").addClass("error");
				$("#"+$str_2).focus();
				
				return 1;
			}
			
			return 0;
		}
		
		//メールチェック
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}
		
		
		function checkIsPassword(value) 
		{
			if (value.match(/^[0-9a-zA-Z!#$%&@()*+,./_-]{6,}$/) == null) {
				return false;
			}
			return true;
		}

		
	});
	
//-->
</script>

<!--文字カウント-->
<script type="text/javascript">
$(function(){
    var countMax = 330;
    $('textarea').bind('keydown keyup keypress change',function(){
        var thisValueLength = $(this).val().length;
        var countDown = (countMax)-(thisValueLength);
        $('.count').html(countDown);
 
        if(countDown < 0){
            $('.count').css({color:'#ff0000',fontWeight:'bold'});
        } else {
            $('.count').css({color:'#000000',fontWeight:'normal'});
        }
    });
    $(window).load(function(){
        $('.count').html(countMax);
    });
});
</script>
<!--文字カウント-->
</head>



<body>
<div id="container">

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<div id="mainbox" class="heightblok"><!--コンテンツの外枠-->


<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /start /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>


			<div class="pan2">
				登録の為にあなたの簡単なプロフィールを入力してください
			</div>

				<div class="inputdetamail">
<!--登録申請されたアドレス-->
					<label class="title">※登録されたメールアドレス（登録ID）</label>
					<div class="completed">
					<? echo $arr_temp_key[0]["login_email"];?>
					</div>

				</div>

				<div class="inputdeta">

				<form action="entry_2_confirm.php" class="formoid-default-skyblue" style="background-color:#f1eae2;font-size:11px;" name="formName" method="post">
        <input type="hidden" name="temp_key" value="<? echo $temp_key?>">
<!-- ニックネーム-->
					<div class="element-input">
						<label class="title">ニックネーム（全角8文字まで）</label>
            <? $var = "nickname";?>
            <input name="<? echo $var;?>" id="<? echo $var;?>" class="large" type="text" />
            <label id="err_<? echo $var;?>"></label>
					</div>
<!-- エリア大-->
					<div class="element-select">
						<label class="title">仕事を探してるエリアはどこですか？</label>
						<div class="large"><span>
            		<?
							$arr_where_s= array();
							$arr_where_l["flag_open"] = "1"; //公開のみ
							$result_cate_area_l =  $common_catearea->Fn_cate_area_l_list ($arr_where_l) ;
                ?>
								<? $var = "cate_area_l_id"; ?>
							<select name="<? echo $var;?>" id="<? echo $var;?>">
								<option value = "">未選択</option>
								<?
                foreach($result_cate_area_l as $arr_cate_area_l)
                {
								$db_cate_area_l_id = $arr_cate_area_l["cate_area_l_id"];
								$db_cate_area_l_title = $arr_cate_area_l["cate_area_l_title"];
                ?>
                <option value="<? echo $db_cate_area_l_id;?>" <? if($$var==$db_cate_area_l_id) { echo " selected ";}?>><? echo $db_cate_area_l_title;?></option>
                <?
                }
                ?>
							</select>
              </span>
              <label id="err_<? echo $var;?>"></label>
						</div>
					</div>
<!-- エリア小-->
					<div class="element-select">
						<label class="title">さらに詳しく（この部分は公開されません）</label>
						<div class="large"><span> 
            
              <?
							//area 小
							$arr_where_s= array();
							$arr_where_s["flag_open"] = "1"; //公開のみ
							$result_cate_area_s =  $common_catearea->Fn_cate_area_s_list ($arr_where_s) ;
              ?>
							<? $var = "cate_area_s_id"; ?>
							<select name="<? echo $var;?>" id="<? echo $var;?>">
								<option value = "">-未選択-</option>
								<?
                foreach($result_cate_area_s as $arr_cate_area_s)
                {
								$db_cate_area_l_id = $arr_cate_area_s["cate_area_l_id"];
								$db_cate_area_s_id = $arr_cate_area_s["cate_area_s_id"];
								$db_cate_area_s_title = $arr_cate_area_s["cate_area_s_title"];
                ?>
                <option value="<? echo $db_cate_area_s_id;?>" <? if($$var==$db_cate_area_s_id) { echo " selected ";}?> class="a_<? echo $db_cate_area_l_id;?>"><? echo $db_cate_area_s_title;?></option>
                <?
                }
                ?>
							</select>
              </span>
              <label id="err_<? echo $var;?>"></label>
						</div>
					</div>
<!-- パスワード-->
					<div class="element-input" >
						<label class="title">パスワード（半角6～20文字以内）<div style="color:#00F">*おすすめパスワードが表示されているので、自由に変更してください。</div></label>
            <? $var = "login_pw";?>
            <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>"  class="large" type="text" />
            <label id="err_<? echo $var;?>"></label>
					</div>
<!-- パスワード確認-->
					<div class="element-input">	
						<label class="title">パスワード（もう一度）</label>
            <? $var = "login_pw_confirm";?>
            <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" class="large" type="text" />
            <label id="err_<? echo $var;?>"></label>
					</div>
<!-- 自己紹介-->
					<div class="element-textarea">
						<label class="title">あなたの自己紹介（今は未記入でも構いません）</label>
            <? $var = "member_comment";?>
						<textarea name="<? echo $var;?>" id="<? echo $var;?>"  class="medium" cols="20" rows="5" ></textarea>
						<div class="counttext">あと<span class="count">330</span>文字まで</div>
            <label id="err_<? echo $var;?>"></label>
					</div>

					<div class="submit">
          	<? $var = "form_confirm";?>
            <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="submit" value="これで登録する"/>
          </div>
				</form>
				</div>



      
<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /end /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
  


		</div>
<!--main boxコンテンツの外枠-->


	</div>
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>

</div><!--container-->
</body>
</html>

