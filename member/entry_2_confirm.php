<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "会員登録/ログイン";
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMemberTemp.php";
	$common_member_temp = new CommonMemberTemp();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_cate_area = new CommonCateArea();
	
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

	<!--ページ専用のCSS-->
	<link href="/member/css/entry.css" rel="stylesheet" type="text/css" />

	<!--アイコン用CSS-->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--フォームで使用-->
	<link rel="stylesheet" href="/kanto/app/form/formoid1/formoid-default-skyblue.css" type="text/css" />

</head>



<body>
<?

	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}
	
	if($temp_key=="")
	{
		$common_connect-> Fn_javascript_back("正しく入力して下さい。");
	}
	
	//発行されてキーが一致してるかをチェック
	$arr_where = array();
	$arr_where["temp_key"] = $temp_key;
	
	$arr_data = array();
	$arr_data[] = "member_temp_id";
	$arr_data[] = "login_email";
	$arr_data[] = "facebook";
	$arr_data[] = "twitter";
	
	$arr_temp_key = $common_member_temp->Fn_db_member_temp_data($arr_data, $arr_where)  ;

	if(is_null($arr_temp_key[0]))
	{
		$common_connect-> Fn_javascript_back("もう一度仮登録を行ってください。");
	}
	$facebook = $arr_temp_key[0]["facebook"];
	$twitter = $arr_temp_key[0]["twitter"];
	
	if($nickname == "" || $cate_area_s_id == "" || $login_pw == "")
	{
		$common_connect -> Fn_javascript_back("正しく入力して下さい。");
	}
?>
<div id="container">

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<div id="mainbox" class="heightblok"><!--コンテンツの外枠-->


<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /start /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>

			<div class="pan3">
				登録内容を確認して下さい。
			</div>


      <form action="entry_2_save.php" class="formoid-default-skyblue" style="font-size:14px;" method="post">
      <input type="hidden" name="temp_key" value="<? echo $temp_key?>">
			<table class="brwsr2">

			<tbody>

				<tr>
					<th>メールアドレス（登録ID）</th>
						<td class="data fst">
						<? echo $arr_temp_key[0]["login_email"];?>
 						</td>
					</tr>

				<tr>
 					<td class="bar" colspan="2"></td>
				</tr>

				<tr>
 					<th>ニックネーム</th>
  						<td class="data fst">
  						<? $var = "nickname";?>
              <input id="<? echo $var;?>" name="<? echo $var;?>" type="hidden" value="<? echo $$var;?>"><? echo $$var;?>
 						</td>
				</tr>

				<tr>
 					<td class="bar" colspan="2"></td>
				</tr>

				<tr>
 					<th>仕事を探したいエリア</th>
  						<td class="data fst">
              <?
							//area 大
							$arr_where= array();
							$arr_where["flag_open"] = "1"; //公開のみ
							$arr_where["cate_area_l_id"] = $cate_area_l_id;
							$result_cate_area =  $common_cate_area->Fn_cate_area_l_list ($arr_where) ;
              	echo $result_cate_area[0]["cate_area_l_title"];
								
							//area 小
							$arr_where= array();
							$arr_where["flag_open"] = "1"; //公開のみ
							$arr_where["cate_area_s_id"] = $cate_area_s_id;
							$result_cate_area =  $common_cate_area->Fn_cate_area_s_list ($arr_where) ;
              	if($result_cate_area[0]["cate_area_s_title"]!=""){ echo "／".$result_cate_area[0]["cate_area_s_title"];}
							?>
              <? $var = "cate_area_l_id";?>
              <input id="<? echo $var;?>" name="<? echo $var;?>" type="hidden" value="<? echo $$var;?>">
              <? $var = "cate_area_s_id";?>
              <input id="<? echo $var;?>" name="<? echo $var;?>" type="hidden" value="<? echo $$var;?>">
              
  						</td>
				</tr>

				<tr>
 					<td class="bar" colspan="2"></td>
				</tr>

				<tr>
 					<th>パスワード</th>
  						<td class="data fst">
  						<? $var = "login_pw";?>
              <input id="<? echo $var;?>" name="<? echo $var;?>" type="hidden" value="<? echo $$var;?>">＊＊＊＊＊＊
  						</td>
				</tr>

				<tr>
 					<td class="bar" colspan="2"></td>
				</tr>

				<tr>
 					<th>自己紹介</th>
  						<td class="data fst">
  						<? $var = "member_comment";?>
              <input id="<? echo $var;?>" name="<? echo $var;?>" type="hidden" value="<? echo $$var;?>"><? echo nl2br($$var);?>
  						</td>
 				</tr>
			</tbody>

			</table>


			<div class="inputfinal">
				<div class="submit">
					<input type="submit" value="コソットに会員登録をする"/>
				</div>
			</div>
      </form>


      
<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /end /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
  


		</div>
<!--main boxコンテンツの外枠-->


	</div>
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>

</div><!--container-->
</body>
</html>

