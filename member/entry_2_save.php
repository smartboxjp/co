<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMemberTemp.php";
	$common_member_temp = new CommonMemberTemp();
	
	$meta_title = "会員登録/ログイン";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>
	<link href="./css/login.css" rel="stylesheet" type="text/css" />

</head>

<body>
<?
	$datetime = date("Y-m-d H:i:s");
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}
	
	//発行されてキーが一致してるかをチェック
	$sql = "SELECT member_temp_id, login_email, facebook, twitter FROM member_temp where temp_key='".$temp_key."' " ;
	$db_result = $common_dao->db_query_bind($sql);

	if(!$db_result)
	{
		$common_connect-> Fn_javascript_back("もう一度仮登録を行ってください。");
	}
	$facebook = $db_result[0]["facebook"];
	$twitter = $db_result[0]["twitter"];
	$login_email = $db_result[0]["login_email"];
	
	if($temp_key=="" || $nickname == "" || $cate_area_s_id == "" || $login_pw == "")
	{
		$common_connect -> Fn_javascript_back("正しく入力して下さい。");
	}
	
	
	
	//仮DBから本DBへ移動
	$flag_open = 1;

	$arr_db_field = array("login_email", "nickname", "login_pw", "member_comment");
	$arr_db_field = array_merge($arr_db_field, array("twitter", "facebook", "yahoo", "cate_area_l_id", "cate_area_s_id"));

	$db_insert = "insert into member ( ";
	$db_insert .= " member_id, ";
	foreach($arr_db_field as $key=>$value)
	{
		$db_insert .= $value.", ";
	}
	$db_insert .= " regi_date, up_date ";
	$db_insert .= " ) values ( ";
	$db_insert .= " '', ";
	foreach($arr_db_field as $key=>$value)
	{
		$db_insert .= "'".$$value."', ";
	}
	$db_insert .= " '$datetime', '$datetime')";
	
	$db_result = $common_dao->db_update($db_insert);

	
	//仮DBから削除
	$db_up = "Delete from member_temp where login_email='".$login_email."' " ;
	$db_result = $common_dao->db_update($db_up);
	
	$return_member = $common_member -> Fn_db_apply_auto();
	
	$db_member_id = $return_member[0]["last_id"];
	
	//ログイン
	session_start();
	$_SESSION['member_id']=$db_member_id;
	$_SESSION['nickname']=$nickname;
	

	$common_connect-> Fn_redirect(global_no_ssl."/member/entry_thankyou.php");
?>

</body>

</html>
