<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "会員登録/ログイン";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

	<!--ページ専用のCSS-->
	<link href="/member/css/entry.css" rel="stylesheet" type="text/css" />

	<!--アイコン用CSS-->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--フォームで使用-->
	<link rel="stylesheet" href="/kanto/app/form/formoid1/formoid-default-skyblue.css" type="text/css" />

</head>



<body>

<div id="container">

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>
			<div id="mainbox" class="heightblok"><!--コンテンツの外枠-->


<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /start /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>

			<div class="pan4">
				無事登録が完了致しました
			</div>




			<div class="infobottom">
				<p class="inputinfo"> <img src="/member/img/point1.png"/>コソットの会員登録で出来る事</p>



				<div class="blokleft">
					<div class="blokimgleft">
  						<img src="/member/img/inputimg1.png">
  					</div>

　　					<div class="bloktextleft"> 
　   						<p>お店の口コミや評価の投稿</p>お店で働いた感想や評価を投稿できます。どんな所が働きやすかったか、逆に改善してほしい所はどこかなど、あなたの主観で意見を述べてみましょう。
　					</div>
 					<div class="blokbottom"></div>
				</div>


				<div class="blokright">
　					<div class="blokimgleft">
  						<img src="/member/img/inputimg2.png">
  					</div>

　　					<div class="bloktextleft"> 
　　						<p>メールアドレス非公開でお店とやり取り</p>会員登録されているなら、お店への面接の申し込みや問い合わせがアドレス入力なくやり取り可能です。簡単でプライバシーの守られた環境下でお仕事探しが出来ます。
　　					</div>
 					<div class="blokbottom"></div>
				</div>

				<div class="blokleft">
  					<div class="blokimgleft">
  						<img src="/member/img/inputimg3.png">
  					</div>

　　					<div class="bloktextleft">
　　						<p>お気に入り登録</p>10件まで気になるお店をお気に入り登録が出来ます。お気に入りの中で評価の高いお店から順番に問い合わせなど、自分でスケジュールが組みやすくなります。
　　					</div>
　					<div class="blokbottom"></div>
				</div>

				<div class="blokright">
　					<div class="blokimgleft">
  						<img src="/member/img/inputimg4.png">
  					</div>

　　					<div class="bloktextleft"> 
　　						<p>スケジュール管理</p>自分だけのスケジュール表で、予定を簡単に登録、見やすく整理出来ます。面接や出勤など忘れがちな予定を書き込みましょう。※現在PCのみ
　					</div>
 					<div class="blokbottom"></div>
				</div>


				<div class="blokbottom"></div>



			</div>



			<div class="kutikomipagenation">

				<div id="kutikomipagenationbotan">
					<ul>
					<li><a href="#">マイページにログインする</a></li>
					<li><a href="#">サイトに戻る</a></li>
					<li><a href="#">会員規約を読む</a></li>
					</ul> 
				</div>

			</div><!--kutikomipagenation-->







      
<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /end /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
  


		</div>
<!--main boxコンテンツの外枠-->


	</div>
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>

</div><!--container-->
</body>
</html>

