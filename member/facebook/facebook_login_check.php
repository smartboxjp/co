<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
    $common_member = new CommonMember();
    
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/facebook_src/autoload.php";
    
   
    session_start();
    $fb = new Facebook\Facebook([
        'app_id' => MY_APP_ID, // Replace {app-id} with your app id
        'app_secret' => MY_APP_SECRET,
        'default_graph_version' => 'v2.2',
        'persistent_data_handler' => 'session',
    ]);

    $helper = $fb->getRedirectLoginHelper();

    try {
      $accessToken = $helper->getAccessToken();
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    if (! isset($accessToken)) {
      if ($helper->getError()) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Error: " . $helper->getError() . "\n";
        echo "Error Code: " . $helper->getErrorCode() . "\n";
        echo "Error Reason: " . $helper->getErrorReason() . "\n";
        echo "Error Description: " . $helper->getErrorDescription() . "\n";
      } else {
        header('HTTP/1.0 400 Bad Request');
        echo 'Bad request';
      }
      exit;
    }

    try {
      // Returns a `Facebook\FacebookResponse` object
      $response = $fb->get('/me?fields=id,name,email', $accessToken);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $user = $response->getGraphUser();
    
    $meta_title = "Facebookのログイン";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

</head>

<body>
<?

    try {
      // Returns a `Facebook\FacebookResponse` object
      $response = $fb->get('/me?fields=id,name,email', $accessToken);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $user = $response->getGraphUser();

    /*
    echo '<hr />id: ' . $user['id'];
    echo '<hr />Name: ' . $user['name'];
    echo '<hr />Email: ' . $user['email'];
    // OR
    // echo 'Name: ' . $user->getName();
    */
    $login_email = $user['email'];
    $facebook_id = $user["id"];

    
    $datetime = date("Y-m-d H:i:s");
    
    if($login_email == "" || $facebook_id == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力して下さい。");
    }
    
    //すでに登録されていれば、ログイン成功
    $sql = "SELECT member_id, nickname, flag_open FROM member where login_email='".$login_email."' and facebook='".$facebook_id."' " ;
    $arr_member = $common_dao->db_query_bind($sql, $arr_bind);

    if(!is_null($arr_member[0]))
    {
        if($arr_member[0]["flag_open"]=="1")
        {
            //ログイン日
            $arr_where = array();
            $arr_where["member_id"] = $arr_member[0]["member_id"];
            $arr_data = array();
            $arr_data["lastlogin_date"] = $datetime;
            $common_member->Fn_member_update ($arr_data, $arr_where);
            
            //ログイン
            session_start();
            $_SESSION['member_id']=$arr_member[0]["member_id"];
            $_SESSION['nickname']=$arr_member[0]["nickname"];
            
            $common_connect-> Fn_javascript_move("ログインしました。", global_no_ssl."/".$global_area."/");
        }
        else
        {
            $common_connect -> Fn_javascript_back("ログインが禁止されているIDです。");
        }
        
    }
    else
    {
        $common_connect -> Fn_javascript_move("一致してるデータがありません。\\r\\n会員登録してください。", "/member/entry_1.php");
    }
    
?>

</body>

</html>