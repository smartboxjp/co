<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMemberTemp.php";
	$common_member_temp = new CommonMemberTemp();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/facebook_src/autoload.php";
	

    session_start();
    $fb = new Facebook\Facebook([
        'app_id' => MY_APP_ID, // Replace {app-id} with your app id
        'app_secret' => MY_APP_SECRET,
        'default_graph_version' => 'v2.2',
        'persistent_data_handler' => 'session',
    ]);

    $helper = $fb->getRedirectLoginHelper();

    try {
      $accessToken = $helper->getAccessToken();
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    if (! isset($accessToken)) {
      if ($helper->getError()) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Error: " . $helper->getError() . "\n";
        echo "Error Code: " . $helper->getErrorCode() . "\n";
        echo "Error Reason: " . $helper->getErrorReason() . "\n";
        echo "Error Description: " . $helper->getErrorDescription() . "\n";
      } else {
        header('HTTP/1.0 400 Bad Request');
        echo 'Bad request';
      }
      exit;
    }

/*
// Logged in
echo '<h3>Access Token</h3>';
var_dump($accessToken->getValue());

// The OAuth 2.0 client handler helps us manage access tokens
$oAuth2Client = $fb->getOAuth2Client();

// Get the access token metadata from /debug_token
$tokenMetadata = $oAuth2Client->debugToken($accessToken);
echo '<h3>Metadata</h3>';
var_dump($tokenMetadata);

// Validation (these will throw FacebookSDKException's when they fail)
$tokenMetadata->validateAppId(MY_APP_ID); // Replace {app-id} with your app id
// If you know the user ID this access token belongs to, you can validate it here
//$tokenMetadata->validateUserId('123');
$tokenMetadata->validateExpiration();

if (! $accessToken->isLongLived()) {
  // Exchanges a short-lived access token for a long-lived one
  try {
    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
  } catch (Facebook\Exceptions\FacebookSDKException $e) {
    echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
    exit;
  }

  echo '<h3>Long-lived</h3>';
  print_r($accessToken->getValue());
}

$_SESSION['fb_access_token'] = (string) $accessToken;

// User is logged in with a long-lived access token.
// You can redirect them to a members-only page.
//header('Location: https://example.com/members.php');

*/
    try {
      // Returns a `Facebook\FacebookResponse` object
      $response = $fb->get('/me?fields=id,name,email', $accessToken);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $user = $response->getGraphUser();

    /*
    echo '<hr />id: ' . $user['id'];
    echo '<hr />Name: ' . $user['name'];
    echo '<hr />Email: ' . $user['email'];
    // OR
    // echo 'Name: ' . $user->getName();
    */
	$login_email = $user['email'];
	$facebook_id = $user["id"];




	$meta_title = "Facebookの会員登録";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

</head>

<body>
<?
	 

	
	$datetime = date("Y-m-d H:i:s");
	
	if($login_email == "" || $facebook_id == "")
	{
		$common_connect -> Fn_javascript_back("正しく入力して下さい。");
	}
	
	//すでに登録されていれば、ログイン成功
	$arr_data = array();
	$arr_data[] = "login_email";
	
	$arr_where = array();
	$arr_where["login_email"] = $login_email;
	
	$arr_member = $common_member -> Fn_db_member_data_select ($arr_data, $arr_where) ;
	if(!is_null($arr_member[0]))
	{
		$common_connect -> Fn_javascript_move("すでに登録されているメールです。\\r\\nログインしてください。", "/member/");
	}
	
	//すでに登録されていれば、ログイン成功
	$arr_data = array();
	$arr_data[] = "login_email";
	$arr_data[] = "facebook";
	
	$arr_where = array();
	$arr_where["facebook"] = $facebook_id;
	
	$arr_member = $common_member -> Fn_db_member_data_select ($arr_data, $arr_where) ;
	

	//facebook_idで一致してるかをチェック
	if(!is_null($arr_member[0]))
	{
		if($login_email == $arr_member[0]["login_email"])
		{
			$common_connect -> Fn_javascript_move("すでに登録されているいます。\\r\\nログインしてください。", "/member/");
		}
		else
		{
			$common_connect -> Fn_javascript_move("メールが一致しません。\\r\\nSNS側で変更があった場合ログイン後メールを修正してください。", "/member/");
		}
	}
	//データがなければ会員登録をさせる
	else
	{
		//同じfacebook idがあれば削除
        $db_up = "Delete from member_temp where facebook='".$facebook_id."' ";
        $db_result = $common_dao->db_update($db_up, $arr_bind);
		
		//同じemailがあれば削除
        $db_up = "Delete from member_temp where login_email='".$login_email."' ";
        $db_result = $common_dao->db_update($db_up, $arr_bind);
		
		
		$temp_key = date("Ymd").$common_connect-> Fn_random_password(20);
		
		//仮登録を行う
		$arr_data = array();
		$arr_data["facebook"] = $facebook_id;
		$arr_data["login_email"] = $login_email;
		$arr_data["temp_key"] = $temp_key;
		$common_member_temp -> Fn_member_temp_insert_temp($arr_data);
		
		//本登録ページへ遷移
		$common_connect-> Fn_redirect("/member/entry_2.php?temp_key=".$temp_key);
	}
	
?>

</body>

</html>