<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "パスワードの再発行";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

	<!--ページ専用のCSS-->
	<link href="/member/css/entry.css" rel="stylesheet" type="text/css" />

	<!--アイコン用CSS-->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--フォームで使用-->
	<link rel="stylesheet" href="/kanto/app/form/formoid1/formoid-default-skyblue.css" type="text/css" />

<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input_email("login_email");
			
			
			
			if(err_check_count!=0)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
		});
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div style='color:#F00'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		function check_input_email($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div style='color:#F00'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str).val()) == false)
			{
				err ="<div style='color:#F00'>メールアドレスは半角英数字でご入力ください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}

		//メールチェック
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}

	});

</script>
</head>



<body>

<div id="container">

	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<div id="mainbox" class="heightblok"><!--コンテンツの外枠-->


<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /start /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>


			<div class="passreissueblok">

				<p class="passtitle">
				パスワードの再発行
				</p>

				
					<div class="infotitle">登録時に設定したパスワードをお忘れですか？</div>
					パスワードを忘れた場合は、下記のフォームから登録した時のメールアドレスを入力して再発行の手続きをして下さい。<br>
					※再発行メールの期限は24時間以内です。もし24時間過ぎた場合は再び再発行の手続きをして下さい。<br>
					メールアドレスを忘れた場合、新しいアドレスで<a href="http://cossot.com/kanto/member/entry_1.php">→新規登録←</a>をしてください。
					<div class="passbody">
            <form action="forgot_save.php" class="formoid-default-skyblue" method="post">
							<div class="emailformblok">
								<div class="mailleft">※登録時のメールアドレス</div>
                <? $var = "login_email";?>
								<input class="large" type="email" name="<? echo $var;?>" id="<? echo $var;?>" placeholder="半角英数字　例：sample@cossot.com" />
							</div>

							<div class="submit">
								<input name="form_confirm" id="form_confirm" type="submit" value="パスワードを再発行する"/>
							</div>
						</form>
					</div>



			</div>




      
<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /end /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
  


		</div>
<!--main boxコンテンツの外枠-->


	</div>
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>

</div><!--container-->
</body>
</html>

