<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	$common_email = new CommonEmail(); //メール関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	$meta_title = "パスワードの再発行";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>
</head>

<body>
<?
	$datetime = date("Y-m-d H:i:s");
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}
	
	if($login_email=="")
	{
		$common_connect-> Fn_javascript_back("正しく入力して下さい。");
	}
	

	//既に登録されているメールかをチェック
	$arr_where = array();
	$arr_where["login_email"] = $login_email;
	
	$arr_data = array();
	$arr_data[] = "login_email";
	
	$arr_where_not = array();
	$arr_where_not["flag_open"] = "0";
	
	$result_login_email = $common_member->Fn_db_member_data_select ($arr_data, $arr_where, $arr_where_not) ;

	if($result_login_email)
	{
		$login_pw = $common_connect-> Fn_random_password(10);
	}
	else
	{
		$common_connect-> Fn_javascript_back("登録されてないメールです。");
	}
	
	$arr_data = array();
	$var = "login_pw"; $arr_data[$var] = $$var;
	$arr_where = array();
	$var = "login_email"; $arr_where[$var] = $$var;
	$common_member->Fn_member_update ($arr_data, $arr_where) ;
	
	$temp_url = global_ssl."/member/";
	
	//Thank youメール
	if ($login_email != "")
	{
		$subject = "『 コソット 』パスワードを再発行しました。";
		
		$body = file_get_contents("./mail/forgot.php");
		$body = str_replace("[login_email]", $login_email, $body);
		$body = str_replace("[login_pw]", $login_pw, $body);
		$body = str_replace("[temp_url]", $temp_url, $body);
		$body = str_replace("[datetime]", $datetime, $body);
		$body = str_replace("[global_send_mail]", $global_send_mail, $body);
		$body = str_replace("[global_email_footer]", $global_email_footer, $body);
		
		$common_email-> Fn_send_utf($login_email."<".$login_email.">",$subject,$body,$global_mail_from,$global_send_mail);
	}
	
	//$common_connect -> Fn_email_log($login_email, $subject, $body); //メールログ
	//$common_email-> Fn_send_utf($global_send_mail."<".$global_send_mail.">",$subject,$body,$global_mail_from,$global_send_mail);

	$_SESSION['member_id']="";
	$_SESSION['nickname']="";

	setcookie("pck", "", time() - 1800, "/");

	$common_connect-> Fn_javascript_move("パスワード再発行完了しました。メールを確認してください。", global_no_ssl."/");
?>

</body>

</html>
