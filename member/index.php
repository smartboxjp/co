<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	
	$meta_title = "会員登録/ログイン";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>
	<link href="./css/login.css" rel="stylesheet" type="text/css" />
	<!--アイコン用CSS-->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input_email("login_email");
			err_check_count += check_input("login_pw");
			
			
			
			if(err_check_count!=0)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
		});
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div style='color:#F00'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		function check_input_email($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<div style='color:#F00'>正しく入力してください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkIsEmail($('#'+$str).val()) == false)
			{
				err ="<div style='color:#F00'>メールアドレスは半角英数字でご入力ください。</div>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			
			return 0;
		}

		//メールチェック
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}

	});

</script>
</head>



<body class="bacimg">

<div id="container">
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?>

  <div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->



<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /start /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>



    <!--ログインメインボックス-->
    <div id="loginbox">

	<div class="start">
        	<span style="font-family: 'Pinyon Script', cursive;">December(12)All the best</span>
	</div>

	<div class="leftblok">
		<div class="loginform">
  
    <form action="login_check.php" class="login" method="post">
      <h1>コソットログイン</h1>
      <? $var = "login_email";?>
      <input type="email" name="<? echo $var;?>" id="<? echo $var;?>" class="login-input" placeholder="メールアドレス" autofocus>
      <label id="err_<?=$var;?>"></label>
      <? $var = "login_pw";?>
      <input type="password" name="<? echo $var;?>" id="<? echo $var;?>" class="login-input" placeholder="パスワード">
      <label id="err_<?=$var;?>"></label>
      <input name="form_confirm" id="form_confirm" type="submit" value="Login" class="login-submit">
      <p class="checkbox">
        <? $var = "login_auto";?>
        <input type="checkbox" name="<?=$var;?>" id="<?=$var;?>" value="on" >
        <label for="<?=$var;?>" class="check">パスワードを保存</label>
      </p>
      <p class="login-help"><a href="/member/entry_1.php"><i class="fa fa-chevron-right" aria-hidden="true"></i>登録がまだな方はこちら</a></p>
      <p class="login-help"><a href="/member/forgot.php"><i class="fa fa-chevron-right" aria-hidden="true"></i>パスワードを忘れてしまったら</a></p>
      <? $var = "return_url";?>
      <input type="hidden" name="<?php echo $var;?>" value="<?php echo $$var;?>">
    </form>
		</div>
	</div>

	<div class="rightblok">
		<div class="loginboxsns">
			<table class="snstable">
				<tr>
			<td><img src="img/twitterlogin.png" alt="" width="30" height="30" align="middle"/></td> <td><a href="/member/twitter/twitter_login.php"> Twitterアカウントでログイン</a></td>
				</tr>

				<tr>
          <td><img src="img/facebooklogin.png" alt="" width="30" height="30" align="middle"/></td>
          <td>
          <?php
            // $facebook->getLoginUrl()でログインのURLを生成します。
            $params = array('redirect_uri' => $redirect_uri);
          ?>
          <a href="/member/facebook/facebook_login.php"> Facebookアカウントでログイン</a>
          </td>
				</tr>
			</table>
		</div>
	</div>

	<div class="loginboxbottom">
	
	</div>
    
    </div>
    <!--/loginbox-->
  


<?php /* /_/_/_/_/_/_/_/_/_/_/_/_/_ /end /_/_/_/_/_/_/_/_/_/_/_/_/_ /*/ ?>
  
  </div>
  <!--contentsin-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footerloginpage.php"); ?>
</div><!--container-->

</body>
</html>
