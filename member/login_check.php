<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	$meta_title = "ログイン";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>
	<link href="./css/login.css" rel="stylesheet" type="text/css" />

</head>

<body>
<?
	$datetime = date("Y-m-d H:i:s");
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}
	
	if($login_email=="" || $login_pw=="")
	{
		$common_connect-> Fn_javascript_back("正しく入力して下さい。");
	}
	

	//ログインチェック
	$arr_where = array();
	$var = "login_email";
	$arr_where[$var] = $$var;
	$var = "login_pw";
	$arr_where[$var] = $$var;
	$arr_where["flag_open"] = "1";
	$result_login = $common_member->Fn_login ($arr_where);
	
	if($result_login)
	{
		$flag_open = $result_login["0"]["flag_open"];
		$db_login_email = $result_login["0"]["login_email"];
		$db_member_id = $result_login["0"]["member_id"];
		$db_nickname = $result_login["0"]["nickname"];
	}
	else
	{
		$common_connect->Fn_javascript_back("IDとパスワードを確認してください。");
	}

	if($flag_open!="1")
	{
		$common_connect->Fn_javascript_back("ログイン権限がありません。");
	}
	
	if ($db_login_email == $login_email)
	{
		//ログイン
		session_start();
		$_SESSION['member_id']=$db_member_id;
		$_SESSION['nickname']=$db_nickname;
		setcookie("pck", "", time()-3600, "/");
		
		$arr_where = array();
		$arr_where["member_id"] = $db_member_id;
		
		//最終ログイン
		$arr_data = array();
		$arr_data["lastlogin_date"] = $datetime;
		$common_member->Fn_member_update ($arr_data, $arr_where);

		if($db_member_id!="" && $login_auto="on")
		{
			$arr_where = array();
			$arr_where["member_id"] = $db_member_id;
			
			$login_cookie = sha1($db_member_id."_".$db_login_email."_".$login_pw);
			$arr_data = array();
			$arr_data["member_login_cookie"] = $login_cookie;
			$common_member->Fn_member_update ($arr_data, $arr_where);
			
	
			setcookie("pck", $login_cookie, time()+3600*24*31, "/");//暗号化してクッキーに保存, 31日間
		}
		
		if($return_url=="" || $return_url=="/member/")
		{
			$return_url="/kanto/";
		}
		
		$common_connect->Fn_redirect(global_ssl.$return_url);

	}
?>

</body>

</html>
