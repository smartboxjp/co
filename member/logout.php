<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	$meta_title = "ログアウト";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>
</head>

<body>
<?

	session_start();
	//session_destroy();
	$member_id = $_SESSION['member_id'];
	$_SESSION['member_id']="";
	$_SESSION['nickname']="";

	setcookie("pck", "", time() - 1800, "/");
	
	if($member_id!="")
	{
		$arr_where = array();
		$arr_where["member_id"] = $member_id;
		$arr_data = array();
		$arr_data["member_login_cookie"] = "";
		$common_member->Fn_member_update ($arr_data, $arr_where);
	}

	echo ("<meta http-equiv='Refresh' content='0; URL=/".$global_area."/'>");

?>

</body>

</html>
