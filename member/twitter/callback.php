<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMemberTemp.php";
	$common_member_temp = new CommonMemberTemp();
		
	//ライブラリを読み込む	
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_include/twitter_src/autoload.php';
	
	use Abraham\TwitterOAuth\TwitterOAuth;
	
	//oauth_tokenとoauth_verifierを取得
	if($_SESSION['oauth_token'] == $_GET['oauth_token'] and $_GET['oauth_verifier']){
		
		//Twitterからアクセストークンを取得する
		$connection = new TwitterOAuth(Consumer_Key, Consumer_Secret, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
		$access_token = $connection->oauth('oauth/access_token', array('oauth_verifier' => $_GET['oauth_verifier'], 'oauth_token'=> $_GET['oauth_token']));
	
		//取得したアクセストークンでユーザ情報を取得
		$user_connection = new TwitterOAuth(Consumer_Key, Consumer_Secret, $access_token['oauth_token'], $access_token['oauth_token_secret']);
		//$user_info = $user_connection->get('account/verify_credentials');	
		$user_info = $user_connection->get("account/verify_credentials", ['include_entities'=> 'false', 'include_email'=> 'true']);
		
		// ユーザ情報の展開
		//var_dump($user_info);
		
		//適当にユーザ情報を取得
		/*
		$id = $user_info->id;
		$name = $user_info->name;
		$screen_name = $user_info->screen_name;
		$profile_image_url_https = $user_info->profile_image_url_https;
		$text = $user_info->status->text;
		$email = $user_info->status->email;
		var_dump($user_info);
		*/
		$login_email = $user_info->email;
		$twitter_id = $access_token["oauth_token"];
	}
	
	$meta_title = "Twitterの会員登録";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

</head>

<body>
<?
	$datetime = date("Y-m-d H:i:s");
	if($login_email == "" || $twitter_id == "")
	{
		$common_connect -> Fn_javascript_back("正しく入力して下さい。");
	}
		

	//すでに登録されていれば、ログイン成功
	$sql = "SELECT login_email FROM member where login_email='".$login_email."' ";
	$db_result = $common_dao->db_query_bind($sql);

	if($db_result)
	{
		$common_connect -> Fn_javascript_move("すでに登録されているメールです。\\r\\nログインしてください。", "/member/");
	}
	
	//すでに登録されていれば、ログイン成功
	$sql = "SELECT login_email, twitter FROM member where twitter='".$twitter_id."' ";
	$arr_member = $common_dao->db_query_bind($sql);
	
	
	//twitter_idで一致してるかをチェック
	if(!is_null($arr_member[0]))
	{
		if($login_email == $arr_member[0]["login_email"])
		{
			$common_connect -> Fn_javascript_move("すでに登録されているいます。\\r\\nログインしてください。", "/member/");
		}
		else
		{
			$common_connect -> Fn_javascript_move("メールが一致しません。\\r\\nSNS側で変更があった場合ログイン後メールを修正してください。", "/member/");
		}
	}
	//データがなければ会員登録をさせる
	else
	{
		//同じtwitter idがあれば削除
		$where .= "  1  ".$where;
		$db_up = "Delete from member_temp where twitter='".$twitter_id."'";
		$db_result = $common_dao->db_update($db_up);

		
		//同じemailがあれば削除
		$db_up = "Delete from member_temp where login_email='".$login_email."'";
		$db_result = $common_dao->db_update($db_up);
		
		
		$temp_key = date("Ymd").$common_connect-> Fn_random_password(20);
		
		//仮登録を行う
		$arr_data = array();
		$arr_data["twitter"] = $twitter_id;
		$arr_data["login_email"] = $login_email;
		$arr_data["temp_key"] = $temp_key;
		$common_member_temp -> Fn_member_temp_insert_temp($arr_data);
		
		//本登録ページへ遷移
		$common_connect-> Fn_redirect(global_no_ssl."/member/entry_2.php?temp_key=".$temp_key);
	}
?>