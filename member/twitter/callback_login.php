<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMemberTemp.php";
	$common_member_temp = new CommonMemberTemp();
		
	//ライブラリを読み込む	
	require_once $_SERVER['DOCUMENT_ROOT'].'/app_include/twitter_src/autoload.php';
	
	use Abraham\TwitterOAuth\TwitterOAuth;
	
	//oauth_tokenとoauth_verifierを取得
	if($_SESSION['oauth_token'] == $_GET['oauth_token'] and $_GET['oauth_verifier']){
		
		//Twitterからアクセストークンを取得する
		$connection = new TwitterOAuth(Consumer_Key, Consumer_Secret, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
		$access_token = $connection->oauth('oauth/access_token', array('oauth_verifier' => $_GET['oauth_verifier'], 'oauth_token'=> $_GET['oauth_token']));
	
		//取得したアクセストークンでユーザ情報を取得
		$user_connection = new TwitterOAuth(Consumer_Key, Consumer_Secret, $access_token['oauth_token'], $access_token['oauth_token_secret']);
		//$user_info = $user_connection->get('account/verify_credentials');	
		$user_info = $user_connection->get("account/verify_credentials", ['include_entities'=> 'false', 'include_email'=> 'true']);
		
		// ユーザ情報の展開
		//var_dump($user_info);
		
		//適当にユーザ情報を取得
		/*
		$id = $user_info->id;
		$name = $user_info->name;
		$screen_name = $user_info->screen_name;
		$profile_image_url_https = $user_info->profile_image_url_https;
		$text = $user_info->status->text;
		$email = $user_info->status->email;
		var_dump($user_info);
		*/
		$login_email = $user_info->email;
		$twitter_id = $access_token["oauth_token"];
	}
	
	$meta_title = "Twitterのログイン";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

</head>

<body>
<?
	$datetime = date("Y-m-d H:i:s");
	
	if($login_email == "" || $twitter_id == "")
	{
		$common_connect -> Fn_javascript_back("正しく入力して下さい。");
	}
	
	//すでに登録されていれば、ログイン成功
	$arr_data = array();
	$arr_data[] = "member_id";
	$arr_data[] = "nickname";
	$arr_data[] = "flag_open";
	
	$arr_where = array();
	$arr_where["login_email"] = $login_email;
	$arr_where["twitter"] = $twitter_id;

	
	$arr_member = $common_member -> Fn_db_member_data_select ($arr_data, $arr_where) ;

	if(!is_null($arr_member[0]))
	{
		if($arr_member[0]["flag_open"]=="1")
		{
			//ログイン日
			$arr_where = array();
			$arr_where["member_id"] = $arr_member[0]["member_id"];
			$arr_data = array();
			$arr_data["lastlogin_date"] = $datetime;
			$common_member->Fn_member_update ($arr_data, $arr_where);
			
			//ログイン
			session_start();
			$_SESSION['member_id']=$arr_member[0]["member_id"];
			$_SESSION['nickname']=$arr_member[0]["nickname"];
			
			$common_connect-> Fn_javascript_move("ログインしました。", global_no_ssl."/".$global_area."/");
		}
		else
		{
			$common_connect -> Fn_javascript_back("ログインが禁止されているIDです。");
		}
	}
	else
	{
		$common_connect -> Fn_javascript_move("一致してるデータがありません。\\r\\n会員登録してください。", "/member/entry_1.php");
	}
	
?>

</body>

</html>