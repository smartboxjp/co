<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,user-scalable=no">
<meta name="description" content="" >
<meta name="keywords" content="" >
<link rel="stylesheet" type="text/css" href="/sp/kanto/css/import.css" media="all" >
<title>トップ||</title>
</head>

<body class="index">

<!--ドロワーメニュー-->
<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/sp/kanto/common/drawermenu.php"); ?>
<div id="wrapper">

<!--header-->
<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/sp/kanto/common/header.php"); ?>

<!--nav-->
<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/sp/kanto/common/header-nav.php"); ?>

<section id="contents">
  <div class="device">	
		<div class="swiper-container">
			<div class="swiper-wrapper">
				<div class="swiper-slide"><img alt="" src="/sp/kanto/img/1.jpg" /></div>
				<div class="swiper-slide"><img alt="" src="/sp/kanto/img/2.jpg" /></div>
				<div class="swiper-slide"><img alt="" src="/sp/kanto/img/3.jpg" /></div>
				<div class="swiper-slide"><img alt="" src="/sp/kanto/img/4.jpg" /></div>
		    </div>
	    </div>
		<a class="arrow-left" href="#"></a>
		<div class="pagination">&nbsp;</div>
		<a class="arrow-right" href="#"></a>			
	  </div>

      <form action=""><p class="searchField"><input type="text"  placeholder="キーワードから求人検索を検索する" name="" id=""><input type="submit" value=""></p></form> 

		<section id="indexRank">
			<h2 class="catTtl"><span>評価ランキング</span></h2>
			<div class="rankInner">

				<div class="swiper2">	
					<div class="swiper-wrapper">
					<!--1位-->
						<div class="swiper-slide">
                        	<a href="" class="boxLink02">
								<p class="order""><img class="rankNum" src="/sp/kanto/common/img/rank01.png" alt="1位"></p>						
								<p class="swipeImg"><img alt="" src="/sp/kanto/shop/img/test/d90.jpg" /></p>
								
								<dl>
									<dt>店名</dt>
									<dd>（渋谷/キャバクラ）</dd>
									<dd class="boxStar">
										<img src="/sp/kanto/common/img/starOn.png"  alt="">
										<img src="/sp/kanto/common/img/starOn.png"  alt="">
										<img src="/sp/kanto/common/img/starOn.png"  alt="">
										<img src="/sp/kanto/common/img/starOn.png"  alt="">
										<img src="/sp/kanto/common/img/starOn.png"  alt="">
										<span class="starNum">3.50<span>点</span></span></dd>
								</dl>
                            </a>
						</div>
						
						
					<!--２位-->
					
						<div class="swiper-slide">
                        	<a href="" class="boxLink02">
								<p class="order"><img class="rankNum"  src="/sp/kanto/common/img/rank02.png" alt="2位"></p>				
								<p><img alt="" src="/sp/kanto/shop/img/test/d90.jpg" /></p>
								<dl>
									<dt>店名</dt>
									<dd>（渋谷/キャバクラ）</dd>
                					<dd class="boxStar">
                						<img src="/sp/kanto/common/img/starOn.png"  alt="">
                						<img src="/sp/kanto/common/img/starOn.png"  alt="">
                						<img src="/sp/kanto/common/img/starOn.png"  alt="">
                						<img src="/sp/kanto/common/img/starOn.png"  alt="">
                						<img src="/sp/kanto/common/img/common/starOn.png"  alt="">
                						<span class="starNum">3.50<span>点</span></span>
                					</dd>
								</dl>			
                            </a>				
						</div>
                        
                        
                       <!--3位-->
                       
						<div class="swiper-slide">
                        	<a  href="" class="boxLink02">
								<p class="order"><img class="rankNum"  src="/sp/kanto/common/img/rank03.png" alt="3位"></p>
								<p><img alt="" src="/sp/kanto/shop/img/test/d90.jpg" /></p>
								<dl>
									<dt>店名</dt>
									<dd>（渋谷/キャバクラ）</dd>
                					<dd class="boxStar">
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt="">
                						<span class="starNum">3.50<span>点</span></span>
                					</dd>
								</dl>
							</a>					
						</div>
						
						<!--4位-->
						
						<div class="swiper-slide">
                        	<a  href="" class="boxLink02">
								<p class="order"><img class="rankNum"  src="/sp/kanto/common/img/rank04.png" alt="4位"></p>
								<p><img alt="" src="/sp/kanto/shop/img/test/d90.jpg" /></p>
								<dl>
									<dt>店名</dt>
									<dd>（渋谷/キャバクラ）</dd>
                					<dd class="boxStar">
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt="">
                						<span class="starNum">3.50<span>点</span></span>
                					</dd>
								</dl>
							</a>					
						</div>
						
						<div class="swiper-slide">
                        	<a  href="" class="boxLink02">
								<p class="order"><img class="rankNum"  src="/sp/kanto/common/img/rank05.png" alt="5位"></p>
								<p><img alt="" src="/sp/kanto/shop/img/test/d90.jpg" /></p>
								<dl>
									<dt>店名</dt>
									<dd>（渋谷/キャバクラ）</dd>
                					<dd class="boxStar">
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt="">
                						<span class="starNum">3.50<span>点</span></span>
                					</dd>
								</dl>
							</a>					
						</div>
						<div class="swiper-slide">
                        	<a  href="" class="boxLink02">
								<p class="order"><img class="rankNum"  src="/sp/kanto/common/img/rank06.png" alt="6位"></p>
								<p><img alt="" src="/sp/kanto/shop/img/test/d90.jpg" /></p>
								<dl>
									<dt>店名</dt>
									<dd>（渋谷/キャバクラ）</dd>
                					<dd class="boxStar">
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt="">
                						<span class="starNum">3.50<span>点</span></span>
                					</dd>
								</dl>
							</a>					
						</div>
						<div class="swiper-slide">
                        	<a  href="" class="boxLink02">
								<p class="order"><img class="rankNum"  src="/sp/kanto/common/img/rank07.png" alt="7位"></p>
								<p><img alt="" src="/sp/kanto/shop/img/test/d90.jpg" /></p>
								<dl>
									<dt>店名</dt>
									<dd>（渋谷/キャバクラ）</dd>
                					<dd class="boxStar">
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt="">
                						<span class="starNum">3.50<span>点</span></span>
                					</dd>
								</dl>
							</a>					
						</div>
						<div class="swiper-slide">
                        	<a  href="" class="boxLink02">
								<p class="order"><img class="rankNum"  src="/sp/kanto/common/img/rank08.png" alt="8位"></p>
								<p><img alt="" src="/sp/kanto/shop/img/test/d90.jpg" /></p>
								<dl>
									<dt>店名</dt>
									<dd>（渋谷/キャバクラ）</dd>
                					<dd class="boxStar">
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt="">
                						<span class="starNum">3.50<span>点</span></span>
                					</dd>
								</dl>
							</a>					
						</div>
						<div class="swiper-slide">
                        	<a  href="" class="boxLink02">
								<p class="order"><img class="rankNum"  src="/sp/kanto/common/img/rank09.png" alt="9位"></p>
								<p><img alt="" src="/sp/kanto/shop/img/test/d90.jpg" /></p>
								<dl>
									<dt>店名</dt>
									<dd>（渋谷/キャバクラ）</dd>
                					<dd class="boxStar">
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt=""> 
                						<img src="/sp/kanto/common/img/starOn.png"  alt="">
                						<span class="starNum">3.50<span>点</span></span>
                					</dd>
								</dl>
							</a>					
						</div>
																													
					</div>
					</div>
					<a class="arrow-right2" href="#"></a>
					
				</div>
				<a  class="more boxLink02" href="ranking/index.html"><span>他のランキングをもっと見る</span></a>
		</section>


  <section class="i30  pickup">
    <h2 class="catTtl"><span>ピックアップ求人</span></h2>
    <div class="baseBox higherOrder">
      <a class="boxLink" href="">
        <figure class="boxImg">
          <img src="images/common/w30.jpg" alt="">
        </figure>
        <div class="boxCnt">
          <p class="shopName">店名店名店名店名店名店名店名店名店名店名</p>
          <p class="areaName">（東京渋谷エリア/キャバクラ）</p>
          <p class="boxStar"><img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""><span class="starNum">3.50<span>点</span></span></p>  
        </div>
        </a>
      </div>
    <div class="baseBox higherOrder">
      <a class="boxLink" href="">
        <figure class="boxImg">
          <img src="images/common/w30.jpg" alt="">
        </figure>
        <div class="boxCnt">
          <p class="shopName">店名店名店名店名店名店名店名店名店名店名</p>
          <p class="areaName">（東京渋谷エリア/キャバクラ）</p>
          <p class="boxStar"><img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""><span class="starNum">3.50<span>点</span></span></p>  
        </div>
        </a>
      </div>
    <div class="baseBox higherOrder">
      <a class="boxLink" href="">
        <figure class="boxImg">
          <img src="images/common/w30.jpg" alt="">
        </figure>
        <div class="boxCnt">
          <p class="shopName">店名店名店名店名店名店名店名店名店名店名</p>
          <p class="areaName">（東京渋谷エリア/キャバクラ）</p>
          <p class="boxStar"><img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""><span class="starNum">3.50<span>点</span></span></p>  
        </div>
        </a>
      </div>    
  </section>

  <section class="i20 new">
    <h2 class="catTtl"><span>新着口コミ一覧</span></h2>
    <div class="baseBox baseColor">
      <a class="boxLink" href="">
        <figure class="boxImg">
          <img src="images/common/img20.jpg" alt="">
          <figcaption class="commName">名前名前名前名前</figcaption>
        </figure>
        <div class="boxCnt hukidashiWrap">
          <div class="hukidashi">
            <div class="hukidashiInner">
              <p>テキストテキストテキストテキス</p>
              <p class="shopName">店名店名店名店名店名店名店名店名店名</p>
              <p class="areaName">（東京渋谷エリア/キャバクラ）</p>
              <p class="boxStar"><img src="images/common/starOn.png"  alt=""><img src="images/common/starOn.png"  alt=""><img src="images/common/starOn.png"  alt=""><img src="images/common/starOn.png"  alt=""><img src="images/common/starOn.png"  alt=""><span class="starNum">3.50<span>点</span></span></p>
            </div>
          </div>
          <p class="date">2014/08/05投稿</p>
        </div>
        </a>
      </div> 
    <div class="baseBox baseColor">
      <a class="boxLink" href="">
        <figure class="boxImg">
          <img src="images/common/img20.jpg" alt="">
          <figcaption class="commName">名前名前名前名前</figcaption>
        </figure>
        <div class="boxCnt hukidashiWrap">
          <div class="hukidashi">
            <div class="hukidashiInner">
              <p>テキストテキストテキストテキス</p>
              <p class="shopName">店名店名店名店名店名店名店名店名店名</p>
              <p class="areaName">（東京渋谷エリア/キャバクラ）</p>
              <p class="boxStar"><img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""><span class="starNum">3.50<span>点</span></span></p>
            </div>
          </div>
          <p class="date">2014/08/05投稿</p>        
        </div>
        </a>
      </div> 
    <div class="baseBox baseColor">
      <a class="boxLink" href="">
        <figure class="boxImg">
          <img src="images/common/img20.jpg" alt="">
          <figcaption class="commName">名前名前名前名前</figcaption>
        </figure>
        <div class="boxCnt hukidashiWrap">
          <div class="hukidashi">
            <div class="hukidashiInner">
              <p>テキストテキストテキストテキス</p>
              <p class="shopName">店名店名店名店名店名店名店名店名店名</p>
              <p class="areaName">（東京渋谷エリア/キャバクラ）</p>
              <p class="boxStar"><img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""> <img src="images/common/starOn.png"  alt=""><span class="starNum">3.50<span>点</span></span></p>
            </div>
          </div>
          <p class="date">2014/08/05投稿</p>        
        </div>
        </a>
      </div>
    <p><a  class="more boxLink02" href=""><span>もっと新着情報を見る</span></a></p>          
  </section>

  <section class="i30 blogInfo">
    <h2 class="catTtl"><span>最新ブログ情報</span></h2>
    <div class="baseBox baseColor">
      <a class="boxLink" href="">
        <figure class="boxImg">  
          <img src="images/common/w30.jpg" alt="">
        </figure>
        <div class="boxCnt boxBorder">
          <p>タイトルタイトルタイトルタイトタイトルタイトルタイトルタイト</p>
          <p class="shopName">店名店名店名店名店名店名店名店名店名店名</p>
          <p class="areaName">（東京渋谷エリア/キャバクラ）</p>
        </div>
        <p class="date">2014/08/05投稿</p>
        </a>
      </div> 
    <div class="baseBox baseColor">
      <a class="boxLink" href="">
        <figure class="boxImg">  
          <img src="images/common/w30.jpg" alt="">
        </figure>
        <div class="boxCnt boxBorder">
          <p>タイトルタイトルタイトルタイトタイトルタイトルタイトルタイト</p>
          <p class="shopName">店名店名店名店名店名店名店名店名店名店名</p>
          <p class="areaName">（東京渋谷エリア/キャバクラ）</p>
        </div>
        <p class="date">2014/08/05投稿</p>
        </a>
      </div> 
    <div class="baseBox baseColor">
      <a class="boxLink" href="">
        <figure class="boxImg">  
          <img src="images/common/w30.jpg" alt="">
        </figure>
        <div class="boxCnt boxBorder">
          <p>タイトルタイトルタイトルタイトタイトルタイトルタイトルタイト</p>
          <p class="shopName">店名店名店名店名店名店名店名店名店名店名</p>
          <p class="areaName">（東京渋谷エリア/キャバクラ）</p>
        </div>
        <p class="date">2014/08/05投稿</p>
        </a>
      </div>        
    <p><a  class="more boxLink02" href=""><span>もっと新着情報を見る</span></a></p>
  </section>

  <section class="topics">
    <h2 class="catTtl"><span>トピックス</span></h2>
    <ul>
      <li><a href="" class="boxLink02"><span><img src="images/common/topics.jpg" alt=""></span>テキストテキストテキストテキストテキスト</img></a></li>
      <li><a href="" class="boxLink02"><span><img src="images/common/topics.jpg" alt=""></span>テキストテキストテキストテキストテキスト</img></a></li>
      <li><a href="" class="boxLink02"><span><img src="images/common/topics.jpg" alt=""></span>テキストテキストテキストテキストテキスト</img></a></li>
      <li><a href="" class="boxLink02"><span><img src="images/common/topics.jpg" alt=""></span>テキストテキストテキストテキストテキスト</img></a></li>   
      <li><a href="" class="boxLink02"><span><img src="images/common/topics.jpg" alt=""></span>テキストテキストテキストテキストテキスト</img></a></li>
      <li><a href="" class="boxLink02"><span><img src="images/common/topics.jpg" alt=""></span>テキストテキストテキストテキストテキスト</img></a></li>         
      </ul>
  </section> 

  <p class="bnrSpace"><a href="" class="boxLink02"><img src="images/bnrKari.png" alt=""></a></p>

  <section class="movie">
    <h2 class="catTtl"><span>ピックアップ動画</span></h2>
    <div class="movieBody">
      <div class="movieTop">
        <p><img src="images/moiveKariL.jpg"alt=""></p>
        <p class="shopName">店名店名店名店名店名店名店名店名店名店名</p>
        <p class="date">2014/08/05投稿</p>
        </div>
      <ul>
        <li><img src="images/movieKariM.jpg" alt=""></li>
        <li><img src="images/movieKariM.jpg" alt=""></li>
        <li><img src="images/movieKariM.jpg" alt=""></li>
        <li><img src="images/movieKariM.jpg" alt=""></li>
        </ul>
      </div>
    <p><a  class="more" href=""><span>もっと動画を見る</span></a></p>
  </section>

  <nav class="searchBox">
    <section>  
      <h2 class="areaTtl searchTtl"><span>地域から求人を検索する</span></h2>
      <ul class="baseNav navHarf arrowLink">
        <li><a href="">東京エリア</a></li>
        <li><a href="">神奈川エリア</a></li>
        <li><a href="">千葉エリア</a></li>
        <li><a href="">埼玉エリア</a></li>
        <li><a href="">栃木エリア</a></li>
        <li><a href="">茨城エリア</a></li>
        <li><a href="">群馬エリア</a></li>
        <li><a href="">すべての地域から</a></li>
        </ul>
      </section>
    <section>
      <h2 class="typeTtl searchTtl"><span>業種から求人を検索する</span></h2>
      <ul class="baseNav navHarf arrowLink">
        <li><a href="">テキスト</a></li>
        <li><a href="">テキスト</a></li>
        <li><a href="">テキスト</a></li>
        <li><a href="">テキスト</a></li>
        <li><a href="">テキスト</a></li>
        <li><a href="">テキスト</a></li>
        <li><a href="">テキスト</a></li>
        <li><a href="">テキスト</a></li>
        </ul>
      </section>
    <section>
      <h2 class="recoTtl searchTtl"><span>オススメの求人検索</span></h2>
      <ul class="baseNav arrowLink">
        <li><a href="">評価の高い求人から探す</a></li>
        <li><a class="recoLink" href="">PR店から求人を探す<img src="images/common/icoReco.png" alt="おすすめ"></a></li>
        <li><a href="">新着店舗から求人を探す</a></li>
        <li><a href="">人気エリアから求人を探す</a></li>
        </ul>
      </section>
  </nav>

  <div class="searchWrap"><form action=""><p class="searchField"><input type="text"  placeholder="キーワードから求人検索を検索する" name="" id=""><input type="submit" value=""></p></form></div>

  <section class="info">
    <h2>編集部からのお知らせ</h2>
    <ul>
      <li><a href="">2014/08/05</a><a href="">テキストテキストテキスト</a></li>
      <li><a href="">2014/08/05</a><a href="">テキストテキストテキスト</a></li>
      <li><a href="">2014/08/05</a><a href="">テキストテキストテキスト</a></li>
      </ul>
    <p><a  class="more" href=""><span>お知らせ一覧</span></a></p>    
  </section>

</section><!--contents-->

   <p class="pageTop"><a href="#wrapper"><span>ページトップ</span></a></p>
   <p class="bnrSpace"><a href=""><img src="images/common/middleBnr.jpg"  alt=""></a></p>

<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/sp/kanto/common/footer.php"); ?>


</div><!--wrapper-->
<link rel="stylesheet" type="text/css" href="/sp/kanto/css/idangerous.swiper.css">
<script type="text/javascript" src="/sp/kanto/js/jquery.js"></script>
<script type="text/javascript" src="/sp/kanto/js/idangerous.swiper.js"></script>
<script type="text/javascript" src="/sp/kanto/js/common.js"></script> <script type="text/javascript">

  var mySwiper = new Swiper('.swiper-container',{
		autoplay:10000,
    pagination: '.pagination',
    loop:true,
    grabCursor: true,
    paginationClickable: true
  })
	
		
  $('.arrow-left').on('click', function(e){
    e.preventDefault()
    mySwiper.swipePrev()
  })
  $('.arrow-right').on('click', function(e){
    e.preventDefault()
    mySwiper.swipeNext()
  })
	
  $('.arrow-right2').on('click', function(e){
    e.preventDefault()
    Swiper2.swipeNext()
  })	
 
  var Swiper2 = new Swiper('.swiper2',{
    loop:true,
    grabCursor: true,
		slidesPerView: 3,
    spaceBetween: 10
  })

</script>
</body>
</html>
