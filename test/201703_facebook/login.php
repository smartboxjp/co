<?
require_once $_SERVER['DOCUMENT_ROOT']."/app_include/facebook_src/autoload.php";

session_start();
$_SESSION["facebook"] = "facebook";
$fb = new Facebook\Facebook([
  'app_id' => '588305291330356', // Replace {app-id} with your app id
  'app_secret' => '9e6b3c715274a9571ffa0e6e4782e505',
  'default_graph_version' => 'v2.2',
  'persistent_data_handler' => 'session',
  ]);

$helper = $fb->getRedirectLoginHelper();

$permissions = ['email']; // Optional permissions
$loginUrl = $helper->getLoginUrl('http://www.cossot.com/test/201703_facebook/fb-callback.php', $permissions);

echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
?>