<?php
require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_include/connect.php";
$common_connect = new CommonConnect();
$common_dao = new CommonDao(); //DB関連

$meta_title = "タイトル";
$meta_description = "description です";
$meta_keywords = "キーワードです";
?>

<?php require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/kanto/common/header/header_meta.php";?>


<!--アイコン用CSS-->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!--専用のCSS-->
<link href="/kanto/user/css/style_admin.css" rel="stylesheet" type="text/css"/>

<!--プログレスバー-->
<link href="/kanto/app/pie-chart/plugins.css" rel="stylesheet" type="text/css"/>


</head>

<body>

	<!-- Page content -->
	<div id="page-content">


		<!-- Dashboard 2 Content -->
		<div class="row">


			<!--左-->
			<div class="col-md-2">

				<div class="block">



					<!---->
					<div class="wizard-steps">
						<div class="row">
							<div class="col-xs-12 active">
								<!--無料の場合activeをはずす-->
								<span>有料プラン</span>
							</div>
						</div>
					</div>
					<!---->

				
					<div class="widget">
						<div class="indexchart">
							<p><i class="fa fa-bar-chart"></i>現在の平均点</p>
						</div>
						<div class="widget-extra-full text-center">
							<div class="pie-chart" data-percent="62" data-size="90" data-bar-color="#1bbae1">
								<span>3.17点</span>
							</div>
						</div>
					</div>


					<div class="widget">
						<div class="indexchart">
							<p><i class="fa fa-bar-chart"></i>昨日のアクセス数</p>
						</div>
						<div class="widget-extra-full text-center">
							<div class="pie-chart" data-percent="100" data-size="90" data-bar-color="#1bbae1">
								<span>14574</span>
							</div>
						</div>
					</div>

					<div class="widget">
						<div class="indexchart">
							<p><i class="fa fa-comments-o"></i>お店の口コミ数</p>
						</div>
						<div class="widget-extra-full text-center">
							<div class="pie-chart" data-percent="45" data-size="90" data-bar-color="#1bbae1">
								<span>22件</span>
							</div>
						</div>
					</div>


					<div class="widget">
						<div class="indexchart">
							<p><i class="fa fa-picture-o"></i>お店の関連画像数</p>
						</div>
						<div class="widget-extra-full text-center">
							<div class="pie-chart" data-percent="45" data-size="90" data-bar-color="#1bbae1">
								<span>7枚</span>
							</div>
						</div>
					</div>



				</div>

			</div>








		</div>


	</div>

	<!--プログレス-->
	<script src="/app_manager/js/vendor/bootstrap.min.js"></script>
	
	<script type="text/javascript" src="/kanto/app/pie-chart/plugins.js"></script>
	<script type="text/javascript" src="/kanto/app/pie-chart/app.js"></script>
	
	<!--
	<script type="text/javascript" src="/kanto/app/bootstrap/bootstrap.min.css"></script>
	<script type="text/javascript" src="/kanto/app/pie-chart/plugins.js"></script>
	<script type="text/javascript" src="/kanto/app/pie-chart/app.js"></script>
	-->


	<!--プログレス-->
	

</body>
</html>