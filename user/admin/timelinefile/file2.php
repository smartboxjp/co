
										
											<ul class="timelist">

												<!---まとめが投稿されました-->
												<li class="timeblok">
													<div class="timeline_body">
														<div class="timeline_icon">
															<img src="/kanto/user/img/icon/matome.png" width="50px" class="pull-left">
															<div class="pull-left">
																<div class="timeline_title">まとめ記事が投稿されました。fufu</div>
																<p class="time-watch"><i class="fa fa-clock-o"></i>今のニュース</p>
															</div>
															
															<div class="boxclear"></div>
															

															
														</div>

													</div>
													
													
													<div class="matomebottom">
														<span class="matome-type">（ファッション）</span>
														<p class="matome-title"><a href="">まとめタイトルが最大で30文字まで表示されますよ</a></p>
														<div class="matomeimg-blok">
															<a href="" target="_blank">
																<img src="/kanto/beautymatome/deta/abc.jpg">
															</a>
													
														</div>
													

													</div>
													



												</li>
												<!---まとめが投稿されました-->


												<!---信憑性-->
												<li class="timeblok">
													<div class="timeline_body">

														<div class="timeline_icon">
															<img src="/kanto/user/img/icon/voiserank.png" width="50px" class="pull-left">
															<div class="pull-left">
																<div class="timeline_title">信憑性がつきました</div>
																<p class="time-watch"><i class="fa fa-clock-o"></i>30分以内のニュース</p>
															</div>
															<div class="boxclear"></div>
															<div class="timelinecoment">
																<span class="userimg">
																		<img src="/kanto/user/img/test/adm_photo.gif">
																</span>
																あなたの投稿した口コミに信憑性がつきました。
															</div>
														</div>

													</div>

													<div class="timeline_footer">
														<div class="stamped">
															<div class="voise-rank">
																<img src="/kanto/common/img/ranksa.png">
															</div>
														</div>

														<a href="" target="_blank">
																<img src="/kanto/user/img/test/sanple1.jpg" width="70px" class="pull-left">
															</a>
													


														<div class="footcoment">
															<div class="shopname">
																<a href="" target="_blank">お店の名前が表示されます。</a>
															</div>
															<div class="sterimg">
																<img src="/kanto/common/img/star1.0s.png">
															</div>
															<div class="commenttitle">
																<a href="" target="_blank">口コミのタイトルが表示されます</a>
															</div>
														</div>
														<div class="boxclear"></div>

													</div>

												</li>

												<!---信憑性-->



												<!---インフォメーション-->
												<li class="timeblok">
													<div class="timeline_body">
														<div class="timeline_icon">
															<img src="/kanto/user/img/icon/info.png" width="50px" class="pull-left">
															<div class="pull-left">
																<div class="timeline_title">お知らせ</div>
																<p class="time-watch"><i class="fa fa-clock-o"></i>半日以内のニュース</p>
															</div>
															<div class="boxclear"></div>
															<div class="timelinecoment">
																<span class="shopimg">
																<img src="/kanto/user/img/icon/caution.png">コソットからお知らせです。
																</span>
															


															</div>
														</div>

													</div>

													<div class="timeline_footer">
														<a href="" class="infotext">新しく関西版がオープンしました。</a>
													</div>

												</li>
												<!---インフォメーション-->


												<!---ランキング-->
												<li class="timeblok">
													<div class="timeline_body">
														<div class="timeline_icon">
															<img src="/kanto/user/img/icon/ranking.png" width="50px" class="pull-left">
															<div class="pull-left">
																<div class="timeline_title">ランキング更新</div>
																<p class="time-watch"><i class="fa fa-clock-o"></i>三日前のニュース</p>
															</div>
															<div class="boxclear"></div>
															<div class="timelinecoment">
																<span class="shopimg">
																<img src="/kanto/user/img/icon/caution.png">ランキングが更新されました。
																</span>
															
															</div>
														</div>

													</div>

													<div class="timeline_footer">
														<table class="ranktable">
															<caption>平均点ランキング<span class="ranking-date">2017/08/25更新</span>
															</caption>
															<tr>
																<th>1位</th>
																<td class="table-shop">
																	<a href="" target="_blank">お店の名</a>
																</td>
																<td><span class="rankingpoint">3.15点</span>
																</td>
																<td class="arrow"><i class="fa fa-arrow-up"></i>
																</td>
															</tr>
															<tr>
																<th>2位</th>
																<td class="table-shop">
																	<a href="" target="_blank">名前がはいります。</a>
																</td>
																<td><span class="rankingpoint">3.15点</span>
																</td>
																<td class="arrow"><i class="fa fa-arrow-right"></i>
																</td>
															</tr>
															<tr>
																<th>3位</th>
																<td class="table-shop">
																	<a href="" target="_blank">こここ</a>
																</td>
																<td><span class="rankingpoint">3.15点</span>
																</td>
																<td class="arrow"><i class="fa fa-arrow-down"></i>
																</td>
															</tr>
														</table>
														<div class="ranknext"><a href="">全てのランキングを見る</a>
														</div>
														<div class="boxclear"></div>
													</div>

												</li>

												<!---ランキング-->

												<!---新しいお店-->
												<li class="timeblok">
													<div class="timeline_body">
														<div class="timeline_icon">
															<img src="/kanto/user/img/icon/shop.png" width="50px" class="pull-left">
															<div class="pull-left">
																<div class="timeline_title">新しいお店が登録されました。</div>
																<p class="time-watch"><i class="fa fa-clock-o"></i>2時間以内のニュース</p>
															</div>
															<div class="boxclear"></div>
															<div class="timelinecoment">
																<span class="shopimg">
																<img src="/kanto/user/img/icon/caution.png">登録エリアに新しいお店が登録されました。
																</span>
															

															</div>

														</div>

													</div>

													<div class="timeline_footer">
														<a href="" target="_blank">
																<img src="/kanto/user/img/test/sanple1.jpg" width="70px" class="pull-left">
															</a>
													




														<div class="footcoment">
															<div class="shopname">
																<a href="" target="_blank">お店の名前が表示されます。</a>
															</div>
															<div class="sterimg">
																<img src="/kanto/common/img/star1.0s.png">
																<span class="newshop-point">【3.14点】</span>
															</div>
															<div class="jobtype">
																（デリバリーヘルス）
															</div>
														</div>
														<div class="boxclear"></div>

													</div>

												</li>
												<!---新しいお店-->

												<!---画像投稿-->
												<li class="timeblok">
													<div class="timeline_body">
														<div class="timeline_icon">
															<img src="/kanto/user/img/icon/img.png" width="50px" class="pull-left">
															<div class="pull-left">
																<div class="timeline_title">お店に関連画像が投稿されました。</div>
																<p class="time-watch"><i class="fa fa-clock-o"></i>1時間以内のニュース</p>
															</div>
															<div class="boxclear"></div>
															<div class="timelinecoment">
																<span class="userimg">
																	<a href="" target="_blank">
																		<img src="/kanto/user/img/test/adm_photo.gif">
																		ミスリズリサさん
																	</a>
																</span>
																が登録エリアのお店に関連画像を投稿しました。
															</div>

														</div>

													</div>

													<div class="timeline_footer">
														<div class="img-frame">
															<div class="samimg-timeline">
																<a href="/kanto/user/img/test/111.jpg" rel="lightbox" title="コメントが最大で10文字">
																	<img src="/kanto/user/img/test/123.jpg">
																</a>
															

																<div class="img-shopname"><a href="">お店の名前が最大で20文字まで</a>
																</div>
															</div>
														</div>
													</div>

												</li>

												<!---画像投稿-->

												<!---口コミ投稿-->
												<li class="timeblok">
													<div class="timeline_body">
														<div class="timeline_icon">
															<img src="/kanto/user/img/icon/comment.png" width="50px" class="pull-left">
															<div class="pull-left">
																<div class="timeline_title">お店に口コミが投稿されました。</div>
																<p class="time-watch"><i class="fa fa-clock-o"></i>30分以内のニュース</p>
															</div>
															<div class="boxclear"></div>
															<div class="timelinecoment">
																<span class="userimg">
																	<a href="" target="_blank">
																		<img src="/kanto/user/img/test/adm_photo.gif">
																		ミスリズリサさん
																	</a>
																</span>
																が登録エリアのお店に口コミを投稿しました。
															</div>
														</div>

													</div>

													<div class="timeline_footer">

														<a href="" target="_blank">
																<img src="/kanto/user/img/test/sanple1.jpg" width="70px" class="pull-left">
															</a>
													


														<div class="footcoment">
															<div class="shopname">
																<a href="" target="_blank">お店の名前が表示されます。</a>
															</div>
															<div class="sterimg">
																<img src="/kanto/common/img/star1.0s.png">
															</div>
															<div class="commenttitle">
																<a href="" target="_blank">口コミのタイトルが表示されます</a>
															</div>
														</div>
														<div class="boxclear"></div>

													</div>

												</li>

												<!---口コミ投稿-->



												<!---登録エリアのお店の点数が変化-->
												<li class="timeblok">
													<div class="timeline_body">
														<div class="timeline_icon">
															<img src="/kanto/user/img/icon/shopchart.png" width="50px" class="pull-left">
															<div class="pull-left">
																<div class="timeline_title">お店の点数が変更されました。</div>
																<p class="time-watch"><i class="fa fa-clock-o"></i>今のニュース</p>
															</div>
															<div class="boxclear"></div>
															<div class="timelinecoment">
																<span class="shopimg">
																<img src="/kanto/user/img/icon/caution.png">登録エリアに掲載されてるお店の点数に変動がありました。
																</span>
															
															</div>
														</div>

													</div>

													<div class="timeline_footer">
														<a href="" target="_blank">
															<img src="/kanto/user/img/test/sanple1.jpg" width="70px" class="pull-left">
														</a>
													

														<div class="footcoment">
															<div class="shopname">
																<a href="" target="_blank">お店の名前が表示されます。</a>
															</div>
															<p>登録エリアに掲載されてる店名が表示..の平均点が<span class="kutikomipoint">【3.15点】→【3.17点】
															</span> に変更されました。</p>
														</div>
														<div class="boxclear"></div>
													</div>

												</li>

												<!---登録エリアのお店の点数が変化-->



												<!---メールが来てます-->
												<li class="timeblok">
													<div class="timeline_body">
														<div class="timeline_icon">
															<img src="/kanto/user/img/icon/mail.png" width="50px" class="pull-left">
															<div class="pull-left">
																<div class="timeline_title">返信がきてます。</div>
																<p class="time-watch"><i class="fa fa-clock-o"></i>半日以内のニュース</p>
															</div>
															<div class="boxclear"></div>
															<div class="timelinecoment">
																<span class="shopimg">
																<img src="/kanto/user/img/icon/caution.png">メールしたお店から返信が来てます。
																</span>
															




															</div>
														</div>

													</div>

													<div class="timeline_footer">
														<a href="" target="_blank">
															<img src="/kanto/user/img/test/sanple1.jpg" width="70px" class="pull-left">
														</a>
													




														<div class="footcoment">
															<div class="shopname">
																<a href="" target="_blank">デリバリーヘルス庵</a>から返信が届いてます。
															</div>
															<div class="mailtitle">
																※早めの返信をお願いします。

															</div>
														</div>
														<div class="boxclear"></div>
													</div>

												</li>
												<!---メールが来てます-->


												<!---PR-->
												<li class="timeblok">
													<div class="timeline_body">
														<div class="timeline_icon">
															<div class="timelinecoment-pr">
																<span class="shopimg">
																<img src="/kanto/user/img/icon/caution.png">PR
																</span>
															
															</div>
														</div>
														<div class="pr-shopname">
															<a href="" target="_blank">
																お店の名前が表示されます。
															</a>
														
														</div>

													</div>

													<div class="prblok">
														<a href="" target="_blank">
														<img src="/kanto/shop/img/test/shop1.jpg">
														</a>
													
													</div>


												</li>
												<!---PR-->



												<!---メールが来てます-->
												<li class="timeblok">
													<div class="timeline_body">
														<div class="timeline_icon">
															<img src="/kanto/user/img/icon/mail.png" width="50px" class="pull-left">
															<div class="pull-left">
																<div class="timeline_title">メールがきてます。</div>
																<p class="time-watch"><i class="fa fa-clock-o"></i>半日以内のニュース</p>
															</div>
															<div class="boxclear"></div>
															<div class="timelinecoment">
																<span class="shopimg">
																<img src="/kanto/user/img/icon/caution.png">コソットからメールが来てます。
																</span>
															




															</div>
														</div>

													</div>

													<div class="timeline_footer">
														<a href="" target="_blank" class="cossort-sam">
															<img src="/kanto/user/img/test/cossoticon.png" width="70px" class="pull-left">
														</a>
													




														<div class="footcoment">
															<div class="shopname">
																<a href="" target="_blank">コソット</a>からメールが届いてます。
															</div>
															<div class="mailtitle">
																※早めの返信をお願いします。

															</div>
														</div>
														<div class="boxclear"></div>
													</div>

												</li>
												<!---メールが来てます-->


												<!---ユーザーが登録されました-->
												<li class="timeblok">
													<div class="timeline_body">
														<div class="timeline_icon">
															<img src="/kanto/user/img/icon/user.png" width="50px" class="pull-left">
															<div class="pull-left">
																<div class="timeline_title">新しいユーザー。</div>
																<p class="time-watch"><i class="fa fa-clock-o"></i>昨日のニュース</p>
															</div>
															<div class="boxclear"></div>
															<div class="timelinecoment">
																<span class="shopimg">
																<img src="/kanto/user/img/icon/caution.png">新しいユーザーが登録されました。
																</span>
															




															</div>
														</div>

													</div>

													<div class="timeline_footer">
														<a href="" target="_blank" class="cossort-sam">
															<img src="/kanto/user/img/test/adm_photo.gif" width="70px" class="pull-left userimg">
														</a>
													




														<div class="footcoment">
															<div class="shopname">
																<a href="" target="_blank">MARIA</a>
															</div>
															<div class="newuserarea">
																エリア:【東京都】
															</div>
															<div class="newuserentry">
																登録日:【2017/08/24】
															</div>
														</div>
														<div class="boxclear"></div>
													</div>

												</li>
												<!---ユーザーが登録されました-->





												<!---ブログが投稿されました-->
												<li class="timeblok">
													<div class="timeline_body">
														<div class="timeline_icon">
															<img src="/kanto/user/img/icon/blog.png" width="50px" class="pull-left">
															<div class="pull-left">
																<div class="timeline_title">ブログが投稿されました。</div>
																<p class="time-watch"><i class="fa fa-clock-o"></i>昨日のニュース</p>
															</div>
															<div class="boxclear"></div>
															<div class="timelinecoment">
																<span class="shopimg">
																	<a href="" target="_blank">
																		<img src="/kanto/user/img/test/sanple1.jpg">
																		お店の名前がさいだいでにじゅうもじまで
																	</a>
																</span>
																がブログを投稿しました。
															</div>
														</div>

													</div>


													<div class="timeline_footer">
														<a href="" target="_blank">
															<img src="/kanto/blog/img/test/neko.jpg" width="90px" class="pull-left">
														</a>
													


														<div class="footcoment">
															<div class="shopname">
																<a href="" target="_blank">今ならバック率100%キャンペーン</a>
															</div>
															<div class="blogcome">ここには文章が入ります。ここには文章が入ります。ここには文章が入ります。ここには文章が入ります。
															</div>
														</div>
														<div class="boxclear"></div>
													</div>

												</li>
												<!---ブログが投稿されました-->
											</ul>
											

											
									