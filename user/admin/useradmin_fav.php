<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "○○さんの管理画面";
	$meta_description = "";
	$meta_keywords = "";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>


<!--アイコン用CSS-->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--専用のCSS-->
<link href="/user/css/style_admin.css" rel="stylesheet" type="text/css" />

<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

<!--アプリケーション画像拡大-->
<link rel="stylesheet" href="/kanto/app/zoom/css/lightbox.css">

<!--アプリケーションモーダルウインドウ-->
<script src="/kanto/app/modal/modal-multi.js"></script>


<!--テキスト省略 別ファイル-->
<script type="text/javascript" src="/kanto/user/js/textcount.js"></script>
<!--テキスト省略-->

<!--文字カウント-->
<script type="text/javascript">
$(function(){
    var countMax = 330;
    $('textarea').bind('keydown keyup keypress change',function(){
        var thisValueLength = $(this).val().length;
        var countDown = (countMax)-(thisValueLength);
        $('.count').html(countDown);
 
        if(countDown < 0){
            $('.count').css({color:'#ff0000',fontWeight:'bold'});
        } else {
            $('.count').css({color:'#000000',fontWeight:'normal'});
        }
    });
    $(window).load(function(){
        $('.count').html(countMax);
    });
});
</script>
<!--文字カウント-->

</head>

<body class="admin">

<div id="container">

	<!--グローバルナビゲーション-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?><!--ナビゲーション-->
	<!--グローバルナビゲーション-->


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<!--ログインナビゲーション-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?><!--ログインナビゲーション-->	
		<!--ログインナビゲーション-->

		<div id="mainbox" class="clearfix"><!--コンテンツの外枠-->

			<!--パンくず-->
			<ul id="pan">
				<li><a href="index.html">トップページ</a></li>
				<li><a href="index.html">*****</a></li>
				<li><a href="index.html">*****</a></li>
				<li>*****</li>

			</ul>
			<!--パンくず-->

			<div class="boxs clearfix">


					<div class="leftbox">
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-list.php"); ?>
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-info.php"); ?>
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-caution.php"); ?>
					</div>


				<div class="centerbox">

					
					<div class="fav-hedblok">
						<div class="hed-icon"><i class="fa fa-star-o"></i></div>
						<div class="hed-small">お店のお気に入りは登録は最大で10件まで登録が可能です。</div>
					</div>

					<div class="fav">
							

						<div class="bookshopwaku">
							<p class="bookshopkome">お店キャッチコピーお店キャッチコピー</p>
							<div class="bookleft"><a href="#"><img src="/kanto/shop/img/test/adm_photo_noimage.gif" width="111" height="111" alt=""/></a></div>
								<div class="bookright">
									<div class="rightshopname"><a href="#">ムラムラM字妻那覇店ムラムラM字妻那覇店</a></div>
									<div class="rightshophosi"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi2.png" width="20px"><img src="/kanto/common/img/hosi3.png" width="20px"> <span class="adminpoint">【3.50点】</span></div>
									<div class="rightshoparea">エリア：東京/池袋 巣鴨 大塚</div>
									<div class="rightshoptype">業種：デリバリーヘルス</div>
									<div class="rightshoptel">電話：0120-22-2354</div>
								</div>

							<div class="bookbottm"><span class="bookmaildel"><a href="" class="editbutton"><i class="fa fa-envelope-o"></i>メールを送る</a></span>
							<span class="bookmaildel"><a href="" class="editbutton">削除</a></span></div>

						</div>



						<div class="bookshopwaku">
							<p class="bookshopkome">お店キャッチコピーお店キャッチコピー</p>
							<div class="bookleft"><a href="#"><img src="/kanto/shop/img/test/adm_photo_noimage.gif" width="111" height="111" alt=""/></a></div>
								<div class="bookright">
									<div class="rightshopname"><a href="#">ムラムラM字妻那覇店ムラムラM字妻那覇店</a></div>
									<div class="rightshophosi"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi2.png" width="20px"><img src="/kanto/common/img/hosi3.png" width="20px"> <span class="adminpoint">【3.50点】</span></div>
									<div class="rightshoparea">エリア：東京/池袋 巣鴨 大塚</div>
									<div class="rightshoptype">業種：デリバリーヘルス</div>
									<div class="rightshoptel">電話：0120-22-2354</div>
								</div>

							<div class="bookbottm"><span class="bookmaildel"><a href="" class="editbutton"><i class="fa fa-envelope-o"></i>メールを送る</a></span>
							<span class="bookmaildel"><a href="" class="editbutton">削除</a></span></div>

						</div>



						<div class="bookshopwaku">
							<p class="bookshopkome">お店キャッチコピーお店キャッチコピー</p>
							<div class="bookleft"><a href="#"><img src="/kanto/shop/img/test/adm_photo_noimage.gif" width="111" height="111" alt=""/></a></div>
								<div class="bookright">
									<div class="rightshopname"><a href="#">ムラムラM字妻那覇店ムラムラM字妻那覇店</a></div>
									<div class="rightshophosi"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi2.png" width="20px"><img src="/kanto/common/img/hosi3.png" width="20px"> <span class="adminpoint">【3.50点】</span></div>
									<div class="rightshoparea">エリア：東京/池袋 巣鴨 大塚</div>
									<div class="rightshoptype">業種：デリバリーヘルス</div>
									<div class="rightshoptel">電話：0120-22-2354</div>
								</div>

							<div class="bookbottm"><span class="bookmaildel"><a href="" class="editbutton"><i class="fa fa-envelope-o"></i>メールを送る</a></span>
							<span class="bookmaildel"><a href="" class="editbutton">削除</a></span></div>

						</div>


						<div class="bookshopwaku">
							<p class="bookshopkome">お店キャッチコピーお店キャッチコピー</p>
							<div class="bookleft"><a href="#"><img src="/kanto/shop/img/test/adm_photo_noimage.gif" width="111" height="111" alt=""/></a></div>
								<div class="bookright">
									<div class="rightshopname"><a href="#">ムラムラM字妻那覇店ムラムラM字妻那覇店</a></div>
									<div class="rightshophosi"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi2.png" width="20px"><img src="/kanto/common/img/hosi3.png" width="20px"> <span class="adminpoint">【3.50点】</span></div>
									<div class="rightshoparea">エリア：東京/池袋 巣鴨 大塚</div>
									<div class="rightshoptype">業種：デリバリーヘルス</div>
									<div class="rightshoptel">電話：0120-22-2354</div>
								</div>

							<div class="bookbottm"><span class="bookmaildel"><a href="" class="editbutton"><i class="fa fa-envelope-o"></i>メールを送る</a></span>
							<span class="bookmaildel"><a href="" class="editbutton">削除</a></span></div>

						</div>
						
						
						<div class="bookshopwaku">
							<p class="bookshopkome">お店キャッチコピーお店キャッチコピー</p>
							<div class="bookleft"><a href="#"><img src="/kanto/shop/img/test/adm_photo_noimage.gif" width="111" height="111" alt=""/></a></div>
								<div class="bookright">
									<div class="rightshopname"><a href="#">ムラムラM字妻那覇店ムラムラM字妻那覇店</a></div>
									<div class="rightshophosi"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi2.png" width="20px"><img src="/kanto/common/img/hosi3.png" width="20px"> <span class="adminpoint">【3.50点】</span></div>
									<div class="rightshoparea">エリア：東京/池袋 巣鴨 大塚</div>
									<div class="rightshoptype">業種：デリバリーヘルス</div>
									<div class="rightshoptel">電話：0120-22-2354</div>
								</div>

							<div class="bookbottm"><span class="bookmaildel"><a href="" class="editbutton"><i class="fa fa-envelope-o"></i>メールを送る</a></span>
							<span class="bookmaildel"><a href="" class="editbutton">削除</a></span></div>

						</div>

					</div>
					
							<!-- ページネーション -->
							<div class="pagination-blok">
								<div id="nextcaunt">
									<?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
								</div>
								<div class="boxclear"></div>
							</div>


				</div><!--centerbox-->

				<div class="rightbox">
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/useradmin_data.php"); ?>
						
						<!--212px以内で画像のアフィリエイト部分。　求人とは関係ない女性向けの広告。プログラムは入れないで大丈夫です。-->
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/advertisement.php"); ?>
				</div>


		</div>
			<!--boxs clearfix-->




	</div>
		<!--mainbox-->


<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>



</div>
	<!--contentsin-->





		<!--フッター-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
		<!--フッター-->




</div><!--container-->


<!--モーダル検索バー-->

			<div id="modal-content-01" class="modal-content">
				<div class="modalsearchbox">
				<p class="modaltitle"><i class="fa fa-search"></i>求人を検索</p>
					<div class="modallight">
						<form>
						<input type="search" placeholder="お店の名前や業種で検索">
						<input type="image" value="検索" src="/kanto/common/light/img/search01.png">
						</form>
					</div>
				</div>
			</div>


<!--モーダル検索バー-->

<!--テキスト省略-->
	<script>
		jQuery( document ).ready( function ( $ ) {
			$( ".blogcome" ).each( function () {
				if ( $( this ).text().length > 28 ) {
					$( this ).text( $( this ).text().substr( 0, 27 ) );
					$( this ).append( '…' );
				}
			} );
		} );
	</script>
	<!--テキスト省略-->

	<!--チャート-->
	<script type="text/javascript" src="/kanto/app/Chart/Chart.min.js"></script>
	<script>/*phpの数字出力と相性悪い？？valueからのコード記述でcolor,highlight,labeなどがエラー*/
		var doughnutData = [ {
			value: <? if($arr_ranking_count["50"]=="") { echo "0";} else {echo $arr_ranking_count["50"];}?>,
			color: "#ff2b08",
			highlight: "#ff664d",
			label: "信憑性あり！"
		}, {
			value: <? if($arr_ranking_count["10"]=="") { echo "0";} else {echo $arr_ranking_count["10"];}?>,
			color: "#fc7d00",
			highlight: "#ffa248",
			label: "信憑性少しあり"
		}, {
			value: <? if($arr_ranking_count["20"]=="") { echo "0";} else {echo $arr_ranking_count["20"];}?>,
			color: "#65d600",
			highlight: "#9df152",
			label: "信憑性イマイチ"
		}, {
			value: <? if($arr_ranking_count["30"]=="") { echo "0";} else {echo $arr_ranking_count["30"];}?>,
			color: "#00d2f8",
			highlight: "#63e4fb",
			label: "信憑性なし"
		}, {
			value: <? if($arr_ranking_count["40"]=="") { echo "0";} else {echo $arr_ranking_count["40"];}?>,
			color: "#7c7c7c",
			highlight: "#a09d9d",
			label: "判定出来ず"
		}, {/*↓作動テスト用　本番では削除*/
			value: 100,
			color: "#FAAC58",
			highlight: "#F6E3CE",
			label: "判定出来ず"
		} ];
		window.onload = function(){
      	var ctx = document.getElementById("doughnut-chart").getContext("2d");
      	window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {
        responsive : true
      });
    }

	</script>



</body>
</html>

