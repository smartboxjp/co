<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "○○さんの管理画面";
	$meta_description = "";
	$meta_keywords = "";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>


<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--専用のCSS-->
<link href="/user/css/style_admin.css" rel="stylesheet" type="text/css" />

<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

<!--アプリケーション画像拡大-->
<link rel="stylesheet" href="/kanto/app/zoom/css/lightbox.css">

<!--アプリケーションモーダルウインドウ-->
<script src="/kanto/app/modal/modal-multi.js"></script>


<!--テキスト省略 別ファイル-->
<script type="text/javascript" src="/kanto/user/js/textcount.js"></script>
<!--テキスト省略-->

<!--文字カウント-->
<script type="text/javascript">
$(function(){
    var countMax = 330;
    $('textarea').bind('keydown keyup keypress change',function(){
        var thisValueLength = $(this).val().length;
        var countDown = (countMax)-(thisValueLength);
        $('.count').html(countDown);
 
        if(countDown < 0){
            $('.count').css({color:'#ff0000',fontWeight:'bold'});
        } else {
            $('.count').css({color:'#000000',fontWeight:'normal'});
        }
    });
    $(window).load(function(){
        $('.count').html(countMax);
    });
});
</script>
<!--文字カウント-->
<script language="javascript"> 
    function fnChangeDel(i) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './useradmin_img_del.php?voice_img_id='+i;
        } 
    }
</script>

</head>

<body class="admin">
<?
	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION["member_id"];
?>

<div id="container">

	<!--グローバルナビゲーション-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?><!--ナビゲーション-->
	<!--グローバルナビゲーション-->


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<!--ログインナビゲーション-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?><!--ログインナビゲーション-->	
		<!--ログインナビゲーション-->

		<div id="mainbox" class="clearfix"><!--コンテンツの外枠-->

			<!--パンくず-->
			<ul id="pan">
				<li><a href="index.html">トップページ</a></li>
				<li><a href="index.html">*****</a></li>
				<li><a href="index.html">*****</a></li>
				<li>*****</li>

			</ul>
			<!--パンくず-->



			<div class="boxs clearfix">


				<div class="leftbox">
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-list.php"); ?>
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-info.php"); ?>
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-caution.php"); ?>
				</div>


				<div class="centerbox">

						<div class="contents">
							
							<div class="photo-hedblok">
								<div class="hed-icon"><i class="fa fa-picture-o"></i></div>
									<div class="hed-small">投稿した画像はここで自由に削除することができます。<br>
									コメントのみの変更は出来きませんので再度写真をアップして下さい。</div>
							</div>

							<div class="photo">
<!-- アップロード -->

								<div class="tubnakawaku"><!--画像エリア外枠-->

<?php
    
    $view_count=12;   // List count
    $offset=0;

    if(!$page)
    {
        $page=1;
    }
    Else
    {
        $offset=$view_count*($page-1);
    }
    
    $where = "";
    $where .= " and i.member_id='".$member_id."' ";
    $where .= " and i.flag_open='1' ";
    $where .= " and s.flag_open='1' ";

    //合計
    $sql_count = "SELECT count(i.shop_id) as all_count FROM voice_img i inner join shop s on i.shop_id=s.shop_id where s.flag_open=1 ".$where ;
    
    $db_result_count = $common_dao->db_query_bind($sql_count);
    if($db_result_count)
    {
        $all_count = $db_result_count[0]["all_count"];
    }
    
    //リスト表示
    $arr_db_field = array("voice_img_id");
    
    $sql = "SELECT i.shop_id, s.shop_name, i.regi_date, i.up_date, i.voice_img_title, i.img_1, i.up_date, ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM voice_img i inner join shop s on i.shop_id=s.shop_id where s.flag_open=1 ".$where ;
    if($order_name != "")
    {
        $sql .= " order by ".$order_name." ".$order;
    }
    else
    {
        $sql .= " order by i.regi_date desc";
    }
    $sql .= " limit $offset,$view_count";
    
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $inner_count = count($db_result);
        for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
        {
            foreach($arr_db_field as $val)
            {
                $$val = $db_result[$db_loop][$val];
            }
            $shop_id = $db_result[$db_loop]["shop_id"];
            $shop_name=$db_result[$db_loop]["shop_name"];
            $regi_date = $db_result[$db_loop]["regi_date"];
            $up_date = $db_result[$db_loop]["up_date"];
            $voice_img_title = $db_result[$db_loop]["voice_img_title"];
            $img_1 = $db_result[$db_loop]["img_1"];
?>
									<div class="imglistboxsoto">

										<div class="imageboxnaka">
											<figure><!--マウスオーバーテキスト-->
						                        <? if($img_1!="") { ?>
						                        <div class="samimg"><img src="/<? echo global_voice_img_dir.$voice_img_id."/".$img_1."?d=".date(his);?>"></div>
						                        <? } ?>
												<figcaption>
													<p class="captext"><a href="/kanto/shop/?shop_id=<? echo $shop_id;?>"><? echo $shop_name;?></a></p>
												</figcaption>
											</figure>

											<form action="./useradmin_img_up.php" method="POST" name="form_write">
											<input type="hidden" name="voice_img_id" value="<? echo $voice_img_id;?>">
											<div class="caption"><input type="text" name="voice_img_title" size="14" value="<? echo $voice_img_title;?>" class="hoge" maxlength="10" /></div>
											<div class="inputbutton">
												<button type="text" class="editbutton">登録</button>
												<a href="#" onClick='fnChangeDel("<?php echo $voice_img_id;?>");' class="editbutton">削除</a>
											</div>
											</form>
										</div><!--box内-->

									</div><!--box外----------------------->
<?
		}
	}
?>


							
								</div>


							</div>
							
							<!-- ページネーション -->
							<div class="pagination-blok">
								<div id="nextcaunt">
									<?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
								</div>
								<div class="boxclear"></div>
							</div>
							

						</div><!--contents-->

				</div><!--centerbox-->



					<div class="rightbox">
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/useradmin_data.php"); ?>
						
						<!--212px以内で画像のアフィリエイト部分。　求人とは関係ない女性向けの広告。プログラムは入れないで大丈夫です。-->
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/advertisement.php"); ?>
					</div>


			</div>
			<!--boxs clearfix-->




		</div>
		<!--mainbox-->


		<!--ページトップ-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>


	</div>
	<!--contentsin-->





		<!--フッター-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
		<!--フッター-->




</div><!--container-->


<!--モーダル検索バー-->

			<div id="modal-content-01" class="modal-content">
				<div class="modalsearchbox">
				<p class="modaltitle"><i class="fa fa-search"></i>求人を検索</p>
					<div class="modallight">
						<form>
						<input type="search" placeholder="お店の名前や業種で検索">
						<input type="image" value="検索" src="/kanto/common/light/img/search01.png">
						</form>
					</div>
				</div>
			</div>


<!--モーダル検索バー-->

	<!--テキスト省略-->
	<script>
		jQuery( document ).ready( function ( $ ) {
			$( ".blogcome" ).each( function () {
				if ( $( this ).text().length > 28 ) {
					$( this ).text( $( this ).text().substr( 0, 27 ) );
					$( this ).append( '…' );
				}
			} );
		} );
	</script>
	<!--テキスト省略-->

	<!--チャート-->
	<script type="text/javascript" src="/kanto/app/Chart/Chart.min.js"></script>
	<script>/*phpの数字出力と相性悪い？？valueからのコード記述でcolor,highlight,labeなどがエラー*/
		var doughnutData = [ {
			value: <? if($arr_ranking_count["50"]=="") { echo "0";} else {echo $arr_ranking_count["50"];}?>,
			color: "#ff2b08",
			highlight: "#ff664d",
			label: "信憑性あり！"
		}, {
			value: <? if($arr_ranking_count["10"]=="") { echo "0";} else {echo $arr_ranking_count["10"];}?>,
			color: "#fc7d00",
			highlight: "#ffa248",
			label: "信憑性少しあり"
		}, {
			value: <? if($arr_ranking_count["20"]=="") { echo "0";} else {echo $arr_ranking_count["20"];}?>,
			color: "#65d600",
			highlight: "#9df152",
			label: "信憑性イマイチ"
		}, {
			value: <? if($arr_ranking_count["30"]=="") { echo "0";} else {echo $arr_ranking_count["30"];}?>,
			color: "#00d2f8",
			highlight: "#63e4fb",
			label: "信憑性なし"
		}, {
			value: <? if($arr_ranking_count["40"]=="") { echo "0";} else {echo $arr_ranking_count["40"];}?>,
			color: "#7c7c7c",
			highlight: "#a09d9d",
			label: "判定出来ず"
		}, {/*↓作動テスト用　本番では削除*/
			value: 100,
			color: "#FAAC58",
			highlight: "#F6E3CE",
			label: "判定出来ず"
		} ];
		window.onload = function(){
      	var ctx = document.getElementById("doughnut-chart").getContext("2d");
      	window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {
        responsive : true
      });
    }

	</script>

<script type="text/javascript" src="/kanto/app/zoom/js/lightbox.js"></script>

</body>
</html>

