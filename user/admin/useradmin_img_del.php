<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>画像データ削除</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?
    //ログインチェック
    $common_connect -> Fn_member_check();
    $member_id = $_SESSION["member_id"];

    $voice_img_id = $common_connect->h($_GET["voice_img_id"]);
    
    
    if($voice_img_id=="" || $member_id=="")
    {
        $common_connect -> Fn_javascript_back("正しく入力してください。");
    }
    
    if(file_exists($_SERVER['DOCUMENT_ROOT']."/".global_voice_img_dir.$voice_img_id))
    {
        $common_connect -> Fn_deldir($_SERVER['DOCUMENT_ROOT']."/".global_voice_img_dir.$voice_img_id);
    }
    //削除
    $dbup = "delete from voice_img where voice_img_id='".$voice_img_id."' and member_id='".$member_id."' ";
    $db_result = $common_dao->db_update($dbup);
    
    $common_connect-> Fn_javascript_move("削除しました", "useradmin_img.php");
?>
</body>
</html>