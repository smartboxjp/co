<?php 
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	$meta_title = "タイトル";
	$meta_description = "description です";
	$meta_keywords = "キーワードです";
?>

<?php require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/kanto/common/header/header_meta.php";?>


<!--アイコン用CSS-->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--専用のCSS-->
<link href="/user/css/style_admin.css" rel="stylesheet" type="text/css"/>
<!--アプリケーション/フォームデザイン-->
<link href="/kanto/app/form/formoid-default-skyblue.css" rel="stylesheet" type="text/css" />
<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

<!--アプリケーション画像拡大-->
<link rel="stylesheet" href="/kanto/app/zoom/css/lightbox.css">

<!--アプリケーションモーダルウインドウ-->
<script src="/kanto/app/modal/modal-multi.js"></script>


</head>

<body class="admin">

<div id="container">

	<!--グローバルナビゲーション-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?><!--ナビゲーション-->
	<!--グローバルナビゲーション-->


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<!--ログインナビゲーション-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?><!--ログインナビゲーション-->	
		<!--ログインナビゲーション-->

		<div id="mainbox" class="clearfix"><!--コンテンツの外枠-->

			<!--パンくず-->
			<ul id="pan">
				<li><a href="index.html">トップページ</a></li>
				<li><a href="index.html">*****</a></li>
				<li><a href="index.html">*****</a></li>
				<li>*****</li>

			</ul>
			<!--パンくず-->


			<div class="boxs clearfix">

				<div class="leftbox">
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/user/common/adminleft-list.php"); ?>
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/user/common/adminleft-info.php"); ?>
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/user/common/adminleft-caution.php"); ?>
				</div>


				<div class="centerbox">


						<div class="contents">

							<div class="mail-hedblok">
								<div class="hed-icon"><i class="fa fa-envelope-o"></i></div>
								<div class="hed-small">返信が来たメールは早めに返信してください。</div>
							</div>
						
						
							<div class="mailbox_details">
								
								
									<div class="mailhed_left">
										<a href=""><img src="/kanto/shop/img/test/sanple1.jpg" width="60px"></a>
									</div>
									
									<div class="mailhed_left">
										<div class="mailshopname"><a href="">お店の名前がここには表示されます。最大で</a></div>
										<div class="jobtype">(デリバリーヘルス)</div>
										<div class="deta">2017/08/25　17:25</div>
									</div>
									
									
								
									
									<div class="lineandclea"></div>
								
							
								
									<div class="detailsbody">
										本文が表示されます
										
										本文が表示されます

										本文が表示されます

										本文が表示されます

										本文が表示されます

										本文が表示されます

										本文が表示されます

										本文が表示されます

										<p>本文が表示されます</p>
										<p>本文が表示されます</p>
										<p>本文が表示されます</p>
										<p>本文が表示されます</p>
										<p>本文が表示されます</p>
										<p>本文が表示されます</p>
										<p>本文が表示されます</p>
										<p>本文が表示されます</p>
										<p>本文が表示されます</p>
										
									</div>
								
									
									<div class="detailsbottom">
									
									<div class="replybox">
										
										<form class="formoid-default-skyblue" method="post">
												
											<div class="element-textarea">
												<label class="title">上記のメールに返信してみましょう。</label>
												<textarea style="font-size:13px;" class="medium" name="textarea" placeholder="返信内容を入力して下さい。" cols="25" rows="8" ></textarea>
											</div>
												
											
												<div class="submit"><input type="submit" value="返信する"/></div>

										</form>
										

									</div>
									
									</div>


							</div>



						</div><!--contents-->
						
						



				</div><!--centerbox-->

					<div class="rightbox">
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/useradmin_data.php"); ?>
						
						<!--212px以内で画像のアフィリエイト部分。　求人とは関係ない女性向けの広告。プログラムは入れないで大丈夫です。-->
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/advertisement.php"); ?>
					</div>


			</div>
			<!--boxs clearfix-->




		</div>
		<!--mainbox-->

			<!--ページトップ-->
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/".$global_area."/common/footer/page_top.php"); ?>


	</div>
	<!--contentsin-->





		<!--フッター-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
		<!--フッター-->




</div><!--container-->


<!--モーダル検索バー-->

			<div id="modal-content-01" class="modal-content">
				<div class="modalsearchbox">
				<p class="modaltitle"><i class="fa fa-search"></i>求人を検索</p>
					<div class="modallight">
						<form>
						<input type="search" placeholder="お店の名前や業種で検索">
						<input type="image" value="検索" src="/kanto/common/light/img/search01.png">
						</form>
					</div>
				</div>
			</div>


<!--モーダル検索バー-->

	<!--テキスト省略-->
	<script>
		jQuery( document ).ready( function ( $ ) {
			$( ".blogcome" ).each( function () {
				if ( $( this ).text().length > 28 ) {
					$( this ).text( $( this ).text().substr( 0, 27 ) );
					$( this ).append( '…' );
				}
			} );
		} );
	</script>
	<!--テキスト省略-->

	<!--チャート-->
	<script type="text/javascript" src="/kanto/app/Chart/Chart.min.js"></script>
	<script>/*phpの数字出力と相性悪い？？valueからのコード記述でcolor,highlight,labeなどがエラー*/
		var doughnutData = [ {
			value: <? if($arr_ranking_count["50"]=="") { echo "0";} else {echo $arr_ranking_count["50"];}?>,
			color: "#ff2b08",
			highlight: "#ff664d",
			label: "信憑性あり！"
		}, {
			value: <? if($arr_ranking_count["10"]=="") { echo "0";} else {echo $arr_ranking_count["10"];}?>,
			color: "#fc7d00",
			highlight: "#ffa248",
			label: "信憑性少しあり"
		}, {
			value: <? if($arr_ranking_count["20"]=="") { echo "0";} else {echo $arr_ranking_count["20"];}?>,
			color: "#65d600",
			highlight: "#9df152",
			label: "信憑性イマイチ"
		}, {
			value: <? if($arr_ranking_count["30"]=="") { echo "0";} else {echo $arr_ranking_count["30"];}?>,
			color: "#00d2f8",
			highlight: "#63e4fb",
			label: "信憑性なし"
		}, {
			value: <? if($arr_ranking_count["40"]=="") { echo "0";} else {echo $arr_ranking_count["40"];}?>,
			color: "#7c7c7c",
			highlight: "#a09d9d",
			label: "判定出来ず"
		}, {/*↓作動テスト用　本番では削除*/
			value: 100,
			color: "#FAAC58",
			highlight: "#F6E3CE",
			label: "判定出来ず"
		} ];
		window.onload = function(){
      	var ctx = document.getElementById("doughnut-chart").getContext("2d");
      	window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {
        responsive : true
      });
    }

	</script>


<script type="text/javascript" src="/kanto/app/zoom/js/lightbox.js"></script>

</body>
</html>