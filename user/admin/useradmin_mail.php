<?php 
	require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	$meta_title = "○○さんの管理画面";
	$meta_description = "";
	$meta_keywords = "";
?>

<?php require_once $_SERVER[ 'DOCUMENT_ROOT' ] . "/kanto/common/header/header_meta.php";?>


<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--専用のCSS-->
<link href="/user/css/style_admin.css" rel="stylesheet" type="text/css"/>

<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

<!--アプリケーション画像拡大-->
<link rel="stylesheet" href="/kanto/app/zoom/css/lightbox.css">

<!--アプリケーションモーダルウインドウ-->
<script src="/kanto/app/modal/modal-multi.js"></script>


<!--テキスト省略 別ファイル-->
<script type="text/javascript" src="/kanto/user/js/textcount.js"></script>
<!--テキスト省略-->


<![endif]-->
<script type="text/javascript" src="/kanto/user/js/jquery.cookie.js"></script>
<script type="text/javascript">
$(function(){
n = ($.cookie('opened'));
if ( n === undefined ) { n = 0; }
$(".tab li").eq(n).addClass("active");
$(".tab").nextAll(".panel").eq(n).show();
if (n > 0) {
    $(".tab li").removeClass("active");
    $(".tab li").eq(n).addClass("active");
    $(".tab").nextAll(".panel").hide();
    $(".tab").nextAll(".panel").eq(n).show();
}
$(".tab li").click(function(){
    var index = $(this).parent("ul").children("li").index(this);
    $(this).siblings("li").removeClass("active");
    $.cookie("opened",index);
    $(this).addClass("active");
    $(this).parent("ul").nextAll(".panel").hide();
    $(this).parent("ul").nextAll(".panel").eq(index).show();

    if ($(this).parent("ul").nextAll(".panel").eq(index).hasClass('calendar')){
        // カレンダーを表示
        $('.panel.calendar').show();
        $('#calendar').fullCalendar('render');
    }
});

n = ($.cookie('opened2'));
if ( n === undefined ) { n = 0; }
$(".tab2 li").eq(n).addClass("active");
$(".tab2").nextAll(".panel").eq(n).show();
if (n > 0) {
    $(".tab2 li").removeClass("active");
    $(".tab2 li").eq(n).addClass("active");
    $(".tab2").nextAll(".panel").hide();
    $(".tab2").nextAll(".panel").eq(n).show();
}
$(".tab2 li").click(function(){
    var index = $(this).parent("ul").children("li").index(this);
    $(this).siblings("li").removeClass("active");
    $.cookie("opened2",index);
    $(this).addClass("active");
    $(this).parent("ul").nextAll(".panel").hide();
    $(this).parent("ul").nextAll(".panel").eq(index).show();
});

$(window).load(function(){
		$('input[type=text],input[type=password],textarea').each(function(){
				var thisTitle = $(this).attr('title');
				if(!(thisTitle === '')){
						$(this).wrapAll('<span style="text-align:left;display:inline-block;position:relative;"></span>');
						$(this).parent('span').append('<span class="placeholder">' + thisTitle + '</span>');
						$('.placeholder').css({
								top:'8px',
								left:'10px',
								fontSize:'120%',
								lineHeight:'120%',
								textAlign:'left',
								color:'#999',
								overflow:'hidden',
								position:'absolute',
								zIndex:'99'
						}).click(function(){
								$(this).prev().focus();
						});

						$(this).focus(function(){
								$(this).next('span').css({display:'none'});
						});

						$(this).blur(function(){
								var thisVal = $(this).val();
								if(thisVal === ''){
										$(this).next('span').css({display:'inline-block'});
								} else {
										$(this).next('span').css({display:'none'});
								}
						});

						var thisVal = $(this).val();
						if(thisVal === ''){
								$(this).next('span').css({display:'inline-block'});
						} else {
								$(this).next('span').css({display:'none'});
						}
				}
		});
});


})
</script>

</head>

<body class="admin">

<div id="container">

	<!--グローバルナビゲーション-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?><!--ナビゲーション-->
	<!--グローバルナビゲーション-->


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<!--ログインナビゲーション-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?><!--ログインナビゲーション-->	
		<!--ログインナビゲーション-->

		<div id="mainbox" class="clearfix"><!--コンテンツの外枠-->

			<!--パンくず-->
			<ul id="pan">
				<li><a href="index.html">トップページ</a></li>
				<li><a href="index.html">*****</a></li>
				<li><a href="index.html">*****</a></li>
				<li>*****</li>

			</ul>
			<!--パンくず-->


			<div class="boxs clearfix">

				<div class="leftbox">
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-list.php"); ?>
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-info.php"); ?>
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-caution.php"); ?>
				</div>


				<div class="centerbox">


						<div class="contents">

							<div class="mail-hedblok">
								<div class="hed-icon"><i class="fa fa-envelope-o"></i></div>
								<div class="hed-small">返信が来たメールは早めに返信してください。</div>
							</div>
						
						
							<div class="mailbox">



								<ul class="tab2 clearfix">
									<li>受信メール一覧</li>
									<li>送信メール一覧</li>
								</ul>



								<div class="panel smail">

									<table width="100%">
										<tr>
											<th width="20%"><div>受信日</div></th>
											<th width="20%"><div>状態</div></th>
											<th width="45%"><div>送信者【店名】</div></th>
											<th width="15%"><div>指示</div></th>
										</tr>

										<tr>
											<td><div class="date">08/11 19:21</div></td>
											<td><div class="flag red">[未読]</div></td>
											<td><a href="/kanto/user/admin/useradmin_mail_details.php"><span class="meilshopname">お店のなまえお店お店のなまえお店こぶとり</span></a>
											<br><span class="jobtype">（デリバリーヘルス）</span></td>
											<td><div class="date3"><a href="" class="editbutton">削除</a></div></td>
										</tr>

										<tr>
										<td><div class="date">08/11 19:21</div></td>
										<td><div class="flag gray">[既読]</div></td>
										<td><a href="#"><span class="meilshopname">お店のなまえお店お店のなまえお店こ</span></a>
										<br><span class="jobtype">（デリバリーヘルス）</span></td>
										<td><div class="date3"><a href="" class="editbutton">削除</a></div></td>
										</tr>

										<tr>
										<td><div class="date">08/11 19:21</div></td>
										<td><div class="flag green">[返信済み]</div></td>
										<td><a href="#"><span class="meilshopname">お店のなまえお店お店のなまえお店こ</span></a>
										<br><span class="jobtype">（デリバリーヘルス）</span></td>
										<td><div class="date3"><a href="" class="editbutton">削除</a></div></td>
										</tr>

										<tr>
										<td><div class="date">08/11 19:21</div></td>
										<td><div class="flag green">[返信済み]</div></td>
										<td><a href="#"><span class="meilshopname">お店のなまえお店お店のなまえお店こ</span></a>
										<br><span class="jobtype">（デリバリーヘルス）</span></td>
										<td><div class="date3"><a href="" class="editbutton">削除</a></div></td>
										</tr>

										<tr>
										<td><div class="date">08/11 19:21</div></td>
										<td><div class="flag gray">[既読]</div></td>
										<td><a href="#"><span class="meilshopname">お店のなまえお店お店のなまえお店こ</span></a>
										<br><span class="jobtype">（デリバリーヘルス）</span></td>
										<td><div class="date3"><a href="" class="editbutton">削除</a></div></td>
										</tr>

										<tr>
										<td><div class="date">08/11 19:21</div></td>
										<td><div class="flag green">[返信済み]</div></td>
										<td><a href="#"><span class="meilshopname">クラブ桜</span></a>
										<br><span class="jobtype">（デリバリーヘルス）</span></td>
										<td><div class="date3"><a href="" class="editbutton">削除</a></div></td>
										</tr>

										<tr>
										<td><div class="date">08/11 19:21</div></td>
										<td><div class="flag red">[未読]</div></td>
										<td><a href="#"><span class="meilshopname">お店のなまえお店お店のなまえお店こ</span></a>
										<br><span class="jobtype">（デリバリーヘルス）</span></td>
										<td><div class="date3"><a href="" class="editbutton">削除</a></div></td>
										</tr>

										<tr>
										<td><div class="date">08/11 19:21</div></td>
										<td><div class="flag green">[返信済み]</div></td>
										<td><a href="#"><span class="meilshopname">クラブ桜</span></a>
										<br><span class="jobtype">（デリバリーヘルス）</span></td>
									<td><div class="date3"><a href="" class="editbutton">削除</a></div></td>
										</tr>

										<tr>
										<td><div class="date">08/11 19:21</div></td>
										<td><div class="flag green">[返信済み]</div></td>
										<td><a href="#"><span class="meilshopname">クラブ桜</span></a>
										<br><span class="jobtype">（デリバリーヘルス）</span></td>
										<td><div class="date3"><a href="" class="editbutton">削除</a></div></td>
										</tr>

										<tr>
										<td><div class="date">08/11 19:21</div></td>
										<td><div class="flag green">[返信済み]</div></td>
										<td><a href="#"><span class="meilshopname">クラブ桜</span></a>
										<br><span class="jobtype">（デリバリーヘルス）</span></td>
										<td><div class="date3"><a href="" class="editbutton">削除</a></div></td>
										</tr>
									</table>

									<!-- ページネーション -->
									<div class="pagination-blok">
										<div id="nextcaunt">
											<?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
										</div>
										<div class="boxclear"></div>
									</div>

								</div>





								<div class="panel smail">


									<table width="100%">
										<tr>
										<th width="20%"><div>送信日</div></th>
										<th width="20%"><div>送信歴</div></th>
										<th width="45%"><div>送信先【店名】</div></th>
										<th width="15%"><div>指示</div></th>
										</tr>

										<tr>
										<td><div class="date">08/11 19:21</div></td>
										<td><div class="flag red">初送信</div></td>
										<td><a href="#"><span class="meilshopname">お店のなまえお店お店のなまえお店こ</span></a>
										<br><span class="jobtype">（デリバリーヘルス）</span></td>
										<td><div class="date3"><a href="" class="editbutton">削除</a></div></td>
										</tr>

										<tr>
										<td><div class="date">08/11 19:21</div></td>
										<td><div class="flag green">2回目以降</div></td>
										<td><a href="#"><span class="meilshopname">お店のなまえお店お店のなまえお店こ</span></a>
										<br><span class="jobtype">（デリバリーヘルス）</span></td>
										<td><div class="date3"><a href="" class="editbutton">削除</a></div></td>
										</tr>

										<tr>
										<td><div class="date">08/11 19:21</div></td>
										<td><div class="flag green">2回目以降</div></td>
										<td><a href="#"><span class="meilshopname">お店のなまえお店お店のなまえお店こ</span></a>
										<br><span class="jobtype">（デリバリーヘルス）</span></td>
										<td><div class="date3"><a href="" class="editbutton">削除</a></div></td>
										</tr>
									</table>

									<!-- ページネーション -->
									<div class="pagination-blok">
										<div id="nextcaunt">
											<?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
										</div>
									<div class="boxclear"></div>
									</div>


								</div>

						<!---メールボックス-->
							</div>



						</div><!--contents-->
						
						



				</div><!--centerbox-->

					<div class="rightbox">
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/useradmin_data.php"); ?>
						
						<!--212px以内で画像のアフィリエイト部分。　求人とは関係ない女性向けの広告。プログラムは入れないで大丈夫です。-->
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/advertisement.php"); ?>
					</div>


			</div>
			<!--boxs clearfix-->




		</div>
		<!--mainbox-->

			<!--ページトップ-->
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>


	</div>
	<!--contentsin-->





		<!--フッター-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
		<!--フッター-->




</div><!--container-->


<!--モーダル検索バー-->

			<div id="modal-content-01" class="modal-content">
				<div class="modalsearchbox">
				<p class="modaltitle"><i class="fa fa-search"></i>求人を検索</p>
					<div class="modallight">
						<form>
						<input type="search" placeholder="お店の名前や業種で検索">
						<input type="image" value="検索" src="/kanto/common/light/img/search01.png">
						</form>
					</div>
				</div>
			</div>


<!--モーダル検索バー-->

	<!--テキスト省略-->
	<script>
		jQuery( document ).ready( function ( $ ) {
			$( ".blogcome" ).each( function () {
				if ( $( this ).text().length > 28 ) {
					$( this ).text( $( this ).text().substr( 0, 27 ) );
					$( this ).append( '…' );
				}
			} );
		} );
	</script>
	<!--テキスト省略-->

	<!--チャート-->
	<script type="text/javascript" src="/kanto/app/Chart/Chart.min.js"></script>
	<script>/*phpの数字出力と相性悪い？？valueからのコード記述でcolor,highlight,labeなどがエラー*/
		var doughnutData = [ {
			value: <? if($arr_ranking_count["50"]=="") { echo "0";} else {echo $arr_ranking_count["50"];}?>,
			color: "#ff2b08",
			highlight: "#ff664d",
			label: "信憑性あり！"
		}, {
			value: <? if($arr_ranking_count["10"]=="") { echo "0";} else {echo $arr_ranking_count["10"];}?>,
			color: "#fc7d00",
			highlight: "#ffa248",
			label: "信憑性少しあり"
		}, {
			value: <? if($arr_ranking_count["20"]=="") { echo "0";} else {echo $arr_ranking_count["20"];}?>,
			color: "#65d600",
			highlight: "#9df152",
			label: "信憑性イマイチ"
		}, {
			value: <? if($arr_ranking_count["30"]=="") { echo "0";} else {echo $arr_ranking_count["30"];}?>,
			color: "#00d2f8",
			highlight: "#63e4fb",
			label: "信憑性なし"
		}, {
			value: <? if($arr_ranking_count["40"]=="") { echo "0";} else {echo $arr_ranking_count["40"];}?>,
			color: "#7c7c7c",
			highlight: "#a09d9d",
			label: "判定出来ず"
		}, {/*↓作動テスト用　本番では削除*/
			value: 100,
			color: "#FAAC58",
			highlight: "#F6E3CE",
			label: "判定出来ず"
		} ];
		window.onload = function(){
      	var ctx = document.getElementById("doughnut-chart").getContext("2d");
      	window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {
        responsive : true
      });
    }

	</script>



</body>
</html>