<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	$meta_title = "○○さんの管理画面";
	$meta_description = "";
	$meta_keywords = "";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--専用のCSS-->
<link href="/user/css/style_admin.css" rel="stylesheet" type="text/css" />

<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

<!--アプリケーション/フォームデザイン-->
<link href="/kanto/app/form/formoid1/formoid-default-skyblue.css" rel="stylesheet" type="text/css" />

<!--アプリケーション cropper-->
<link href="/kanto/app/cropper/cropper.min.css" rel="stylesheet">

<!--アプリケーションモーダルウインドウ-->
<script src="/kanto/app/modal/modal-multi.js"></script>

<!--アプリケーション チュートリアル-->
<link href="/kanto/app/chard/chardinjs.css" rel="stylesheet">
<script src="/kanto/app/chard/chardinjs.min.js"></script>


<!--設定ページドロっプダウン-->
<script>
$(function(){
    $('.usertitle li').hover(function(){
        $("ul:not(:animated)", this).slideDown();
    }, function(){
        $("ul.child",this).slideUp();
    });
});
</script>
<!--設定ページドロっプダウン-->

<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
				$('#form_regist').submit();
		});
	});
</script>

</head>

<body class="admin">
<?
	//ログインチェック
	$common_connect -> Fn_member_check();
	
	
	$arr_data = array();
	$arr_data[] = "member_img";
	$arr_member = $common_member -> Fn_session_member_info($arr_data);
	
	$member_img = $arr_member[0]["member_img"];
?>

<div id="container">

	<!--グローバルナビゲーション-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?><!--ナビゲーション-->
	<!--グローバルナビゲーション-->


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<!--ログインナビゲーション-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?><!--ログインナビゲーション-->	
		<!--ログインナビゲーション-->

		<div id="mainbox" class="clearfix"><!--コンテンツの外枠-->

			<!--パンくず-->
			<ul id="pan">
				<li><a href="index.html">トップページ</a></li>
				<li><a href="index.html">*****</a></li>
				<li><a href="index.html">*****</a></li>
				<li>*****</li>

			</ul>
			<!--パンくず-->


			<div class="boxs clearfix">
				<div class="leftbox">
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-list.php"); ?>
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-info.php"); ?>
						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-caution.php"); ?>
				</div>

<!--クリッパー-->

				<div class="rightblok">
          			<form action="./useradmin_prfimg_save.php" method="POST" name="form_regist" id="form_regist" class="formoid-default-skyblue" enctype="multipart/form-data">
          			
						<div class="element-file">
							<div id="mainbg" data-intro="最初にプロフィールにしたい画像を選択して下さい" data-position="top">
							<label class="title topspc-minus">プロフィールに使用する画像を選んで下さい。</label>
							<label class="large" >
								<div class="button">画像選択</div>
                  			<label title="Upload image file" for="inputImage" class="btn-primary">
                  		<input type="file" accept="image/*" name="file" id="inputImage" class="hide file_input">
                  		<div class="file_text">ファイルが選択されてません</div>
              				</label>
							</div>
						</div>
            			<input type="hidden" name="cropped" class="cropped">
					</form>
					

						<div class="campus">
							<div class="campusleft">
								<div id="mainbg" data-intro="選択された画像はキャンパスに表示されます。マウスでカットしたい場所に動かしてください。" data-position="left">
								<div class="image-crop">
									<img src="/kanto/app/cropper/img/p3.jpg">
								</div>
								</div>
							</div>


							<div class="previwblok">
								<div class="img-preview img-preview-sm"></div>
							</div>


							<div class="campusright">


								<div id="mainbg" data-intro="画像を操作出来ます。回転、拡大、縮小などでお好みの位置にセットしてください。" data-position="bottom">
                                				<div class="btn-group">
									<div class="btnplayoutleft topspc spcleft">
										<button class="cntrolbtn" id="rotateLeft" type="button"><i class="fa fa-undo"></i></button>
									</div>

									<div class="btnplayoutleft">
										<div class="btnblok">
											<button class="cntrolbtn" id="zoomIn" type="button"><i class="fa fa-search-plus"></i></button>
										</div>
										<div class="btnblok">
                      									<button class="cntrolbtn" id="zoomOut" type="button"><i class="fa fa-search-minus"></i></button>
										</div>
									</div>

									<div class="btnplayoutleft topspc spcright">
                    								<button class="cntrolbtn" id="rotateRight" type="button"><i class="fa fa-repeat"></i></button>
									</div>
                  						</div>
								</div>

								<div class="enterbtn">
									<button class="endbtn" id="form_confirm" type="button"><i class="fa fa-scissors"></i>これで決定</button>
									<a href="#" class="picbutton topspc"><i class="fa fa-trash-o" aria-hidden="true"></i>画像を削除をする</a>
								</div>
                					</div>

<script type="text/javascript">
$(function(){
$('body').chardinJs();
$('a[data-toggle="chardinjs"]').on('click', function(e) {
    e.preventDefault();
    $('body').data('chardinJs').toggle();
});
});
</script>


							<div class="campusright-bottom">
								<div id="kaisetsu"><a href="" data-toggle="chardinjs"><i class="fa fa-exclamation-circle"></i>プロフィール画像の編集方法</a></div>
							</div>

							<div class="campusbottom"></div>
            </div>
				</div>







<!--クリッパー-->

			</div><!--boxs clearfix-->



		</div>
		<!--mainbox-->

		<!--ページトップ-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>

	</div>
	<!--contentsin-->





		<!--フッター-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
		<!--フッター-->




</div><!--container-->

<!--モーダル検索バー-->


			<div id="modal-content-01" class="modal-content">
				<div class="modalsearchbox">
				<p class="modaltitle"><i class="fa fa-search"></i>求人を検索</p>
					<div class="modallight">
						<form>
						<input type="search" placeholder="お店の名前や業種で検索">
						<input type="image" value="検索" src="/kanto/common/light/img/search01.png">
						</form>
					</div>
				</div>
			</div>



<!--モーダル検索バー-->

<!--フォームファイル名表示-->
<script type="text/javascript" src="/kanto/app/form/formoid1/formoid-default-skyblue.js"></script>
<!--フォームファイル名表示-->
<!-- cropper -->
    <script src="/kanto/app/cropper/cropper.min.js"></script>
    <script>
        $(document).ready(function(){

            var $image = $(".image-crop > img")
            $($image).cropper({
                aspectRatio: 1,//トリミング比率サイズ
                preview: ".img-preview",
                done: function(data) {
                    // Output the result data for cropping image.
										$('.cropped').val($image.cropper("getDataURL"));
                }
            });

            var $inputImage = $("#inputImage");
            if (window.FileReader) {
                $inputImage.change(function() {
                    var fileReader = new FileReader(),
                            files = this.files,
                            file;

                    if (!files.length) {
                        return;
                    }

                    file = files[0];

                    if (/^image\/\w+$/.test(file.type)) {
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function () {
                            $inputImage.val("");
                            $image.cropper("reset", true).cropper("replace", this.result);
                        };
                    } else {
                        showMessage("Please choose an image file.");
                    }
                });
            } else {
                $inputImage.addClass("hide");
            }

            $("#download").click(function() {
                window.open($image.cropper("getDataURL"));
            });

            $("#zoomIn").click(function() {
                $image.cropper("zoom", 0.1);
            });

            $("#zoomOut").click(function() {
                $image.cropper("zoom", -0.1);
            });

            $("#rotateLeft").click(function() {
                $image.cropper("rotate", 45);
            });

            $("#rotateRight").click(function() {
                $image.cropper("rotate", -45);
            });

            $("#setDrag").click(function() {
                $image.cropper("setDragMode", "crop");
            });

            $('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true
            });

            $('#data_2 .input-group.date').datepicker({
                startView: 1,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                format: "dd/mm/yyyy"
            });

            $('#data_3 .input-group.date').datepicker({
                startView: 2,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });

            $('#data_4 .input-group.date').datepicker({
                minViewMode: 1,
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                todayHighlight: true
            });

            $('#data_5 .input-daterange').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });

            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, { color: '#1AB394' });

            var elem_2 = document.querySelector('.js-switch_2');
            var switchery_2 = new Switchery(elem_2, { color: '#ED5565' });

            var elem_3 = document.querySelector('.js-switch_3');
            var switchery_3 = new Switchery(elem_3, { color: '#1AB394' });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green'
            });

            $('.demo1').colorpicker();

            var divStyle = $('.back-change')[0].style;
            $('#demo_apidemo').colorpicker({
                color: divStyle.backgroundColor
            }).on('changeColor', function(ev) {
                        divStyle.backgroundColor = ev.color.toHex();
                    });

            $('.clockpicker').clockpicker();

            $('input[name="daterange"]').daterangepicker();

            $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

            $('#reportrange').daterangepicker({
                format: 'MM/DD/YYYY',
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2015',
                dateLimit: { days: 60 },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'right',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-primary',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Cancel',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            });

            $(".select2_demo_1").select2();
            $(".select2_demo_2").select2();
            $(".select2_demo_3").select2({
                placeholder: "Select a state",
                allowClear: true
            });


            $(".touchspin1").TouchSpin({
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

            $(".touchspin2").TouchSpin({
                min: 0,
                max: 100,
                step: 0.1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                postfix: '%',
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });

            $(".touchspin3").TouchSpin({
                verticalbuttons: true,
                buttondown_class: 'btn btn-white',
                buttonup_class: 'btn btn-white'
            });


        });
        var config = {
                '.chosen-select'           : {},
                '.chosen-select-deselect'  : {allow_single_deselect:true},
                '.chosen-select-no-single' : {disable_search_threshold:10},
                '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                '.chosen-select-width'     : {width:"95%"}
                }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }

        $("#ionrange_1").ionRangeSlider({
            min: 0,
            max: 5000,
            type: 'double',
            prefix: "$",
            maxPostfix: "+",
            prettify: false,
            hasGrid: true
        });

        $("#ionrange_2").ionRangeSlider({
            min: 0,
            max: 10,
            type: 'single',
            step: 0.1,
            postfix: " carats",
            prettify: false,
            hasGrid: true
        });

        $("#ionrange_3").ionRangeSlider({
            min: -50,
            max: 50,
            from: 0,
            postfix: "°",
            prettify: false,
            hasGrid: true
        });

        $("#ionrange_4").ionRangeSlider({
            values: [
                "January", "February", "March",
                "April", "May", "June",
                "July", "August", "September",
                "October", "November", "December"
            ],
            type: 'single',
            hasGrid: true
        });

        $("#ionrange_5").ionRangeSlider({
            min: 10000,
            max: 100000,
            step: 100,
            postfix: " km",
            from: 55000,
            hideMinMax: true,
            hideFromTo: false
        });

        $(".dial").knob();

        $("#basic_slider").noUiSlider({
            start: 40,
            behaviour: 'tap',
            connect: 'upper',
            range: {
                'min':  20,
                'max':  80
            }
        });

        $("#range_slider").noUiSlider({
            start: [ 40, 60 ],
            behaviour: 'drag',
            connect: true,
            range: {
                'min':  20,
                'max':  80
            }
        });

        $("#drag-fixed").noUiSlider({
            start: [ 40, 60 ],
            behaviour: 'drag-fixed',
            connect: true,
            range: {
                'min':  20,
                'max':  80
            }
        });


    </script>



</body>
</html>

