<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	$common_image = new CommonImage(); //画像
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	$meta_title = "会員登録修正";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>
	<link href="./css/login.css" rel="stylesheet" type="text/css" />

</head>

<body>
<?
	$datetime = date("Y-m-d H:i:s");
	
	$arr_where = array();
	$var = "s_member_id";
	$arr_where[$var] = $_SESSION['member_id'];
	$arr_member = $common_member -> Fn_member_info($arr_where);
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($cropped == "")
	{
		$common_connect -> Fn_javascript_back("正しく画像を選択して下さい。");
	}
	
	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION['member_id'];
	

	//Folder生成
	$up_file_name = "member_img.png";
	$save_dir = $global_path.global_member_dir.$member_id."/";
	$fname_new_name[1] = $up_file_name;
	
	//Folder生成
	$common_image -> create_folder ($save_dir);
	//ヘッダに「data:image/png;base64,」が付いているので、それは外す
	$cropped = preg_replace("/data:[^,]+,/i","",$cropped);
	
	//残りのデータはbase64エンコードされているので、デコードする
	$cropped = base64_decode($cropped);
	
	//まだ文字列の状態なので、画像リソース化
	$image = imagecreatefromstring($cropped);
 
	imagesavealpha($image, TRUE); // 透明色の有効
	imagepng($image ,$save_dir.$fname_new_name[1]);
	
	$common_image -> create_thumbnail($save_dir.$up_file_name, "", $save_dir, 540);

	$arr_where = array();
	$var = "member_id"; $arr_where[$var] = $_SESSION['member_id'];
	
	$arr_data = array();
	$var = "member_img"; $arr_data[$var] = $fname_new_name[1];
	
	$common_member -> Fn_member_update($arr_data, $arr_where);

	$common_connect-> Fn_javascript_move("修正しました。", global_no_ssl."/kanto/user/admin/useradmin_prfimg.php");
?>

</body>

</html>