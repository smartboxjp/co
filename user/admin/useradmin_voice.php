<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonVoice.php";
	$common_voice = new CommonVoice();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_cate_area = new CommonCateArea();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateJob.php";
	$common_cate_job = new CommonCateJob();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonShop.php";
	$common_shop = new CommonShop();
	
	//エリア
	$arr_area = $common_cate_area->Fn_cate_area_list();
	if(!is_null($arr_area))
	{
		foreach($arr_area as $arr_key=>$arr_value)
		{
			$arr_cate_area_s_id[$arr_value["cate_area_s_id"]] = $arr_value["cate_area_l_title"];
			$arr_cate_area_s_ids[$arr_value["cate_area_l_id"]][] = $arr_value["cate_area_s_id"];
		}
	}
	
	//職種
	$arr_job = $common_cate_job->Fn_db_cate_job_all();
	if(!is_null($arr_job))
	{
		foreach($arr_job as $arr_key=>$arr_value)
		{
			$arr_cate_job_id[$arr_value["cate_job_id"]] = $arr_value["cate_job_title"];
		}
	}
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}
	
	$meta_title = $_SESSION['nickname']."さんの管理画面";
	$meta_description = "";
	$meta_keywords = "";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>


<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--専用のCSS-->
<link href="/user/css/style_admin.css" rel="stylesheet" type="text/css" />

<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

<!--アプリケーション/フォームデザイン-->
<link href="/kanto/app/form/formoid-default-skyblue.css" rel="stylesheet" type="text/css" />

<!--アプリケーション画像拡大-->
<link rel="stylesheet" href="/kanto/app/zoom/css/lightbox.css">

<!--アプリケーションモーダルウインドウ-->
<script src="/kanto/app/modal/modal-multi.js"></script>


<!--テキスト省略 別ファイル-->
<script type="text/javascript" src="/kanto/user/js/textcount.js"></script>
<!--テキスト省略-->

<!--文字カウント-->
<script type="text/javascript">
	$( function () {
		var countMax = 30;
		$( 'input' ).bind( 'keydown keyup keypress change', function () {
			var thisValueLength = $( this ).val().length;
			var countDown = ( countMax ) - ( thisValueLength );
			$( '.count' ).html( countDown );

			if ( countDown < 0 ) {
				$( '.count' ).css( {
					color: '#ff0000',
					fontWeight: 'bold'
				} );
			} else {
				$( '.count' ).css( {
					color: '#000000',
					fontWeight: 'normal'
				} );
			}
		} );
		$( window ).load( function () {
			$( '.count' ).html( countMax );
		} );
	} );

	$( function () {
		$( '.comment1,.comment2,.comment3' ).bind( 'keyup', function () {
			for ( num = 1; num <= 3; num++ ) {
				var thisValueLength = $( ".comment" + num ).val().replace( /\s+/g, '' ).length; // ←★replace()を追加
				$( ".count" + num ).html( thisValueLength );
			}
		} );
	} );
</script>

<script type="text/javascript">
	$( function () {
		$( '#form_confirm' ).click( function () {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input_in( "voice_title", 30 );
			err_check_count += check_input_sel( "voice_period" );
			err_check_count += check_radio( "voice_star" );
			err_check_count += check_input_over( "voice_comment_1", 15 );
			err_check_count += check_input_over( "voice_comment_2", 15 );
			err_check_count += check_input_over( "voice_comment_3", 130 );



			if ( err_check_count != 0 ) {
				alert( "入力に不備があります" );
				return false;
			} else {
				//$('#form_confirm').submit();
				$( '#form_confirm', "body" ).submit();
				return true;
			}
		} );

		function check_input( $str ) {
			$( "#err_" + $str ).html( err_default );
			$( "#" + $str ).css( background, bgcolor_default );

			if ( $( '#' + $str ).val() == "" ) {
				err = "<div style='color:#F00'>正しく入力してください。</div>";
				$( "#err_" + $str ).html( err );
				$( "#" + $str ).css( background, bgcolor_err );

				return 1;
			}
			return 0;
		}


		//以内
		function check_input_in( $str, $moji_count ) {
			$( "#err_" + $str ).html( err_default );
			$( "#" + $str ).css( background, bgcolor_default );

			if ( $( '#' + $str ).val() == "" ) {
				err = "<div style='color:#F00'>正しく入力してください。</div>";
				$( "#err_" + $str ).html( err );
				$( "#" + $str ).css( background, bgcolor_err );

				return 1;
			} else if ( $( '#' + $str ).val().replace( /\s+/g, '' ).length > $moji_count ) {
				err = "<div style='color:#F00'>" + $moji_count + "文字以内正しく入力してください。</div>";
				$( "#err_" + $str ).html( err );
				$( "#" + $str ).css( background, bgcolor_err );

				return 1;
			}

			return 0;
		}

		//以上
		function check_input_over( $str, $moji_count ) {
			$( "#err_" + $str ).html( err_default );
			$( "#" + $str ).css( background, bgcolor_default );

			if ( $( '#' + $str ).val() == "" ) {
				err = "<div style='color:#F00'>正しく入力してください。</div>";
				$( "#err_" + $str ).html( err );
				$( "#" + $str ).css( background, bgcolor_err );

				return 1;
			} else if ( $( '#' + $str ).val().replace( /\s+/g, '' ).length < $moji_count ) {
				err = "<div style='color:#F00'>" + $moji_count + "文字以上正しく入力してください。</div>";
				$( "#err_" + $str ).html( err );
				$( "#" + $str ).css( background, bgcolor_err );

				return 1;
			}

			return 0;
		}

		function check_input_sel( $str ) {
			$( "#err_" + $str ).html( err_default );
			$( "#" + $str ).css( background, bgcolor_default );

			if ( $( '#' + $str ).val() == "" ) {
				err = "<div style='color:#F00'>正しく選択してください。</div>";
				$( "#err_" + $str ).html( err );
				$( "#" + $str ).css( background, bgcolor_err );

				return 1;
			}
			return 0;
		}

		function check_radio( $str ) {
			err = "";
			$( "#err_" + $str ).html( err_default );

			if ( $( "input[name='" + $str + "']:checked" ).val() == undefined ) {
				err = "<br /><span style='color:#F00'>正しく選択してください。</span>";
			}

			if ( err != "" ) {
				$( "#err_" + $str ).html( err );
				return 1;
			}
			return 0;
		}


	} );
</script>

<!--文字カウント-->

</head>

<body class="admin">
<?
	//ログインチェック
	$common_connect -> Fn_member_check();
	$member_id = $_SESSION["member_id"];
?>
<div id="container">

	<!--グローバルナビゲーション-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?><!--ナビゲーション-->
	<!--グローバルナビゲーション-->


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<!--ログインナビゲーション-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?><!--ログインナビゲーション-->	
		<!--ログインナビゲーション-->

		<div id="mainbox" class="clearfix"><!--コンテンツの外枠-->

			<!--パンくず-->
			<ul id="pan">
				<li><a href="index.html">トップページ</a></li>
				<li><a href="index.html">*****</a></li>
				<li><a href="index.html">*****</a></li>
				<li>*****</li>

			</ul>
			<!--パンくず-->



			<div class="boxs clearfix">

				<div class="leftbox">
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-list.php"); ?>
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-info.php"); ?>
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-caution.php"); ?>
				</div>

				


				<div class="tow-voice">


					<div class="voice-hedblok">
						<div class="hed-icon"><i class="fa fa-comment-o"></i></div>
						<div class="hed-small">一度投稿した口コミは編集は出来ますが、削除をすることは出来ません。</div>
					</div>

					<div class="voice">
<?php
	$arr_voice_ranking = $common_voice ->Fn_voice_ranking();
	
	$view_count=3;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$arr_where = array();
	$arr_where["member_id"] = $member_id;	
	//合計
	$all_count = 0;

	$sql = " SELECT count(voice_id) as all_count FROM voice where flag_open=1 and member_id='".$member_id."' " ;
	$arr_voice_all = $common_dao->db_query_bind($sql, $arr_bind);
	$all_count = $arr_voice_all[0]["all_count"];
	
	//リスト表示
	$arr_etc["offset"] = $offset;
	$arr_etc["view_count"] = $view_count;

	$arr_data = array("voice_id", "shop_name", "member_id", "v.cate_area_s_id", "v.cate_job_id");
	$arr_data = array_merge($arr_data, array("voice_title", "voice_period", "voice_star"));
	$arr_data = array_merge($arr_data, array("voice_comment_1", "voice_comment_2", "voice_comment_3", "ranking", "v.regi_date", "v.up_date"));
	
	
	$arr_voice_list = $common_voice -> Fn_voice_list ($arr_data, $arr_where, null, $arr_etc);
	if(!is_null($arr_voice_list)) {
		foreach($arr_voice_list as $arr_key=>$arr_value)
		{
			foreach($arr_value as $key=>$value)
			{
				$$key=$value;
			}
?>
								
							<!--投稿口コミ-->
							<div class="item">
								<div class="sinpixyou"><img src="/kanto/common/img/smallrank<? echo $ranking;?>.png" alt="<? echo $arr_voice_ranking[$ranking];?>" /></div>
								<div class="date"><? echo substr($regi_date, 0, 10);?> 投稿済</div>
								<div class="shoptitleandsankou"><h4><a href="/kanto/shop/?shop_id=<? echo $shop_id;?>"><? echo $shop_name;?></a></h4></div>
								<p class="mypagejobcra">(<? echo $arr_cate_area_s_id[$cate_area_s_id];?>/<? echo $arr_cate_job_id[$cate_job_id];?>)</p>
								<div class="lineandclea"></div>

								<ul class="clearfix shopinfo">
								
									<li class="title"><? echo $voice_title;?></li>
									<br>
									<li class="vote"><img src="/kanto/common/img/star<? echo sprintf('%.1f',($voice_star/2));?>m.png"> <span class="adminpoint">【<? echo sprintf('%.2f',($voice_star/2));?>点】</span></li>
									<li class="comment">
										<div class="daimoku">このお店で働いた期間はどの位ですか？ </div>
											<p><? if($voice_period!="") { echo nl2br($voice_period); } else { echo "未回答";}?></p>
										<div class="daimoku">お店のいいと感じた所はどこですか？</div>
											<p><? if($voice_comment_1!="") { echo nl2br($voice_comment_1); } else { echo "未回答";}?></p>
										<div class="daimoku">お店の悪いと感じた所はどこですか？ </div>
											<p><? if($voice_comment_2!="") { echo nl2br($voice_comment_2); } else { echo "未回答";}?></p>
										<div class="daimoku">詳さらに詳しくお店の感想を教えてください</div>
											<p><? if($voice_comment_3!="") { echo nl2br($voice_comment_3); } else { echo "未回答";}?></p>
									</li>
								</ul>

								<div class="kutikomibottomadmin">
									<div class="sankoupoint"><span class="sankoupoint"><i class="fa fa-heart"></i>参考になった数（21）</span></div>
									
									<div class="like_box">
									<a class="modal-syncer button-link editbutton" data-target="modal-content-02"><i class="fa fa-pencil"></i>編集する</a>
									</div>
									
									<div class="kutikomibottomadmincenter"></div>
								</div>
							</div>
<?
		}
	}
?>


							<div class="kzinextbox">

							</div>

						</div>
						<!-- 口コミ -->
						
						
						<!-- ページネーション -->
						<div class="pagination-blok">
							<div id="nextcaunt">
								<?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
							</div>
							<div class="boxclear"></div>
						</div>
				</div><!--tow-voice-->
				
				
				<div class="rightbox_voice">
					<!--212px以内で画像のアフィリエイト部分。　求人とは関係ない女性向けの広告。プログラムは入れないで大丈夫です。-->
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/l_advertisement.php"); ?>
				</div>

			</div><!--boxs clearfix-->

		<!--ページトップ-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>

		</div>
		<!--mainbox-->



	</div>
	<!--contentsin-->





		<!--フッター-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
		<!--フッター-->




</div><!--container-->



<!--モーダル検索バー-->

			<div id="modal-content-01" class="modal-content">
				<div class="modalsearchbox">
				<p class="modaltitle"><i class="fa fa-search"></i>求人を検索</p>
					<div class="modallight">
						<form>
						<input type="search" placeholder="お店の名前や業種で検索">
						<input type="image" value="検索" src="/kanto/common/light/img/search01.png">
						</form>
					</div>
				</div>
			</div>


<!--モーダル検索バー-->


<!--口コミ編集-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/voice-edit.php"); ?>
<!--口コミ編集-->




<!-- Events -->
<script>
  $(document).on('opening', '.remodal', function () {
    console.log('opening');
  });

  $(document).on('opened', '.remodal', function () {
    console.log('opened');
  });

  $(document).on('closing', '.remodal', function (e) {
    console.log('closing' + (e.reason ? ', reason: ' + e.reason : ''));
  });

  $(document).on('closed', '.remodal', function (e) {
    console.log('closed' + (e.reason ? ', reason: ' + e.reason : ''));
  });

  $(document).on('confirmation', '.remodal', function () {
    console.log('confirmation');
  });

  $(document).on('cancellation', '.remodal', function () {
    console.log('cancellation');
  });

//  Usage:
//  $(function() {
//
//    // In this case the initialization function returns the already created instance
//    var inst = $('[data-remodal-id=modal]').remodal();
//
//    inst.open();
//    inst.close();
//    inst.getState();
//    inst.destroy();
//  });

  //  The second way to initialize:
  $('[data-remodal-id=modal2]').remodal({
    modifier: 'with-red-theme'
  });
</script>


</body>
</html>