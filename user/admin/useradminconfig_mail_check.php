<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "メール変更";
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMemberTemp.php";
	$common_member_temp = new CommonMemberTemp();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_catearea = new CommonCateArea();

	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}

?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--専用のCSS-->
<link href="/user/css/style_admin.css" rel="stylesheet" type="text/css" />

<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

<!--フォームデザイン-->
<link rel="stylesheet" href="/kanto/app/form/formoid1/formoid-default-skyblue.css" type="text/css" />

<!--アプリケーションモーダルウインドウ-->
<script src="/kanto/app/modal/modal-multi.js"></script>

<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->

</head>

<body class="admin">
<?

	//ログインチェック
	$common_connect -> Fn_member_check();
	
	if($temp_key=="")
	{
		$common_connect-> Fn_javascript_back("正しく入力して下さい。");
	}
	
	//発行されてキーが一致してるかをチェック
	$arr_where = array();
	$arr_where["temp_key"] = $temp_key;
	
	$arr_data = array();
	$arr_data[] = "member_temp_id";
	$arr_data[] = "login_email";
	
	$arr_temp_key = $common_member_temp->Fn_db_member_temp_data($arr_data, $arr_where)  ;

	if(is_null($arr_temp_key[0]))
	{
		$common_connect-> Fn_javascript_back("もう一度メール変更を行ってください。");
	}
	else
	{
		$login_email = $arr_temp_key[0]["login_email"];
	}
	
	//メールupdate
	$arr_data = array();
	$arr_data["login_email"] = $login_email;
	
	$arr_where = array();
	$arr_where["member_id"] = $_SESSION["member_id"];
	
	$common_member->Fn_member_update ($arr_data, $arr_where);

	//同じemailがあれば削除
	$arr_where = array();
	$arr_where["login_email"] = $login_email;
	$common_member_temp -> Fn_db_member_temp_del($arr_where);
?>

<div id="container">

	<!--グローバルナビゲーション-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?><!--ナビゲーション-->
	<!--グローバルナビゲーション-->


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<!--ログインナビゲーション-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?><!--ログインナビゲーション-->	
		<!--ログインナビゲーション-->

		<div id="mainbox" class="clearfix"><!--コンテンツの外枠-->

			<!--パンくず-->
			<ul id="pan">
				<li><a href="index.html">トップページ</a></li>
				<li><a href="index.html">*****</a></li>
				<li><a href="index.html">*****</a></li>
				<li>*****</li>

			</ul>
			<!--パンくず-->

			<!--ユーザー設定ナビゲーション-->
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/userconfig.php"); ?>
			<!--ユーザー設定ナビゲーション-->


			<div class="boxs clearfix">


				<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/user/common/adminleft.php"); ?>

				<div class="configblok">


					<div class="configzoom-top">
						<i class="fa fa-envelope-o" aria-hidden="true"></i>メール変更完了しました。
					</div>

				</div><!--configblok-->

			</div><!--boxs clearfix-->

		</div>
		<!--mainbox-->


<!--ページトップ-->
			<p id="pagetop"><a href="#container"><img src="/kanto/common/img/pagetop.gif" alt="Page Top" width="130" height="26" /></a></p>


	</div>
	<!--contentsin-->





		<!--フッター-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
		<!--フッター-->




</div><!--container-->



<!--モーダル検索バー-->

			<div id="modal-content-01" class="modal-content">
				<div class="modalsearchbox">
				<p class="modaltitle"><i class="fa fa-search"></i>求人を検索</p>
					<div class="modallight">
						<form>
						<input type="search" placeholder="お店の名前や業種で検索">
						<input type="image" value="検索" src="/kanto/common/light/img/search01.png">
						</form>
					</div>
				</div>
			</div>


<!--モーダル検索バー-->


<!-- Events -->
<script>
  $(document).on('opening', '.remodal', function () {
    console.log('opening');
  });

  $(document).on('opened', '.remodal', function () {
    console.log('opened');
  });

  $(document).on('closing', '.remodal', function (e) {
    console.log('closing' + (e.reason ? ', reason: ' + e.reason : ''));
  });

  $(document).on('closed', '.remodal', function (e) {
    console.log('closed' + (e.reason ? ', reason: ' + e.reason : ''));
  });

  $(document).on('confirmation', '.remodal', function () {
    console.log('confirmation');
  });

  $(document).on('cancellation', '.remodal', function () {
    console.log('cancellation');
  });

//  Usage:
//  $(function() {
//
//    // In this case the initialization function returns the already created instance
//    var inst = $('[data-remodal-id=modal]').remodal();
//
//    inst.open();
//    inst.close();
//    inst.getState();
//    inst.destroy();
//  });

  //  The second way to initialize:
  $('[data-remodal-id=modal2]').remodal({
    modifier: 'with-red-theme'
  });
</script>



<script type="text/javascript" src="/kanto/app/zoom/js/lightbox.js"></script>
</body>
</html>

