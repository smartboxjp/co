<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonEmail.php";
	$common_email = new CommonEmail(); //メール関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMemberTemp.php";
	$common_member_temp = new CommonMemberTemp();
	
	$meta_title = "会員登録/ログイン";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

</head>

<body>
<?
	$datetime = date("Y-m-d H:i:s");
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}
	
	if($login_email=="")
	{
		$common_connect-> Fn_javascript_back("正しく入力して下さい。");
	}
	
	$arr_where = array();
	$arr_where["login_email"] = $login_email;
	
	$arr_data = array();
	$arr_data[] = "member_id";
	
	//既に登録されているメールかをチェック
	$result_login_email = $common_member->Fn_db_member_data_select ($arr_data, $arr_where);
	
	if(!is_null($result_login_email[0]))
	{
		$common_connect-> Fn_javascript_back("既に登録されているメールです。");
	}
	
	//同じemailがあれば削除
	$arr_where = array();
	$arr_where["login_email"] = $login_email;
	$common_member_temp -> Fn_db_member_temp_del($arr_where);
	
	
	$temp_key = date("Ymd").$common_connect-> Fn_random_password(20);
	
	//仮登録を行う
	$arr_data = array();
	$arr_data["login_email"] = $login_email;
	$arr_data["temp_key"] = $temp_key;
	$common_member_temp -> Fn_member_temp_insert_temp($arr_data);
	

	$temp_url = global_no_ssl."/user/admin/useradminconfig_mail_check.php?temp_key=".$temp_key;

	//Thank youメール
	if ($login_email != "")
	{
		$subject = "『 コソット 』メールアドレス変更のお知らせ";
		
		$body = file_get_contents("./mail/useradminconfig_mail.user.php");
		$body = str_replace("[login_email]", $login_email, $body);
		$body = str_replace("[temp_url]", $temp_url, $body);
		$body = str_replace("[datetime]", $datetime, $body);
		$body = str_replace("[global_send_mail]", $global_send_mail, $body);
		$body = str_replace("[global_email_footer]", $global_email_footer, $body);
		
		$common_email-> Fn_send_utf($login_email."<".$login_email.">",$subject,$body,$global_mail_from,$global_send_mail);
	}
	
	//$common_connect -> Fn_email_log($login_email, $subject, $body); //メールログ
	//$common_email-> Fn_send_utf($global_send_mail."<".$global_send_mail.">",$subject,$body,$global_mail_from,$global_send_mail);

	$common_connect-> Fn_redirect(global_no_ssl."/user/admin/useradminconfig_mail_tanks.php?login_email=".$login_email);
?>

</body>

</html>
