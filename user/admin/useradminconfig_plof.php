<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCateArea.php";
	$common_catearea = new CommonCateArea();
	
	
	$meta_title = "○○さんの管理画面";
	$meta_description = "";
	$meta_keywords = "";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>

<!--アイコン用CSS-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--専用のCSS-->
<link href="/user/css/style_admin.css" rel="stylesheet" type="text/css" />

<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

<!--フォームデザイン-->
<link rel="stylesheet" href="/kanto/app/form/formoid1/formoid-default-skyblue.css" type="text/css" />

<!--アプリケーションモーダルウインドウ-->
<script src="/kanto/app/modal/modal-multi.js"></script>

<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->


<!--文字カウント-->
<script type="text/javascript">
$(function(){
    var countMax = 330;
    $('textarea').bind('keydown keyup keypress change',function(){
        var thisValueLength = $(this).val().length;
        var countDown = (countMax)-(thisValueLength);
        $('.count').html(countDown);
 
        if(countDown < 0){
            $('.count').css({color:'#ff0000',fontWeight:'bold'});
        } else {
            $('.count').css({color:'#000000',fontWeight:'normal'});
        }
    });
    $(window).load(function(){
        $('.count').html(countMax);
    });
});
</script>
<!--文字カウント-->


<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			err_check = false;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input("member_comment");
			
			if(err_check_count)
			{
				//alert("入力に不備があります");
				return false;
			}
			else
			{
				$('#form_confirm').submit();
				return true;
			}
			
			
		});
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			$("#"+$str).removeClass("error").removeClass("reauired");
			
			if($('#'+$str).val().replace(/　/g," ").match(/^\s+$/))
			{
				err ="<span class='error'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				$("#"+$str).toggleClass("reauired").addClass("error");
				$("#"+$str).focus();
				
				return 1;
			}
			else if($('#'+$str).val()=="")
			{
				err ="<span class='error'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				$("#"+$str).toggleClass("reauired").addClass("error");
				$("#"+$str).focus();
				
				return 1;
			}
			return 0;
		}
		
		//メールチェック
		function check_input_email($str_1) 
		{
			$("#err_"+$str_1).html(err_default);
			$("#"+$str_1).css(background,bgcolor_default);
			$("#"+$str_1).removeClass("error").removeClass("reauired");
			
			if($('#'+$str_1).val()!="")
			{
				if(checkIsEmail($('#'+$str_1).val()) == false)
				{
					err ="<span class='error'>メールアドレスは半角英数字でご入力ください。</span>";
					$("#err_"+$str_1).html(err);
					$("#"+$str_1).css(background,bgcolor_err);
					$("#"+$str_1).toggleClass("reauired").addClass("error");
					$("#"+$str_1).focus();
					
					return 1;
				}
			}
			
			return 0;
		}

		//メールチェック
		function check_input_password($str_1, $str_2) 
		{
			$("#err_"+$str_1).html(err_default);
			$("#"+$str_1).css(background,bgcolor_default);
			$("#"+$str_1).removeClass("error").removeClass("reauired");
			
			$("#err_"+$str_2).html(err_default);
			$("#"+$str_2).css(background,bgcolor_default);
			$("#"+$str_2).removeClass("error").removeClass("reauired");
			
			if($('#'+$str_1).val() != "" )
			{
				if(checkIsPassword($('#'+$str_1).val()) == false)
				{
					err ="<span class='error'>パスワードは６文字以上の半角英数字「!#$%&@()*+,.」でご入力ください。</span>";
					$("#err_"+$str_1).html(err);
					$("#"+$str_1).css(background,bgcolor_err);
					$("#"+$str_1).toggleClass("reauired").addClass("error");
					$("#"+$str_1).focus();
					
					return 1;
				}
				else if($('#'+$str_1).val().length < 6)
				{
					err ="<span class='error'>パスワードは６文字以上の半角英数字「!#$%&@()*+,.」でご入力ください。</span>";
					$("#err_"+$str_1).html(err);
					$("#"+$str_1).css(background,bgcolor_err);
					$("#"+$str_1).toggleClass("reauired").addClass("error");
					$("#"+$str_1).focus();
					
					return 1;
				}
				else if($('#'+$str_1).val() != $('#'+$str_2).val() )
				{
					err ="<span class='error'>パスワードを正しく入力してください。</span>";
					$("#err_"+$str_2).html(err);
					$("#"+$str_2).css(background,bgcolor_err);
					$("#"+$str_2).toggleClass("reauired").addClass("error");
					$("#"+$str_2).focus();
					
					return 1;
				}
			}
			
			return 0;
		}
		
		//メールチェック
		function checkIsEmail(value) {
			if (value.match(/.+@.+\..+/) == null) {
				return false;
			}
			return true;
		}
		
		
		function checkIsPassword(value) 
		{
			if (value.match(/^[0-9a-zA-Z!#$%&@()*+,./_-]{6,}$/) == null) {
				return false;
			}
			return true;
		}

		
	});
	
//-->
</script>
</head>

<body class="admin">
<?
	//ログインチェック
	$common_connect -> Fn_member_check();
	
	
	$arr_data = array();
	$arr_data[] = "nickname";
	$arr_data[] = "cate_area_l_id";
	$arr_data[] = "cate_area_s_id";
	$arr_data[] = "member_comment";
	$arr_data[] = "login_email";
	$arr_member = $common_member -> Fn_session_member_info($arr_data);
?>

<div id="container">

	<!--グローバルナビゲーション-->
	<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?><!--ナビゲーション-->
	<!--グローバルナビゲーション-->


	<div id="contentsin"><!--コンテンツ内容を1000pxに抑える外枠。フッター、ヘッターは枠外-->

		<!--ログインナビゲーション-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?><!--ログインナビゲーション-->	
		<!--ログインナビゲーション-->

		<div id="mainbox" class="clearfix"><!--コンテンツの外枠-->

			<!--パンくず-->
			<ul id="pan">
				<li><a href="index.html">トップページ</a></li>
				<li><a href="index.html">*****</a></li>
				<li><a href="index.html">*****</a></li>
				<li>*****</li>

			</ul>
			<!--パンくず-->



			<div class="boxs clearfix">


				<div class="leftbox">
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-list.php"); ?>
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-info.php"); ?>
					<?php require_once($_SERVER["DOCUMENT_ROOT"]."/user/common/adminleft-caution.php"); ?>
				</div>




				<div class="configblok">


					<div class="configzoom-top">
						ユーザーデータ登録情報 (編集不可)
					</div>

					<table class="configzoom01">
					<tr> 
						<th scope="row">ニックネーム</th> 
						<td class="configdeta"><? echo $arr_member[0]["nickname"];?></td> 
					</tr> 

					<tr> 
						<th scope="row">登録エリア1</th> 
						<td class="configdeta">
            <?
						$arr_where = array();
						$arr_where["cate_area_l_id"] = $arr_member[0]["cate_area_l_id"];
						$arr_where["flag_open"] = "1";
						$return_catearea=$common_catearea -> Fn_cate_area_l_list ($arr_where);
						echo $return_catearea[0]["cate_area_l_title"];
						?>
            </td> 
					</tr> 

					<tr> 
						<th scope="row">登録エリア2</th> 
						<td class="configdeta">
            <?
						$arr_where = array();
						$arr_where["cate_area_s_id"] = $arr_member[0]["cate_area_s_id"];
						$arr_where["flag_open"] = "1";
						$return_catearea=$common_catearea -> Fn_cate_area_s_list ($arr_where);
						echo $return_catearea[0]["cate_area_s_title"];
						?>
            </td> 
					</tr> 
					</table> 

					<div class="configzoom-top topspace">
						プロフィールの編集と登録
					</div>

					<table class="configzoom01">
					<form action="useradminconfig_plof_save.php" class="formoid-default-skyblue" method="post">

					<tr> 
						<th scope="row">プロフィール編集</th> 
						<td>
            	<div class="formoid-default-skyblue">
              <? $var = "member_comment";?>
              <textarea name="<? echo $var;?>" id="<? echo $var;?>" class="configlarge" cols="20" rows="5" ><? echo $arr_member[0]["member_comment"];?></textarea>
              <label id="err_<? echo $var;?>"></label>
              </div>
              <div class="counttext">あと<span class="count">330</span>文字まで</div>
            </td> 
					</tr> 

						<th scope="row"></th> 
						<td>
            <span class="formoid-default-skyblue">
							<? $var = "form_confirm";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="submit" value="プロフィールを登録する"/>
            </span>
            </td> 
					</tr>

					</form>
					</table> 
					


				</div><!--configblok-->

			</div><!--boxs clearfix-->

		</div>
		<!--mainbox-->

		<!--ページトップ-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>

	</div>
	<!--contentsin-->





		<!--フッター-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
		<!--フッター-->




</div><!--container-->



<!--モーダル検索バー-->

			<div id="modal-content-01" class="modal-content">
				<div class="modalsearchbox">
				<p class="modaltitle"><i class="fa fa-search"></i>求人を検索</p>
					<div class="modallight">
						<form>
						<input type="search" placeholder="お店の名前や業種で検索">
						<input type="image" value="検索" src="/kanto/common/light/img/search01.png">
						</form>
					</div>
				</div>
			</div>


<!--モーダル検索バー-->


<!-- Events -->
<script>
  $(document).on('opening', '.remodal', function () {
    console.log('opening');
  });

  $(document).on('opened', '.remodal', function () {
    console.log('opened');
  });

  $(document).on('closing', '.remodal', function (e) {
    console.log('closing' + (e.reason ? ', reason: ' + e.reason : ''));
  });

  $(document).on('closed', '.remodal', function (e) {
    console.log('closed' + (e.reason ? ', reason: ' + e.reason : ''));
  });

  $(document).on('confirmation', '.remodal', function () {
    console.log('confirmation');
  });

  $(document).on('cancellation', '.remodal', function () {
    console.log('cancellation');
  });

//  Usage:
//  $(function() {
//
//    // In this case the initialization function returns the already created instance
//    var inst = $('[data-remodal-id=modal]').remodal();
//
//    inst.open();
//    inst.close();
//    inst.getState();
//    inst.destroy();
//  });

  //  The second way to initialize:
  $('[data-remodal-id=modal2]').remodal({
    modifier: 'with-red-theme'
  });
</script>


</body>
</html>

