<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	$meta_title = "会員情報修正";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>
	<link href="./css/login.css" rel="stylesheet" type="text/css" />

</head>

<body>
<?
	$datetime = date("Y-m-d H:i:s");
	
	$arr_where = array();
	$var = "s_member_id";
	$arr_where[$var] = $_SESSION['member_id'];
	$arr_member = $common_member -> Fn_member_info($arr_where);
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($member_comment == "")
	{
		$common_connect -> Fn_javascript_back("正しく入力して下さい。");
	}
	
	//ログインチェック
	$common_connect -> Fn_member_check();
	
	$arr_where = array();
	$var = "member_id"; $arr_where[$var] = $_SESSION['member_id'];
	
	$arr_data = array();
	$var = "member_comment"; $arr_data[$var] = $$var;
	
	$common_member -> Fn_member_update($arr_data, $arr_where);

	$common_connect-> Fn_javascript_move("修正しました。", global_no_ssl."/kanto/user/admin/useradminconfig_plof.php");
?>

</body>

</html>
