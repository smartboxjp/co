<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	$meta_title = "会員登録/ログイン";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>
	<link href="./css/login.css" rel="stylesheet" type="text/css" />

</head>

<body>
<?
	$datetime = date("Y-m-d H:i:s");
	
	$arr_where = array();
	$var = "s_member_id";
	$arr_where[$var] = $_SESSION['member_id'];
	$arr_member = $common_member -> Fn_member_info($arr_where);
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($member_comment == "")
	{
		$common_connect -> Fn_javascript_back("正しく入力して下さい。");
	}
	
	//ログインチェック
	$common_connect -> Fn_member_check();
	
	//すでに登録されているメールがあるかをチェック
	if($login_email!="")
	{
		$arr_where = array();
		$member_id = $_SESSION['member_id'];
		$var = "flag_open"; $arr_where[$var] = "all";
		$var = "login_email"; if($$var!="") { $arr_where[$var] = $$var; }
		
		$arr_where_not = array();
		$var = "member_id"; $arr_where_not[$var] = $$var;
		
		$arr_data = array();
		$var = "login_email"; $arr_data[$var] = $var;
		
		
		$return_member = $common_member -> Fn_db_member_data ($member_id, $arr_data, $arr_where, $arr_where_not) ;
		echo $return_member[0]["login_email"];
	}
exit;
	
	$arr_where = array();
	$var = "member_id"; $arr_where[$var] = $_SESSION['member_id'];
	
	$arr_data = array();
	$var = "login_email"; if($$var!="") { $arr_data[$var] = $$var; }
	$var = "login_pw"; if($$var!="") { $arr_data[$var] = $$var; }
	$var = "member_comment"; $arr_data[$var] = $$var;
	
	$common_member -> Fn_member_update($arr_data, $arr_where);

	$common_connect-> Fn_redirect(global_no_ssl."/kanto/member/entry_thankyou.php?login_email=".$login_email);
?>

</body>

</html>
