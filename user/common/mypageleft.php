<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonMember.php";
	$common_member = new CommonMember();
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonVoice.php";
	$common_voice = new CommonVoice();
?>
       					<div class="leftbox">
                  <div class="content_in">

                   <dl class="clearfix prof">
									<dt>
										<?
                    $arr_data = array();
                    $arr_data[] = "member_img";
                    $arr_data[] = "member_id";
                    $arr_member_left = $common_member -> Fn_session_member_info($arr_data);
                    
                    $member_img = $arr_member_left[0]["member_img"];
                    $member_id = $arr_member_left[0]["member_id"];
                    if($member_img!="") {
                    ?>
                    <img src="/<? echo global_member_dir.$member_id."/".$member_img;?>" width="120"/>
                    <?
                    } else {
                    ?>
                    <img src="/kanto/user/img/test/adm_photo.gif" width="120"/>
                    <?
                    }
                    ?>
									</dt>
									<dd>登録エリア：</dd>
										<dd>【東京】</dd>
									<dd>登録日:</dd>
										<dd>【2015/08/25】</dd>
								</dl>


								<h3><? echo $member_name;?>さんのマイページ</h3>

								<div class="darea_1">
									<div class="dareatitle">
									<i class="fa fa-comments"></i>投稿した口コミ対する信憑性
									</div>

									<dl class="clearfix">
                								<dt class="red"><img src="/kanto/common/img/smallrank50.png"></dt>
                									<dd class="red">11件</dd>
                								<dt class="orange"><img src="/kanto/common/img/smallrank10.png"></dt>
                									<dd class="orange">20件</dd>
                								<dt class="green"><img src="/kanto/common/img/smallrank20.png"></dt>
                									<dd class="green">7件</dd>
                								<dt class="blue"><img src="/kanto/common/img/smallrank30.png"></dt>
                									<dd class="blue">17件</dd>
                								<dt class="gray"><img src="/kanto/common/img/smallrank40.png"</dt>
                									<dd class="gray">15件</dd>
                								<dt class="cya total">投稿口コミ数</dt>
                									<dd class="cya total">58件</dd>
              								</dl>

              								<div class="kome">
										<a href="" target="_blank"><i class="fa fa-chevron-right" aria-hidden="true"></i>口コミの信憑性</a>
									</div>
								</div>

								


								<div class="darea_3">
									<div class="dareatitle">
									<i class="fa fa-bar-chart"></i>お店への口コミの傾向
									</div>
              								<dl class="clearfix">
										<dt>【素晴らしい】</dt>
                									<dd>0店舗</dd>
										<dt>【平均的】</dt>
                									<dd>1店舗</dd>
										<dt>【イマイチ】</dt>
                									<dd>1店舗</dd>
										<dt>【二度と働かない】</dt>
                									<dd>0店舗</dd>
									</dl>

              								<div class="kome">
										<a href="" target="_blank"><i class="fa fa-chevron-right" aria-hidden="true"></i>星の目安について</a>
									</div>
           							</div>


								<div class="darea_2">
              								<dl class="clearfix">
                								<dt><i class="fa fa-thumbs-o-up"></i> 参考になった</dt>
                									<dd>17回</dd>
                								<dt><i class="fa fa-camera-retro"></i> 投稿画像枚数</dt>
                									<dd>8枚</dd>
                								<dt><i class="fa fa-line-chart"></i> 平均評価点数</dt>
                									<dd>3.5点</dd>
              								</dl>
            							</div>
          						</div>

          						
							<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/light/prbanneruserpage.php"); ?>
        					</div>