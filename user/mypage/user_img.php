<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "○○さんの投稿した投稿画像一覧です";
	$meta_description = "○○さんがお店に投稿した投稿画像一覧にです。是非参考にして見てください。";
	$meta_keywords = "○○さんのマイページ,画像;
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>
<!--アイコン用CSS-->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!--専用css-->
<link href="/kanto/user/css/style_mypage.css" rel="stylesheet" type="text/css" />

<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

<!--アプリケーション画像拡大-->
<link rel="stylesheet" href="/kanto/app/zoom/css/lightbox.css">

<!--アプリケーション 続きを読む -->
<script type="text/javascript">
		function showMoreJq(btn) {
			var targetId = btn.getAttribute("href");	// 表示対象のid名をhref属性値から得る
			$(targetId).slideDown("slow");				// 表示対象をアニメーション効果と共に表示
			$(btn.parentNode).slideUp("fast");			// 続きを読むボタンを消す (※slideUpのほか、hideや、fadeOutなども使用可能)
			return false;								// リンクとして機能しないようfalseを返す
		}

		$(function(){
			// ▽続きの表示エリアを非表示にする
			$(".readmore-area").hide();
			// ▽「続きを読む」ボタンがクリックされた際の処理を割り当てる
			$(".readmore-button-box a").click( function() { return showMoreJq(this); } );
		});
</script>

</head>



<body class="mypage">
<?
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}
	if($member_id=="") { $common_connect -> Fn_javascript_back("正しく入力してください。"); }
?>
	<div id="container">
		<!--グローバルナビゲーション-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?><!--ナビゲーション-->
		<!--グローバルナビゲーション-->


		<div id="contentsin">

			<!--ログインナビゲーション-->
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>	

				<div id="mainbox">

      <!--パンくず-->
      					<ul id="pan">
        					<li><a href="index.html">トップページ</a></li>
        					<li><a href="index.html">*****</a></li>
        					<li><a href="index.html">*****</a></li>
        					<li>*****</li>
      					</ul>


					<div class="boxs clearfix">


						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/user/common/mypageleft.php"); ?>


						<div class="rightbox">


							<ul class="tabBtn">
								<li><a href="/user/mypage/user_voice.php"><i class="fa fa-comments"></i> 投稿した口コミ</a></li>
								<li class="activ"><i class="fa fa-camera-retro"></i> 投稿した画像</li>
								<li><a href="/user/mypage/user_prf.php"><i class="fa fa-user"></i> プロフィール</a></li>
							</ul>

							<div class="tubnakawaku">
<?php
	$member_id = $common_connect->h($_GET["member_id"]);

    $view_count=8;   // List count
    $offset=0;

    if(!$page)
    {
        $page=1;
    }
    Else
    {
        $offset=$view_count*($page-1);
    }

    $where = "";
    $where .= " and flag_open=1 ";
    $where .= " and member_id='".$member_id."' ";


    //合計
    $sql_count = "SELECT count(shop_id) as all_count FROM voice_img where 1 ".$where ;
    $db_result_count = $common_dao->db_query_bind($sql_count);
    if($db_result_count)
    {
        $all_count = $db_result_count[0]["all_count"];
    }
    
    //リスト表示
    $arr_db_field = array("voice_img_id", "member_id");
    
    $sql = "SELECT shop_id, regi_date, up_date, voice_img_title, img_1, up_date, ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM voice_img where 1 ".$where ;
    $sql .= " order by regi_date desc";
    $sql .= " limit $offset,$view_count";
    
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            foreach($arr_db_field as $val)
            {
                $$val = $db_result[$db_loop][$val];
            }
            $shop_id = $db_result[$db_loop]["shop_id"];
            $regi_date = $db_result[$db_loop]["regi_date"];
            $up_date = $db_result[$db_loop]["up_date"];
            $voice_img_title = $db_result[$db_loop]["voice_img_title"];
            $img_1 = $db_result[$db_loop]["img_1"];
?>
								<div class="imglistboxsoto">

									<div class="imageboxnaka">
										<div class="samimg">
											<a class="example-image-link" href="/kanto/user/img/test/imgtest1.jpg" data-lightbox="example-1">
												<img src="/kanto/user/img/test/imgtest1.jpg">
											</a>
										</div>
										<div class="caption"><? echo $voice_img_title;?></div>
										<div class="imgshop">
	                                            <?
	                                            if($member_id==0)
	                                            {
	                                            ?>
	                                            【お店から投稿】
	                                            <?
	                                            }
	                                            else
	                                            {

	                                                $sql_member = " select nickname from member where member_id='".$member_id."' and flag_open=1";
	                                                $db_result_member = $common_dao->db_query_bind($sql_member);
	                                                if($db_result_member)
	                                                {
	                                            ?>
	                                                <a href="<? echo "/user/mypage/user_img.php?member_id=".$member_id;?>.">【<? echo $db_result_member[0]["nickname"];?>】</a>
	                                            <?
	                                                }
	                                            ?>
	                                            <?
	                                            }
	                                            ?>
										</div>
									</div>
								</div>
<?
		}
	}
?>
							

							</div><!-- tubnsksesku -->



							<script src="/kanto/app/zoom/js/lightbox-plus-jquery.min.js"></script>

					
					    	<div class="ander">
								<div id="nextcaunt">
									<?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
									<div class="mypagebottomboxcria"></div>
								</div>
							</div>
     					
					
						</div>
     					
     					

     					
     					
      					</div>
    
				</div>

			<!--ページトップ-->
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>
		</div>


<!--フッター-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
		<!--フッター-->

		<script type="text/javascript" src="js/slide_simple_pack.js"></script>
	</div>


	<!--全ページ共通スクリプト-->
	<script type="text/javascript" src="/kanto/app/textcount/textcount.js"></script>
	<script type="text/javascript" src="/kanto/app/resize/imgLiquid-min.js"></script>
	<script type="text/javascript">
$(document).ready(function() {
    $(".samimg").imgLiquid({
    });
});
</script>
</body>
</html>

