<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "○○さんの投稿したプロフィールです";
	$meta_description = "○○さんのプロフィールです。";
	$meta_keywords = "○○さんのマイページ,プロフィール";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>
<!--アイコン用CSS-->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!--専用css-->
<link href="/kanto/user/css/style_mypage.css" rel="stylesheet" type="text/css" />

<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

<!--アプリケーション画像拡大-->
<link rel="stylesheet" href="/kanto/app/zoom/css/lightbox.css">

<!--アプリケーション 続きを読む -->
<script type="text/javascript">
		function showMoreJq(btn) {
			var targetId = btn.getAttribute("href");	// 表示対象のid名をhref属性値から得る
			$(targetId).slideDown("slow");				// 表示対象をアニメーション効果と共に表示
			$(btn.parentNode).slideUp("fast");			// 続きを読むボタンを消す (※slideUpのほか、hideや、fadeOutなども使用可能)
			return false;								// リンクとして機能しないようfalseを返す
		}

		$(function(){
			// ▽続きの表示エリアを非表示にする
			$(".readmore-area").hide();
			// ▽「続きを読む」ボタンがクリックされた際の処理を割り当てる
			$(".readmore-button-box a").click( function() { return showMoreJq(this); } );
		});
</script>

</head>



<body class="mypage">

	<div id="container">
		<!--グローバルナビゲーション-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?><!--ナビゲーション-->
		<!--グローバルナビゲーション-->


		<div id="contentsin">

			<!--ログインナビゲーション-->
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>	

				<div id="mainbox">

      <!--パンくず-->
      					<ul id="pan">
        					<li><a href="index.html">トップページ</a></li>
        					<li><a href="index.html">*****</a></li>
        					<li><a href="index.html">*****</a></li>
        					<li>*****</li>
      					</ul>


					<div class="boxs clearfix">

 						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/user/common/mypageleft.php"); ?>


						<div class="rightbox">


							<ul class="tabBtn">
								<li><a href="/user/mypage/user_voice.php"><i class="fa fa-comments"></i> 投稿した口コミ</a></li>
								<li><a href="/user/mypage/user_img.php"><i class="fa fa-camera-retro"></i> 投稿した画像</a></li>
								<li class="activ"><i class="fa fa-user"></i> プロフィール</li>
							</ul>

							<div class="mypageprof">

								<div class="mypageprofimg"><img src="/kanto/user/img/test/adm_photo.gif" width="60" height="60" /></div>
								<div class="mypageprofname">名前最大で8文字まで</div>
								<div class="mypageprofkome"><i class="fa fa-pencil"></i>まだプロフィールの記入はありません。
								<br>あああああああああああああああああああああああああああああああああうううう</div>

							</div><!--mypageprof-->

						</div>
      					</div>
    
				</div>

			<!--ページトップ-->
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>
		</div>


<!--フッター-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
		<!--フッター-->

	</div>

</body>
</html>

