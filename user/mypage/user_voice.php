<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$meta_title = "○○さんの投稿した口コミ一覧です";
	$meta_description = "○○さんがお店に投稿した口コミの一覧にです。是非参考にして見てください。";
	$meta_keywords = "○○さんのマイページ,口コミ";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT']."/kanto/common/header/header_meta.php";?>
<!--アイコン用CSS-->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!--専用css-->
<link href="/kanto/user/css/style_mypage.css" rel="stylesheet" type="text/css" />

<!--jquery-->
<script type="text/javascript" src="/kanto/app/jquery.min.js"></script>

<!--アプリケーション画像拡大-->
<link rel="stylesheet" href="/kanto/app/zoom/css/lightbox.css">

<!--アプリケーション 続きを読む -->
<script type="text/javascript">
		function showMoreJq(btn) {
			var targetId = btn.getAttribute("href");	// 表示対象のid名をhref属性値から得る
			$(targetId).slideDown("slow");				// 表示対象をアニメーション効果と共に表示
			$(btn.parentNode).slideUp("fast");			// 続きを読むボタンを消す (※slideUpのほか、hideや、fadeOutなども使用可能)
			return false;								// リンクとして機能しないようfalseを返す
		}

		$(function(){
			// ▽続きの表示エリアを非表示にする
			$(".readmore-area").hide();
			// ▽「続きを読む」ボタンがクリックされた際の処理を割り当てる
			$(".readmore-button-box a").click( function() { return showMoreJq(this); } );
		});
</script>

</head>



<body class="mypage">

	<div id="container">
		<!--グローバルナビゲーション-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/header.php"); ?><!--ナビゲーション-->
		<!--グローバルナビゲーション-->


		<div id="contentsin">

			<!--ログインナビゲーション-->
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/header/headerlogin.php"); ?>	

				<div id="mainbox">

      <!--パンくず-->
      					<ul id="pan">
        					<li><a href="index.html">トップページ</a></li>
        					<li><a href="index.html">*****</a></li>
        					<li><a href="index.html">*****</a></li>
        					<li>*****</li>
      					</ul>


					<div class="boxs clearfix">



						<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/user/common/mypageleft.php"); ?>


						<div class="rightbox">


							<ul class="tabBtn">
								<li class="activ"><i class="fa fa-comments"></i> 投稿した口コミ</li>
								<li><a href="/user/mypage/user_img.php"><i class="fa fa-camera-retro"></i> 投稿した画像</a></li>
								<li><a href="/user/mypage/user_prf.php"><i class="fa fa-user"></i> プロフィール</a></li>
							</ul>

		
							<div class="kutikomiboxmypage">

									<div class="mypagerank"><img src="/kanto/common/img/smallrank10.png"></div>
									<div class="mypagetime">2017/08/25投稿</div>
									<div class="kutikomirisutoshop"><a href="" target="_blank">ショップ名。最大で二十文字まで表示可能。</a><!--地域業種--><span class="workareakutikomi">(東京都・上野,吉原,鶯谷,日暮里,錦糸町/デリバリーヘルス)</span></div>

									<hr class="indexsikiri">

									<div class="kutikomititleindex"><a href="" target="_blank">口コミタイトルを表示します。最大で25文字まで</a></div>
									<div class="indexsterpink"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi2.png" width="20px"><img src="/kanto/common/img/hosi3.png" width="20px"><span>【4.50点】</span></div>


									<div class="maypagektikomibun">

									<!-- ▼(2)「続きを読む」ボタン▼ -->
										<p class="readmore-button-box">
											<a href="#readmore1" onclick="return showMore(this);">▼口コミ内容を詳しく見る</a>
										</p>

									<!-- ▼(3)最初は隠しておく文章▼ -->
										<div id="readmore1" class="readmore-area">

											<div class="kutikomizoomupkoumokuwaku">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
													このお店で働いた期間はどの位ですか？
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
												1年未満
											</div>

											<div class="kutikomizoomupkoumokuwaku">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
													お店のいいと思った所を教えて下さい。
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
												送迎がどこまでも来てくれえるところ
											</div>

											<div class="kutikomizoomupkoumokuwaku">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
													お店の改善してほしい所を教えて下さい。
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
												お金がとてもかかるところ
											</div>

											<div class="kutikomizoomupkoumokuwakul">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
												評価の内容を出来るだけ詳しく教えて下さい。
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
											このグループで働くのは2度目でしたが今回のお店のほうがまだましといった感じですかね。私としてはもっとこうしてほしいとい意見はありますが
											多分どこにいってもこんなもんだと思ってます。しいて言うならば、スタッフの対応は全体的に素晴らしいと感じてます。
											</div>

										</div>

										<div class="mypagebottombox">
											<div class="mypagebottomboxleft">
												<i class="fa fa-heart"></i>参考になった(24)
											</div>

											<div class="mypagebottomboxright">
												<a href="mailto:info@example.com?subject=No【】の口コミの違反報告&amp;body=口コミNo【】の口コミが違反してます。">この口コミを報告</a>
											</div>

											<div class="mypagebottomboxcria">
											</div>
										</div>
									</div>
								
							</div>

							<div class="kutikomiboxmypage">

								<div class="shopkutikomimypage">

									<div class="mypagerank"><img src="/kanto/common/img/smallrank20.png"></div>
									<div class="mypagetime">2017/08/25投稿</div>
									<div class="kutikomirisutoshop"><a href="" target="_blank">ショップ名。最大で二十文字まで表示可能。</a><!--地域業種--><span class="workareakutikomi">(東京都・上野,吉原,鶯谷,日暮里,錦糸町/デリバリーヘルス)</span></div>

									<hr class="indexsikiri">

									<div class="kutikomititleindex"><a href="" target="_blank">口コミタイトルを表示します。最大で25文字まで</a></div>
									<div class="indexsterpink"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi2.png" width="20px"><img src="/kanto/common/img/hosi3.png" width="20px"><span>【4.50点】</span></div>


									<div class="maypagektikomibun">

									<!-- ▼(2)「続きを読む」ボタン▼ -->
										<p class="readmore-button-box">
											<a href="#readmore2" onclick="return showMore(this);">▼口コミ内容を詳しく見る</a>
										</p>

									<!-- ▼(3)最初は隠しておく文章▼ -->
										<div id="readmore2" class="readmore-area">

											<div class="kutikomizoomupkoumokuwaku">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
													このお店で働いた期間はどの位ですか？
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
												1年未満
											</div>

											<div class="kutikomizoomupkoumokuwaku">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
													お店のいいと思った所を教えて下さい。
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
												送迎がどこまでも来てくれえるところ
											</div>

											<div class="kutikomizoomupkoumokuwaku">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
													お店の改善してほしい所を教えて下さい。
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
												お金がとてもかかるところ
											</div>

											<div class="kutikomizoomupkoumokuwakul">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
												評価の内容を出来るだけ詳しく教えて下さい。
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
											このグループで働くのは2度目でしたが今回のお店のほうがまだましといった感じですかね。私としてはもっとこうしてほしいとい意見はありますが
											多分どこにいってもこんなもんだと思ってます。しいて言うならば、スタッフの対応は全体的に素晴らしいと感じてます。
											</div>

										</div>

										<div class="mypagebottombox">
											<div class="mypagebottomboxleft">
												<i class="fa fa-heart"></i>参考になった(24)
											</div>

											<div class="mypagebottomboxright">
												<a href="mailto:info@example.com?subject=No【】の口コミの違反報告&amp;body=口コミNo【】の口コミが違反してます。">この口コミを報告</a>
											</div>

											<div class="mypagebottomboxcria">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="kutikomiboxmypage">

								<div class="shopkutikomimypage">

									<div class="mypagerank"><img src="/kanto/common/img/smallrank30.png"></div>
									<div class="mypagetime">2017/08/25投稿</div>
									<div class="kutikomirisutoshop"><a href="" target="_blank">ショップ名。最大で二十文字まで表示可能。</a><!--地域業種--><span class="workareakutikomi">(東京都・上野,吉原,鶯谷,日暮里,錦糸町/デリバリーヘルス)</span></div>

									<hr class="indexsikiri">

									<div class="kutikomititleindex"><a href="" target="_blank">口コミタイトルを表示します。最大で25文字まで</a></div>
									<div class="indexsterpink"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi2.png" width="20px"><img src="/kanto/common/img/hosi3.png" width="20px"><span>【4.50点】</span></div>


									<div class="maypagektikomibun">

									<!-- ▼(2)「続きを読む」ボタン▼ -->
										<p class="readmore-button-box">
											<a href="#readmore3" onclick="return showMore(this);">▼口コミ内容を詳しく見る</a>
										</p>

									<!-- ▼(3)最初は隠しておく文章▼ -->
										<div id="readmore3" class="readmore-area">

											<div class="kutikomizoomupkoumokuwaku">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
													このお店で働いた期間はどの位ですか？
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
												1年未満
											</div>

											<div class="kutikomizoomupkoumokuwaku">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
													お店のいいと思った所を教えて下さい。
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
												送迎がどこまでも来てくれえるところ
											</div>

											<div class="kutikomizoomupkoumokuwaku">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
													お店の改善してほしい所を教えて下さい。
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
												お金がとてもかかるところ
											</div>

											<div class="kutikomizoomupkoumokuwakul">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
												評価の内容を出来るだけ詳しく教えて下さい。
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
											このグループで働くのは2度目でしたが今回のお店のほうがまだましといった感じですかね。私としてはもっとこうしてほしいとい意見はありますが
											多分どこにいってもこんなもんだと思ってます。しいて言うならば、スタッフの対応は全体的に素晴らしいと感じてます。
											</div>

										</div>

										<div class="mypagebottombox">
											<div class="mypagebottomboxleft">
												<i class="fa fa-heart"></i>参考になった(24)
											</div>

											<div class="mypagebottomboxright">
												<a href="mailto:info@example.com?subject=No【】の口コミの違反報告&amp;body=口コミNo【】の口コミが違反してます。">この口コミを報告</a>
											</div>

											<div class="mypagebottomboxcria">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="kutikomiboxmypage">

								<div class="shopkutikomimypage">

									<div class="mypagerank"><img src="/kanto/common/img/smallrank50.png"></div>
									<div class="mypagetime">2017/08/25投稿</div>
									<div class="kutikomirisutoshop"><a href="" target="_blank">ショップ名。最大で二十文字まで表示可能。</a><!--地域業種--><span class="workareakutikomi">(東京都・上野,吉原,鶯谷,日暮里,錦糸町/デリバリーヘルス)</span></div>

									<hr class="indexsikiri">

									<div class="kutikomititleindex"><a href="" target="_blank">口コミタイトルを表示します。最大で25文字まで</a></div>
									<div class="indexsterpink"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi2.png" width="20px"><img src="/kanto/common/img/hosi3.png" width="20px"><span>【4.50点】</span></div>


									<div class="maypagektikomibun">

									<!-- ▼(2)「続きを読む」ボタン▼ -->
										<p class="readmore-button-box">
											<a href="#readmore4" onclick="return showMore(this);">▼口コミ内容を詳しく見る</a>
										</p>

									<!-- ▼(3)最初は隠しておく文章▼ -->
										<div id="readmore4" class="readmore-area">

											<div class="kutikomizoomupkoumokuwaku">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
													このお店で働いた期間はどの位ですか？
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
												1年未満
											</div>

											<div class="kutikomizoomupkoumokuwaku">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
													お店のいいと思った所を教えて下さい。
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
												送迎がどこまでも来てくれえるところ
											</div>

											<div class="kutikomizoomupkoumokuwaku">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
													お店の改善してほしい所を教えて下さい。
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
												お金がとてもかかるところ
											</div>

											<div class="kutikomizoomupkoumokuwakul">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
												評価の内容を出来るだけ詳しく教えて下さい。
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
											このグループで働くのは2度目でしたが今回のお店のほうがまだましといった感じですかね。私としてはもっとこうしてほしいとい意見はありますが
											多分どこにいってもこんなもんだと思ってます。しいて言うならば、スタッフの対応は全体的に素晴らしいと感じてます。
											</div>

										</div>

										<div class="mypagebottombox">
											<div class="mypagebottomboxleft">
												<i class="fa fa-heart"></i>参考になった(24)
											</div>

											<div class="mypagebottomboxright">
												<a href="mailto:info@example.com?subject=No【】の口コミの違反報告&amp;body=口コミNo【】の口コミが違反してます。">この口コミを報告</a>
											</div>

											<div class="mypagebottomboxcria">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="kutikomiboxmypage">

								<div class="shopkutikomimypage">

									<div class="mypagerank"><img src="/kanto/common/img/smallrank50.png"></div>
									<div class="mypagetime">2017/08/25投稿</div>
									<div class="kutikomirisutoshop"><a href="" target="_blank">ショップ名。最大で二十文字まで表示可能。</a><!--地域業種--><span class="workareakutikomi">(東京都・上野,吉原,鶯谷,日暮里,錦糸町/デリバリーヘルス)</span></div>

									<hr class="indexsikiri">

									<div class="kutikomititleindex"><a href="" target="_blank">口コミタイトルを表示します。最大で25文字まで</a></div>
									<div class="indexsterpink"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi1.png" width="20px"><img src="/kanto/common/img/hosi2.png" width="20px"><img src="/kanto/common/img/hosi3.png" width="20px"><span>【4.50点】</span></div>


									<div class="maypagektikomibun">

									<!-- ▼(2)「続きを読む」ボタン▼ -->
										<p class="readmore-button-box">
											<a href="#readmore5" onclick="return showMore(this);">▼口コミ内容を詳しく見る</a>
										</p>

									<!-- ▼(3)最初は隠しておく文章▼ -->
										<div id="readmore5" class="readmore-area">

											<div class="kutikomizoomupkoumokuwaku">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
													このお店で働いた期間はどの位ですか？
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
												1年未満
											</div>

											<div class="kutikomizoomupkoumokuwaku">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
													お店のいいと思った所を教えて下さい。
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
												送迎がどこまでも来てくれえるところ
											</div>

											<div class="kutikomizoomupkoumokuwaku">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
													お店の改善してほしい所を教えて下さい。
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
												お金がとてもかかるところ
											</div>

											<div class="kutikomizoomupkoumokuwakul">
												<span class="kutikomizoomupkoumokutitle"><!--口コミ本文各項目タイトル-->
												評価の内容を出来るだけ詳しく教えて下さい。
												</span>
											</div>

											<div class="kutikomizoomupkoumokuhonbun">
											このグループで働くのは2度目でしたが今回のお店のほうがまだましといった感じですかね。私としてはもっとこうしてほしいとい意見はありますが
											多分どこにいってもこんなもんだと思ってます。しいて言うならば、スタッフの対応は全体的に素晴らしいと感じてます。
											</div>

										</div>

										<div class="mypagebottombox">
											<div class="mypagebottomboxleft">
												<i class="fa fa-heart"></i>参考になった(24)
											</div>

											<div class="mypagebottomboxright">
												<a href="mailto:info@example.com?subject=No【】の口コミの違反報告&amp;body=口コミNo【】の口コミが違反してます。">この口コミを報告</a>
											</div>

											<div class="mypagebottomboxcria">
											</div>
										</div>
									</div>
								</div>
							</div>
							
							

							<div class="ander">

								<div id="nextcaunt">
									<ul>
										<li><a href="#">最初</a></li>
										<li><a href="#">1</a></li>
										<li><a href="#">2</a></li>
										<li><a href="#">3</a></li>
										<li><a href="#">4</a></li>
										<li><a href="#">5</a></li>
										<li><a href="#">6</a></li>
										<li><a href="#">7</a></li>
										<li><a href="#">8</a></li>
										<li><a href="#">9</a></li>
										<li><a href="#">10</a></li>
										<li><a href="#">最後</a></li>
									</ul>
									<div class="mypagebottomboxcria"></div>
								</div>
							</div>

						</div>
      					</div>
    
				</div>

			<!--ページトップ-->
			<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/page_top.php"); ?>
		</div>


<!--フッター-->
		<?php require_once($_SERVER["DOCUMENT_ROOT"]."/kanto/common/footer/footer.php"); ?>
		<!--フッター-->

		
	</div>

</body>
</html>

